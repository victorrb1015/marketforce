<?php
	include '../marketforce/security/session/session-settings.php';
	//DB Connection
	include 'connection.php';

	if($_SERVER['HTTP_HOST'] == 'sandbox.marketforceapp.com'){
		error_reporting(E_ALL);
	}else{
		error_reporting(0);
	}

	//Load Variables...
	$org = mysqli_real_escape_string($mf_conn, $_POST['org']);
	$user = mysqli_real_escape_string($mf_conn, $_POST['user']);
	$pass = mysqli_real_escape_string($mf_conn, $_POST['password']);

	//Search for Organization...
	$oq = "SELECT * FROM `organizations` WHERE `org_id` = '" . $org . "'";
	$og = mysqli_query($mf_conn, $oq) or die('Org Table Connection Error: ' . $mf_conn->error);
	if(mysqli_num_rows($og) <= 0){
	echo 'Not Found';
	}else{
	echo '<script>
			localStorage.setItem("org_choice","' . $org . '");
			</script>';
	$or = mysqli_fetch_array($og);
	$_SESSION['org_db_name'] = $or['db_name'];
	$_SESSION['org_logo_url'] = $or['logo_url'];
	$_SESSION['org_logo_url_alt'] = $or['org_logo_url_alt'];
	$_SESSION['org_name'] = $or['org_name'];
		$_SESSION['org_full_name'] = $or['full_org_name'];
		$_SESSION['org_abrev'] = $or['org_abrev'];
	$_SESSION['org_id'] = $or['org_id'];
	$_SESSION['org_bg'] = $or['org_bg'];
	$_SESSION['org_orders_rep_id'] = $or['orders_rep_id'];
	$_SESSION['org_orders_rep_name'] = $or['orders_rep_name'];
	$_SESSION['org_orders_rep_email'] = $or['orders_rep_email'];
	$_SESSION['org_address'] = $or['org_address'];
	$_SESSION['org_city'] = $or['org_city'];
	$_SESSION['org_state'] = $or['org_state'];
	$_SESSION['org_zip'] = $or['org_zip'];
	$_SESSION['org_email'] = $or['email'];
	$_SESSION['org_phone'] = $or['phone'];
	$_SESSION['org_global_map'] = $or['global_map'];
	$_SESSION['org_sales_portal_api_url'] = $or['sales_portal_api_url'];
	include 'connection.php';

	//Search for user
	$q = "SELECT * FROM `users` WHERE `username` = '" . $user . "' AND `password` = '" . $pass . "' AND `inactive` != 'Yes'";
	
	$get = mysqli_query($conn, $q) or die('DB CONNECTION ERROR: ' . $conn->error);

	//Check is user is valid
	if(mysqli_num_rows($get) == 0){
		/*
		$response = "That Username/Password is invalid!";
		echo $response;
		echo '<br>';
		print_r($get);
		echo '<br>';
		echo $_SESSION['org_db_name'];
		echo '<br><br>';
		echo 'User: ' . $user;
		echo '<br>';
		echo 'Pass: ' . $pass;
		*/
		echo '<script>
				window.location = "../index.php?r=' . $response . '";
			</script>';
	}else{
			//Setup SESSION Variables
		$r = mysqli_fetch_array($get);

		
		$_SESSION['fname'] = $r['fname'];
		$_SESSION['lname'] = $r['lname'];
		$_SESSION['full_name'] = $r['fname'] . ' ' . $r['lname'];
		$_SESSION['email'] = $r['email'];
		$_SESSION['user_id'] = $r['ID'];
	$_SESSION['inventory_zone'] = $r['inventory_zone'];
		$_SESSION['position'] = $r['position'];
		$_SESSION['admin'] = $r['admin'];
		$_SESSION['super_user'] = $r['super_user'];
		$_SESSION['super_group'] = $r['super_group'];
		$_SESSION['manager'] = $r['manager'];
			$_SESSION['ostr_supervisor'] = $r['ostr_supervisor'];
		$_SESSION['EC'] = $r['EC'];
		$_SESSION['accountant'] = $r['accountant'];
		$_SESSION['check_request'] = $r['check_request'];
		$_SESSION['doc_approval'] = $r['doc_approval'];
		$_SESSION['send_survey'] = $r['send_survey'];
		$_SESSION['preowned'] = $r['preowned'];
		$_SESSION['dealer_activation'] = $r['dealer_activation'];
		$_SESSION['collections'] = $r['collections'];
		$_SESSION['collections2'] = $r['collections2'];
		$_SESSION['repairs'] = $r['repairs'];
		$_SESSION['repairs_submit'] = $r['repairs_submit'];
		$_SESSION['repos'] = $r['repos'];
		$_SESSION['orders'] = $r['orders'];
		$_SESSION['inventory'] = $r['inventory'];
		$_SESSION['inventory_zone'] = $r['inventory_zone'];
		$_SESSION['in'] = 'Yes';
		$_SESSION['myState'] = $r['myState'];
		$_SESSION['myState2'] = $r['myState2'];
		$_SESSION['myState3'] = $r['myState3'];
		$_SESSION['myState4'] = $r['myState4'];
		$_SESSION['schedule_state'] = $r['schedule_state'];
		
		//INSERT LOGIN DATE PUNCH
			$pq = "UPDATE `users` SET `last_login` = CURRENT_DATE WHERE `ID` = '" . $_SESSION['user_id'] . "'";
			mysqli_query($conn, $pq) or die($conn->error);
			
			/*include '../phpmailer/PHPMailerAutoload.php';
			$mail = new PHPMailer;
			include '../phpmailsettings.php';
			$mail->addAddress('3362513969@txt.att.net');
			$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
			$mail->Subject = 'Login Request!';
			$mail->Body = $_SESSION['full_name'] . ' logged into Market Force!';
			
			if($_SESSION['full_name'] != 'Mike Burton'){
			//$mail->send();
			}*/
			
		echo "<html><script>
			//window.location='../marketforce/index.php?openmap=yes';
			window.location='../marketforce/index.php';
			</script></html>";
		
	}

}//End If Organization is found...

//include '../index.php';


?>