# plutonianbe/php54-apache:latest
FROM php:5.4-apache 

RUN a2enmod rewrite

COPY . /var/www/html/

RUN chown -R www-data:www-data /var/www/html

RUN docker-php-ext-install mysql

RUN docker-php-ext-enable mysql



