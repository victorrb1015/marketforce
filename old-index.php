<?php
//This is a test comment...
error_reporting(E_ALL);
if($_SERVER['HTTP_HOST'] == 'beta-myorders.marketforceapp.com' || $_SERVER['HTTP_HOST'] == 'orders.marketforceapp.com'){
  echo '<script>
        window.location = "oportal/";
        </script>';
}
include 'marketforce/security/session/session-settings.php';
//session_start();
include 'php/connection.php';
error_reporting(E_ALL);

if(isset($_GET['logout'])) {
		session_destroy();
	}

if($_SESSION['in'] == 'Yes'){
	
	echo '<html>
				<head>
				<script>
				window.location="marketforce/index.php";
				</script>
				</head>
				</html>';
	}
	
	$response = $_GET['r'];
	
?>
<html>
<head>
  <meta name="google-site-verification" content="CBbH-0vDxumNjMuZqRkM-SELerSUj7_FS1pT5kKIWtA" />
	   <title>Market Force | All Steel</title>
    <link rel="icon" href="landing-assets/images/favicon.ico" type="image/x-icon">
</head>
<body>
	
<form action="php/validate.php" method="post">
  <h1 style="color:grey;"><img style="width:80%;" src="assets/img/mf-logo.png"><br><br>Log In</h1>
  <div class="inset">
  <p>
    <label for="org">ORGANIZATION</label>
    <select name="org" id="org" style="width:100%;" required>
      <option value="">Select Your Organization</option>
      <?php
      $oq = "SELECT * FROM `organizations` WHERE `inactive` != 'Yes' AND `super_group` = 'ASC' ORDER BY `org_name` DESC";
      $og = mysqli_query($mf_conn, $oq) or die($conn->error);
      while($or = mysqli_fetch_array($og)){
        echo '<option value="' . $or['org_id'] . '">' . $or['org_name'] . '</option>';
      }
      ?>
    </select>
  </p>
    <br>
  <p>
    <label for="user">USERNAME</label>
    <input type="text" name="user" id="user" autofocus='autofocus' required>
  </p>
  <p>
    <label for="password">PASSWORD</label>
    <input type="password" name="password" id="password" required>
  </p>
  <p style="color:black;">
    Server IP: <?php echo $_SERVER['SERVER_ADDR']; ?>
  </p>
  </div>
  <p class="p-container">
    <span id='error' name="error"><?php echo $response; ?></span>
		<span id="register"><!--<a style="color: black;" href="marketforce/signup.php">Register Here</a>--></span><br><br>
		<span id="forgot"><a style="color:black;" href="marketforce/forgot_password.php" target="_blank">Forgot Password?</a></span>
    <input type="submit" name="go" id="go" value="Log in">
  </p>
</form>
</body>
<script>
var org_choice = localStorage.getItem('org_choice');
document.getElementById('org').value = org_choice;
</script>
<style type="text/css">
* { box-sizing: border-box; padding:0; margin: 0; }

	.spacer{
		height:25%;
		text-align: center;
	}
	.spacer img{
		width: 80%;
	}
body {
	font-family: "HelveticaNeue-Light","Helvetica Neue Light","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
  color:white;
  font-size:12px;
  //background:#333 url(/images/classy_fabric.png);
	background-image: url('https://burtonsolution.tech/allsteel/marketforce/img/stock-bg.jpg');
	background-size: 100% 100%;
	background-size: cover;
}

form {
 	background:#fff; 
  width:300px;
  margin:30px auto;
  border-radius:0.4em;
  border:5px solid #191919;
  overflow:hidden;
  position:relative;
  box-shadow: 0 5px 10px 5px rgba(0,0,0,0.2);
	opacity: 0.8;
}

form:after {
  content:"";
  display:block;
  position:absolute;
  height:1px;
  width:100px;
  left:20%;
  background:linear-gradient(left, #111, #444, #b6b6b8, #444, #111);
  top:0;
}

form:before {
 	content:"";
  display:block;
  position:absolute;
  width:8px;
  height:5px;
  border-radius:50%;
  left:34%;
  top:-7px;
  box-shadow: 0 0 6px 4px #fff;
}

.inset {
 	padding:20px; 
  //border-top:1px solid #19191a;
}

form h1 {
  font-size:18px;
  text-shadow:0 1px 0 black;
  text-align:center;
  padding:15px 0;
 // border-bottom:1px solid rgba(0,0,0,1);
  position:relative;
}

form h1:after {
 	content:"";
  display:block;
  width:250px;
  height:100px;
  position:absolute;
  top:0;
  left:50px;
  pointer-events:none;
  transform:rotate(70deg);
  background:linear-gradient(50deg, rgba(255,255,255,0.15), rgba(0,0,0,0));
  
}

label {
 	color:#666;
  display:block;
  padding-bottom:9px;
}

input[type=text],
input[type=password] {
 	width:100%;
  padding:8px 5px;
  background:linear-gradient(#1f2124, #27292c);
  border:1px solid #222;
  box-shadow:
    0 1px 0 rgba(255,255,255,0.1);
  border-radius:0.3em;
  margin-bottom:20px;
  color: white;
}

label[for=remember]{
 	color:white;
  display:inline-block;
  padding-bottom:0;
  padding-top:5px;
}

input[type=checkbox] {
 	display:inline-block;
  vertical-align:top;
}

.p-container {
 	padding:0 20px 20px 20px; 
}

.p-container:after {
 	clear:both;
  display:table;
  content:"";
}

.p-container span {
  display:block;
  float:left;
  color:red;
  padding-top:8px;
}

input[type=submit] {
 	padding:5px 20px;
  border:1px solid rgba(0,0,0,0.4);
  text-shadow:0 -1px 0 rgba(0,0,0,0.4);
  box-shadow:
    inset 0 1px 0 rgba(255,255,255,0.3),
    inset 0 10px 10px rgba(255,255,255,0.1);
  border-radius:0.3em;
  background:#0184ff;
  color:white;
  float:right;
  font-weight:bold;
  cursor:pointer;
  font-size:13px;
}

input[type=submit]:hover {
  box-shadow:
    inset 0 1px 0 rgba(255,255,255,0.3),
    inset 0 -10px 10px rgba(255,255,255,0.1);
}

input[type=text]:hover,
input[type=password]:hover,
label:hover ~ input[type=text],
label:hover ~ input[type=password] {
</style>
</html>