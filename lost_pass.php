<?php
//This is a test comment...
error_reporting(E_ALL);
if ($_SERVER['HTTP_HOST'] == 'beta-myorders.marketforceapp.com' || $_SERVER['HTTP_HOST'] == 'orders.marketforceapp.com') {
	echo '<script>
        window.location = "oportal/";
        </script>';
}
include 'marketforce/security/session/session-settings.php';
//session_start();
include 'php/connection.php';
error_reporting(E_ALL);

if (isset($_GET['logout'])) {
	session_destroy();
} elseif ($_SESSION['in'] == 'Yes') {
	echo '<script>
				  window.location="marketforce/index.php";
				</script>';
}

$response = $_GET['r'];

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Login | MarketForce</title>
	<?php include 'marketforce/global/sections/head.php'; ?>
</head>

<body onload="cred_check();">
	<!-- Preloader -->
	<?php include 'global/sections/preloader.php'; ?>
	<!-- /Preloader -->
	<div class="wrapper theme-4-active pimary-color-red">

		<!--Login-->

		<div class="wrapper pa-0">
			<div class="form-group mb-0 pull-right">
				<span class="inline-block pr-10">Having Issues?</span>
				<a class="inline-block btn btn-info btn-rounded btn-outline" href="mailto:support@marketforceapp.com?subject=Customer%20Portal%20Issue%20Report&body=I%20am%20having%20an%20issue%20with%20the%20following:">Contact Support</a>
			</div>
			<div class="clearfix"></div>
			</header>

			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page" style="height:100%;">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height" style="height: 322px;">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Forgot Password
												<img src="../assets/img/white-mf-logo.png">
											</h3>
										</div>
										<div class="form-wrap">
											<form action="php/validate_email.php" method="post">
												<div class="form-group">
													<label class="control-label mb-10" for="org">ORGANIZATION</label>
													<select class="form-control" name="org" id="org" style="width:100%;" required>
														<option value="">Select Your Organization</option>
														<?php
														if ($_REQUEST['open'] == 'y') {
															$oq = "SELECT * FROM `organizations` WHERE `inactive` != 'Yes' ORDER BY `org_name` DESC";
														} else {
															$oq = "SELECT * FROM `organizations` WHERE `inactive` != 'Yes' AND `super_group` = 'ASC' ORDER BY `org_name` DESC";
														}

														$og = mysqli_query($mf_conn, $oq) or die($conn->error);
														while ($or = mysqli_fetch_array($og)) {
															echo '<option value="' . $or['org_id'] . '">' . $or['org_name'] . '</option>';
														}
														?>
													</select>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="email">Email &nbsp;&nbsp; <span id="error_message" style="color:red;"><?php echo $_GET['r']; ?></span></label>
													<input type="email" class="form-control" required="" id="email" name="email" placeholder="Enter your email">
												</div>
												
												<p style="color:white;">
													Server IP: <?php echo $_SERVER['SERVER_ADDR']; ?>
												</p>
												<div class="form-group text-center">
													<button type="submit" class="btn btn-info btn-rounded">send</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->
				</div>

			</div>
			<!-- /Login -->




		</div>


		<!-- Footer -->
		<?php include 'global/sections/footer.php'; ?>
		<!-- /Footer -->

	</div>
	<!-- /Main Content -->

	</div>
	<!-- /#wrapper -->

	<!--Footer-->
	<?php include 'global/sections/includes.php'; ?>
	<!--<script src="login/js/credentials.js"></script>-->
	<script>
		var org_choice = localStorage.getItem('org_choice');
		document.getElementById('org').value = org_choice;
	</script>
</body>

</html>