<?php
//This is a test comment...
include 'php/connection.php';

$switch = 'OFF';
//$switch = 'ON';

if($switch == 'OFF'){
  echo 'The Master Switch has been turned OFF for this page to prevent accidental loss of data!';
  break;
}
if(!$_GET['id']){
  echo '<script>
        var idx = prompt("Please Enter The Dealers ID");
        window.location = "delete.php?id="+idx;
        </script>
        ';
}else{
//Load Variables
$id = $_GET['id'];
echo 'Dealer ID: ' . $_GET['id'] . '<br>';
//Run Delete queries

//Get Dealer Info
$nq = "SELECT * FROM `dealers` WHERE `ID` = '" . $id . "'";
$ng = mysqli_query($conn, $nq) or die($conn->error);
if(mysqli_num_rows($ng) != 0){
$nr = mysqli_fetch_array($ng);
  $name = $nr['name'];
}else{
  $name = 'Unknown Dealer';
}

//Main dealer list
$q = "DELETE FROM `dealers` WHERE `ID` = '" . $id . "'";
mysqli_query($conn, $q) or die($conn->error);
echo $name . ' was deleted from the Main Dealer list!<br>';
  
//New dealer list
$q = "DELETE FROM `new_dealer_form` WHERE `ID` = '" . $id . "'";
mysqli_query($conn, $q) or die($conn->error);
echo $name . ' was deleted from the New Dealer list!<br>';

//Dealer Closing list
$q = "DELETE FROM `dealer_closing` WHERE `ID` = '" . $id . "'";
mysqli_query($conn, $q) or die($conn->error);
echo $name . ' was deleted from the Dealer Closing list!<br>';

//Display Addon list
$q = "DELETE FROM `display_addons` WHERE `ID` = '" . $id . "'";
mysqli_query($conn, $q) or die($conn->error);
echo $name . ' was deleted from the Display Addon list!<br>';

//Display Orders list
$q = "DELETE FROM `display_orders` WHERE `ID` = '" . $id . "'";
mysqli_query($conn, $q) or die($conn->error);
echo $name . ' was deleted from the Display Orders list!<br>';

//Display Relocation list
$q = "DELETE FROM `display_relocation` WHERE `ID` = '" . $id . "'";
mysqli_query($conn, $q) or die($conn->error);
echo $name . ' was deleted from the Display Relocation list!<br>';

//Display Removal list
$q = "DELETE FROM `display_removal` WHERE `ID` = '" . $id . "'";
mysqli_query($conn, $q) or die($conn->error);
echo $name . ' was deleted from the Display Removal list!<br>';

//New dealer list
$q = "DELETE FROM `new_dealer_form` WHERE `ID` = '" . $id . "'";
mysqli_query($conn, $q) or die($conn->error);
echo $name . ' was deleted from the New Dealer list!<br>';

//Signatures list
$q = "DELETE FROM `signatures` WHERE `dealer_id` = '" . $id . "'";
mysqli_query($conn, $q) or die($conn->error);
echo $name . ' was deleted from the Signatures list!<br>';

//Visit Notes list
/*$q = "DELETE FROM `visit_notes` WHERE `dealer_id` = '" . $id . "'";
mysqli_query($conn, $q) or die($conn->error);
echo $name . ' was deleted from the Visit Notes list!<br>';*/

echo 'This is the end of the removal script!';

echo '<br><a href="delete.php"><button type="button">Delete Another Dealer</button></a>';
}

?>