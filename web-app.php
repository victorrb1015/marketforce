<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>MarketForce | Your Market Solution</title>
    <meta name="viewport" content="width=device-width height=device-height initial-scale=1.0">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="landing-assets/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CMuli:100,300,400,600,800">
    <link rel="stylesheet" href="landing-assets/css/bootstrap.css">
    <link rel="stylesheet" href="landing-assets/css/fonts.css">
    <link rel="stylesheet" href="landing-assets/css/style.css">
    <style>
      .partner-img {
        filter: grayscale(100%); 
        -webkit-filter: grayscale(100%); 
        filter: gray; 
        -webkit-transition: all .6s ease;
      }
      .partner-img:hover {
        filter: grayscale(0%); 
        -webkit-filter: grayscale(0%); 
        filter: none;
      }
    </style>
  </head>
  <body>
    <style>.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}</style>
    <div class="ie-panel"><a href="//windows.microsoft.com/en-US/internet-explorer/"><img src="landing-assets/images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div class="preloader" id="loading">
      <div class="preloader-body">
        <div id="loading-center-object">
          <div class="object" id="object_four"></div>
          <div class="object" id="object_three"></div>
          <div class="object" id="object_two"></div>
          <div class="object" id="object_one"></div>
        </div>
      </div>
    </div>
    <div class="page">
      <!-- Page Header-->
      <header class="section page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-classic" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="0px" data-xl-stick-up-offset="0px" data-xxl-stick-up-offset="0px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-main">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                <!-- RD Navbar Brand-->
                <div class="rd-navbar-brand"><a class="brand" href="index.html"><img class="brand-logo-dark" src="assets/img/full-mf-logo.png" alt="" width="113" height="38" srcset="assets/img/full-mf-logo.png 2x"/></a></div>
              </div>
              <div class="rd-navbar-nav-wrap">
                <p class="rd-navbar-slogan">Simple, intuitive and powerful</p>
                <!-- RD Navbar Nav-->
                <ul class="rd-navbar-nav">
                  <!--<li class="rd-nav-item active"><a class="rd-nav-link" href="index.html">Home</a>
                    <ul class="rd-menu rd-navbar-dropdown">
                      <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="mobile-app.html">Mobile App</a></li>
                      <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="desktop-app.html">Desktop App</a></li>
                      <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="messenger.html">Messenger</a></li>
                      <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="web-app.html">Web App</a></li>
                    </ul>
                  </li>-->
                  <li class="rd-nav-item"><a class="rd-nav-link" href="#">Features</a>
                    <!-- RD Navbar Megamenu-->
                    <div class="rd-menu rd-navbar-megamenu">
                      <ul class="rd-navbar-megamenu-inner">
                        <li class="rd-megamenu-item">
                          <ul class="rd-megamenu-list">
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="typography.html">Typography</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="buttons.html">Buttons</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="forms.html">Forms</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="tabs-and-accordions.html">Tabs and Accordions</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="progress-bars.html">Progress bars</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="tables.html">Tables</a></li>
                          </ul>
                        </li>
                        <li class="rd-megamenu-item">
                          <ul class="rd-megamenu-list">
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="about-us.html">About Us</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="careers.html">Careers</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="faq.html">FAQ</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="testimonials.html">Testimonials</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="features.html">Features</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="features-2.html">Features 2</a></li>
                          </ul>
                        </li>
                        <li class="rd-megamenu-item">
                          <ul class="rd-megamenu-list">
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="grid-system.html">Grid system</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="privacy-policy.html">Privacy policy</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="404-page.html">404 Page</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="coming-soon.html">Coming Soon</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="search-results.html">Search results</a></li>
                          </ul>
                        </li>
                        <li class="rd-megamenu-item">
                          <ul class="rd-megamenu-list">
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="our-team.html">Our Team</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="our-team-2.html">Our Team 2</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="contact-us.html">Contact Us</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="login.html">Login</a></li>
                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="register.html">Register</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li class="rd-nav-item"><a class="rd-nav-link" href="pricing.html">Pricing</a>
                  </li>
                </ul>
                <!-- RD Navbar Search--><a class="rd-navbar-link rd-navbar-link-1 button-winona" href="login.php" target="_blank">Get MarketForce</a><a class="rd-navbar-link rd-navbar-link-2 button-winona" href="contact-us.html">Contact Us</a>
                <!--<div class="rd-navbar-element rd-navbar-element_centered">
                  <div class="group-xs"><a class="icon icon-sm link-social-2 mdi mdi-facebook" href="#"></a><a class="icon icon-sm link-social-2 mdi mdi-twitter" href="#"></a><a class="icon icon-sm link-social-2 mdi mdi-instagram" href="#"></a></div>
                </div>-->
              </div>
              <!--<div class="rd-navbar-element rd-navbar-element_right">
                <ul class="list-localization">
                  <li>
                    <label>
                      <input name="localization" value="dt" type="radio"/><span class="label-text">DT</span>
                    </label>
                  </li>
                  <li>
                    <label>
                      <input name="localization" value="en" type="radio" checked="checked"/><span class="label-text">EN</span>
                    </label>
                  </li>
                  <li>
                    <label>
                      <input name="localization" value="fr" type="radio"/><span class="label-text">FR</span>
                    </label>
                  </li>
                </ul>
              </div>-->
              <div class="rd-navbar-dummy"></div>
            </div>
          </nav>
        </div>
      </header>
      <!-- Subscribe to Our Newsletter-->
      <section class="section section-decoration-2">
        <div class="range justify-content-xl-between">
          <div class="cell-md-6 cell-xl-5 z-2">
            <div class="block-6">
              <h2 class="heading-decoration-1 wow clipInLeft"><span class="font-weight-extra-black">more possibilities </span><br><span class="font-weight-thin">to grow your business</span></h2>
              <h4 class="text-default wow clipInLeft" data-wow-delay=".1s">Meet MarketForce. The simple, intuitive and powerful app to manage your work and business activity. Explore app of the next generation for free.</h4>
              <form class="rd-form rd-mailform form-lg form-inline-1 wow clipInLeft" data-form-output="form-output-t1" data-form-type="subscribe" method="post" action="bat/rd-mailform.php" data-wow-delay=".1s">
                <div class="form-wrap">
                  <input class="form-input" id="subscribe-form-2-email" type="email" name="email" data-constraints="@Email @Required">
                  <label class="form-label" for="subscribe-form-2-email">Enter your e-mail...</label>
                </div>
                <button class="button form-button button-primary" type="submit" aria-label="Subscribe"><span class="icon form-button-icon-default mdi mdi-arrow-right"></span><span class="icon form-button-icon-success mdi mdi-check"></span><span class="icon form-button-icon-error mdi mdi-close"></span></button>
                <div class="form-output" id="form-output-1"></div>
              </form>
            </div>
          </div>
          <div class="cell-md-6 cell-xl-6">
            <article class="safari-frame-1"> 
              <div class="safari-frame-1-bg wow fadeIn" data-wow-duration=".3s"> </div>
              <div class="safari-frame-1-outer wow fadeInUpSmall" data-wow-delay=".5s" data-wow-duration=".7s"><img class="safari-frame-1-header" src="landing-assets/images/safari-frame-header-891x44.png" alt="" width="891" height="44"/>
                <div class="safari-frame-1-inner" style="background-image: url(landing-assets/images/thumbs/order-portal.jpg);background-position: bottom;"></div>
              </div>
            </article>
          </div>
        </div>
        <div class="section-decoration-2-element wow fadeIn"></div>
      </section>
      <!-- About App-->
      <section class="section section-lg bg-gray-100">
        <div class="container">
          <div class="row row-50 justify-content-center justify-content-lg-start flex-lg-row-reverse align-items-center">
            <div class="col-sm-10 col-lg-6 wow fadeInRightSmall">
              <div class="block-5">
                <div class="badge">About App</div>
                <h3>The Only App You’ll Need to Manage Your Business</h3>
                <p class="big text-gray-900">MarketForce App introduces a new way of managing your work and getting better results for your business.</p>
                <!-- Quote Light-->
                <blockquote class="quote-light quote-light_borderless">
                  <div class="quote-light-mark linearicons-quote-open"></div>
                  <div class="quote-light-text">
                    <p>
                      We designed MarketForce as a unique tool, which can be used to help our users improve the efficiency and performance of their business from one platform. 
                      No matter where you are or how many locations your business has!
                    </p>
                  </div>
                </blockquote>
              </div>
            </div>
            <div class="col-sm-10 col-lg-6 wow fadeInLeftSmall">
              <div class="thumbnail-classy">
                <img src="landing-assets/images/thumbs/sm-mf.jpg" style="width:100%;" />
                <!--<div class="thumbnail-classy-caption"><a class="thumbnail-classy-button mdi mdi-play" href="https://www.youtube.com/watch?v=I5FlP07kdvM" data-lightgallery="item">
                    <svg class="thumbnail-classy-button-shape" width="86" height="86" viewbox="0 0 88 88" xmlns="//www.w3.org/2000/svg" shape-rendering="crispEdges">
                      <rect x="1" y="1" width="86" height="86" rx="6" ry="6"></rect>
                    </svg></a></div>-->
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Meet MarketForce-->
      <section class="section section-md text-center">
        <div class="container container-fluid">
          <div class="badge wow fadeIn">Meet MarketForce </div>
          <h3 class="wow fadeIn" data-wow-delay=".1s">App of The Next Generation</h3>
        </div>
        <div class="container-fullwidth">
          <!-- Owl Carousel-->
          <div class="owl-carousel owl-carousel-box-modern wow fadeIn" data-items="1" data-sm-items="2" data-lg-items="3" data-xl-items="3" data-xxl-items="4" data-dots="true" data-nav="true" data-stage-padding="15" data-xl-stage-padding="0" data-loop="false" data-margin="15" data-xl-margin="0" data-mouse-drag="false" data-wow-delay=".2s">
            <!-- Box Modern-->
            <article class="box-modern box-modern_alternate" data-anime="circles-2">
              <div class="box-modern-media">
                <div class="box-modern-icon mdi mdi-flash"></div>
                <div class="box-modern-circle box-modern-circle-1"></div>
                <div class="box-modern-circle box-modern-circle-2"></div>
              </div>
              <p class="box-modern-title">High Performance</p>
              <div class="box-modern-text">
                <p>Our app has better performance than any other business software</p>
              </div>
            </article>
            <!-- Box Modern-->
            <article class="box-modern" data-anime="circles-2">
              <div class="box-modern-media">
                <div class="box-modern-icon mdi mdi-lightbulb"></div>
                <div class="box-modern-circle box-modern-circle-1"></div>
                <div class="box-modern-circle box-modern-circle-2"></div>
              </div>
              <p class="box-modern-title">Innovations</p>
              <div class="box-modern-text">
                <p>MarketForce is a fully innovative business app built to increase your benefits.</p>
              </div>
            </article>
            <!-- Box Modern-->
            <article class="box-modern box-modern_alternate-1" data-anime="circles-2">
              <div class="box-modern-media"> 
                <div class="box-modern-icon mdi mdi-hangouts"></div>
                <div class="box-modern-circle box-modern-circle-1"></div>
                <div class="box-modern-circle box-modern-circle-2"></div>
              </div>
              <p class="box-modern-title">24\7 Support</p>
              <div class="box-modern-text">
                <p>We provide free 24/7 premium support  to all our registered clients.</p>
              </div>
            </article>
            <!-- Box Modern-->
            <article class="box-modern" data-anime="circles-2">
              <div class="box-modern-media">
                <div class="box-modern-icon mdi mdi-dice"></div>
                <div class="box-modern-circle box-modern-circle-1"></div>
                <div class="box-modern-circle box-modern-circle-2"></div>
              </div>
              <p class="box-modern-title">Fully Customizable</p>
              <div class="box-modern-text">
                <p>We offer outstanding customization as a part of the MarketForce Enterprise app.</p>
              </div>
            </article>
            <!-- Box Modern-->
            <article class="box-modern" data-anime="circles-1">
              <div class="box-modern-media">
                <div class="box-modern-icon mdi mdi-airballoon"></div>
                <div class="box-modern-circle box-modern-circle-1"></div>
                <div class="box-modern-circle box-modern-circle-2"></div>
              </div>
              <p class="box-modern-title">Cloud-Based</p>
              <div class="box-modern-text">
                <p>We store the most important data in our secure cloud storage.</p>
              </div>
            </article>
          </div>
        </div>
      </section>
      <!-- About our App-->
      <section class="section section-md bg-gray-100">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-10 col-lg-12">
              <div class="tabs-custom tabs-features wow fadeIn" id="tabs-1">
                <div class="tabs-features-aside">
                  <div class="badge">About our App</div>
                  <h3><span>Best Business</span><br><span style="color:#FF0000;">Application</span></h3>
                  <ul class="nav nav-tabs">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#tabs-1-1" data-toggle="tab">
                        <p>A wide variety of functionality</p></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-1-2" data-toggle="tab">
                        <p>Free ongoing improvement updates</p></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-1-3" data-toggle="tab">
                        <p>Full support for registered users</p></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-1-4" data-toggle="tab">
                        <p>Data stored securely in the cloud</p></a></li>
                  </ul>
                </div>
                <div class="tab-content" style="/*background: url(landing-assets/images/thumbs/mf-inv-man.jpg) no-repeat;background-size: contain;background-position: top;*/">
                  <div class="tab-pane fade show active" id="tabs-1-1" style="background: url(landing-assets/images/thumbs/mf-inv-man.jpg) no-repeat;background-size: contain;background-position: top;"></div>
                  <div class="tab-pane fade" id="tabs-1-2" style="background: url(landing-assets/images/thumbs/updates.jpg) no-repeat;background-size: contain;background-position: top;"></div>
                  <div class="tab-pane fade" id="tabs-1-3" style="background: url(landing-assets/images/thumbs/tech-support.jpg) no-repeat;background-size: contain;background-position: top;"></div>
                  <div class="tab-pane fade" id="tabs-1-4" style="background: url(landing-assets/images/thumbs/servers.jpg) no-repeat;background-size: contain;background-position: top;"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="section z-2">
        <!-- Simple and Clear steps-->
        <div class="parallax-container section-lg section-bottom-0 oh text-center" data-parallax-img="images/bg-image-1.jpg">
          <div class="parallax-content">
            <div class="container context-dark">
              <div class="badge wow fadeIn" style="color:#FF0000;background:#000000;">Simple and Clear steps</div>
              <h3 class="wow fadeIn" data-wow-delay=".1s" style="color:black;">Configuring MarketForce  </h3>
            </div>
            <div class="slick-slider-2-main wow fadeIn" data-wow-delay=".2s">
              <div class="container">
                <div class="slick-slider-2-main-inner">
                  <!-- Slick Carousel-->
                  <div class="slick-slider carousel-parent" id="parent-carousel-2" data-arrows="true" data-loop="true" data-dots="false" data-swipe="true" data-items="1" data-child="#child-carousel-2" data-for="#child-carousel-2">
                    <div class="item">
                      <!-- Post Lina-->
                      <article class="post-lina">
                        <div class="icon post-lina-icon linearicons-new-tab"></div>
                        <p class="post-lina-title">Register</p>
                        <div class="post-lina-text">
                          <p>First, you need to register a user account on our website before configuring and using it on a regular basis.</p>
                        </div><a class="button button-default-outline button-winona" href="register.html">Get Started</a>
                      </article>
                    </div>
                    <div class="item">
                      <!-- Post Lina-->
                      <article class="post-lina">
                        <div class="icon post-lina-icon linearicons-new-tab"></div>
                        <p class="post-lina-title">Configure</p>
                        <div class="post-lina-text">
                          <p>After registering, proceed to Preferences and set up the interface language and enter your company data.</p>
                        </div><a class="button button-default-outline button-winona" href="register.html">Get Started</a>
                      </article>
                    </div>
                    <div class="item">
                      <!-- Post Lina-->
                      <article class="post-lina">
                        <div class="icon post-lina-icon linearicons-new-tab"></div>
                        <p class="post-lina-title">Integrate</p>
                        <div class="post-lina-text">
                          <p>Get instant access to the app and integrate it with the software used by your company.</p>
                        </div><a class="button button-default-outline button-winona" href="register.html">Get Started</a>
                      </article>
                    </div>
                    <div class="item">
                      <!-- Post Lina-->
                      <article class="post-lina">
                        <div class="icon post-lina-icon linearicons-new-tab"></div>
                        <p class="post-lina-title">Enjoy</p>
                        <div class="post-lina-text">
                          <p>Finally, you can enjoy the app’s performance while managing all projects and activities.</p>
                        </div><a class="button button-default-outline button-winona" href="register.html">Get Started</a>
                      </article>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="slick-slider-2-aside wow fadeIn" data-wow-delay=".2s">
          <div class="container">
            <div class="slick-slider-2-aside-inner">
              <div class="slick-slider carousel-child" id="child-carousel-2" data-for="#parent-carousel-2" data-arrows="false" data-loop="false" data-dots="false" data-swipe="true" data-items="1" data-sm-items="3" data-md-items="3" data-lg-items="4" data-xl-items="4" data-slide-to-scroll="1">
                <div class="item">
                  <p>01. Register</p>
                </div>
                <div class="item">
                  <p>02. Configure</p>
                </div>
                <div class="item">
                  <p>03. Integrate</p>
                </div>
                <div class="item">
                  <p>04. Enjoy</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- What we Offer-->
      <section class="section section-lg bg-white">
        <div class="container">
          <div class="row row-50 justify-content-center justify-content-xl-between align-items-center flex-md-row-reverse">
            <div class="col-sm-10 col-lg-5 wow fadeInRightSmall">
              <div class="badge">What we Offer </div>
              <h3>We Have a Lot of Useful Solutions for You</h3>
              <p class="big">MarketForce unites everything entrepreneurs have asked for since globalization of information technologies: including:</p>
              <ul class="list-marked list-marked_secondary">
                <li>Service route scheduling</li>
                <li>Automated debt collections system</li>
                <li>Management your customers</li>
              </ul><a class="button button-default-outline button-winona" href="login.html">Start Using for Free</a>
            </div>
            <div class="col-sm-10 col-lg-7 wow fadeInLeftSmall">
              <div class="safari-frame-2-left">
                <article class="safari-frame-2"><img class="safari-frame-2-header" src="landing-assets/images/safari-frame-2-610x40.png" alt="" width="610" height="40"/>
                  <div class="safari-frame-2-inner" style="background-image: url(landing-assets/images/thumbs/collections-report.jpg);background-size:contain;background-positon:top;background-repeat:no-repeat;"></div>
                  <div class="tooltip-point" data-toggle="tooltip" data-placement="top" data-class="tooltip-light" title="Track debt by category" style="position: absolute; top: 40%; left: 34%;"> </div>
                </article>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- More Features-->
      <!--<section class="section section-lg bg-white">
        <div class="container">
          <div class="row row-50 justify-content-center justify-content-xl-between align-items-center">
            <div class="col-sm-10 col-lg-5 wow fadeInLeftSmall">
              <div class="badge">More Features</div>
              <h3>Statistics Tracking</h3>
              <p class="big">Our Statistics Module integrated into MarketForce App is a powerful tool that gathers all important data on your company’s commercial success on one screen, including:</p>
              <ul class="list-marked list-marked_secondary">
                <li>Daily/Weekly sales summary  </li>
                <li>Amount of converted leads</li>
                <li>Number of referrals and opened newsletters</li>
              </ul><a class="button button-default-outline button-winona" href="login.html">Start Using for Free</a>
            </div>
            <div class="col-sm-10 col-lg-7 wow fadeInRightSmall">
              <div class="safari-frame-2-right">
                <article class="safari-frame-2"><img class="safari-frame-2-header" src="landing-assets/images/safari-frame-2-610x40.png" alt="" width="610" height="40"/>
                  <div class="safari-frame-2-inner" style="background-image: url(images/safari-frame-2-inner-2-610x392.jpg);"></div>
                </article>
              </div>
            </div>
          </div>
        </div>
      </section>-->
      <!-- Pricing Plans-->
      <section class="section parallax-container section-lg bg-accent text-center" data-parallax-img="images/bg-image-1.jpg">
        <div class="parallax-content">
          <div class="container">
            <div class="badge">Pricing Plans</div>
            <h3>Choose Your Plan </h3>
            <p class="big offset-top-5"><span style="max-width: 740px;">We offer four different plans that cover the needs of modern startups and businesses. They are provided via monthly or annual payments for your convenience.</span></p>
            <!-- Pricing Table Modern-->
            <div class="pt-modern wow fadeIn" id="tabs-pt">
              <!-- Nav tabs-->
              <div class="nav-tabs-outer">
                <ul class="nav nav-tabs">
                  <li class="nav-item"><a class="nav-link active" href="#tabs-pt-1" data-toggle="tab"><span>Startup</span><span class="nav-item-indicator" role="presentation"></span></a>
                    <ul class="list pt-modern-features">
                      <li><span class="icon"></span><span>Comprehensive Dashboard</span></li>
                      <li><span class="icon"></span><span>Customer Manager</span></li>
                      <li><span class="icon"></span><span>Built-in Customer Communications</span></li>
                      <li><span class="icon"></span><span>Employee HR Center</span></li>
                      <li><span class="icon"></span><span>Reports</span></li>
                      <li><span class="icon"></span><span>Up to 5 Users</span></li>
                    </ul>
                  </li>
                  <li class="nav-item"><a class="nav-link" href="#tabs-pt-2" data-toggle="tab"><span>Installer</span><span class="nav-item-indicator" role="presentation"></span><span class="badge badge-secondary">Popular</span></a>
                    <ul class="list pt-modern-features">
                      <li><span class="icon"></span><span>Startup Plan Features</span></li>
                      <li><span class="icon"></span><span>Order Tracking/Invoicing</span></li>
                      <li><span class="icon"></span><span>Permit/license expiration tracking</span></li>
                      <li><span class="icon"></span><span>Route Scheduling</span></li>
                      <li><span class="icon"></span><span>Accounting/Expense Reports</span></li>
                      <li><span class="icon"></span><span>Up to 15 Users</span></li>
                    </ul>
                  </li>
                  <li class="nav-item"><a class="nav-link" href="#tabs-pt-3" data-toggle="tab"><span>Comprehensive</span><span class="nav-item-indicator" role="presentation"></span></a>
                    <ul class="list pt-modern-features">
                      <li><span class="icon"></span><span>Installer Plan Features</span></li>
                      <li><span class="icon"></span><span>Automated Debt Collections System</span></li>
                      <li><span class="icon"></span><span>Reposession System</span></li>
                      <li><span class="icon"></span><span>Repairs System</span></li>
                      <li><span class="icon"></span><span>24/7 Support</span></li>
                      <li><span class="icon"></span><span>Up to 35 Users</span></li>
                    </ul>
                  </li>
                  <li class="nav-item"><a class="nav-link" href="#tabs-pt-4" data-toggle="tab"><span>Enterprise</span><span class="nav-item-indicator" role="presentation"></span></a>
                    <ul class="list pt-modern-features">
                      <li><span class="icon"></span><span>All Plan Features</span></li>
                      <li><span class="icon"></span><span>Custom Development</span></li>
                      <li><span class="icon"></span><span>Dedicated Account Specialist</span></li>
                      <li><span class="icon"></span><span>24/7 Support</span></li>
                      <li><span class="icon"></span><span>Product Training Seminars</span></li>
                      <li><span class="icon"></span><span>Unlimited Users</span></li>
                    </ul>
                  </li>
                </ul>
              </div>
              <!-- Tab panes-->
              <div class="tab-content">
                <div class="tab-pane fade show active" id="tabs-pt-1">
                  <div class="pt-modern-price-outer bg-gray-100">
                    <p class="pt-modern-price"><span class="pt-modern-price-currency">$</span><span class="pt-modern-price-value">40</span><span class="pt-modern-price-small">00</span></p>
                    <p class="pt-modern-price-caption">per month</p>
                  </div>
                  <div class="pt-modern-price-outer">
                    <p class="pt-modern-price"><span class="pt-modern-price-currency">$</span><span class="pt-modern-price-value">480</span><span class="pt-modern-price-small">00</span></p>
                    <p class="pt-modern-price-caption">per year</p>
                  </div>
                  <div class="pt-modern-element"><a class="link link-modern" href="#"><span class="icon mdi mdi-arrow-right"></span><span>Get Started</span></a></div>
                </div>
                <div class="tab-pane fade" id="tabs-pt-2">
                  <div class="pt-modern-price-outer bg-gray-100">
                    <p class="pt-modern-price"><span class="pt-modern-price-currency">$</span><span class="pt-modern-price-value">80</span><span class="pt-modern-price-small">00</span></p>
                    <p class="pt-modern-price-caption">per month</p>
                  </div>
                  <div class="pt-modern-price-outer">
                    <p class="pt-modern-price"><span class="pt-modern-price-currency">$</span><span class="pt-modern-price-value">960</span><span class="pt-modern-price-small">00</span></p>
                    <p class="pt-modern-price-caption">per year</p>
                  </div>
                  <div class="pt-modern-element"><a class="link link-modern" href="#"><span class="icon mdi mdi-arrow-right"></span><span>Get Started</span></a></div>
                </div>
                <div class="tab-pane fade" id="tabs-pt-3">
                  <div class="pt-modern-price-outer bg-gray-100">
                    <p class="pt-modern-price"><span class="pt-modern-price-currency">$</span><span class="pt-modern-price-value">120</span><span class="pt-modern-price-small">00</span></p>
                    <p class="pt-modern-price-caption">per month</p>
                  </div>
                  <div class="pt-modern-price-outer">
                    <p class="pt-modern-price"><span class="pt-modern-price-currency">$</span><span class="pt-modern-price-value">1,440</span><span class="pt-modern-price-small">00</span></p>
                    <p class="pt-modern-price-caption">per year</p>
                  </div>
                  <div class="pt-modern-element"><a class="link link-modern" href="#"><span class="icon mdi mdi-arrow-right"></span><span>Get Started</span></a></div>
                </div>
                <div class="tab-pane fade" id="tabs-pt-4">
                  <div class="pt-modern-price-outer bg-gray-100">
                    <p class="pt-modern-price"><span class="pt-modern-price-currency"></span><span class="pt-modern-price-value">Call</span><span class="pt-modern-price-small"></span></p>
                    <p class="pt-modern-price-caption">Us</p>
                  </div>
                  <div class="pt-modern-price-outer">
                    <p class="pt-modern-price"><span class="pt-modern-price-currency"></span><span class="pt-modern-price-value">For</span><span class="pt-modern-price-small"></span></p>
                    <p class="pt-modern-price-caption">Pricing</p>
                  </div>
                  <div class="pt-modern-element"><a class="link link-modern" href="#"><span class="icon mdi mdi-arrow-right"></span><span>Get Started</span></a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Popular Questions-->
      <!--<section class="section section-md bg-gray-100 text-center">
        <div class="container">
          <div class="badge">Popular Questions</div>
          <h3>Frequently Asked Questions</h3>
          <div class="owl-carousel owl-carousel_type-3" data-items="1" data-md-items="2" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="0" data-loop="true" data-margin="30" data-autoplay="true" data-autoplay-timeout="4000" data-mouse-drag="false">
            <article class="post-info">
              <div class="post-info-header"><span class="icon post-info-icon mdi mdi-information"></span>
                <h4 class="post-info-title">Do you provide scripts?</h4>
              </div>
              <div class="post-info-main">
                <p>Our templates do not include any additional scripts. Newsletter subscriptions, search fields, forums, image galleries are inactive.</p>
              </div>
            </article>
            <article class="post-info">
              <div class="post-info-header"><span class="icon post-info-icon mdi mdi-information text-secondary"></span>
                <h4 class="post-info-title">Why should I purchase a template?</h4>
              </div>
              <div class="post-info-main">
                <p>The major advantage is price: You get a high quality design for just $20-$70. You don’t have to hire a web designer or web design studio to make a web site for you.</p>
              </div>
            </article>
            <article class="post-info">
              <div class="post-info-header"><span class="icon post-info-icon mdi mdi-information text-color-6"></span>
                <h4 class="post-info-title">What are the formats of your templates?</h4>
              </div>
              <div class="post-info-main">
                <p>Website templates are available in Photoshop and HTML formats. Fonts are included with the Photoshop file. HTML is compatible with modern software.</p>
              </div>
            </article>
            <article class="post-info">
              <div class="post-info-header"><span class="icon post-info-icon mdi mdi-information"></span>
                <h4 class="post-info-title">In what formats are your templates available?</h4>
              </div>
              <div class="post-info-main">
                <p>Website templates are available in Photoshop and HTML formats. Fonts are included with the Photoshop file.</p>
              </div>
            </article>
            <article class="post-info">
              <div class="post-info-header"><span class="icon post-info-icon mdi mdi-information text-secondary"></span>
                <h4 class="post-info-title">How can I pay for my order?</h4>
              </div>
              <div class="post-info-main">
                <p>We accept Visa, MasterCard, and American Express credit and debit cards for your convenience.</p>
              </div>
            </article>
            <article class="post-info">
              <div class="post-info-header"><span class="icon post-info-icon mdi mdi-information text-color-6"></span>
                <h4 class="post-info-title">What do I receive when I order a template?</h4>
              </div>
              <div class="post-info-main">
                <p>After you complete the payment via our secure form you will receive the instructions for downloading the product.</p>
              </div>
            </article>
            <article class="post-info">
              <div class="post-info-header"><span class="icon post-info-icon mdi mdi-information text-secondary"></span>
                <h4 class="post-info-title">Do you provide any scripts with your templates?</h4>
              </div>
              <div class="post-info-main">
                <p>Our templates do not include any additional scripts. Newsletter subscriptions, search fields, forums, image galleries are inactive.   </p>
              </div>
            </article>
          </div>
        </div>
      </section>-->
      <!-- Join our community-->
      <section class="section section-md">
        <div class="container">
          <div class="row row-50 justify-content-center justify-content-lg-start align-items-center">
            <div class="col-md-10 col-lg-4">
              <div class="badge wow clipInLeft">Join our community</div>
              <h2 class="block-7 wow clipInLeft" data-wow-delay=".1s">Over <span style="white-space: nowrap;">230</span> Users</h2>
              <p class="wow clipInLeft" data-wow-delay=".2s">We have a community of dedicated users who contribute a lot to the development of our app. Look at what they have to say!</p>
            </div>
            <div class="col-lg-8 wow fadeIn" data-wow-delay=".3s"> 
              <div class="map">
                <div class="map-inner">
                  <button class="map-marker" id="map-marker-1" data-multitoggle="#world-map-popup-1" data-scope="#map-marker-1, .map-popup" style="left: 20%; top: 39%;"></button>
                  <!--<button class="map-marker" id="map-marker-2" data-multitoggle="#world-map-popup-2" data-scope="#map-marker-2, .map-popup" style="left: 65%; top: 24%;"></button>
                  <button class="map-marker" id="map-marker-3" data-multitoggle="#world-map-popup-3" data-scope="#map-marker-3, .map-popup" style="left: 53%; top: 54%;"></button>
                  <button class="map-marker" id="map-marker-4" data-multitoggle="#world-map-popup-4" data-scope="#map-marker-4, .map-popup" style="left: 30%; top: 70%;"></button>-->
                  <img class="map-image" src="landing-assets/images/world-map.svg" alt=""/>
                </div>
                <article class="map-popup" id="world-map-popup-1">
                  <div class="map-popup-toggle" id="world-map-toggle-1" data-multitoggle="#world-map-popup-1"><span class="icon mdi mdi-close"></span></div>
                  <div class="map-popup-inner map-popup-header">
                    <p class="map-popup-location">USA</p>
                    <p class="map-popup-meta">230+ Users</p>
                  </div>
                  <div class="map-popup-inner map-popup-main">
                    <!-- Owl Carousel-->
                    <div class="owl-carousel" data-items="1" data-dots="true" data-nav="false" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false">
                      <blockquote class="quote-minimal">
                        <div class="quote-minimal-text">
                          <p><a href="#">#MarketForce</a> is awesome! I use it a lot to manage my startup even when I’m not at the office.</p>
                        </div>
                        <div class="quote-minimal-meta"><img class="quote-minimal-avatar" src="landing-assets/images/testimonials-6-74x74.jpg" alt="" width="74" height="74"/>
                          <div class="quote-minimal-meta-main">
                            <cite class="quote-minimal-cite heading-5">Jack Oliver</cite>
                            <p class="quote-minimal-position">Customer</p>
                          </div>
                        </div>
                      </blockquote>
                      <blockquote class="quote-minimal">
                        <div class="quote-minimal-text">
                          <p>Every complex project that my company deals with can now be easily managed via <a href="#">#MarketForce</a></p>
                        </div>
                        <div class="quote-minimal-meta"><img class="quote-minimal-avatar" src="landing-assets/images/testimonials-2-74x74.jpg" alt="" width="74" height="74"/>
                          <div class="quote-minimal-meta-main">
                            <cite class="quote-minimal-cite heading-5">Peter Wilson</cite>
                            <p class="quote-minimal-position">Customer</p>
                          </div>
                        </div>
                      </blockquote>
                      <blockquote class="quote-minimal">
                        <div class="quote-minimal-text">
                          <p>I discovered <a href="#">#MarketForce</a> a year ago and got instantly attracted by its numerous wonderful features.</p>
                        </div>
                        <div class="quote-minimal-meta"><img class="quote-minimal-avatar" src="landing-assets/images/testimonials-4-74x74.jpg" alt="" width="74" height="74"/>
                          <div class="quote-minimal-meta-main">
                            <cite class="quote-minimal-cite heading-5">James Anderson</cite>
                            <p class="quote-minimal-position">Customer</p>
                          </div>
                        </div>
                      </blockquote>
                    </div>
                  </div>
                </article>
                <!--<article class="map-popup" id="world-map-popup-2">
                  <div class="map-popup-toggle" id="world-map-toggle-2" data-multitoggle="#world-map-popup-2"><span class="icon mdi mdi-close"></span></div>
                  <div class="map-popup-inner map-popup-header">
                    <p class="map-popup-location">Russia</p>
                    <p class="map-popup-meta">250,000 Customers</p>
                  </div>
                  <div class="map-popup-inner map-popup-main">
                    <div class="owl-carousel" data-items="1" data-dots="true" data-nav="false" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false">
                      <blockquote class="quote-minimal">
                        <div class="quote-minimal-text">
                          <p>I discovered <a href="#">#MarketForce_app</a> a year ago and got instantly attracted by its numerous wonderful features.</p>
                        </div>
                        <div class="quote-minimal-meta"><img class="quote-minimal-avatar" src="landing-assets/images/testimonials-4-74x74.jpg" alt="" width="74" height="74"/>
                          <div class="quote-minimal-meta-main">
                            <cite class="quote-minimal-cite heading-5">James Anderson</cite>
                            <p class="quote-minimal-position">Customer</p>
                          </div>
                        </div>
                      </blockquote>
                      <blockquote class="quote-minimal">
                        <div class="quote-minimal-text">
                          <p><a href="#">#MarketForce_app</a> is awesome! I use it a lot to manage my startup even when I’m not at the office.</p>
                        </div>
                        <div class="quote-minimal-meta"><img class="quote-minimal-avatar" src="landing-assets/images/testimonials-6-74x74.jpg" alt="" width="74" height="74"/>
                          <div class="quote-minimal-meta-main">
                            <cite class="quote-minimal-cite heading-5">Jack Oliver</cite>
                            <p class="quote-minimal-position">Customer</p>
                          </div>
                        </div>
                      </blockquote>
                      <blockquote class="quote-minimal">
                        <div class="quote-minimal-text">
                          <p>Every complex project that my company deals with can now be easily managed via <a href="#">#MarketForce_app</a></p>
                        </div>
                        <div class="quote-minimal-meta"><img class="quote-minimal-avatar" src="landing-assets/images/testimonials-2-74x74.jpg" alt="" width="74" height="74"/>
                          <div class="quote-minimal-meta-main">
                            <cite class="quote-minimal-cite heading-5">Peter Wilson</cite>
                            <p class="quote-minimal-position">Customer</p>
                          </div>
                        </div>
                      </blockquote>
                    </div>
                  </div>
                </article>
                <article class="map-popup" id="world-map-popup-3">
                  <div class="map-popup-toggle" id="world-map-toggle-3" data-multitoggle="#world-map-popup-3"><span class="icon mdi mdi-close"></span></div>
                  <div class="map-popup-inner map-popup-header">
                    <p class="map-popup-location">Egypt</p>
                    <p class="map-popup-meta">120,000 Customers</p>
                  </div>
                  <div class="map-popup-inner map-popup-main">
                    <div class="owl-carousel" data-items="1" data-dots="true" data-nav="false" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false">
                      <blockquote class="quote-minimal">
                        <div class="quote-minimal-text">
                          <p>Every complex project that my company deals with can now be easily managed via <a href="#">#MarketForce_app</a></p>
                        </div>
                        <div class="quote-minimal-meta"><img class="quote-minimal-avatar" src="landing-assets/images/testimonials-2-74x74.jpg" alt="" width="74" height="74"/>
                          <div class="quote-minimal-meta-main">
                            <cite class="quote-minimal-cite heading-5">Peter Wilson</cite>
                            <p class="quote-minimal-position">Customer</p>
                          </div>
                        </div>
                      </blockquote>
                      <blockquote class="quote-minimal">
                        <div class="quote-minimal-text">
                          <p>I discovered <a href="#">#MarketForce_app</a> a year ago and got instantly attracted by its numerous wonderful features.</p>
                        </div>
                        <div class="quote-minimal-meta"><img class="quote-minimal-avatar" src="landing-assets/images/testimonials-4-74x74.jpg" alt="" width="74" height="74"/>
                          <div class="quote-minimal-meta-main">
                            <cite class="quote-minimal-cite heading-5">James Anderson</cite>
                            <p class="quote-minimal-position">Customer</p>
                          </div>
                        </div>
                      </blockquote>
                      <blockquote class="quote-minimal">
                        <div class="quote-minimal-text">
                          <p><a href="#">#MarketForce_app</a> is awesome! I use it a lot to manage my startup even when I’m not at the office.</p>
                        </div>
                        <div class="quote-minimal-meta"><img class="quote-minimal-avatar" src="landing-assets/images/testimonials-6-74x74.jpg" alt="" width="74" height="74"/>
                          <div class="quote-minimal-meta-main">
                            <cite class="quote-minimal-cite heading-5">Jack Oliver</cite>
                            <p class="quote-minimal-position">Customer</p>
                          </div>
                        </div>
                      </blockquote>
                    </div>
                  </div>
                </article>
                <article class="map-popup" id="world-map-popup-4">
                  <div class="map-popup-toggle" id="world-map-toggle-4" data-multitoggle="#world-map-popup-4"><span class="icon mdi mdi-close"></span></div>
                  <div class="map-popup-inner map-popup-header">
                    <p class="map-popup-location">Brazil</p>
                    <p class="map-popup-meta">150,000 Customers</p>
                  </div>
                  <div class="map-popup-inner map-popup-main">
                    <div class="owl-carousel" data-items="1" data-dots="true" data-nav="false" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false">
                      <blockquote class="quote-minimal">
                        <div class="quote-minimal-text">
                          <p>I discovered <a href="#">#MarketForce_app</a> a year ago and got instantly attracted by its numerous wonderful features.</p>
                        </div>
                        <div class="quote-minimal-meta"><img class="quote-minimal-avatar" src="landing-assets/images/testimonials-4-74x74.jpg" alt="" width="74" height="74"/>
                          <div class="quote-minimal-meta-main">
                            <cite class="quote-minimal-cite heading-5">James Anderson</cite>
                            <p class="quote-minimal-position">Customer</p>
                          </div>
                        </div>
                      </blockquote>
                      <blockquote class="quote-minimal">
                        <div class="quote-minimal-text">
                          <p>Every complex project that my company deals with can now be easily managed via <a href="#">#MarketForce_app</a></p>
                        </div>
                        <div class="quote-minimal-meta"><img class="quote-minimal-avatar" src="landing-assets/images/testimonials-2-74x74.jpg" alt="" width="74" height="74"/>
                          <div class="quote-minimal-meta-main">
                            <cite class="quote-minimal-cite heading-5">Peter Wilson</cite>
                            <p class="quote-minimal-position">Customer</p>
                          </div>
                        </div>
                      </blockquote>
                      <blockquote class="quote-minimal">
                        <div class="quote-minimal-text">
                          <p><a href="#">#MarketForce_app</a> is awesome! I use it a lot to manage my startup even when I’m not at the office.</p>
                        </div>
                        <div class="quote-minimal-meta"><img class="quote-minimal-avatar" src="landing-assets/images/testimonials-6-74x74.jpg" alt="" width="74" height="74"/>
                          <div class="quote-minimal-meta-main">
                            <cite class="quote-minimal-cite heading-5">Jack Oliver</cite>
                            <p class="quote-minimal-position">Customer</p>
                          </div>
                        </div>
                      </blockquote>
                    </div>
                  </div>
                </article>-->
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Our Investors-->
      <section class="section section-md bg-gray-100 text-center">
        <div class="container">
          <!-- Owl Carousel-->
          <div class="owl-carousel owl-carousel-centered" data-items="2" data-sm-items="3" data-md-items="4" data-lg-items="5" data-dots="true" data-nav="false" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false">
            <div class="wow clipInLeft"><a class="link-image-1" href="#"><img src="https://ignition-innovations.com/images/brand/logo.png" alt=""/></a></div>
            <div class="wow clipInLeft" data-wow-delay=".1s"><a class="link-image-1" href="#"><img class="partner-img" src="//www.theaerationcorps.com/images/acpics/aeration-logo_final.png" alt=""/></a></div>
            <div class="wow clipInLeft" data-wow-delay=".2s"><a class="link-image-1" href="#"><img src="//marketforceapp.com/marketforce/forms/new-dealer-form/images/all-steel-red.jpg" alt=""/></a></div>
            <div class="wow clipInLeft" data-wow-delay=".3s"><a class="link-image-1" href="#"><img src="//www.acero.industries/images/new/ascero-logo.png" alt=""/></a></div>
            <div class="wow clipInLeft" data-wow-delay=".4s"><a class="link-image-1" href="#"><img src="//eccarports.com/assets/img/ECC%20LOGO.png" alt=""/></a></div>
            <div class="wow clipInLeft" data-wow-delay=".4s"><a class="link-image-1" href="#"><img src="//beta.ignition633.org/assets/img/home-tile/love.png" alt=""/></a></div>
            <div class="wow clipInLeft" data-wow-delay=".4s"><a class="link-image-1" href="#"><img src="//www.connectionscsp.org/wp-content/uploads/2016/06/connections-30-year-logo.jpg" alt=""/></a></div>
            <div class="wow clipInLeft" data-wow-delay=".4s"><a class="link-image-1" href="#"><img src="//81outfitters.com/images/logo-tagline.png" alt=""/></a></div>
            <div class="wow clipInLeft" data-wow-delay=".4s"><a class="link-image-1" href="#"><img src="//burtonsolution.tech/SDC/global/img/sdc-black.png" alt=""/></a></div>
          </div>
        </div>
      </section>
      <!-- Download the App-->
      <section class="section parallax-container section-lg bg-accent text-center" data-parallax-img="images/bg-image-1.jpg">
        <div class="parallax-content">
          <div class="container">
            <div class="badge wow fadeIn">Use the App</div>
            <h3 class="wow fadeIn" data-wow-delay=".1s">Time to Open New Horizons</h3>
            <p class="wow fadeIn" data-wow-delay=".2s"><span style="max-width: 740px; opacity: .65;">It’s time to increase your profit and achieve better results for your business! Use our app today and experience the new standard of project management.</span></p>
            <a class="button button-secondary button-winona wow fadeIn" data-wow-delay=".3s" href="login.html">Start Using MarketForce</a>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      <footer class="section footer-light">
        <div class="container">
          <div class="footer-light-main">
            <div class="brand-outer"> <a class="brand" href="index.html"><img class="brand-logo-dark" src="assets/img/full-mf-logo.png" alt="" width="113" height="38" srcset="assets/img/full-mf-logo.png 2x"/></a></div>
            <ul class="list footer-light-list">
              <li><a href="features.html">Features</a></li>
              <li><a href="pricing.html">Pricing</a></li>
            </ul>
            <p class="rights"><span>&copy;&nbsp; </span><span class="copyright-year"></span><span>&nbsp;</span><span>MarketForce</span><span>.&nbsp;</span><a href="privacy-policy.html">Privacy Policy</a></p>
          </div>
        </div>
      </footer>
    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="landing-assets/js/core.min.js"></script>
    <script src="landing-assets/js/script.js"></script>
  </body>
</html>