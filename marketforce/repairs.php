<?php
include 'security/session/session-settings.php';

if ($_SESSION['in'] != 'Yes') {
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Market Force | <?php

switch ($_SESSION['org_id']) {
	case "162534":
		echo 'Northedge Steel ';
		break;
	case "615243":
		echo 'Legacy';
		break;
	default:
		echo 'All Steel';
}
?></title>
	<!--JQuery-->
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="css/sb-admin.css" rel="stylesheet">

	<!-- Custom JS -->
	<script src="js/new/viewed.js"></script>

	<!-- Morris Charts CSS -->
	<link href="css/plugins/morris.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">



	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<style>
		.badge:hover {
			cursor: pointer;
		}

		th,
		td {
			padding: 10px;
		}

		#dl,
		dc {
			height: 430px;
		}

		.panel-body {
			height: 382px;
		}

		#scroll {
			overflow: scroll;
		}

		::-webkit-scrollbar {
			// width: 0px;  /* remove scrollbar space */
			//background: transparent;  /* optional: just make scrollbar invisible */
		}

		/* optional: show position indicator in red */
		::-webkit-scrollbar-thumb {
			// background: #FF0000;
		}


		.packet-table td,
		th {
			border: 1px solid black;
		}

		.packet-table {
			margin: auto;
		}

		#hoverGreen {
			color: blue;
		}

		#hoverGreen:hover {
			color: green;
		}

		#hoverRed {
			color: black;
		}

		#hoverRed:hover {
			color: blue;
		}

		mark {
			background: yellow;
		}
	</style>
	<script>
		/*if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  //xmlhttp.open("GET",eURL,true);
  //xmlhttp.send();
	xmlhttp.open("POST", pURL, true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send(params);*/

		var rep_name = '<? echo $_SESSION['full_name']; ?>';
		var rep_id = '<? echo $_SESSION['user_id']; ?>';

		function add_repair(mode, n) {
			var error_box = document.getElementById('error_box');

			var cname = document.getElementById('cname').value;
			if (cname === '') {
				document.getElementById('cname_error').innerHTML = '*Please Enter The Customer Name!';
				error_box.innerHTML = '*Please Enter The Customer Name!';
				return;
			}
			cname = urlEncode(cname);
			var inv = document.getElementById('inv').value;
			if (inv === '' && mode != 'Draft') {
				document.getElementById('inv_error').innerHTML = '*Please Enter The Invoice Number!';
				error_box.innerHTML = '*Please Enter The Invoice Number!';
				return;
			}
			inv = urlEncode(inv);
			/*var dname = document.getElementById('dname').value;
			if(dname === ''){
				document.getElementById('dname_error').innerHTML = '*Please Enter The Dealer\'s Name!';
				return;
			}
			dname = urlEncode(dname);*/
			var sale_date = document.getElementById('sale_date').value;
			if (sale_date === '' && mode != 'Draft') {
				document.getElementById('sale_date_error').innerHTML = '*Please Enter The Sale Date!';
				error_box.innerHTML = '*Please Enter The Sale Date!';
				return;
			}
			sale_date = urlEncode(sale_date);
			var contractor_name = document.getElementById('contractor_name').value;
			if (contractor_name === '' && mode != 'Draft') {
				document.getElementById('contractor_name_error').innerHTML = '*Please Enter The Contractor\'s Name!';
				error_box.innerHTML = '*Please Enter The Contractor\'s Name!';
				return;
			}
			contractor_name = urlEncode(contractor_name);
			var install_date = document.getElementById('install_date').value;
			if (install_date === '' && mode != 'Draft') {
				document.getElementById('install_date_error').innerHTML = '*Please Enter The Installed Date!';
				error_box.innerHTML = '*Please Enter The Installed Date!';
				return;
			}
			install_date = urlEncode(install_date);
			var address = document.getElementById('raddress').value;
			if (address === '' && mode != 'Draft') {
				document.getElementById('address_error').innerHTML = 'Please Enter The Repair Address!';
				error_box.innerHTML = 'Please Enter The Repair Address!';
				return;
			}
			address = urlEncode(address);
			var city = document.getElementById('rcity').value;
			if (city === '' && mode != 'Draft') {
				document.getElementById('city_error').innerHTML = 'Please Enter The City!';
				error_box.innerHTML = 'Please Enter The City!';
				return;
			}
			city = urlEncode(city);
			var state = document.getElementById('state').value;
			if (state === '') {
				document.getElementById('state_error').innerHTML = 'Please Enter The State!';
				error_box.innerHTML = 'Please Enter The State!';
				return;
			}
			state = urlEncode(state);
			var zip = document.getElementById('rzip').value;
			if (zip === '' && mode != 'Draft') {
				document.getElementById('zip_error').innerHTML = 'Please Enter The Zip Code!';
				error_box.innerHTML = 'Please Enter The Zip Code!';
				return;
			}
			zip = urlEncode(zip);
			var building = document.getElementById('building-size-type').value;
			if (building === '' && mode != 'Draft') {
				document.getElementById('building-size-type_error').innerHTML = 'Please Enter The Building Size and Type!';
				error_box.innerHTML = 'Please Enter The Building Size and Type!';
				return;
			}
			building = urlEncode(building);
			var cust_statement = document.getElementById('cust_statement').value;
			if (cust_statement === '' && mode != 'Draft') {
				document.getElementById('cust_statement_error').innerHTML = '*Please Enter The Customer Statement!';
				error_box.innerHTML = '*Please Enter The Customer Statement!';
				return;
			}
			cust_statement = urlEncode(cust_statement);
			var dealer_statement = document.getElementById('dealer_statement').value;
			if (dealer_statement === '' && mode != 'Draft') {
				document.getElementById('dealer_statement_error').innerHTML = '*Please Enter The Dealer Statment!';
				error_box.innerHTML = '*Please Enter The Dealer Statment!';
				return;
			}
			dealer_statement = urlEncode(dealer_statement);
			var contractor_statement = document.getElementById('contractor_statement').value;
			if (contractor_statement === '' && mode != 'Draft') {
				document.getElementById('contractor_statement_error').innerHTML = '*Please Enter The Contractor Statment!';
				error_box.innerHTML = '*Please Enter The Contractor Statment!';
				return;
			}
			contractor_statement = urlEncode(dealer_statement);
			var details = document.getElementById('details').value;
			if (details === '' && mode != 'Draft') {
				document.getElementById('details_error').innerHTML = '*Please Enter The Details Of The Repair!';
				error_box.innerHTML = '*Please Enter The Details Of The Repair!';
				return;
			}
			details = urlEncode(details);
			var urgent = document.getElementById('urgent').value;
			if (urgent === '' && mode != 'Draft') {
				document.getElementById('urgent_error').innerHTML = '*Please Select The Urgency Status!';
				error_box.innerHTML = '*Please Select The Urgency Status!';
				return;
			}
			var allsteel_cost = document.getElementById('allsteel_cost').value;
			if (allsteel_cost === '' && mode != 'Draft') {
				document.getElementById('total_cost_error').innerHTML = '*Every Cost Field Must Be Filled Out [0 or an amount]!';
				error_box.innerHTML = '*Every Cost Field Must Be Filled Out [0 or an amount]!';
				return;
			}
			var dealer_cost = document.getElementById('dealer_cost').value;
			if (dealer_cost === '' && mode != 'Draft') {
				document.getElementById('total_cost_error').innerHTML = '*Every Cost Field Must Be Filled Out [0 or an amount]!';
				error_box.innerHTML = '*Every Cost Field Must Be Filled Out [0 or an amount]!';
				return;
			}
			var contractor_cost = document.getElementById('contractor_cost').value;
			if (contractor_cost === '' && mode != 'Draft') {
				document.getElementById('total_cost_error').innerHTML = '*Every Cost Field Must Be Filled Out [0 or an amount]!';
				error_box.innerHTML = '*Every Cost Field Must Be Filled Out [0 or an amount]!';
				return;
			}
			var customer_cost = document.getElementById('customer_cost').value;
			if (customer_cost === '' && mode != 'Draft') {
				document.getElementById('total_cost_error').innerHTML = '*Every Cost Field Must Be Filled Out [0 or an amount]!';
				error_box.innerHTML = '*Every Cost Field Must Be Filled Out [0 or an amount]!';
				return;
			}
			var customer_balance = document.getElementById('customer_balance').value;
			if (customer_balance === '' && mode != 'Draft') {
				document.getElementById('total_cost_error').innerHTML = '*Every Cost Field Must Be Filled Out [0 or an amount]!';
				error_box.innerHTML = '*Every Cost Field Must Be Filled Out [0 or an amount]!';
				return;
			}
			var total_cost = document.getElementById('total_cost').value;
			if (total_cost === '' && mode != 'Draft') {
				document.getElementById('total_cost_error').innerHTML = '*Every Cost Field Must Be Filled Out [0 or an amount]!';
				error_box.innerHTML = '*Every Cost Field Must Be Filled Out [0 or an amount]!';
				return;
			}
			total_cost = urlEncode(total_cost);

			document.getElementById('mode').value = mode;
			//document.getElementById('new').value = n;
			document.getElementById('sub_btn').disabled = true; //disable button to prevent duplicate entries...
			document.getElementById('draft_btn').disabled = true; //disable button to prevent duplicate entries...
			document.getElementById('new-repair-form').submit();
			$("#newRepair").modal("hide");
			//document.getElementById('new-repair-form').reset();
			//window.location.reload();
		}



		function urlEncode(url) {
			url = url.replace(/&/g, '%26');
			url = url.replace(/#/g, '%23');
			return url;
		}

		function cost_total() {
			var ac = document.getElementById('allsteel_cost').value;
			ac = ac.replace(",", "");
			var dc = document.getElementById('dealer_cost').value;
			dc = dc.replace(",", "");
			var con_cost = document.getElementById('contractor_cost').value;
			con_cost = con_cost.replace(",", "");
			var cust_cost = document.getElementById('customer_cost').value;
			cust_cost = cust_cost.replace(",", "");
			var total = Number(ac) + Number(dc) + Number(con_cost) + Number(cust_cost);
			document.getElementById('total_cost').value = total.toFixed(2);
		}

		function move(id, mode) {
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else { // code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {

					alert(this.responseText);
					window.location.reload();

				}
			}
			xmlhttp.open("GET", "repairs/repair-handler.php?id=" + id + "&mode=" + mode + "&rep_name=<?php echo $_SESSION['full_name']; ?>&rep_id=<?php echo $_SESSION['user_id']; ?>", true);
			xmlhttp.send();
			//xmlhttp.open("POST", pURL, true);
			//xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			//xmlhttp.send(params);
		}

		function load_modal(mode, id, name) {
			//Load Note Modal
			if (mode === 'note') {
				document.getElementById('cust_id').value = id;
				document.getElementById('cust_name').innerHTML = name;
				document.getElementById('rcname').value = name;
				//document.getElementById('repairNote').style.display = 'block';
				//$("#repairNote").modal("show");
			}

			if (mode === 'Not Done') {
				document.getElementById('icust_id').value = id;
				document.getElementById('icust_name').innerHTML = name;
				document.getElementById('ircname').value = name;
			}

			if (mode === 'New') {
				document.getElementById('new-repair-form').reset();
				document.getElementById('rep_name').value = '<? echo $_SESSION['full_name']; ?>';
				document.getElementById('rep_id').value = '<? echo $_SESSION['user_id']; ?>';
				document.getElementById('new').value = 'True';
				document.getElementById('sub_by').innerHTML = '';
				document.getElementById('img1').innerHTML = '<input type="file" id="img1" name="img_1" class="form-control" />';
				document.getElementById('img2').innerHTML = '<input type="file" id="img2" name="img_2" class="form-control" />';
				document.getElementById('img3').innerHTML = '<input type="file" id="img3" name="img_3" class="form-control" />';
				document.getElementById('img4').innerHTML = '<input type="file" id="img4" name="img_4" class="form-control" />';
				document.getElementById('img5').innerHTML = '<input type="file" id="img5" name="img_5" class="form-control" />';
			}

		}



		function add_note() {
			var id = document.getElementById('cust_id').value;
			var cname = document.getElementById('rcname').value;
			cname = urlEncode(cname);
			var rep_id = '<? echo $_SESSION['user_id']; ?>';
			var rep_name = '<? echo $_SESSION['full_name']; ?>';
			var note = document.getElementById('rnote').value;
			if (note === '') {
				document.getElementById('rnote_error').innerHTML = 'Please Enter A Note!';
				return;
			}
			note = urlEncode(note);
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else { // code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {

					alert(this.responseText);
					window.location.reload();

				}
			}
			xmlhttp.open("GET", "repairs/add-repair-note.php?id=" + id + "&rid=" + rep_id + "&rname=" + rep_name + "&note=" + note + "&cname=" + cname, true);
			xmlhttp.send();
		}


		function add_inote() {
			var id = document.getElementById('icust_id').value;
			var mode = 'Not Done';
			var cname = document.getElementById('ircname').value;
			var rep_id = '<? echo $_SESSION['user_id']; ?>';
			var rep_name = '<? echo $_SESSION['full_name']; ?>';
			var note = document.getElementById('irnote').value;
			if (note === '') {
				document.getElementById('irnote_error').innerHTML = 'Please Enter A Note!';
				return;
			}
			note = urlEncode(note);
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else { // code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {

					alert(this.responseText);
					window.location.reload();

				}
			}
			xmlhttp.open("GET", "repairs/repair-handler.php?id=" + id + "&rid=" + rep_id + "&rname=" + rep_name + "&note=" + note + "&cname=" + cname + "&mode=" + mode, true);
			xmlhttp.send();
		}



		function fetch_repair(id) {

			document.getElementById('img1').innerHTML = '<input type="file" id="img1" name="img_1" class="form-control" />';
			document.getElementById('img2').innerHTML = '<input type="file" id="img2" name="img_2" class="form-control" />';
			document.getElementById('img3').innerHTML = '<input type="file" id="img3" name="img_3" class="form-control" />';
			document.getElementById('img4').innerHTML = '<input type="file" id="img4" name="img_4" class="form-control" />';
			document.getElementById('img5').innerHTML = '<input type="file" id="img5" name="img_5" class="form-control" />';

			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else { // code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					//alert(this.responseText);
					var q = JSON.parse(this.responseText); //This puts the JSON data into useable data
					//window.location.reload();
					//Load Modal Fields Here...
					document.getElementById('cname').value = q.cname;
					document.getElementById('today_date').innerHTML = q.today_date;
					document.getElementById('inv').value = q.inv;
					document.getElementById('state').value = q.state;
					document.getElementById('dname').value = q.dname;
					document.getElementById('sale_date').value = q.sale_date;
					document.getElementById('contractor_name').value = q.contractor_name;
					document.getElementById('install_date').value = q.install_date;
					document.getElementById('raddress').value = q.address;
					document.getElementById('rcity').value = q.city;
					document.getElementById('state').value = q.state;
					document.getElementById('rzip').value = q.zip;
					document.getElementById('building-size-type').value = q.building;
					document.getElementById('cust_statement').value = q.cust_statement;
					document.getElementById('dealer_statement').value = q.dealer_statement;
					document.getElementById('contractor_statement').value = q.contractor_statement;
					document.getElementById('details').value = q.details;
					document.getElementById('cphone').value = q.cphone;
					document.getElementById('cemail').value = q.cemail;
					document.getElementById('task1').value = q.task1;
					document.getElementById('task2').value = q.task2;
					document.getElementById('task3').value = q.task3;
					document.getElementById('task4').value = q.task4;
					document.getElementById('task5').value = q.task5;
					document.getElementById('task6').value = q.task6;
					document.getElementById('allsteel_cost').value = q.allsteel_cost;
					document.getElementById('dealer_cost').value = q.dealer_cost;
					document.getElementById('contractor_cost').value = q.contractor_cost;
					document.getElementById('customer_cost').value = q.customer_cost;
					document.getElementById('customer_balance').value = q.customer_balance;
					document.getElementById('total_cost').value = q.total_cost;
					document.getElementById('urgent').value = q.urgent;
					document.getElementById('iid').value = q.ID;
					document.getElementById('dname').value = q.did;
					if (q.img1 != '') {
						document.getElementById('img1').innerHTML = '<p>Image 1</p><a href="' + q.img1 + '" target="_blank" style="color:blue;font-weight:bold;">View Image</a>&nbsp;&nbsp;<a style="color:red;" href="Javascript:remove_img(\'img1\',' + id + ');">(Remove)</a>';
					}
					if (q.img2 != '') {
						document.getElementById('img2').innerHTML = '<p>Image 2</p><a href="' + q.img2 + '" target="_blank" style="color:blue;font-weight:bold;">View Image</a>&nbsp;&nbsp;<a style="color:red;" href="Javascript:remove_img(\'img2\',' + id + ');">(Remove)</a>';
					}
					if (q.img3 != '') {
						document.getElementById('img3').innerHTML = '<p>Image 3</p><a href="' + q.img3 + '" target="_blank" style="color:blue;font-weight:bold;">View Image</a>&nbsp;&nbsp;<a style="color:red;" href="Javascript:remove_img(\'img3\',' + id + ');">(Remove)</a>';
					}
					if (q.img4 != '') {
						document.getElementById('img4').innerHTML = '<p>Image 4</p><a href="' + q.img4 + '" target="_blank" style="color:blue;font-weight:bold;">View Image</a>&nbsp;&nbsp;<a style="color:red;" href="Javascript:remove_img(\'img4\',' + id + ');">(Remove)</a>';
					}
					if (q.img5 != '') {
						document.getElementById('img5').innerHTML = '<p>Image 5</p><a href="' + q.img5 + '" target="_blank" style="color:blue;font-weight:bold;">View Image</a>&nbsp;&nbsp;<a style="color:red;" href="Javascript:remove_img(\'img5\',' + id + ');">(Remove)</a>';
					}

					//Submitted By...
					if (q.submitted_by_name == '') {
						document.getElementById('sub_by').innerHTML = 'Repair Created By: <span style="color:blue;font-weight:bold;">UNKNOWN</span>';
					} else {
						document.getElementById('sub_by').innerHTML = 'Repair Created By: <span style="color:blue;font-weight:bold;">' + q.submitted_by_name + '</span>';
					}
					//Setup Mode
					document.getElementById('new').value = 'False';

				}
			}
			xmlhttp.open("GET", "repairs/fetch-repair.php?id=" + id, true);
			xmlhttp.send();
		}

		function remove_img(img, id) {

			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else { // code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {

					alert(this.responseText);
					fetch_repair(id);

				}
			}
			xmlhttp.open("GET", "repairs/remove-image.php?img=" + img + "&id=" + id, true);
			xmlhttp.send();
		}
	</script>
</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<?php include 'nav.php'; ?>

		<div id="page-wrapper">

			<div class="container-fluid">

				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">
							<!-- Dashboard <small>Statistics Overview</small>-->
							<img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;"><small> Repairs</small>
						</h1>
						<ol class="breadcrumb">
							<li class="active">
								<i class="fa fa-dashboard"></i> Dashboard
							</li>
							<li>
								<i class="fa fa-wrench"></i> Repairs
							</li>
							<br><br>
							<li>
								<small>
									<b>Status Legend:</b>
									<span style="background:red;color:white;padding:5px;border:1px solid black;">Pending (no money owed)</span>&nbsp;
									<span style="background:green;color:white;padding:5px;border:1px solid black;">Pending (money owed)</span>&nbsp;
									<!--<span style="background:blue;color:white;padding:5px;border:1px solid black;">Pending (Awaiting Forms)</span>&nbsp;-->
									<span style="background:white;color:black;padding:5px;border:1px solid black;">Scheduled</span>&nbsp;
									<span style="background:purple;color:white;padding:5px;border:1px solid black;">Not Done</span>&nbsp;
									<span style="background:pink;color:black;padding:5px;border:1px solid black;">Draft</span>&nbsp;
								</small>
							</li>
							<li>
								<small>
									<b>Date Legend:</b>
									<span style="background:black;color:white;padding:5px;border:1px solid black;">Awaiting Forms</span>&nbsp;
									<span style="background:white;color:black;padding:5px;border:1px solid black;">Week 1</span>&nbsp;
									<span style="background:blue;color:white;padding:5px;border:1px solid black;">Week 2</span>&nbsp;
									<span style="background:yellow;color:black;;padding:5px;border:1px solid black;">Week 3</span>&nbsp;
									<span style="background:red;color:white;;padding:5px;border:1px solid black;">Week 4+</span>&nbsp;
								</small>
							</li>
						</ol>
					</div>
				</div>
				<!-- /.row -->

				<!-- This is a notification -->
				<?php
				$notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
				$notget = mysqli_query($conn, $notq);
				while ($notr = mysqli_fetch_array($notget)) {
					echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
				}
				?>
				<!-- /.row -->


				<!--This is where real content for the page will go-->




				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-info">
							<div class="panel-heading">
								<h3 class="panel-title">
									Pending Repairs &nbsp;&nbsp;&nbsp;
									<button type="button" class="btn btn-primary" onclick="load_modal('New')" data-toggle="modal" data-target="#newRepair">New Repair</button>
									<?php
									if ($_GET['q'] == 'ALL') {
										echo '<a href="repairs.php" style="text-align:right;">
																<button type="button" class="btn btn-success">View My States</button>
															</a>';
									} else {
										echo '<a href="repairs.php?q=ALL" style="text-align:right;">
																<button type="button" class="btn btn-success">View All</button>
															</a>';
									}
									?>
									<a href="http://marketforceapp.com/marketforce/map/repairs-map.php" target="_blank">
										<button type="button" class="btn btn-info"><i class="fa fa-globe"></i> View Map</button>
									</a>
								</h3>
							</div>
							<div class="panel-body" style="height:auto;">
								<div class="table-responsive">
									<table class="table table-bordered table-hover table-striped packet-table">
										<tr>
											<th>Inv #</th>
											<th>Name</th>
											<th>Date</th>
											<th>Location</th>
											<th>Contractor</th>
											<th>Repair Cost</th>
											<th>Status</th>
											<th>Size / Type</th>
											<th style="min-width:400px;">Notes</th>
											<th>Actions</th>
										</tr>
										<?php
										if ($_GET['q'] == 'ALL') {
											$rq = "SELECT * FROM `repairs` WHERE `status` != 'Completed' AND `status` != 'Cancelled' ORDER BY FIELD(`status`,'2nd Attempt','Pending','Scheduled','Draft'), `today_date` ASC";
										} else {
											if ($_SESSION['user_id'] == '11') {
												$rq = "SELECT * FROM `repairs` WHERE `status` != 'Completed' AND `status` != 'Cancelled' AND `status` = 'Draft'";
											} else {
												$rq = "SELECT * FROM `repairs` WHERE `status` != 'Completed' AND `status` != 'Cancelled' AND (`state` = '" . $_SESSION['myState'] . "' OR `state` = '" . $_SESSION['myState2'] . "' OR `state` = '" . $_SESSION['myState3'] . "' OR `state` = '" . $_SESSION['myState4'] . "') ORDER BY FIELD(`status`,'2nd Attempt','Pending','Scheduled','Draft'), `today_date` ASC";
											}
										}
										$rg = mysqli_query($conn, $rq) or die($conn->error);
										while ($rr = mysqli_fetch_array($rg)) {
											//Main Color Coding...
											if ($rr['urgent'] == 'Yes') {
												$style = ' style="color:white;background:green;font-weight:bold;"';
											}
											if ($rr['urgent'] == 'No') {
												$style = ' style="color:white;background:red;font-weight:bold;"';
											}
											/*if($rr['status'] == 'Not Done'){
														$style = ' style="color:black;background:purple;font-weight:bold;"';
													}*/
											if ($rr['status'] == 'Scheduled') {
												$sstyle = $style;
												$style = ' style="color:black;background:white;font-weight:bold;"';
											}
											if ($rr['status'] == 'Draft') {
												$style = ' style="color:black;background:pink;font-weight:bold;"';
											}

											//Date Color Coding...
											$td = date("m-d-y");
											//$d1 = strtotime($rr['today_date']);
											$d1 = strtotime($rr['pl_sign_date']);
											$d2 = strtotime("-2 week");
											$d3 = strtotime("-3 week");
											$d4 = strtotime("-4 week");
											if ($rr['pl_sign'] != '' && $rr['pl_sign'] != '{"lines":[]}') {
												if ($d4 >= $d1) {
													$age = 'style="background:red;"';
												} else if ($d3 >= $d1) {
													$age = 'style="background:yellow;color:black;"';
												} else if ($d2 >= $d1) {
													$age = 'style="background:blue;"';
												} else {
													$age = 'style="background:white;color:black;"';
												}
												/*if($rr['status'] == 'Draft'){
														$age = 'style="background:pink;"';
													}*/
												if ($rr['status'] == 'Not Done') {
													$age = 'style="background:purple;"';
												}
											} else {
												$age = 'style="background:black;color:white;"';
											}

											//Check for Address Issues
											if ($rr['lat'] == 'Error' || $rr['lng'] == 'Error') {
												$address_warn = '<p style="color:yellow;"><i class="fa fa-exclamation-triangle"></i> Possible Address Issue!</p>';
											} else {
												$address_warn = '';
											}

											echo '<tr' . $style . '>';
											echo '<td><a style="color:black;" href="http://marketforceapp.com/marketforce/scheduling/beta-order-invoice.php?inv=' . $rr['inv'] . '" target="_blank">' . $rr['inv'] . '</a></td>';
											echo '<td' . $sstyle . '>' . $rr['cname'] . $address_warn . '</td>';

											if ($rr['pl_sign'] == '{"lines":[]}') {
												echo '<td ' . $age . '>' . date("m/d/y", strtotime($rr['today_date'])) . '</td>';
											} else {
												echo '<td ' . $age . '>' . date("m/d/y", strtotime($rr['pl_sign_date'])) . '</td>';
											}

											echo '<td>' . $rr['city'] . ', ' . $rr['state'] . '</td>
																	<td>' . $rr['contractor_name'] . '</td>
																	<td>$' . $rr['total_cost'] . '</td>
																	<td>' . $rr['status'] . '</td>
																	<td>' . $rr['building_size_type'] . '</td>';

											$nq = "SELECT * FROM `repair_notes` WHERE `cid` = '" . $rr['ID'] . "' AND `inactive` != 'Yes' ORDER BY `ID` DESC";
											$ng = mysqli_query($conn, $nq) or die($conn->error);

											echo '<td style="max-height:160px;max-width:250px;">
																<div style="max-height:150px;overflow:scroll;color:black;">';
											while ($nr = mysqli_fetch_array($ng)) {
												echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-comments-o"></i> ' . $nr['rep_name'] . ' ' . date("m/d/y", strtotime($nr['date'])) . ' ' . date("h:i A", strtotime($nr['time'])) . '
													</span></h4>
														<div class="well well-sm">
														' . mysqli_real_escape_string($conn, $nr['note']) . '
													</div>';
											}
											echo '</div>
																</td>';

											if ($rr['status'] == 'Pending' || $rr['status'] == 'Not Done') {
												echo '<td style="background:white;">
																	<section style="white-space:nowrap;">
                                    <a href="repairs/view-repair.php?id=' . $rr['ID'] . '" target="_blank">
                                    <button type="button" class="btn btn-info btn-xs">Repair Form</button>
                                    </a>';
												if ($rr['customer_cost'] > 0 || $rr['customer_balance'] > 0) {
													echo '<a href="repairs/owes-punch-list.php?id=' . $rr['ID'] . '" target="_blank">
                              		      <button type="button" class="btn btn-info btn-xs">Punch List</button>
                              		      </a>';
												} else {
													echo '<a href="repairs/paid-punch-list.php?id=' . $rr['ID'] . '" target="_blank">
                              		      <button type="button" class="btn btn-info btn-xs">Punch List</button>
                              		      </a>';
												}
												echo '</section><br>
																		<button type="button" class="btn btn-warning btn-xs" onclick="load_modal(\'note\',' . $rr['ID'] . ',\'' . mysqli_real_escape_string($conn, $rr['cname']) . '\');" data-toggle="modal" data-target="#repairNote">Add Note</button>
																		<button type="button" class="btn btn-primary btn-xs" onclick="move(' . $rr['ID'] . ',\'Scheduled\');">Schedule</button>
																		<br><br>
																		<button type="button" class="btn btn-danger btn-xs" style="background:black;" onclick="fetch_repair(' . $rr['ID'] . ');" data-toggle="modal" data-target="#newRepair"><i class="fa fa-pencil"></i> Edit</button>
																		<button type="button" class="btn btn-danger btn-xs" onclick="move(' . $rr['ID'] . ',\'Cancelled\');">Cancel</button>
                                    <button type="button" class="btn btn-default btn-xs" style="background:grey;color:white;" onclick="send_closing(' . $rr['ID'] . ');">Send Closing</button>
                                    <br>';
												if ($rr['closing_email_sent'] != '0000-00-00') {
													echo '<p><small>Closing Last Sent: ' . date("m/d/y", strtotime($rr['closing_email_sent'])) . '</small></p>';
												}
												echo '</td>';
											}
											if ($rr['status'] == 'Scheduled') {
												echo '<td style="background:white;">
                                    <a href="repairs/view-repair.php?id=' . $rr['ID'] . '" target="_blank">
                                    <button type="button" class="btn btn-info btn-xs">Repair Form</button>
                                    </a>';
												if ($rr['customer_cost'] > 0 || $rr['customer_balance'] > 0) {
													echo '<a href="repairs/owes-punch-list.php?id=' . $rr['ID'] . '" target="_blank">
                              		      <button type="button" class="btn btn-info btn-xs">Punch List</button>
                              		      </a>';
												} else {
													echo '<a href="repairs/paid-punch-list.php?id=' . $rr['ID'] . '" target="_blank">
                              		      <button type="button" class="btn btn-info btn-xs">Punch List</button>
                              		      </a>';
												}
												echo '<br><br>
																		<button type="button" class="btn btn-warning btn-xs" onclick="load_modal(\'note\',' . $rr['ID'] . ',\'' . mysqli_real_escape_string($conn, $rr['cname']) . '\');" data-toggle="modal" data-target="#repairNote">Add Note</button>
																		<button type="button" class="btn btn-danger btn-xs" style="background:black;" onclick="fetch_repair(' . $rr['ID'] . ');" data-toggle="modal" data-target="#newRepair"><i class="fa fa-pencil"></i> Edit</button>
																		<br><br>
																		<button type="button" class="btn btn-success btn-xs" onclick="move(' . $rr['ID'] . ',\'Completed\');">Completed</button>
																		<button type="button" class="btn btn-danger btn-xs" onclick="load_modal(\'Not Done\',' . $rr['ID'] . ',\'' . mysqli_real_escape_string($conn, $rr['cname']) . '\');" data-toggle="modal" data-target="#incompleteNote">Not Done</button>
                                    <button type="button" class="btn btn-default btn-xs" style="background:grey;color:white;" onclick="send_closing(' . $rr['ID'] . ');">Send Closing</button>
                                    <br>';
												if ($rr['closing_email_sent'] != '0000-00-00') {
													echo '<p><small>Closing Last Sent: ' . date("m/d/y", strtotime($rr['closing_email_sent'])) . '</small></p>';
												}
												echo '</td>';
											}
											if ($rr['status'] == 'Draft') {
												echo '<td style="background:white;">
                                    <a href="repairs/view-repair.php?id=' . $rr['ID'] . '" target="_blank">
                                    <button type="button" class="btn btn-info btn-xs">Repair Form</button>
                                    </a>';
												if ($rr['customer_cost'] > 0 || $rr['customer_balance'] > 0) {
													echo '<a href="repairs/owes-punch-list.php?id=' . $rr['ID'] . '" target="_blank">
                              		      <button type="button" class="btn btn-info btn-xs">Punch List</button>
                              		      </a>';
												} else {
													echo '<a href="repairs/paid-punch-list.php?id=' . $rr['ID'] . '" target="_blank">
                              		      <button type="button" class="btn btn-info btn-xs">Punch List</button>
                              		      </a>';
												}
												echo '<br><br>
																		<button type="button" class="btn btn-warning btn-xs" onclick="load_modal(\'note\',' . $rr['ID'] . ',\'' . mysqli_real_escape_string($conn, $rr['cname']) . '\');" data-toggle="modal" data-target="#repairNote">Add Note</button>
																		<button type="button" class="btn btn-danger btn-xs" style="background:black;" onclick="fetch_repair(' . $rr['ID'] . ');" data-toggle="modal" data-target="#newRepair"><i class="fa fa-pencil"></i> Edit</button>
																		<br><br>
																		<button type="button" class="btn btn-danger btn-xs" onclick="move(' . $rr['ID'] . ',\'Cancelled\');">Cancel</button>
                                    <button type="button" class="btn btn-default btn-xs" style="background:grey;color:white;" onclick="send_closing(' . $rr['ID'] . ');">Send Closing</button>
                                    <br>';
												if ($rr['closing_email_sent'] != '0000-00-00') {
													echo '<p><small>Closing Last Sent: ' . date("m/d/y", strtotime($rr['closing_email_sent'])) . '</small></p>';
												}
												echo '</td>';
											}
											echo '</tr>';
										}

										?>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!--End Row-->




				</div>
			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Morris Charts JavaScript -->
	<script src="js/plugins/morris/raphael.min.js"></script>
	<script src="js/plugins/morris/morris.min.js"></script>






	<!-- Modal -->
	<div class="modal fade" id="newRepair" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">New Repair</h4>
				</div>
				<div class="modal-body">
					<form id="new-repair-form" action="repairs/add-repair.php" method="post" enctype="multipart/form-data">
						<img src="<?php

									switch ($_SESSION['org_id']) {
										case "832122":
											echo 'http://marketforceapp.com/assets/img/brands/acero-news.png';
											break;
										case "738004":
											echo 'http://marketforceapp.com/assets/img/brands/acero-news.png';
											break;
										case "654321":
											echo 'http://marketforceapp.com/assets/img/brands/acero-news.png';
											break;
										case "162534":
											echo 'http://marketforceapp.com/assets/img/brands/NorthEdgeSteel-01.png';
											break;
										case "615243":
											echo 'http://marketforceapp.com/assets/img/brands/legacyMFlogo.png';
											break;
										default:
											echo 'http://marketforceapp.com/assets/img/brands/Logo-mailing-allsteel.png';
									}
									?>" style="width:40%;float: left;" />
						<h3 style="text-align:right;">Authorized Repair</h3>
						<br><br><br><br>
						<hr />
						<div class="row">
							<div class="col-lg-6">
								<p><mark>Customer Name</mark></p>
								<input type="text" class="form-control" id="cname" name="cname" />
								<p style="color:red;font-weight:bold;" id="cname_error"></p>
							</div>
							<div class="col-lg-3">
								<p>Today's Date</p>
								<!--<input type="text" class="form-control date" id="today_date" name="today_date" placeholder="mm/dd/yyyy" />-->
								<p id="today_date"><? echo date("m/d/y"); ?></p>
							</div>
							<div class="col-lg-3">
								<p><mark>Invoice#</mark></p>
								<input type="text" class="form-control" id="inv" name="inv" />
								<p style="color:red;font-weight:bold;" id="inv_error"></p>
							</div>
						</div>
						<!--END ROW-->

						<div class="row">
							<div class="col-lg-6">
								<p><mark>Dealer Name</mark></p>
								<!--<input type="text" class="form-control" id="dname" name="dname" />-->
								<select class="form-control" id="dname" name="dname">
									<option value="">None</option>
									<?php
									$dlq = "SELECT * FROM `dealers` WHERE `inactive` != 'Yes' ORDER BY `name` ASC";
									$dlg = mysqli_query($conn, $dlq) or die($conn->error);
									while ($dlr = mysqli_fetch_array($dlg)) {
										echo '<option value="' . $dlr['ID'] . '">' . $dlr['name'] . ' (' . $dlr['city'] . ', ' . $dlr['state'] . ')</option>';
									}
									?>
								</select>
								<p style="color:red;font-weight:bold;" id="dname_error"></p>
							</div>
							<div class="col-lg-6">
								<p>Sale Date</p>
								<input type="text" class="form-control date" id="sale_date" name="sale_date" placeholder="mm/dd/yyyy" autocomplete="off" />
								<p style="color:red;font-weight:bold;" id="sale_date_error"></p>
							</div>
						</div>
						<!--END ROW-->

						<div class="row">
							<div class="col-lg-6">
								<p>Contractor's Name</p>
								<input type="text" class="form-control" id="contractor_name" name="contractor_name" />
								<p style="color:red;font-weight:bold;" id="contractor_name_error"></p>
							</div>
							<div class="col-lg-6">
								<p>Date Installed</p>
								<input type="text" class="form-control date" id="install_date" name="install_date" placeholder="mm/dd/yyyy" autocomplete="off" />
								<p style="color:red;font-weight:bold;" id="install_date_error"></p>
							</div>
						</div><!-- END ROW -->

						<div class="row">
							<div class="col-lg-6">
								<p>Address For Repair</p>
								<input type="text" name="raddress" id="raddress" class="form-control" />
								<p style="color:red;font-weight:bold;" id="address_error"></p>
							</div>
							<div class="col-lg-6">
								<p><mark>City</mark></p>
								<input type="text" name="rcity" id="rcity" class="form-control" />
								<p style="color:red;font-weight:bold;" id="city_error"></p>
							</div>
						</div>
						<!--END ROW-->

						<div class="row">
							<div class="col-lg-6">
								<p><mark>State</mark></p>
								<select class="form-control" id="state" name="state">
									<option value="">Select One</option>
									<?php
									$sq = "SELECT DISTINCT `state` FROM `dealers` WHERE `inactive` != 'Yes' AND `state` != '' ORDER BY `state` ASC";
									$sg = mysqli_query($conn, $sq) or die($conn->error);
									while ($sr = mysqli_fetch_array($sg)) {
										echo '<option value="' . $sr['state'] . '">' . $sr['state'] . '</option>';
									}
									?>
								</select>
								<p style="color:red;font-weight:bold;" id="state_error"></p>
							</div>
							<div class="col-lg-6">
								<p>Zip Code</p>
								<input type="text" name="rzip" id="rzip" class="form-control" />
								<p style="color:red;font-weight:bold;" id="zip_error"></p>
							</div>
						</div>
						<!--END ROW-->


						<div class="row">
							<div class="col-lg-12">
								<p><mark>Buliding Size / Type</mark></p>
								<textarea class="form-control" id="building-size-type" name="building-size-type"></textarea>
								<p style="color:red;font-weight:bold;" id="building-size-type_error"></p>
							</div>
						</div>
						<!--END ROW-->


						<div class="row">
							<div class="col-lg-12">
								<p><mark>Customer Statement</mark></p>
								<textarea class="form-control" id="cust_statement" name="cust_statement"></textarea>
								<p style="color:red;font-weight:bold;" id="cust_statement_error"></p>
							</div>
						</div>
						<!--END ROW-->

						<div class="row">
							<div class="col-lg-12">
								<p>Dealer Statement</p>
								<textarea class="form-control" id="dealer_statement" name="dealer_statement"></textarea>
								<p style="color:red;font-weight:bold;" id="dealer_statement_error"></p>
							</div>
						</div>
						<!--END ROW-->

						<div class="row">
							<div class="col-lg-12">
								<p>Contractor Statement</p>
								<textarea class="form-control" id="contractor_statement" name="contractor_statement"></textarea>
								<p style="color:red;font-weight:bold;" id="contractor_statement_error"></p>
							</div>
						</div>
						<!--END ROW-->

						<div class="row">
							<div class="col-lg-12">
								<p>Details of Approved Repair(Punch List)</p>
								<textarea class="form-control" id="details" name="details"></textarea>
								<p style="color:red;font-weight:bold;" id="details_error"></p>
							</div>
						</div>
						<!--END ROW-->

						<div class="row">
							<div class="col-lg-6">
								<div class="alert alert-info">
									<small>
										<p>Phone Prompt:</p>
										<p><i class="fa fa-microphone fa-fw"></i> What is the best <b>cell phone number</b> that we could text notifications to regarding this repair?</p>
										<!--<p style="color:red;">NOTE: If the customer says they do not have one or refuses to give this info,</p>-->
									</small>
								</div>
								<p><mark>Customer Cell Phone#:</mark></p>
								<input type="text" class="form-control phone" id="cphone" name="cphone">
								<p style="color:red;font-weight:bold;" id="cphone_error"></p>
							</div>

							<div class="col-lg-6">
								<div class="alert alert-info">
									<small>
										<p>Phone Prompt:</p>
										<p><i class="fa fa-microphone fa-fw"></i> What is the best <b>email address</b> that we could email notifications to regarding this repair?</p>
									</small>
								</div>
								<p><mark>Customer Email:</mark></p>
								<input type="text" class="form-control" id="cemail" name="cemail">
								<p style="color:red;font-weight:bold;" id="cemail_error"></p>
							</div>
						</div>
						<!--END ROW-->

						<?php
						if ($_SESSION['repairs_submit'] == 'Yes') {
							$pl_disable = '';
						} else {
							$pl_disable = 'readonly="readonly"';
						}
						?>
						<hr />
						<h3 style="text-align:center;">Punch List:</h3>
						<div class="row">
							<div class="col-lg-2">
								<p>Task 1:</p>
							</div>
							<div class="col-lg-10">
								<input type="text" class="form-control" id="task1" name="task1" <? echo $pl_disable; ?> />
							</div>
						</div>
						<!--END ROW-->

						<div class="row">
							<div class="col-lg-2">
								<p>Task 2:</p>
							</div>
							<div class="col-lg-10">
								<input type="text" class="form-control" id="task2" name="task2" <? echo $pl_disable; ?> />
							</div>
						</div>
						<!--END ROW-->

						<div class="row">
							<div class="col-lg-2">
								<p>Task 3:</p>
							</div>
							<div class="col-lg-10">
								<input type="text" class="form-control" id="task3" name="task3" <? echo $pl_disable; ?> />
							</div>
						</div>
						<!--END ROW-->
						<div class="row">
							<div class="col-lg-2">
								<p>Task 4:</p>
							</div>
							<div class="col-lg-10">
								<input type="text" class="form-control" id="task4" name="task4" <? echo $pl_disable; ?> />
							</div>
						</div>
						<!--END ROW-->

						<div class="row">
							<div class="col-lg-2">
								<p>Task 5:</p>
							</div>
							<div class="col-lg-10">
								<input type="text" class="form-control" id="task5" name="task5" <? echo $pl_disable; ?> />
							</div>
						</div>
						<!--END ROW-->

						<div class="row">
							<div class="col-lg-2">
								<p>Task 6:</p>
							</div>
							<div class="col-lg-10">
								<input type="text" class="form-control" id="task6" name="task6" <? echo $pl_disable; ?> />
							</div>
						</div>
						<!--END ROW-->

						<hr />

						<div class="row">
							<div class="col-lg-12" style="text-align:center;">
								<p>Is this customer witholding payment until this repair is completed?</p>
								<select class="form-control" id="urgent" name="urgent">
									<option value="">Select One</option>
									<option value="Yes">Yes</option>
									<option value="No">No</option>
								</select>
								<p style="color:red;font-weight:bold;" id="urgent_error"></p>
							</div>
						</div>
						<!--END ROW-->

						<hr />

						<div class="row" style="height:100px;">
							<div class="col-lg-3">
								<p><?php
									switch ($_SESSION['org_id']) {
										case "558558":
											echo 'United Metals';
											break;
										case "162534":
											echo 'Northedge steel';
											break;
										case "615243":
											echo 'Legacy';
											break;
										default:
											echo 'AllSteel';
									}
									?> Repair Cost:
								<p>
							</div>
							<div class="col-lg-3">
								<div class="input-group input-group-sm">
									<span class="input-group-addon" id="sizing-addon3">$</span>
									<input type="text" class="form-control usd" id="allsteel_cost" name="allsteel_cost" onkeyup="cost_total();" aria-describedby="sizing-addon3" />
								</div>
							</div>

							<div class="col-lg-6" id="img1">
								<p>Image 1</p>
								<input type="file" id="img1" name="img_1" class="form-control" />
							</div>
						</div>
						<!--END ROW-->

						<div class="row" style="height:100px;">
							<div class="col-lg-3">
								<p>Dealer Repair Cost:
								<p>
							</div>
							<div class="col-lg-3">
								<div class="input-group input-group-sm">
									<span class="input-group-addon" id="sizing-addon3">$</span>
									<input type="text" class="form-control usd" id="dealer_cost" name="dealer_cost" onkeyup="cost_total();" aria-describedby="sizing-addon3" />
								</div>
							</div>

							<div class="col-lg-6" id="img2">
								<p>Image 2</p>
								<input type="file" id="img2" name="img_2" class="form-control" />
							</div>
						</div>
						<!--END ROW-->

						<div class="row" style="height:100px;">
							<div class="col-lg-3">
								<p>Contractor Repair Cost:
								<p>
							</div>
							<div class="col-lg-3">
								<div class="input-group input-group-sm">
									<span class="input-group-addon" id="sizing-addon3">$</span>
									<input type="text" class="form-control usd" id="contractor_cost" name="contractor_cost" onkeyup="cost_total();" aria-describedby="sizing-addon3" />
								</div>
							</div>

							<div class="col-lg-6" id="img3">
								<p>Image 3</p>
								<input type="file" id="img3" name="img_3" class="form-control" />
							</div>
						</div>
						<!--END ROW-->

						<div class="row" style="height:100px;">
							<div class="col-lg-3">
								<p>Customer Repair Cost:
								<p>
							</div>
							<div class="col-lg-3">
								<div class="input-group input-group-sm">
									<span class="input-group-addon" id="sizing-addon3">$</span>
									<input type="text" class="form-control usd" id="customer_cost" name="customer_cost" onkeyup="cost_total();" aria-describedby="sizing-addon3" />
								</div>
							</div>

							<div class="col-lg-6" id="img4">
								<p>Image 4</p>
								<input type="file" id="img4" name="img_4" class="form-control" />
							</div>
						</div>
						<!--END ROW-->

						<div class="row" style="height:100px;">
							<div class="col-lg-3">
								<p>Customer Outstanding Balance:
								<p>
							</div>
							<div class="col-lg-3">
								<div class="input-group input-group-sm">
									<span class="input-group-addon" id="sizing-addon3">$</span>
									<input type="text" class="form-control usd" id="customer_balance" name="customer_balance" aria-describedby="sizing-addon3" />
								</div>
							</div>

							<div class="col-lg-6" id="img5">
								<p>Image 5</p>
								<input type="file" id="img5" name="img_5" class="form-control" />
							</div>
						</div>
						<!--END ROW-->


						<div class="row" style="height:100px;">
							<div class="col-lg-3">
								<p><b>Total Cost Of This Repair</b>
								<p>
							</div>
							<div class="col-lg-3">
								<div class="input-group input-group-sm">
									<span class="input-group-addon" id="sizing-addon3">$</span>
									<input type="text" class="form-control usd" id="total_cost" name="total_cost" value="0" aria-describedby="sizing-addon3" />
								</div>
							</div>

						</div>
						<!--END ROW-->
						<p id="sub_by"></p>

						<p style="color:red;font-weight:bold;" id="total_cost_error"></p>
						<input type="hidden" id="rep_name" name="rep_name" value="<? echo $_SESSION['full_name']; ?>" />
						<input type="hidden" id="rep_id" name="rep_id" value="<? echo $_SESSION['user_id']; ?>" />
						<input type="hidden" id="mode" name="mode" value="" />
						<input type="hidden" id="new" name="new" value="" />
						<input type="hidden" id="stat" name="stat" value="" />
						<input type="hidden" id="iid" name="iid" value="" />
					</form>

				</div>
				<div class="modal-footer">
					<p style="color:red;font-weight:bold;" id="error_box"></p>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-warning" id="draft_btn" onclick="add_repair('Draft','False');">Save Draft</button>
					<?php
					if ($_SESSION['repairs_submit'] == 'Yes') {
						echo '<button type="button" class="btn btn-success" id="sub_btn" onclick="add_repair(\'\',\'True\');">Submit</button>';
					}
					?>
				</div>
			</div>

		</div>
	</div>
	<!-- End Modal -->




	<!-- Modal -->
	<div class="modal fade" id="repairNote" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Note For <span id="cust_name" style="color:red;"></span>'s Repair</h4>
				</div>
				<div class="modal-body">
					<p>Please Enter Your Note:</p>
					<textarea class="form-control" name="rnote" id="rnote"></textarea>
					<p id="rnote_error" style="color:red;font-weight:bold;"></p>
					<input type="hidden" id="rcname" value="" />
					<input type="hidden" id="cust_id" value="" />
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success" id="sub_btn" onclick="add_note();">Submit</button>
				</div>
			</div>

		</div>
	</div>
	<!-- End Modal -->


	<!-- Modal -->
	<div class="modal fade" id="incompleteNote" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Note For <span id="icust_name" style="color:red;"></span>'s Repair</h4>
				</div>
				<div class="modal-body">
					<p>Please Enter Your Notes Regarding The Incomplete Repair:</p>
					<textarea class="form-control" name="irnote" id="irnote"></textarea>
					<p id="irnote_error" style="color:red;font-weight:bold;"></p>
					<input type="hidden" id="ircname" value="" />
					<input type="hidden" id="icust_id" value="" />
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success" id="isub_btn" onclick="add_inote();">Submit</button>
				</div>
			</div>

		</div>
	</div>
	<!-- End Modal -->



	<?php include 'footer.html'; ?>
</body>
<script src="repairs/js/email-functions.js"></script>

</html>