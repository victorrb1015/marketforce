<?php
include '../../php/connection.php';

//$cid = $_POST['customer_id'];
//$company_name = mysqli_real_escape_string($conn, $_POST['company_name']);
$sessionname = $_SESSION['full_name'];
$sessionid = $_SESSION['user_id'];
$project = mysqli_real_escape_string($conn, $_POST['pm_project_name']);
$client = mysqli_real_escape_string($conn, $_POST['pm_client_name']);
$tasknumber = mysqli_real_escape_string($conn, $_POST['pm_task_numberl']);
$taskname = mysqli_real_escape_string($conn, $_POST['pm_task_name']); 
$start = mysqli_real_escape_string($conn, $_POST['pm_start_time']);
$resume = mysqli_real_escape_string($conn, $_POST['pm_resume_time']);
$pause = mysqli_real_escape_string($conn, $_POST['pm_pause_time']);
$end = mysqli_real_escape_string($conn, $_POST['pm_end_time']);
//$zip = mysqli_real_escape_string($conn, $_POST['customer_zip']);
//$authUsers = mysqli_real_escape_string($conn, $_POST['authUsers']);
//$notes = mysqli_real_escape_string($conn, $_POST['customer_notes']);
//$mode = $_POST['customer_mode'];
//$monthnumber = date("m", strtotime("today");
//$dayofmonth = date("d", strtotime("today");    
$diff1 = abs($start - $pause);
$diff2 = abs($resume - $end);
$intervala = $diff1 / (60*60);
$intervalb = $diff2 / (60*60);
$intervalc = $intervala + $intervalb;

//Tax Form Uploader...
$newTaxForm = false;
if($_FILES['taxForm']['size'] != 0) {
$newTaxForm = true;
$uid = uniqid();
$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/../img-storage/taxforms/";
$target_file2 = $target_dir . basename($_FILES["taxForm"]["name"]);

$uploadOk = 1;
$imageFileType = pathinfo($target_file2,PATHINFO_EXTENSION);
$target_file = $target_dir . $uid . "_taxForm_" . $idate . "." . $imageFileType;

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
   && $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "pdf" 
   && $imageFileType != "PDF" ) {
    echo "<br>ERROR!- only JPG, JPEG, PNG & GIF files are allowed.";
    //echo "<br><script>alert('" . $imageFileType . "');</script>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "<br>ERROR!- your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["taxForm"]["tmp_name"], $target_file)) {
      
      //INSERT FILE PATH INTO DATABASE
      $file_name_1 = 'http://burtonsolution.tech/img-storage/taxforms/' . $uid . "_taxForm_" . $idate . "." . $imageFileType;
      
        echo "<br>The file '". basename( $_FILES["taxForm"]["name"]). "' has been uploaded.";
        
    }else{
      echo '<br>There was an error moving your file from the temporary location to the permanent location.';
    }
}
}//End File Size Check to see if file exists...







//Add Task...
if($mode == 'New'){
	
	$aq = "INSERT INTO `projects`
			(
			`date`,
			`from_rep_id`,
      `from_rep_name`,
			`project`,
			`client_name`,
			`month_number`,
			`day_of_month`,
      `task_number`,
      `task_name`,
      `start`,
      `pause`,
      `interval_a`,
      `resume`,
      `end`,
			`interval_b`,
			`interval_c`,
			`completed_date`,
			`status`,
			`inactive`
			)
			VALUES
			(
			CURRENT_DATE,
      '" . $sessionid . "',
      '" . $sessionname . "',
			'" . $project . "',
			'" . $client . "',
			'" . $monthnumber . "',
			'" . $dayofmonth . "',
      '" . $tasknumber . "',
      '" . $taskname . "',
      '" . $start . "',
      '" . $pause . "',
      '" . $intervala . "',
      '" . $resume . "',
      '" . $end . "',
			'" . $intervalb . "',
			'" . $intervalc . "',
			CURRENT_DATE,
			'Active',
			'No'
			)";

	mysqli_query($conn, $aq) or die($conn->error);
	echo 'Task Successully Added!';

}

if($mode == 'Edit'){

	$uq = "UPDATE `sales_customers` SET
      `company_name` = '" . $company_name . "',
			`customer_first_name` = '" . $fname . "',
			`customer_last_name` = '" . $lname . "',
			`customer_email` = '" . $email . "',
			`customer_phone` = '" . $phone . "',
     	`customer_phone_2` = '" . $phone2 . "',
      `customer_address` = '" . $address . "',
      `customer_city` = '" . $city . "',
      `customer_state` = '" . $state . "',
      `customer_zip` = '" . $zip . "',
      `auth_users` = '" . $authUsers . "',";
  if($newTaxForm == true){
    $uq .= "`taxform_url` = '" . $file_name_1 . "',";
  }
    $uq .= "`customer_notes` = '" . $notes . "'
			WHERE `ID` = '" . $cid . "'";

	mysqli_query($conn, $uq) or die($conn->error);
	echo '<br>Task Updated Successfully!';

}

echo '<script>
      window.location = "../../index.php";
      </script>';

?>