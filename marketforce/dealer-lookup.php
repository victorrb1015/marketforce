<?php
include 'security/session/session-settings.php';
include 'php/connection.php';
$pageName = '';
switch($_SESSION['org_id']) {
    case "832122":
        $pageName = 'Customer Lookup';
        break;
    case "123456":
    case '532748':
        $pageName = 'Client Lookup';
        break;
    default:
        $pageName = 'Dealer Lookup';
}

$pageIcon = 'fas fa-search';

echo '<script>
        var rep_id = "' . $_SESSION['user_id'] . '";
        var rep_name = "' . $_SESSION['full_name'] . '";';

//if($_SESSION['admin'] == 'Yes'){
if($_SESSION['in'] == 'Yes'){
  $a = 'Yes';
  echo 'var a = "Yes";';
}else{
  $a = 'No';
  echo 'var a = "No";';
}

//If All American Marketing
if($_SESSION['org_id'] == '123456'){
  echo 'var aam = true;';
}else{
  echo 'var aam = false;';
}

echo '</script>';

$cache_buster = uniqid();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>MarketForce | All Steel</title>
  <?php include 'global/sections/head.php'; ?>
  
   <style>
    .badge:hover{
      cursor: pointer;
    }
		.main li{
			float: left;
		}
		.main li li{
			float: none;
		}
		li{
			list-style-type: none;
		}
    .result_table td, th{
      padding: 15px;
      border: 1px solid black;
    }
    .result_table a{
      color: blue;
    }
    .result_table tr:hover{
      background: grey;
    }
    .image_gallery img{
      padding: 5px;
    }
     #dealerAppModal small{
       color:red;
       font-size: 10px;
     }
     #da_form col-lg-12{
       margin-bottom: 5px !Important;
     }
		
		<?php
		if($_SESSION['dealer_activation'] != 'Yes'){
			echo '#hider-th{
				visibility: hidden;
			}
			#hider-td{
				visibility: hidden;
			}';
		}
		?>
  </style>
  <?php //include 'dealer-lookup/js/main-functions.php'; ?>
</head>

<body>
  <!-- Preloader -->
  <?php include 'global/sections/preloader.php'; ?>
  <!-- /Preloader -->
  <div class="wrapper theme-4-active pimary-color-red">

    <!--Navigation-->
    <?php include 'global/sections/nav.php'; ?>


    <!-- Main Content -->
    <div class="page-wrapper">
      <!--Includes Footer-->

      <div class="container-fluid pt-25">
        <?php include 'global/sections/page-title-bar.php'; ?>

        <!--Main Content Here-->
        <?php include 'dealer-lookup/sections/search-bar.php'; ?>
        
        
        

        <!-- Footer -->
        <?php include 'global/sections/footer.php'; ?>
        <!-- /Footer -->

      </div>
      <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->

<!--JQuery Files-->
  <script src="jquery/external/jquery/jquery.js"></script>
  <script src="jquery/jquery-ui.js"></script>
  <script>
   $(document).ready(function(){
      $("#dealer_select").change(function(){

        $( "#accordion" ).accordion({
          collapsible: true
        });
      });
   });
</script>
    <!--Footer-->
    <?php include 'global/sections/includes.php'; ?>
</body>
<!-- JS Files -->
<script src="lookup/js/lookup-functions.js?cb=<?php echo $cache_buster; ?>"></script>
<script src="dealer-lookup/js/main-functions.js?cb=<?php echo $cache_buster; ?>"></script>
<script src="dealer-lookup/js/dealer-app-functions.js?cb=<?php echo $cache_buster; ?>"></script>
  
<!--Modals-->
<?php 
  if($_SESSION['org_id'] == '123456'){
    include 'dealer-lookup/modals/client-app-modal.php';
  }else{
    include 'dealer-lookup/modals/dealer-app-modal.php';
  }
  ?>
<?php include 'dealer-lookup/modals/internal-note-modal.php'; ?>
<?php include 'lookup/modals/reassign-rep-modal.php'; ?>

</html>