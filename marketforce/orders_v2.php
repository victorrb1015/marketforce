<?php
include 'security/session/session-settings.php';
include 'php/connection.php';
$pageName = 'Orders';
$pageIcon = 'fas fa-file-invoice-dollar';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>MarketForce | Orders</title>
    <?php include 'global/sections/head.php'; ?>
</head>
<body>
<!-- Preloader -->
<?php include 'global/sections/preloader.php'; ?>
<!-- /Preloader -->
<div class="wrapper theme-4-active pimary-color-red">
    <!--Navigation-->
    <?php include 'global/sections/nav.php'; ?>
    <!-- Main Content -->
    <div class="page-wrapper"><!--Includes Footer-->
        <div class="container-fluid pt-25">
            <?php include 'global/sections/page-title-bar.php'; ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary card-view" style="background: transparent;">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                MarketForce Orders
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <!--Main Content Here-->
            <iframe
                width="100%"
                height="100%"
                style="min-heigth:900px"
                src="<?php
                if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
                    echo 'https';
                }else{
                    echo 'http';
                }
                ?>://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/orders_v2/main.php">
            </iframe>
        </div>
        <!-- Footer -->
        <?php include 'global/sections/footer.php'; ?>
        <!-- /Footer -->
    </div>
    <!-- /Main Content -->
</div>
<!-- /#wrapper -->
<!--Footer-->
<?php include 'global/sections/includes.php'; ?>

</body>
</html>

