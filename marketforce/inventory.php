<?php
include 'security/session/session-settings.php';
include 'php/connection.php';
$pageName = 'Inventory';
$pageIcon = 'fas fa-boxes';

if($_SESSION['inventory'] == 'Yes'){
  $contentEditable = 'contenteditable';
}else{
  $contentEditable = '';
}

if($_SESSION['position'] == 'Inventory Consignment User'){
	echo '<script>
			var sessionPosition = "ICU";
		  </script>';
}else{
	echo '<script>
			var sessionPosition = "";
		  </script>';
}

//Cache Buster...
$cache_code = uniqid();
echo '<script>
        var rep_id = "' . $_SESSION['user_id'] . '";
        var rep_name = "' . $_SESSION['full_name'] . '";
      </script>';

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>MarketForce | All Steel</title>
    <?php include 'global/sections/head.php'; ?>
  <!--<script src="//labelwriter.com/software/dls/sdk/js/DYMO.Label.Framework.latest.js" type="text/javascript" charset="UTF-8"></script>-->
  <script src="inventory/js/DYMO.Label.Framework.Latest.js" type="text/javascript" charset="UTF-8"></script>
  <style>
    .dataTables_filter {
       float: left !important;
    }
  </style>
</head>
DYMO.Label.Framework.Latest.js
<body>
	<!-- Preloader -->
	<?php include 'global/sections/preloader.php'; ?>
	<!-- /Preloader -->
    <div class="wrapper theme-4-active pimary-color-red">

    	<!--Navigation-->
    	<?php include 'global/sections/nav.php'; ?>
		
		
        <!-- Main Content -->
		<div class="page-wrapper"><!--Includes Footer-->

      <div class="container-fluid pt-25">
				<?php include 'global/sections/page-title-bar.php'; ?>
        
        <!--Main Content Here-->

		<!--Inventory Tabs-->
        <div class="tab-content" id="mySubTabContent">
          <ul class="nav nav-tabs">
            <li class="nav-item active"><a data-toggle="tab" href="#raw" style="color:#DC0031;"><span class="glyphicon glyphicon-menu-hamburger"></span> Raw Materials</a></li>
            <li class="nav-item"><a data-toggle="tab" href="#components" style="color:#DC0031;"><span class="glyphicon glyphicon-th-large"></span> Components</a></li>
            <li class="nav-item"><a data-toggle="tab" href="#products" style="color:#DC0031;"><span class="glyphicon glyphicon-barcode"></span> Products</a></li>
            <li class="nav-item"><a data-toggle="tab" href="#coils" style="color:#DC0031;"><span class="glyphicon glyphicon-oil"></span> Coils</a></li>
            <?php
            /*
            if($_SESSION['org_id'] == '654321'){
            echo '<li class="nav-item"><a data-toggle="tab" href="#mxtab" style="color:green;"><span class="glyphicon"></span> México</a></li>';
            }
            else{
            echo '<li class="nav-item"><a data-toggle="tab" href="#intab" style="color:orange;"><span class="glyphicon"></span> Indiana</a></li>
                  <li class="nav-item"><a data-toggle="tab" href="#nctab" style="color:lightgreen;"><span class="glyphicon"></span> North Carolina</a></li>
                  <li class="nav-item"><a data-toggle="tab" href="#oktab" style="color:peachpuff;"><span class="glyphicon"></span> Oklahoma</a></li>
                  <li class="nav-item"><a data-toggle="tab" href="#gatab" style="color:cyan;"><span class="glyphicon"></span> Georgia</a></li>
                  <li class="nav-item"><a data-toggle="tab" href="#catab" style="color:yellow;"><span class="glyphicon"></span> California</a></li>
                  <li class="nav-item"><a data-toggle="tab" href="#txtab" style="color:magenta;"><span class="glyphicon"></span> Texas</a></li>';
            }
            */
            ?>
          </ul> 

    <!-- Raw Materials Tab -->
    <div class="tab-pane active in" id="raw" role="tabpanel">
     	<?php include 'inventory/sections/raw-inv-table.php'; ?>  
		</div>

		<!-- Components Tab -->
		<div class="tab-pane" id="components" role="tabpanel">
      <?php include 'inventory/sections/component-inv-table.php'; ?>
		</div>

		<!-- Products Tab -->
		<div class="tab-pane" id="products" role="tabpanel">
    	<?php include 'inventory/sections/products-inv-table.php'; ?>
		</div>

		<!-- Coils Tab -->
		<div class="tab-pane" id="coils" role="tabpanel">
    	<?php include 'inventory/sections/coils-inv-table.php'; ?>
		</div>
          
    <!-- IN Materials Tab -->
    <!-- <div class="tab-pane" id="intab" role="tabpanel">
     	<?php //include 'inventory/sections/in-inv-table.php'; ?>
		</div> -->
          
    <!-- NC Materials Tab -->
   <!--  <div class="tab-pane" id="nctab" role="tabpanel">
     	<?php //include 'inventory/sections/nc-inv-table.php'; ?>  
		</div> -->
          
    <!-- OK Materials Tab -->
    <!-- <div class="tab-pane" id="oktab" role="tabpanel">
     	<?php //include 'inventory/sections/ok-inv-table.php'; ?>  
		</div> -->
          
    <!-- CA Materials Tab -->
    <!-- <div class="tab-pane" id="catab" role="tabpanel">
     	<?php //include 'inventory/sections/ca-inv-table.php'; ?>  
		</div> -->
          
    <!-- GA Materials Tab -->
    <!-- <div class="tab-pane" id="gatab" role="tabpanel">
     	<?php //include 'inventory/sections/ga-inv-table.php'; ?>  
		</div> -->

    <!-- TX Materials Tab -->
    <!-- <div class="tab-pane" id="txtab" role="tabpanel">
     	<?php //include 'inventory/sections/tx-inv-table.php'; ?>  
		</div> -->
          
    <!-- MX Materials Tab -->
    <div class="tab-pane" id="mxtab" role="tabpanel">
     	<?php //include 'inventory/sections/mx-inv-table.php'; ?>  
		</div>
      
          
			<!-- Footer -->
			<?php include 'global/sections/footer.php'; ?>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!--Footer-->
	<?php include 'global/sections/includes.php'; ?>
</body>
			<!-- JS Files -->
			<script src="inventory/js/item-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="inventory/js/barcode-print-functions.js?cb=<?php echo $cache_code; ?>"></script>
			<script src="inventory/js/convert-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="orders/js/color-functions.js?cb=<?php echo $cache_code; ?>"></script>
			<!--Modals-->
			<?php include 'inventory/modals/add-edit-item-modal.php'; ?>
      <?php include 'orders/modals/colors-modal.php'; ?>
      
  <!-- jQuery Datatables -->
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script>
    $("#raw_table").dataTable({
      "paging": false,
    });
    $("#components_table").dataTable({
      "paging": false,
    });
    $("#products_table").dataTable({
      "paging": false,
    });
    $("#coils_table").dataTable({
      "paging": false,
    });
  </script>

</html>
