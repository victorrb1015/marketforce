<?php
include '../../php/connection.php';

//Load Variables
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];
$cbiz = mysqli_real_escape_string($conn, $_GET['cbiz']);
$caddress = mysqli_real_escape_string($conn, $_GET['caddress']);
$ccity = mysqli_real_escape_string($conn, $_GET['ccity']);
$cstate = mysqli_real_escape_string($conn, $_GET['cstate']);
$czip = mysqli_real_escape_string($conn, $_GET['czip']);
$cname = mysqli_real_escape_string($conn, $_GET['cname']);
$cemail = mysqli_real_escape_string($conn, $_GET['cemail']);
$cphone = mysqli_real_escape_string($conn, $_GET['cphone']);
$cinfo = mysqli_real_escape_string($conn, $_GET['cinfo']);

//Generate a unique Prospect ID
$PID = 'CCP' . uniqid();

//INSERT INTO Prospect Database...
$q = "INSERT INTO `prospects`
      (
      `PID`,
      `date`,
      `time`,
      `rep_id`,
      `rep_name`,
      `bizname`,
      `address`,
      `city`,
      `state`,
      `zip`,
      `owner_name`,
      `initial_notes`,
      `email`,
      `phone`,
      `converted`,
      `inactive`
      )
      VALUES
      (
      '" . $PID . "',
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $rep_id . "',
      '" . $rep_name . "',
      '" . $cbiz . "',
      '" . $caddress . "',
      '" . $ccity . "',
      '" . $cstate . "',
      '" . $czip . "',
      '" . $cname . "',
      '" . $cinfo . "',
      '" . $cemail . "',
      '" . $cphone . "',
      'No',
      'No'
      )";

mysqli_query($conn, $q) or die($conn->error);

echo $cbiz . ' was added as a new prospect!';

?>