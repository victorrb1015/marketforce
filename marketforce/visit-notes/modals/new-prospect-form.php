<div class="modal fade" role="dialog" tabindex="-1" id="newProspect">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">New Prospect Form</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Business Information</h4>
                            <p class="text-primary">Name of Business:</p><input class="form-control" type="text" placeholder="Business Name" id="pbiz" style="width:100%;" name="pbiz"></div>
                    </div>
                    <div class="row Address" id="Address">
                        <div class="col-md-12">
                            <p class="text-primary">Address:</p><input class="form-control" type="text" placeholder="Address" id="paddress" style="width:100%;" name="paddress"></div>
                        <div class="col-md-4">
                            <p class="text-primary">City:</p><input class="form-control" type="text" placeholder="City" id="pcity" style="width:100%;" name="pcity"></div>
                        <div class="col-md-4">
                            <p class="text-primary">State:</p>
                            <select class="form-control" id="pstate" style="width:100%;" name="pstate" disabled>
                              <option value="">State</option>
                              <?
                              $sq = "SELECT * FROM `states`";
                              $sg = mysqli_query($conn, $sq) or die($conn->error);
                              while($sr = mysqli_fetch_array($sg)){
                                if($sr['state'] == $_GET['state']){$selected = 'selected';}else{$selected = '';}
                                echo '<option value="' . $sr['state'] . '" ' . $selected . '>' . $sr['state'] . '</option>';
                              }
                              ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <p class="text-primary">Zip Code:</p><input class="form-control" type="text" placeholder="Zip Code" id="pzip" style="width:100%;" name="pzip"></div>
                    </div>
                    <div class="row" style="background-color:#ffffff;">
                        <div class="col-md-12"><br></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="/*background:rgba(51,122,183,0.2);*/">
                            <hr>
                            <h4>Owner/Contact Information</h4>
                            <p class="text-primary">Owner/Contact Name:</p><input class="form-control" type="text" placeholder="Name" id="pname" style="width:100%;" name="pname"><br>
                            <p class="text-primary">Email Address:</p><input class="form-control" type="text" placeholder="Email" id="pdealer_email" style="width:100%;" name="pdealer_email"><br>
                            <p class="text-primary">Phone Number:</p><input class="form-control phone" type="text" placeholder="Phone Number" id="pdealer_phone" style="width:100%;" name="pdealer_phone"><br>
                            <p class="text-primary">Notes regarding this Prospect:</p><textarea class="form-control" placeholder="This would be a great new dealer because..." id="pinfo" style="width:100%;" name="pinfo"></textarea>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                  <p id="prospect_response" style="color:red;font-weight:bold;"></p>
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" type="button" id="npb" onclick="new_prospect();">Save</button>
                </div>
            </div>
        </div>
    </div>