<div class="modal fade" role="dialog" tabindex="-1" id="visitNoteForm">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <h2 class="text-center modal-title">Visit Note For <span id="fdName" style="color:blue;"></span></h2>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4>Basic Information</h4>
                            <p class="text-primary">With whom did you meet?</p>
                            <input class="form-control" type="text" placeholder="John Smith" id="name" name="name">
                            <p class="text-primary">Notes from your visit today:</p>
                            <textarea id="vnote" name="vnote" class="form-control" placeholder="Met with John today about the new..." style="height:150px;"></textarea>
                            <!--<div style="text-align:right;"><button class="btn btn-primary" type="button">View Previous Notes</button></div>-->
                        </div>
                        <div class="col-lg-6">
                            <h4>Dealer Contact Information</h4>
                            <p class="text-primary">Email Address:</p>
                            <input class="form-control" type="text" placeholder="Email" id="dealer_email" name="dealer_email">
                            <div class="checkbox"><label><input type="checkbox" id="email_cb" name="email_cb">Verified No Email Address</label></div>
                            <p class="text-primary">Fax Number:</p><input class="form-control" type="text" placeholder="Fax" id="dealer_fax" name="dealer_fax">
                            <div class="checkbox"><label><input type="checkbox" id="fax_cb" name="fax_cb">Verified No Fax Number</label></div>
                            <p class="text-primary">Phone Number:</p><input class="form-control" type="text" placeholder="Cell" id="dealer_cell" name="dealer_cell">
                            <div class="checkbox"><label><input type="checkbox" id="dcell_cb" name="dcell_cb">Verified No Phone Number</label></div>
                        </div>
                    </div>
                    <div class="row" style="background-color:#ffffff;">
                        <div class="col-md-12"><br></div>
                    </div>
                    <!--<div class="row" style="height:251px;">-->
                    <div class="row">
                        <div class="col-lg-6 list-box-custom" id="form-box">
                            <h4 class="text-center bg-primary">Price Training</h4>
                            <p><input type="checkbox" id="pb1" name="pb1" value="Review 795/895 Pricing">&nbsp;Review 795/895 Pricing&nbsp;</p>
                            <p><input type="checkbox" id="pb2" name="pb2" value="Dealer Book Updated">&nbsp;<strong>Dealer Book Updated</strong></p>
                            <p><input type="checkbox" id="pb6" name="pb6" value="Revised Warranty TRAINED">&nbsp;Revised Warranty TRAINED&nbsp;</p>
                            <p><input type="checkbox" id="pb7" name="pb7" value="Warranty Handout - Make Copies">&nbsp;Warranty Handout - Make Copies&nbsp;</p>
                            <p><input type="checkbox" id="pb8" name="pb8" value="Angled and Concrete Support">&nbsp;Angled &amp; Concrete Support</p>
                            <p><input type="checkbox" id="pb9" name="pb9" value="New HIGHLIGHTED Order Forms">&nbsp;New HIGHLIGHTED Order Forms&nbsp;</p>
                        </div>
                        <div class="col-lg-6 list-box-custom" id="form-box">
                            <h4 class="text-center bg-primary">Waivers &amp; Forms</h4>
                            <p><input type="checkbox" id="cb1" name="cb1" value="Build Over">&nbsp;Build Over</p>
                            <p><input type="checkbox" id="cb2" name="cb2" value="Vertical Metal">&nbsp;Vertical Metal&nbsp;</p>
                            <p><input type="checkbox" id="pb7" name="pb7" value="Warranty Handout - Make Copies">&nbsp;Warranty Handout - Make Copies&nbsp;</p>
                            <p><input type="checkbox" id="cb3" name="cb3" value="Door Placement">&nbsp;Door Placement&nbsp;</p>
                            <p><input type="checkbox" id="cb4" name="cb4" value="Survey Form">&nbsp;Survey Form&nbsp;</p>
                            <p><input type="checkbox" id="cb5" name="cb5" value="Lean To Waiver">&nbsp;Lean To Waiver&nbsp;</p>
                            <p><input type="checkbox" id="cb6" name="cb6" value="ASC Welcome Letter">&nbsp;ASC Welcome Letter&nbsp;</p>
                        </div>
                      
                    <!--</div>
                    <div class="row" style="height:193px;">-->
                      
                        <div class="col-lg-6 list-box-custom" id="form-box">
                            <h4 class="text-center bg-primary">Highlighted Area:</h4>
                            <p><input type="checkbox" id="cb7" name="cb7" value="Roof and Length">&nbsp;Roof &amp; Length</p>
                            <p><input type="checkbox" id="cb8" name="cb8" value="Permit">&nbsp;Permit</p>
                            <p><input type="checkbox" id="cb9" name="cb9" value="Level Lot">&nbsp;Level Lot</p>
                            <p><input type="checkbox" id="cb10" name="cb10" value="Underground Lines">&nbsp;Underground Lines&nbsp;</p>
                            <p><input type="checkbox" id="cb11" name="cb11" value="Installer">&nbsp;Installer</p>
                        </div>
                        <div class="col-lg-6 list-box-custom" id="form-box">
                            <h4 class="text-center bg-primary">Supplies:</h4>
                            <p><input type="checkbox" id="cb12" name="cb12" value="Order Form">&nbsp;Order Form</p>
                            <p><input type="checkbox" id="cb13" name="cb13" value="Brochures">&nbsp;Brochures</p>
                            <p><input type="checkbox" id="cb14" name="cb14" value="Sales Flyers">&nbsp;Sales Flyers</p>
                            <p><input type="checkbox" id="cb15" name="cb15" value="Catalog">&nbsp;Catalog</p>
                        </div>
                      
                    <!--</div>
                    <div class="row" style="height:245px;">-->
                      
                        <div class="col-lg-6 list-box-custom" id="form-box">
                            <h4 class="text-center bg-primary">Dealer Login:</h4>
                            <p><input type="checkbox" id="cb16" name="cb16" value="Dealer ID">&nbsp;Dealer ID</p>
                            <p><input type="checkbox" id="cb17" name="cb17" value="Login Demo">&nbsp;Login Demo</p>
                            <p><input type="checkbox" id="cb18" name="cb18" value="Store Demo">&nbsp;Store Demo</p>
                        </div>
                        <div class="col-lg-6 list-box-custom" id="form-box">
                            <h4 class="text-center bg-primary">Tasks:</h4>
                            <p><input type="checkbox" id="cb19" name="cb19" value="Dealer Signs">&nbsp;Dealer Signs</p>
                            <p><input type="checkbox" id="cb20" name="cb20" value="Repairs">&nbsp;Repairs</p>
                            <p style="color:rgb(255,0,0);"><input type="checkbox" id="cb21" name="cb21" value="Verify Email">&nbsp;<strong>Verify Email</strong></p>
                            <p><input type="checkbox" id="cb22" name="cb22" value="Contacts">&nbsp;Contacts</p>
                            <p><input type="checkbox" id="cb23" name="cb23" value="Color Samples">&nbsp;Color Samples</p>
                            <p style="color:rgb(255,0,0);"><input type="checkbox" id="cb24" name="cb24" value="Verified Mailing Address">&nbsp;<strong>Verified Mailing Address</strong>&nbsp;</p>
                            <p><input type="checkbox" id="cb25" name="cb25" value="Verified Displays">&nbsp;<strong>Verified Displays</strong>&nbsp;</p>
                        </div>
                    </div>
                    <div class="col-lg-12"><br>
                        <hr>
                        <h3 class="text-center">Daily Hours</h3>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr></tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="info">Day</td>
                                        <td class="info">Open</td>
                                        <td class="info">Close</td>
                                        <td class="info">Not Open</td>
                                    </tr>
                                    <tr>
                                        <td>Sunday</td>
                                        <td class="active"><input class="form-control" type="text" id="sun_open" style="width:75%;" name="sun_open"></td>
                                        <td class="active"><input class="form-control" type="text" id="sun_close" style="width:75%;" name="sun_close"></td>
                                        <td><input type="checkbox" id="sun_not_open" name="sun_not_open" onchange="hour_disable('sun');"></td>
                                    </tr>
                                    <tr>
                                        <td>Monday</td>
                                        <td class="active"><input class="form-control" type="text" placeholder="9" id="mon_open" style="width:75%;" name="mon_open"></td>
                                        <td class="active"><input class="form-control" type="text" placeholder="5" id="mon_close" style="width:75%;" name="mon_close"></td>
                                        <td><input type="checkbox" id="mon_not_open" name="mon_not_open" onchange="hour_disable('mon');"></td>
                                    </tr>
                                    <tr>
                                        <td>Tuesday</td>
                                        <td class="active"><input class="form-control" type="text" placeholder="9" id="tue_open" style="width:75%;" name="tue_open"></td>
                                        <td class="active"><input class="form-control" type="text" placeholder="5" id="tue_close" style="width:75%;" name="tue_close"></td>
                                        <td><input type="checkbox" id="tue_not_open" name="tue_not_open" onchange="hour_disable('tue');"></td>
                                    </tr>
                                    <tr>
                                        <td>Wednesday</td>
                                        <td class="active"><input class="form-control" type="text" placeholder="9" id="wed_open" style="width:75%;" name="tue_open"></td>
                                        <td class="active"><input class="form-control" type="text" placeholder="5" id="wed_close" style="width:75%;" name="wed_close"></td>
                                        <td><input type="checkbox" id="wed_not_open" name="wed_not_open" onchange="hour_disable('wed');"></td>
                                    </tr>
                                    <tr>
                                        <td>Thursday</td>
                                        <td class="active"><input class="form-control" type="text" placeholder="9" id="thu_open" style="width:75%;" name="thu_open"></td>
                                        <td class="active"><input class="form-control" type="text" placeholder="5" id="thu_close" style="width:75%;" name="thu_close"></td>
                                        <td><input type="checkbox" id="thu_not_open" name="thu_not_open" onchange="hour_disable('thu');"></td>
                                    </tr>
                                    <tr>
                                        <td>Friday</td>
                                        <td class="active"><input class="form-control" type="text" placeholder="9" id="fri_open" style="width:75%;" name="fri_open"></td>
                                        <td class="active"><input class="form-control" type="text" placeholder="5" id="fri_close" style="width:75%;" name="fri_close"></td>
                                        <td><input type="checkbox" id="fri_not_open" name="fri_not_open" onchange="hour_disable('fri');"></td>
                                    </tr>
                                    <tr>
                                        <td>Saturday</td>
                                        <td class="active"><input class="form-control" type="text" placeholder="9" id="sat_open" style="width:75%;" name="sat_open"></td>
                                        <td class="active"><input class="form-control" type="text" placeholder="5" id="sat_close" style="width:75%;" name="sat_close"></td>
                                        <td><input type="checkbox" id="sat_not_open" name="sat_not_open" onchange="hour_disable('sat');"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                            <h3 class="text-center">Addresses</h3>
                        </div>
                        <div class="col-md-4" style="border-right:1px solid black;border-top:2px solid black;">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Physical</h4>
                                </div>
                                <div class="col-md-12">
                                    <p class="text-primary">Address:</p><input class="form-control" type="text" placeholder="Address" id="address1_physical" style="width:100%;" name="address1_physical"></div>  
                              <div class="col-md-12">
                                    <p class="text-primary">City:</p><input class="form-control" type="text" placeholder="City" id="city1_physical" style="width:100%;" name="city1_physical"></div>
                                <div class="col-md-6">
                                    <p class="text-primary">State:</p>
                                    <select class="form-control" id="state1_physical" style="width:100%;" name="state1_physical">
                                      <option value="">State</option>
                                      <?
                                      $sq = "SELECT * FROM `states` ORDER BY `state` ASC";
                                      $sg = mysqli_query($conn, $sq) or die($conn->error);
                                      while($sr = mysqli_fetch_array($sg)){
                                        echo '<option value="' . $sr['state'] . '">' . $sr['state'] . '</option>';
                                      }
                                      ?>
                                  </select>
                                </div>
                                <div class="col-md-6">
                                    <p class="text-primary">Zip Code:</p><input class="form-control" type="text" placeholder="Zip" id="zip1_physical" style="width:100%;" name="zip1_physical"></div>
                            </div>
                        </div>
                        <div class="col-md-4" style="border-right:1px solid black;border-left:1px solid black;border-top:2px solid black;">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Mailing</h4>
                                </div>
                                <div class="col-md-12">
                                    <p class="text-primary">Address:</p><input class="form-control" type="text" placeholder="Address" id="address2_mailing" style="width:100%;" name="address2_mailing"></div>  
                              <div class="col-md-12">
                                    <p class="text-primary">City:</p><input class="form-control" type="text" placeholder="City" id="city2_mailing" style="width:100%;" name="city2_mailing"></div>
                                <div class="col-md-6">
                                    <p class="text-primary">State:</p>
                                    <select class="form-control" id="state2_mailing" style="width:100%;" name="state2_mailing">
                                      <option value="">State</option>
                                      <?
                                      $sq = "SELECT * FROM `states` ORDER BY `state` ASC";
                                      $sg = mysqli_query($conn, $sq) or die($conn->error);
                                      while($sr = mysqli_fetch_array($sg)){
                                        echo '<option value="' . $sr['state'] . '">' . $sr['state'] . '</option>';
                                      }
                                      ?>
                                  </select>
                                </div>
                                <div class="col-md-6">
                                    <p class="text-primary">Zip Code:</p><input class="form-control" type="text" placeholder="Zip" id="zip2_mailing" style="width:100%;" name="zip2_mailing"></div>
                            </div>
                        </div>
                        <div class="col-md-4" style="border-left:1px solid black;border-top:2px solid black;">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Shipping</h4>
                                </div>
                                <div class="col-md-12">
                                    <p class="text-primary">Address:</p><input class="form-control" type="text" placeholder="Address" id="address3_shipping" style="width:100%;" name="address3_shipping"></div>  
                              <div class="col-md-12">
                                    <p class="text-primary">City:</p><input class="form-control" type="text" placeholder="City" id="city3_shipping" style="width:100%;" name="city3_shipping"></div>
                                <div class="col-md-6">
                                    <p class="text-primary">State:</p>
                                    <select class="form-control" id="state3_shipping" style="width:100%;" name="state3_shipping">
                                      <option value="">State</option>
                                      <?
                                      $sq = "SELECT * FROM `states` ORDER BY `state` ASC";
                                      $sg = mysqli_query($conn, $sq) or die($conn->error);
                                      while($sr = mysqli_fetch_array($sg)){
                                        echo '<option value="' . $sr['state'] . '">' . $sr['state'] . '</option>';
                                      }
                                      ?>
                                  </select>
                                </div>
                                <div class="col-md-6">
                                    <p class="text-primary">Zip Code:</p><input class="form-control" type="text" placeholder="Zip" id="zip3_shipping" style="width:100%;" name="zip3_shipping"></div>
                            </div>
                        </div>
                    </div><br>
                    <hr>
                    <div class="row">
                        <div class="col-lg-6">
                            <h3>Type of Visit</h3>
                            <div class="radio"><label><input type="radio" id="Check-In" name="vtype" value="Check-In">Regular Check-In</label></div>
                            <div class="radio"><label><input type="radio" id="Training" name="vtype" value="Training">Training Visit</label></div>
                            <div class="radio"><label><input type="radio" id="Internal" name="vtype" value="Internal Note">Internal Note</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="unman" name="unman"><strong>This Dealer Location is an UNMANNED LOT</strong></label></div>
                            <div class="checkbox"><label><input type="checkbox" id="signs" name="signs" value="Done"><strong>This Dealer has updated signs</strong></label></div><br></div>
                        <div class="col-lg-6">
                            <h3>Communication Preferences</h3>
                            <div class="checkbox"><label><input type="checkbox" id="comm_phone" name="comm_phone" value="Yes">Phone</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="comm_fax" name="comm_fax" value="Yes">Fax</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="comm_email" name="comm_email" value="Yes">Email</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="comm_usmail" name="comm_usmail" value="Yes">US Mail</label></div><br><br><br></div>
                    </div>
                  <input type="hidden" id="idNum" name="idNum" value="" />
                  <input type="hidden" id="vnStatus" name="vnStatus" value="" />
                </div>
                <div class="modal-footer">
                  <p id="response" style="color:red;font-weight:bold;"></p>
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" type="button" onclick="add_note();">Add Note</button>
                </div>
            </div>
        </div>
    </div>