<div class="modal fade" role="dialog" tabindex="-1" id="previousNotes">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h2 class="text-center modal-title">Previous Visit Notes For: <span style="color:blue;" id="prev-note-dname"></span></h2>
                </div>
                <div class="modal-body" id="prev-notes-container">
                    
                </div>
                <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Close</button><button class="btn btn-primary" type="button">Save</button></div>
            </div>
        </div>
    </div>