    <div class="modal fade" role="dialog" tabindex="-1" id="visitNoteForm">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h2 class="modal-title text-center">Visit Notes</h2>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-primary" style="height: 358px;">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Basic Information</h3>
                                </div>
                                <div class="panel-body">
                                    <div>
                                        <div style="/*background:rgba(51,122,183,0.2);*/">
                                            <p class="text-primary">With whom did you meet?</p><input type="text" id="683_name" style="width:100%;" name="683_name" placeholder="John Smith">
                                            <p class="text-primary">Notes from your visit today:</p><textarea id="683_notes" style="width:100%;height:150px;" name="683_notes" placeholder="Met with John today on the new..."></textarea>
                                            <div style="text-align:right;"><button class="btn btn-primary" type="button">View Previous Notes</button></div>
                                        </div>
                                    </div>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-primary" style="height: 358px;">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Type of Visit</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div>
                                                <div style="height:auto;background-color:#ffffff;">
                                                    <div class="radio"><label><input type="radio" id="Check-In" name="Check-In">Regular Check-In</label></div>
                                                    <div class="radio"><label><input type="radio" id="Training" name="Training">Training Visit</label></div>
                                                    <div class="radio"><label><input type="radio" id="Internal" name="Internal">Internal Note</label></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title text-center">Task Lists</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-6 col-sm-6" id="form-box" style="height:251px;">
                                        <h4 class="text-center bg-primary">Price Training</h4>
                                        <p><input type="checkbox" id="683_pb1" name="683_pb1">&nbsp;Review 795/895 Pricing&nbsp;</p>
                                        <p><input type="checkbox" id="683_pb2" name="683_pb2">&nbsp;<strong>Dealer Book Updated</strong></p>
                                        <p><input type="checkbox" id="683_pb6" name="683_pb6">&nbsp;Revised Warranty TRAINED&nbsp;</p>
                                        <p><input type="checkbox" id="683_pb7" name="683_pb7">&nbsp;Warranty Handout - Make Copies&nbsp;</p>
                                        <p><input type="checkbox" id="683_pb8" name="683_pb8">&nbsp;Angled &amp; Concrete Support</p>
                                        <p><input type="checkbox" id="683_pb9" name="683_pb9">&nbsp;New HIGHLIGHTED Order Forms&nbsp;</p>
                                    </div>
                                    <div class="col-md-6" id="form-box" style="height:251px;">
                                        <h4 class="text-center bg-primary">Waivers &amp; Forms</h4>
                                        <p><input type="checkbox" id="683_cb1" name="683_cb1">&nbsp;Build Over</p>
                                        <p><input type="checkbox" id="683_cb2" name="683_cb2">&nbsp;Vertical Metal&nbsp;</p>
                                        <p><input type="checkbox" id="683_pb7" name="683_pb7">&nbsp;Warranty Handout - Make Copies&nbsp;</p>
                                        <p><input type="checkbox" id="683_cb3" name="683_cb3">&nbsp;Door Placement&nbsp;</p>
                                        <p><input type="checkbox" id="683_cb4" name="683_cb4">&nbsp;Survey Form&nbsp;</p>
                                        <p><input type="checkbox" id="683_cb5" name="683_cb5">&nbsp;Lean To Waiver&nbsp;</p>
                                        <p><input type="checkbox" id="683_cb6" name="683_cb6">&nbsp;ASC Welcome Letter&nbsp;</p>
                                    </div>
                                    <div class="col-md-6" id="form-box">
                                        <h4 class="text-center bg-primary">Highlighted Area:</h4>
                                        <p><input type="checkbox" id="683_cb7" name="683_cb7">&nbsp;Roof &amp; Length</p>
                                        <p><input type="checkbox" id="683_cb7" name="683_cb7">&nbsp;Permit</p>
                                        <p><input type="checkbox" id="683_cb9" name="683_cb9">&nbsp;Level Lot</p>
                                        <p><input type="checkbox" id="683_cb10" name="683_cb10">&nbsp;Underground Lines&nbsp;</p>
                                        <p><input type="checkbox" id="683_cb11" name="683_cb11">&nbsp;Installer</p>
                                    </div>
                                    <div class="col-md-6" id="form-box" style="height:193px;">
                                        <h4 class="text-center bg-primary">Supplies:</h4>
                                        <p><input type="checkbox" id="683_cb12" name="683_cb12">&nbsp;Order Form</p>
                                        <p><input type="checkbox" id="683_cb13" name="683_cb13">&nbsp;Brochures</p>
                                        <p><input type="checkbox" id="683_cb14" name="683_cb14">&nbsp;Sales Flyers</p>
                                        <p><input type="checkbox" id="683_cb15" name="683_cb15">&nbsp;Catalog</p>
                                    </div>
                                    <div class="col-md-6" id="form-box" style="height:253px;">
                                        <h4 class="text-center bg-primary">Dealer Login:</h4>
                                        <p><input type="checkbox" id="683_cb16" name="683_cb16">&nbsp;Dealer ID</p>
                                        <p><input type="checkbox" id="683_cb17" name="683_cb17">&nbsp;Login Demo</p>
                                        <p><input type="checkbox" id="683_cb18" name="683_cb18">&nbsp;Store Demo</p>
                                    </div>
                                    <div class="col-md-6" id="form-box">
                                        <h4 class="text-center bg-primary">Tasks:</h4>
                                        <p><input type="checkbox" id="683_cb19" name="683_cb19">&nbsp;Dealer Signs</p>
                                        <p><input type="checkbox" id="683_cb20" name="683_cb20">&nbsp;Repairs</p>
                                        <p style="color:rgb(255,0,0);"><input type="checkbox" id="683_cb21" name="683_cb21">&nbsp;<strong>Verify Email</strong></p>
                                        <p><input type="checkbox" id="683_cb21" name="683_cb21">&nbsp;Contacts</p>
                                        <p><input type="checkbox" id="683_cb23" name="683_cb23">&nbsp;Color Samples</p>
                                        <p style="color:rgb(255,0,0);"><input type="checkbox" id="683_cb24" name="683_cb24">&nbsp;<strong>Verified Mailing Address</strong>&nbsp;</p>
                                        <p><input type="checkbox" id="683_cb25" name="683_cb25">&nbsp;<strong>Verified Displays</strong>&nbsp;</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"><button class="btn btn-default" data-dismiss="modal" type="button">Close</button><button class="btn btn-primary" type="button">Add Note</button></div>
            </div>
        </div>
    </div>