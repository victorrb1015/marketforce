<div class="modal fade" role="dialog" tabindex="-1" id="coldVisitNote">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Prospect Visit Form</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="background-color:#ffffff;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="/*background:rgba(51,122,183,0.2);*/">
                            <h4>Visit Information</h4>
                            <p class="text-primary">WIth whom did you meet?</p><input class="form-control" type="text" placeholder="John Smith" id="cold_name" style="width:100%;" name="cold_name">
                            <p class="text-primary">Notes from your visit today:</p><textarea class="form-control" placeholder="Met with John today on the new..." id="cold_info" style="width:100%;height:150px;" name="cold_info"></textarea>
                        </div>
                        <!--<div class="col-md-12"><br>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                          <h4>Contact Info</h4>
                            <p class="text-primary">Email Address:</p><input class="form-control" type="text" placeholder="Email" id="cold_dealer_email" style="width:100%;" name="cold_dealer_email">
                          <p class="text-primary">Phone Number:</p><input class="form-control" type="text" placeholder="(XXX)XXX-XXXX" id="cold_dealer_phone" style="width:100%;" name="cold_dealer_phone">
                        </div>-->
                    </div>
                  <input type="hidden" id="cold_call_PID" name="cold_call_PID" value="" />
                </div>
                <div class="modal-footer">
                  <p id="cold_response" style="color:red;font-weight:bold;"></p>
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" type="button" onclick="cold_call('cold')">Add Note</button>
                </div>
            </div>
        </div>
    </div>