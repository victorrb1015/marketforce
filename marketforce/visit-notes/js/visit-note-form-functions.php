<script>
//Set Visit Note Form ID & Name Details...
function setDets(fid,fdName){
  document.getElementById('idNum').value = fid;
  document.getElementById('fdName').innerHTML = fdName;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
        
        //alert(this.responseText);
        var q = JSON.parse(this.responseText);
      
      document.getElementById('dealer_email').value = q.email;
      document.getElementById('dealer_fax').value = q.fax;
      document.getElementById('dealer_cell').value = q.phone;
      //Sunday
      document.getElementById('sun_open').value = q.sun_open;
      document.getElementById('sun_close').value = q.sun_close;
      if(q.sun_status === 'Closed'){
        document.getElementById('sun_not_open').checked = true;
        document.getElementById('sun_open').disabled = true;
        document.getElementById('sun_close').disabled = true;
      }
      //Monday
      document.getElementById('mon_open').value = q.mon_open;
      document.getElementById('mon_close').value = q.mon_close;
      if(q.mon_status === 'Closed'){
        document.getElementById('mon_not_open').checked = true;
        document.getElementById('mon_open').disabled = true;
        document.getElementById('mon_close').disabled = true;
      }
      //Tuesday
      document.getElementById('tue_open').value = q.tue_open;
      document.getElementById('tue_close').value = q.tue_close;
      if(q.tue_status === 'Closed'){
        document.getElementById('tue_not_open').checked = true;
        document.getElementById('tue_open').disabled = true;
        document.getElementById('tue_close').disabled = true;
      }
      //Wednesday
      document.getElementById('wed_open').value = q.wed_open;
      document.getElementById('wed_close').value = q.wed_close;
      if(q.wed_status === 'Closed'){
        document.getElementById('wed_not_open').checked = true;
        document.getElementById('wed_open').disabled = true;
        document.getElementById('wed_close').disabled = true;
      }
      //Thursday
      document.getElementById('thu_open').value = q.thu_open;
      document.getElementById('thu_close').value = q.thu_close;
      if(q.thu_status === 'Closed'){
        document.getElementById('thu_not_open').checked = true;
        document.getElementById('thu_open').disabled = true;
        document.getElementById('thu_close').disabled = true;
      }
      //Friday
      document.getElementById('fri_open').value = q.fri_open;
      document.getElementById('fri_close').value = q.fri_close;
      if(q.fri_status === 'Closed'){
        document.getElementById('fri_not_open').checked = true;
        document.getElementById('fri_open').disabled = true;
        document.getElementById('fri_close').disabled = true;
      }
      //Saturday
      document.getElementById('sat_open').value = q.sat_open;
      document.getElementById('sat_close').value = q.sat_close;
      if(q.sat_status === 'Closed'){
        document.getElementById('sat_not_open').checked = true;
        document.getElementById('sat_open').disabled = true;
        document.getElementById('sat_close').disabled = true;
      }
      
      //Address Information...
      document.getElementById('address1_physical').value = q.address;
      document.getElementById('city1_physical').value = q.city;
      document.getElementById('state1_physical').value = q.state;
      document.getElementById('zip1_physical').value = q.zip;
      document.getElementById('address2_mailing').value = q.maddress;
      document.getElementById('city2_mailing').value = q.mcity;
      document.getElementById('state2_mailing').value = q.mstate;
      document.getElementById('zip2_mailing').value = q.mzip;
      document.getElementById('address3_shipping').value = q.saddress;
      document.getElementById('city3_shipping').value = q.scity;
      document.getElementById('state3_shipping').value = q.sstate;
      document.getElementById('zip3_shipping').value = q.szip;
      
      if(q.status === 'Unmanned Lot'){
        document.getElementById('unman').checked = true;
      }
      if(q.new_signs === 'Done'){
        document.getElementById('signs').checked = true;
      }
      //Communication Preferences...
      if(q.comm_phone === 'Yes'){
        document.getElementById('comm_phone').checked = true;
      }
      if(q.comm_fax === 'Yes'){
        document.getElementById('comm_fax').checked = true;
      }
      if(q.comm_email === 'Yes'){
        document.getElementById('comm_email').checked = true;
      }
      if(q.comm_usmail === 'Yes'){
        document.getElementById('comm_usmail').checked = true;
      }
      
      document.getElementById('vnStatus').value = q.status;
      
    }
  }
  xmlhttp.open("GET","visit-notes/php/get-dealer-dets.php?id="+fid,true);
  xmlhttp.send();
}


//Parse and send Visit Note Data to Proccessing Script...
function add_note(){
  var id = document.getElementById('idNum').value;
	var stat = document.getElementById('vnStatus').value;
	//var xstate = document.getElementById('xstate').value;
	var biz = "";
	var user = "<?php echo $_SESSION['user_id']; ?>";
  
  //Check if Note is Internal...
  if(document.querySelector('input[name="vtype"]:checked') === null){
		document.getElementById('response').innerHTML = 'Please Select The Visit Type...';
		return;
	}else{
	var vtype = document.querySelector('input[name="vtype"]:checked').value;
  }
  
  //Get Visit Notes and Contact Details...
	var m = document.getElementById('vnote').value;
  m = urlEncode(m);
	//var m = m.replace(/\'/g,"&apos;");
	//var m = m.replace(/\"/g,'&quot;');
	//var m = m.replace(/&/g,'-xxx-');
	var n = document.getElementById('name').value;
	if(n == ''){
		document.getElementById('response').innerHTML = 'Please Enter The Name Of The Person You Dealt With...';
		return;
	}
	if(m == ''){
		document.getElementById('response').innerHTML = 'Please Enter The Notes From Your Visit...';
		return;
	}
  
  
	
	var x = document.getElementById('cb1').checked; if(x){var cb1 = document.getElementById('cb1').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Waivers & Forms items have been completed!"; return;}
	 x = document.getElementById('cb2').checked; if(x){var cb2 = document.getElementById('cb2').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Waivers & Forms items have been completed!"; return;}
	 x = document.getElementById('cb3').checked; if(x){var cb3 = document.getElementById('cb3').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Waivers & Forms items have been completed!"; return;}
	 x = document.getElementById('cb4').checked; if(x){var cb4 = document.getElementById('cb4').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Waivers & Forms items have been completed!"; return;}
	 x = document.getElementById('cb5').checked; if(x){var cb5 = document.getElementById('cb5').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Waivers & Forms items have been completed!"; return;}
	 x = document.getElementById('cb6').checked; if(x){var cb6 = document.getElementById('cb6').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Waivers & Forms items have been completed!"; return;}
	 x = document.getElementById('cb7').checked; if(x){var cb7 = document.getElementById('cb7').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Highlighted Area items have been completed!"; return;}
	 x = document.getElementById('cb8').checked; if(x){var cb8 = document.getElementById('cb8').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Highlighted Area items have been completed!"; return;}
	 x = document.getElementById('cb9').checked; if(x){var cb9 = document.getElementById('cb9').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Highlighted Area items have been completed!"; return;}
	 x = document.getElementById('cb10').checked; if(x){var cb10 = document.getElementById('cb10').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Highlighted Area items have been completed!"; return;}
	 x = document.getElementById('cb11').checked; if(x){var cb11 = document.getElementById('cb11').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Highlighted Area items have been completed!"; return;}
	 x = document.getElementById('cb12').checked; if(x){var cb12 = document.getElementById('cb12').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Supplies items have been completed!"; return;}
	 x = document.getElementById('cb13').checked; if(x){var cb13 = document.getElementById('cb13').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Supplies items have been completed!"; return;}
	 x = document.getElementById('cb14').checked; if(x){var cb14 = document.getElementById('cb14').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Supplies items have been completed!"; return;}
	 x = document.getElementById('cb15').checked; if(x){var cb15 = document.getElementById('cb15').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Supplies items have been completed!"; return;}
	 x = document.getElementById('cb16').checked; if(x){var cb16 = document.getElementById('cb16').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Dealer Login items have been completed!"; return;}
	 x = document.getElementById('cb17').checked; if(x){var cb17 = document.getElementById('cb17').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Dealer Login items have been completed!"; return;}
	 x = document.getElementById('cb18').checked; if(x){var cb18 = document.getElementById('cb18').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Dealer Login items have been completed!"; return;}
	 x = document.getElementById('cb19').checked; if(x){var cb19 = document.getElementById('cb19').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Tasks items have been completed!"; return;}
	 x = document.getElementById('cb20').checked; if(x){var cb20 = document.getElementById('cb20').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Tasks items have been completed!"; return;}
	 x = document.getElementById('cb21').checked; if(x){var cb21 = document.getElementById('cb21').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Tasks items have been completed!"; return;}
	 x = document.getElementById('cb22').checked; if(x){var cb22 = document.getElementById('cb22').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Tasks items have been completed!"; return;}
	 x = document.getElementById('cb23').checked; if(x){var cb23 = document.getElementById('cb23').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Tasks items have been completed!"; return;}
	 x = document.getElementById('cb24').checked; if(x){var cb24 = document.getElementById('cb24').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Tasks items have been completed!"; return;}
	 x = document.getElementById('cb25').checked; if(x){var cb25 = document.getElementById('cb25').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Tasks items have been completed!"; return;}
	var de = document.getElementById('dealer_email').value;
	var ce = document.getElementById('email_cb').checked;
	var df = document.getElementById('dealer_fax').value;
	var cf = document.getElementById('fax_cb').checked;
	var dc = document.getElementById('dealer_cell').value;
	var cc = document.getElementById('dcell_cb').checked;
	
	//Parsing for the new Price Training Boxes
	var x = document.getElementById('pb1').checked; if(x){var pb1 = document.getElementById('pb1').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Price Training items have been completed!"; return;}
	 x = document.getElementById('pb2').checked; if(x){var pb2 = document.getElementById('pb2').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Price Training items have been completed!"; return;}
	 //x = document.getElementById('pb3').checked; if(x){var pb3 = document.getElementById('pb3').value;}
	 //x = document.getElementById('pb4').checked; if(x){var pb4 = document.getElementById('pb4').value;}
	// x = document.getElementById('pb5').checked; if(x){var pb5 = document.getElementById('pb5').value;}
	 x = document.getElementById('pb6').checked; if(x){var pb6 = document.getElementById('pb6').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Price Training items have been completed!"; return;}
	 x = document.getElementById('pb7').checked; if(x){var pb7 = document.getElementById('pb7').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Price Training items have been completed!"; return;}
	 x = document.getElementById('pb8').checked; if(x){var pb8 = document.getElementById('pb8').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Price Training items have been completed!"; return;}
	 x = document.getElementById('pb9').checked; if(x){var pb9 = document.getElementById('pb9').value;}else if(stat == 'New' && vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please ensure all Price Training items have been completed!"; return;}
	
	//Setup for removing pb3-5
			var pb3 = 'undefined';
			var pb4 = 'undefined';
			var pb5 = 'undefined';

	if(ce == false && de == '' && vtype != 'Internal Note'){
		document.getElementById('response').innerHTML = 'Please confirm the dealers email address!';
		return;
	}
	if(cf == false && df == '' && vtype != 'Internal Note'){
		document.getElementById('response').innerHTML = 'Please confirm the dealers fax number!';
		return;
	}
	if(cc == false && dc == '' && vtype != 'Internal Note'){
		document.getElementById('response').innerHTML = 'Please confirm the Dealers cell phone number!';
		return
	}
	
	if(document.querySelector('input[name="vtype"]:checked') === null){
		document.getElementById('response').innerHTML = 'Please Select The Visit Type...';
		return;
	}else{
	var vtype = document.querySelector('input[name="vtype"]:checked').value;
	var x = document.getElementById('pb2').checked; if(x){var pb2 = document.getElementById('pb2').value;}else if(vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please confirm that the dealer\'s handbook is up-to-date!"; return;}
	var x = document.getElementById('cb21').checked; if(x){var cb21 = document.getElementById('cb21').value;}else if(vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please confirm the dealer\'s email address!"; return;}
	var x = document.getElementById('cb24').checked; if(x){var cb24 = document.getElementById('cb24').value;}else if(vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please confirm the dealer\'s mailing address!"; return;}
	var x = document.getElementById('cb25').checked; if(x){var cb25 = document.getElementById('cb25').value;}else if(vtype != 'Internal Note'){document.getElementById('response').innerHTML = "Please confirm the dealer\'s displays on site!"; return;}
	}
	
	var z = document.getElementById('unman').checked; if(z){var unman = document.getElementById('unman').value;}
	var signs = document.getElementById('signs').checked; if(signs){var signs = document.getElementById('signs').value;}
	
	//Farm Daily Hours data
	var sun_open = document.getElementById('sun_open').value;
	var sun_close = document.getElementById('sun_close').value;
	if(document.querySelector("input[name='sun_not_open']:checked")){
		var sun_status = 'Closed';
	}else{
		var sun_status = 'Open';
	}
	var mon_open = document.getElementById('mon_open').value;
	var mon_close = document.getElementById('mon_close').value;
	if(document.querySelector("input[name='mon_not_open']:checked")){
		var mon_status = 'Closed';
	}else{
		var mon_status = 'Open';
	}
	var tue_open = document.getElementById('tue_open').value;
	var tue_close = document.getElementById('tue_close').value;
	if(document.querySelector("input[name='tue_not_open']:checked")){
		var tue_status = 'Closed';
	}else{
		var tue_status = 'Open';
	}
	var wed_open = document.getElementById('wed_open').value;
	var wed_close = document.getElementById('wed_close').value;
	if(document.querySelector("input[name='wed_not_open']:checked")){
		var wed_status = 'Closed';
	}else{
		var wed_status = 'Open';
	}
	var thu_open = document.getElementById('thu_open').value;
	var thu_close = document.getElementById('thu_close').value;
	if(document.querySelector("input[name='thu_not_open']:checked")){
		var thu_status = 'Closed';
	}else{
		var thu_status = 'Open';
	}
	var fri_open = document.getElementById('fri_open').value;
	var fri_close = document.getElementById('fri_close').value;
	if(document.querySelector("input[name='fri_not_open']:checked")){
		var fri_status = 'Closed';
	}else{
		var fri_status = 'Open';
	}
	var sat_open = document.getElementById('sat_open').value;
	var sat_close = document.getElementById('sat_close').value;
	if(document.querySelector("input[name='sat_not_open']:checked")){
		var sat_status = 'Closed';
	}else{
		var sat_status = 'Open';
	}
  
  
  //Communication Preferences...
  x = document.getElementById('comm_phone').checked; if(x){var comm_phone = document.getElementById('comm_phone').value;}else{comm_phone = 'No';}
  x = document.getElementById('comm_fax').checked; if(x){var comm_fax = document.getElementById('comm_fax').value;}else{comm_fax = 'No';}
  x = document.getElementById('comm_email').checked; if(x){var comm_email = document.getElementById('comm_email').value;}else{comm_email = 'No';}
  x = document.getElementById('comm_usmail').checked; if(x){var comm_usmail = document.getElementById('comm_usmail').value;}else{comm_usmail = 'No';}

	//Address Information...
  var paddress = document.getElementById('address1_physical').value;
  var pcity = document.getElementById('city1_physical').value;
  var pstate = document.getElementById('state1_physical').value;
  var pzip = document.getElementById('zip1_physical').value;
  
  var maddress = document.getElementById('address2_mailing').value;
  var mcity = document.getElementById('city2_mailing').value;
  var mstate = document.getElementById('state2_mailing').value;
  var mzip = document.getElementById('zip2_mailing').value;
  
  var saddress = document.getElementById('address3_shipping').value;
  var scity = document.getElementById('city3_shipping').value;
  var sstate = document.getElementById('state3_shipping').value;
  var szip = document.getElementById('zip3_shipping').value;
  
	
	if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      if(this.responseText === 'You have already added a note for this dealer today!'){
				document.getElementById('response').innerHTML = this.responseText;
			}else{
      alert(this.responseText);
      document.getElementById('response').innerHTML = this.responseText;
			//document.getElementById('notes').value='';
			//document.getElementById('name').value='';
			//$('input:checkbox').removeAttr('checked');
			window.location.reload();
			//document.getElementById('rec').value='default';
			}
    }
  }
    var params = "m="+m;
    params += "&id="+id;
    params += "&user="+user;
		params += "&n="+n;
		params += "&cb1="+cb1;
		params += "&cb2="+cb2;
		params += "&cb3="+cb3;
		params += "&cb4="+cb4;
		params += "&cb5="+cb5;
		params += "&cb6="+cb6;
		params += "&cb7="+cb7;
		params += "&cb8="+cb8;
		params += "&cb9="+cb9;
		params += "&cb10="+cb10;
		params += "&cb11="+cb11;
		params += "&cb12="+cb12;
		params += "&cb13="+cb13;
		params += "&cb14="+cb14;
		params += "&cb15="+cb15;
		params += "&cb16="+cb16;
		params += "&cb17="+cb17;
		params += "&cb18="+cb18;
		params += "&cb19="+cb19;
		params += "&cb20="+cb20;
		params += "&cb21="+cb21;
		params += "&cb22="+cb22;
		params += "&cb23="+cb23;
		params += "&cb24="+cb24;
		params += "&cb25="+cb25;
    params += "&biz="+biz;
		params += "&de="+de;
		params += "&vtype="+vtype;
		params += "&pb1="+pb1;
		params += "&pb2="+pb2;
		params += "&pb3="+pb3;
		params += "&pb4="+pb4;
		params += "&pb5="+pb5;
		params += "&pb6="+pb6;
		params += "&pb7="+pb7;
		params += "&pb8="+pb8;
		params += "&pb9="+pb9;
		params += "&df="+df;
		params += "&unman="+unman;
		params += "&sun_open="+sun_open;
		params += "&sun_close="+sun_close;
		params += "&sun_status="+sun_status;
		params += "&mon_open="+mon_open;
		params += "&mon_close="+mon_close;
		params += "&mon_status="+mon_status;
		params += "&tue_open="+tue_open;
		params += "&tue_close="+tue_close;
		params += "&tue_status="+tue_status;
		params += "&wed_open="+wed_open;
		params += "&wed_close="+wed_close;
		params += "&wed_status="+wed_status;
		params += "&thu_open="+thu_open;
		params += "&thu_close="+thu_close;
		params += "&thu_status="+thu_status;
		params += "&fri_open="+fri_open;
		params += "&fri_close="+fri_close;
		params += "&fri_status="+fri_status;
		params += "&sat_open="+sat_open;
		params += "&sat_close="+sat_close;
		params += "&sat_status="+sat_status;
		params += "&signs="+signs;
		params += "&dc="+dc;
    params += "&comm_phone="+comm_phone;
    params += "&comm_fax="+comm_fax;
    params += "&comm_email="+comm_email;
    params += "&comm_usmail="+comm_usmail;
    params += "&paddress="+paddress;
    params += "&pcity="+pcity;
    params += "&pstate="+pstate;
    params += "&pzip="+pzip;
    params += "&maddress="+maddress;
    params += "&mcity="+mcity;
    params += "&mstate="+mstate;
    params += "&mzip="+mzip;
    params += "&saddress="+saddress;
    params += "&scity="+scity;
    params += "&sstate="+sstate;
    params += "&szip="+szip;
  params = encodeURI(params);
  xmlhttp.open("POST","visit-notes/php/add-visit-note.php",true);
  xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xmlhttp.send(params);
}
  
  
  
  
  function get_prev_notes(id,dname){
    document.getElementById('prev-note-dname').innerHTML = '';
    document.getElementById('prev-notes-container').innerHTML = '';
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
        
        document.getElementById('prev-note-dname').innerHTML = dname;
        document.getElementById('prev-notes-container').innerHTML = this.responseText;
      
    }
  }
  xmlhttp.open("GET","visit-notes/php/get-notes.php?id="+id,true);
  xmlhttp.send();
  }
</script>