<script>
  function new_prospect(){
    var rep_id = '<? echo $_SESSION['user_id']; ?>';
    var rep_name = '<? echo $_SESSION['full_name']; ?>';
    var cbiz = document.getElementById('pbiz').value;
    if(cbiz === ''){
      document.getElementById('prospect_response').innerHTML = 'Please enter the Business Name!';
      return;
    }
    cbiz = urlEncode(cbiz);
    var caddress = document.getElementById('paddress').value;
    /*if(caddress === ''){
      document.getElementById('prospect_response').innerHTML = 'Please enter the Business Address!';
      return;
    }*/
    caddress = urlEncode(caddress);
    var ccity = document.getElementById('pcity').value;
    /*if(ccity === ''){
      document.getElementById('prospect_response').innerHTML = 'Please enter the Business City!';
      return;
    }*/
    ccity = urlEncode(ccity);
    var cstate = document.getElementById('pstate').value;
    /*if(cstate === ''){
      document.getElementById('prospect_response').innerHTML = 'Please enter the Business State!';
      return;
    }*/
    cstate = urlEncode(cstate);
    var czip = document.getElementById('pzip').value;
    /*if(czip === ''){
      document.getElementById('prospect_response').innerHTML = 'Please enter the Business Zip Code!';
      return;
    }*/
    czip = urlEncode(czip);
    var cname = document.getElementById('pname').value;
    /*if(cname === ''){
      document.getElementById('prospect_response').innerHTML = 'Please enter the Owner/Contact Name!';
      return;
    }*/
    cname = urlEncode(cname);
    var cemail = document.getElementById('pdealer_email').value;
    /*if(cemail === ''){
      document.getElementById('prospect_response').innerHTML = 'Please enter the Owner/Contact Email!';
      return;
    }*/
    cemail = urlEncode(cemail);
    var cphone = document.getElementById('pdealer_phone').value;
    /*if(cphone === ''){
      document.getElementById('prospect_response').innerHTML = 'Please enter the Owner/Contact Phone Number!';
      return;
    }*/
    cphone = urlEncode(cphone);
    var cinfo = document.getElementById('pinfo').value;
    if(cinfo === ''){
      document.getElementById('prospect_response').innerHTML = 'Please enter notes regarding the prospect!';
      return;
    }
    cinfo = urlEncode(cinfo);
    
    document.getElementById('npb').disabled = true;
    
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      if(this.responseText === 'You have already added a note for this dealer today!'){
				document.getElementById(id+'_response').innerHTML = this.responseText;
			}else{
      alert(this.responseText);
			window.location.reload();
			}
    }
  }
  xmlhttp.open("GET","visit-notes/php/add-prospect.php?rep_id="+rep_id+"&rep_name="+rep_name+
               "&cbiz="+cbiz+
               "&caddress="+caddress+
               "&ccity="+ccity+
               "&cstate="+cstate+
               "&czip="+czip+
               "&cname="+cname+
               "&cemail="+cemail+
               "&cphone="+cphone+
               "&cinfo="+cinfo,true);
	xmlhttp.send();
  }
  
  
  function remove_prospect(pid,d){
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
     alert(this.responseText);
     window.location.reload();
    }
  }
  xmlhttp.open("GET","visit-notes/php/remove-prospect.php?pid="+pid+"&d="+d,true);
	xmlhttp.send();
  }
</script>