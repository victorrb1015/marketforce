<?php
include 'security/session/session-settings.php';
include 'php/connection.php';
include 'security/sections/security-check-logged-in.php';
$pageName = 'Accounting';
$pageIcon = 'fas fa-usd';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $pageName; ?> | MarketForce</title>
    <?php include 'global/sections/head.php'; ?>
  <link href="accounting/css/custom.css" rel="stylesheet" />
  <?php include 'accounting/js/custom.php'; ?>
</head>

<body>
	<!-- Preloader -->
	<?php include 'global/sections/preloader.php'; ?>
	<!-- /Preloader -->
    <div class="wrapper theme-4-active pimary-color-red">

    	<!--Navigation-->
    	<?php include 'global/sections/nav.php'; ?>
		
		
        <!-- Main Content -->
		<div class="page-wrapper"><!--Includes Footer-->

      <div class="container-fluid pt-25"><!--Main Content Here-->
				<?php include 'global/sections/page-title-bar.php'; ?>
        <?php include 'global/sections/notifications.php'; ?>
        
        <?php
        $c_tab = '';
        $e_tab = '';
        $a_tab = '';
        $c_nav = '';
        $e_nav = '';
        $a_nav = '';
        if($_GET['tab'] == 'c'){
          $c_tab = 'active in';
          $c_nav = 'active';
        }
        if($_GET['tab'] == 'e'){
          $e_tab = 'active in';
          $e_nav = 'active';
        }
        if($_GET['tab'] == 'a'){
          $a_tab = 'active in';
          $a_nav = 'active';
        }
        if(!$_GET['tab'] || $_GET['tab'] == ''){
          $c_tab = 'active in';
          $c_nav = 'active';
        }
        ?>

        <ul class="nav nav-tabs">
          <li class="nav-item <?php echo $c_nav; ?>"><a data-toggle="tab" href="#check-requests" style="color:#337ab7;"><i class="fas fa-money-check-alt"></i>  Check Requests</a></li>
          <li class="nav-item <?php echo $e_nav; ?>"><a data-toggle="tab" href="#electronic-payments" style="color:#337ab7;"><i class="fas fa-receipt"></i> Electronic Payments</a></li>
          <li class="nav-item <?php echo $a_nav; ?>"><a data-toggle="tab" href="#affiliate-payments" style="color:#337ab7;"><i class="fas fa-user-tag"></i> Affiliate Payments</a></li>
        </ul>
        
        <div class="tab-content" id="myTabContent">
          
          <div class="tab-pane <?php echo $c_tab; ?>" id="check-requests" role="tabpanel">
            <?php 
              if($_SESSION['check_request'] == 'Yes'){
                include 'accounting/sections/check-request-table.php';
              }else{
                echo '<h2>You do not have access to this Module...';
              }
            ?>       
          </div>
        
          <div class="tab-pane <?php echo $e_tab; ?>" id="electronic-payments" role="tabpanel">
          <?php
            if($_SESSION['collections'] == 'Yes'){
              include 'accounting/sections/electronic-payments-table.php';
            }else{
              echo '<h2>You do not have access to this Module...';
            }
          ?>
          </div>
          
          <div class="tab-pane <?php echo $a_tab; ?>" id="affiliate-payments" role="tabpanel">
          <?php
            if($_SESSION['accountant'] == 'Yes'){
              include 'accounting/sections/affiliate-payments-table.php';
            }else{
              echo '<h2>You do not have access to this Module...';
            }
          ?>
          </div>
          
        </div>
        
        
			</div>
			
			
			<!-- Footer -->
			<?php include 'global/sections/footer.php'; ?>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
  
  <!-- Modals -->
  <?php include 'accounting/modals/complete-cr-modal.php'; ?>
  <?php include 'accounting/modals/info-cr-modal.php'; ?>
  <?php include 'accounting/modals/modal-check-request-form.php'; ?>
  <?php include 'accounting/modals/crdoc-upload-modal.php'; ?>
  <?php include 'accounting/modals/payment-not-received-modal.php'; ?>
	
	<!--Footer-->
	<?php include 'global/sections/includes.php'; ?>
</body>

</html>
