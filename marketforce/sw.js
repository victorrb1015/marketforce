var cacheName = 'v3.13.3';

var cached_files = [
  "/marketforce/scheduling/php/fetch-current-orders.php?org_id=257823",//All Steel
  "/marketforce/scheduling/php/fetch-current-orders.php?org_id=162534",//NorthEdge
  "/marketforce/scheduling/php/fetch-current-orders.php?org_id=615243",//Legacy
];

var cached_file = '/marketforce/scheduling/php/fetch-current-orders.php';

self.addEventListener('install', function() {
  response_message('Update','MarketForce has been updated to '+cacheName);
});

self.addEventListener('fetch', e => {
  e.respondWith(
    caches.open(cacheName).then(function(cache){
      return cache.match(e.request).then(function(response) {
        return response || fetch(e.request).then(function(response){
          //if(cached_files.includes(e.request.url)){
          if(e.request.url.includes(cached_file)){
            cache.put(e.request, response.clone());
            console.log('Cached response for: '+e.request.url);
            return response;
          }else{
            //console.log(e.request);
            //console.log(cached_files);
            return response;
          }
        });
      });
    })
  );
});

self.addEventListener('push', () => {
  self.registration.showNotification('Test Message', {});
});


self.addEventListener('message', function (event) {
  
  if (event.data.action === 'skipWaiting') {
    self.skipWaiting();
    response_message('Update','MarketForce is being updated in the background. This window will be refreshed momentarily');
  }
  
  if(event.data.action === 'getCacheName'){
    response_message('cacheName',cacheName);
  }
  
});



function response_message(type,msg){
  // Select who we want to respond to
    self.clients.matchAll({
      includeUncontrolled: true,
      type: 'window',
    }).then((clients) => {
      if (clients && clients.length) {
        // Send a response - the clients
        // array is ordered by last focused
        clients[0].postMessage({
          type: type,
          msg: msg
        });
      }
    });
}