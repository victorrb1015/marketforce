<?php
include '../../php/connection.php';
$reportName = 'Coil Total Report';

//Load Variables...

?>
<html>

<head>
    <title>
        <?php echo $reportName; ?>
    </title>
    <link href="../../jquery/jquery-ui.css" rel="stylesheet"/>
    <style>
        /* page */
        html {
            font: 16px/1 "Open Sans", sans-serif;
            overflow: auto;
            padding: 0.5in;
        }

        html {
            background: #999;
            cursor: default;
        }

        body {
            box-sizing: border-box;
            min-height: 11in;
            margin: 0 auto;
            overflow: hidden;
            padding: 0.5in;
            width: 8.5in;
        }

        body {
            background: #FFF;
            border-radius: 1px;
            box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
        }

        th,
        td {
            border: 1px solid black;
            padding: 5px;
        }

        .btn {
            padding-left: 8px;
            padding-right: 8px;
            background: blue;
            border-radius: 25px;
            color: white;
        }

        @media print {
            * {
                -webkit-print-color-adjust: exact;
            }

            html {
                background: none;
                padding: 0;
            }

            body {
                box-shadow: none;
                margin: 0;
            }

            span:empty {
                display: none;
            }

            .add,
            .cut {
                display: none;
            }
        }

        @page {
            margin: 0;
        }
    </style>
</head>

<body>
<h1 style="text-align:center;">
    <?php echo $reportName; ?>
</h1>

<table id="report_table" style="/*margin:auto;*/">
    <thead>
    <tr style="background:lightgray;">
        <th>Coil Color</th>
        <th>QTY</th>
        <th>Total Weight (lb)</th>
        <th>Minimum Threshold</th>
        <?php if($_SESSION['manager'] === 'Yes'){
            echo '<th>Actions</th>';
        }?>
    </tr>
    </thead>
    <tbody>
    <?php

    $q = "SELECT item_name, count(*) as Quantity,  SUM(item_weight) as 'Total Weight', min_group FROM `inventory_items`  where item_type = 'Coil'  And inactive != 'Yes'  GROUP BY item_name";
    $g = mysqli_query($conn, $q) or die($conn->error);
    $num_row = 0;
    while ($r = mysqli_fetch_array($g)) {
        if (Null == $r['min_group']) {
            $r['min_group'] = 0;
        }
        echo '<tr ';
        if ($r['Quantity'] < $r['min_group']) {
            echo 'style="background-color: red;color: white"';
        }
        echo '>
              <td id="name_' . $num_row . '">' . $r['item_name'] . '</td>
              <td> <a href="coil-list-report.php?item_name=' . $r['item_name'] . '" target="_blank">' . number_format($r['Quantity']) . '</a></td>
              <td>' . number_format($r['Total Weight']) . '</td>
              <td >'.$r['min_group'].'</td>';
        if($_SESSION['manager'] === 'Yes'){
            echo '
              <td>
                    <button onclick="update_min_group('. $num_row . ')">Update Min Threshold Group</button>
              </td>';
        }
        echo '</tr>';
        $num_row++;
    }
    ?>

    </tbody>
</table>
</body>
<!--JQuery Files-->
<script src="../../jquery/external/jquery/jquery.js"></script>
<script src="../../jquery/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
        $(".date").datepicker();
    });

    function update_min_group(num_row){
        const name = document.getElementById("name_" + num_row).innerHTML;
        const num1 = prompt('Enter Minimum Threshold Value:');
        const min_group = parseInt(num1);
        console.log(min_group);
        $.ajax({
            type: "POST",
            url: "php/update-min-group.php",
            dataType: "json",
            data: {
                "name": name,
                "min_group": min_group
            },
            success: function(response){
                console.log(response);
                location.reload();
            }
        });
    }
</script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $("#report_table").dataTable({
        "paging": false
    });
</script>

</html>