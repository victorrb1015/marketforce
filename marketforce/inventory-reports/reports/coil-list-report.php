<?php
include '../../php/connection.php';
$reportName = 'Coil List Report';

//Load Variables...
$item_name = mysqli_real_escape_string($conn, $_REQUEST['item_name']);
?>

  <html>

  <head>
    <title>
      <?php echo $reportName; ?>
    </title>
    <link href="../../jquery/jquery-ui.css" rel="stylesheet" />
    <style>
      /* page */

      html {
        font: 16px/1 "Open Sans", sans-serif;
        overflow: auto;
        padding: 0.5in;
      }

      html {
        background: #999;
        cursor: default;
      }

      body {
        box-sizing: border-box;
        min-height: 11in;
        margin: 0 auto;
        overflow: hidden;
        padding: 0.5in;
        width: 8.5in;
      }

      body {
        background: #FFF;
        border-radius: 1px;
        box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
      }

      th,
      td {
        border: 1px solid black;
        padding: 5px;
      }

      .btn {
        padding-left: 8px;
        padding-right: 8px;
        background: blue;
        border-radius: 25px;
        color: white;
      }

      @media print {
        * {
          -webkit-print-color-adjust: exact;
        }
        html {
          background: none;
          padding: 0;
        }
        body {
          box-shadow: none;
          margin: 0;
        }
        span:empty {
          display: none;
        }
        .add,
        .cut {
          display: none;
        }
      }

      @page {
        margin: 0;
      }
    </style>
  </head>

  <body>
    <h1 style="text-align:center;">
      <?php echo $reportName; ?>
    </h1>





    <table id="rec_table" style="/*margin:auto;*/">
      <thead>
        <tr style="background:lightgray;">
          <th>Date</th>
          <th>Barcode</th>
          <th>Item Name</th>
          <th>Item Info</th>
          <th>QTY</th>
          <th>Days In Stock</th>
          <th>Weight (lb)</th>
        </tr>
      </thead>
      <tbody>
        <?php

$q = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND `item_type` = 'Coil' AND `item_name` = '" . $item_name . "' AND `qty` > 0 ORDER BY `date` DESC";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  
  $date1 = strtotime("today");
  $date2 = strtotime($r['date']);
  $diff = abs($date1 - $date2);
  $years = floor($diff / (365*60*60*24)); 
  $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
  $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
  $totaldays = floor(($diff)/ (60*60*24));
  
  echo '<tr>
          <td>' . date("m/d/y", strtotime($r['date'])) . '</td>
          <td>' . $r['barcode'] . '</td>
          <td>' . $r['item_name'] . '</td>
          <td>' . $r['item_info'] . '</td>
          <th>' . $r['qty'] . '</th>
          <td>' . $totaldays . '</td>
          <td>' . $r['item_weight'] . '</td>
        </tr>';
}
?>



      </tbody>
    </table>
  </body>
  <!--JQuery Files-->
  <script src="../../jquery/external/jquery/jquery.js"></script>
  <script src="../../jquery/jquery-ui.js"></script>
  <script>
    $(document).ready(function() {
      $(".date").datepicker();
    });
  </script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script>
    $("#rec_table").dataTable({
      "paging": false
    });
  </script>

  </html>