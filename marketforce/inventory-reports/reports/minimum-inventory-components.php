<?php
include '../../php/connection.php';
$reportName = 'Minimum Threshold Components';
?>
<html>

<head>
    <title>
        <?php echo $reportName; ?>
    </title>
    <link href="../../jquery/jquery-ui.css" rel="stylesheet" />
    <style>
        /* page */
        html {
            font: 16px/1 "Open Sans", sans-serif;
            overflow: auto;
            padding: 0.5in;
        }
        html {
            background: #999;
            cursor: default;
        }
        body {
            box-sizing: border-box;
            min-height: 11in;
            margin: 0 auto;
            overflow: hidden;
            padding: 0.5in;
            width: 8.5in;
        }
        body {
            background: #FFF;
            border-radius: 1px;
            box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
        }
        th,
        td {
            border: 1px solid black;
            padding: 5px;
        }
        .btn {
            padding-left: 8px;
            padding-right: 8px;
            background: blue;
            border-radius: 25px;
            color: white;
        }
        @media print {
            * {
                -webkit-print-color-adjust: exact;
            }
            html {
                background: none;
                padding: 0;
            }
            body {
                box-shadow: none;
                margin: 0;
            }
            span:empty {
                display: none;
            }
            .add,
            .cut {
                display: none;
            }
        }
        @page {
            margin: 0;
        }
    </style>
</head>

<body>
<h1 style="text-align:center;">
    <?php echo $reportName; ?>
</h1>

<table id="report_table" style="/*margin:auto;*/">
    <thead>
    <tr style="background:lightgray;">
        <th>Barcode</th>
        <th>Item Name</th>
        <th>Type</th>
        <th>Current QOH</th>
        <th>Min Threshold</th>
    </tr>
    </thead>
    <tbody>
    <?php

    $q = "SELECT barcode, item_name, item_type, qty, min_qty FROM `inventory_items` WHERE item_type = 'Component' AND inactive != 'Yes' AND min_qty != '0' AND qty <= min_qty";
    $g = mysqli_query($conn, $q) or die($conn->error);
    while($r = mysqli_fetch_array($g)){

        echo '<tr>
              <td>' . $r['barcode'] . '</td>
              <td>' . $r['item_name'] . '</td>
              <td>' . $r['item_type'] . '</td>
              <td>' . $r['qty'] . '</td>
              <td>' . $r['min_qty'] . '</td>
            </tr>';


    }
    ?>

    </tbody>
</table>
</body>
<!--JQuery Files-->
<script src="../../jquery/external/jquery/jquery.js"></script>
<script src="../../jquery/jquery-ui.js"></script>
<script>
    $(document).ready(function() {
        $(".date").datepicker();
    });
</script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $("#report_table").dataTable({
        "paging": false
    });
</script>

</html>