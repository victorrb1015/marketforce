<?php
include '../../php/connection.php';
$reportName = 'Low Inventory Report ';
$class = 'table-danger';
$lowReport =  array();
$q = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND `item_type` NOT LIKE 'Product'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)) {
    $val_qty = (int)$r['qty'];
    $val_min_qty = (int)$r['min_qty'];
    if (($val_qty <= $val_min_qty) && $val_qty !== 0) {
        $array = [
            'barcode' => $r['barcode'],
            'item_name' => $r['item_name'],
            'qty' => number_format($r['qty']),
            'min_qty' => number_format($r['min_qty']),
            'ID' => $r['ID'],
        ];
        $lowReport[] = $array;
    }
}
$query3 = 'SELECT item_id FROM inventory_requisition WHERE po_done is null AND inactive is False group by item_id';
$g3 = mysqli_query($conn, $query3) or die($conn->error);
$report = array();
while ($res = mysqli_fetch_array($g3)) {
    foreach ($lowReport as $clave=>$value)
    {
        if($value['ID'] === $res['item_id']){
            $query2 = 'SELECT quote, po_due_date FROM inventory_requisition WHERE po_done is null AND inactive is False and item_id = '.$value['ID'].' AND created_at = (SELECT max(created_at) from inventory_requisition where item_id = '.$value['ID'].')';
            $g2 = mysqli_query($conn, $query2) or die($conn->error);
            $var = $g2->fetch_assoc();
            $lowReport[$clave] += [
                'quote' => $var['quote'],
                'qty_requ' => $var['po_due_date']
            ];
        }
    }
}
//$row_cnt = $g->num_rows;
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title><?php echo $reportName; ?></title>
    <style>
        .table-warning {
            --bs-table-bg: #e8de68;
            --bs-table-striped-bg: #fff200;
        }
        .table-success{
            --bs-table-bg: #5abd7c ;
            --bs-table-striped-bg: #00a651;
        }
        .table-danger{
            --bs-table-bg: #c6323d ;
            --bs-table-striped-bg: #a31d2d;
            --bs-table-striped-color: white;
            color: white;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <h1> <?php echo $reportName; ?></h1>
    </div>
    <div class="row">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th scope="col">Barcode</th>
                <th scope="col">Item Name</th>
                <th scope="col">Current QOH</th>
                <th scope="col">Min Threshold</th>
                <th scope="col">Quote</th>
                <th scope="col">PO Due Date</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $max = count($lowReport);
            $i = 0;
            while($i < ($max)){
                if($lowReport[$i]['quote'] === null){
                    $class = 'table-danger';
                    echo '<tr class="'.$class.'">
                          <td>' . $lowReport[$i]['barcode'] . '</td>
                          <td>' . $lowReport[$i]['item_name'] . '</td>
                          <td>' . $lowReport[$i]['qty'] . '</td>
                          <td>' . $lowReport[$i]['min_qty'] . '</td>
                          <td>' . $lowReport[$i]['quote'] . '</td>
                          <td>' . $lowReport[$i]['qty_requ'] . '</td>
                          <td><a type="button" class="btn btn-primary" href="requisition-report.php?item='. $lowReport[$i]['ID'].'">Requisition</a></td>
                      </tr>';
                    unset($lowReport[$i]);
                }
                $i++;
            }
            $i = 0;
            while($i < ($max)){
                if($lowReport[$i]['quote'] !== null  && ($lowReport[$i]['qty_requ'] === '0000-00-00')){
                    $class = 'table-warning';
                    echo '<tr class="'.$class.'">
                          <td>' . $lowReport[$i]['barcode'] . '</td>
                          <td>' . $lowReport[$i]['item_name'] . '</td>
                          <td>' . $lowReport[$i]['qty'] . '</td>
                          <td>' . $lowReport[$i]['min_qty'] . '</td>
                          <td>' . $lowReport[$i]['quote'] . '</td>
                          <td>' . $lowReport[$i]['qty_requ'] . '</td>
                          <td><a type="button" class="btn btn-primary" href="requisition-report.php?item='. $lowReport[$i]['ID'].'">Requisition</a></td>
                      </tr>';
                    unset($lowReport[$i]);
                }
                $i++;
            }
            $i = 0;
            while($i < ($max)){
                if(($lowReport[$i]['quote'] !== NULL) && ($lowReport[$i]['qty_requ'] !== '0000-00-00')){
                    $class = 'table-success';
                    echo '<tr class="'.$class.'">
                          <td>' . $lowReport[$i]['barcode'] . '</td>
                          <td>' . $lowReport[$i]['item_name'] . '</td>
                          <td>' . $lowReport[$i]['qty'] . '</td>
                          <td>' . $lowReport[$i]['min_qty'] . '</td>
                          <td>' . $lowReport[$i]['quote'] . '</td>
                          <td>' . $lowReport[$i]['qty_requ'] . '</td>
                          <td><a type="button" class="btn btn-primary" href="requisition-report.php?item='. $lowReport[$i]['ID'].'">Requisition</a></td>
                      </tr>';
                    unset($lowReport[$i][$i]);
                }
                $i++;
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>