<?php
session_start();
error_reporting(0);
$db_host = 'localhost';
$db_user = 'marketfo_mf';
$db_pass = '#NgTFJQ!z@t8';
$db_mf_db = 'marketfo_marketforce';
$db_db1 = 'marketfo_mf_acerotx';
$db_db2 = 'marketfo_mf_aceroca';
$db_db3 = 'marketfo_mf_allsteelcarports';
$report = array();

$conn =  mysqli_connect($db_host,$db_user,$db_pass,$db_db1) or die('Main Connection Error: ' . $conn->error);
$q = "SELECT item_name, count(*) as Quantity,  SUM(item_weight) as 'Total Weight' FROM `inventory_items`  where item_type = 'Coil'  And inactive != 'Yes'  GROUP BY item_name";
$g = mysqli_query($conn, $q) or die($conn->error);
while ($r = mysqli_fetch_array($g)) {
    $array = [
        'item_name' => $r['item_name'],
        'quantity_tx' => $r['Quantity'],
        'Weight_tx' => $r['Total Weight'],
        'Total_qty' => $r['Quantity'],
        'Total_weight' => $r['Total Weight']
    ];
    $report[] = $array;
}
mysqli_close($conn);
$conn2 =  mysqli_connect($db_host,$db_user,$db_pass,$db_db2) or die('Main Connection Error: ' . $conn2->error);
$q = "SELECT item_name, count(*) as Quantity,  SUM(item_weight) as 'Total Weight' FROM `inventory_items`  where item_type = 'Coil'  And inactive != 'Yes'  GROUP BY item_name";
$g = mysqli_query($conn2, $q) or die($conn2->error);
$igual = True;
while ($r = mysqli_fetch_array($g)) {
    $array = [
        'item_name' => $r['item_name'],
        'quantity_ca' => $r['Quantity'],
        'Weight_ca' => $r['Total Weight'],
        'Total_qty' => $r['Quantity'],
        'Total_weight' => $r['Total Weight']
    ];
    foreach ($report as $clave=>$value){
        $cadena1 =str_replace(' ', '', $r['item_name']);
        $cadena2 =str_replace(' ', '', $value['item_name']);
        if($cadena1 === $cadena2){
            $report[$clave] += [
                'quantity_ca' => $r['Quantity'],
                'Weight_ca' => $r['Total Weight']
            ];
            $report[$clave]['Total_qty'] = ($value['Total_qty'] + $r['Quantity']);
            $report[$clave]['Total_weight'] = ($value['Total_weight'] + $r['Total Weight']);
            $igual = False;
        }
    }
    if($igual) {
        $report[] = $array;
    }
    $report[] = $array;
}
mysqli_close($conn2);
$conn3 =  mysqli_connect($db_host,$db_user,$db_pass,$db_db3) or die('Main Connection Error: ' . $conn3->error);
$q = "SELECT item_name, count(*) as Quantity,  SUM(item_weight) as 'Total Weight' FROM `inventory_items`  where item_type = 'Coil'  And inactive != 'Yes'  GROUP BY item_name";
$g = mysqli_query($conn3, $q) or die($conn3->error);
$igual = True;
while ($r = mysqli_fetch_array($g)) {

    $array = [
        'item_name' => $r['item_name'],
        'quantity_in' => $r['Quantity'],
        'Weight_in' => $r['Total Weight'],
        'Total_qty' => $r['Quantity'],
        'Total_weight' => $r['Total Weight']
    ];
    foreach ($report as $clave=>$value){
        $cadena1 =str_replace(' ', '', $r['item_name']);
        $cadena2 =str_replace(' ', '', $value['item_name']);
        if($cadena1 === $cadena2){
            $report[$clave] += [
                'quantity_in' => $r['Quantity'],
                'Weight_in' => $r['Total Weight']
            ];
            $report[$clave]['Total_qty'] = ($value['Total_qty'] + $r['Quantity']);
            $report[$clave]['Total_weight'] = ($value['Total_weight'] + $r['Total Weight']);
            $igual = False;
        }
    }
    if($igual) {
        $report[] = $array;
    }
}
mysqli_close($conn3);
$reportName = 'Coil Total Report';
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title><?php echo $reportName; ?></title>
</head>
<body>
<div class="container">
    <div class="row">
        <h1> <?php echo $reportName; ?></h1>
    </div>
    <div class="row">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Coil Color</th>
                    <th scope="col">QTY TX</th>
                    <th scope="col">Total Weight (lb) TX</th>
                    <th scope="col">QTY CA</th>
                    <th scope="col">Total Weight (lb) CA</th>
                    <th scope="col">QTY IN</th>
                    <th scope="col">Total Weight (lb) IN</th>
                    <th scope="col">Total QTY Acero</th>
                    <th scope="col">Total Weight (lb) Acero</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($report as $clave=>$value)
            {
                echo '<tr>
              <td>' . $value['item_name'] . '</td>
              <td>' . number_format($value['quantity_tx']) . '</td>
              <td>' . number_format($value['Weight_tx']) . '</td>
              <td>' . number_format($value['quantity_ca']) . '</td>
              <td>' . number_format($value['Weight_ca']) . '</td>
              <td>' . number_format($value['quantity_in']) . '</td>
              <td>' . number_format($value['Weight_in']) . '</td>
              <td>' . number_format($value['Total_qty']) . '</td>
              <td>' . number_format($value['Total_weight']) . '</td>
              </tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
