<?php
include '../../php/connection.php';
$reportName = 'Adjustments Report';

//Load Variables...

?>
  <html>

  <head>
    <title>
      <?php echo $reportName; ?>
    </title>
    <link href="../../jquery/jquery-ui.css" rel="stylesheet" />
    <style>
      /* page */
      html {
        font: 16px/1 "Open Sans", sans-serif;
        overflow: auto;
        padding: 0.5in;
      }
      html {
        background: #999;
        cursor: default;
      }
      body {
        box-sizing: border-box;
        min-height: 11in;
        margin: 0 auto;
        overflow: hidden;
        padding: 0.5in;
        width: 8.5in;
      }
      body {
        background: #FFF;
        border-radius: 1px;
        box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
      }
      th,
      td {
        border: 1px solid black;
        padding: 5px;
        font-size: 12px;
      }
      .btn {
        padding-left: 8px;
        padding-right: 8px;
        background: blue;
        border-radius: 25px;
        color: white;
      }
      @media print {
        * {
          -webkit-print-color-adjust: exact;
        }
        html {
          background: none;
          padding: 0;
        }
        body {
          box-shadow: none;
          margin: 0;
        }
        span:empty {
          display: none;
        }
        .add,
        .cut {
          display: none;
        }
      }
      @page {
        margin: 0;
      }
    </style>
  </head>

  <body>
    <h1 style="text-align:center;">
      <?php echo $reportName; ?>
    </h1>





    <table id="rec_table" style="/*margin:auto;*/">
      <thead>
        <tr style="background:lightgray;">
          <th>Record ID</th>
          <th>Date</th>
          <!--<th>Zone</th>-->
          <th>Item Name</th>
          <!--<th>Barcode</th>-->
          <th>Type</th>
          <th>Adjustment QTY</th>
          <th>Reference</th>
          <th>User</th>
        </tr>
      </thead>
      <tbody>
        <?php

$q = "SELECT * FROM `inventory_adjustments` WHERE `inactive` != 'Yes' AND `adjustment` != 0 ORDER BY `ID` DESC";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  
  $iq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $r['pid'] . "'";
  $ig = mysqli_query($conn, $iq) or die($conn->error);

  if(mysqli_num_rows($ig) <= 0){
  	//$item_name = '[DELETED ITEM]';
  	
  }else{
  	//$item_name = $ir['item_name'];
  	$ir = mysqli_fetch_array($ig);
  	if($r['adjustment'] > 0){
  		$sign = '+';
  	}else{
  		$sign = '';
  	}
  	
  	if($r['type'] == ''){
  		$type = 'Manual';
  	}else{
  		$type = $r['type'];
  	}
  	  echo '<tr>
  		  <td>' . $r['ID'] . '</td>
          <td>' . date("m/d/y", strtotime($r['date'])) . ' ' . date("h:iA",strtotime($r['time'])) . '</td>
          <!--<td>' . $r['zone'] . '</td>-->
          <td>' . $ir['item_name'] . '</td>
          <!--<td>' . $ir['barcode'] . '</td>-->
          <td>' . $type . '</td>
          <td>' . $sign . $r['adjustment'] . '</td>
          <td>' . $r['waste_number'] . $r['process_id'] . $r['receiving_number'] . $r['shipping_number'] . $r['invoice_number'] . '</td>
          <td>' . $r['user_name'] . '</td>
        </tr>';
  }


}
?>



      </tbody>
    </table>
  </body>
  <!--JQuery Files-->
  <script src="../../jquery/external/jquery/jquery.js"></script>
  <script src="../../jquery/jquery-ui.js"></script>
  <script>
    $(document).ready(function() {
      $(".date").datepicker();
    });
  </script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script>
    $("#rec_table").dataTable({
      "paging": false,
      "order": [[ 0, "DESC" ]]
    });
  </script>

  </html>