<?php
$db_host = 'localhost';
$db_user = 'marketfo_mf';
$db_pass = '#NgTFJQ!z@t8';
$db_mf_db = 'marketfo_marketforce';
$db_db = 'marketfo_mf_acerotx';
$conn = mysqli_connect($db_host,$db_user,$db_pass,$db_db) or die('Main Connection Error: ' . $conn->error);
$id_item = $_GET['item'];
$q = "SELECT * FROM `inventory_items` WHERE ID = '".$id_item."'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
$query = "SELECT * FROM inventory_requisition WHERE item_id =".$id_item." AND inactive = FALSE";
$action = mysqli_query($conn, $query) or die($conn->error);

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <title>Requisition report</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col" style="padding-top: 10px;">
                <h1>Order for <?php echo $r['item_name']?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-8">
                                <a type="button" class="btn btn-info" href="low-inventory-report.php">
                                Back
                                </a>
                            </div>
                            <div class="col-4">
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                    Add New
                                </button>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 20px;">
                            <div class="container">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th scope="col">Quote</th>
                                        <th scope="col">PO</th>
                                        <th scope="col">Quantity Inventory</th>
                                        <th scope="col">Requested amount</th>
                                        <th scope="col">PO Due Date</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    while($res = mysqli_fetch_array($action)){
                                        echo '<tr>
                                            <td>'.$res['quote'].'</td>
                                            <td>'.$res['po'].'</td>
                                            <td>'.$res['qty_inventory'].'</td>
                                            <td>'.$res['qty_requ'].'</td>
                                            <td>'.$res['po_due_date'].'</td>
                                            <td>
                                                 <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop2" onclick="id_item ='.$res['ID'].'">
                                                    PO Due Date
                                                 </button>
                                                 <a type="button" class="btn btn-danger" onclick="delete_requisition('.$res['ID'].')">Delete</a>
                                            </td>
                                        </tr>';
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal Form -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Add New Order</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="mb-3">
                                <label for="name" class="form-label">Item Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $r['item_name'];?>" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="datepicker" class="form-label">Quote Date</label>
                                    <input type="text" id="datepicker" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="qty_request" class="form-label">Requested amount</label>
                                    <input type="number" class="form-control" id="qty_request" name="qty_request" >
                                </div>
                            </div>
                        </div>
                        <div class="row" style="background-color: #ced4da;">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="datepicker2" class="form-label">Po Due Date</label>
                                    <input type="text" id="datepicker2" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                <label for="po" class="form-label">PO</label>
                                <input type="text" class="form-control" id="po" name="po"  >
                            </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" onclick="add_requisition(<?php echo $id_item;?>, <?php echo $r['qty'];?>)" data-bs-dismiss="modal">Save</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Due Date -->
    <div class="modal fade" id="staticBackdrop2" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Update Order</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="datepicker2" class="form-label">Po Due Date</label>
                                    <input type="text" id="datepicker3" class="form-control">
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" onclick="PO_due_date_requisition(<?php echo $id_item;?>, <?php echo $r['qty'];?>)" data-bs-dismiss="modal">Save</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script>
        $('#datepicker').datepicker({ format: 'yyyy-mm-dd' });
        $('#datepicker2').datepicker({ format: 'yyyy-mm-dd' });
        $('#datepicker3').datepicker({ format: 'yyyy-mm-dd' });
        var id_item = 0;

        function add_requisition(id, qty){
            const q_date = $("#datepicker").val();
            const po_due_date = $("#datepicker2").val();
            const r_amount = $("#qty_request").val();
            const po = $("#po").val();
            console.log(po_due_date);
            $.ajax({
                type: "POST",
                url: "php/add-requisition.php",
                dataType: "json",
                data: {
                    "id": id,
                    "qty": qty,
                    "q_date": q_date,
                    "po_due_date": po_due_date,
                    "r_amount": r_amount,
                    "po": po
                },
                success: function(response){
                    $("#datepicker").val("");
                    $("#datepicker2").val("");
                    $("#qty_request").val("");
                    $("#po").val("");
                    console.log(response);
                    location.reload();
                }
            }).always(function () {
                $("#datepicker").val("");
                $("#datepicker2").val("");
                $("#qty_request").val("");
                $("#po").val("");
                location.reload();
            });
        }

        function update_requisition(id){
            $.ajax({
                type: "POST",
                url: "php/update-requisition.php",
                dataType: "json",
                data: {
                    "id": id,
                },
                success: function(response){
                    console.log(response);
                    location.reload();
                }
            }).always(function () {
                location.reload();
            });
        }

        function delete_requisition(id){
            $.ajax({
                type: "POST",
                url: "php/incative-requisition.php",
                dataType: "json",
                data: {
                    "id": id,
                },
                success: function(response){
                    console.log(response);
                    location.reload();
                }
            }).always(function () {
                location.reload();
            });
        }

        function PO_due_date_requisition(id){
            const po_due_date = $("#datepicker3").val();
            $.ajax({
                type: "POST",
                url: "php/update-po-requisition.php",
                dataType: "json",
                data: {
                    "id": id_item,
                    "po_due_date": po_due_date
                },
                success: function(response){
                    console.log(response);
                    $("#datepicker3").val("");
                    location.reload();
                }
            }).always(function () {
                $("#datepicker3").val("");
                location.reload();
            });
        }
    </script>
</body>
</html>

