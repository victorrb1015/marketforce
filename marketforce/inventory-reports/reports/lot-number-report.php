<?php
include '../../php/connection.php';
$reportName = 'Lot Number Report';

//Load Variables...
$lot_num = $_REQUEST['lot_num'];

?>

  <html>

  <head>
    <title>
      <?php echo $reportName; ?>
    </title>
    <link href="../../jquery/jquery-ui.css" rel="stylesheet" />
    <style>
      /* page */

      html {
        font: 16px/1 "Open Sans", sans-serif;
        overflow: auto;
        padding: 0.5in;
      }

      html {
        background: #999;
        cursor: default;
      }

      body {
        box-sizing: border-box;
        min-height: 11in;
        margin: 0 auto;
        overflow: hidden;
        padding: 0.5in;
        width: 8.5in;
      }

      body {
        background: #FFF;
        border-radius: 1px;
        box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
      }

      th,
      td {
        border: 1px solid black;
        padding: 5px;
        font-size:12px;
      }

      .btn {
        padding-left: 8px;
        padding-right: 8px;
        background: blue;
        border-radius: 25px;
        color: white;
      }

      @media print {
        * {
          -webkit-print-color-adjust: exact;
        }
        html {
          background: none;
          padding: 0;
        }
        body {
          box-shadow: none;
          margin: 0;
        }
        span:empty {
          display: none;
        }
        .add,
        .cut {
          display: none;
        }
      }

      @page {
        margin: 0;
      }
    </style>
  </head>

  <body>
    <h1 style="text-align:center;">
      <?php echo $reportName; ?>
    </h1>

  <?php
    if($_REQUEST['lot_num']){
      
      echo '<div style="margin:auto;text-align:center;">
                <h4>Lot Number: ' . $lot_num . '</h4>
                <a href="">Try Another Lot Number</a>
            </div>';
      
      //Get Product_id
      $pq = "SELECT * FROM `inv_man_processes_run` WHERE `lot_number` = '" . $lot_num . "'";
      $pg = mysqli_query($conn, $pq) or die($conn->error);
      $pr = mysqli_fetch_array($pg);
      $pid = $pr['process_id'];
      $pqty = $pr['process_qty'];
      $pdate = $pr['date'];
      $ptime = $pr['time'];

      
      echo '<table id="rec_table" style="/*margin:auto;*/">
      <thead>
        <tr style="background:lightgray;">
          <th>Date</th>
          <th>Barcode</th>
          <th>Item</th>
          <th>Qty</th>
          <th>Units</th>
          <th>Action</th>
          <th>User</th>
        </tr>
      </thead>
      <tbody>';
        
      $q = "SELECT * FROM `inv_man_process_items` WHERE `inactive` != 'Yes' AND `process_id` = '" . $pid . "'";
      $g = mysqli_query($conn, $q) or die($conn->error);
      while($r = mysqli_fetch_array($g)){
          $iq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $r['item_id'] . "'";
          $ig = mysqli_query($conn, $iq) or die($conn->error);
          $ir = mysqli_fetch_array($ig);
          $item_name = $ir['item_name'];
          $barcode = $ir['barcode'];
          $units = $ir['item_units'];

          $action_qty = ($r['qty'] * $pqty);

        echo '<tr>
                <td>' . date("m/d/y",strtotime($pr['date'])) . ' ' . date("h:iA",strtotime($pr['time'])) . '</td>
                <td>' . $barcode . '</td>
                <td>' . $item_name . '</td>
                <td>' . $action_qty . '</td>
                <td>' . $units . '</td>
                <td>' . $r['mode'] . '</td>
                <td>' . $r['user_name'] . '</td>
              </tr>';
      }
      
    }else{
      
      echo '<div style="margin:auto;text-align:center;">
              <p>Please enter the Lot Number to view it\'s records</p>
              <form action="#" method="POST">
                <input type="text" id="lot_num" name="lot_num" placeholder="Lot Number">
              </form>
            </div>';
      
    }
    
?>



      </tbody>
    </table>



  </body>
  <!--JQuery Files-->
  <script src="../../jquery/external/jquery/jquery.js"></script>
  <script src="../../jquery/jquery-ui.js"></script>
  <script>
    $(document).ready(function() {
      $(".date").datepicker();
    });
  </script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script>
    $("#rec_table").dataTable({
      "paging": false
    });
  </script>

  </html>