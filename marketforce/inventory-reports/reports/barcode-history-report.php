<?php
include '../../php/connection.php';
$reportName = 'Barcode History Report';

//Load Variables...
$barcode = $_REQUEST['barcode'];

?>

  <html>

  <head>
    <title>
      <?php echo $reportName; ?>
    </title>
    <link href="../../jquery/jquery-ui.css" rel="stylesheet" />
    <style>
      /* page */

      html {
        font: 16px/1 "Open Sans", sans-serif;
        overflow: auto;
        padding: 0.5in;
      }

      html {
        background: #999;
        cursor: default;
      }

      body {
        box-sizing: border-box;
        min-height: 11in;
        margin: 0 auto;
        overflow: hidden;
        padding: 0.5in;
        width: 8.5in;
      }

      body {
        background: #FFF;
        border-radius: 1px;
        box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
      }

      th,
      td {
        border: 1px solid black;
        padding: 5px;
        font-size:12px;
      }

      .btn {
        padding-left: 8px;
        padding-right: 8px;
        background: blue;
        border-radius: 25px;
        color: white;
      }

      @media print {
        * {
          -webkit-print-color-adjust: exact;
        }
        html {
          background: none;
          padding: 0;
        }
        body {
          box-shadow: none;
          margin: 0;
        }
        span:empty {
          display: none;
        }
        .add,
        .cut {
          display: none;
        }
      }

      @page {
        margin: 0;
      }
    </style>
  </head>

  <body>
    <h1 style="text-align:center;">
      <?php echo $reportName; ?>
    </h1>

  <?php
    if($_REQUEST['barcode']){
      
      echo '<div style="margin:auto;text-align:center;">
                <h4>Lot Number: ' . $barcode . '</h4>
                <a href="">Try Another Barcode</a>
            </div>';
      
      //Get Product_id
      $pq = "SELECT * FROM `inventory_items` WHERE `barcode` = '" . $barcode . "'";
      $pg = mysqli_query($conn, $pq) or die($conn->error);
      $pr = mysqli_fetch_array($pg);
      $pid = $pr['ID'];
      
      echo '<table id="rec_table" style="/*margin:auto;*/">
      <thead>
        <tr style="background:lightgray;">
          <th>Date</th>
          <th>Record Type</th>
          <th>Adjustment</th>
          <th>Reference</th>
          <th>Notes</th>
          <th>User</th>
        </tr>
      </thead>
      <tbody>';
        
      $q = "SELECT * FROM `inventory_adjustments` WHERE `inactive` != 'Yes' AND `pid` = '" . $pid . "'";
      $g = mysqli_query($conn, $q) or die($conn->error);
      while($r = mysqli_fetch_array($g)){
        if($r['type'] == 'Manufacturing Process' && $r['process_run_id'] != ''){
          $lq = "SELECT * FROM `inv_man_processes_run` WHERE `inactive` != 'Yes' AND `process_run_id` = '" . $r['process_run_id'] . "'";
          $lg = mysqli_query($conn, $lq) or die($conn->error);
          $lr = mysqli_fetch_array($lg);
          $lot_num = $lr['lot_number'];
          $lot_message = 'Lot Number: ' . $lot_num;
        }
        echo '<tr>
                <td>' . date("m/d/y",strtotime($r['date'])) . ' ' . date("h:iA",strtotime($r['time'])) . '</td>
                <td>' . $r['type'] . '</td>
                <td>' . $r['adjustment'] . '</td>
                <td>' . $r['waste_number'] . $r['process_run_id'] . $r['receiving_number'] . $r['shipping_number'] . $r['invoice_number'] . '</td>
                <td>' . $r['reason'] . $lot_message . '</td>
                <td>' . $r['user_name'] . '</td>
              </tr>';
      }
      
    }else{
      
      echo '<div style="margin:auto;text-align:center;">
              <p>Please enter the barcode to view it\'s history records</p>
              <form action="#" method="POST">
                <input type="text" id="barcode" name="barcode" placeholder="Barcode">
              </form>
            </div>';
      
    }
    
?>



      </tbody>
    </table>



  </body>
  <!--JQuery Files-->
  <script src="../../jquery/external/jquery/jquery.js"></script>
  <script src="../../jquery/jquery-ui.js"></script>
  <script>
    $(document).ready(function() {
      $(".date").datepicker();
    });
  </script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script>
    $("#rec_table").dataTable({
      "paging": false
    });
  </script>

  </html>