<?php
include '../../php/connection.php';

echo '
<html>
<head>
<title>Negative QOH Report</title>
<link href="../../jquery/jquery-ui.css" rel="stylesheet" />
<style>
/* page */

html { font: 16px/1 "Open Sans", sans-serif; overflow: auto; padding: 0.5in; }
html { background: #999; cursor: default; }

body { box-sizing: border-box; min-height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }

th, td{
  border:1px solid black;
  padding:5px;
}

@media print {
	* { -webkit-print-color-adjust: exact; }
	html { background: none; padding: 0; }
	body { box-shadow: none; margin: 0; }
	span:empty { display: none; }
	.add, .cut { display: none; }
}

@page { margin: 0; }
</style>
</head>
<body>
<h1 style="text-align:center;">Negative QOH Report</h1>
';



  
 echo '<table id="qoh_table" style="margin:auto;">
        <thead>
         <tr style="background:lightgray;">
         <th>Product Barcode</th>
         <th>Product Name</th>
         <th>Current QOH</th>
         <th>Last Adjustment</th>
         </tr>
         </thead>
         <tbody>';

$iiq = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND `qty` < 0 ORDER BY FIELD(`item_type`, 'Raw', 'Component', 'Product')";
$iig = mysqli_query($conn, $iiq) or die($conn->error);
while($iir = mysqli_fetch_array($iig)){
  echo '<tr>
          <td>' . $iir['barcode'] . '</td>
          <td>' . $iir['item_name'] . '</td>
          <td>' . $iir['qty'] . '</td>';
  //Get Last Adjustment Action...
  $iaq = "SELECT * FROM `inventory_adjustments` WHERE `inactive` != 'Yes' AND `pid` = '" . $iir['ID'] . "' ORDER BY `ID` DESC";
  $iag = mysqli_query($conn, $iaq) or die($conn->error);
  $iar = mysqli_fetch_array($iag);
  $last_action = $iar['type'];
    echo '<td>' . $last_action . ' [' . date('m/d/y',strtotime($iar['date'])) . ']</td>';
  
  echo '</tr>';
}

echo '</tbody>
      </table>';

echo '';



echo '</body>
<!--JQuery Files-->
        <script src="../../jquery/external/jquery/jquery.js"></script>
        <script src="../../jquery/jquery-ui.js"></script>
        <script>
          $(document).ready(function(){
            $( ".date" ).datepicker();
          });
        </script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
  $("#qoh_table").dataTable({
    "paging": false
  });
</script>
</html>';

?>