<?php
include '../../php/connection.php';

echo '
<html>
<head>
<title>Receiving Manifests Report</title>
<link href="../../jquery/jquery-ui.css" rel="stylesheet" />
<style>
/* page */

html { font: 16px/1 "Open Sans", sans-serif; overflow: auto; padding: 0.5in; }
html { background: #999; cursor: default; }

body { box-sizing: border-box; min-height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }

th, td{
  border:1px solid black;
  padding:5px;
}

.btn{
  padding-left: 8px;
  padding-right: 8px;
  background: blue;
  border-radius: 25px;
  color:white;
}

@media print {
	* { -webkit-print-color-adjust: exact; }
	html { background: none; padding: 0; }
	body { box-shadow: none; margin: 0; }
	span:empty { display: none; }
	.add, .cut { display: none; }
}

@page { margin: 0; }
</style>
</head>
<body>
<h1 style="text-align:center;">Receiving Manifests Report</h1>
';



  
 echo '<table id="rec_table" style="margin:auto;">
        <thead>
         <tr style="background:lightgray;">
         <th>Date</th>
         <th>Manifest Number</th>
         <th>Reference Number</th>
         <th>Created By</th>
         <th>Actions</th>
         </tr>
         </thead>
         <tbody>';

$iiq = "SELECT * FROM `inv_rec_manifests` WHERE `inactive` != 'Yes' ORDER BY `date` DESC";
$iig = mysqli_query($conn, $iiq) or die($conn->error);
while($iir = mysqli_fetch_array($iig)){
  echo '<tr>
          <td>' . date("m/d/y", strtotime($iir['date'])) . '</td>
          <td>' . $iir['receiving_number'] . '</td>
          <td>' . $iir['rec_ref'] . '</td>
          <td>' . $iir['user_name'] . '</td>
          <td style="text-align:center;">
            <a class="btn" href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/inventory-receiving/rec-manifest.php?id=' . $iir['ID'] . '" target="_blank">
              View
            </a>
          </td>';
  echo '</tr>';
}

echo '</tbody>
      </table>';



echo '</body>
<!--JQuery Files-->
        <script src="../../jquery/external/jquery/jquery.js"></script>
        <script src="../../jquery/jquery-ui.js"></script>
        <script>
          $(document).ready(function(){
            $( ".date" ).datepicker();
          });
        </script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
  $("#rec_table").dataTable({
    "paging": false
  });
</script>
</html>';

?>