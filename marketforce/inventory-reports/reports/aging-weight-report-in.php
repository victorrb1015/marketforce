<?php
include '../../php/connection.php';


echo '
<html>
<head>
<title>Aging & Weight Report Indiana</title>
<link href="../../jquery/jquery-ui.css" rel="stylesheet" />
<style>
/* page */

html { font: 16px/1 "Open Sans", sans-serif; overflow: auto; padding: 0.5in; }
html { background: #999; cursor: default; }

body { box-sizing: border-box; min-height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }

th, td{
  border:1px solid black;
  padding:5px;
}

.btn{
  padding-left: 8px;
  padding-right: 8px;
  background: blue;
  border-radius: 25px;
  color:white;
}

@media print {
	* { -webkit-print-color-adjust: exact; }
	html { background: none; padding: 0; }
	body { box-shadow: none; margin: 0; }
	span:empty { display: none; }
	.add, .cut { display: none; }
}

@page { margin: 0; }
</style>
</head>
<body>
<h1 style="text-align:center;">Aging & Weight Report Indiana</h1>
';



  
 echo '<table id="rec_table" style="margin:auto;">
        <thead>
         <tr style="background:lightgray;">
         <th>Date</th>
         <th>Item Type</th>
         <th>Barcode</th>
         <th>Item Name</th>
         <th>Item Info</th>
         <th>Days In Stock</th>
         <th>Weight (lb)</th>';
//         <th>Actions</th>
  echo       '</tr>
         </thead>
         <tbody>';

$iiq = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND `zone` = 'IN' ORDER BY `date` DESC";
$iig = mysqli_query($conn, $iiq) or die($conn->error);
while($iir = mysqli_fetch_array($iig)){
 
  $date1 = strtotime("today");
  $date2 = strtotime($iir['date']);
  $diff = abs($date1 - $date2);
  $years = floor($diff / (365*60*60*24)); 
  $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
  $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
  $totaldays = floor(($diff)/ (60*60*24));
  
  echo '<tr>
          <td>' . date("m/d/y", strtotime($iir['date'])) . '</td>
          <td>' . $iir['item_type'] . '</td>
          <td>' . $iir['barcode'] . '</td>
          <td>' . $iir['item_name'] . '</td>
          <td>' . $iir['item_info'] . '</td>
          <td>' . $totaldays . '</td>
          <td>' . $iir['item_weight'] . '</td>
        </tr>';
  
  //        <td style="text-align:center;">
    //        <a class="btn" href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/inventory-receiving/rec-manifest.php?id=' . $iir['ID'] . '" target="_blank">
      //        View
        //    </a>
          //</td>
        //</tr>';
}

$wiq = "SELECT SUM(item_weight) FROM `inventory_items` WHERE `inactive` != 'Yes' AND `zone` = 'IN'";
$wig = mysqli_query($conn, $wiq) or die($conn->error);
while($wir = mysqli_fetch_array($wig)){
echo '  <tr>
          <td><b>TOTAL WEIGHT</b></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td><b>' . round($wir['SUM(item_weight)']) . '</b></td>
        </tr>';
}

echo '</tbody>
     </table>';



echo '</body>
<!--JQuery Files-->
        <script src="../../jquery/external/jquery/jquery.js"></script>
        <script src="../../jquery/jquery-ui.js"></script>
        <script>
          $(document).ready(function(){
            $( ".date" ).datepicker();
          });
        </script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
  $("#rec_table").dataTable({
    "paging": false
  });
</script>
</html>';

?>