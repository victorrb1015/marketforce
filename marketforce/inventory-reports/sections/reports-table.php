<div class="table-responsive">
  <table class="table table-bordered table-hover table-striped packet-table">
    <thead>
      <tr style="background: #212121;">
        <th>Report Name</th>
        <th>Report Description</th>
        <th>View</th>
      </tr>
    </thead>
    <tbody>
      
       <tr>
        <td><b>Inventory Count Report</b></td>
        <td>This report displays the current on hand inventory with space to write in actual on hands.</td>
        <td>
          <a href="inventory-reports/reports/inventory-count-report.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
       <tr>
        <td><b>Coil Count Report</b></td>
        <td>This report displays the current on hand Coils with space to write in actual on hands.</td>
        <td>
          <a href="inventory-reports/reports/coil-count-report.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
       <tr>
        <td><b>Barcode History Report</b></td>
        <td>This report displays the history of activity for a specific barcode.</td>
        <td>
          <a href="inventory-reports/reports/barcode-history-report.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
       <tr>
        <td><b>Lot Number Report</b></td>
        <td>This report displays the activity for a specific Lot Number.</td>
        <td>
          <a href="inventory-reports/reports/lot-number-report.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
      <tr>
        <td><b>Negative QOH Report</b></td>
        <td>This report displays all the inventory with negative quantity on-hand.</td>
        <td>
          <a href="inventory-reports/reports/negative-qoh-report.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
      <tr>
        <td><b>Receiving Manifests Report</b></td>
        <td>This report displays all the Receiving Manifests generated in the Inventory System.</td>
        <td>
          <a href="inventory-reports/reports/receiving-manifests-report.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
      <tr>
        <td><b>Shipping Manifests Report</b></td>
        <td>This report displays all the Shipping Manifests generated in the Inventory System.</td>
        <td>
          <a href="inventory-reports/reports/shipping-manifests-report.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
      <tr>
        <td><b>QOH Adjustments Report</b></td>
        <td>This report displays all the QOH adjustments made in the Inventory System.</td>
        <td>
          <a href="inventory-reports/reports/adjustments-report.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
      <tr>
        <td><b>Low Inventory Report</b></td>
        <td>This report displays all the items with current QOH below their set minimum threshold in the Inventory System.</td>
        <td>
            <a href="inventory-reports/reports/low-inventory-report.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
     <tr>
        <td><b>Aging and Weight Report Global</b></td>
        <td>This report displays the Items with their individual and collective weight, as well as the days they have been sitting in the Inventory System for all Zones.</td>
        <td>
          <a href="inventory-reports/reports/aging-weight-report.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
       <tr>
        <td><b>Coils Report</b></td>
        <td>This report displays the list of all coil color currently in-stock with total quantity and weight for each coil color.</td>
        <td>
          <a href="inventory-reports/reports/coil-total-report.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>

       <tr>
        <td><b>Minimum Threshold Coil</b></td>
        <td>This report shows all items with current QOH below the minimum set in the system .</td>
        <td>
          <a href="inventory-reports/reports/minimum-inventory-coil.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i>
              View
            </span>
          </a>
        </td>
      </tr>

       <tr>
        <td><b>Minimum Threshold Component</b></td>
        <td>This report shows all items with current QOH below the minimum set in the system .</td>
        <td>
          <a href="inventory-reports/reports/minimum-inventory-components.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i>
              View
            </span>
          </a>
        </td>
      </tr>

      
      <!--<tr>
        <td><b>Aging and Weight Report</b><b style="color: orange;"> Indiana</b></td>
        <td>This report displays the Items with their individual and collective weight, as well as the days they have been sitting in the Inventory System for Indiana.</td>
        <td>
          <a href="inventory-reports/reports/aging-weight-report-in.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
      <tr>
        <td><b>Aging and Weight Report</b><b style="color: lightgreen;"> North Carolina</b></td>
        <td>This report displays the Items with their individual and collective weight, as well as the days they have been sitting in the Inventory System for North Carolina.</td>
        <td>
          <a href="inventory-reports/reports/aging-weight-report-nc.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
      <tr>
        <td><b>Aging and Weight Report</b><b style="color: peachpuff;"> Oklahoma</b></td>
        <td>This report displays the Items with their individual and collective weight, as well as the days they have been sitting in the Inventory System for Oklahoma.</td>
        <td>
          <a href="inventory-reports/reports/aging-weight-report-ok.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
      <tr>
        <td><b>Aging and Weight Report</b><b style="color: cyan;"> Georgia</b></td>
        <td>This report displays the Items with their individual and collective weight, as well as the days they have been sitting in the Inventory System for Georgia.</td>
        <td>
          <a href="inventory-reports/reports/aging-weight-report-ga.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
       <tr>
        <td><b>Aging and Weight Report </b><b style="color: yellow;">California</b></td>
        <td>This report displays the Items with their individual and collective weight, as well as the days they have been sitting in the Inventory System for California.</td>
        <td>
          <a href="inventory-reports/reports/aging-weight-report-ca.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
       <tr>
        <td><b>Aging and Weight Report</b><b style="color: magenta;"> Texas</b></td>
        <td>This report displays the Items with their individual and collective weight, as well as the days they have been sitting in the Inventory System for Texas.</td>
        <td>
          <a href="inventory-reports/reports/aging-weight-report-tx.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>
      
              <td><b>Aging and Weight Report</b><b style="color: green;"> México</b></td>
        <td>This report displays the Items with their individual and collective weight, as well as the days they have been sitting in the Inventory System for México.</td>
        <td>
          <a href="inventory-reports/reports/aging-weight-report-mx.php" target="_blank">
            <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;">
              <i class="fa fa-eye"></i> 
              View
            </span>
          </a>
        </td>
      </tr>-->
      
    </tbody>
  </table>
</div>