<?php
include 'security/session/session-settings.php';

if(!isset($_SESSION['in'])){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';

//if($_SESSION['admin'] == 'Yes'){
if($_SESSION['in'] == 'Yes'){
  $a = 'Yes';
}else{
  $a = 'No';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

		<!--JQuery CSS-->
    <link href="jquery/jquery-ui.css" rel="stylesheet">

	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
		.main li{
			float: left;
		}
		.main li li{
			float: none;
		}
		li{
			list-style-type: none;
		}
    .result_table td, th{
      padding: 15px;
      border: 1px solid black;
    }
    .result_table a{
      color: blue;
    }
    .result_table tr:hover{
      background: grey;
    }
    .image_gallery img{
      padding: 5px;
    }
		
		<?php
		if($_SESSION['dealer_activation'] != 'Yes'){
			echo '#hider-th{
				visibility: hidden;
			}
			#hider-td{
				visibility: hidden;
			}';
		}
		?>
  </style>
<?php include 'dealer-lookup/js/main-functions.php'; ?>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> <?php switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }?> Lookup</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                          <li class="active">
                                <i class=" fa fa-fw fa-file"></i> <?php switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }?> Lookup
                          </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							<div class="row">
                <div class="top">
                  <h2>
                    Search for a <?php if($_SESSION['org_id'] == '123456'){echo 'customer';}else{echo 'dealer';}?> to lookup information:
                  </h2>
                  <input style="width:45%; height:35px;" type="text" name="search" id="search" placeholder="Search Here..." onkeyup="searcher(this.value);" autofocus/>
                  <br><br>
                  <div id="search_results"></div>
                  <h1 class="page-header"></h1>
                </div>
                <br><br>
                <div id="accordion"></div>
              </div>
							
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

	
	<!-- Modal -->
  <div class="modal fade" id="intNote" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Log An Internal Note For <span style="color:red;" id="int_name"></span></h4>
        </div>
        <div class="modal-body">
          <p>Enter A Note Here (max 5,000 characters):</p>
					<textarea id="inote" class="form-control" style="height:250px;" maxlength="5000"></textarea>
					<input type="hidden" id="int_id" value="" />
					<br><br>
					<p id="int_error_msg" style="color:red;font-weight:bold;"></p>
        </div>
        <div class="modal-footer">
					<button type="button" class="btn btn-success" onclick="log_inote();" data-dismiss="modal">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	
	
    <!-- jQuery -->
    <script src="js/jquery.js"></script>


 <?php include 'footer.html'; ?>
	
	<!-- Modals -->
  <?php include 'lookup/modals/reassign-rep-modal.php'; ?>
  
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    
	
	  <!--JQuery Files-->
    <script src="jquery/external/jquery/jquery.js"></script>
    <script src="jquery/jquery-ui.js"></script>
    <script>

			
   $(document).ready(function(){
  $("#dealer_select").change(function(){
		
    $( "#accordion" ).accordion({
      collapsible: true
    });
  });
	 });
  
  </script>
	
  <script src="lookup/js/lookup-functions.js"></script>
	
</body>

</html>