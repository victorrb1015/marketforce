<?php
ini_set('session.save_path', '/home/marketforce/mf_temp');
session_start();
$db_host = 'localhost';
$db_user = 'marketfo_mf';
$db_pass = '#NgTFJQ!z@t8';
$db_mf_db = 'marketfo_marketforce';
$db_db = $_SESSION['org_db_name'];
$mysql0 = new mysqli($db_host, $db_user, $db_pass, 'marketfo_marketforce');
$sql_terms = "SELECT terms from organizations where org_id like '" . $_SESSION['org_id'] . "'";
$result0 = $mysql0->query($sql_terms);
$res_array = $result0->fetch_array();

$mysqli = new mysqli($db_host, $db_user, $db_pass, $db_db);

include 'security/session/session-settings.php';
$sql_category = "SELECT * FROM `man_category` where inactive = 0 ORDER BY name";
$result_category = $mysqli->query($sql_category);


?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/spinner.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
    <!-- SELECT2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>

    <title>Manufacture</title>
    <style>
        body {
            background-color: black;
        }

    </style>
</head>

<body>
<div class="m-3">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="card">
                    <div class="card-header">
                        <h5 class="text-center fw-bolder">Category</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <select id="category" class="form-select" name="category" onchange="getCategory();">
                                    <?php
                                    while ($dr = $result_category->fetch_array()) {
                                        echo '<option value="' . $dr['ID'] . '">' . $dr['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <a type="button" class="btn btn-success" data-bs-toggle="modal"
                                       data-bs-target="#new_category"><i class="fa-solid fa-circle-plus"></i>
                                        New Category</a>
                                    <a type="button" class="btn btn-warning"><i class="fa-solid fa-pen-clip"></i>
                                        Edit Category</a>
                                    <a type="button" class="btn btn-danger"><i class="fa-solid fa-circle-xmark"></i>
                                        Delete Category</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="text-center fw-bolder">SubCategory</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div id="sub_category_section"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <a type="button" class="btn btn-success"><i class="fa-solid fa-circle-plus"></i>
                                        New Sub
                                        Category</a>
                                    <a type="button" class="btn btn-warning"><i class="fa-solid fa-pen-clip"></i>
                                        Edit Sub
                                        Category</a>
                                    <a type="button" class="btn btn-danger"><i class="fa-solid fa-circle-xmark"></i>
                                        Delete Sub
                                        Category</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="text-center fw-bolder">Process</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="d-grid gap-2 d-md-block">
                                    <a type="button" class="btn btn-primary"><i class="fa-solid fa-circle-info"></i>
                                        View ALl Process Finish</a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <a type="button" class="btn btn-success"><i class="fa-solid fa-circle-plus"></i>
                                        New Process</a>
                                </div>
                            </div>
                        </div>
                        <section id="cards">
                            <div class="row pt-3">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="text-center fw-bolder">List of Process</h5>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h5 class="text-center fw-bolder">Proces Name</h5>
                                                        </div>
                                                        <div class="card-body">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 px-4">
                                    <div class="row">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="text-center fw-bolder">Process in progress</h5>
                                            </div>
                                            <div class="card-body">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="text-center fw-bolder">Process Finished Today</h5>
                                            </div>
                                            <div class="card-body">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="new_category" tabindex="-1" aria-labelledby="modal_new_category" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_new_category">Add / Edit Category</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="mb-3">
                        <label for="name_category" class="form-label">Name</label>
                        <input type="text" class="form-control" id="name_category">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" id="save_new_category" class="btn btn-primary" data-bs-dismiss="modal">Save
                    changes
                </button>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/d26731de46.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(document).ready(function () {
        $('#category').select2();
        getCategory();
    });


    function getCategory() {
        var category = $('#category').val();
        $.ajax({
            type: "POST",
            url: "php/get_info.php",
            data: {
                action: 'get_subcategory',
                ID: category
            },
            success: function (data) {
                console.log(data);
                $('#sub_category_section').empty();
                $('#sub_category_section').css('padding-top', '15px');
                $('#sub_category_section').css('padding-bottom', '15px');
                var select = $("<select></select>").attr("id", "sub_category").attr("name", "sub_category").attr('style', "width: 100%;");
                $.each(data.sub_categories, function (index, json) {
                    select.append($("<option></option>").attr("value", json.ID).text(json.name));
                });
                $('#sub_category_section').append(select);
                $('#sub_category').select2();
            }
        });
    }
</script>

</body>

</html>