<?php
include 'security/session/session-settings.php';
include 'php/connection.php';

if($_SESSION['org_id'] == '257823'){$pageName = 'Transfers (For internal coil transfers ONLY)';}else{$pageName = 'Shipping';};
$pageIcon = 'fas fa-shipping-fast';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>MarketForce | All Steel</title>
    <?php include 'global/sections/head.php'; ?>
</head>

<body>
	<!-- Preloader -->
	<?php include 'global/sections/preloader.php'; ?>
	<!-- /Preloader -->
    <div class="wrapper theme-4-active pimary-color-red">

    	<!--Navigation-->
    	<?php include 'global/sections/nav.php'; ?>
		
		
        <!-- Main Content -->
		<div class="page-wrapper"><!--Includes Footer-->

      <div class="container-fluid pt-25">
				<?php include 'global/sections/page-title-bar.php'; ?>
        
        <!--Main Content Here-->

        <!-- Title -->
    		<?php include 'inventory-shipping/sections/title.php'; ?>
        
        <!-- Barcode -->
        <?php include 'inventory-shipping/sections/barcode.php'; ?>
        
        <!-- Table -->
        <?php include 'inventory-shipping/sections/items-table.php'; ?>
        
					<button type="button" class="btn btn-default" onclick="clear_items();">Clear</button>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmShippingModal">Confirm</button>
        
				<!-- /Table -->
			
			<?php include 'inventory-shipping/modals/confirm-shipping-modal.php'; ?>

			<!-- Footer -->
			<?php include 'global/sections/footer.php'; ?>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!--Footer-->
	<?php include 'global/sections/includes.php'; ?>
  <script src="inventory-shipping/js/shipping-functions.js"></script>
</body>

</html>
