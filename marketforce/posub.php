<?php
include 'php/connection.php';
echo '<html lang="en">
      <head>
      <title>Pre-Owned Subscriber List</title>
      <style>
        td, th{
          padding: 5px;
          border: 2px solid black;
        }
        th{
          background: grey;
        }
      </style>
      </head>
      <body>
      <u><h1>Pre-Owned Subscribers:</h1></u>
      <table>
      <tr>
      <th>Email</th>
      <th>Reference</th>
      <th>Other?</th>
      </tr>';
$q = "SELECT * FROM `subscribers` ORDER BY `ID` DESC";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  $ref = $r['reference'];
  if($ref == ''){
    $ref = 'Unknown';
  }
  $txt = $r['other_txt'];
  if($txt == ''){
    $txt = 'N/A';
  }
  echo '<tr>
        <td>' . $r['email'] . '</td><td>' . $ref . '</td><td>' . $txt . '</td>
        </tr>';
}
echo '</table>
      </body>
      </html>';
?>