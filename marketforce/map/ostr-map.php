<?php
/*------------------------------------CHANGE RECORDS--------------------------------------------*/
/*
1. 
*/
/*-----------------------------------END CHANGE RECORDS-----------------------------------------*/
include '../php/connection.php';

$quot = "'";
$sarray = array(); //Array to be filled with names and IDs for search function

echo '<script>';
$q = "SELECT * FROM `users` WHERE `inactive` != 'Yes' AND `position` = 'Outside Sales Rep'";
$eg = mysqli_query($conn, $q) or die($conn->error);
while($er = mysqli_fetch_array($eg)){
		$eid = $er['ID'];
    $email = $er['email'];
	echo 'var lat' . $er['ID'] . ';
        var lng' . $er['ID'] . ';
				var deviceName = "";
				';
  /*echo 'if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
        var q = JSON.parse(this.responseText);
        //alert(q.ID+"-->"+q.lat);
        var lat' . $er['ID'] . ' = parseFloat(q.lat);
        var lng' . $er['ID'] . ' = parseFloat(q.lng);
        //alert(lat' . $er['ID'] . ');
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/map/locator/fetch-location.php?id=' . $er['ID'] . '",true);
  xmlhttp.send();';
  */
}
echo '</script>';
?>
<html>
  <head>
		<!--<meta http-equiv="Cache-control" content="public">-->
    <title>Market Force Map Test</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://use.fontawesome.com/70c95f2a09.js"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
        //margin-top: -55%;
        //z-index: -9;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
  height: 45px;
  opacity: 0.9;
}
      #pac-input:focus {
  border-color: #4d90fe;
}
      .pac-controls {
  display: inline-block;
  padding: 5px 11px;
}

.pac-controls label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}
      #top{
        width: 40%;
        //border: 2px solid red;
        text-align: center;
        z-index: 1;
        position: fixed;
        margin-right: 30%;
        margin-left: 30%;
      }
      #pac-input{
        //width:100%;
      }
      #results{
        background-color: #ffffff;
        width: 40%;
        max-height: 50%;
        overflow: scroll;
        position: fixed;
        margin-left: 60%;
        opacity: 0.9;
        z-index: 2;
      }
      #results p{
        font-size: 13px;
      }
      #results p:hover{
        background-color: #E3E3E6;
        cursor: pointer;
      }
      #results a{
        text-decoration: none;
        color: black;
      }
      h5{
        color:red;
      }
      h4{
        color:blue;
      }
      #main{
        width: 100%;
      }
  </style>
    <script>
      //This is the area for the search functionality...
        function searcher(val){
					var val = val.replace('&','%26');
        if(val === ""){
        document.getElementById("results").innerHTML = "";
        document.getElementById("results").style.visibility = "hidden";
        return;
      }
        if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //alert(xmlhttp.reponseText);
      if(xmlhttp.responseText == 1){
        document.getElementById("results").innerHTML = "<h3 style='text-align:center;'><u>Dealer List:</u></h3><h4>No Results Found...</h4>";
        document.getElementById("results").style.visibility = "visible";
        return;
      }
      document.getElementById("results").innerHTML = "<br><u><h3 style='text-align:center;'>Dealer List:</h3></u>" + xmlhttp.responseText + "<br>";
      document.getElementById("results").style.visibility = "visible";
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/map/searcher.php?str="+val,true);
  xmlhttp.send();
        
      }
      
      
      
      	function add_note(xid){
        var rep = '<?php echo $_GET['fn']; ?>';
        var note = document.getElementById("ta_"+xid).value;
        if(note === ''){
					alert("Please enter a note!");
					return;
				}
			
         if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("ta_"+xid).value = '';
			//alert(xmlhttp.reponseText);
      document.getElementById("btn_"+xid).innerHTML = 'Note Added!';
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/map/php/add-note.php?id="+xid+"&rep="+rep+"&note="+note,true);
  xmlhttp.send();
      }
      
    function update_bar(p){
			window.parent.document.getElementById("progress_bar").style.width = p+"%";
			
			if(p === 100 || p === '100'){
				window.parent.document.getElementById("progress_bar_div").style.visibility = "hidden";
			}
		}
    </script>
  </head>
  <body>
		
    <div id="main">
    <div id="top">
    <input id="pac-input" class="controls" type="text" onkeyup="searcher(this.value);" value="" placeholder="Search Box" style="text-align:center; margin:auto;"/>
    </div>
    <div id="results"></div>
    <div id="map"></div>
    </div>
    <script>
      function initAutocomplete(zx){
        if(!zx){
          var zx = 5;
        }
        
     
        
        
        
        //Starts the map...
        //center: new google.maps.LatLng(40.213362, -85.377858),
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(40.213362, -85.377858),
          zoom: zx,
					gestureHandling: 'greedy'
        });
        
        
          
        //Sets up the information window for when a marker is clicked...
        var infoWindow = new google.maps.InfoWindow;
        
        <?php
				$i = 0;//Initial Starting of Dealer Sequence...
				
        $g = mysqli_query($conn, $q) or die($conn->error);
        
        while($r = mysqli_fetch_array($g)){
					$eid = $r['ID'];
          
           echo 'setInterval(function (){if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
        var q = JSON.parse(this.responseText);
        //alert(q.ID+"-->"+q.lat);
        //var lat' . $r['ID'] . ' = parseFloat(q.lat);
        //var lng' . $r['ID'] . ' = parseFloat(q.lng);
        //alert(lat' . $r['ID'] . ');
				var latlng = new google.maps.LatLng(parseFloat(q.lat), parseFloat(q.lng));
   			marker' . $r['ID'] . '.setPosition(latlng);
				if(document.getElementById("' . $r['ID'] . '")){
				document.getElementById("' . $r['ID'] . '").innerHTML = "<h1>"+q.deviceName+"</h1>"+
																																"<h2>Nearby: "+q.nearby+"</h2>";
				}
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/map/locator/fetch-location.php?id=' . $r['ID'] . '",true);
  xmlhttp.send();},2000);';
          
          
					//Update Progress Percentage
				$crow++;
				$percent = ($crow / $trows) * 100;
				//echo 'update_bar(' . $percent . ');';
					
					
					/*$bName2 = $r['name'];
					
					if($i == 0){//Initial Setup for Parsers
					$bName = $r['name'];
					$bName2 = $r['name'];
					$ini = 'yes';
					$i++;
					}
					//echo 'alert("bname->' . $bName . ' bname2->' . $bName2 . ' ini->' . $ini . '");';
					if($bName != $bName2 || $ini == 'yes'){//If same dealer
						*/
          
          /*$state = $r['state'];
          switch ($state){
            case "Indiana":
              $color = "blue";
              $colorh = "0000FF";
              break;
            case "Illinois":
              $color = "brown";
              $colorh = "A52A2A";
              break;
            case "Texas":
              $color = "darkgreen";
              $colorh = "006400";
              break;
            case "Arkansas":
              $color = "green";
              $colorh = "008000";
              break;
            case "Kentucky":
              $color = "orange";
              $colorh = "FFA500";
              break;
            case "Michigan":
              $color = "paleblue";
              $colorh = "BDE4FE";
              break;
            case "Missouri":
              $color = "pink";
              $colorh = "FFC0CB";
              break;
            case "Ohio":
              $color = "purple";
              $colorh = "800080";
              break;
            case "Oklahoma":
              $color = "red";
              $colorh = "FF0000";
              break;
            case "Tennessee":
              $color = "yellow";
              $colorh = "FFFF00";
              break;
            default:
              $color = "";
              $colorh = "";
          }*/
          
        
          
          
        echo 'var point' . $r['ID'] . ' = new google.maps.LatLng(0,0);';
        //echo 'var point' . $r['ID'] . ' = new google.maps.LatLng("' . $xxlat . '","' . $xxlng . '");';
          
        /*if($r['status'] == 'Pending'){
						if($r['urgent'] == 'Yes'){*/
          		echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bbT|' . mysqli_real_escape_string($conn,$r['fname']) . '|FFFFFF|000000";';
						/*}else{
          		echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bbT|' . mysqli_real_escape_string($conn,$r['contractor_name']) . '|F5001F|000000";';
						}
					}
        if($r['status'] == 'Draft'){
          echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=glyphish_bookmark|bbT|' . mysqli_real_escape_string($conn,$r['contractor_name']) . '|FBBDCC|000000";';
        }
        if($r['status'] == 'Scheduled'){
          echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=truck|bbT|' . mysqli_real_escape_string($conn,$r['contractor_name']) . '|C6EF8C|000000";';
        }
				if($r['status'] == '2nd Attempt'){
          echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=glyphish_redo|bbT|' . mysqli_real_escape_string($conn,$r['contractor_name']) . '|F9FF00|000000";';
        }
				//REPO HERE [Icon --> legal]...
				if($color == ''){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/warning.png";';
				}*/
				
					
   //Get Last Visit Date...
					/*	if(strlen($r['date']) > 1){
							$lvdate = date("m/d/Y", strtotime($r['date']));
						}else{
							$lvdate = "Unknown";
						}
         /* $lvdq = "SELECT `date` FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "' ORDER BY `date` DESC LIMIT 1";
          $lvdg = mysqli_query($conn, $lvdq) or die($conn->error);
          $lvdr = mysqli_fetch_array($lvdg);
          if(mysqli_num_rows($lvdg) !=0){
            $lvdate = date("m/d/Y", strtotime($lvdr['date']));
          }else{
            $lvdate = "Unknown";
          }*/
          
          
          
  echo 'var marker' . $r['ID'] . ' = new google.maps.Marker({
          position: point' . $r['ID'] . ',
          map: map,
          icon: img' . $r['ID'] . '
        });
        
        
        var cont' . $r['ID'] . ' = document.createElement("div");
        
        //var icon = document.createElement("i");
        //icon.className = "fa fa-exclamation-triangle";
        
        cont' . $r['ID'] . '.id = "' . $r['ID'] . '";
        cont' . $r['ID'] . '.innerHTML = "<h1></h1>";
        //cont' . $r['ID'] . '.innerHTML += "<br><br>";
				';
						
					
        
					
        //Check for images
					
						if($r['url']){
						/*echo 'var imgc = document.createElement("div");';
              echo 'var imger = document.createElement("img");
                    var anchor = document.createElement("a");
                    imger.setAttribute("src", "' . $r['url'] . '");
                    imger.setAttribute("width", "40%");
                    anchor.setAttribute("href", "' . $r['url'] . '");
                    anchor.setAttribute("target", "_blank");
                    anchor.appendChild(imger);
                    imgc.appendChild(anchor);';
							echo 'cont' . $r['ID'] . '.appendChild(imgc);';*/
						}//if url exists
					
						//echo 'alert("bname->' . $bName . ' bname2->' . $bName2 . ' ini->' . $ini . '");';
							/*if($r['url']){
							echo 'var imgc = document.createElement("div");';
              echo 'var imger = document.createElement("img");
                    var anchor = document.createElement("a");
                    imger.setAttribute("src", "' . $r['url'] . '");
                    //imger.setAttribute("height", "150");
                    imger.setAttribute("width", "40%");
                    anchor.setAttribute("href", "' . $r['url'] . '");
                    anchor.setAttribute("target", "_blank");
                    anchor.appendChild(imger);
                    imgc.appendChild(anchor);';
							echo 'cont' . $r['ID'] . '.appendChild(imgc);';
							}//if url exists*/
					
					//OLD IMAGING CODE...
          /*$icq = "SELECT * FROM `dealer_imgs` WHERE `dealer_id` = '" . $r['ID'] . "'";
          $icg = mysqli_query($conn, $icq) or die($conn->error);
          if(mysqli_num_rows($icg) == 0){
          }else{
            echo 'var imgc = document.createElement("div");';
            while($icr = mysqli_fetch_array($icg)){
              echo 'var imger = document.createElement("img");
                    var anchor = document.createElement("a");
                    imger.setAttribute("src", "' . $icr['url'] . '");
                    //imger.setAttribute("height", "150");
                    imger.setAttribute("width", "40%");
                    anchor.setAttribute("href", "' . $icr['url'] . '");
                    anchor.setAttribute("target", "_blank");
                    anchor.appendChild(imger);
                    imgc.appendChild(anchor);';
            }
            echo 'cont' . $r['ID'] . '.appendChild(imgc);';
          }*///End OLD IMAGING CODE...
         
  echo '//cont' . $r['ID'] . '.innerHTML +=  "<h5>Last Visited: ' . $lvdate . '</h5>";
        //cont' . $r['ID'] . '.innerHTML +=  "<h4><b>Owner:</b> ' . $r['ownername'] . '</h4>";
        //cont' . $r['ID'] . '.innerHTML +=  "<p><b>Status</b>: ' . $r['status'] . '</p>";
        //cont' . $r['ID'] . '.innerHTML +=  "<p><b>Address</b>: ' . $r['address'] . '</p>";
        //cont' . $r['ID'] . '.innerHTML +=  "<p><b>City: </b>' . $r['city'] . '</p>";
        //cont' . $r['ID'] . '.innerHTML +=  "<p><b>State: </b>' . $r['state'] . '</p>";
        //cont' . $r['ID'] . '.innerHTML +=  "<p><b>Zip: </b>' . $r['zip'] . '</p>";
        //cont' . $r['ID'] . '.innerHTML +=  "<p><b>Phone: </b>' . $r['phone'] . '</p>";
        //cont' . $r['ID'] . '.innerHTML +=  "<p><b>Fax: </b>' . $r['fax'] . '</p>";
        //cont' . $r['ID'] . '.innerHTML +=  "<p><b>Email: </b>";
        //var mcemail = document.createElement("p");
        //var bo = document.createElement("b");
        //bo.textContent = "Email: ";
        //mcemail.appendChild(bo);
        //var cemail = document.createElement("a");
        //cemail.href = "mailto:' . $r['email'] . '";
        //cemail.textContent = "' . $r['email'] . '";
        //mcemail.appendChild(cemail);
        //cont' . $r['ID'] . '.appendChild(mcemail);
        ';
   
      
  /*echo 'cont' . $r['ID'] . '.innerHTML +=  "<p><b>2016 Sales: </b>' . $r['2016_sales'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>2015 Sales: </b>' . $r['2015_sales'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>Status: </b>' . $r['status'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<h4><u>Account Notes:</u></h4>";';*/
					
					
					
        //Existing notes query
        /*  $enq = "SELECT * FROM `internal_notes` WHERE `dealer_id` = '" . $r['ID'] . "' ORDER BY `ID` DESC";
          $eng = mysqli_query($conn, $enq) or die($conn->error);
          while($enr = mysqli_fetch_array($eng)){
            echo 'cont' . $r['ID'] . '.innerHTML += "<p><b><u>' . date("m/d/Y", strtotime($enr['date'])) . ' by ' . $enr['rep'] . ' @ ' . date("h:iA", strtotime($enr['time'])) . '</u></b><br>' . mysqli_real_escape_string($conn,$enr['note']) . '</p>";';
          }
          
          
        //New Note Form
        echo 'var ta = document.createElement("textarea");
        ta.style.width = "300px";
        ta.style.height = "50px";
        ta.id = "ta_' . $r['ID'] . '";
        cont' . $r['ID'] . '.appendChild(ta);
        var br = document.createElement("br");
        cont' . $r['ID'] . '.appendChild(br);
        var btn = document.createElement("button");
        btn.setAttribute("type", "button");
        btn.style.width = "125px";
        btn.style.background = "green";
        btn.innerHTML = "Add Note";
        btn.id = "btn_' . $r['ID'] . '";
        btn.setAttribute("onclick", "add_note(' . $r['ID'] . ');");
        cont' . $r['ID'] . '.appendChild(btn);';
        */
        
        echo 'marker' . $r['ID'] . '.addListener("click", function() {
          infoWindow.setContent(cont' . $r['ID'] . ');
          infoWindow.open(map, marker' . $r['ID'] . ');
					map.setZoom(16);
    			map.setCenter(marker' . $r['ID']  . '.getPosition());
        });
        ';
          
       echo '$("body").on("click", "#x' . $r['ID'] . '", function () {
              document.getElementById("results").style.visibility = "hidden";
              infoWindow.setContent(cont' . $r['ID'] . ');
              infoWindow.open(map, marker' . $r['ID'] . ');
              restore();
              });';
						
					
          
        }//End main while "$r =" loop...
          ?>
        
        function restore(){
          document.getElementById("top").innerHTML = '<input id="pac-input" class="controls" type="text" onkeyup="searcher(this.value);" value="" placeholder="Search Box" style="text-align:center; margin:auto;"/>';
        //THIS IS THE START OF THE AUTOCOMPLETE SEGMENT FOR THE DIRECTION AND/OR CUSTOMER LOCATOR CAPABILITIES...
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
        
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
        
          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
        //THIS IS THE END OF THE AUTOCOMPLETE SEGMENT FOR THE PREDICTIONS ON RANDOM ADDRESS QUERIES...  
        }
        
       //THIS IS THE START OF THE AUTOCOMPLETE SEGMENT FOR THE DIRECTION AND/OR CUSTOMER LOCATOR CAPABILITIES...
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
        
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
        
          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
        //THIS IS THE END OF THE AUTOCOMPLETE SEGMENT FOR THE PREDICTIONS ON RANDOM ADDRESS QUERIES...  
        
				
      }
      document.getElementById("results").style.visibility = "hidden";
			
			
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg&callback=initAutocomplete&libraries=places">
    </script>
     
  </body>
</html>