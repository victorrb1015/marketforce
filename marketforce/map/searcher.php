<?php
include '../php/connection.php';

//Load Varibles
$str = mysqli_real_escape_string($conn, $_GET['str']);


//$q = "SELECT * FROM `dealers` WHERE `name` LIKE '%" . $str . "%' AND `inactive` != 'Yes' AND `hidden` != 'Yes' ORDER BY `name` ASC";
$q = "SELECT * FROM `dealers` WHERE `name` LIKE '%" . $str . "%' AND `hidden` != 'Yes' ORDER BY `name` ASC";

$g = mysqli_query($conn, $q) or die($conn->error);
if(mysqli_num_rows($g) == 0){
  echo 1;
}else{
while($r = mysqli_fetch_array($g)){
  //Find State Color
  $state = $r['state'];
          switch ($state){
            case "Indiana":
              $color = "blue";
              $colorh = "0000FF";
							$fontc = "FFFFFF";
              break;
            case "Illinois":
              $color = "brown";
              $colorh = "A52A2A";
							$fontc = "FFFFFF";
              break;
            case "Texas":
              $color = "darkgreen";
              $colorh = "006400";
							$fontc = "000000";
              break;
            case "Arkansas":
              $color = "green";
              $colorh = "008000";
							$fontc = "000000";
              break;
            case "Kentucky":
              $color = "orange";
              $colorh = "FFA500";
							$fontc = "000000";
              break;
            case "Michigan":
              $color = "paleblue";
              $colorh = "BDE4FE";
							$fontc = "000000";
              break;
            case "Missouri":
              $color = "pink";
              $colorh = "FFC0CB";
							$fontc = "000000";
              break;
            case "Ohio":
              $color = "purple";
              $colorh = "800080";
							$fontc = "FFFFFF";
              break;
            case "Oklahoma":
              $color = "red";
              $colorh = "FF0000";
							$fontc = "000000";
              break;
            case "Tennessee":
              $color = "yellow";
              $colorh = "FFFF00";
							$fontc = "000000";
              break;
            default:
              $color = "";
              $colorh = "";
							$fontc = "000000";
          }
              
          //Find Trained Status
              $status = $r['status'];
           switch ($status){
             case "New":
               if($r['new_signs'] == 'Done'){
                  $url = "https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin_star|N|" . $colorh . "|" . $fontc . "|000000";
                }else{
                  $url = "https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin|N|" . $colorh . "|" . $fontc;
                }
               //$url = "http://marketforceapp.com/marketforce/map/icons/google/" . $color . "_MarkerN.png";
               break;
             case "Trained":
               if($r['new_signs'] == 'Done'){
                  $url = "https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin_star|T|" . $colorh . "|" . $fontc . "|000000";
                }else{
                  $url = "https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin|T|" . $colorh . "|" . $font;
                }
               //$url = "http://marketforceapp.com/marketforce/map/icons/google/" . $color . "_MarkerT.png";
               break;
             case "Visited":
                $qv = 'V';
                $qvq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "' AND QUARTER(`date`) = '1' AND YEAR(`date`) = '2019'";
                $qvg = mysqli_query($conn, $qvq) or die($conn->error);
                if(mysqli_num_rows($qvg) > 0){
                  $qv .= '1';
                }
                $qvq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "' AND QUARTER(`date`) = '2' AND YEAR(`date`) = '2019'";
                $qvg = mysqli_query($conn, $qvq) or die($conn->error);
                if(mysqli_num_rows($qvg) > 0){
                  $qv .= '2';
                }
                $qvq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "' AND QUARTER(`date`) = '3' AND YEAR(`date`) = '2019'";
                $qvg = mysqli_query($conn, $qvq) or die($conn->error);
                if(mysqli_num_rows($qvg) > 0){
                  $qv .= '3';
                }
                $qvq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "' AND QUARTER(`date`) = '4' AND YEAR(`date`) = '2019'";
                $qvg = mysqli_query($conn, $qvq) or die($conn->error);
                if(mysqli_num_rows($qvg) > 0){
                  $qv .= '4';
                }
                if($r['new_signs'] == 'Done'){
                  $url = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=star|bb|" . $qv . "|" . $colorh . "|" . $fontc;
                }else{
                  $url = "https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bb|" . $qv . "|" . $colorh . "|" . $fontc;
                }
               //$url = "http://marketforceapp.com/marketforce/map/icons/google/" . $color . "_MarkerV.png";
               break;
             case "Unmanned Lot":
               if($r['new_signs'] == 'Done'){
                  $url = "https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin_star|U|" . $colorh . "|" . $fontc . "|000000";
                }else{
                  $url = "https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin|U|" . $colorh . "|" . $fontc;
                }
               //$url = "http://marketforceapp.com/marketforce/map/icons/google/" . $color . "_MarkerU.png";
               break;
             default:
               $url = "";
           }
          if($r['inactive'] == 'Yes'){
          $url = "https://chart.googleapis.com/chart?chst=d_map_spin&chld=1|0|" . $colorh . "|7|b|Closed|Dealer";
          }
              
  echo '<p class="x' . $r['ID'] . '">
        <b>' . $r['name'] . '</b>, <small>' . $r['city'] . ', ' . $r['state'] . '</small> <img class="icon_pin" src="' . $url . '" />
        </p>';
}
}//End num_rows check...
?>










