<?php
header("Access-Control-Allow-Origin: *");
/*------------------------------------CHANGE RECORDS--------------------------------------------*/
/*
1. 
*/
/*-----------------------------------END CHANGE RECORDS-----------------------------------------*/
include '../php/connection.php';

$quot = "'";
$sarray = array(); //Array to be filled with names and IDs for search function

//New Orders Query...
//$q = "SELECT * FROM `salesPortalLinked` WHERE `orderStatus` = 'In Shop' AND `archieved` != 'Yes' AND `in_use` != 'Yes'";
$q = "SELECT * FROM `salesPortalLinked` WHERE `orderStatus` = 'In Shop' AND `archieved` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
$ng = mysqli_num_rows($g);

//Repairs Query...
//$rq = "SELECT * FROM `repairs` WHERE `inactive` != 'Yes' AND `in_use` != 'Yes' AND `status` != 'Completed' AND `status` != 'Cancelled' AND `status` != 'Draft' AND `lat` != 'Error' AND `lng` != 'Error'";
$rq = "SELECT * FROM `repairs` WHERE `inactive` != 'Yes' AND `status` != 'Completed' AND `status` != 'Cancelled' AND `status` != 'Draft' AND `lat` != 'Error' AND `lng` != 'Error'";
$rg = mysqli_query($conn, $rq) or die($conn->error);
$nrg = mysqli_num_rows($rg);

//Total Number of Rows..
$tnr = ($ng + $nrg);

?>
<html>
  <head>
		<!--<meta http-equiv="Cache-control" content="public">-->
    <title>Market Force Map Test</title>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
    
    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/70c95f2a09.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>
    <link rel="stylesheet" href="scheduling/css/map-style.css"/>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <!-- Custom Map Scripts -->
    <script src="scheduling/js/map-functions.js?cb=<?php echo $cache_buster;?>"></script>
  </head>
  <body>
    						<!--PRELOADER GIF-->
					<div class="se-pre-con"></div>
		
    <div id="main">
    <div id="top">
    <input id="pac-input" class="controls" type="text" onkeyup="searcher(this.value);" value="" placeholder="Search Box" style="text-align:center; margin:auto;"/>
    </div>
    <div id="map"></div>
    </div>
    <script src="scheduling/js/map-api.js?cb=<?php echo $cache_buster; ?>"></script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg&callback=initAutocomplete&libraries=places">
    </script>
     
  </body>
</html>