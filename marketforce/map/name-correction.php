<?php 
include '../php/connection.php';

//Get All Dealers from new_dealer_form
$dq = "SELECT * FROM `new_dealer_form`";
$dg = mysqli_query($conn, $dq) or die($conn->error);
while($dr = mysqli_fetch_array($dg)){
  
  //Set Active Status...
  if($dr['inactive'] == 'Yes'){
    $active = '[INACTIVE]';
  }else{
    $active = '';
  }
  
  //Pull Last Rep_id to make a visit...
  $vq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $dr['ID'] . "' ORDER BY `ID` DESC LIMIT 1";
  $vg = mysqli_query($conn, $vq) or die($conn->error);
  $vr = mysqli_fetch_array($vg);
  $rep_id = $vr['user'];
  
  //Get Rep Name..
  $nq = "SELECT * FROM `users` WHERE `ID` = '" . $rep_id . "'";
  $ng = mysqli_query($conn, $nq) or die($conn->error);
  $nr = mysqli_fetch_array($ng);
  $rep_name = $nr['fname'] . ' ' . $nr['lname'];
  
  //Update the new_dealer_form Database...
  $uq = "UPDATE `new_dealer_form` SET `user` = '" . $rep_name . "' WHERE `ID` = '" . $dr['ID'] . "'";
  if(mysqli_num_rows($vg) > 0){
  //mysqli_query($conn, $uq) or die($conn->error);
      echo '<p>' . $active . ' ' . $dr['bizname'] . '\'s Rep Updated From "' . $dr['user'] . '" to "' . $rep_name . '!</p>';
  }else{
      echo '<p style="color:red;">' . $active . ' ' . $dr['bizname'] . '\'s Rep Not Updated From "' . $dr['user'] . '"!</p>';
  }
  

  
}