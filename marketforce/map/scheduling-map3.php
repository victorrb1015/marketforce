<?php
/*------------------------------------CHANGE RECORDS--------------------------------------------*/
/*
1. 
*/
/*-----------------------------------END CHANGE RECORDS-----------------------------------------*/
include '../php/connection.php';

$quot = "'";
$sarray = array(); //Array to be filled with names and IDs for search function

//New Orders Query...
//$q = "SELECT * FROM `salesPortalLinked` WHERE `orderStatus` = 'In Shop' AND `archieved` != 'Yes' AND `in_use` != 'Yes'";
$q = "SELECT * FROM `salesPortalLinked` WHERE `orderStatus` = 'In Shop' AND `archieved` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
$ng = mysqli_num_rows($g);

//Repairs Query...
//$rq = "SELECT * FROM `repairs` WHERE `inactive` != 'Yes' AND `in_use` != 'Yes' AND `status` != 'Completed' AND `status` != 'Cancelled' AND `status` != 'Draft' AND `lat` != 'Error' AND `lng` != 'Error'";
$rq = "SELECT * FROM `repairs` WHERE `inactive` != 'Yes' AND `status` != 'Completed' AND `status` != 'Cancelled' AND `status` != 'Draft' AND `lat` != 'Error' AND `lng` != 'Error'";
$rg = mysqli_query($conn, $rq) or die($conn->error);
$nrg = mysqli_num_rows($rg);

//Total Number of Rows..
$tnr = ($ng + $nrg);

?>
<html>
  <head>
		<!--<meta http-equiv="Cache-control" content="public">-->
    <title>Market Force Map Test</title>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
    
    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/70c95f2a09.js"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
        //margin-top: -55%;
        //z-index: -9;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
  height: 45px;
  opacity: 0.9;
}
      #pac-input:focus {
  border-color: #4d90fe;
}
      .pac-controls {
  display: inline-block;
  padding: 5px 11px;
}

.pac-controls label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}
      #top{
        width: 40%;
        //border: 2px solid red;
        text-align: center;
        z-index: 1;
        position: fixed;
        margin-right: 30%;
        margin-left: 30%;
      }
      #pac-input{
        //width:100%;
      }
      #results{
        background-color: #ffffff;
        width: 40%;
        max-height: 50%;
        overflow: scroll;
        position: fixed;
        margin-left: 60%;
        opacity: 0.9;
        z-index: 2;
      }
      #results p{
        font-size: 13px;
      }
      #results p:hover{
        background-color: #E3E3E6;
        cursor: pointer;
      }
      #results a{
        text-decoration: none;
        color: black;
      }
      h5{
        color:red;
      }
      h4{
        color:blue;
      }
      #main{
        width: 100%;
      }
  </style>
    <script>
			//Global Variables
      var map;
			var stop_id = [];
			var stop_name = [];
			var stop_cost = [];
			var stop_address = [];
			var stop_location = [];
      var stop_type = [];
			var route_total_cost = 0;
      var marker_array = [];
      var stop_marker_array = [];
      var stop_type = [];
      var opt_status;
      var home_marker = [];
      var waypoint_order = [];
      var mURL;
      var IN_markers = [];
      var IL_markers = [];
      var TX_markers = [];
      var AR_markers = [];
      var KY_markers = [];
      var MI_markers = [];
      var MO_markers = [];
      var OH_markers = [];
      var OK_markers = [];
      var TN_markers = [];
      var KS_markers = [];
      var MS_markers = [];
      var AZ_markers = [];
      var WV_markers = [];
      var PA_markers = [];
      var MD_markers = [];
      var NE_markers = [];
      var IN_home_address = '2200 N Granville Ave, Muncie, IN 47303';
      var OK_home_address = '2520 S 32nd St, Muskogee, OK 74401';
      var home_address = IN_home_address;
   <?php
    if($_GET['schedule_state'] == 'Oklahoma'){
      echo 'var home_address = OK_home_address;';
    }else{
      echo 'var home_address = IN_home_address;';
    }
      ?>
      console.log('Dispatch Location: '+home_address);
      var leg_dur = [];
      var directions_response;
      
 function urlEncode(url){
		url = url.replace(/&/g, '%26'); 
		url = url.replace(/#/g, '%23');
		return url;
	}
      
  //Format Drive Time...
      function formatTime(secs){
      var sec_num = parseInt(secs, 10)    
      var hours   = Math.floor(sec_num / 3600) % 24
      var minutes = Math.floor(sec_num / 60) % 60
      var seconds = sec_num % 60    
      return [hours,minutes,seconds]
          .map(v => v < 10 ? "0" + v : v)
          .filter((v,i) => v !== "00" || i > 0)
          .join(":")
     }
      
      
			
      //This is the area for the search functionality...
        function searcher(val){
					var val = val.replace('&','%26');
        if(val === ""){
        document.getElementById("results").innerHTML = "";
        document.getElementById("results").style.visibility = "hidden";
        return;
      }
        if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //alert(xmlhttp.reponseText);
      if(xmlhttp.responseText == 1){
        document.getElementById("results").innerHTML = "<h3 style='text-align:center;'><u>Dealer List:</u></h3><h4>No Results Found...</h4>";
        document.getElementById("results").style.visibility = "visible";
        return;
      }
      document.getElementById("results").innerHTML = "<br><u><h3 style='text-align:center;'>Dealer List:</h3></u>" + xmlhttp.responseText + "<br>";
      document.getElementById("results").style.visibility = "visible";
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/map/searcher.php?str="+val,true);
  xmlhttp.send();
        
      }
      
      
      
      	function add_note(xid){
        var rep = '<?php echo $_GET['fn']; ?>';
        var note = document.getElementById("ta_"+xid).value;
        if(note === ''){
					alert("Please enter a note!");
					return;
				}
			
         if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("ta_"+xid).value = '';
			//alert(xmlhttp.reponseText);
      document.getElementById("btn_"+xid).innerHTML = 'Note Added!';
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/map/php/add-note.php?id="+xid+"&rep="+rep+"&note="+note,true);
  xmlhttp.send();
      }
      
    function update_bar(p){
			window.parent.document.getElementById("progress_bar").style.width = p+"%";
			
			if(p === 100 || p === '100'){
				window.parent.document.getElementById("progress_bar_div").style.visibility = "hidden";
			}
		}
      
      
   function add_to_route(id,mode,type){
     var xx = stop_id.length;
     //alert(id+", "+mode);
     if(type !== 'edit'){
     if(mode === 'repair'){
       document.getElementById('rbtn_'+id).disabled = true;
     }else{
       document.getElementById('btn_'+id).disabled = true;
     }
     }
      if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp22=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp22=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp22.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var q = JSON.parse(this.responseText);
      //alert(q);
      console.log(q);
      /*window.parent.document.getElementById('route_main').innerHTML += '<div id="repair_'+q.orderID+'" class="alert alert-warning" style="padding:5px;">'+
								'<button type="button" id="repairBTN_'+q.orderID+'" class="close repairBTN">x</button>'+	
                '<p><b>(?) '+q.customer+'</b></p>'+
									'<ul>'+
										'<li>'+q.buildingCity+', '+q.buildingState+'</li>'+
										'<li>Cost: $'+q.totalSale+'</li>'+
									'</ul>'+
								'</div>';*/
      var div = document.createElement("DIV");
      div.id = 'repair_'+q.orderID;
      div.classList.add("alert");
      div.classList.add("alert-warning");
      div.style.padding = '5px';
      //Button
      var btn = document.createElement('button');
      btn.setAttribute("type","button");
      btn.id = 'repairBTN_'+q.orderID;
      btn.classList.add("close");
      btn.classList.add("repairBTN");
      btn.innerHTML = 'x';
      div.appendChild(btn);
      //Paragraph
      var p = document.createElement("p");
      p.innerHTML = '(?) '+q.customer;
      p.style.fontWeight = 'bold';
      div.appendChild(p);
      //City, State
      var ul = document.createElement("UL");
      var li = document.createElement("LI");
      li.innerHTML = q.buildingCity+', '+q.buildingState;
      ul.appendChild(li);
      //Cost
      var li2 = document.createElement("LI");
      li2.innerHTML = 'Cost: '+q.totalSale;
      ul.appendChild(li2);
      div.appendChild(ul);
      parent.document.getElementById('route_main').appendChild(div);
      
      console.log(xx);
      stop_id.push(q.orderID);
			stop_name.push(q.customer);
			stop_cost.push(q.totalSale);
			stop_address.push(q.buildingAddress+' '+q.buildingCity+', '+q.buildingState+' '+q.buildingZipCode);
			stop_location.push(q.buildingCity+', '+q.buildingState);
      if(mode === 'repair'){
        stop_type.push('Repair');
      }else{
        stop_type.push('Order');
      }
			route_total_cost = route_total_cost + Number(q.totalSale);
			window.parent.document.getElementById('route_total').innerHTML = route_total_cost;
			var mbURL = 'https://www.google.com/maps/dir/?api=1&origin='+home_address+'&destination='+home_address+'&travelmode=driving&waypoints=';
			for( var i = 0; i < stop_address.length; i++){
				if(i > 0){
					mbURL = mbURL + '%7C';
				}
				mbURL = mbURL + stop_address[i];
			}
			parent.window.document.getElementById('mapurl_btn').href = mbURL;
      //alert(q.orderID);
      //Setup Removal Listener Script...
      parent.document.getElementById('repairBTN_'+q.orderID).addEventListener("click", function(){
        console.log('Waypoint '+q.orderID+' was removed from the array');
        stop_id.splice(xx,1);
        console.log(stop_id);
        stop_name.splice(xx,1);
        console.log(stop_name);
        route_total_cost = route_total_cost - q.totalSale;
        stop_cost.splice(xx,1);
        stop_address.splice(xx,1);
        stop_location.splice(xx,1);
        stop_type.splice(xx,1);
        parent.document.getElementById('repair_'+q.orderID).remove();
        parent.document.getElementById('route_total').innerHTML = route_total_cost;var mbURL = 'https://www.google.com/maps/dir/?api=1&origin='+home_address+'&destination='+home_address+'&travelmode=driving&waypoints=';
			for( var i = 0; i < stop_address.length; i++){
				if(i > 0){
					mbURL = mbURL + '%7C';
				}
				mbURL = mbURL + stop_address[i];
			}
			parent.window.document.getElementById('mapurl_btn').href = mbURL;
      });
      console.log('Event Lister Added for order#: '+q.orderID);
      
      
    }
  }
  xmlhttp22.open("GET","http://marketforceapp.com/marketforce/scheduling/php/fetch-order-info.php?id="+id+"&mode="+mode,true);
  xmlhttp22.send();
   }
      
      
  
    </script>
    	<style>
						/* This only works with JavaScript, 
if it's not present, don't show loader */
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	//background: url(../img/loader-128x/Preloader_7.gif) center no-repeat #fff;
  background: url(../img/loaders/loading-circle.gif) center no-repeat rgba(250,250,250,0.5);
}
        
 #loading-bar{
    text-align: center;
    width:100%;
    margin: auto;
    z-index: 99;
    position: absolute;
    //left: 113px;
    top: 0px;
 }
	</style>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>-->
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script>
//paste this code under the head tag or in a separate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
</script>
  </head>
  <body>
    						<!--PRELOADER GIF-->
					<div class="se-pre-con"></div>
		
    <div id="main">
    <div id="top">
    <input id="pac-input" class="controls" type="text" onkeyup="searcher(this.value);" value="" placeholder="Search Box" style="text-align:center; margin:auto;"/>
    </div>
    <!--<div id="results"></div>-->
    <!--//Progress Bar...-->
    <!--<div id="loading-bar" style="display:inline;">
      <h3 style="text-align:center;" id="loading-title">Loading...</h3>
      <div class="progress" style="margin:auto;width:90%;">
        <div id="pb" class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      <br>
      <p id="statusfeed" style="text-align:center;"></p>
    </div>-->
    <div id="map"></div>
    </div>
    <script>
      function initAutocomplete(zx){
        if(!zx){
          var zx = 5;
        }
        
        
        
        
     
        parent.document.getElementById('confirmRouteBTN').addEventListener('click', function(){
          var rrid = "<?php echo $_GET['edit_route']; ?>";
          if(rrid !== ''){
          parent.delete_route(rrid,'edit');
          }
          var rep_id = parent.document.getElementById('rep_id_tag').value;
          var rep_name = parent.document.getElementById('rep_name_tag').value;
          if(stop_id.length <= 0){
            alert("You have not added any invoices to the route!");
            return;
          }
          var sstop_name = JSON.stringify(stop_name);
          sstop_name = urlEncode(sstop_name);
          var sstop_id = JSON.stringify(stop_id);
          sstop_id = urlEncode(sstop_id);
          var sstop_address = JSON.stringify(stop_address);
          sstop_address = urlEncode(sstop_address);
          var sstop_cost = JSON.stringify(stop_cost);
          sstop_cost = urlEncode(sstop_cost);
          var sstop_location = JSON.stringify(stop_location);
          sstop_location = urlEncode(sstop_location);
          var sstop_type = JSON.stringify(stop_type);
          sstop_type = urlEncode(sstop_type);
          var swaypoint_order = JSON.stringify(waypoint_order);
          swaypoint_order = urlEncode(swaypoint_order);
          var smURL = urlEncode(mURL);
          
          
          if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
          } else {  // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlhttp.onreadystatechange=function() {
            if (this.readyState==4 && this.status==200) {
              
              parent.document.getElementById('exitCheck').value = 'Yes';
              //alert('Your Route has been confirmed!');
              alert(this.responseText);
              parent.window.location = 'http://marketforceapp.com/marketforce/scheduling.php?bypass=y';
              
            }
          }
          xmlhttp.open("GET","http://marketforceapp.com/marketforce/scheduling/php/confirm-route.php?"+
                       "stop_name="+sstop_name+
                       "&stop_id="+sstop_id+
                       "&stop_address="+sstop_address+
                       "&stop_cost="+sstop_cost+
                       "&stop_location="+sstop_location+
                       "&stop_type="+sstop_type+
                       "&rep_id="+rep_id+
                       "&rep_name="+rep_name+
                       "&waypoint_order="+swaypoint_order+
                       "&mURL="+smURL+
                       "&directions_response="+directions_response
                       ,true);
          if(rrid === ''){
            var to = 0;
          }else{
            var to = 3000;
          }
          setTimeout(function (){
            xmlhttp.send();
          },to);
          
        });
        
        
        //Starts the map...
        //center: new google.maps.LatLng(40.213362, -85.377858),
         map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(40.213362, -85.377858),
          zoom: zx,
					gestureHandling: 'greedy'
        });
        
        
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        /*var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 6,
          center: {lat: 41.85, lng: -87.65}
        });*/
        directionsDisplay.setMap(map);
        
          
        //Sets up the information window for when a marker is clicked...
        var infoWindow = new google.maps.InfoWindow;
        
        
        //HOME BASE MARKER 2200 N Granville Ave, Muncie, IN 47303
        <?php
        if($_GET['schedule_state'] == 'Oklahoma'){
          //HOME BASE MARKER 2520 S 32nd St, Muskogee, OK 74401
          echo 'var hpoint = new google.maps.LatLng(35.723101,-95.402094);';
        }else{
          //HOME BASE MARKER 2200 N Granville Ave, Muncie, IN 47303
          echo 'var hpoint = new google.maps.LatLng(40.213878,-85.377739);';
        }
        
        ?>
					
				//Setup the icon image for the Home Base...
        var himg = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=home|bbT|Main Office|3c3c3c|FFFFFF";
          
       
        var hmarker = new google.maps.Marker({
          position: hpoint,
          map: map,
          icon: himg,
          zIndex: 10000
        });
        
        //marker_array.push(hmarker);
        
        var hcont = document.createElement("div");
        
        
        hcont.id = "home_base";
        hcont.innerHTML = "<h1>Main Office</h1>";
						
         
   
        hmarker.addListener("click", function() {
          infoWindow.setContent(hcont);
          infoWindow.open(map, hmarker);
        });
        
        
        
        <?php
				$i = 0;//Initial Starting of Dealer Sequence...
				
        //echo 'document.getElementById("loading-title").innerHTML = "Loading...";';
        
        //Loop through all New Orders.........................
        while($r = mysqli_fetch_array($g)){
          
          $state = $r['buildingState'];
          switch ($state){
            case "IN":
              $color = "blue";
              $colorh = "0000FF";
              break;
            case "IL":
              $color = "brown";
              $colorh = "A52A2A";
              break;
            case "TX":
              $color = "darkgreen";
              $colorh = "006400";
              break;
            case "AR":
              $color = "green";
              $colorh = "008000";
              break;
            case "KY":
              $color = "orange";
              $colorh = "FFA500";
              break;
            case "MI":
              $color = "paleblue";
              $colorh = "BDE4FE";
              break;
            case "MO":
              $color = "pink";
              $colorh = "FFC0CB";
              break;
            case "OH":
              $color = "purple";
              $colorh = "800080";
              break;
            case "OK":
              $color = "red";
              $colorh = "FF0000";
              break;
            case "TN":
              $color = "yellow";
              $colorh = "FFFF00";
              break;
            default:
              $color = "";
              $colorh = "";
          }
          
        
          $lq = "SELECT * FROM `portal_relationship` WHERE `orderID` = '" . $r['orderID'] . "' AND `lat` != 'Error'";
          $lg = mysqli_query($conn, $lq) or die($conn->error);
          $lr = mysqli_fetch_array($lg);
          
          
          
        echo 'var point' . $r['orderID'] . ' = new google.maps.LatLng("' . $lr['lat'] . '","' . $lr['lng'] . '");';
					
				//Setup the icon image for the orders...
        //Date Color Coding...
				$td = date("m-d-y");
				//$d1 = strtotime($rr['today_date']);
				$d1 = strtotime($r['invoiceDate']);
				$d2 = strtotime("-2 week");
				$d3 = strtotime("-3 week");
				$d4 = strtotime("-4 week");
          
				if($d4 >= $d1){
					$mBG = 'FF0000';//Red
          $mFC = '000000';//Black
				}else if($d3 >= $d1){
					$mBG = 'FFFF00';//Yellow
          $mFC = '000000';//Black
				}else if($d2 >= $d1){
					$mBG = '0000FF';//Blue
          $mFC = 'FFFFFF';//White
				}else{
					$mBG = 'FFFFFF';//White
          $mFC = '000000';//Black
				}
          
          
        //echo 'var img' . $r['orderID'] . ' = "http://marketforceapp.com/marketforce/map/icons/warning.png";';
        if($r['in_use'] == 'Yes'){
          echo 'var img' . $r['orderID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=truck|bbT|Assigned To Route|9D9D9D|FFFFFF";';
        }else{
          echo 'var img' . $r['orderID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=bank-dollar|bbT|' . number_format(mysqli_real_escape_string($conn,$r['totalSale']),2) . '|' . $mBG . '|' . $mFC . '";';
        }
          
       
  echo 'var marker' . $r['orderID'] . ' = new google.maps.Marker({
          position: point' . $r['orderID'] . ',
          map: map,
          icon: img' . $r['orderID'] . '
        });
        ';
        
        
        switch ($state){
            case "IN":
              echo 'IN_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "IL":
              echo 'IL_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "TX":
              echo 'TX_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "AR":
              echo 'AR_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "KY":
              echo 'KY_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "MI":
              echo 'MI_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "MO":
              echo 'MO_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "OH":
              echo 'OH_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "OK":
              echo 'OK_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "TN":
              echo 'TN_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "KS":
              echo 'KS_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "MS":
              echo 'MS_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "AZ":
              echo 'AZ_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "WV":
              echo 'WV_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "PA":
              echo 'PA_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "MD":
              echo 'MD_markers.push(marker' . $r['orderID'] . ');';
              break;
            case "NE":
              echo 'NE_markers.push(marker' . $r['orderID'] . ');';
              break;
            default:
              //Do Nothing...
          }
          
        
        echo 'marker_array.push(marker' . $r['orderID'] . ');
        
        var cont' . $r['orderID'] . ' = document.createElement("div");
        
        var icon = document.createElement("i");
        icon.className = "fa fa-exclamation-triangle";
        
        cont' . $r['orderID'] . '.id = "' . $r['orderID'] . '";
        cont' . $r['orderID'] . '.innerHTML = "<h1><u>' . str_replace('\r','',mysqli_real_escape_string($conn,$r['customer'])) . '</u></h1>";
        //cont' . $r['orderID'] . '.innerHTML += "<br><br>";
				';
						
         
  echo '//cont' . $r['orderID'] . '.innerHTML +=  "<h5>Last Visited: ' . $lvdate . '</h5>";
        //cont' . $r['orderID'] . '.innerHTML +=  "<h4><b>Owner:</b> ' . $r['ownername'] . '</h4>";
        cont' . $r['orderID'] . '.innerHTML += "<p><b>Invoice#:</b> <b>' . $r['orderID'] . '</b></p>";
        cont' . $r['orderID'] . '.innerHTML +=  "<p><b>Status</b>: ' . $r['orderStatus'] . '</p>";
        cont' . $r['orderID'] . '.innerHTML +=  "<p><b>Address</b>: ' . $r['buildingAddress'] . '</p>";
        cont' . $r['orderID'] . '.innerHTML +=  "<p><b>City: </b>' . $r['buildingCity'] . '</p>";
        cont' . $r['orderID'] . '.innerHTML +=  "<p><b>State: </b>' . $r['buildingState'] . '</p>";
        cont' . $r['orderID'] . '.innerHTML +=  "<p><b>Zip: </b>' . $r['buildingZipCode'] . '</p>";
        //cont' . $r['orderID'] . '.innerHTML +=  "<p><b>Phone: </b>' . $r['customerPhone'] . '</p>";
        //cont' . $r['orderID'] . '.innerHTML +=  "<p><b>Fax: </b>' . $r['fax'] . '</p>";
        //cont' . $r['orderID'] . '.innerHTML +=  "<p><b>Email: </b>";
        //var mcemail = document.createElement("p");
        //var bo = document.createElement("b");
        //bo.textContent = "Email: ";
        //mcemail.appendChild(bo);
        //var cemail = document.createElement("a");
        //cemail.href = "mailto:' . $r['email'] . '";
        //cemail.textContent = "' . $r['email'] . '";
        //mcemail.appendChild(cemail);
        //cont' . $r['orderID'] . '.appendChild(mcemail);
        var btn = document.createElement("button");
        btn.type = "button";
        btn.classList.add("btn");
        btn.classList.add("btn-success");
        btn.setAttribute("onclick", "add_to_route(\'' . $r['orderID'] . '\');");
        btn.setAttribute("style", "background:#5cb85c;padding:5px;border-radius:4px;color:white;");
        btn.setAttribute("id", "btn_' . $r['orderID'] . '");';
          if($r['in_use'] == 'Yes'){
            echo 'btn.setAttribute("disabled", true);';
          }
  echo 'btn.innerHTML = "Add To Route";
        cont' . $r['orderID'] . '.appendChild(btn);
        ';
   
        echo 'marker' . $r['orderID'] . '.addListener("click", function() {
          infoWindow.setContent(cont' . $r['orderID'] . ');
          infoWindow.open(map, marker' . $r['orderID'] . ');
          $("#btn_' . $r['orderID'] . '").click(function(){
            marker' . $r['orderID'] . '.setIcon("https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=truck|bb|Pending|C6EF8C|000000"); // set image path here...
            var al = stop_marker_array.length;
            stop_marker_array.push(marker' . $r['orderID'] . ');
            console.log("After Add Marker Array");
            console.log(stop_marker_array);
            setTimeout(function(){
              parent.window.document.getElementById("repairBTN_' . $r['orderID'] . '").addEventListener("click",function(){
                marker' . $r['orderID'] . '.setIcon(img' . $r['orderID'] . '); // reset image path here...
                infoWindow.setContent(cont' . $r['orderID'] . ');
                infoWindow.open(map, marker' . $r['orderID'] . ');
                document.getElementById("btn_' . $r['orderID'] . '").disabled = false;
                infoWindow.close();
                stop_marker_array.splice(al,1);
                console.log("After Remove Marker Array");
                console.log(stop_marker_array);
              });
            },500);
            infoWindow.close();
          });
        });
        ';
          
       echo '$("body").on("click", "#x' . $r['orderID'] . '", function () {
              document.getElementById("results").style.visibility = "hidden";
              infoWindow.setContent(cont' . $r['orderID'] . ');
              infoWindow.open(map, marker' . $r['orderID'] . ');
              restore();
              });';
						
          $i++;
          $progress = $i / $tnr * 100;
          //echo 'document.getElementById("pb").style.width = "' . $progress . '%";';
          
        }//End New Orders Loop Through..........................................
        
  /*---------------------------------------------------------------------------------------------------------------REPAIRS---------------------------------------------------------------------------*/
        
        //echo 'document.getElementById("loading-title").innerHTML = "Loading...";';
        
        //Loop through Repairs........................
        while($r = mysqli_fetch_array($rg)){
          
          $state = $r['state'];
          switch ($state){
            case "Indiana":
              $color = "blue";
              $colorh = "0000FF";
              break;
            case "Illinois":
              $color = "brown";
              $colorh = "A52A2A";
              break;
            case "Texas":
              $color = "darkgreen";
              $colorh = "006400";
              break;
            case "Arkansas":
              $color = "green";
              $colorh = "008000";
              break;
            case "Kentucky":
              $color = "orange";
              $colorh = "FFA500";
              break;
            case "Michigan":
              $color = "paleblue";
              $colorh = "BDE4FE";
              break;
            case "Missouri":
              $color = "pink";
              $colorh = "FFC0CB";
              break;
            case "Ohio":
              $color = "purple";
              $colorh = "800080";
              break;
            case "Oklahoma":
              $color = "red";
              $colorh = "FF0000";
              break;
            case "Tennessee":
              $color = "yellow";
              $colorh = "FFFF00";
              break;
            default:
              $color = "white";
              $colorh = "FFFFFF";
          }
          
          
        echo 'var rpoint' . $r['ID'] . ' = new google.maps.LatLng("' . $r['lat'] . '","' . $r['lng'] . '");';
					
				//Setup the icon image for the orders...
        //if($r['customer_balance'] == '' && $r['customer_cost'] == 0){
        if(1 == 2){
          $cbalance = 0;
        }else{
          $cbalance1 = mysqli_real_escape_string($conn,str_replace(',','',$r['customer_balance']));
          $cbalance2 = mysqli_real_escape_string($conn,str_replace(',','',$r['customer_cost']));
          $cbalance = number_format(($cbalance1 + $cbalance2),2);
        }
        if($r['contractor_name'] == ''){
          $contractor = 'No Contractor Set';
        }else{
          $contractor = mysqli_real_escape_string($conn,$r['contractor_name']);
        }
          
          
           //Date Color Coding...
           $td = date("m-d-y");
           //$d1 = strtotime($rr['today_date']);
           $d1 = strtotime($r['pl_sign_date']);
           $d2 = strtotime("-2 week");
           $d3 = strtotime("-3 week");
           $d4 = strtotime("-4 week");
       if($r['pl_sign'] != '' && $r['pl_sign'] != '{"lines":[]}'){
           if($d4 >= $d1){
             $mBG = 'FF0000';//Red
             $mFC = '000000';//Black
           }else if($d3 >= $d1){
             $mBG = 'FFFF00';//Yellow
             $mFC = '000000';//Black
           }else if($d2 >= $d1){
             $mBG = '0000FF';//Blue
             $mFC = 'FFFFFF';//White
           }else{
             $mBG = 'FFFFFF';//White
             $mFC = '000000';//Black
           }
           if($r['status'] == 'Draft'){
             $mBG = 'FFC0CB';//Pink
             $mFC = '000000';//Black
           }
           if($r['status'] == 'Not Done'){
             $mBG = '800080';//Purple
             $mFC = 'FFFFFF';//White
           }
       }else{
         $mBG = '000000';//Black
         $mFC = 'FFFFFF';//White
       }
        //echo 'var rimg' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/warning.png";';
        if($r['in_use'] == 'Yes'){
          echo 'var rimg' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=truck|bbT|Assigned To Route|9D9D9D|FFFFFF";';
        }else{
          echo 'var rimg' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=repair|bbT|$' . $cbalance . ' -> ' . $contractor . '|' . $mBG . '|' . $mFC . '";';
        }
      
  echo 'var rmarker' . $r['ID'] . ' = new google.maps.Marker({
          position: rpoint' . $r['ID'] . ',
          map: map,
          icon: rimg' . $r['ID'] . '
        });';
       
          switch ($state){
            case "Indiana":
              echo 'IN_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Illinois":
              echo 'IL_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Texas":
              echo 'TX_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Arkansas":
              echo 'AR_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Kentucky":
              echo 'KY_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Michigan":
              echo 'MI_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Missouri":
              echo 'MO_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Ohio":
              echo 'OH_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Oklahoma":
              echo 'OK_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Tennessee":
              echo 'TN_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Kansas":
              echo 'KS_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Mississippi":
              echo 'MS_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Arizona":
              echo 'AR_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "West Virginia":
              echo 'WV_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Pennsylvania":
              echo 'PA_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Maryland":
              echo 'MD_markers.push(rmarker' . $r['ID'] . ');';
              break;
            case "Nebraska":
              echo 'NE_markers.push(rmarker' . $r['ID'] . ');';
              break;
            
            default:
              //Do Nothing...
          }
          
          
  echo 'marker_array.push(rmarker' . $r['ID'] . ');';
        
  echo 'var rcont' . $r['ID'] . ' = document.createElement("div");';
          
  echo 'rcont' . $r['ID'] . '.id = "' . $r['ID'] . '";';
  
  echo 'rcont' . $r['ID'] . '.innerHTML = "<h1><u>' . str_replace('\r','',mysqli_real_escape_string($conn,$r['cname'])) . '</u></h1>";';
				
						
          
  echo '//rcont' . $r['ID'] . '.innerHTML +=  "<h5>Last Visited: ' . $lvdate . '</h5>";
        //rcont' . $r['ID'] . '.innerHTML +=  "<h4><b>Owner:</b> ' . $r['ownername'] . '</h4>";
        rcont' . $r['ID'] . '.innerHTML += "<p><b>Invoice#:</b> <b>' . $r['inv'] . '</b></p>";
        rcont' . $r['ID'] . '.innerHTML +=  "<p><b>Status</b>: ' . $r['status'] . '</p>";
        rcont' . $r['ID'] . '.innerHTML +=  "<p><b>Address</b>: ' . $r['address'] . '</p>";
        rcont' . $r['ID'] . '.innerHTML +=  "<p><b>City: </b>' . $r['city'] . '</p>";
        rcont' . $r['ID'] . '.innerHTML +=  "<p><b>State: </b>' . $r['state'] . '</p>";
        rcont' . $r['ID'] . '.innerHTML +=  "<p><b>Zip: </b>' . $r['zip'] . '</p>";
        rcont' . $r['ID'] . '.innerHTML +=  "<p><b>Phone: </b>' . $r['cphone'] . '</p>";
        //rcont' . $r['ID'] . '.innerHTML +=  "<p><b>Fax: </b>' . $r['fax'] . '</p>";
        //rcont' . $r['ID'] . '.innerHTML +=  "<p><b>Email: </b>";
        //var mcemail = document.createElement("p");
        //var bo = document.createElement("b");
        //bo.textContent = "Email: ";
        //mcemail.appendChild(bo);
        //var cemail = document.createElement("a");
        //cemail.href = "mailto:' . $r['email'] . '";
        //cemail.textContent = "' . $r['email'] . '";
        //mcemail.appendChild(cemail);
        //rcont' . $r['ID'] . '.appendChild(mcemail);
        var btn = document.createElement("button");
        btn.type = "button";
        btn.classList.add("btn");
        btn.classList.add("btn-success");
        btn.setAttribute("onclick", "add_to_route(\'' . $r['ID'] . '\',\'repair\');");
        btn.setAttribute("style", "background:#5cb85c;padding:5px;border-radius:4px;color:white;");
        btn.setAttribute("id", "rbtn_' . $r['ID'] . '");';
          if($r['in_use'] == 'Yes'){
            echo 'btn.setAttribute("disabled", true);';
          }
  echo 'btn.innerHTML = "Add To Route";
        rcont' . $r['ID'] . '.appendChild(btn);
        ';
   
        /*echo 'rmarker' . $r['ID'] . '.addListener("click", function() {
          infoWindow.setContent(rcont' . $r['ID'] . ');
          infoWindow.open(map, rmarker' . $r['ID'] . ');
          $("#rbtn_' . $r['ID'] . '").click(function(){
            rmarker' . $r['ID'] . '.setIcon("https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=truck|bb|Pending|C6EF8C|000000"); // set image path here...
          });
        });
        ';*/
          
        echo 'rmarker' . $r['ID'] . '.addListener("click", function() {
          infoWindow.setContent(rcont' . $r['ID'] . ');
          infoWindow.open(map, rmarker' . $r['ID'] . ');
          $("#rbtn_' . $r['ID'] . '").click(function(){
            rmarker' . $r['ID'] . '.setIcon("https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=truck|bb|Pending|C6EF8C|000000"); // set image path here...
            var al = stop_marker_array.length;
            stop_marker_array.push(rmarker' . $r['ID'] . ');
            console.log("After Add Marker Array");
            console.log(stop_marker_array);
            setTimeout(function(){
              parent.document.getElementById("repairBTN_' . $r['ID'] . '").addEventListener("click",function(){
                rmarker' . $r['ID'] . '.setIcon(rimg' . $r['ID'] . '); // reset image path here...
                infoWindow.setContent(rcont' . $r['ID'] . ');
                infoWindow.open(map, rmarker' . $r['ID'] . ');
                document.getElementById("rbtn_' . $r['ID'] . '").disabled = false;
                infoWindow.close();
                stop_marker_array.splice(al,1);
                console.log("After Remove Marker Array");
                console.log(stop_marker_array);
              });
            },500);
            infoWindow.close();
          });
        });
        ';
          
       echo '$("body").on("click", "#rx' . $r['ID'] . '", function () {
              document.getElementById("results").style.visibility = "hidden";
              infoWindow.setContent(rcont' . $r['ID'] . ');
              infoWindow.open(map, rmarker' . $r['ID'] . ');
              restore();
              });';
						
          $rrr = $r['ID'];
          $i++;
          $progress = $i / $tnr * 100;
          //echo 'document.getElementById("pb").style.width = "' . $progress . '%";';
        }
        $xix = $i - 1;
        //echo 'document.getElementById("statusfeed").innerHTML = "";';
        //echo 'document.getElementById("loading-title").innerHTML = "";';
        
        echo 'rmarker' . $rrr . '.addListener("visible_changed",function(){
            alert("DONE!");
        });';
          ?>
        
        
var rid = "<?php echo $_GET['edit_route']; ?>";
if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			//alert(this.responseText);
      console.log(this.responseText);
      var q = JSON.parse(this.responseText);
      console.log(q.stop.length);
      var i;
      for(i = 0; i < q.stop.length; i++){
        var qq = JSON.parse(q.stop[i]);
        var sid = qq.stop_id;
        console.log('stop ID: '+sid);
        var smode = qq.stop_type.toLowerCase();
        console.log('Stop Type: '+smode);
        add_to_route(sid,smode,'edit');
      }
      
    }
  }
  xmlhttp.open("GET","../scheduling/php/get-route-details-JSON.php?rid="+rid,true);
if(rid !== ''){  
  xmlhttp.send();
}
        
        
        //Console Log the Entire Marker Array for Debugging...
				console.log("TOTAL MARKER ARRAY");
				console.log(marker_array);
        
        map.addListener('rightclick', function(){
          console.log("MARKER ARRAY:");
          console.log(marker_array);
          console.log("STOP ARRAY:");
          console.log(stop_marker_array);
          console.log("STOP TYPE ARRAY");
          console.log(stop_type);
          console.log("Stop ID ARRAY");
          console.log(stop_id);
          console.log("Indiana Markers: "+IN_markers);
          console.log("Illinois Markers: "+IL_markers);
          console.log("Texas Markers: "+TX_markers);
          console.log("Arkansas Markers: "+AR_markers);
          console.log("Kentucky Markers: "+KY_markers);
          console.log("Michigan Markers: "+MI_markers);
          console.log("Missouri Markers: "+MO_markers);
          console.log("Ohio Markers: "+OH_markers);
          console.log("Oklahoma Markers: "+OK_markers);
          console.log("Tennessee Markers: "+TN_markers);
          var Murl = parent.window.document.getElementById('mapurl_btn').href;
          console.log("Map Directions URL: "+Murl);
          alert("Your Debugging Info is now available in the Console!");
        });
        
        
/*This is the Map State Layer Show/Hide Scripting & Departure Location Settings-----------------------------------------------------------------------------------------------------*/
  var IN_E = window.parent.document.querySelector("input[name=IN_CB]");
    IN_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < IN_markers.length; i++){
          IN_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < IN_markers.length; i++){
          IN_markers[i].setMap(null);
        }
      }
  });   
   var IL_E = window.parent.document.querySelector("input[name=IL_CB]");
    IL_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < IL_markers.length; i++){
          IL_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < IL_markers.length; i++){
          IL_markers[i].setMap(null);
        }
      }
  });    
   var TX_E = window.parent.document.querySelector("input[name=TX_CB]");
    TX_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < TX_markers.length; i++){
          TX_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < TX_markers.length; i++){
          TX_markers[i].setMap(null);
        }
      }
  });   
   var AR_E = window.parent.document.querySelector("input[name=AR_CB]");
    AR_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < AR_markers.length; i++){
          AR_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < AR_markers.length; i++){
          AR_markers[i].setMap(null);
        }
      }
  });   
   var KY_E = window.parent.document.querySelector("input[name=KY_CB]");
    KY_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < KY_markers.length; i++){
          KY_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < KY_markers.length; i++){
          KY_markers[i].setMap(null);
        }
      }
  });   
   var MI_E = window.parent.document.querySelector("input[name=MI_CB]");
    MI_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < MI_markers.length; i++){
          MI_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < MI_markers.length; i++){
          MI_markers[i].setMap(null);
        }
      }
  });   
   var MO_E = window.parent.document.querySelector("input[name=MO_CB]");
    MO_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < MO_markers.length; i++){
          MO_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < MO_markers.length; i++){
          MO_markers[i].setMap(null);
        }
      }
  });   
   var OH_E = window.parent.document.querySelector("input[name=OH_CB]");
    OH_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < OH_markers.length; i++){
          OH_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < OH_markers.length; i++){
          OH_markers[i].setMap(null);
        }
      }
  });   
   var OK_E = window.parent.document.querySelector("input[name=OK_CB]");
    OK_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < OK_markers.length; i++){
          OK_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < OK_markers.length; i++){
          OK_markers[i].setMap(null);
        }
      }
  });   
   var TN_E = window.parent.document.querySelector("input[name=TN_CB]");
    TN_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < TN_markers.length; i++){
          TN_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < TN_markers.length; i++){
          TN_markers[i].setMap(null);
        }
      }
  });
   var KS_E = window.parent.document.querySelector("input[name=KS_CB]");
    KS_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < KS_markers.length; i++){
          KS_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < KS_markers.length; i++){
          KS_markers[i].setMap(null);
        }
      }
  });
   var MS_E = window.parent.document.querySelector("input[name=MS_CB]");
    MS_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < MS_markers.length; i++){
          MS_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < MS_markers.length; i++){
          MS_markers[i].setMap(null);
        }
      }
  });
   var AZ_E = window.parent.document.querySelector("input[name=AZ_CB]");
    AZ_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < AZ_markers.length; i++){
          AZ_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < AZ_markers.length; i++){
          AZ_markers[i].setMap(null);
        }
      }
  });
   var WV_E = window.parent.document.querySelector("input[name=WV_CB]");
    WV_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < WV_markers.length; i++){
          WV_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < WV_markers.length; i++){
          WV_markers[i].setMap(null);
        }
      }
  });
   var PA_E = window.parent.document.querySelector("input[name=PA_CB]");
    PA_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < PA_markers.length; i++){
          PA_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < PA_markers.length; i++){
          PA_markers[i].setMap(null);
        }
      }
  });
   var MD_E = window.parent.document.querySelector("input[name=MD_CB]");
    MD_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < MD_markers.length; i++){
          MD_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < MD_markers.length; i++){
          MD_markers[i].setMap(null);
        }
      }
  });
   var NE_E = window.parent.document.querySelector("input[name=NE_CB]");
    NE_E.addEventListener('change', function() {
      if(this.checked){
        //alert('Checked');
        //Displays all state markers on the map... 
        for (var i = 0; i < NE_markers.length; i++){
          NE_markers[i].setMap(map);
        }
      }else{
        //alert('Not Checked');
        //Hides all state markers on the map...
        for (var i = 0; i < NE_markers.length; i++){
          NE_markers[i].setMap(null);
        }
      }
  });
   
        
        
 window.parent.document.getElementById('to_OK').addEventListener('click', function() {
    //Set the Home Office Marker to Oklahoma Office
    var OKhpoint = new google.maps.LatLng(35.723101,-95.402094);
    hmarker.setPosition(OKhpoint);
    home_address = OK_home_address;
    update_directions_link();
    window.parent.document.getElementById('to_IN').style.display = "inline";
    window.parent.document.getElementById('to_OK').style.display = "none";
    window.parent.document.getElementById('depart_from').innerHTML = "Oklahoma Office";
  });
        
  window.parent.document.getElementById('to_IN').addEventListener('click', function() {
    //Set the Home Office Marker to Oklahoma Office
    var INhpoint = new google.maps.LatLng(40.213878,-85.377739);
    hmarker.setPosition(INhpoint);
    home_address = IN_home_address;
    update_directions_link();
    window.parent.document.getElementById('to_IN').style.display = "none";
    window.parent.document.getElementById('to_OK').style.display = "inline";
    window.parent.document.getElementById('depart_from').innerHTML = "Indiana Office";
  });
        
  
  window.parent.document.getElementById('clear_all_layers').addEventListener('click', function(){
    /*IN_E.checked = false;
    IL_E.checked = false;
    TX_E.checked = false;
    AR_E.checked = false;
    KY_E.checked = false;
    MI_E.checked = false;
    MO_E.checked = false;
    OH_E.checked = false;
    OK_E.checked = false;
    TN_E.checked = false;*/
    //Click Simulation
    IN_E.click();
    IL_E.click();
    TX_E.click();
    AR_E.click();
    KY_E.click();
    MI_E.click();
    MO_E.click();
    OH_E.click();
    OK_E.click();
    TN_E.click();
    KS_E.click();
    MS_E.click();
    AZ_E.click();
    WV_E.click();
    PA_E.click();
    MD_E.click();
    NE_E.click();
    
  });
  
        
  function update_directions_link(){
    var update_mbURL = 'https://www.google.com/maps/dir/?api=1&origin='+home_address+'&destination='+home_address+'&travelmode=driving&waypoints=';
			for( var i = 0; i < stop_address.length; i++){
				if(i > 0){
					update_mbURL = update_mbURL + '%7C';
				}
				update_mbURL = update_mbURL + stop_address[i];
			}
			window.parent.document.getElementById('mapurl_btn').href = update_mbURL;
  }
        
/*This is the end of the Map State Layer Show/Hide Scripting & Departure Location Settings-----------------------------------------------------------------------------------------------------*/
        
/*This is the ROUTE OPTIMIZATION SCRIPT---------------------------------------------------------------------------------------------------------------*/
				
	
//Listen for the "Optimize Route" button to be clicked...
  window.parent.document.getElementById('opt_btn').addEventListener('click', function() {
    if(stop_id.length <= 0){
      alert('You have not added any invoices to the route!');
      return;
    }
    calculateAndDisplayRoute(directionsService, directionsDisplay);
  });
        
			
/*End ROUTE OPTIMIZATION SCRIPT------------------------------------------------------------------------------------------------------------------------*/
        
        function restore(){
          document.getElementById("top").innerHTML = '<input id="pac-input" class="controls" type="text" onkeyup="searcher(this.value);" value="" placeholder="Search Box" style="text-align:center; margin:auto;"/>';
        //THIS IS THE START OF THE AUTOCOMPLETE SEGMENT FOR THE DIRECTION AND/OR CUSTOMER LOCATOR CAPABILITIES...
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
        
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
        
          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
        //THIS IS THE END OF THE AUTOCOMPLETE SEGMENT FOR THE PREDICTIONS ON RANDOM ADDRESS QUERIES...  
        }
        
       //THIS IS THE START OF THE AUTOCOMPLETE SEGMENT FOR THE DIRECTION AND/OR CUSTOMER LOCATOR CAPABILITIES...
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
        
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
        
          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
        //THIS IS THE END OF THE AUTOCOMPLETE SEGMENT FOR THE PREDICTIONS ON RANDOM ADDRESS QUERIES...  
        
				
      }
      document.getElementById("results").style.visibility = "hidden";
			
/*OUTTER ROUTE OPTIMIZATION SCRIPT-------------------------------------------------------------------------------*/
			function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        directionsDisplay.setMap(map);
        opt_status = true;
        //Hides all markers on the map...
        for (var i = 0; i < marker_array.length; i++){
          marker_array[i].setMap(null);
        }
        //Displays ONLY 
        for (var i = 0; i < stop_marker_array.length; i++){
          stop_marker_array[i].setMap(map);
        }
        //hmarker.setMap(map);
        
        //Switch the Optimize button for the View All Button...
        parent.document.getElementById('opt_btn').style.display = "none";
        parent.document.getElementById('confirmRouteBTN').style.display = "inline";
        parent.document.getElementById('cancelRouteBTN').style.display = "inline";
        //parent.document.getElementById('opt_btn2').style.display = "inline";
        
        //Set Optimize Button Listener
        parent.document.getElementById('opt_btn2').addEventListener('click',function(){
          opt_status = false;
          for (var i = 0; i < marker_array.length; i++){
            marker_array[i].setMap(map);
          }
          parent.document.getElementById('opt_btn2').style.display = "none";
          parent.document.getElementById('opt_btn').style.display = "inline";
          var pt = new google.maps.LatLng(40.213362, -85.377858);
          map.setCenter(pt);
          map.setZoom(5);
          directionsDisplay.setMap(null);
        });
        
  var waypts = [];
				for(var i = 0; i < stop_id.length; i++){
					waypts.push({
        location: stop_address[i],
        stopover: true
      });
				}
  /*var checkboxArray = document.getElementById('waypoints');
  for (var i = 0; i < checkboxArray.length; i++) {
    if (checkboxArray.options[i].selected) {
      waypts.push({
        location: checkboxArray[i].value,
        stopover: true
      });
    }
  }*/

  directionsService.route({
    origin: home_address,
    destination: home_address,
    waypoints: waypts,
    optimizeWaypoints: true,
    travelMode: 'DRIVING'
  }, function(response, status) {
    if (status === 'OK') {
      directionsDisplay.setDirections(response);//Function to draw route on the map...
      directions_response = response;
      var route = response.routes[0];
      waypoint_order = route.waypoint_order;
      console.log("Waypoint Order:");
      console.log(waypoint_order);
      var summaryPanel = window.parent.document.getElementById('route_main');
      summaryPanel.innerHTML = '';
			//Setup MAPS URL to add link to button
			mURL = 'https://www.google.com/maps/dir/?api=1&origin='+home_address+'&destination='+home_address+'&travelmode=driving&waypoints=';
      // For each route, display summary information.
			var total_yards = 0;
			var total_dist = 0;
      var total_time = 0;
      for (var i = 0; i < route.legs.length; i++) {
				var x = route.waypoint_order[i];
				
				if(i < route.legs.length - 1){
					if(i > 0){
						mURL = mURL + '%7C';
					}
					mURL = mURL + stop_address[x];
				}
        console.log(mURL);
        var routeSegment = i + 1;
        /*summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
            '</b><br>';
        summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
        summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
        summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
				summaryPanel.innerHTML += route.waypoint_order + '<br>';*/
				total_yards = total_yards + route.legs[i].distance.value;
        total_time = total_time + route.legs[i].duration.value;
        leg_dur.push(route.legs[i].duration.value);
        
				
      }
			//alert(mURL);//ENDING MAP URL...
			parent.window.document.getElementById('mapurl_btn').href = mURL;
			total_yards = total_yards / 1760;//1760 is the number of yards in a mile used for the conversion from yards to miles...
			total_dist = total_yards.toFixed(1);
			window.parent.document.getElementById('route_miles').innerHTML = total_dist;
      
      //Format the Time
      var fTime = formatTime(total_time);
      window.parent.document.getElementById('route_time').innerHTML = fTime;
		
			//var x = route.waypoint_order;
			for(var i = 0; i < route.waypoint_order.length; i++){
				var x = route.waypoint_order[i];
        var sid = stop_id[x];
				var snum = i + 1;
				/*summaryPanel.innerHTML += '<div class="alert alert-warning" style="padding:5px;" id="repair_'+sid+'">'+
                                    '<button id="repairBTN2_'+sid+'" type="button" class="close">x</button>'+	
																		 '<p><b>('+snum+') '+stop_name[x]+'</b></p>'+
																		 '<ul>'+
																		 	'<li>Muskogee, Oklahoma</li>'+
																		 	'<li>Cost: $'+stop_cost[x]+'</li>'+
																		 '</ul>'+
																	 '</div>';*/
      console.log("SID: "+sid);
      var div = document.createElement("DIV");
      div.id = 'repair_'+sid;
      div.classList.add("alert");
      div.classList.add("alert-warning");
      div.style.padding = '5px';
      //Button
      var btn = document.createElement('button');
      btn.setAttribute("type","button");
      btn.id = 'repairBTN_'+sid;
      btn.classList.add("close");
      btn.classList.add("repairBTN");
      btn.innerHTML = 'x';
      //div.appendChild(btn);
      //Paragraph
      var p = document.createElement("p");
      p.innerHTML = '('+snum+') '+stop_name[x];
      p.style.fontWeight = 'bold';
      div.appendChild(p);
      //City, State
      var ul = document.createElement("UL");
      var li = document.createElement("LI");
      li.innerHTML = stop_location[x];
      ul.appendChild(li);
      //Cost
      var li2 = document.createElement("LI");
      li2.innerHTML = 'Cost: $'+stop_cost[x];
      ul.appendChild(li2);
      div.appendChild(ul);
      
      var tt = formatTime(leg_dur[i]);
      var tp = document.createElement("span");
      tp.classList.add("badge");
      tp.innerHTML = "Travel Time: "+tt;
      div.appendChild(tp);
        
      parent.document.getElementById('route_main').appendChild(div);
        
     
        
        
			/*parent.document.getElementById('repairBTN2_'+sid).addEventListener("click", function(){
        console.log('Waypoint '+x+' was removed from the array');
        stop_id.splice(x,1);
        stop_name.splice(x,1);
        route_total_cost = route_total_cost - stop_cost[x];
        stop_cost.splice(x,1);
        stop_address.splice(x,1);
        stop_location.splice(x,1);
        stop_type.splice(x,1);
        parent.document.getElementById('repair_'+sid).remove();
        parent.document.getElementById('route_total').innerHTML = route_total_cost;
        if(opt_status === true){
          calculateAndDisplayRoute(directionsService, directionsDisplay);
        }
        stop_marker_array[x].setIcon
        if(eval("rmarker"+sid)){
            eval("rmarker"+sid).setIcon(["rimg"+sid]); // reset image path here...
            infoWindow.setContent(["rcont"+sid]);
            infoWindow.open(map, ["rmarker"+sid]);
            document.getElementById("rbtn_"+sid).disabled = false;
            infoWindow.close();
        }else if(eval("marker"+sid)){
            eval("marker"+sid).setIcon(["img"+sid]); // reset image path here...
            infoWindow.setContent(["cont"+sid]);
            infoWindow.open(map, ["marker"+sid]);
            document.getElementById("btn_"+sid).disabled = false;
            infoWindow.close();
        }else{
          console.log("Error! ID: "+sid);
          alert("There was an error removing the waypoint!");
        }
      });*/
        
      }
			
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}		
      
			
/* END OUTTER OPTIMIZATION SCRIPT ----------------------------------------------------------------------------*/
 

      
      
      /*(function(){
        alert('ready!');
        parent.document.getElementById('confirmRouteBTN').addEventListener('click', function(){
          var sstop_name = JSON.stringify(stop_name);
          
          if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
          } else {  // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlhttp.onreadystatechange=function() {
            if (this.readyState==4 && this.status==200) {
              
              alert(xmlhttp.reponseText);
              
            }
          }
          xmlhttp.open("GET","http://marketforceapp.com/marketforce/scheduling/php/confirm-route.php?stop_name="+sstop_name,true);
          xmlhttp.send();
          
        });
      }) ();*/
      
      
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg&callback=initAutocomplete&libraries=places">
    </script>
     
  </body>
</html>