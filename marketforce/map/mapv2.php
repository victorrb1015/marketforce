<?php
include '../php/connection.php';

$sarray = array(); //Array to be filled with names and IDs for search function

$q = "SELECT * FROM `dealers` WHERE `lat` != 'Error' AND `lng` != 'Error' AND `lat` != 'Check' AND `lng` != 'Check' AND `inactive` != 'Yes' AND `hidden` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
?>
<html>
  <head>
    <title>Market Force Map Test</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://use.fontawesome.com/70c95f2a09.js"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
        //margin-top: -55%;
        //z-index: -9;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
  height: 45px;
  opacity: 0.9;
}
      #pac-input:focus {
  border-color: #4d90fe;
}
      .pac-controls {
  display: inline-block;
  padding: 5px 11px;
}

.pac-controls label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}
      #top{
        width: 40%;
        //border: 2px solid red;
        text-align: center;
        z-index: 1;
        position: fixed;
        margin-right: 30%;
        margin-left: 30%;
      }
      #pac-input{
        //width:100%;
      }
      #results{
        background-color: #ffffff;
        width: 40%;
        max-height: 50%;
        overflow: scroll;
        position: fixed;
        margin-left: 60%;
        opacity: 0.9;
        z-index: 2;
      }
      #results p{
        font-size: 13px;
      }
      #results p:hover{
        background-color: #E3E3E6;
        cursor: pointer;
      }
      #results a{
        text-decoration: none;
        color: black;
      }
      h5{
        color:red;
      }
      h4{
        color:blue;
      }
      #main{
        width: 100%;
      }
  </style>

    <script>
      //This is the area for the search functionality...
        function searcher(val){
					var val = val.replace('&','%26');
        if(val === ""){
        document.getElementById("results").innerHTML = "";
        document.getElementById("results").style.visibility = "hidden";
        return;
      }
        if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //alert(xmlhttp.reponseText);
      if(xmlhttp.responseText == 1){
        document.getElementById("results").innerHTML = "<h3 style='text-align:center;'><u>Dealer List:</u></h3><h4>No Results Found...</h4>";
        document.getElementById("results").style.visibility = "visible";
        return;
      }
      document.getElementById("results").innerHTML = "<br><u><h3 style='text-align:center;'>Dealer List:</h3></u>" + xmlhttp.responseText + "<br>";
      document.getElementById("results").style.visibility = "visible";
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/map/searcher.php?str="+val,true);
  xmlhttp.send();
        
      }
      
      
      
      	function add_note(xid){
        var rep = '<?php echo $_GET['fn']; ?>';
        var note = document.getElementById("ta_"+xid).value;
        if(note === ''){
					alert("Please enter a note!");
					return;
				}
			
         if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("ta_"+xid).value = '';
			//alert(xmlhttp.reponseText);
      document.getElementById("btn_"+xid).innerHTML = 'Note Added!';
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/map/php/add-note.php?id="+xid+"&rep="+rep+"&note="+note,true);
  xmlhttp.send();
      }
      
    
    </script>
  </head>
  <body>
    <div id="main">
    <div id="top">
    <input id="pac-input" class="controls" type="text" onkeyup="searcher(this.value);" value="" placeholder="Search Box" style="text-align:center; margin:auto;"/>
    </div>
    <div id="results"></div>
    <div id="map"></div>
    </div>
    <script>
      function initAutocomplete(zx){
        if(!zx){
          var zx = 5;
        }
        
     
        
        
        
        //Starts the map...
        //center: new google.maps.LatLng(40.213362, -85.377858),
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(40.213362, -85.377858),
          zoom: zx,
					gestureHandling: 'greedy'
        });
        
        
          
        //Sets up the information window for when a marker is clicked...
        var infoWindow = new google.maps.InfoWindow;
        
        <?php
        while($r = mysqli_fetch_array($g)){
          $state = $r['state'];
          switch ($state){
            case "Indiana":
              $color = "blue";
              break;
            case "Illinois":
              $color = "brown";
              break;
            case "Texas":
              $color = "darkgreen";
              break;
            case "Arkansas":
              $color = "green";
              break;
            case "Kentucky":
              $color = "orange";
              break;
            case "Michigan":
              $color = "paleblue";
              break;
            case "Missouri":
              $color = "pink";
              break;
            case "Ohio":
              $color = "purple";
              break;
            case "Oklahoma":
              $color = "red";
              break;
            case "Tennessee":
              $color = "yellow";
              break;
            default:
              $color = "";
          }
          
        
          
          
        echo 'var point' . $r['ID'] . ' = new google.maps.LatLng("' . $r['lat'] . '","' . $r['lng'] . '");';
        
        if($r['status'] == 'New'){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/google/' . $color . '_MarkerN.png";';
        }
        if($r['status'] == 'Trained'){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/google/' . $color . '_MarkerT.png";';
        }
        if($r['status'] == 'Visited'){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/google/' . $color . '_MarkerV.png";';
        }
				if($r['status'] == 'Price'){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/star-metal.png";';
        }
				if($r['status'] == 'Unmanned Lot'){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/google/' . $color . '_MarkerU.png";';
        }
				if($color == ''){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/warning.png";';
				}
        
   //Get Last Visit Date...
          $lvdq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "' ORDER BY `date` DESC LIMIT 1";
          $lvdg = mysqli_query($conn, $lvdq) or die($conn->error);
          $lvdr = mysqli_fetch_array($lvdg);
          if(mysqli_num_rows($lvdg) !=0){
            $lvdate = date("m/d/Y", strtotime($lvdr['date']));
          }else{
            $lvdate = "Unknown";
          }
          
          
   //Get Owner's Name
          $onq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $r['ID'] . "'";
          $ong = mysqli_query($conn, $onq) or die($conn->error);
          $onr = mysqli_fetch_array($ong);
          
          
  echo 'var marker' . $r['ID'] . ' = new google.maps.Marker({
          position: point' . $r['ID'] . ',
          map: map,
          icon: img' . $r['ID'] . '
        });
        
        
        var cont' . $r['ID'] . ' = document.createElement("div");
        
        var icon = document.createElement("i");
        icon.className = "fa fa-exclamation-triangle";
        
        cont' . $r['ID'] . '.id = "' . $r['ID'] . '";
        cont' . $r['ID'] . '.innerHTML = "<h1><u>' . str_replace('\r','',$r['name']) . '</u></h1>";
        var file_link = document.createElement("a");
        var link_icon = document.createElement("i");
        link_icon.className = "fa fa-list-alt";
        file_link.setAttribute("href", "../print-forms.php?ider=' . $r['ID'] . '");
        file_link.setAttribute("target", "_blank");
        file_link.setAttribute("style", "font-size:20px;color:blue;font-weight:bold;")
        file_link.appendChild(link_icon);
        file_link.textContent = " View Forms";
        cont' . $r['ID'] . '.appendChild(file_link);
        cont' . $r['ID'] . '.innerHTML += "<br><br>";
        ';
        
        //Check for images
        /*  $icq = "SELECT * FROM `dealer_imgs` WHERE `dealer_id` = '" . $r['ID'] . "'";
          $icg = mysqli_query($conn, $icq) or die($conn->error);
          if(mysqli_num_rows($icg) == 0){
            
          }else{
            echo 'var imgc = document.createElement("div");';
            while($icr = mysqli_fetch_array($icg)){
              echo 'var imger = document.createElement("img");
                    var anchor = document.createElement("a");
                    imger.setAttribute("src", "' . $icr['url'] . '");
                    //imger.setAttribute("height", "150");
                    imger.setAttribute("width", "40%");
                    anchor.setAttribute("href", "' . $icr['url'] . '");
                    anchor.setAttribute("target", "_blank");
                    anchor.appendChild(imger);
                    imgc.appendChild(anchor);';
            }
            echo 'cont' . $r['ID'] . '.appendChild(imgc);';
          }*/
          
  echo 'cont' . $r['ID'] . '.innerHTML +=  "<h5>Last Visited: ' . $lvdate . '</h5>";
        cont' . $r['ID'] . '.innerHTML +=  "<h4><b>Owner:</b> ' . $onr['ownername'] . '</h4>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>Address</b>: ' . $r['address'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>City: </b>' . $r['city'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>State: </b>' . $r['state'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>Zip: </b>' . $r['zip'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>Phone: </b>' . $r['phone'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>Fax: </b>' . $r['fax'] . '</p>";
        //cont' . $r['ID'] . '.innerHTML +=  "<p><b>Email: </b>";
        var mcemail = document.createElement("p");
        var bo = document.createElement("b");
        bo.textContent = "Email: ";
        mcemail.appendChild(bo);
        var cemail = document.createElement("a");
        cemail.href = "mailto:' . $r['email'] . '";
        cemail.textContent = "' . $r['email'] . '";
        mcemail.appendChild(cemail);
        cont' . $r['ID'] . '.appendChild(mcemail);
        //cont' . $r['ID'] . '.innerHTML += "</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b><u>Displays:</u></b><br>' . $r['displays'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>2016 Sales: </b>' . $r['2016_sales'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>2015 Sales: </b>' . $r['2015_sales'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>Status: </b>' . $r['status'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<h4><u>Account Notes:</u></h4>";';
    
        //Existing notes query
          $enq = "SELECT * FROM `internal_notes` WHERE `dealer_id` = '" . $r['ID'] . "' ORDER BY `ID` DESC";
          $eng = mysqli_query($conn, $enq) or die($conn->error);
          while($enr = mysqli_fetch_array($eng)){
            echo 'cont' . $r['ID'] . '.innerHTML += "<p><b><u>' . date("m/d/Y", strtotime($enr['date'])) . ' by ' . $enr['rep'] . ' @ ' . date("h:iA", strtotime($enr['time'])) . '</u></b><br>' . mysqli_real_escape_string($conn,$enr['note']) . '</p>";';
            
          }
          
          
        //New Note Form
        echo 'var ta = document.createElement("textarea");
        ta.style.width = "300px";
        ta.style.height = "50px";
        ta.id = "ta_' . $r['ID'] . '";
        cont' . $r['ID'] . '.appendChild(ta);
        var br = document.createElement("br");
        cont' . $r['ID'] . '.appendChild(br);
        var btn = document.createElement("button");
        btn.setAttribute("type", "button");
        btn.style.width = "125px";
        btn.style.background = "green";
        btn.innerHTML = "Add Note";
        btn.id = "btn_' . $r['ID'] . '";
        btn.setAttribute("onclick", "add_note(' . $r['ID'] . ');");
        cont' . $r['ID'] . '.appendChild(btn);
        
        
        marker' . $r['ID'] . '.addListener("click", function() {
          infoWindow.setContent(cont' . $r['ID'] . ');
          infoWindow.open(map, marker' . $r['ID'] . ');
        });
        ';
          
       echo '$("body").on("click", "#x' . $r['ID'] . '", function () {
              document.getElementById("results").style.visibility = "hidden";
              infoWindow.setContent(cont' . $r['ID'] . ');
              infoWindow.open(map, marker' . $r['ID'] . ');
              restore();
              });';
          
        }
          ?>
        
        function restore(){
          document.getElementById("top").innerHTML = '<input id="pac-input" class="controls" type="text" onkeyup="searcher(this.value);" value="" placeholder="Search Box" style="text-align:center; margin:auto;"/>';
        //THIS IS THE START OF THE AUTOCOMPLETE SEGMENT FOR THE DIRECTION AND/OR CUSTOMER LOCATOR CAPABILITIES...
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
        
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
        
          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
        //THIS IS THE END OF THE AUTOCOMPLETE SEGMENT FOR THE PREDICTIONS ON RANDOM ADDRESS QUERIES...  
        }
        
       //THIS IS THE START OF THE AUTOCOMPLETE SEGMENT FOR THE DIRECTION AND/OR CUSTOMER LOCATOR CAPABILITIES...
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
        
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
        
          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
        //THIS IS THE END OF THE AUTOCOMPLETE SEGMENT FOR THE PREDICTIONS ON RANDOM ADDRESS QUERIES...  
        
        
        
      }
      document.getElementById("results").style.visibility = "hidden";
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg&callback=initAutocomplete&libraries=places">
    </script>
     
  </body>
</html>