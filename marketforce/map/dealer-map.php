<?php
/*------------------------------------CHANGE RECORDS--------------------------------------------*/
/*
1. Integrated with Dynamic Icons from Google Charts
2. Inactive Dealers are now visible until manually removed with a note explaining why they are 
	 being removed from the map (Replaced with who? Not being Replace?)
3. 
*/
/*-----------------------------------END CHANGE RECORDS-----------------------------------------*/
include '../php/connection.php';
$quot = "'";
$sarray = array(); //Array to be filled with names and IDs for search function

$sqlD = "SELECT 
      `ID`,
      `name`,
      `address`,
      `city`,
      `zip`,
      `state`,
      `displays`,
      `lat`,
      `lng`,
      `status`,
			`new_signs`,
      `inactive`
      FROM `dealers` 
      WHERE 
      (lat = 'Error' OR lat = '') OR
      (lng = 'Error' OR lng = '') OR
      (lat = 'Check' OR lng = 'Check')";

$qD = mysqli_query($conn, $sqlD) or die($conn->error);
while($rD = mysqli_fetch_assoc($qD)){
  
  $addressURL = $conn->real_escape_string($rD['address']) . " , " . $conn->real_escape_string($rD['city']) . " , " . $conn->real_escape_string($rD['state']) . " , " . $conn->real_escape_string($rD['zip']);
  
  $Url = "https://maps.googleapis.com/maps/api/geocode/json?";
  $datos = [
      "address" => $addressURL,
      "key" => "AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg"
  ];
  $datos = http_build_query($datos);
  $Url .= $datos;
  $aContext = array("http" =>
      array(
          "method" => "GET",
          "header" =>
              'Accept: application/json' . "\r\n" .
              'Content-Type: application/json' . "\r\n",
          'verify_peer' => false,
          'verify_peer_name' => false,
      )
  );
  $cxContext = stream_context_create($aContext);
  $sFile = file_get_contents($Url, false, $cxContext);
  $jData = json_decode(json_encode($sFile), true);
  $jdata = json_decode($jData);

  if (strlen($jdata->status) > 3) {
    $lat = "Error";
    $lng = "Error";
  } else {
    $lat = ($jdata->results[0]->geometry->location->lat) ? $jdata->results[0]->geometry->location->lat : "Error";
    $lng = ($jdata->results[0]->geometry->location->lng) ? $jdata->results[0]->geometry->location->lng : "Error";
  }
  
  $sqlUD = "UPDATE dealers SET lat ='".$lat."' ,lng ='".$lng."' WHERE ID=".$rD['ID'];
  $qUD = mysqli_query($conn, $sqlUD) or die($conn->error);
  
}

$q = "SELECT 
      `ID`,
      `state`,
      `displays`,
      `lat`,
      `lng`,
      `status`,
			`new_signs`,
      `inactive`
      FROM `dealers` 
      WHERE 
      `lat` != 'Error' AND 'lat' != '' AND
      `lng` != 'Error' AND 'lng' != '' AND
      `lat` != 'Check' AND 
      `lng` != 'Check' AND 
      `hidden` != 'Yes'";

//OLD BASIC QUERY
//$q = "SELECT * FROM `dealers` WHERE `lat` != 'Error' AND `lng` != 'Error' AND `lat` != 'Check' AND `lng` != 'Check' AND `inactive` != 'Yes' AND `hidden` != 'Yes'";

$g = mysqli_query($conn, $q) or die($conn->error);
$globg = mysqli_query($conn, $q) or die($conn->error);

//Setup Loading Progress Bar Variables
$trows = mysqli_num_rows($g);
$crow = 0;
$percent = 0;



?>
<html>
  <head>
		<!--<meta http-equiv="Cache-control" content="public">-->
    <title>Market Force Map Test</title>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
		<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/70c95f2a09.js"></script>
		
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
        //margin-top: -55%;
        //z-index: -9;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
  height: 45px;
  opacity: 0.9;
}
      #pac-input:focus {
  border-color: #4d90fe;
}
      .pac-controls {
  display: inline-block;
  padding: 5px 11px;
}

.pac-controls label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}
      #top{
        width: 40%;
        //border: 2px solid red;
        text-align: center;
        z-index: 1;
        position: fixed;
        margin-right: 30%;
        margin-left: 30%;
      }
      #pac-input{
        //width:100%;
      }
      #results{
        background-color: #ffffff;
        width: 40%;
        max-height: 50%;
        overflow: scroll;
        position: fixed;
        margin-left: 60%;
        opacity: 0.9;
        z-index: 2;
      }
      #results p{
        font-size: 13px;
      }
      #results p:hover{
        background-color: #E3E3E6;
        cursor: pointer;
      }
      #results a{
        text-decoration: none;
        color: black;
      }
      h5{
        color:red;
      }
      h4{
        color:blue;
      }
      #main{
        width: 100%;
      }
  </style>
    <script>
			
      //This is the area for the search functionality...
      function searcher(val){
				var val = val.replace('&','%26');
        if(val === ""){
          document.getElementById("results").innerHTML = "";
          document.getElementById("results").style.visibility = "hidden";
          return;
        }
        if (window.XMLHttpRequest) {
          // code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
        } else {  // code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
          if (this.readyState==4 && this.status==200) {
            //alert(xmlhttp.reponseText);
            if(xmlhttp.responseText == 1){
              document.getElementById("results").innerHTML = "<h3 style='text-align:center;'><u>Dealer List:</u></h3><h4>No Results Found...</h4>";
              document.getElementById("results").style.visibility = "visible";
              return;
            }
            document.getElementById("results").innerHTML = "<br><u><h3 style='text-align:center;'>Dealer List:</h3></u>" + xmlhttp.responseText + "<br>";
            document.getElementById("results").style.visibility = "visible";
          }
        }
        xmlhttp.open("GET","searcher.php?str="+val,true);
        xmlhttp.send();
        
      }
      
      
      
      function add_note(xid){
        var rep = '<?php echo $_GET['fn']; ?>';
        var note = document.getElementById("ta_"+xid).value;
        if(note === ''){
					alert("Please enter a note!");
					return;
				}
			
        if (window.XMLHttpRequest) {
          // code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
        } else {  // code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
          if (this.readyState==4 && this.status==200) {
            document.getElementById("ta_"+xid).value = '';
            //alert(xmlhttp.reponseText);
            document.getElementById("btn_"+xid).innerHTML = 'Note Added!';
            fetch_dealer(xid);
          }
        }
        xmlhttp.open("GET","php/add-note.php?id="+xid+"&rep="+rep+"&note="+note,true);
        xmlhttp.send();
      }
			
			
			
			function hide_dealer(xid){
        var rep = '<?php echo $_GET['fn']; ?>';
        var note = document.getElementById("ta_"+xid).value;
        if(note === ''){
					alert("Please enter a note!");
					return;
				}
				note = urlEncode(note);
			
        if (window.XMLHttpRequest) {
          // code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
        } else {  // code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
          if (this.readyState==4 && this.status==200) {
            document.getElementById("ta_"+xid).value = '';
            //alert(xmlhttp.reponseText);
            document.getElementById("btn_"+xid).innerHTML = 'Note Added!';
            window.location.reload();
          }
        }
        xmlhttp.open("GET","php/hide-dealer.php?id="+xid+"&rep="+rep+"&note="+note,true);
        xmlhttp.send();
      }
			
			
			function urlEncode(url){
		url = url.replace(/&/g, '%26'); 
		url = url.replace(/#/g, '%23');
		return url;
	}
			
      
    function update_bar(p){
			window.parent.document.getElementById("progress_bar").style.width = p+"%";
			
			if(p === 100 || p === '100'){
				window.parent.document.getElementById("progress_bar_div").style.visibility = "hidden";
			}
		}
      
  
  function get_directions(url){
    var win = window.location = url;
    //win.focus();
  }
      
  function fetch_dealer(id){
    console.log("Fetching Data For ID#: "+id);
    var xx = document.getElementById(id);
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
    } else {  // code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
      if (this.readyState==4 && this.status==200) {
        
      //Do Something with the Data................................................................
        var q = JSON.parse(this.responseText);
        
          var icon = document.createElement("i");
          icon.className = "fa fa-exclamation-triangle";
          
          xx.innerHTML = "<h1><u>"+q.name+"</u></h1>";
          var file_link = document.createElement("a");
          //var link_icon = document.createElement("i");
          //link_icon.className = "fa fa-list-alt";
          file_link.setAttribute("href", "../print-forms.php?ider="+id);
          file_link.setAttribute("target", "_blank");
          file_link.setAttribute("style", "font-size:20px;color:blue;font-weight:bold;")
          //file_link.appendChild(link_icon);
          file_link.textContent = " View Forms";
          xx.appendChild(file_link);
          xx.innerHTML += "<br><br>";
          var dir_link = document.createElement("a");
          var href = "//maps.apple.com/?daddr="+q.address+"+"+q.city+"+"+q.state+"+"+q.zip;
          var fhref = encodeURI(href);
          dir_link.setAttribute("href", "Javascript: get_directions('"+fhref+"');");
          //dir_link.setAttribute("href", fhref);
          //dir_link.setAttribute("target", "_blank");
          dir_link.setAttribute("style", "font-size:18px;color:green;font-weight:bold;");
          dir_link.textContent = "Directions";
          xx.appendChild(dir_link);//Turn on for Directions
          xx.innerHTML += "<br><br>";
          var view_loc = document.createElement("a");
          view_loc.setAttribute("href", "https://www.google.com/maps/@?api=1&map_action=pano&viewpoint="+q.lat+","+q.lng+"&heading=-45&pitch=38&fov=80");
          view_loc.setAttribute("target", "_blank");
          view_loc.setAttribute("style", "font-size:18px;color:blue;font-weight:bold;");
          view_loc.textContent = "View Location";
          xx.appendChild(view_loc);//Turn on for viewing location on google street view
          //xx.innerHTML += "<a href=' . $quot . '//maps.apple.com/?daddr=' . $r['address'] . '+' . $r['city'] . '+' . $r['state'] . $quot . '>Directions</a>";
          xx.innerHTML +=  "<h5>Last Visited: "+q.last_visit+"</h5>";
          xx.innerHTML +=  "<h4><b>Owner:</b> "+q.owner_name+"</h4>";
          xx.innerHTML +=  "<p><b>Address:</b> "+q.address+"</p>";
          xx.innerHTML +=  "<p><b>City: </b>"+q.city+"</p>";
          xx.innerHTML +=  "<p><b>State: </b>"+q.state+"</p>";
          xx.innerHTML +=  "<p><b>Zip: </b>"+q.zip+"</p>";
          xx.innerHTML +=  "<p><b>Phone: </b>"+q.phone+"</p>";
          xx.innerHTML +=  "<p><b>Fax: </b>"+q.fax+"</p>";
          //cont' . $r['ID'] . '.innerHTML +=  "<p><b>Email: </b>";
          var mcemail = document.createElement("p");
          var bo = document.createElement("b");
          bo.textContent = "Email: ";
          mcemail.appendChild(bo);
          var cemail = document.createElement("a");
          cemail.href = "mailto:"+q.email;
          cemail.textContent = q.email;
          mcemail.appendChild(cemail);
          xx.appendChild(mcemail);
          xx.innerHTML += "<h3><u>Daily Hours</u></h3>";
          if(q.sun_status != 'Closed'){
            xx.innerHTML += "<p>Sunday: <b>"+q.sun_open+" - "+q.sun_close+"</b></p>";
          }else{
            xx.innerHTML += "<p>Sunday: <b style=\"color:red;\">CLOSED</b></p>";
          }
          if(q.mon_status != 'Closed'){
            xx.innerHTML += "<p>Monday: <b>"+q.mon_open+" - "+q.mon_close+"</b></p>";
          }else{
            xx.innerHTML += "<p>Monday: <b style=\"color:red;\">CLOSED</b></p>";
          }
          if(q.tue_status != 'Closed'){
            xx.innerHTML += "<p>Tuesday: <b>"+q.tue_open+" - "+q.tue_close+"</b></p>";
          }else{
            xx.innerHTML += "<p>Tuesday: <b style=\"color:red;\">CLOSED</b></p>";
          }
          if(q.wed_status != 'Closed'){
            xx.innerHTML += "<p>Wednesday: <b>"+q.wed_open+" - "+q.wed_close+"</b></p>";
          }else{
            xx.innerHTML += "<p>Wednesday: <b style=\"color:red;\">CLOSED</b></p>";
          }
          if(q.thu_status != 'Closed'){
            xx.innerHTML += "<p>Thursday: <b>"+q.thu_open+" - "+q.thu_close+"</b></p>";
          }else{
            xx.innerHTML += "<p>Thursday: <b style=\"color:red;\">CLOSED</b></p>";
          }
          if(q.fri_status != 'Closed'){
            xx.innerHTML += "<p>Friday: <b>"+q.fri_open+" - "+q.fri_close+"</b></p>";
          }else{
            xx.innerHTML += "<p>Friday: <b style=\"color:red;\">CLOSED</b></p>";
          }
          if(q.sat_status != 'Closed'){
            xx.innerHTML += "<p>Saturday: <b>"+q.sat_open+" - "+q.sat_close+"</b></p>";
          }else{
            xx.innerHTML += "<p>Saturday: <b style=\"color:red;\">CLOSED</b></p>";
          }
        //Displays
          xx.innerHTML +=  "<p style=\"background-color:yellow;color:black;margin:0px;\"><b><u>Displays:</u></b><br>"+q.displays+"</p>";
        if(q.tcg == 'true'){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>Two Car Garage:</b> "+q.tcg_v1+"X"+q.tcg_v2+"X"+q.tcg_v3+"</p>";
        }
        if(q.g == 'true'){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>Garage:</b> "+q.g_v1+"X"+q.g_v2+"X"+q.g_v3+"</p>";
        }
        if(q.cua == 'true'){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>Combo Unit A-Frame:</b> "+q.cua_v1+"X"+q.cua_v2+"X"+q.cua_v3+"</p>";
        }
        if(q.cur == 'true'){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>Combo Unit Regular:</b> "+q.cur_v1+"X"+q.cur_v2+"X"+q.cur_v3+"</p>";
        }
        if(q.rvp == 'true'){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>RV Port:</b> "+q.rvp_v1+"X"+q.rvp_v2+"X"+q.rvp_v3+"</p>";
        }
        if(q.cst == 'true'){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>Carport Standard:</b> "+q.cst_v1+"X"+q.cst_v2+"X"+q.cst_v3+"</p>";
        }
        if(q.csp == 'true'){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>Carport Special:</b> "+q.csp_v1+"X"+q.csp_v2+"X"+q.csp_v3+"</p>";
        }
        if(q.vb == 'true'){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>Vertical Building:</b> "+q.vb_v1+"X"+q.vb_v2+"X"+q.vb_v3+"</p>";
        }
        if(q.u == 'true'){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>Utility - Front Railing:</b> "+q.u_v1+"X"+q.u_v2+"X"+q.u_v3+"</p>";
        }
        if(q.au == 'true'){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>A-Frame Utility:</b> "+q.au_v1+"X"+q.au_v2+"X"+q.au_v3+"</p>";
        }
        if(q.us == 'true'){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>Utility Standard:</b> "+q.us_v1+"X"+q.us_v2+"X"+q.us_v3+"</p>";
        }
        if(q.vu == 'true'){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>Vertical Utility:</b> "+q.vu_v1+"X"+q.vu_v2+"X"+q.vu_v3+"</p>";
        }
        if(q.display_notes != ''){
          xx.innerHTML += "<p style=\"background-color:yellow;color:black;margin:0px;\"><b>Notes:</b> "+q.display_notes+"</p>";
        }
      
      xx.innerHTML +=  "<p><b>2016 Sales: </b>"+q.sales16+"</p>";
      xx.innerHTML +=  "<p><b>2015 Sales: </b>"+q.sales15+"</p>";
      xx.innerHTML +=  "<p><b>Status: </b>"+q.status+"</p>";
      
    //Active / Inactive .....................................................................
      if(q.inactive != 'Yes'){
        //New Note Form
        xx.innerHTML +=  "<h4><u>Account Notes:</u></h4>";
				function loop_array(item, index){
					xx.innerHTML += item;
				}
				q.account_notes.forEach(loop_array);
        var ta = document.createElement("textarea");
        ta.style.width = "300px";
        ta.style.height = "50px";
        ta.id = "ta_"+id;
        xx.appendChild(ta);
        var br = document.createElement("br");
        xx.appendChild(br);
        var btn = document.createElement("button");
        btn.setAttribute("type", "button");
        btn.style.width = "125px";
        btn.style.background = "green";
        btn.innerHTML = "Add Note";
        btn.id = "btn_"+id;
        btn.setAttribute("onclick", "add_note("+id+");");
        xx.appendChild(btn);
			}else{
				//Removal Reason
				xx.innerHTML +=  "<h4><u>Replacement Note:</u></h4>";
        var ta = document.createElement("textarea");
        ta.style.width = "300px";
        ta.style.height = "50px";
        ta.id = "ta_"+id;
        xx.appendChild(ta);
        var br = document.createElement("br");
        xx.appendChild(br);
        var btn = document.createElement("button");
        btn.setAttribute("type", "button");
        btn.style.width = "125px";
        btn.style.background = "green";
        btn.innerHTML = "Submit";
        btn.id = "btn_"+id;
        btn.setAttribute("onclick", "hide_dealer("+id+");");
        xx.appendChild(btn);
			}
      
    }//End Main DO SOMETHING section...
  }
  xmlhttp.open("GET","php/fetch-dealer.php?id="+id,true);
  xmlhttp.send();
  }
				
    </script>
	<style>
						/* This only works with JavaScript, 
if it's not present, don't show loader */
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	//background: url(../img/loader-128x/Preloader_7.gif) center no-repeat #fff;
  background: url(../img/loaders/loading-circle.gif) center no-repeat rgba(250,250,250,0.5);
}
	</style>
<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script>
//paste this code under the head tag or in a separate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
</script>
  </head>
  <body>
						<!--PRELOADER GIF-->
					<div class="se-pre-con"></div>
		
    <div id="main">
    <div id="top">
    <input id="pac-input" class="controls" type="text" onkeyup="searcher(this.value);" value="" placeholder="Search Box" style="text-align:center; margin:auto;"/>
    </div>
    <div id="results"></div>
    <div id="map"></div>
    </div>
    <script>
			
			<?php
			while($gr = mysqli_fetch_array($globg)){
				echo 'var marker' . $r['ID'] . ';
				';
			}
			?>
			
      function initAutocomplete(zx){
        if(!zx){
          var zx = 5;
        }
        
     
        
        
        //Starts the map...
        //center: new google.maps.LatLng(40.213362, -85.377858),
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(40.213362, -85.377858),
          zoom: zx,
					gestureHandling: 'greedy'
        });
        
        
          
        //Sets up the information window for when a marker is clicked...
        var infoWindow = new google.maps.InfoWindow;
        
        <?php
				$i = 0;//Initial Starting of Dealer Sequence...
				
        while($r = mysqli_fetch_array($g)){
					
					
					
					/*$bName2 = $r['name'];
					
					if($i == 0){//Initial Setup for Parsers
					$bName = $r['name'];
					$bName2 = $r['name'];
					$ini = 'yes';
					$i++;
					}
					//echo 'alert("bname->' . $bName . ' bname2->' . $bName2 . ' ini->' . $ini . '");';
					if($bName != $bName2 || $ini == 'yes'){//If same dealer
						*/
          
          $state = $r['state'];
          switch ($state){
            case "Indiana":
              $color = "blue";
              $colorh = "0000FF";
							$fontc = "FFFFFF";
              break;
            case "Illinois":
              $color = "brown";
              $colorh = "A52A2A";
							$fontc = "FFFFFF";
              break;
            case "Texas":
              $color = "darkgreen";
              $colorh = "006400";
							$fontc = "000000";
              break;
            case "Arkansas":
              $color = "green";
              $colorh = "008000";
							$fontc = "000000";
              break;
            case "Kentucky":
              $color = "orange";
              $colorh = "FFA500";
							$fontc = "000000";
              break;
            case "Michigan":
              $color = "paleblue";
              $colorh = "BDE4FE";
							$fontc = "000000";
              break;
            case "Missouri":
              $color = "pink";
              $colorh = "FFC0CB";
							$fontc = "000000";
              break;
            case "Ohio":
              $color = "purple";
              $colorh = "800080";
							$fontc = "FFFFFF";
              break;
            case "Oklahoma":
              $color = "red";
              $colorh = "FF0000";
							$fontc = "000000";
              break;
            case "Kansas"://Used to be Tennessee...
              $color = "yellow";
              $colorh = "FFFF00";
							$fontc = "000000";
              break;
            case "Iowa":
              $color = "cyan";
              $colorh = "00FFFF";
              $fontc = "000000";
              break;
            case "Tennessee":
              $color = "greenyellow";
              $colorh = "ADFF2F";
              $fontc = "000000";
              break;
            case "Louisiana":
              $color = "mistyrose";
              $colorh = "FFE4E1";
              $fontc = "000000";
              break;
            case "North Carolina":
              $color = "thistle";
              $colorh = "D8BFD8";
              $fontc = "000000";
              break;
            case "Wisconsin":
              $color = "palegoldenrod";
              $colorh = "EEE8AA";
              $fontc = "000000";
              break;
            case "Queretaro":
              $color = "teal";
              $colorh = "008080";
              $fontc = "FFFFFF";
              break;
            case "Generic":
              $color = "lightcyan";
              $colorh = "E0FFFF";
              $fontc = "FFFFFF";
              break;
            default:
              $color = "";
              $colorh = "";
							$fontc = "000000";
          }
          
        //Get Rep's Name...
          $rnq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $r['ID'] . "'";
          
          $rng = mysqli_query($conn,$rnq) or die($conn->error);
          $rnr = mysqli_fetch_array($rng);
          $rawName = explode(" ", $rnr['user']);
          $dRepName = ' - ' . $rawName[0][0] . '.' . $rawName[1][0] . '.';
          
          
        echo 'var point' . $r['ID'] . ' = new google.maps.LatLng("' . $r['lat'] . '","' . $r['lng'] . '");';
        
        if($r['status'] == 'New'){
					if($r['new_signs'] == 'Done'){
          	//echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=star|bb|N|' . $colorh . '|' . $fontc . '";';
          	echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=star|bb|New' . $dRepName . '|' . $colorh . '|' . $fontc . '";';
          	//echo 'var img' . $r['ID'] . ' = "//marketforceapp.com/icons/icon.php?text=New' . $dRepName . '&bg=' . $colorh . '&tcolor=' . $fontc . '";';
					}else{
          	//echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bb|N|' . $colorh . '|' . $fontc . '";';
          	//echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bb|New' . $dRepName . '|' . $colorh . '|' . $fontc . '";';
          	echo 'var img' . $r['ID'] . ' = "//marketforceapp.com/icons/icon.php?text=New' . $dRepName . '&bg=' . $colorh . '&tcolor=' . $fontc . '";';
					}
        }
        if($r['status'] == 'Trained'){
					if($r['new_signs'] == 'Done'){
          	//echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=star|bb|T|' . $colorh . '|' . $fontc . '";';
          	echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=star|bb|Trained' . $dRepName . '|' . $colorh . '|' . $fontc . '";';
          	//echo 'var img' . $r['ID'] . ' = "//marketforceapp.com/icons/icon.php?text=Trained' . $dRepName . '&bg=' . $colorh . '&tcolor=' . $fontc . '";';
					}else{
          	//echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bb|T|' . $colorh . '|' . $fontc . '";';
          	//echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bb|Trained' . $dRepName . '|' . $colorh . '|' . $fontc . '";';
          	echo 'var img' . $r['ID'] . ' = "//marketforceapp.com/icons/icon.php?text=Trained' . $dRepName . '&bg=' . $colorh . '&tcolor=' . $fontc . '";';
					}
        }
        if($r['status'] == 'Visited'){
					$qv = 'V';
					$qvq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "' AND QUARTER(`date`) = '1' AND YEAR(`date`) = '" . date("Y") . "'";
					$qvg = mysqli_query($conn, $qvq) or die($conn->error);
					if(mysqli_num_rows($qvg) > 0){
						$qv .= '1';
					}
					$qvq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "' AND QUARTER(`date`) = '2' AND YEAR(`date`) = '" . date("Y") . "'";
					$qvg = mysqli_query($conn, $qvq) or die($conn->error);
					if(mysqli_num_rows($qvg) > 0){
						$qv .= '2';
					}
					$qvq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "' AND QUARTER(`date`) = '3' AND YEAR(`date`) = '" . date("Y") . "'";
					$qvg = mysqli_query($conn, $qvq) or die($conn->error);
					if(mysqli_num_rows($qvg) > 0){
						$qv .= '3';
					}
					$qvq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "' AND QUARTER(`date`) = '4' AND YEAR(`date`) = '" . date("Y") . "'";
					$qvg = mysqli_query($conn, $qvq) or die($conn->error);
					if(mysqli_num_rows($qvg) > 0){
						$qv .= '4';
					}
          //$qv .= ' - Kelly E.';
					if($r['new_signs'] == 'Done'){
          	echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=star|bb|' . $qv . $dRepName . '|' . $colorh . '|' . $fontc . '";';
          	//echo 'var img' . $r['ID'] . ' = "//marketforceapp.com/icons/icon.php?text=' . $qv . $dRepName . '&bg=' . $colorh . '&tcolor=' . $fontc . '";';
					}else{
          	//echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bb|' . $qv . $dRepName . '|' . $colorh . '|' . $fontc . '";';
          	echo 'var img' . $r['ID'] . ' = "//marketforceapp.com/icons/icon.php?text=' . $qv . $dRepName . '&bg=' . $colorh . '&tcolor=' . $fontc . '";';
					}
        }
				/*if($r['status'] == 'Price'){
          echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin_star|T|' . $colorh . '|000000|F60032";';
        }*/
				if($r['status'] == 'Unmanned Lot'){
					if($r['new_signs'] == 'Done'){
          	//echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin_star|U|' . $colorh . '|' . $fontc . '|000000";';
          	echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=star|bb|Unmanned' . $dRepName . '|' . $colorh . '|' . $fontc . '";';
          	//echo 'var img' . $r['ID'] . ' = "//marketforceapp.com/icons/icon.php?text=Unmanned' . $dRepName . '&bg=' . $colorh . '&tcolor=' . $fontc . '";';
					}else{
          	//echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin|U|' . $colorh . '|' . $fontc . '";';
          	//echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bb|Unmanned' . $dRepName . '|' . $colorh . '|' . $fontc . '";';
          	echo 'var img' . $r['ID'] . ' = "//marketforceapp.com/icons/icon.php?text=Unmanned' . $dRepName . '&bg=' . $colorh . '&tcolor=' . $fontc . '";';
					}
        }
        if($r['inactive'] == 'Yes'){
          //echo 'var img' . $r['ID'] . ' = "https://chart.googleapis.com/chart?chst=d_map_spin&chld=1|0|' . $colorh . '|7|b|Closed|Dealer";';
          echo 'var img' . $r['ID'] . ' = "//marketforceapp.com/icons/icon.php?text=Closed Dealer&bg=' . $colorh . '&tcolor=' . $fontc . '";';
        }
				if($color == ''){
          echo 'var img' . $r['ID'] . ' = "//marketforceapp.com/marketforce/map/icons/warning.png";';
				}
					

          
          
  echo 'var marker' . $r['ID'] . ' = new google.maps.Marker({
          position: point' . $r['ID'] . ',
          map: map,
          icon: img' . $r['ID'] . '
        });
        
        
				
        var cont' . $r['ID'] . ' = document.createElement("div");
        cont' . $r['ID'] . '.id = "' . $r['ID'] . '";
        cont' . $r['ID'] . '.style.maxHeight = "350px";
				';
						
					
        echo 'marker' . $r['ID'] . '.addListener("click", function() {
          infoWindow.setContent(cont' . $r['ID'] . ');
          infoWindow.open(map, marker' . $r['ID'] . ');
          fetch_dealer(' . $r['ID'] . ');
        });
				
        ';
          
       echo '$("body").on("click",".x' . $r['ID'] . '",function () {
              document.getElementById("results").style.visibility = "hidden";
              infoWindow.setContent(cont' . $r['ID'] . ');
              infoWindow.open(map, marker' . $r['ID'] . ');
              fetch_dealer(' . $r['ID'] . ');
              });
							';
						
    
          
        }//End main while "$r =" loop...
          ?>
          
<?php
//Pull Global Marked Dealers from other organizations...
if($_GET['org_global_map'] == 'Yes'){
    echo 'console.warn("Global Map ON...");';
    $mconn = mysqli_connect('localhost','mburton9_mf','@LHO08nbK)9$','mburton9_marketforce');
    $oq = "SELECT * FROM `organizations` WHERE `inactive` != 'Yes' AND `super_group` = 'ASC'";
    $og = mysqli_query($mconn, $oq);
    while($or = mysqli_fetch_array($og)){
    	$odbname = $or['db_name'];
      echo 'console.warn("' . $odbname . '");';
    	$nconn = mysqli_connect('localhost','mburton9_mf','@LHO08nbK)9$',$odbname);
    	$gdq = "SELECT * FROM `dealers` WHERE `inactive` != 'Yes' AND `global_dealer` = 'Yes' AND `lat` != 'Error' AND `lng` != 'Error' AND `lat` != 'Check' AND `lng` != 'Check' AND `hidden` != 'Yes'";
    	$gdg = mysqli_query($nconn, $gdq);
    	while($gdr = mysqli_fetch_array($gdg)){
        echo 'console.log("' . mysqli_real_escape_string($nconn,$or['org_name']) . ' - Dealer: ' . mysqli_real_escape_string($nconn,$gdr['name']) . '");';
    		echo 'var gpoint' . $gdr['ID'] . ' = new google.maps.LatLng("' . $gdr['lat'] . '","' . $gdr['lng'] . '");';
        echo 'console.log("' . $gdr['lat'] . '","' . $gdr['lng'] . '");';
    		echo 'var gimg' . $gdr['ID'] . ' = "//marketforceapp.com/marketforce/map/icons/redx.png";';
    		echo 'var gmarker' . $gdr['ID'] . ' = new google.maps.Marker({
          				position: gpoint' . $gdr['ID'] . ',
          				map: map,
          				icon: gimg' . $gdr['ID'] . '
        		  });';
        	echo 'var gcont' . $gdr['ID'] . ' = document.createElement("div");';
        	echo 'gcont' . $gdr['ID'] . '.id = "g' . $gdr['ID'] . '";';
        	echo 'gcont' . $gdr['ID'] . '.style.maxHeight = "350px";';
        	echo 'gcont' . $gdr['ID'] . '.innerHTML = "<b>[Global Dealer] - ' . $or['org_name'] . '</b><br><p style=\"text-align:center;\">' . $gdr['name'] . '</p>";';
        	echo 'gmarker' . $gdr['ID'] . '.addListener("click", function() {
                  infoWindow.setContent(gcont' . $gdr['ID'] . ');
                  infoWindow.open(map, gmarker' . $gdr['ID'] . ');
                  //fetch_dealer(\'g' . $gdr['ID'] . '\');
                });';
        	echo '$("body").on("click",".x' . $gdr['ID'] . '",function () {
                  document.getElementById("results").style.visibility = "hidden";
                  infoWindow.setContent(gcont' . $gdr['ID'] . ');
                  infoWindow.open(map, gmarker' . $gdr['ID'] . ');
                  //fetch_dealer(\'g' . $gdr['ID'] . '\');
            	  });';
    	}
    }
}else{
  echo 'console.warn("Global Map OFF...");';
}
	?>
				
        function restore(){
          document.getElementById("top").innerHTML = '<input id="pac-input" class="controls" type="text" onkeyup="searcher(this.value);" value="" placeholder="Search Box" style="text-align:center; margin:auto;"/>';
        //THIS IS THE START OF THE AUTOCOMPLETE SEGMENT FOR THE DIRECTION AND/OR CUSTOMER LOCATOR CAPABILITIES...
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
        
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
        
          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
        //THIS IS THE END OF THE AUTOCOMPLETE SEGMENT FOR THE PREDICTIONS ON RANDOM ADDRESS QUERIES...  
        }
        
       //THIS IS THE START OF THE AUTOCOMPLETE SEGMENT FOR THE DIRECTION AND/OR CUSTOMER LOCATOR CAPABILITIES...
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
        
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
        
          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
        //THIS IS THE END OF THE AUTOCOMPLETE SEGMENT FOR THE PREDICTIONS ON RANDOM ADDRESS QUERIES...  
        
				
      }
      document.getElementById("results").style.visibility = "hidden";
			
			
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg&callback=initAutocomplete&libraries=places">
    </script>
		<script>
			// The function to trigger the marker click, 'id' is the reference index to the 'markers' array.
				function myClick(mid){
        google.maps.event.trigger(eval("marker"+mid), 'click');
    }
		</script>
  </body>
</html>