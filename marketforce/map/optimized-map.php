<?php
/*------------------------------------CHANGE RECORDS--------------------------------------------*/
/*
1. Changed * to `date` for the last visit date query
2. Setup new main query with INNER JOIN
3. Changed $onr['ownername'] to $r['ownername'] on line 347
4. Commented out query on Lines 296 - 298
5. Changed the Last Visit Date Coding on line 308
6. Added the if statements to parse data for each dealer row
7. Added progress bar percentage calulator for loading purposes
*/
/*-----------------------------------END CHANGE RECORDS-----------------------------------------*/
include '../php/connection.php';

$quot = "'";
$sarray = array(); //Array to be filled with names and IDs for search function

$q = "SELECT 
      `dealers`.`ID` AS ID,
      `dealers`.`name` AS name,
      `dealers`.`address` AS address,
      `dealers`.`city` AS city,
      `dealers`.`state` AS state,
      `dealers`.`zip` AS zip,
      `dealers`.`phone` AS phone,
      `dealers`.`fax` AS fax,
      `dealers`.`email` AS email,
      `dealers`.`displays` AS displays,
      `dealers`.`2016_sales` AS 2016_sales,
      `dealers`.`2015_sales` AS 2015_sales,
      `dealers`.`lat` AS lat,
      `dealers`.`lng` AS lng,
      `dealers`.`status` AS status,
			`dealers`.`sun_open` AS sun_open,
			`dealers`.`sun_close` AS sun_close,
			`dealers`.`sun_status` AS sun_status,
			`dealers`.`mon_open` AS mon_open,
			`dealers`.`mon_close` AS mon_close,
			`dealers`.`mon_status` AS mon_status,
			`dealers`.`tue_open` AS tue_open,
			`dealers`.`tue_close` AS tue_close,
			`dealers`.`tue_status` AS tue_status,
			`dealers`.`wed_open` AS wed_open,
			`dealers`.`wed_close` AS wed_close,
			`dealers`.`wed_status` AS wed_status,
			`dealers`.`thu_open` AS thu_open,
			`dealers`.`thu_close` AS thu_close,
			`dealers`.`thu_status` AS thu_status,
			`dealers`.`fri_open` AS fri_open,
			`dealers`.`fri_close` AS fri_close,
			`dealers`.`fri_status` AS fri_status,
			`dealers`.`sat_open` AS sat_open,
			`dealers`.`sat_close` AS sat_close,
			`dealers`.`sat_status` AS sat_status,
      `new_dealer_form`.`ownername` AS ownername,
      `visit_notes`.`date` AS date,
      `dealer_imgs`.`url` AS url
      FROM `dealers` 
      LEFT JOIN `new_dealer_form`
      ON `dealers`.`ID` = `new_dealer_form`.`ID` 
      LEFT JOIN `visit_notes`
      ON `dealers`.`ID` = `visit_notes`.`dealer_id`
      LEFT JOIN `dealer_imgs`
			ON `dealers`.`ID` = `dealer_imgs`.`dealer_id`
      WHERE 
      `dealers`.`lat` != 'Error' AND 
      `dealers`.`lng` != 'Error' AND 
      `dealers`.`lat` != 'Check' AND 
      `dealers`.`lng` != 'Check' AND 
      `dealers`.`inactive` != 'Yes' AND 
      `dealers`.`hidden` != 'Yes'";

//OLD BASIC QUERY
//$q = "SELECT * FROM `dealers` WHERE `lat` != 'Error' AND `lng` != 'Error' AND `lat` != 'Check' AND `lng` != 'Check' AND `inactive` != 'Yes' AND `hidden` != 'Yes'";

$g = mysqli_query($conn, $q) or die($conn->error);

//Setup Loading Progress Bar Variables
$trows = mysqli_num_rows($g);
$crow = 0;
$percent = 0;

?>
<html>
  <head>
		<!--<meta http-equiv="Cache-control" content="public">-->
    <title>Market Force Map Test</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://use.fontawesome.com/70c95f2a09.js"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
        //margin-top: -55%;
        //z-index: -9;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
  height: 45px;
  opacity: 0.9;
}
      #pac-input:focus {
  border-color: #4d90fe;
}
      .pac-controls {
  display: inline-block;
  padding: 5px 11px;
}

.pac-controls label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}
      #top{
        width: 40%;
        //border: 2px solid red;
        text-align: center;
        z-index: 1;
        position: fixed;
        margin-right: 30%;
        margin-left: 30%;
      }
      #pac-input{
        //width:100%;
      }
      #results{
        background-color: #ffffff;
        width: 40%;
        max-height: 50%;
        overflow: scroll;
        position: fixed;
        margin-left: 60%;
        opacity: 0.9;
        z-index: 2;
      }
      #results p{
        font-size: 13px;
      }
      #results p:hover{
        background-color: #E3E3E6;
        cursor: pointer;
      }
      #results a{
        text-decoration: none;
        color: black;
      }
      h5{
        color:red;
      }
      h4{
        color:blue;
      }
      #main{
        width: 100%;
      }
  </style>
    <script>
			
      //This is the area for the search functionality...
        function searcher(val){
					var val = val.replace('&','%26');
        if(val === ""){
        document.getElementById("results").innerHTML = "";
        document.getElementById("results").style.visibility = "hidden";
        return;
      }
        if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //alert(xmlhttp.reponseText);
      if(xmlhttp.responseText == 1){
        document.getElementById("results").innerHTML = "<h3 style='text-align:center;'><u>Dealer List:</u></h3><h4>No Results Found...</h4>";
        document.getElementById("results").style.visibility = "visible";
        return;
      }
      document.getElementById("results").innerHTML = "<br><u><h3 style='text-align:center;'>Dealer List:</h3></u>" + xmlhttp.responseText + "<br>";
      document.getElementById("results").style.visibility = "visible";
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/map/searcher.php?str="+val,true);
  xmlhttp.send();
        
      }
      
      
      
      	function add_note(xid){
        var rep = '<?php echo $_GET['fn']; ?>';
        var note = document.getElementById("ta_"+xid).value;
        if(note === ''){
					alert("Please enter a note!");
					return;
				}
			
         if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("ta_"+xid).value = '';
			//alert(xmlhttp.reponseText);
      document.getElementById("btn_"+xid).innerHTML = 'Note Added!';
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/map/php/add-note.php?id="+xid+"&rep="+rep+"&note="+note,true);
  xmlhttp.send();
      }
      
    function update_bar(p){
			window.parent.document.getElementById("progress_bar").style.width = p+"%";
			
			if(p === 100 || p === '100'){
				window.parent.document.getElementById("progress_bar_div").style.visibility = "hidden";
			}
		}
    </script>
  </head>
  <body>
		
    <div id="main">
    <div id="top">
    <input id="pac-input" class="controls" type="text" onkeyup="searcher(this.value);" value="" placeholder="Search Box" style="text-align:center; margin:auto;"/>
    </div>
    <div id="results"></div>
    <div id="map"></div>
    </div>
    <script>
      function initAutocomplete(zx){
        if(!zx){
          var zx = 5;
        }
        
     
        
        
        
        //Starts the map...
        //center: new google.maps.LatLng(40.213362, -85.377858),
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(40.213362, -85.377858),
          zoom: zx,
					gestureHandling: 'greedy'
        });
        
        
          
        //Sets up the information window for when a marker is clicked...
        var infoWindow = new google.maps.InfoWindow;
        
        <?php
				$i = 0;//Initial Starting of Dealer Sequence...
				
        while($r = mysqli_fetch_array($g)){
					
					//Update Progress Percentage
				$crow++;
				$percent = ($crow / $trows) * 100;
				//echo 'update_bar(' . $percent . ');';
					
					
					$bName2 = $r['name'];
					
					if($i == 0){//Initial Setup for Parsers
					$bName = $r['name'];
					$bName2 = $r['name'];
					$ini = 'yes';
					$i++;
					}
					//echo 'alert("bname->' . $bName . ' bname2->' . $bName2 . ' ini->' . $ini . '");';
					if($bName != $bName2 || $ini == 'yes'){//If same dealer
						
          $state = $r['state'];
          switch ($state){
            case "Indiana":
              $color = "blue";
              break;
            case "Illinois":
              $color = "brown";
              break;
            case "Texas":
              $color = "darkgreen";
              break;
            case "Arkansas":
              $color = "green";
              break;
            case "Kentucky":
              $color = "orange";
              break;
            case "Michigan":
              $color = "paleblue";
              break;
            case "Missouri":
              $color = "pink";
              break;
            case "Ohio":
              $color = "purple";
              break;
            case "Oklahoma":
              $color = "red";
              break;
            case "Tennessee":
              $color = "yellow";
              break;
            default:
              $color = "";
          }
          
        
          
          
        echo 'var point' . $r['ID'] . ' = new google.maps.LatLng("' . $r['lat'] . '","' . $r['lng'] . '");';
        
        if($r['status'] == 'New'){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/google/' . $color . '_MarkerN.png";';
        }
        if($r['status'] == 'Trained'){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/google/' . $color . '_MarkerT.png";';
        }
        if($r['status'] == 'Visited'){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/google/' . $color . '_MarkerV.png";';
        }
				if($r['status'] == 'Price'){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/star-metal.png";';
        }
				if($r['status'] == 'Unmanned Lot'){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/google/' . $color . '_MarkerU.png";';
        }
				if($color == ''){
          echo 'var img' . $r['ID'] . ' = "http://marketforceapp.com/marketforce/map/icons/warning.png";';
				}
					
   //Get Last Visit Date...
						if(strlen($r['date']) > 1){
							$lvdate = date("m/d/Y", strtotime($r['date']));
						}else{
							$lvdate = "Unknown";
						}
         /* $lvdq = "SELECT `date` FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "' ORDER BY `date` DESC LIMIT 1";
          $lvdg = mysqli_query($conn, $lvdq) or die($conn->error);
          $lvdr = mysqli_fetch_array($lvdg);
          if(mysqli_num_rows($lvdg) !=0){
            $lvdate = date("m/d/Y", strtotime($lvdr['date']));
          }else{
            $lvdate = "Unknown";
          }*/
          
          
   //Get Owner's Name
         /* $onq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $r['ID'] . "'";
          $ong = mysqli_query($conn, $onq) or die($conn->error);
          $onr = mysqli_fetch_array($ong);*/
          
          
  echo 'var marker' . $r['ID'] . ' = new google.maps.Marker({
          position: point' . $r['ID'] . ',
          map: map,
          icon: img' . $r['ID'] . '
        });
        
        
        var cont' . $r['ID'] . ' = document.createElement("div");
        
        var icon = document.createElement("i");
        icon.className = "fa fa-exclamation-triangle";
        
        cont' . $r['ID'] . '.id = "' . $r['ID'] . '";
        cont' . $r['ID'] . '.innerHTML = "<h1><u>' . str_replace('\r','',$r['name']) . '</u></h1>";
        var file_link = document.createElement("a");
        var link_icon = document.createElement("i");
        link_icon.className = "fa fa-list-alt";
        file_link.setAttribute("href", "../print-forms.php?ider=' . $r['ID'] . '");
        file_link.setAttribute("target", "_blank");
        file_link.setAttribute("style", "font-size:20px;color:blue;font-weight:bold;")
        file_link.appendChild(link_icon);
        file_link.textContent = " View Forms";
        cont' . $r['ID'] . '.appendChild(file_link);
        cont' . $r['ID'] . '.innerHTML += "<br><br>";
				var dir_link = document.createElement("a");
				var href = "http://maps.apple.com/?daddr=' . $r['address'] . '+' . $r['city'] . '+' . $r['state'] . '+' . $r['zip'] . '"
				var fhref = encodeURI(href);
				dir_link.setAttribute("href", fhref);
				dir_link.setAttribute("target", "_blank");
				dir_link.setAttribute("style", "font-size:18px;color:green;font-weight:bold;");
				dir_link.textContent = "Directions";
				cont' . $r['ID'] . '.appendChild(dir_link);//Turn on for Directions
				cont' . $r['ID'] . '.innerHTML += "<br><br>";
				var view_loc = document.createElement("a");
				view_loc.setAttribute("href", "https://www.google.com/maps/@?api=1&map_action=pano&viewpoint=' . $r['lat'] . ',' . $r['lng'] . '&heading=-45&pitch=38&fov=80");
				view_loc.setAttribute("target", "_blank");
				view_loc.setAttribute("style", "font-size:18px;color:blue;font-weight:bold;");
				view_loc.textContent = "View Location";
				cont' . $r['ID'] . '.appendChild(view_loc);//Turn on for viewing location on google street view
        //cont' . $r['ID'] . '.innerHTML += "<a href=' . $quot . 'http://maps.apple.com/?daddr=' . $r['address'] . '+' . $r['city'] . '+' . $r['state'] . $quot . '>Directions</a>";
				';
						
					
        
					
        //Check for images
					
						if($r['url']){
						/*echo 'var imgc = document.createElement("div");';
              echo 'var imger = document.createElement("img");
                    var anchor = document.createElement("a");
                    imger.setAttribute("src", "' . $r['url'] . '");
                    imger.setAttribute("width", "40%");
                    anchor.setAttribute("href", "' . $r['url'] . '");
                    anchor.setAttribute("target", "_blank");
                    anchor.appendChild(imger);
                    imgc.appendChild(anchor);';
							echo 'cont' . $r['ID'] . '.appendChild(imgc);';*/
						}//if url exists
					
						//echo 'alert("bname->' . $bName . ' bname2->' . $bName2 . ' ini->' . $ini . '");';
							/*if($r['url']){
							echo 'var imgc = document.createElement("div");';
              echo 'var imger = document.createElement("img");
                    var anchor = document.createElement("a");
                    imger.setAttribute("src", "' . $r['url'] . '");
                    //imger.setAttribute("height", "150");
                    imger.setAttribute("width", "40%");
                    anchor.setAttribute("href", "' . $r['url'] . '");
                    anchor.setAttribute("target", "_blank");
                    anchor.appendChild(imger);
                    imgc.appendChild(anchor);';
							echo 'cont' . $r['ID'] . '.appendChild(imgc);';
							}//if url exists*/
					
					//OLD IMAGING CODE...
          /*$icq = "SELECT * FROM `dealer_imgs` WHERE `dealer_id` = '" . $r['ID'] . "'";
          $icg = mysqli_query($conn, $icq) or die($conn->error);
          if(mysqli_num_rows($icg) == 0){
          }else{
            echo 'var imgc = document.createElement("div");';
            while($icr = mysqli_fetch_array($icg)){
              echo 'var imger = document.createElement("img");
                    var anchor = document.createElement("a");
                    imger.setAttribute("src", "' . $icr['url'] . '");
                    //imger.setAttribute("height", "150");
                    imger.setAttribute("width", "40%");
                    anchor.setAttribute("href", "' . $icr['url'] . '");
                    anchor.setAttribute("target", "_blank");
                    anchor.appendChild(imger);
                    imgc.appendChild(anchor);';
            }
            echo 'cont' . $r['ID'] . '.appendChild(imgc);';
          }*///End OLD IMAGING CODE...
         
  echo 'cont' . $r['ID'] . '.innerHTML +=  "<h5>Last Visited: ' . $lvdate . '</h5>";
        cont' . $r['ID'] . '.innerHTML +=  "<h4><b>Owner:</b> ' . $r['ownername'] . '</h4>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>Address</b>: ' . $r['address'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>City: </b>' . $r['city'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>State: </b>' . $r['state'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>Zip: </b>' . $r['zip'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>Phone: </b>' . $r['phone'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>Fax: </b>' . $r['fax'] . '</p>";
        //cont' . $r['ID'] . '.innerHTML +=  "<p><b>Email: </b>";
        var mcemail = document.createElement("p");
        var bo = document.createElement("b");
        bo.textContent = "Email: ";
        mcemail.appendChild(bo);
        var cemail = document.createElement("a");
        cemail.href = "mailto:' . $r['email'] . '";
        cemail.textContent = "' . $r['email'] . '";
        mcemail.appendChild(cemail);
        cont' . $r['ID'] . '.appendChild(mcemail);
				cont' . $r['ID'] . '.innerHTML += "<h4>Daily Hours</h4>";';
		if($r['sun_status'] != 'Closed'){
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Sunday: ' . $r['sun_open'] . '-' . $r['sun_close'] . '</p>";';
		}else{
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Sunday: <b>CLOSED</b></p>";';
		}
		if($r['mon_status'] != 'Closed'){
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Monday: ' . $r['mon_open'] . '-' . $r['mon_close'] . '</p>";';
		}else{
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Monday: <b>CLOSED</b></p>";';
		}		 
		if($r['tue_status'] != 'Closed'){
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Tuesday: ' . $r['tue_open'] . '-' . $r['tue_close'] . '</p>";';
		}else{
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Tuesday: <b>CLOSED</b></p>";';
		}
		if($r['wed_status'] != 'Closed'){
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Wednesday: ' . $r['wed_open'] . '-' . $r['wed_close'] . '</p>";';
		}else{
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Wednesday: <b>CLOSED</b></p>";';
		}
		if($r['thu_status'] != 'Closed'){
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Thursday: ' . $r['thu_open'] . '-' . $r['thu_close'] . '</p>";';
		}else{
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Thursday: <b>CLOSED</b></p>";';
		}
		if($r['fri_status'] != 'Closed'){
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Friday: ' . $r['fri_open'] . '-' . $r['fri_close'] . '</p>";';
		}else{
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Friday: <b>CLOSED</b></p>";';
		}
		if($r['sat_status'] != 'Closed'){
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Saturday: ' . $r['sat_open'] . '-' . $r['sat_close'] . '</p>";';
		}else{
			echo 'cont' . $r['ID'] . '.innerHTML += "<p>Saturday: <b>CLOSED</b></p>";';
		}
						
       echo 'cont' . $r['ID'] . '.innerHTML +=  "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b><u>Displays:</u></b><br>' . $r['displays'] . '</p>";';
            
    $diq = "SELECT * FROM `display_orders` WHERE `ID` = '" . $r['ID'] . "'";
    $dig = mysqli_query($conn, $diq) or die($conn->error);
    while($dir = mysqli_fetch_array($dig)){
      
      if($dir['tcg'] == 'true'){
      echo 'cont' . $r['ID'] . '.innerHTML += "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b>Two Car Garage:</b> ' . $dir['tcg_v1'] . 'X' . $dir['tcg_v2'] . 'X' . $dir['tcg_v3'] . '</p>";';
      }
      
      if($dir['g'] == 'true'){
      echo 'cont' . $r['ID'] . '.innerHTML += "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b>Garage:</b> ' . $dir['g_v1'] . 'X' . $dir['g_v2'] . 'X' . $dir['g_v3'] . '</p>";';
      }
      
      if($dir['cua'] == 'true'){
      echo 'cont' . $r['ID'] . '.innerHTML += "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b>Combo Unit A-Frame:</b> ' . $dir['cua_v1'] . 'X' . $dir['cua_v2'] . 'X' . $dir['cua_v3'] . '</p>";';
      }
      
      if($dir['cur'] == 'true'){
      echo 'cont' . $r['ID'] . '.innerHTML += "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b>Combo Unit Regular:</b> ' . $dir['cur_v1'] . 'X' . $dir['cur_v2'] . 'X' . $dir['cur_v3'] . '</p>";';
      }
      
      if($dir['rvp'] == 'true'){
      echo 'cont' . $r['ID'] . '.innerHTML += "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b>RV Port:</b> ' . $dir['rvp_v1'] . 'X' . $dir['rvp_v2'] . 'X' . $dir['rvp_v3'] . '</p>";';
      }
      
      if($dir['cst'] == 'true'){
      echo 'cont' . $r['ID'] . '.innerHTML += "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b>Carport Standard:</b> ' . $dir['cst_v1'] . 'X' . $dir['cst_v2'] . 'X' . $dir['cst_v3'] . '</p>";';
      }
      
      if($dir['csp'] == 'true'){
      echo 'cont' . $r['ID'] . '.innerHTML += "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b>Carport Special:</b> ' . $dir['csp_v1'] . 'X' . $dir['csp_v2'] . 'X' . $dir['csp_v3'] . '</p>";';
      }
      
      if($dir['vb'] == 'true'){
      echo 'cont' . $r['ID'] . '.innerHTML += "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b>Vertical Building:</b> ' . $dir['vb_v1'] . 'X' . $dir['vb_v2'] . 'X' . $dir['vb_v3'] . '</p>";';
      }
      
      if($dir['u'] == 'true'){
      echo 'cont' . $r['ID'] . '.innerHTML += "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b>Utility - Front Railing:</b> ' . $dir['u_v1'] . 'X' . $dir['u_v2'] . 'X' . $dir['u_v3'] . '</p>";';
      }
      
      if($dir['au'] == 'true'){
      echo 'cont' . $r['ID'] . '.innerHTML += "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b>A-Frame Utility:</b> ' . $dir['au_v1'] . 'X' . $dir['au_v2'] . 'X' . $dir['au_v3'] . '</p>";';
      }
      
      if($dir['us'] == 'true'){
      echo 'cont' . $r['ID'] . '.innerHTML += "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b>Utility Standard:</b> ' . $dir['us_v1'] . 'X' . $dir['us_v2'] . 'X' . $dir['us_v3'] . '</p>";';
      }
      
      if($dir['vu'] == 'true'){
      echo 'cont' . $r['ID'] . '.innerHTML += "<p style=' . $quot . 'background-color:yellow;color:black;margin:0px;' . $quot . '><b>Vertical Utility:</b> ' . $dir['vu_v1'] . 'X' . $dir['vu_v2'] . 'X' . $dir['vu_v3'] . '</p>";';
      }
      
    }
  echo 'cont' . $r['ID'] . '.innerHTML +=  "</p>";';
  echo 'cont' . $r['ID'] . '.innerHTML +=  "<p><b>2016 Sales: </b>' . $r['2016_sales'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>2015 Sales: </b>' . $r['2015_sales'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<p><b>Status: </b>' . $r['status'] . '</p>";
        cont' . $r['ID'] . '.innerHTML +=  "<h4><u>Account Notes:</u></h4>";';
					
					
					
        //Existing notes query
          $enq = "SELECT * FROM `internal_notes` WHERE `dealer_id` = '" . $r['ID'] . "' ORDER BY `ID` DESC";
          $eng = mysqli_query($conn, $enq) or die($conn->error);
          while($enr = mysqli_fetch_array($eng)){
            echo 'cont' . $r['ID'] . '.innerHTML += "<p><b><u>' . date("m/d/Y", strtotime($enr['date'])) . ' by ' . $enr['rep'] . ' @ ' . date("h:iA", strtotime($enr['time'])) . '</u></b><br>' . mysqli_real_escape_string($conn,$enr['note']) . '</p>";';
          }
          
          
        //New Note Form
        echo 'var ta = document.createElement("textarea");
        ta.style.width = "300px";
        ta.style.height = "50px";
        ta.id = "ta_' . $r['ID'] . '";
        cont' . $r['ID'] . '.appendChild(ta);
        var br = document.createElement("br");
        cont' . $r['ID'] . '.appendChild(br);
        var btn = document.createElement("button");
        btn.setAttribute("type", "button");
        btn.style.width = "125px";
        btn.style.background = "green";
        btn.innerHTML = "Add Note";
        btn.id = "btn_' . $r['ID'] . '";
        btn.setAttribute("onclick", "add_note(' . $r['ID'] . ');");
        cont' . $r['ID'] . '.appendChild(btn);
        
        
        marker' . $r['ID'] . '.addListener("click", function() {
          infoWindow.setContent(cont' . $r['ID'] . ');
          infoWindow.open(map, marker' . $r['ID'] . ');
        });
        ';
          
       echo '$("body").on("click", "#x' . $r['ID'] . '", function () {
              document.getElementById("results").style.visibility = "hidden";
              infoWindow.setContent(cont' . $r['ID'] . ');
              infoWindow.open(map, marker' . $r['ID'] . ');
              restore();
              });';
						
						$ini = 'no';//End of initialization...
						}else{
							$bName = $r['name'];//Set new name for next loop time...
						}//End if parser...
          
        }//End main while "$r =" loop...
          ?>
        
        function restore(){
          document.getElementById("top").innerHTML = '<input id="pac-input" class="controls" type="text" onkeyup="searcher(this.value);" value="" placeholder="Search Box" style="text-align:center; margin:auto;"/>';
        //THIS IS THE START OF THE AUTOCOMPLETE SEGMENT FOR THE DIRECTION AND/OR CUSTOMER LOCATOR CAPABILITIES...
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
        
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
        
          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
        //THIS IS THE END OF THE AUTOCOMPLETE SEGMENT FOR THE PREDICTIONS ON RANDOM ADDRESS QUERIES...  
        }
        
       //THIS IS THE START OF THE AUTOCOMPLETE SEGMENT FOR THE DIRECTION AND/OR CUSTOMER LOCATOR CAPABILITIES...
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
        
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
        
          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
        //THIS IS THE END OF THE AUTOCOMPLETE SEGMENT FOR THE PREDICTIONS ON RANDOM ADDRESS QUERIES...  
        
				
      }
      document.getElementById("results").style.visibility = "hidden";
			
			
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg&callback=initAutocomplete&libraries=places">
    </script>
     
  </body>
</html>