<?php
include 'connection.php';

//Load Variables
$id = $_GET['id'];

//Main Dealer Database
$q = "SELECT * FROM `dealers` WHERE `ID` = '" . $id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);

//New Dealer Form Database
$nq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $id . "'";
$ng = mysqli_query($conn, $nq) or die($conn->error);
$nr = mysqli_fetch_array($ng);

//Dealer Visit Notes
$vq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $id . "' ORDER BY `date` DESC LIMIT 1";
$vg = mysqli_query($conn, $vq) or die($conn->error);
$vr = mysqli_fetch_array($vg);
  if(strlen($vr['date']) > 1){
    $lvdate = date("m/d/Y", strtotime($vr['date']));
  }else{
    $lvdate = "Unknown";
  }

//Displays
$dq = "SELECT * FROM `display_orders` WHERE `ID` = '" . $id . "'";
$dg = mysqli_query($conn, $dq) or die($conn->error);
$dr = mysqli_fetch_array($dg);

//Existing notes query
$enq = "SELECT * FROM `internal_notes` WHERE `dealer_id` = '" . $id . "' ORDER BY `ID` DESC";
$eng = mysqli_query($conn, $enq) or die($conn->error);
$intNotes = [];
while($enr = mysqli_fetch_array($eng)){
  array_push($intNotes, "<p><b><u>" . date("m/d/Y", strtotime($enr['date'])) . " by " . $enr['rep'] . " @ " . date("h:iA", strtotime($enr['time'])) . "</u></b><br>" . mysqli_real_escape_string($conn,$enr['note']) . "</p>");
}


//JSON Data Build...
$x->ID = $r['ID'];
$x->name = mysqli_real_escape_string($conn, $r['name']);
$x->address = mysqli_real_escape_string($conn, $r['address']);
$x->city = mysqli_real_escape_string($conn, $r['city']);
$x->state = $r['state'];
$x->zip = $r['zip'];
$x->phone = mysqli_real_escape_string($conn, $r['phone']);
$x->fax = mysqli_real_escape_string($conn, $r['fax']);
$x->email = mysqli_real_escape_string($conn, $r['email']);
$x->displays = mysqli_real_escape_string($conn, $r['displays']);
$x->sales16 = $r['2016_sales'];
$x->sales15 = $r['2015_sales'];
$x->lat = $r['lat'];
$x->lng = $r['lng'];
$x->status = $r['status'];
$x->new_signs = $r['new_signs'];
$x->inactive = $r['inactive'];
$x->status = $r['status'];
$x->sun_open = mysqli_real_escape_string($conn, $r['sun_open']);
$x->sun_close = mysqli_real_escape_string($conn, $r['sun_close']);
$x->sun_status = mysqli_real_escape_string($conn, $r['sun_status']);
$x->mon_open = mysqli_real_escape_string($conn, $r['mon_open']);
$x->mon_close = mysqli_real_escape_string($conn, $r['mon_close']);
$x->mon_status = mysqli_real_escape_string($conn, $r['mon_status']);
$x->tue_open = mysqli_real_escape_string($conn, $r['tue_open']);
$x->tue_close = mysqli_real_escape_string($conn, $r['tue_close']);
$x->tue_status = mysqli_real_escape_string($conn, $r['tue_status']);
$x->wed_open = mysqli_real_escape_string($conn, $r['wed_open']);
$x->wed_close = mysqli_real_escape_string($conn, $r['wed_close']);
$x->wed_status = mysqli_real_escape_string($conn, $r['wed_status']);
$x->thu_open = mysqli_real_escape_string($conn, $r['thu_open']);
$x->thu_close = mysqli_real_escape_string($conn, $r['thu_close']);
$x->thu_status = mysqli_real_escape_string($conn, $r['thu_status']);
$x->fri_open = mysqli_real_escape_string($conn, $r['fri_open']);
$x->fri_close = mysqli_real_escape_string($conn, $r['fri_close']);
$x->fri_status = mysqli_real_escape_string($conn, $r['fri_status']);
$x->sat_open = mysqli_real_escape_string($conn, $r['sat_open']);
$x->sat_close = mysqli_real_escape_string($conn, $r['sat_close']);
$x->sat_status = mysqli_real_escape_string($conn, $r['sat_status']);
$x->owner_name = mysqli_real_escape_string($conn, $nr['ownername']);
$x->last_visit = $lvdate;
$x->account_notes = $intNotes;
//Display Info
$x->tcg = $dr['tcg'];
$x->tcg_v1 = $dr['tcg_v1'];
$x->tcg_v2 = $dr['tcg_v2'];
$x->tcg_v3 = $dr['tcg_v3'];
$x->tcg_v4 = $dr['tcg_v4'];
$x->tcg_v5 = $dr['tcg_v5'];
$x->tcg_v6 = $dr['tcg_v6'];
$x->tcg_price = $dr['tcg_price'];
$x->g = $dr['g'];
$x->g_v1 = $dr['g_v1'];
$x->g_v2 = $dr['g_v2'];
$x->g_v3 = $dr['g_v3'];
$x->g_v4 = $dr['g_v4'];
$x->g_v5 = $dr['g_v5'];
$x->g_v6 = $dr['g_v6'];
$x->g_price = $dr['g_price'];
$x->cua = $dr['cua'];
$x->cua_v1 = $dr['cua_v1'];
$x->cua_v2 = $dr['cua_v2'];
$x->cua_v3 = $dr['cua_v3'];
$x->cua_v4 = $dr['cua_v4'];
$x->cua_v5 = $dr['cua_v5'];
$x->cua_v6 = $dr['cua_v6'];
$x->cua_price = $dr['cua_price'];
$x->cur = $dr['cur'];
$x->cur_v1 = $dr['cur_v1'];
$x->cur_v2 = $dr['cur_v2'];
$x->cur_v3 = $dr['cur_v3'];
$x->cur_v4 = $dr['cur_v4'];
$x->cur_v5 = $dr['cur_v5'];
$x->cur_v6 = $dr['cur_v6'];
$x->cur_price = $dr['cur_price'];
$x->rvp = $dr['rvp'];
$x->rvp_v1 = $dr['rvp_v1'];
$x->rvp_v2 = $dr['rvp_v2'];
$x->rvp_v3 = $dr['rvp_v3'];
$x->rvp_v4 = $dr['rvp_v4'];
$x->rvp_v5 = $dr['rvp_v5'];
$x->rvp_v6 = $dr['rvp_v6'];
$x->rvp_price = $dr['rvp_price'];
$x->cst = $dr['cst'];
$x->cst_v1 = $dr['cst_v1'];
$x->cst_v2 = $dr['cst_v2'];
$x->cst_v3 = $dr['cst_v3'];
$x->cst_v4 = $dr['cst_v4'];
$x->cst_v5 = $dr['cst_v5'];
$x->cst_v6 = $dr['cst_v6'];
$x->cst_price = $dr['cst_price'];
$x->csp = $dr['csp'];
$x->csp_v1 = $dr['csp_v1'];
$x->csp_v2 = $dr['csp_v2'];
$x->csp_v3 = $dr['csp_v3'];
$x->csp_v4 = $dr['csp_v4'];
$x->csp_v5 = $dr['csp_v5'];
$x->csp_v6 = $dr['csp_v6'];
$x->csp_price = $dr['csp_price'];
$x->vb = $dr['vb'];
$x->vb_v1 = $dr['vb_v1'];
$x->vb_v2 = $dr['vb_v2'];
$x->vb_v3 = $dr['vb_v3'];
$x->vb_v4 = $dr['vb_v4'];
$x->vb_v5 = $dr['vb_v5'];
$x->vb_v6 = $dr['vb_v6'];
$x->vb_price = $dr['vb_price'];
$x->u = $dr['u'];
$x->u_v1 = $dr['u_v1'];
$x->u_v2 = $dr['u_v2'];
$x->u_v3 = $dr['u_v3'];
$x->u_v4 = $dr['u_v4'];
$x->u_v5 = $dr['u_v5'];
$x->u_v6 = $dr['u_v6'];
$x->u_price = $dr['u_price'];
$x->au = $dr['au'];
$x->au_v1 = $dr['au_v1'];
$x->au_v2 = $dr['au_v2'];
$x->au_v3 = $dr['au_v3'];
$x->au_v4 = $dr['au_v4'];
$x->au_v5 = $dr['au_v5'];
$x->au_v6 = $dr['au_v6'];
$x->au_price = $dr['au_price'];
$x->us = $dr['us'];
$x->us_v1 = $dr['us_v1'];
$x->us_v2 = $dr['us_v2'];
$x->us_v3 = $dr['us_v3'];
$x->us_v4 = $dr['us_v4'];
$x->us_v5 = $dr['us_v5'];
$x->us_v6 = $dr['us_v6'];
$x->us_price = $dr['us_price'];
$x->vu = $dr['vu'];
$x->vu_v1 = $dr['vu_v1'];
$x->vu_v2 = $dr['vu_v2'];
$x->vu_v3 = $dr['vu_v3'];
$x->vu_v4 = $dr['vu_v4'];
$x->vu_v5 = $dr['vu_v5'];
$x->vu_v6 = $dr['vu_v6'];
$x->vu_price = $dr['vu_price'];
$x->display_notes = mysqli_real_escape_string($conn, $dr['notes']);


//echo $x;
$response = json_encode($x);

echo $response;

?>
  