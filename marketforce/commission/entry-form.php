<?php
session_start();

if($_SESSION['in'] != 'Yes'){
  session_destroy();
  echo 'You do not have the authorization to view this page...';
  echo '<script>
        window.location = "../index.php";
        </script>';
  break;
}

include '../php/connection.php';

if(!$_GET['submit']){
  echo '<div style="text-align:center;">';
  echo '<h1>Select A State</h1>';
  echo '<form action="entry-form.php" method="get">';
  echo '<select id="cstate" name="cstate">';
  $csq = "SELECT DISTINCT `state` FROM `dealers` WHERE `state` != '' ORDER BY `state` ASC";
  $csg = mysqli_query($conn, $csq) or die($conn->error);
  while($csr = mysqli_fetch_array($csg)){
    echo '<option value="' . $csr['state'] . '">' . $csr['state'] . '</option>';
  }
  echo '</select>';
  echo '<br><br>';
  echo '<input type="submit" id="submit" name="submit" value="Submit" />';
  echo '</form>';
  echo '</div>';
  break;
}

//Rep's Names...
$rnames = [];
$rq = "SELECT * FROM `users` WHERE `position` = 'Outside Sales Rep' AND `inactive` != 'Yes' ORDER BY `fname` ASC";
$rg = mysqli_query($conn, $rq) or die($conn->error);
while($rr = mysqli_fetch_array($rg)){
  $fullName = $rr['fname'] . ' ' . $rr['lname'];
  array_push($rnames, $fullName);
}

$e = count($rnames);

echo '<html>
      <head>
        <title>Entry Form</title>
        <style>
        th,td{
          border: 1px solid black;
          padding: 10px;
        }
        th{
          background: 
        }
        @media print{
          .print-hide{
            display: none;
          }
        }
        </style>
        <script>
            var rep_name = "' . $_SESSION['full_name'] . '";
            var rep_id = "' . $_SESSION['user_id'] . '";
            
          function complete(id){
            var sales16 = document.getElementById("16sales_"+id).value;
            if(sales16 == ""){
              alert("Please Enter The Sales For 2016!");
              return;
            }
            var sales17 = document.getElementById("17sales_"+id).value;
            if(sales17 == ""){
              alert("Please Enter The Sales For 2017!");
              return;
            }
            var sales18 = document.getElementById("18sales_"+id).value;
            if(sales18 == ""){
              alert("Please Enter The Sales For 2018!");
              return;
            }
            var investments = document.getElementById("investments_"+id).value;
            if(investments == ""){
              alert("Please Enter The Investments!");
              return;
            }
            var rname = document.getElementById("rname_"+id).value;
            if(rname == ""){
              alert("Please Enter The Assigned Rep\'s Name!");
              return;
            }
            if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
          } else {  // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlhttp.onreadystatechange=function() {
            if (this.readyState==4 && this.status==200) {
            
              alert(this.responseText);
              window.location.reload();
              
            }
          }
          xmlhttp.open("GET","update-info.php?mode=complete&id="+id+
                              "&rep_name="+rep_name+
                              "&rep_id="+rep_id+
                              "&sales16="+sales16+
                              "&sales17="+sales17+
                              "&sales18="+sales18+
                              "&investments="+investments+
                              "&rname="+rname,true);
          xmlhttp.send();
          }
          
          
          function uncomplete(id){
            
            if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
          } else {  // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlhttp.onreadystatechange=function() {
            if (this.readyState==4 && this.status==200) {
            
              alert(this.responseText);
              window.location.reload();
              
            }
          }
          xmlhttp.open("GET","update-info.php?mode=uncomplete&id="+id+"&rep_name="+rep_name+"&rep_id="+rep_id,true);
          xmlhttp.send();
          }
        </script>
      </head>
      <body>';

echo '<a href="entry-form.php" class="print-hide">';
echo '<button type="button" style="background:blue;color:white;font-weight:bold;font-size:14px;" class="print-hide"><-- Choose Another State</button>';
echo '</a>';

echo '<h1><u>Dealer Financial Information Form:</u></h1>';

echo '<table>
        <thead>
          <tr>
            <th>Dealer Name</th>
            <th>Location</th>
            <th>2016 Sales</th>
            <th>2017 Sales</th>
            <th>2018 Sales</th>
            <th>Display Investment</th>
            <th>Assigned Rep</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>';


$q = "SELECT 
      `dealers`.`ID` as `ID`,
      `dealers`.`name` as `name`,
      `dealers`.`city` as `city`,
      `dealers`.`state` as `state`,
      REPLACE(`dealers`.`2016_sales`,'$','') as `2016_sales`,
      `dealers`.`2017_sales` as `2017_sales`,
      `dealers`.`2018_sales` as `2018_sales`,
      `dealers`.`investments` as `investments`,
      `dealers`.`fin_completed` as `fin_completed`,
      `dealers`.`inactive` as `inactive`,
      `new_dealer_form`.`user` as `rep_name`
      FROM `dealers` 
      LEFT JOIN `new_dealer_form` ON `dealers`.`ID` = `new_dealer_form`.`ID`
      WHERE 
      `dealers`.`inactive` != 'Yes' AND 
      `dealers`.`state` = '" . $_GET['cstate'] . "' 
      ORDER BY `dealers`.`name` ASC";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  
  if($r['fin_completed'] == 'Yes'){
    echo '<tr style="background:green;font-weight:bold;color:black;">
          <td>' . $r['name'] . '</td>
          <td>' . $r['city'] . ', ' . $r['state'] . '</td>
          <td>
            $' . $r['2016_sales'] . '
          </td>
          <td>
            $' . $r['2017_sales'] . '
          </td>
          <td>
            $' . $r['2018_sales'] . '
          </td>
          <td>
            $' . $r['investments'] . '
          </td>
          <td>
            ' . $r['rep_name'] . '
        </td>
        <td>
          <button type="button" onclick="uncomplete(' . $r['ID'] . ');" style="background:blue;color:white;font-weight:bold;">Edit</button>
        </td>
        </tr>';
  }else{
  echo '<tr>
          <td>' . $r['name'] . '</td>
          <td>' . $r['city'] . ', ' . $r['state'] . '</td>
          <td>
            <input type="text" id="16sales_' . $r['ID'] . '" name="16sales_' . $r['ID'] . '" value="' . $r['2016_sales'] . '" />
          </td>
          <td>
            <input type="text" id="17sales_' . $r['ID'] . '" name="17sales_' . $r['ID'] . '" value="' . $r['2017_sales'] . '" />
          </td>
          <td>
            <input type="text" id="18sales_' . $r['ID'] . '" name="18sales_' . $r['ID'] . '" value="' . $r['2018_sales'] . '" />
          </td>
          <td>
            <input type="text" id="investments_' . $r['ID'] . '" name="investments_' . $r['ID'] . '" value="' . $r['investments'] . '" />
          </td>
          <td>
            <select id="rname_' . $r['ID'] . '" name="rname_' . $r['ID'] . '">
              <option value="">Select One</option>';
      $i = 0;
  while($i < $e){
    if($r['rep_name'] == $rnames[$i]){
      $selected = 'selected';
    }else{
      $selected = '';
    }
    echo '<option value="' . $rnames[$i] . '" ' . $selected . '>' . $rnames[$i] . '</option>';
    $i++;
  }
   echo '</select>
        </td>
        <td>
          <button type="button" onclick="complete(' . $r['ID'] . ');" style="background:green;color:white;font-weight:bold;">Save</button>
        </td>
        </tr>';
  }
  
}

echo '</tbody>
    </table>';


echo '</body>
      </html>';
?>