<?php
include '../php/connection.php';

//Load Variables
$id = $_GET['id'];
$rep_name = mysqli_real_escape_string($conn, $_GET['rep_name']);
$rep_id = mysqli_real_escape_string($conn, $_GET['rep_id']);
$sales16 = mysqli_real_escape_string($conn, $_GET['sales16']);
$sales17 = mysqli_real_escape_string($conn, $_GET['sales17']);
$sales18 = mysqli_real_escape_string($conn, $_GET['sales18']);
$investments = mysqli_real_escape_string($conn, $_GET['investments']);
$rname = mysqli_real_escape_string($conn, $_GET['rname']);
$mode = mysqli_real_escape_string($conn, $_GET['mode']);




if($mode == 'complete'){//Complete Mode...
  
  $q = "UPDATE `dealers` SET
        `2016_sales` = '" . $sales16 . "',
        `2017_sales` = '" . $sales17 . "',
        `2018_sales` = '" . $sales18 . "',
        `investments` = '" . $investments . "',
        `fin_completed` = 'Yes'
        WHERE `ID` = '" . $id . "'";
  mysqli_query($conn, $q) or die($conn->error);
  
  $q2 = "UPDATE `new_dealer_form` SET
          `user` = '" . $rname . "'
          WHERE `ID` = '" . $id . "'";
  mysqli_query($conn, $q2) or die($conn->error);
  
  //Write to Activity Log...
  $log = fopen("activity-log.txt","a");
  fwrite($log, date("m/d/y h:iA") . " --> " . $rep_name . "\n");
  fwrite($log, "Dealer ID: " . $id . " marked as Completed.\n");
  fwrite($log, "Sales 16: $" . $sales16 . "\n");
  fwrite($log, "Sales 17: $" . $sales17 . "\n");
  fwrite($log, "Sales 18: $" . $sales18 . "\n");
  fwrite($log, "Investments: $" . $investments . "\n");
  fwrite($log, "Assigned Rep Name: " . $rname . "\n");
  fwrite($log, "---------------------------------------------------------------------------------------------\n");
  fclose($log);
  
  echo 'Dealer Completed Successfully!';
  
}elseif($mode == 'uncomplete'){//Uncomplete Mode...
  
  $q = "UPDATE `dealers` SET `fin_completed` = '' WHERE `ID` = '" . $id . "'";
  mysqli_query($conn, $q) or die($conn->error);
  
  //Write to Activity Log...
  $log = fopen("activity-log.txt","a");
  fwrite($log, date("m/d/y h:iA") . " --> " . $rep_name . "\n");
  fwrite($log, "Dealer ID: " . $id . " marked as Uncompleted.\n");
  fwrite($log, "---------------------------------------------------------------------------------------------\n");
  fclose($log);
  
  echo 'Dealer Has Been Set To Edit Mode Successfully!';
  
}else{//Default Response...
  
  //Write to Activity Log...
  $log = fopen("activity-log.txt","a");
  fwrite($log, date("m/d/y h:iA") . " --> " . $rep_name . "\n");
  fwrite($log, "Error in processing data for Dealer ID: " . $id . ".\n");
  fwrite($log, "Attempted Mode: " . $mode . "\n");
  fwrite($log, "Attempted Sales 16: $" . $sales16 . "\n");
  fwrite($log, "Attempted Sales 17: $" . $sales17 . "\n");
  fwrite($log, "Attempted Sales 18: $" . $sales18 . "\n");
  fwrite($log, "Attempted Investments: $" . $investments . "\n");
  fwrite($log, "Attempted Assigned Rep Name: " . $rname . "\n");
  fwrite($log, "---------------------------------------------------------------------------------------------\n");
  fclose($log);
  
  echo 'There was an error processing your request!';
  
}