<?php
include '../../php/connection.php';

$cid = $_POST['customer_id'];
//$company_name = mysqli_real_escape_string($conn, $_POST['company_name']);
$fname = mysqli_real_escape_string($conn, $_POST['customer_first_name']);
$lname = mysqli_real_escape_string($conn, $_POST['customer_last_name']);
$email = mysqli_real_escape_string($conn, $_POST['customer_email']);
$phone = mysqli_real_escape_string($conn, $_POST['customer_phone']);
$phone2 = mysqli_real_escape_string($conn, $_POST['customer_phone_2']);
$address = mysqli_real_escape_string($conn, $_POST['customer_address']);
$city = mysqli_real_escape_string($conn, $_POST['customer_city']);
$state = mysqli_real_escape_string($conn, $_POST['customer_state']);
$zip = mysqli_real_escape_string($conn, $_POST['customer_zip']);
//$authUsers = mysqli_real_escape_string($conn, $_POST['authUsers']);
$notes = mysqli_real_escape_string($conn, $_POST['customer_notes']);
$mode = $_POST['customer_mode'];


//Tax Form Uploader...
$newTaxForm = false;
if($_FILES['taxForm']['size'] != 0) {
$newTaxForm = true;
$uid = uniqid();
$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/../img-storage/taxforms/";
$target_file2 = $target_dir . basename($_FILES["taxForm"]["name"]);

$uploadOk = 1;
$imageFileType = pathinfo($target_file2,PATHINFO_EXTENSION);
$target_file = $target_dir . $uid . "_taxForm_" . $idate . "." . $imageFileType;

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
   && $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "pdf" 
   && $imageFileType != "PDF" ) {
    echo "<br>ERROR!- only JPG, JPEG, PNG & GIF files are allowed.";
    //echo "<br><script>alert('" . $imageFileType . "');</script>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "<br>ERROR!- your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["taxForm"]["tmp_name"], $target_file)) {
      
      //INSERT FILE PATH INTO DATABASE
      $file_name_1 = 'http://burtonsolution.tech/img-storage/taxforms/' . $uid . "_taxForm_" . $idate . "." . $imageFileType;
      
        echo "<br>The file '". basename( $_FILES["taxForm"]["name"]). "' has been uploaded.";
        
    }else{
      echo '<br>There was an error moving your file from the temporary location to the permanent location.';
    }
}
}//End File Size Check to see if file exists...







//Add Customer...
if($mode == 'New'){
	
	$aq = "INSERT INTO `sales_customers`
			(
			`date`,
			`time`,
      `company_name`,
			`customer_first_name`,
			`customer_last_name`,
			`customer_email`,
			`customer_phone`,
      `customer_phone_2`,
      `customer_address`,
      `customer_city`,
      `customer_state`,
      `customer_zip`,
      `auth_users`,
      `taxform_url`,
			`customer_notes`,
			`status`,
			`inactive`
			)
			VALUES
			(
			CURRENT_DATE,
			CURRENT_TIME,
      '" . $company_name . "',
			'" . $fname . "',
			'" . $lname . "',
			'" . $email . "',
			'" . $phone . "',
      '" . $phone2 . "',
      '" . $address . "',
      '" . $city . "',
      '" . $state . "',
      '" . $zip . "',
      '" . $authUsers . "',
      '" . $file_name_1 . "',
			'" . $notes . "',
			'Active',
			'No'
			)";

	mysqli_query($conn, $aq) or die($conn->error);
	echo 'Customer Successully Added!';

}

if($mode == 'Edit'){

	$uq = "UPDATE `sales_customers` SET
      `company_name` = '" . $company_name . "',
			`customer_first_name` = '" . $fname . "',
			`customer_last_name` = '" . $lname . "',
			`customer_email` = '" . $email . "',
			`customer_phone` = '" . $phone . "',
     	`customer_phone_2` = '" . $phone2 . "',
      `customer_address` = '" . $address . "',
      `customer_city` = '" . $city . "',
      `customer_state` = '" . $state . "',
      `customer_zip` = '" . $zip . "',
      `auth_users` = '" . $authUsers . "',";
  if($newTaxForm == true){
    $uq .= "`taxform_url` = '" . $file_name_1 . "',";
  }
    $uq .= "`customer_notes` = '" . $notes . "'
			WHERE `ID` = '" . $cid . "'";

	mysqli_query($conn, $uq) or die($conn->error);
	echo '<br>Customer Updated Successfully!';

}

echo '<script>
      window.location = "../../sales.php?tab=customers";
      </script>';

?>