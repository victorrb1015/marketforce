<?php
include '../../php/connection.php';

//Load Variables
$oid = $_GET['oid'];
$rep_name = $_GET['rep_name'];
$rep_id = $_GET['rep_id'];


//Get Order Details...
$q = "SELECT * FROM `orders` WHERE `ID` = '" . $oid . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);

$x->ID = $r['ID'];
$x->date = date("m/d/y", strtotime($r['date']));
$x->time = date("h:i A", strtotime($r['time']));
$x->rep_id = $r['rep_id'];
$x->rep_name = $r['rep_name'];
$x->cname = $r['cname'];
$x->inv = $r['inv'];
$x->due_date = date("m/d/Y", strtotime($r['due_date']));
$x->notes = $r['notes'];
$x->priority = $r['priority'];
$x->file_url = $r['file_url'];
$x->inv_file_url = $r['inv_file_url'];
$x->bol_file_url = $r['bol_file_url'];
$x->inactive = $r['inactive'];

//Status Details
$x->rec_order = $r['rec_order'];
$x->rec_order_date = date("m/d/y", strtotime($r['rec_order_date']));
$x->rec_order_time = date("h:i A", strtotime($r['rec_order_time']));
$rnq = "SELECT * FROM `users` WHERE `ID` = '" . $r['rec_order_rep'] . "'";
$rng = mysqli_query($conn, $rnq) or die($conn->error);
$rnr = mysqli_fetch_array($rng);
$x->rec_order_rep = $rnr['fname'] . ' ' . $rnr['lname'];

$x->in_shop = $r['in_shop'];
$x->in_shop_date = date("m/d/y", strtotime($r['in_shop_date']));
$x->in_shop_time = date("h:i A", strtotime($r['in_shop_time']));
$rnq = "SELECT * FROM `users` WHERE `ID` = '" . $r['in_shop_rep'] . "'";
$rng = mysqli_query($conn, $rnq) or die($conn->error);
$rnr = mysqli_fetch_array($rng);
$x->in_shop_rep = $rnr['fname'] . ' ' . $rnr['lname'];
$x->shopNum = $r['shopNum'];

$x->shop_done = $r['shop_done'];
$x->shop_done_date = date("m/d/y", strtotime($r['shop_done_date']));
$x->shop_done_time = date("h:i A", strtotime($r['shop_done_time']));
$rnq = "SELECT * FROM `users` WHERE `ID` = '" . $r['shop_done_rep'] . "'";
$rng = mysqli_query($conn, $rnq) or die($conn->error);
$rnr = mysqli_fetch_array($rng);
$x->shop_done_rep = $rnr['fname'] . ' ' . $rnr['lname'];

$x->shipped = $r['shipped'];
$x->shipped_date = date("m/d/y", strtotime($r['shipped_date']));
$x->shipped_time = date("h:i A", strtotime($r['shipped_time']));
$rnq = "SELECT * FROM `users` WHERE `ID` = '" . $r['shipped_rep'] . "'";
$rng = mysqli_query($conn, $rnq) or die($conn->error);
$rnr = mysqli_fetch_array($rng);
$x->shipped_rep = $rnr['fname'] . ' ' . $rnr['lname'];
$x->trackingNum = $r['trackingNum'];

$x->invoiced = $r['invoiced'];
$x->invoiced_date = date("m/d/y", strtotime($r['invoiced_date']));
$x->invoiced_time = date("h:i A", strtotime($r['invoiced_time']));
$rnq = "SELECT * FROM `users` WHERE `ID` = '" . $r['invoiced_rep'] . "'";
$rng = mysqli_query($conn, $rnq) or die($conn->error);
$rnr = mysqli_fetch_array($rng);
$x->invoiced_rep = $rnr['fname'] . ' ' . $rnr['lname'];
$x->invoiceNum = $r['invoiceNum'];

$x->paid = $r['paid'];
$x->paid_date = date("m/d/y", strtotime($r['paid_date']));
$x->paid_time = date("h:i A", strtotime($r['paid_time']));
$rnq = "SELECT * FROM `users` WHERE `ID` = '" . $r['paid_rep'] . "'";
$rng = mysqli_query($conn, $rnq) or die($conn->error);
$rnr = mysqli_fetch_array($rng);
$x->paid_rep = $rnr['fname'] . ' ' . $rnr['lname'];
$x->payDetail = $r['payDetail'];


$response = json_encode($x);

echo $response;

?>