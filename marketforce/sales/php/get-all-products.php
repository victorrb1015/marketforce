<?php
include '../../php/connection.php';
//error_reporting(E_ALL);
$x->response = 'GOOD';
$x->products = array();
//Get Details...
$q = "SELECT * FROM `order_products` WHERE `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
$i = 0;
while($r = mysqli_fetch_array($g)){

$x->products[$i]->ID = $r['ID'];
$x->products[$i]->pname = $r['product_name'];
$x->products[$i]->pinfo = $r['product_info'];
$x->products[$i]->punits = $r['product_units'];
$x->products[$i]->pcost = $r['product_cost'];
$x->products[$i]->pprice = $r['product_price'];
$x->products[$i]->pweight = $r['product_weight'];
$x->products[$i]->length_required = $r['length_required'];
$x->products[$i]->color_required = $r['color_required'];
$x->products[$i]->color_list = $r['color_list'];
$x->products[$i]->inactive = $r['inactive'];
  
$i++;
}

$response = json_encode($x);

echo $response;

?>