<?php
include '../../php/connection.php';

//Load Variables...
$mode = mysqli_real_escape_string($conn, $_GET['mode']);
$list = mysqli_real_escape_string($conn, $_GET['list']);
$color = mysqli_real_escape_string($conn, $_GET['color']);
$id = mysqli_real_escape_string($conn, $_GET['id']);
$rep_id = mysqli_real_escape_string($conn, $_GET['rep_id']);
$rep_name = mysqli_real_escape_string($conn, $_GET['rep_name']);


//Add Color Mode...
if($mode == 'Add'){
  //Check if color exists...
  $cq = "SELECT * FROM `order_product_colors` WHERE `color_name` = '" . $color . "' AND `list` = '" . $list . "' AND `inactive` != 'Yes'";
  $cg = mysqli_query($conn, $cq) or die($conn->error);
  if(mysqli_num_rows($cg) > 0){
    //Send Response...
    $x->status = 'BAD';
    $x->response = '<b style="color:red;font-weight:bold;">The color [' . $color . '] already exists on this list!</b>';
  }else{
    //Add new color to the list...
    $iq = "INSERT INTO `order_product_colors`
          (
          `date`,
          `time`,
          `rep_id`,
          `rep_name`,
          `color_name`,
          `list`,
          `inactive`
          )
          VALUES
          (
          CURRENT_DATE,
          CURRENT_TIME,
          '" . $rep_id . "',
          '" . $rep_name . "',
          '" . $color . "',
          '" . $list . "',
          'No'
          )";
    $ig = mysqli_query($conn, $iq) or die($conn->error);
    if($ig){
      //Get Color ID...
      $q = "SELECT MAX(`ID`) AS `ID` FROM `order_product_colors`";
      $g = mysqli_query($conn, $q) or die($conn->error);
      $r = mysqli_fetch_array($g);
      $cID = $r['ID'];
      //Send Success Reponse...
      $x->status = 'GOOD';
      $x->response = '<b style="color:green;font-weight:bold;">The color [' . $color . '] has been added to this list!<b>';
      $x->colorID = $cID;
    }else{
      //Send Success Reponse...
      $x->status = 'BAD';
      $x->response = '<b style="color:red;font-weight:bold;">*There was an error adding the color [' . $color . '] to this list<b>';
    }
  }
  
  $response = json_encode($x);
  echo $response;
  
}//End Add Color Mode...



//Remove Color Mode...
if($mode == 'Remove'){
  //Get Color name...
  $cnq = "SELECT * FROM `order_product_colors` WHERE `ID` = '" . $id . "'";
  $cng = mysqli_query($conn, $cnq) or die($conn->error);
  $cnr = mysqli_fetch_array($cng);
  $cName = $cnr['color_name'];
  //Set color as inactive in the list...
  $uq = "UPDATE `order_product_colors` SET `inactive` = 'Yes' WHERE `ID` = '" . $id . "'";
  $ug = mysqli_query($conn, $uq) or die($conn->error);
  if($ug){
      //Send Success Reponse...
      $x->status = 'GOOD';
      $x->response = '<b style="color:green;font-weight:bold;">The color [' . $cname . '] was removed from this list!<b>';
    }else{
      //Send Success Reponse...
      $x->status = 'BAD';
      $x->response = '<b style="color:red;font-weight:bold;">*There was an error removing the color [' . $color . '] from this list<b>';
    }
  
  $response = json_encode($x);
  echo $response;
  
}//End Remove Color Mode...


?>