<?php
include '../../php/connection.php';

//Load Variables...
$inv = $_GET['inv'];


//Get Details...
$q = "SELECT * FROM `order_invoices` WHERE `inv_num` = '" . $inv . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);

$x->invNum = $r['inv_num'];
$x->invDate = date("F d, Y", strtotime($r['date']));
$x->rep_id = $r['rep_id'];
$x->rep_name = $r['rep_name'];
$x->cid = $r['cid'];
$x->cname = $r['cname'];
$x->freight = $r['freight'];
$x->taxRate = $r['tax_rate'];
$x->notes = $r['notes'];
$x->status = $r['status'];
$x->sa = $r['ship_address'];

//Get Customer Info...
$cq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $r['cid'] . "'";
$cg = mysqli_query($conn, $cq) or die($conn->error);
$cr = mysqli_fetch_array($cg);
$cemail = $cr['customer_email'];
$cphone = $cr['customer_phone'];
$x->cemail = $cemail;
$x->cphone = $cphone;
if($cr['taxform_url'] != ''){
  $x->taxExempt = 'Yes';
  $x->taxExemptForm = $cr['taxform_url'];
}else{
  $x->taxExempt = 'No';
  $x->taxExemptForm = '';
}

//Get Invoice Items
$iq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $inv . "'";
$ig = mysqli_query($conn, $iq) or die($conn->error);
$i = 0;
while($ir = mysqli_fetch_array($ig)){
  $x->items[$i]->pid = $ir['pid'];
  $x->items[$i]->item_name = $ir['item_name'];
  $x->items[$i]->item_desc = $ir['item_desc'];
  $x->items[$i]->item_color = $ir['item_color'];
  $x->items[$i]->item_length = $ir['item_length'];
  $x->items[$i]->item_rate = $ir['item_rate'];
  $x->items[$i]->item_qty = $ir['item_qty'];
  $x->items[$i]->item_price = $ir['item_price'];
  $x->items[$i]->item_weight = $ir['item_weight'];
  $i++;
}

$response = json_encode($x);

echo $response;

?>