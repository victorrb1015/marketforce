<?php
include '../../php/connection.php';

//Load Variables...
$pid = mysqli_real_escape_string($conn, $_GET['id']);
$pname = mysqli_real_escape_string($conn, $_GET['pname']);
$pinfo = mysqli_real_escape_string($conn, $_GET['pinfo']);
$units = mysqli_real_escape_string($conn, $_GET['punits']);
$cost = mysqli_real_escape_string($conn, $_GET['pcost']);
$price = mysqli_real_escape_string($conn, $_GET['pprice']);
$pweight = mysqli_real_escape_string($conn, $_GET['pweight']);
$length = mysqli_real_escape_string($conn, $_GET['length']);
$color = mysqli_real_escape_string($conn, $_GET['color']);
$color_list = mysqli_real_escape_string($conn, $_GET['color_list']);
$mode = mysqli_real_escape_string($conn, $_GET['mode']);


//Add Product...
if($mode == 'New'){

	$aq = "INSERT INTO `order_products`
			(
			`date`,
			`time`,
			`product_name`,
			`product_info`,
			`product_units`,
			`product_cost`,
			`product_price`,
      `product_weight`,
      `length_required`,
      `color_required`,
      `color_list`,
			`inactive`
			)
			VALUES
			(
			CURRENT_DATE,
			CURRENT_TIME,
			'" . $pname . "',
			'" . $pinfo . "',
			'" . $units . "',
			'" . $cost . "',
			'" . $price . "',
      '" . $pweight . "',
      '" . $length . "',
      '" . $color . "',
      '" . $color_list . "',
			'No'
			)";

	mysqli_query($conn, $aq) or die($conn->error);
  echo 'Product Added Successfully!';
}

//Edit Product...
if($mode == 'Edit'){

	$uq = "UPDATE `order_products` SET 
			`product_name` = '" . $pname . "',
			`product_info` = '" . $pinfo . "',
			`product_units` = '" . $units . "',
			`product_cost` = '" . $cost . "',
			`product_price` = '" . $price . "',
      `product_weight` = '" . $pweight . "',
      `length_required` = '" . $length . "',
      `color_required` = '" . $color . "',
      `color_list` = '" . $color_list . "'
			WHERE `ID` = '" . $pid . "'";
			
	mysqli_query($conn, $uq) or die($conn->error);
  echo 'Product Updated Successfully!';

}


?>