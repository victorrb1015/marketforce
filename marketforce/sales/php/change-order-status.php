<?php
include '../../php/connection.php';

//Load Variables...
$oid = $_GET['oid'];
$mode = $_GET['mode'];
$rep_name = $_GET['rep_name'];
$rep_id = $_GET['rep_id'];
$shopNum = mysqli_real_escape_string($conn, $_GET['shopNum']);
$trackingNum = mysqli_real_escape_string($conn, $_GET['trackingNum']);
$invoiceNum = mysqli_real_escape_string($conn, $_GET['invoiceNum']);
$payDetail = mysqli_real_escape_string($conn, $_GET['payDetail']);
$bypass = $_GET['bypass'];
$dir = $_GET['dir'];

//Get Current Inv Num...
$inq = "SELECT * FROM `orders` WHERE `ID` = '" . $oid . "'";
$ing = mysqli_query($conn, $inq) or die($conn->error);
$inr = mysqli_fetch_array($ing);
$invNum = $inr['inv'];


switch ($mode) {
  case 'rec_order':
    $sq = "UPDATE `orders` SET `rec_order` = 'Yes', `rec_order_date` = CURRENT_DATE, `rec_order_time` = CURRENT_TIME, `rec_order_rep` = '" . $rep_id . "', `status` = 'rec_order' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    $uiq = "UPDATE `order_invoices` SET `date` = CURRENT_DATE, `time` = CURRENT_TIME, `status` = 'Pending' WHERE `inv_num` = '" . $invNum . "'";
    mysqli_query($conn, $uiq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'in_shop':
    $sq = "UPDATE `orders` SET `in_shop` = 'Yes', `in_shop_date` = CURRENT_DATE, `in_shop_time` = CURRENT_TIME, `in_shop_rep` = '" . $rep_id . "', `shopNum` = '" . $shopNum . "', `status` = 'in_shop' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'shop_done':
    $sq = "UPDATE `orders` SET  `shop_done` = 'Yes', `shop_done_date` = CURRENT_DATE, `shop_done_time` = CURRENT_TIME, `shop_done_rep` = '" . $rep_id . "', `status` = 'shop_done' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'shipped':
    $sq = "UPDATE `orders` SET  `shipped` = 'Yes', `trackingNum` = '" . $trackingNum . "', `shipped_date` = CURRENT_DATE, `shipped_time` = CURRENT_TIME, `shipped_rep` = '" . $rep_id . "', `status` = 'shipped' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'invoiced':
    if($bypass == 'yes'){
      $sq = "UPDATE `orders` SET `invoiced` = 'Yes', `invoiced_date` = CURRENT_DATE, `invoiced_time` = CURRENT_TIME, `invoiced_rep` = '" . $rep_id . "', `status` = 'invoiced' WHERE `ID` = '" . $oid . "'";
    }else{
      //$sq = "UPDATE `orders` SET  `inv` = '" . $invoiceNum . "', `invoiced` = 'Yes', `invoiceNum` = '" . $invoiceNum . "', `invoiced_date` = CURRENT_DATE, `invoiced_time` = CURRENT_TIME, `invoiced_rep` = '" . $rep_id . "', `status` = 'invoiced' WHERE `ID` = '" . $oid . "'";
      if($dir == 'back'){
        $sq = "UPDATE `orders` SET `invoiced` = 'Yes', `paid` = '', `paid_date` = '0000-00-00', `paid_time` = '00:00:00', `paid_rep` = '', `status` = 'invoiced' WHERE `ID` = '" . $oid . "'";
      }else{
        $sq = "UPDATE `orders` SET `invoiced` = 'Yes', `invoiceNum` = '" . $invoiceNum . "', `invoiced_date` = CURRENT_DATE, `invoiced_time` = CURRENT_TIME, `invoiced_rep` = '" . $rep_id . "', `status` = 'invoiced' WHERE `ID` = '" . $oid . "'";
      }
    }
      
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'paid':
    $sq = "UPDATE `orders` SET  `paid` = 'Yes', `payDetail` = '" . $payDetail . "', `paid_date` = CURRENT_DATE, `paid_time` = CURRENT_TIME, `paid_rep` = '" . $rep_id . "', `status` = 'paid' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  default:
    echo 'Order Status Error!';
    break;
}

?>