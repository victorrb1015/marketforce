<?php
include '../../php/connection.php';

//Load Variables...
$cid = $_GET['cid'];

$cnq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $cid . "'";
$cng = mysqli_query($conn, $cnq) or die($conn->error);
$cnr = mysqli_fetch_array($cng);
$cname = $cnr['company_name'];

//Get Balance Details...
$cq = "SELECT * FROM `order_credit_lines` WHERE `inactive` != 'Yes' AND `cid` = '" . $cid . "'";
$cg = mysqli_query($conn, $cq) or die($conn->error);
$cr = mysqli_fetch_array($cg);

if(mysqli_num_rows($cg) <= 0){
  $x->response = 'GOOD';
  $x->has_credit = 'No';
}else{
  $x->response = 'GOOD';
  $x->has_credit = 'Yes';
  $x->credit_amount = str_replace(',','',$cr['credit_amount']);
  
  $total_balance = 0;
  $oq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `paid` != 'Yes' AND `cname` = '" . $cname . "'";
  $og = mysqli_query($conn, $oq) or die($conn->error);
  while($or = mysqli_fetch_array($og)){
    $iq = "SELECT * FROM `order_invoices` WHERE `inv_num` = '" . $or['inv'] . "'";
    $ig = mysqli_query($conn, $iq) or die($conn->error);
    $ir = mysqli_fetch_array($ig);
    if($ir['status'] == 'Estimate'){
      //Do nothing because this does not add to balance owed...
    }else{
      $total_balance = $total_balance + $ir['freight'];
      $taxRate = str_replace('.','',$ir['tax_rate']);
      $taxRate = '.0' . $taxRate;
      $taxRate = $taxRate + 1;
      $bq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $ir['inv_num'] . "'";
      $bg = mysqli_query($conn, $bq) or die($conn->error);
      while($br = mysqli_fetch_array($bg)){
        $total_balance = ($total_balance + ((int)$br['item_price'] * $taxRate));
      }
    }
  }
  
  $x->current_balance = $total_balance;
  $x->credit_available = ($x->credit_amount - $x->current_balance);
}

$response = json_encode($x);
echo $response;

?>