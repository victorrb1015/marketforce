<?php
//This file is obsolete -- Replaced with the use of get-colors.php...
include '../../php/connection.php';

//Get Colors...
$q = "SELECT * FROM `order_product_colors` WHERE `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);

$i = 0;
while($r = mysqli_fetch_array($g)){
  $x[$i]->color_name = $r['color_name'];
  $x[$i]->color_code = $r['color_code'];
  $i++;
}

$response = json_encode($x);

echo $response;

?>