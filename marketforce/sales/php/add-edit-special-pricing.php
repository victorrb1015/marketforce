<?php
include '../../php/connection.php';

//echo var_dump($_POST);
//Load variables...
$cname = $_POST['sp_cname'];
$cid = $_POST['sp_cid'];
$rep_name = $_POST['sp_rep_name'];
$rep_id = $_POST['sp_rep_id'];

//Get product list...
$pq = "SELECT * FROM `order_products` WHERE `inactive` != 'Yes'";
$pg = mysqli_query($conn, $pq) or die($conn->error);
while($pr = mysqli_fetch_array($pg)){
  //Check if special price was entered for product...
  $cpid = $pr['ID'];
  $prod_price = mysqli_real_escape_string($conn, $_POST[$cpid . '_sprice']);
  if($prod_price != ''){
    //check if product has special price for this customer...
    $pcq = "SELECT * FROM `order_special_pricing` WHERE `pid` = '" . $cpid . "' AND `cid` = '" . $cid . "' AND `inactive` != 'Yes'";
    $pcg = mysqli_query($conn, $pcq) or die($conn->error);
    if(mysqli_num_rows($pcg) > 0){
      //Update the current record...
      $uq = "UPDATE `order_special_pricing` SET `special_price` = '" . $prod_price . "' WHERE `pid` = '" . $cpid . "' AND `cid` = '" . $cid . "'";
      mysqli_query($conn, $uq) or die($conn->error);
    }else{
      //Insert the new special price record...
      $iq = "INSERT INTO `order_special_pricing` 
              (
              `date`,
              `time`,
              `cid`,
              `pid`,
              `special_price`,
              `inactive`
              )
              VALUES
              (
              CURRENT_DATE,
              CURRENT_TIME,
              '" . $cid . "',
              '" . $cpid . "',
              '" . $prod_price . "',
              'No'
              )";
      mysqli_query($conn, $iq) or die($conn->error);
    }
  }
}

echo 'Special Pricing has been updated successfully!';

echo '<script>
        window.location = "../../orders.php";
      </script>';
  
?>