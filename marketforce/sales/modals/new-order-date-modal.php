<div class="modal fade" role="dialog" tabindex="-1" id="newOrderDateModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h3 class="modal-title text-center">New Order Date</h3>
      </div>
      <div class="modal-body">
        <div class="row" style="margin-bottom:15px;">
          <div class="col-lg-3"></div>
          <div class="col-lg-6">
            <input type="text" class="form-control date" id="new_order_date" name="new_order_date" placeholder="mm/dd/yyyy"/>
            <input type="hidden" id="new_order_date_id" name="new_order_date_id" value="" />
          </div>
          <div class="col-lg-3"></div>
      </div>
      <div class="modal-footer">
        <p class="color:red;font-weight:bold;" id="new_order_date_error"></p>
        <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="button" onclick="change_order_date();">Save</button>
      </div>
    </div>
  </div>
</div>
</div>