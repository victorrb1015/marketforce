<div class="modal fade" role="dialog" tabindex="-1" id="newInvNumberModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h3 class="modal-title text-center">New Invoice Number</h3>
      </div>
      <div class="modal-body">
        <div class="row" style="margin-bottom:15px;">
          <div class="col-lg-3"></div>
          <div class="col-lg-6">
            <input type="text" class="form-control" id="new_inv_number" name="new_inv_number" placeholder="Invoice Number Here..." onkeyup="check_inv_number(this.value);"/>
            <input type="hidden" id="new_inv_number_id" name="new_inv_number_id" value="" />
          </div>
          <div class="col-lg-3"></div>
        </div>
        <div class="row" style="margin-bottom:15px;">
          <div class="col-lg-3"></div>
          <div class="col-lg-6 text-center">
            <p style="color:red;font-weight;" id="inv_warn_mess"></p>
            <i class="fa fa-exclamation-triangle" id="inv_warn" style="color:red;font-size:40px;display:none;"></i>
          </div>
          <div class="col-lg-3"></div>
        </div>
      </div>
      <div class="modal-footer">
        <p class="color:red;font-weight:bold;" id="new_inv_number_error"></p>
        <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="button" id="inv_change_btn" onclick="change_inv_number();" disabled>Save</button>
      </div>
    </div>
  </div>
</div>