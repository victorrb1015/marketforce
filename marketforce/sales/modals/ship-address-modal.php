<div class="modal fade" role="dialog" tabindex="-1" id="addShipAddressModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                  <h3 class="text-center modal-title">Manage Shipping Addresses</h3>
                  <h4 class="text-center" style="color:blue;" id="ship-address-customer"></h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                            <p><strong>Address:</strong></p>
                            <input class="form-control" type="text" name="ship_address" placeholder="Address" id="ship_address" style="width: 100%;">
                            <br>
                            <input class="form-control" type="text" name="ship_city" placeholder="City" id="ship_city" style="width: 100%;">
                            <br>
                            <select class="form-control" name="ship_state" id="ship_state" style="width: 100%;">
                              <option value="">State</option>
                              <?php
                              $sq = "SELECT * FROM `states` ORDER BY `state` ASC";
                              $sg = mysqli_query($conn, $sq) or die($conn->error);
                              while($sr = mysqli_fetch_array($sg)){
                                echo '<option value="' . $sr['state'] . '">' . $sr['state'] . '</option>';
                              }
                              ?>
                            </select>
                            <br>
                            <input class="form-control" name="ship_zip" placeholder="Zip Code" id="ship_zip" style="width: 100%;">
                            <p>&nbsp;</p>
                            <button type="button" class="btn btn-success" onclick="add_ship_address();">Add Address</button>
                            <p style="color:red;font-weight:bold;" id="ship_address_error"></p>
                            <hr>
                            <div id="address_bubbles"></div>
                  <input type="hidden" id="ship_address_cid" />
                </div>
                <div class="modal-footer">
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
              </div>
            </div>
        </div>
    </div>