<div class="modal fade" role="dialog" tabindex="-1" id="infoBox">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                  <h3 class="text-center modal-title">Order Info</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div id="infoBoxBody" class="modal-body">
                  
                </div>
                <div class="modal-footer">
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
              </div>
            </div>
        </div>
    </div>