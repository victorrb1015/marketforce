    <div class="modal fade" role="dialog" tabindex="-1" id="specialPricing">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="modal-title text-center">Add/Edit Special Pricing</h3>
                </div>
                <form id="sp_form" action="orders/php/add-edit-special-pricing.php" method="post">
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                  <th style="">Current Products</th>
                                  <th style="">Cost</th>
                                  <th style="">Norm. Price</th>
                                  <th style="">Spec. Price</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                              $plq = "SELECT * FROM `order_products` WHERE `inactive` != 'Yes'";
                              $plg = mysqli_query($conn, $plq) or die($conn->error);
                              while($plr = mysqli_fetch_array($plg)){
                                echo '<tr>
                                    <td style="color: rgb(48,115,173);"><strong>' . $plr['product_name'] . '</strong></td>
                                    <td style="color: rgb(18,18,18);">' . $plr['product_cost'] . '</td>
                                    <td style="color: rgb(18,18,18);">' . $plr['product_price'] . '</td>
                                    <td><input class="form-control" type="text" name="' . $plr['ID'] . '_sprice" id="' . $plr['ID'] . '_sprice" style="color: rgb(49,102,49);"></td>
                                    <td><button type="button" class="close" onclick="remove_special_price(' . $plr['ID'] . ');"><span aria-hidden="true" style="color:red !Important;">×</span></button>
                                </tr>';
                              }
                              ?>
                              <input type="hidden" id="sp_cname" name="sp_cname" value="" />
                              <input type="hidden" id="sp_cid" name="sp_cid" value="" />
                              <input type="hidden" id="sp_rep_name" name="sp_rep_name" value="<?php echo $_SESSION['full_name']; ?>" />
                              <input type="hidden" id="sp_rep_id" name="sp_rep_id" value="<?php echo $_SESSION['user_id']; ?>" />
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" type="submit">Save</button>
                </div>
                      </form>
            </div>
        </div>
    </div>