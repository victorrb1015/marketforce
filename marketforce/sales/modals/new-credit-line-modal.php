<div class="modal fade" role="dialog" tabindex="-1" id="newCreditLineModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h3 class="modal-title text-center">Add/Edit Credit Line</h3>
      </div>
      <div class="modal-body">
        <div class="row" style="padding: 12px;">
          <div>
            <select id="cid" name="cid" class="input-lg" style="width: 100%;">
              <option value="">Select Customer</option>
              <?php
              $cnq = "SELECT * FROM `order_customers` WHERE `inactive` != 'Yes' ORDER BY `company_name` ASC";
              $cng = mysqli_query($conn, $cnq) or die($conn->error);
              while($cnr = mysqli_fetch_array($cng)){
                echo '<option value="' . $cnr['ID'] . '">' . $cnr['company_name'] . '</option>';
              }
              ?>
            </select>
            <p>&nbsp;</p>
            <div class="input-group input-group-lg">
              <div class="input-group-addon" style="width: 122px;"><span>Credit Line</span></div><input class="form-control usd" type="text" id="credit_amount" name="credit_amount">
              <div class="input-group-btn"><button class="btn btn-default" type="button" style="width: 40px;"><i class="fa fa-dollar"></i></button></div>
            </div>
            <p>&nbsp;</p>
            <p style="font-size: 18px;"><strong>Notes:</strong></p><textarea class="input-lg form-control" name="cl_notes" placeholder="Enter Notes Here..." id="cl_notes" style="width: 100%;height: 197px;"></textarea></div>
       <input type="hidden" id="cl_mode" name="cl_mode" value="New" />
          <input type="hidden" id="cl_id" name="cl_id" value="" />
        </div>
      </div>
      <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Close</button><button class="btn btn-primary" type="button" onclick="save_cl();">Save</button></div>
    </div>
  </div>
</div>