function change_product(e){
  var rnum = e.getAttribute('data-rowid');
  var found = false;
  //Search for product ID...
  var searchField = "ID";
  var searchVal = document.getElementById('item_'+rnum).value;
  console.log('searchVal: '+searchVal);
  if(searchVal === 'custom'){
    document.getElementById('desc_'+rnum).value = '';
    document.getElementById('desc_'+rnum).removeAttribute('readonly');
    document.getElementById('color_'+rnum).value = '';
    document.getElementById('color_'+rnum).setAttribute('disabled','true');
    document.getElementById('length_'+rnum).value = 'N/A';
    document.getElementById('length_'+rnum).setAttribute('readonly','readonly');
    document.getElementById('rate_'+rnum).value = '';
    document.getElementById('rate_'+rnum).removeAttribute('readonly');
    document.getElementById('qty_'+rnum).value = '';
    document.getElementById('qty_'+rnum).removeAttribute('readonly');
    document.getElementById('price_'+rnum).value = '';
    //document.getElementById('price_'+rnum).removeAttribute('readonly');
    document.getElementById('weight_'+rnum).value = 0.00;
    document.getElementById('weight_'+rnum).removeAttribute('readonly');
    set_color_list(searchVal,rnum);
    return;
  }
  
  for (var i=0 ; i <= invProduct.length ; i++)
  {
      if (invProduct[i][searchField] == searchVal) {
          //results.push(invProduct[i]);
        found = true;
        document.getElementById('desc_'+rnum).value = '[Per '+invProduct[i].punits+'] ' + invProduct[i].pinfo;
        if(invProduct[i].color_required === 'Yes'){
            var color = 0;
            document.getElementById('color_'+rnum).value = '';
            document.getElementById('color_'+rnum).removeAttribute('disabled');
          }else{
            var color = 1;
            document.getElementById('color_'+rnum).value = '';
            document.getElementById('color_'+rnum).setAttribute('disabled','true');
          }
        if(typeof specPricing[searchVal] !== 'undefined'){
          console.log('specPrice: TRUE');
          document.getElementById('rate_'+rnum).value = specPricing[searchVal];
          document.getElementById('qty_'+rnum).removeAttribute('readonly');
          if(invProduct[i].length_required === 'Yes'){
            var len = 0;
            document.getElementById('length_'+rnum).value = '';
            document.getElementById('length_'+rnum).removeAttribute('readonly');
          }else{
            var len = 1;
            document.getElementById('length_'+rnum).value = 'N/A';
            document.getElementById('length_'+rnum).setAttribute('readonly','readonly');
          }
          document.getElementById('price_'+rnum).value = (specPricing[searchVal] * len).toFixed(2);
        }else{
          console.log('specPrice: FALSE');
          document.getElementById('rate_'+rnum).value = invProduct[i].pprice;
          document.getElementById('qty_'+rnum).removeAttribute('readonly');
          if(invProduct[i].length_required === 'Yes'){
            var len = 0;
            document.getElementById('length_'+rnum).value = '';
            document.getElementById('length_'+rnum).removeAttribute('readonly');
          }else{
            var len = 1;
            document.getElementById('length_'+rnum).value = 'N/A';
            document.getElementById('length_'+rnum).setAttribute('readonly','readonly');
          }
          document.getElementById('price_'+rnum).value = (invProduct[i].pprice * len).toFixed(2);
        }
        document.getElementById('qty_'+rnum).value = '';
        document.getElementById('weight_'+rnum).value = (invProduct[i].pweight * len).toFixed(2);
        document.getElementById('iweight_'+rnum).value = (invProduct[i].pweight);
        console.log('Item Details: '+invProduct[i].pweight);
        console.log('Weight: '+(invProduct[i].pweight * len).toFixed(2));
        
        /*if(invProduct[i].length_required === 'Yes'){
          document.getElementById('length_'+rnum).value = 0;
          document.getElementById('length_'+rnum).removeAttribute('readonly');
        }
        if(invProduct[i].length_required === 'No'){
          document.getElementById('length_'+rnum).value = 0;
          document.getElementById('length_'+rnum).setAttribute('readonly','readonly');
        }*/
        set_color_list(searchVal,rnum);
        calc_total();
        
        return;
      }else{
        document.getElementById('desc_'+rnum).value = '';
        document.getElementById('color_'+rnum).value = '';
        document.getElementById('rate_'+rnum).value = 0.00;
        document.getElementById('qty_'+rnum).value = '';
        document.getElementById('qty_'+rnum).setAttribute('readonly','readonly');
        document.getElementById('price_'+rnum).value = 0.00;
        document.getElementById('length_'+rnum).value = '';
        document.getElementById('length_'+rnum).setAttribute('readonly','readonly');
        document.getElementById('weight_'+rnum).value = 0.00;
        document.getElementById('iweight_'+rnum).value = 0.00;
        calc_total();
      }
    /*if(found === false){
      alert('Error: No Product Info Found!');
    }*/
    document.getElementById('desc_'+rnum).setAttribute('readonly', 'readonly');
    document.getElementById('rate_'+rnum).setAttribute('readonly', 'readonly');
    document.getElementById('price_'+rnum).setAttribute('readonly', 'readonly');
    document.getElementById('weight_'+rnum).setAttribute('readonly', 'readonly');
    set_color_list(searchVal,rnum);
  }

}


function change_qty(e){
  var rnum = e.getAttribute('data-rowid');
  //alert(rnum);
  var qty = document.getElementById('qty_'+rnum).value;
  var rate = document.getElementById('rate_'+rnum).value;
  var len = document.getElementById('length_'+rnum).value;
  var weight = document.getElementById('iweight_'+rnum).value;
  if(len === 'N/A'){
    len = 1;
  }
  var total = (rate * len * qty);
  document.getElementById('price_'+rnum).value = total.toFixed(2);
  var total_weight = (weight * len * qty);
  document.getElementById('weight_'+rnum).value = total_weight.toFixed(2);
  calc_total();
}


function set_color_list(pid, rnum){
  
if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp2=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp2.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      var r = JSON.parse(this.responseText);
      //Remove All options from input...
      var current_color = document.getElementById('color_'+rnum).value;
      console.log(current_color);
      $('#color_'+rnum).empty();
      //Fill input with the color list choices...
      if(r.color_required === 'Yes'){
        var c = eval('prodColors.'+r.color_list+'.colors');
        var select = document.getElementById('color_'+rnum);
        var option = document.createElement('option');
        option.setAttribute('value','');
        option.text = 'N/A';
        select.appendChild(option);
        for(var i = 0; i < c.length; i++){
          var option = document.createElement('option');
          option.setAttribute('value',c[i]);
          option.text = c[i];
          if(c[i] === current_color){
            option.setAttribute('selected','selected');
            console.log('MATCHED'+current_color);
          }
          select.appendChild(option);
        }
      }else{
        r.color_list = 'N/A';
        var select = document.getElementById('color_'+rnum);
        var option = document.createElement('option');
        option.setAttribute('value','');
        option.text = 'N/A';
        select.appendChild(option);
      }
      console.log('Color List Set To: '+r.color_list+' on Item#: '+rnum);
      
    }
  }
  xmlhttp2.open("GET","../php/get-product-details.php?pid="+pid,true);
  xmlhttp2.send();
  
}


function set_all_color_lists(){
  for(var rni = 0; rni < rowNums.length; rni++){
    var pid = document.getElementById('item_'+rowNums[rni]).value;
    set_color_list(pid, rowNums[rni]);
  }
}

