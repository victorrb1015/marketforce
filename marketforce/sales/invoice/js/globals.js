//Global Variables
var invRows = 0;
var rowNums = [];
var invProduct;
var prodColors;
var amt_paid = 0;
var specPricing = {};
var exempt_status = false;
var invNumUse = false;
var weightOver = false;
var customer_mode = false;
var credit_line = 0;
var invTotal = 0;

//Get Product List...
console.log('Getting Product List...');
if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      console.log('R: '+this.responseText);
			var r = JSON.parse(this.responseText);
      console.log('R: '+r);
      invProduct = r.products;
      console.log('Product List Loaded.');
      
    }
  }
  xmlhttp.open("GET","../php/get-all-products.php",true);
  xmlhttp.send();


//Get Product Color List...
console.log('Getting Product Color List...');
if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp2=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp2.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var r = JSON.parse(this.responseText);
      console.log(r);
      prodColors = r;
      console.log('Product Color List Loaded.');
      console.log(r);
      
    }
  }
  xmlhttp2.open("GET","../php/get-colors.php",true);
  xmlhttp2.send();


	function encodeURL(url){
		url = url.replace(/&/g, '%26'); 
		url = url.replace(/#/g, '%23');
		return url;
	}