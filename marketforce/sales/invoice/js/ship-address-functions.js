function get_addresses(cid,sa){
	var md = document.getElementById('ship_address_select').innerHTML = '';
	if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
		console.log(this.responseText);
		var r = JSON.parse(this.responseText);
		if(r.response === 'GOOD'){
			var md = document.getElementById('ship_address_select');
			var h3 = document.createElement('h3');
			h3.innerHTML = 'Ship To:';
			md.appendChild(h3);
			var s = document.createElement('select');
			s.id = 'shipping_address';
			for(var i = 0; i < r.addresses.length; i++){
				var a = r.addresses[i];
				var o = document.createElement('option');
				o.value = a.address_id;
				if(sa === a.address_id){
					o.setAttribute('selected','selected');
				}
				o.innerHTML = a.address+' '+a.city+', '+a.state+' '+a.zip;
				s.appendChild(o);
			}
			md.appendChild(s);
			console.log('Shipping Address Options Loaded...');
		}else{
			console.warn('An error occurred getting the information you requested');
			var md = document.getElementById('ship_address_select');
			var h3 = document.createElement('h3');
			h3.innerHTML = 'Ship To:';
			md.appendChild(h3);
			var s = document.createElement('select');
			s.id = 'shipping_address';
			var o = document.createElement('option');
			o.value = '';
			o.innerHTML = 'No Shipping Address Found';
			s.appendChild(o);
			md.appendChild(s);
			console.log('Shipping Address Options Loaded...');
		}
      
    }
  }
  xmlhttp.open("GET","../php/get-ship-addresses.php?cid="+cid,true);
  xmlhttp.send();
}