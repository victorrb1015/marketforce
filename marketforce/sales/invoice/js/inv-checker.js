var invelement = document.getElementById('invNum');
invelement.addEventListener('DOMSubtreeModified', function(){
  var nin = invelement.innerHTML;
  //Reset Tab Title...
  document.title = nin;
  
  //Check if Invoice Number Already Used...
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var r = JSON.parse(this.responseText);
      if(r.status === 'GOOD'){
        console.log('GOOD: '+nin);
        invNumUse = false;
        var wiw = document.getElementById('wi-warning');
        if(wiw !== null){
          wiw.remove();
        }
      }else{
        console.log('BAD: '+nin);
        //BAD response...
        var wi = document.createElement('i');
        wi.setAttribute('class','fa fa-exclamation-triangle');
        wi.setAttribute('style','color:red;');
        wi.setAttribute('id','wi-warning');
        //wi.setAttribute('data-toggle','tooltip');
        //wi.setAttribute('data-placement','top');
        //wi.setAttribute('title','Invoice Number Already In Use!');
        document.getElementById('invNum-title').appendChild(wi);
        invNumUse = true;
      }
      
      
    }
  }
  xmlhttp.open("GET","../php/check-invoice.php?invNum="+nin,true);
  xmlhttp.send();
});



function check_inv(nin){
  var nin = invelement.innerHTML;
  //Reset Tab Title...
  document.title = nin;
  
  //Check if Invoice Number Already Used...
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      var r = JSON.parse(this.responseText);
      if(r.status === 'GOOD'){
        console.log('GOOD: '+nin);
        invNumUse = false;
        var wiw = document.getElementById('wi-warning');
        if(wiw !== null){
          wiw.remove();
        }
      }else{
        console.log('BAD: '+nin);
        //BAD response...
        var wi = document.createElement('i');
        wi.setAttribute('class','fa fa-exclamation-triangle');
        wi.setAttribute('style','color:red;');
        wi.setAttribute('id','wi-warning');
        //wi.setAttribute('data-toggle','tooltip');
        //wi.setAttribute('data-placement','top');
        //wi.setAttribute('title','Invoice Number Already In Use!');
        document.getElementById('invNum-title').appendChild(wi);
        invNumUse = true;
      }
      
      
    }
  }
  xmlhttp.open("GET","../php/check-invoice.php?invNum="+nin,true);
  xmlhttp.send();
}