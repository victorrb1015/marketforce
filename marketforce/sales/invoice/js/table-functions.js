//Initiate when document loads...
$(document).ready(function(){
  
  //When the "Add Row" button is clicked...
  $('.add').click(function(){
    
    //Update Invoice Row Count...
    invRows++;
    rowNums.push(invRows);
    
    //Create New Row...
    var row = document.createElement('tr');
    row.setAttribute('id', 'row_'+invRows);
    
    //Create New Cell For Row Number...
    var cell = document.createElement('td');
    row.appendChild(cell);

    //Create New Cell...
    var cell = document.createElement('td');
    
    //Cut Button...
    var cut = document.createElement('a');
    cut.setAttribute('class','cut');
    cut.setAttribute('data-rownum',invRows);
    cut.innerHTML = '-';
    cell.appendChild(cut);
    
    //Item...
    var item = document.createElement('select');
    item.setAttribute('style','width:100%;');
    item.setAttribute('id','item_'+invRows);
    item.setAttribute('name','item_'+invRows);
    item.setAttribute('data-rowid',invRows);
    item.setAttribute('onchange','change_product(this);');
    var option = document.createElement('option');
    option.text = 'Select A Product';
    option.value = '';
    item.add(option);
    for(var i = 0; i < invProduct.length; i++){
      var option = document.createElement('option');
      option.text = invProduct[i].pname;
      option.value = invProduct[i].ID;
      item.add(option);
    }
    var option = document.createElement('option');
    option.text = 'Custom Item';
    option.value = 'custom';
    item.add(option);
    cell.appendChild(item);
    row.appendChild(cell);
    
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Description...
    var ta = document.createElement('textarea');
    ta.setAttribute('style','width:100%;');
    ta.setAttribute('id','desc_'+invRows);
    ta.setAttribute('name','desc_'+invRows);
    ta.setAttribute('data-rowid',invRows);
    ta.setAttribute('readonly','readonly');
    cell.appendChild(ta);
    row.appendChild(cell);
    
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Color...
    var color = document.createElement('select');
    color.setAttribute('style','width:100%;');
    color.setAttribute('id','color_'+invRows);
    color.setAttribute('name','color_'+invRows);
    color.setAttribute('data-rowid',invRows);
    //color.setAttribute('onchange','change_product(this);');
    color.setAttribute('disabled','true');
    var option = document.createElement('option');
    option.text = 'N/A';
    option.value = '';
    color.add(option);
    for(var i = 0; i < prodColors.length; i++){
      var option = document.createElement('option');
      option.text = prodColors[i].color_name;
      option.value = prodColors[i].color_name;
      color.add(option);
    }
    cell.appendChild(color);
    row.appendChild(cell);
    
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Quantity...
    var qty = document.createElement('input');
    qty.setAttribute('type','text');
    //qty.setAttribute('class','number');//Enables JS Mask of the number...
    qty.setAttribute('id','qty_'+invRows);
    qty.setAttribute('name','qty_'+invRows);
    qty.setAttribute('data-rowid',invRows);
    qty.setAttribute('readonly','readonly');
    qty.setAttribute('style','width:100%;');
    qty.setAttribute('onchange','change_qty(this);');
    cell.appendChild(qty);
    row.appendChild(cell);
    
    
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Rate...
    var rate = document.createElement('input');
    rate.setAttribute('type','text');
    //rate.setAttribute('class','usd');
    rate.setAttribute('id','rate_'+invRows);
    rate.setAttribute('name','rate_'+invRows);
    rate.setAttribute('data-rowid',invRows);
    rate.setAttribute('style','width:85%;');
    rate.setAttribute('onchange','change_qty(this);');
    rate.setAttribute('readonly','readonly');
    var t = document.createElement('span');
    t.innerHTML = '$';
    cell.appendChild(t);
    cell.appendChild(rate);
    row.appendChild(cell);
    
    
    
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Length...
    var len = document.createElement('input');
    len.setAttribute('type','text');
    //len.setAttribute('class','number');
    len.setAttribute('id','length_'+invRows);
    len.setAttribute('name','length_'+invRows);
    len.setAttribute('data-rowid',invRows);
    len.setAttribute('readonly','readonly');
    len.setAttribute('style','width:100%;');
    len.setAttribute('onchange','change_qty(this);');
    cell.appendChild(len);
    row.appendChild(cell);
    
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Price...
    var price = document.createElement('input');
    price.setAttribute('type','text');
    price.setAttribute('class','usd');
    price.setAttribute('id','price_'+invRows);
    price.setAttribute('name','price_'+invRows);
    price.setAttribute('data-rowid',invRows);
    price.setAttribute('style','width:85%;');
    price.setAttribute('readonly','readonly');
    var t = document.createElement('span');
    t.innerHTML = '$';
    cell.appendChild(t);
    cell.appendChild(price);
    row.appendChild(cell);
    
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Weight...
    var weight = document.createElement('input');
    weight.setAttribute('type','text');
    weight.setAttribute('id','weight_'+invRows);
    weight.setAttribute('name','weight_'+invRows);
    weight.setAttribute('data-rowid',invRows);
    weight.setAttribute('style','width:85%;');
    weight.setAttribute('readonly','readonly');
    var t = document.createElement('span');
    t.innerHTML = 'lbs';
    cell.appendChild(weight);
    cell.appendChild(t);
    var iweight = document.createElement('input');
    iweight.setAttribute('type','hidden');
    iweight.setAttribute('id','iweight_'+invRows);
    iweight.setAttribute('name','iweight_'+invRows);
    iweight.setAttribute('data-rowid',invRows);
    cell.appendChild(iweight);
    row.appendChild(cell);
    
    //Append New Row to Table...
    var inv_table = document.getElementById('inv_table');
    inv_table.appendChild(row);
    
      //$('.date').mask('00/00/0000');
      $('.number').mask('000,000');
      //$('.time').mask('00:00:00');
      //$('.phone').mask('(000) 000-0000');
      //$('.phone_us').mask('(000) 000-0000');
      $('.usd').mask('000,000,000,000,000.00', {reverse: true});
      //$( ".date" ).datepicker();
 
    recall();

    number_rows();
    
  });//End Add Button Clicked...
  
});//End Document Load...



function recall(){
  //When "Cut" button is clicked...
  $('.cut').click(function(){
    $(this).parentsUntil("tbody").remove();
    var rn = $(this).data("rownum");
    console.log(rowNums);
    var index = rowNums.indexOf(rn);
    if (index > -1) {
      rowNums.splice(index, 1);
    }
    console.log(rowNums);
    verify_pricing();
    calc_total();
    number_rows();
  });
}