function calc_total(){
  invTotal = 0;
  var subTotal = 0;
  var wTotal = 0;
  for(var i = 0; i < rowNums.length; i++){
    //Get Total...
    var p = document.getElementById('price_'+rowNums[i]).value;
    invTotal = invTotal + Number(p);
    //Verify item total weight...
    var wqty = document.getElementById('qty_'+rowNums[i]).value;
    var iw = document.getElementById('iweight_'+rowNums[i]).value;
    var wl = document.getElementById('length_'+rowNums[i]).value;
    if(wl == 'N/A'){
      wl = 1;
    }
    var cw = (iw * wqty * wl);
    var w = document.getElementById('weight_'+rowNums[i]);
    if(cw !== w.value){
      w.value = cw;
    }
    wTotal = wTotal + Number(cw);
  }
  var freight = document.getElementById('freight').value;
  freight = freight.replace(',','');
  //console.log('Freight: '+fr);
  //subTotal = invTotal + Number(freight);
  subTotal = invTotal;
  //console.log(subTotal);
  document.getElementById('subtotal').innerHTML = subTotal.toFixed(2);
  var taxPercent = document.getElementById('taxPercent').value / 100;
  var tax = subTotal * taxPercent;
  document.getElementById('tax').innerHTML = tax.toFixed(2);
  var total = subTotal + tax + Number(freight);
  document.getElementById('total').innerHTML = total.toFixed(2);
  var balance = total - amt_paid;
  document.getElementById('bal_due').innerHTML = balance.toFixed(2);
  document.getElementById('amt_due').innerHTML = balance.toFixed(2);
  //Set Total Weight...
  document.getElementById('total_weight').value = wTotal.toFixed(2);
  //Check if weight is over limit...
  if(wTotal <= 45500){
    weightOver = false;
    var wiw = document.getElementById('weight_warning').innerHTML = '';
    /*if(wiw !== null){
      wiw.remove();
    }*/
  }else{
    if(weightOver !== true){
    var wi = document.createElement('i');
    wi.setAttribute('class','fa fa-exclamation-triangle');
    wi.setAttribute('style','color:red;');
    wi.setAttribute('id','wt-wi-warning');
    //wi.setAttribute('data-toggle','tooltip');
    //wi.setAttribute('data-placement','top');
    //wi.setAttribute('title','Invoice Number Already In Use!');
    document.getElementById('weight_warning').appendChild(wi);
    var t = document.createElement('text');
    t.innerHTML = 'Exceeded!';
    document.getElementById('weight_warning').appendChild(t);
    weightOver = true;
    }
  }
  invTotal = total;
}



function verify_pricing(){
  /*for(var q = 0; q < rowNums.length; q++){
    var rnum = rowNums[q];
    var pid = document.getElementById('item_'+rowNums[q]).value;
    var cprice = document.getElementById('rate_'+rowNums[q]).value;
    var qty = document.getElementById('qty_'+rowNums[q]).value;
    var len = document.getElementById('length_'+rowNums[q]).value;
    if(len === 'N/A'){
      len = 1;
    }
    //Search for product ID...
  var searchField = "ID";
  var searchVal = pid;
    console.log(pid);
  for (var i=0 ; i < invProduct.length ; i++){
    console.log(i);
      if (invProduct[i][searchField] == searchVal) {
          //results.push(invProduct[i]);
        document.getElementById('desc_'+rnum).value = '[Per '+invProduct[i].punits+'] ' + invProduct[i].pinfo;
        if(typeof specPricing[searchVal] !== 'undefined'){
          document.getElementById('rate_'+rnum).value = specPricing[searchVal];
          var nprice = specPricing[searchVal] * qty * len;
          document.getElementById('price_'+rnum).value = nprice.toFixed(2);
        }else{
          document.getElementById('rate_'+rnum).value = invProduct[i].pprice;
          var nprice = invProduct[i].pprice * qty * len;
          document.getElementById('price_'+rnum).value = nprice.toFixed(2);
        }
        //document.getElementById('qty_'+rnum).value = 1;
        calc_total();
        //return;
      }else{
        
      }
    }
  }*/
}


function number_rows(){
  var myRows = document.getElementById('invTable').getElementsByTagName('tbody')[0].getElementsByTagName('tr');
  var row_count = myRows.length;
  for(var i = 0; i < row_count; i++){
    myRows[i].cells[0].innerHTML = (i + 1);
  }
  console.log('Rows Numbered');
}



