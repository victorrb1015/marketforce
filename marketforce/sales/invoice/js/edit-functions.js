function edit_invoice(invn){
  console.log('Loading Invoice#: '+invn);
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var inv = JSON.parse(this.responseText);
      var ii = inv.items;
      console.log('Invoice#: '+inv+' Loaded.');
      var istatus;
      var bgColor;
      var fontColor;
      //Load Main Invoice Info...
      if(inv.status === 'Estimate'){
        istatus = 'Estimate';
        bgColor = 'Pink';
        fontColor = '#000'
      }else{
        istatus = 'Invoice';
        bgColor = 'black';
        fontColor = '#FFF';
      }
      document.title = istatus + ' ' + inv.invNum;
      document.getElementById('docTitle').innerHTML = istatus;
      document.getElementById('docTitle').style.background = bgColor;
      document.getElementById('docTitle').style.color = fontColor;
      document.getElementById('invNum-title').innerHTML = istatus + ' #';
      document.getElementById('customer').value = inv.cid;
      get_addresses(inv.cid,inv.sa);
      document.getElementById('invDate').innerHTML = inv.invDate;
      document.getElementById('cemail').innerHTML = inv.cemail;
      document.getElementById('cphone').innerHTML = inv.cphone;
      document.getElementById('freight').value = inv.freight;
      document.getElementById('taxPercent').value = inv.taxRate;
      document.getElementById('notes').value = inv.notes;

      if(inv.taxExempt === 'Yes'){
        exempt_status = true;
        var a = document.createElement('a');
        a.setAttribute('href',inv.taxExemptForm);
        a.setAttribute('target','_blank');
        var t = document.createElement('b');
        t.innerHTML = 'EXEMPT -- ';
        a.appendChild(t);
        document.getElementById('exempt').appendChild(a);
        document.getElementById('taxPercent').value = 0;
        document.getElementById('taxPercent').setAttribute('readonly','readonly');
      }else{
        exempt_status = false;
        document.getElementById('taxPercent').removeAttribute('readonly');
        document.getElementById('exempt').innerHTML = '';
      }
      
      //Load Invoice Items...
      //var ii = inv.items;
      for(var ic = 0; ic < ii.length; ic++){
        console.log(ii[ic]);
        console.log('DESC: '+ii[ic].item_desc);
        console.log('Length: '+ii[ic].item_length);
    //Update Invoice Row Count...
    invRows++;
    rowNums.push(invRows);
    
    //Create New Row...
    var row = document.createElement('tr');
    row.setAttribute('id', 'row_'+invRows);
    
    //Create New Cell For Row Number...
    var cell = document.createElement('td');
    row.appendChild(cell);

    //Create New Cell...
    var cell = document.createElement('td');
    
    //Cut Button...
    var cut = document.createElement('a');
    cut.setAttribute('class','cut');
    cut.setAttribute('data-rownum',invRows);
    cut.innerHTML = '-';
    cell.appendChild(cut);
    
    //Item...
    var item = document.createElement('select');
    item.setAttribute('id','item_'+invRows);
    item.setAttribute('name','item_'+invRows);
    item.setAttribute('data-rowid',invRows);
    item.setAttribute('style','width:100%;');
    item.setAttribute('onchange','change_product(this);');
    var option = document.createElement('option');
    option.text = 'Select A Product';
    option.value = '';
    item.add(option);
    for(var i = 0; i < invProduct.length; i++){
      var option = document.createElement('option');
      option.text = invProduct[i].pname;
      option.value = invProduct[i].ID;
      if(ii[ic].pid == invProduct[i].ID){
        option.setAttribute('selected','selected');
      }
      item.add(option);
    }
    var option = document.createElement('option');
    option.text = 'Custom Item';
    option.value = 'custom';
    if(ii[ic].pid == 'custom'){
      option.setAttribute('selected','selected');
    }
    item.add(option);
    cell.appendChild(item);
    row.appendChild(cell);
    
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Description...
    var ta = document.createElement('textarea');
    ta.setAttribute('id','desc_'+invRows);
    ta.setAttribute('name','desc_'+invRows);
    ta.setAttribute('data-rowid',invRows);
    ta.setAttribute('style','width:100%;');
    if(ii[ic].pid !== 'custom'){
      ta.setAttribute('readonly','readonly');
    }
    ta.value = ii[ic].item_desc;
    cell.appendChild(ta);
    row.appendChild(cell);
    
        
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Color...
    var color = document.createElement('select');
    color.setAttribute('style','width:100%;');
    color.setAttribute('id','color_'+invRows);
    color.setAttribute('name','color_'+invRows);
    color.setAttribute('data-rowid',invRows);
    //color.setAttribute('onchange','change_product(this);');
    var option = document.createElement('option');
    option.text = 'N/A';
    option.value = '';
    color.add(option);
    var option = document.createElement('option');
      option.text = ii[ic].item_color;
      option.value = ii[ic].item_color;
      option.setAttribute('selected','selected');
      color.add(option);
    if(ii[ic].item_color === ''){
      color.setAttribute('disabled','true');
    }
    cell.appendChild(color);
    row.appendChild(cell);
        
        
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Quantity...
    var qty = document.createElement('input');
    qty.setAttribute('type','text');
    //qty.setAttribute('class','number');//Enables JS Mask of the number...
    qty.setAttribute('id','qty_'+invRows);
    qty.setAttribute('name','qty_'+invRows);
    qty.setAttribute('data-rowid',invRows);
    qty.setAttribute('style','width:100%;');
    qty.setAttribute('onchange','change_qty(this);');
    qty.value = ii[ic].item_qty;
    cell.appendChild(qty);
    row.appendChild(cell);
        
        
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Rate...
    var rate = document.createElement('input');
    rate.setAttribute('type','text');
    //rate.setAttribute('class','usd');
    rate.setAttribute('id','rate_'+invRows);
    rate.setAttribute('name','rate_'+invRows);
    rate.setAttribute('data-rowid',invRows);
    rate.setAttribute('style','width:85%;');
    rate.setAttribute('onchange','change_qty(this);');
    if(ii[ic].pid !== 'custom'){
      rate.setAttribute('readonly','readonly');
    }
    rate.value = ii[ic].item_rate;
    var t = document.createElement('span');
    t.innerHTML = '$';
    cell.appendChild(t);
    cell.appendChild(rate);
    row.appendChild(cell);
    
    
        
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Length...
    var len = document.createElement('input');
    len.setAttribute('type','text');
    //len.setAttribute('class','number');
    len.setAttribute('id','length_'+invRows);
    len.setAttribute('name','length_'+invRows);
    len.setAttribute('data-rowid',invRows);
    len.setAttribute('style','width:100%;');
    len.setAttribute('onchange','change_qty(this);');
    len.value = ii[ic].item_length;
        if(ii[ic].item_length === 'N/A'){
          len.setAttribute('readonly','readonly');
        }
    cell.appendChild(len);
    row.appendChild(cell);
    
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Price...
    var price = document.createElement('input');
    price.setAttribute('type','text');
    //price.setAttribute('class','usd');
    price.setAttribute('id','price_'+invRows);
    price.setAttribute('name','price_'+invRows);
    price.setAttribute('data-rowid',invRows);
    price.setAttribute('style','width:85%;');
    price.setAttribute('readonly','readonly');
    price.value = ii[ic].item_price;
    var t = document.createElement('span');
    t.innerHTML = '$';
    cell.appendChild(t);
    cell.appendChild(price);
    row.appendChild(cell);
        
    //Create New Cell...
    var cell = document.createElement('td');
    
    //Weight...
    var weight = document.createElement('input');
    weight.setAttribute('type','text');
    weight.setAttribute('id','weight_'+invRows);
    weight.setAttribute('name','weight_'+invRows);
    weight.setAttribute('data-rowid',invRows);
    weight.setAttribute('style','width:85%;');
    weight.setAttribute('readonly','readonly');
    weight.value = ii[ic].item_weight;
    var t = document.createElement('span');
    t.innerHTML = 'lbs';
    cell.appendChild(weight);
    cell.appendChild(t);
    var iweight = document.createElement('input');
    iweight.setAttribute('type','hidden');
    iweight.setAttribute('id','iweight_'+invRows);
    iweight.setAttribute('name','iweight_'+invRows);
    iweight.setAttribute('data-rowid',invRows);
    iweight.value = ii[ic].item_weight;
    cell.appendChild(iweight);
    row.appendChild(cell);
    
    //Append New Row to Table...
    var inv_table = document.getElementById('inv_table');
    inv_table.appendChild(row);
        
    
    
      //$('.date').mask('00/00/0000');
      $('.number').mask('000,000');
      //$('.time').mask('00:00:00');
      //$('.phone').mask('(000) 000-0000');
      //$('.phone_us').mask('(000) 000-0000');
      $('.usd').mask('000,000,000,000,000.00', {reverse: true});
      //$( ".date" ).datepicker();
 
    recall();
    calc_total();
    number_rows();
      }
    set_all_color_lists();
    get_special_pricing();
    }
  }
  xmlhttp.open("GET","../php/get-invoice-details.php?inv="+invn,true);
  xmlhttp.send();
  
  
  
}