function pu_load_modal(mode,uid){
  //Clear Form
  document.getElementById('pu_fname').value = '';
  document.getElementById('pu_lname').value = '';
  document.getElementById('pu_email').value = '';
  document.getElementById('pu_company').value = '';
  if(mode === 'New'){
    document.getElementById('pu_mode').value = 'New';
    document.getElementById('pu_uid').value = '';
  }else{
    document.getElementById('pu_mode').value = 'Edit';
    document.getElementById('pu_uid').value = uid;
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      console.log(this.responseText);
			var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById('pu_fname').value = r.fname;
        document.getElementById('pu_lname').value = r.lname;
        document.getElementById('pu_email').value = r.email;
        document.getElementById('pu_company').value = r.company;
        console.log('User info loaded successfully...');
      }else{
        console.log('There was an error fetching the user information');
      }
      
    }
  }
  xmlhttp.open("GET","orders/php/fetch-portal-user.php?uid="+uid,true);
  xmlhttp.send();
  }
}

function save_portal_user(){
  document.getElementById('save_portal_user_btn').disabled = true;
  var fname = document.getElementById('pu_fname').value;
  var lname = document.getElementById('pu_lname').value;
  var email = document.getElementById('pu_email').value;
  var company = document.getElementById('pu_company').value;
  var uid = document.getElementById('pu_uid').value;
  var mode = document.getElementById('pu_mode').value;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      document.getElementById('save_portal_user_btn').disabled = false;
			console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        window.location = 'orders.php?tab=portal_users';
      }else{
        alert('Error: '+r.message);
      }
      
    }
  }
  xmlhttp.open("GET","orders/php/add-edit-portal-user.php?mode="+mode+"&uid="+uid+"&fname="+fname+"&lname="+lname+"&email="+email+"&company="+company,true);
  xmlhttp.send();
}


function remove_portal_user(uid){
  var conf = confirm("Are you sure you want to make this user inactive?");
  if(conf === '' || conf === null || conf === false){
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        window.location = 'orders.php?tab=portal_users';
      }else{
        alert('Error: '+r.message);
      }
      
    }
  }
  xmlhttp.open("GET","orders/php/remove-portal-user.php?uid="+uid,true);
  xmlhttp.send();
}
