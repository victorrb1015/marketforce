function new_customer(){
  document.getElementById('customer_mode').value = 'New';
  document.getElementById('company_name').value = '';
  document.getElementById('customer_first_name').value = '';
  document.getElementById('customer_last_name').value = '';
  document.getElementById('customer_email').value = '';
  document.getElementById('customer_phone').value = '';
  document.getElementById('customer_notes').value = '';
  document.getElementById('customer_address').value = '';
  document.getElementById('customer_city').value = '';
  document.getElementById('customer_state').value = '';
  document.getElementById('customer_zip').value = '';
  document.getElementById('authUsers').value = '';
  document.getElementById('taxForm_div').innerHTML = '<input type="file" class="form-control" id="taxForm" name="taxForm" />';
}


function save_customer(){
  var fname = document.getElementById('customer_first_name').value;
  if(fname === ''){
    alert('Please Enter The Customers First Name!');
    return;
  }
  fname = urlEncode(fname);
  var lname = document.getElementById('customer_last_name').value;
  if(lname === ''){
    alert('Please Enter The Customers Last Name!');
    return;
  }
  lname = urlEncode(lname);
  var email = document.getElementById('customer_email').value;
  if(email === ''){
    alert('Please Enter The Customers Email!');
    return;
  }
  email = urlEncode(email);
  var phone = document.getElementById('customer_phone').value;
  if(phone === ''){
    alert('Please Enter The Customers Phone Number!');
    return;
  }
  phone = urlEncode(phone);
  var address = document.getElementById('customer_address').value;
  address = urlEncode(address);
  var city = document.getElementById('customer_city').value;
  city = urlEncode(city);
  var state = document.getElementById('customer_state').value;
  var zip = document.getElementById('customer_zip').value;
  zip = urlEncode(zip);
  
  var notes = document.getElementById('customer_notes').value;
  notes = urlEncode(notes);
  
  var mode = document.getElementById('customer_mode').value;
  var cid = document.getElementById('customer_id').value;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'orders.php?tab=customers';
      
    }
  }
  xmlhttp.open("GET","sales/php/add-edit-customer.php?fname="+fname+"&lname="+lname+"&email="+email+"&phone="+phone+"&address="+address+"&city="+city+"&state="+state+"&zip="+zip+"&notes="+notes+"&mode="+mode+"&id="+cid,true);
  xmlhttp.send();
}


function edit_customer(id){
  document.getElementById('customer_mode').value = 'Edit';
  document.getElementById('customer_id').value = id;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var q = JSON.parse(this.responseText);
      //document.getElementById('company_name').value = q.company;
      document.getElementById('customer_first_name').value = q.fname;
      document.getElementById('customer_last_name').value = q.lname;
      document.getElementById('customer_email').value = q.email;
      document.getElementById('customer_phone').value = q.phone;
      document.getElementById('customer_phone_2').value = q.phone2;
      document.getElementById('customer_address').value = q.address;
      document.getElementById('customer_city').value = q.city;
      document.getElementById('customer_state').value = q.state;
      document.getElementById('customer_zip').value = q.zip;
      //document.getElementById('authUsers').value = q.authUsers;
      if(q.taxFormURL !== ''){
        document.getElementById('taxForm_div').innerHTML = '<a href="'+q.taxFormURL+'" target="_blank" style="color:blue;weight:bold;"><i class="fa fa-eye"></i> View Form</a>'+
                                                            '&nbsp;'+
                                                            '<small><a href="Javascript: remove_taxForm('+id+');" style="color:red;">(<i class="fa fa-times"></i> Remove)</a></small>';
      }else{
        document.getElementById('taxForm_div').innerHTML = '<input type="file" class="form-control" id="taxForm" name="taxForm" />';
      }
      document.getElementById('customer_notes').value = q.notes;
      
    }
  }
  xmlhttp.open("GET","sales/php/get-customer-details.php?cid="+id,true);
  xmlhttp.send();
}


function remove_customer(cid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'sales.php?tab=customers';
      
    }
  }
  xmlhttp.open("GET","sales/php/delete-customer.php?cid="+cid,true);
  xmlhttp.send();
}


function remove_taxForm(cid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'orders.php?tab=customers';
      
    }
  }
  xmlhttp.open("GET","sales/php/remove-customer-taxform.php?cid="+cid,true);
  xmlhttp.send();
}
