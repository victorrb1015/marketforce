function send_customer_invoice(invNum){
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      var r = JSON.parse(this.responseText);
      if(r.status === 'GOOD'){
			  alert(r.response);
        window.location = 'orders.php?tab=orders';
        //change_status(r.ID,'invoiced','yes');
      }else{
        alert(r.error);
      }
    }
  }
  xmlhttp.open("GET","orders/php/send-customer-invoice.php?inv="+invNum+"&rep_id"+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
}


function send_customer_estimate(invNum){
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      var r = JSON.parse(this.responseText);
      if(r.status === 'GOOD'){
			  alert(r.response);
        window.location = 'orders.php?tab=orders';
        //change_status(r.ID,'invoiced','yes');
      }else{
        alert(r.error);
      }
    }
  }
  xmlhttp.open("GET","orders/php/send-customer-estimate.php?inv="+invNum+"&rep_id"+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
}



function export_inv(){
  var conf = confirm('Are you sure you want to export all pending invoices? Click "OK" to continue. Click "Cancel" to cancel the export.');
  if(conf === false){
    return;
  }else{
    window.open("orders/export/qb-export.php?rep_id="+rep_id+"&rep_name="+rep_name, "_blank");
  }
}