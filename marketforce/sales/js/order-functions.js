function get_order_details(oid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var q = JSON.parse(this.responseText);
      //alert(q.cname);
      
      document.getElementById('cname').value = q.cname;
      document.getElementById('invoice').value = q.inv;
      document.getElementById('due_date').value = q.due_date;
      document.getElementById('notes').value = q.notes;
      document.getElementById('priority').value = q.priority;
      if(q.file_url !== ''){
        document.getElementById('fileDiv').innerHTML = '<a href="'+q.file_url+'" target="_blank">'+
                                                       '<button type="button" class="btn btn-primary btn-xs">'+
                                                       '<i class="fa fa-eye"></i>'+
                                                       ' View Work Order'+
                                                       '</button>'+
                                                       '</a> &nbsp; <button type="button" class="btn btn-danger btn-xs" onclick="remove_file('+q.ID+',1);"><i class="fa fa-trash"></i> Remove</button>';
      }
      if(q.inv_file_url !== ''){
        document.getElementById('inv_fileDiv').innerHTML = '<a href="'+q.inv_file_url+'" target="_blank">'+
                                                       '<button type="button" class="btn btn-primary btn-xs">'+
                                                       '<i class="fa fa-eye"></i>'+
                                                       ' View Invoice'+
                                                       '</button>'+
                                                       '</a> &nbsp; <button type="button" class="btn btn-danger btn-xs" onclick="remove_file('+q.ID+',2);"><i class="fa fa-trash"></i> Remove</button>';
      }
      if(q.bol_file_url !== ''){
        document.getElementById('bol_fileDiv').innerHTML = '<a href="'+q.bol_file_url+'" target="_blank">'+
                                                       '<button type="button" class="btn btn-primary btn-xs">'+
                                                       '<i class="fa fa-eye"></i>'+
                                                       ' View Bill of Lading'+
                                                       '</button>'+
                                                       '</a> &nbsp; <button type="button" class="btn btn-danger btn-xs" onclick="remove_file('+q.ID+',3);"><i class="fa fa-trash"></i> Remove</button>';
      }
      
      document.getElementById('mode').value = 'Update';
      document.getElementById('oid').value = q.ID;
      document.getElementById('newSubBTN').innerHTML = 'Save';
    }
  }
  xmlhttp.open("GET","orders/php/get-order-details.php?oid="+oid,true);
  xmlhttp.send();
}


function new_order(){
  document.getElementById('cname').value = '';
  document.getElementById('invoice').value = '';
  document.getElementById('due_date').value = '';
  document.getElementById('notes').value = '';
  document.getElementById('priority').value = '';
  document.getElementById('fileDiv').innerHTML = '<h4>Work Order Attachment</h4>'+
                                                 '<input type="file" id="fileBox" name="fileBox" class="form-control"/>'+
                                                 '<small>(Supports: .JPG, .PNG, .PDF)</small>';
  document.getElementById('inv_fileDiv').innerHTML = '<h4>Invoice Attachment</h4>'+
                                                 '<input type="file" id="invfileBox" name="invfileBox" class="form-control"/>'+
                                                 '<small>(Supports: .JPG, .PNG, .PDF)</small>';
  document.getElementById('bol_fileDiv').innerHTML = '<h4>Bill of Lading Attachment</h4>'+
                                                 '<input type="file" id="bolfileBox" name="bolfileBox" class="form-control"/>'+
                                                 '<small>(Supports: .JPG, .PNG, .PDF)</small>';
  document.getElementById('mode').value = 'New';
  document.getElementById('oid').value = '';
  document.getElementById('newSubBTN').innerHTML = 'Add';
}


function change_status(oid,mode,bypass,dir){
  var cont = confirm('Are you sure you want to change to '+mode+'?');
    if(!cont){
      return;
    }
  var shopNum;
  if(mode === 'in_shop'){
    shopNum = prompt('Please Enter The Shop Number For Verification:');
    shopNum = urlEncode(shopNum);
  }
  var trackingNum;
  if(mode === 'shipped'){
    trackingNum = prompt('Please enter the Shipping Manifest# or Tracking#');
    trackingNum = urlEncode(trackingNum);
  }
  var invoiceNum;
  /*if(mode === 'invoiced' && bypass !== 'yes'){
    invoiceNum = prompt('Please enter the Quickbooks Invoice Number associated with this order!');
    invoiceNum = urlEncode(invoiceNum);
  }*/
  var payDetail;
  if(mode === 'paid'){
    payDetail = prompt('Please Enter the Check#, CC Confirmation#, or "Cash" if this order was paid in cash');
    payDetail = urlEncode(payDetail);
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'orders.php?tab=orders';
      
    }
  }
  xmlhttp.open("GET","https://marketforceapp.com/marketforce/orders/php/change-order-status.php?dir="+dir+"&bypass="+bypass+"&oid="+oid+"&mode="+mode+"&rep_id="+rep_id+"&rep_name="+rep_name+"&shopNum="+shopNum+"&trackingNum="+trackingNum+"&invoiceNum="+invoiceNum+"&payDetail="+payDetail,true);
  xmlhttp.send();
}


function delete_order(oid){
  var cont = confirm('Are you sure you want to remove this order?');
    if(!cont){
      return;
    }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'orders.php?tab=orders';
      
    }
  }
  xmlhttp.open("GET","orders/php/delete-order.php?oid="+oid,true);
  xmlhttp.send();
}


function add_note(id){
  document.getElementById('noteID').value = id;
}

function submit_note(){
  var oid = document.getElementById('noteID').value;
  var note = document.getElementById('add_note').value;
  note = urlEncode(note);
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'orders.php?tab=orders';
      
    }
  }
  xmlhttp.open("GET","orders/php/add-note.php?oid="+oid+"&note="+note+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
}


function remove_file(oid,file){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'orders.php?tab=orders';
      
    }
  }
  xmlhttp.open("GET","orders/php/remove-file.php?oid="+oid+"&file="+file+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
}


	function urlEncode(url){
		url = url.replace(/&/g, '%26'); 
		url = url.replace(/#/g, '%23');
		return url;
	}



function view_order_info(oid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var q = JSON.parse(this.responseText);
      document.getElementById('infoBoxBody').innerHTML = '<div>'+
                                                          '<h2>Recieved Order:</h2>'+
                                                          '<p>Status: <b>'+q.rec_order+'</b></p>'+
                                                          '<p>Date: <b>'+q.rec_order_date+'</b></p>'+
                                                          '<p>Time: <b>'+q.rec_order_time+'</b></p>'+
                                                          '<p>Rep: <b>'+q.rec_order_rep+'</b></p>';
      if(q.in_shop === 'Yes'){
        document.getElementById('infoBoxBody').innerHTML += '<h2>In Shop:</h2>'+
                                                          '<p>Status: <b>'+q.in_shop+'</b></p>'+
                                                          '<p>Date: <b>'+q.in_shop_date+'</b></p>'+
                                                          '<p>Time: <b>'+q.in_shop_time+'</b></p>'+
                                                          '<p>Rep: <b>'+q.in_shop_rep+'</b></p>'+
                                                          '<p>Shop Number: <b>'+q.shopNum+'</br></p>';
      }
      if(q.shop_done === 'Yes'){
        document.getElementById('infoBoxBody').innerHTML += '<h2>Shop Done:</h2>'+
                                                          '<p>Status: <b>'+q.shop_done+'</b></p>'+
                                                          '<p>Date: <b>'+q.shop_done_date+'</b></p>'+
                                                          '<p>Time: <b>'+q.shop_done_time+'</b></p>'+
                                                          '<p>Rep: <b>'+q.shop_done_rep+'</b></p>';
      }
      if(q.shipped === 'Yes'){
         document.getElementById('infoBoxBody').innerHTML += '<h2>Shipped:</h2>'+
                                                          '<p>Status: <b>'+q.shipped+'</b></p>'+
                                                          '<p>Date: <b>'+q.shipped_date+'</b></p>'+
                                                          '<p>Time: <b>'+q.shipped_time+'</b></p>'+
                                                          '<p>Rep: <b>'+q.shipped_rep+'</b></p>'+
                                                          '<p>Tracking Number: <b>'+q.trackingNum+'</b></p>';
      }
      if(q.invoiced === 'Yes'){
         document.getElementById('infoBoxBody').innerHTML += '<h2>Invoiced:</h2>'+
                                                          '<p>Status: <b>'+q.invoiced+'</b></p>'+
                                                          '<p>Date: <b>'+q.invoiced_date+'</b></p>'+
                                                          '<p>Time: <b>'+q.invoiced_time+'</b></p>'+
                                                          '<p>Rep: <b>'+q.invoiced_rep+'</b></p>'+
                                                          '<p>Invoice#: <b>'+q.invoiceNum+'</b></p>';
      }
      if(q.paid === 'Yes'){
         document.getElementById('infoBoxBody').innerHTML += '<h2>Paid:</h2>'+
                                                          '<p>Status: <b>'+q.paid+'</b></p>'+
                                                          '<p>Date: <b>'+q.paid_date+'</b></p>'+
                                                          '<p>Time: <b>'+q.paid_time+'</b></p>'+
                                                          '<p>Rep: <b>'+q.paid_rep+'</b></p>'+
                                                          '<p>Payment Details: <b>'+q.payDetail+'</b></p>';
      }
       document.getElementById('infoBoxBody').innerHTML += '</div>';
                                                          
      
    }
  }
  xmlhttp.open("GET","orders/php/get-order-details.php?oid="+oid+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
}


function change_order_date(){
	var oid = document.getElementById('new_order_date_id').value;
	var nod = document.getElementById('new_order_date').value;
	if(nod === ''){
		document.getElementById('new_order_date_error').innerHTML = 'Please Enter The New Order Date!';
		return;
	}
	if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'orders.php?tab=orders';
      
    }
  }
  xmlhttp.open("GET","orders/php/change-order-date.php?oid="+oid+"&nod="+nod+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
}


function change_inv_number(){
	var oid = document.getElementById('new_inv_number_id').value;
	var nid = document.getElementById('new_inv_number').value;
	if(nid === ''){
		document.getElementById('new_inv_number_error').innerHTML = 'Please Enter The New Invoice Number!';
		return;
	}
	if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'orders.php?tab=orders';
      
    }
  }
  xmlhttp.open("GET","orders/php/change-inv-number.php?oid="+oid+"&nid="+nid+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
}

function check_inv_number(nin){
	
//Check if Invoice Number Already Used...
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var r = JSON.parse(this.responseText);
      if(r.status === 'GOOD'){
        console.log('GOOD: '+nin);
        document.getElementById('inv_warn').style.display = 'none';
				document.getElementById('inv_change_btn').disabled = false;
				document.getElementById('inv_warn_mess').innerHTML = '';
      }else{
        console.log('BAD: '+nin);
        //BAD response...
        document.getElementById('inv_warn').style.display = 'inline';
				document.getElementById('inv_change_btn').disabled = true;
				document.getElementById('inv_warn_mess').innerHTML = r.response;
      }
    }
  }
  xmlhttp.open("GET","orders/php/check-invoice.php?invNum="+nin,true);
  xmlhttp.send();
}