function new_product(){
  document.getElementById('product_mode').value = "New";
  document.getElementById('product_name').value = '';
  document.getElementById('product_info').value = '';
  document.getElementById('product_units').value = '';
  document.getElementById('product_cost').value = '';
  document.getElementById('product_price').value = '';
  document.getElementById('product_weight').value = '';
  $('input[name=length_choice]').attr('checked',false);
  $('input[name=color_choice]').attr('checked',false);
  document.getElementById('color_list_choice').value = '';
  document.getElementById('color_list_box').style.display = 'none';
  document.getElementById('product_name').focus();
}


function save_product(){
  var pname = document.getElementById('product_name').value;
  if(pname === ''){
    alert('Please Enter The Products Name!');
    return;
  }
  pname = urlEncode(pname);
  var pinfo = document.getElementById('product_info').value;
  if(pinfo === ''){
    alert('Please Enter The Products Info!');
    return;
  }
  pinfo = urlEncode(pinfo);
  var punits = document.getElementById('product_units').value;
  if(punits === ''){
    alert('Please Enter The Products Units!');
    return;
  }
  punits = urlEncode(punits);
  var pcost = document.getElementById('product_cost').value;
  if(pcost === ''){
    alert('Please Enter The Products Cost!');
    return;
  }
  pcost = urlEncode(pcost);
  var pprice = document.getElementById('product_price').value;
  if(pprice === ''){
    alert('Please Enter The Products Price!');
    return;
  }
  pprice = urlEncode(pprice);
  var pweight = document.getElementById('product_weight').value;
  if(pweight === ''){
    alert('Please Enter The Product Weight!');
    return;
  }
  pweight = urlEncode(pweight);
  
  var ilength = document.querySelectorAll('input[name="length_choice"]:checked');
  if(ilength.length < 1){
    alert('Please Indicate Whether Length Is Required On The Invoice!');
    return;
  }
  var length = ilength[0].value;
  if(length === '' || length === 'undefined'){
    alert('Please Indicate Whether Length Is Required On The Invoice!');
    return;
  }
  var icolor = document.querySelectorAll('input[name="color_choice"]:checked');
  if(icolor.length < 1){
    alert('Please Indicate Whether Color Is Required On The Invoice!');
    return;
  }
  var color = icolor[0].value;
  if(color === '' || color === 'undefined'){
    alert('Please Indicate Whether Color Is Required On The Invoice!');
    return;
  }
  
  var color_list = document.getElementById('color_list_choice').value;
  if(color_list === '' && color === 'Yes'){
    alert('Please Select The Color List For This Product!');
    return;
  }
  
  
  var mode = document.getElementById('product_mode').value;
  var pid = document.getElementById('product_id').value;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'orders.php?tab=products';
      
    }
  }
  xmlhttp.open("GET","orders/php/add-edit-product.php?pname="+pname+"&pinfo="+pinfo+"&punits="+punits+"&pcost="+pcost+"&pprice="+pprice+"&pweight="+pweight+"&length="+length+"&color="+color+"&color_list="+color_list+"&mode="+mode+"&id="+pid,true);
  xmlhttp.send();
}


function edit_product(id){
  document.getElementById('product_mode').value = 'Edit';
  document.getElementById('product_id').value = id;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var q = JSON.parse(this.responseText);
      console.log(q);
    document.getElementById('product_name').value = q.pname;
    document.getElementById('product_info').value = q.pinfo;
    document.getElementById('product_units').value = q.punits;
    document.getElementById('product_cost').value = q.pcost;
    document.getElementById('product_price').value = q.pprice;
    document.getElementById('product_weight').value = q.pweight;
      if(q.length_required === 'Yes'){
        document.getElementById('length_no').removeAttribute('checked');
        document.getElementById('length_yes').setAttribute('checked','checked');
      }
      if(q.length_required === 'No'){
        document.getElementById('length_no').setAttribute('checked','checked');
        document.getElementById('length_yes').removeAttribute('checked');
      }
      if(q.color_required === 'Yes'){
        document.getElementById('color_no').removeAttribute('checked');
        document.getElementById('color_yes').setAttribute('checked','checked');
        document.getElementById('color_list_box').style.display = 'inline';
        document.getElementById('color_list_choice').value = q.color_list;
      }
      if(q.color_required === 'No'){
        document.getElementById('color_no').setAttribute('checked','checked');
        document.getElementById('color_yes').removeAttribute('checked');
        document.getElementById('color_list_box').style.display = 'none';
      }
    }
  }
  xmlhttp.open("GET","orders/php/get-product-details.php?pid="+id,true);
  xmlhttp.send();
}


function remove_product(pid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'orders.php?tab=products';
      
    }
  }
  xmlhttp.open("GET","orders/php/delete-product.php?pid="+pid,true);
  xmlhttp.send();
}


function color_list_option(){
  var icolor = document.querySelectorAll('input[name="color_choice"]:checked');
  var color_box = document.getElementById('color_list_box');
  if(icolor.length < 1){
    color_box.style.display = 'none';
  }
  var color = icolor[0].value;
  if(color === 'Yes'){
    color_box.style.display = 'inline';
  }else{
    color_box.style.display = 'none';
  }
}