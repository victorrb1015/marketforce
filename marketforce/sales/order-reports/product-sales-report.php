<?php
include '../../php/connection.php';

//Load Variables...
$pid = $_GET['pid'];

//Get Product List...
$pq = "SELECT * FROM `order_products` WHERE `inactive` != 'Yes' AND `ID` = '" . $pid . "'";
$pg = mysqli_query($conn, $pq) or die($conn->error);
$pr = mysqli_fetch_array($pg);

//Load Variables...
$sdate = mysqli_real_escape_string($conn,$_GET['sdate']);
$edate = mysqli_real_escape_string($conn,$_GET['edate']);
$all = mysqli_real_escape_string($conn,$_GET['all']);

echo '
<html>
<head>
<title>' . $pr['product_name'] . ' Sales Report</title>
<style>
/* page */

html { font: 16px/1 "Open Sans", sans-serif; overflow: auto; padding: 0.5in; }
html { background: #999; cursor: default; }

body { box-sizing: border-box; min-height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }

th, td{
  border:1px solid black;
  padding:5px;
}

@media print {
	* { -webkit-print-color-adjust: exact; }
	html { background: none; padding: 0; }
	body { box-shadow: none; margin: 0; }
	span:empty { display: none; }
	.add, .cut { display: none; }
}

@page { margin: 0; }
</style>
</head>
<body>
<h1 style="text-align:center;">' . $pr['product_name'] . ' Sales Report</h1>
';

 echo '<table>
        <thead>
         <tr style="background:lightgray;">
         <th>Inv#</th>
         <th>Product Name</th>
         <th>Product ID</th>
         <th>Sold By</th>
         <th>Length</th>
         <th>Qty</th>
         <th>Customer Rate</th>
         <th>Item Total</th>
         </tr>
         </thead>
         <tbody>';


  
  
  //Get Sales for this product...
  $spq = "SELECT * FROM `order_invoice_items` WHERE `pid` = '" . $pr['ID'] . "' AND `inactive` != 'Yes'";
  $spg = mysqli_query($conn, $spq) or die($conn->error);
  while($spr = mysqli_fetch_array($spg)){
    if($all != ''){
      $ivq = "SELECT * FROM `order_invoices` WHERE `inv_num` = '" . $spr['inv_num'] . "' AND `inactive` != 'Yes'";
    }else{
      $ivq = "SELECT * FROM `order_invoices` 
              WHERE 
              `inv_num` = '" . $spr['inv_num'] . "' 
              AND 
              `inactive` != 'Yes'
              AND 
              `date` >= '" . $sdate . "' 
              AND 
              `date` <= '" . $edate . "'";
    }
    $ivg = mysqli_query($conn, $ivq) or die($conn->error);
    $rcount = mysqli_num_rows($ivg);
    if($rcount > 0){
  
  echo '<tr>
          <td>' . $spr['inv_num'] . '</td>
          <td>' . $pr['product_name'] . '</td>
          <td>' . $pr['ID'] . '</td>
          <td>' . $pr['product_units'] . '</td>
          <td>' . $spr['item_length'] . '</td>
          <td>' . $spr['item_qty'] . '</td>
          <td>$' . number_format($spr['item_rate'],2) . '</td>
          <td>$' . number_format($spr['item_price'],2) . '</td>
        </tr>';
      
    }
  
}
echo '</tbody>
      </table>';

echo '</body>
</html>';

?>