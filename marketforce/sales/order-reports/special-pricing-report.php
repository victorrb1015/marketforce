<?php
include '../../php/connection.php';

echo '
<html>
<head>
<title>Special Pricing Report</title>
<style>
/* page */

html { font: 16px/1 "Open Sans", sans-serif; overflow: auto; padding: 0.5in; }
html { background: #999; cursor: default; }

body { box-sizing: border-box; min-height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }

th, td{
  border:1px solid black;
  padding:5px;
}

@media print {
	* { -webkit-print-color-adjust: exact; }
	html { background: none; padding: 0; }
	body { box-shadow: none; margin: 0; }
	span:empty { display: none; }
	.add, .cut { display: none; }
}

@page { margin: 0; }
</style>
</head>
<body>
<h1 style="text-align:center;">Special Pricing Report</h1>
';
//Get Product List...
$pq = "SELECT * FROM `order_products` WHERE `inactive` != 'Yes' ORDER BY `product_name` ASC";
$pg = mysqli_query($conn, $pq) or die($conn->error);
while($pr = mysqli_fetch_array($pg)){
  echo '<h3>' . $pr['product_name'] . ' -- [Normal Price: $' . $pr['product_price'] . ' Per ' . $pr['product_units'] . ']</h3>';
  
  
  //Get Special Prices for this product...
  $spq = "SELECT * FROM `order_special_pricing` WHERE `inactive` != 'Yes' AND `pid` = '" . $pr['ID'] . "'";
  $spg = mysqli_query($conn, $spq) or die($conn->error);
  if(mysqli_num_rows($spg) > 0){
    
    
  echo '<table>
        <thead>
         <tr style="background:lightgray;">
         <th>Customer Name</th>
         <th>Special Price</th>
         <th>Date Set</th>
         </tr>
         </thead>
         <tbody>';
  
  while($spr = mysqli_fetch_array($spg)){
    //Get Customer Name...
    $cnq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $spr['cid'] . "'";
    $cng = mysqli_query($conn, $cnq) or die($conn->error);
    $cnr = mysqli_fetch_array($cng);
    if(mysqli_num_rows($cng) > 0){
    $cname = $cnr['company_name'];
    
    echo '<tr>
          <td>' . $cname . '</td>
          <td><b>$' . $spr['special_price'] . '</b> Per ' . $pr['product_units'] . '</td>
          <td>' . date("m/d/y",strtotime($spr['date'])) . '</td>
          </tr>';
  }
  }
  
  echo '</tbody>
      </table>';
  
}else{
  echo '<h4 style="color:red;font-weight:bold;">No Special Pricing Set</h4>';  
  }
  
}

echo '</body>
</html>';

?>