<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h1 class="panel-title">Product Display Table &nbsp; 
          <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#newProductModal" onclick="new_product();"><i class="fa fa-plus"></i> Add Product</button>
          &nbsp;&nbsp;
          <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#colorsModal"><i class="fa fa-list-ul"></i> Manage Colors</button>
        </h1>
      </div>
      <div class="panel-body table-responsive" style="height:auto;">
        <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr class="info">
              <th style="font-size:18px;">Date</th>
              <th style="font-size:18px;">Product Name</th>
              <th style="font-size:18px;">Product Description</th>
              <th style="font-size:18px;">Units</th>
              <th style="font-size:18px;">Weight</th>
              <th style="font-size:18px;">Cost</th>
              <th style="font-size:18px;">Customer Price</th>
              <th style="font-size:18px;">Actions</th>
            </tr>
          </thead>
          <tbody>
         <?php
            $pq = "SELECT * FROM `order_products` WHERE `inactive` != 'Yes'";
            $pg = mysqli_query($conn, $pq) or die($conn->error);
            while($pr = mysqli_fetch_array($pg)){
            echo '<tr>
              <td>' . date("m/d/y", strtotime($pr['date'])) . '</td>
              <td style="color: rgb(47,112,169);"><strong>' . $pr['product_name'] . '</strong></td>
              <td>' . $pr['product_info'] . '</td>
              <td style="color: rgb(7,7,7);">' . $pr['product_units'] . '</td>
              <td>' . $pr['product_weight'] . ' LBS</td>
              <td>' . $pr['product_cost'] . '</td>
              <td style="color: rgb(49,102,49);"><strong>' . $pr['product_price'] . '</strong></td>
              <td>
                <div class="btn-group" role="group">
                  <button class="btn btn-default" type="button" data-toggle="modal" data-target="#newProductModal" onclick="edit_product(' . $pr['ID'] . ');">Edit</button>
                  <button class="btn btn-danger" type="button" onclick="remove_product(' . $pr['ID'] . ');">Delete</button>
                </div>
              </td>
            </tr>';
            }
         ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>