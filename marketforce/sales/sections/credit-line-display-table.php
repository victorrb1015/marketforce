|<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h1 class="panel-title">Credit Line Display Table &nbsp; 
          <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#newCreditLineModal" onclick="new_cl();"><i class="fa fa-plus"></i> Add Credit Line</button>
        </h1>
      </div>
      <div class="panel-body table-responsive" style="height:auto;">
        <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr class="info">
              <th style="font-size:18px;">Date</th>
              <th style="font-size:18px;">Customer Name</th>
              <th style="font-size:18px;">Credit Line Amount</th>
              <th style="font-size:18px;">Current Balance</th>
              <th style="font-size:18px;">Notes</th>
              <th style="font-size:18px;">Status</th>
              <th style="font-size: 18px;width: 191px;">Actions</th>
            </tr>
          </thead>
          <tbody>
        <?php
          $final_total_balance = 0;
          $final_credit_line_total = 0;
          //$clq = "SELECT * FROM `order_credit_lines` WHERE `inactive` != 'Yes' ORDER BY `cname` ASC";
          $clq = "SELECT * FROM `order_credit_lines` WHERE `inactive` != 'Yes'";
          $clg = mysqli_query($conn, $clq) or die($conn->error);
          while($clr = mysqli_fetch_array($clg)){
            $cnq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $clr['cid'] . "'";
            $cng = mysqli_query($conn, $cnq) or die($conn->error);
            $cnr = mysqli_fetch_array($cng);
            $cname = $cnr['company_name'];
            $final_credit_line_total = $final_credit_line_total + str_replace(',','',$clr['credit_amount']);
            echo '<tr>
              <td>' . date("m/d/y", strtotime($clr['date'])) . '</td>
              <td style="color: rgb(47,112,169);"><strong>' . $cname . '</strong></td>
              <td>$' . $clr['credit_amount'] . '</td>
              <td style="color: rgb(7,7,7);">';
            
            
            $total_balance = 0;
            $oq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `paid` != 'Yes' AND `cname` = '" . $cname . "'";
            $og = mysqli_query($conn, $oq) or die($conn->error);
            while($or = mysqli_fetch_array($og)){
              
              $iq = "SELECT * FROM `order_invoices` WHERE `inv_num` = '" . $or['inv'] . "'";
              $ig = mysqli_query($conn, $iq) or die($conn->error);
              $ir = mysqli_fetch_array($ig);
              if($ir['status'] == 'Estimate' || $or['invoiced'] != 'Yes'){
                //Do nothing because this does not add to balance owed...
              }else{
                $total_balance = $total_balance + $ir['freight'];
                $taxRate = str_replace('.','',$ir['tax_rate']);
                $taxRate = '.0' . $taxRate;
                $taxRate = $taxRate + 1;
                //echo $taxRate;
                $bq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $ir['inv_num'] . "'";
                $bg = mysqli_query($conn, $bq) or die($conn->error);
                while($br = mysqli_fetch_array($bg)){
                  $total_balance = $total_balance + $br['item_price'] * $taxRate;
                }
              }
            }
            echo '$' . number_format($total_balance,2);
            $final_total_balance = $final_total_balance + $total_balance;
            
        echo '</td>
              <td style="color: rgb(7,7,7);">' . $clr['notes'] . '</td>
              <td style="color:rgb(213,19,7);"><strong>' . $clr['status'] . '</strong></td>
              <td>
                <div class="btn-group" role="group">
                  <button class="btn btn-default" type="button" data-toggle="modal" data-target="#newCreditLineModal" onclick="edit_cl(' . $clr['ID'] . ');">Edit</button>
                  <button class="btn btn-danger" type="button" onclick="remove_cl(' . $clr['ID'] . ');">Deactivate</button>
                  <!--<button class="btn btn-warning" type="button">Email Statement</button>-->
                </div>
              </td>
            </tr>';
          }
       ?>
            
            <tr style="background:green;color:white;">
              <td></td>
              <td style="text-align:center;"><b>TOTALS:</b></td>
              <td><b>$<?php echo number_format($final_credit_line_total,2); ?></b></td>
              <td><b>$<?php echo number_format($final_total_balance,2); ?></b></td>
              <td><b><?php echo number_format(($final_total_balance / $final_credit_line_total * 100),2) . '% Used'; ?></b></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>