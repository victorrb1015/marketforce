<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h1 class="panel-title">Portal Users &nbsp; 
          <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#newPortalUserModal" onclick="pu_load_modal('New');"><i class="fa fa-plus"></i> Add Portal User</button>
      <?php
          if($_SERVER['HTTP_HOST'] == 'beta.marketforceapp.com'){
            echo '<button type="button" class="btn btn-danger btn-sm" onclick="window.open(\'http://beta-myorders.marketforceapp.com\',\'_blank\');"><i class="fa fa-share"></i> Launch Portal</button>';
          }else{
            echo '<button type="button" class="btn btn-danger btn-sm" onclick="window.open(\'http://orders.marketforceapp.com\',\'_blank\');"><i class="fa fa-share"></i> Launch Portal</button>';
          }
      ?>
        </h1>
      </div>
      <div class="panel-body table-responsive" style="height:auto;">
        <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr class="info">
              <th>Date</th>
              <th>Name</th>
              <th>Username</th>
              <th>Password</th>
              <th>Last Login</th>
              <!--<th>Last Login</th>-->
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
        <?php
          $plq = "SELECT * FROM `users` WHERE `inactive` != 'Yes' AND `org_id` = '" . $_SESSION['org_id'] . "'";
          $plg = mysqli_query($pconn, $plq) or die($pconn->error);
          while($plr = mysqli_fetch_array($plg)){
            $pucq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $plr['cid'] . "'";
            $pucg = mysqli_query($conn, $pucq) or die($conn->error);
            $pucr = mysqli_fetch_array($pucg);
            $cname = $pucr['company_name'];
            echo '<tr>
              <td>' . date("m/d/y", strtotime($plr['date'])) . '</td>
              <td><strong>' . $plr['fname'] . ' ' . $plr['lname'] . ' [' . $cname . ']</strong></td>
              <td>' . $plr['user'] . '</td>
              <td>' . $plr['pass'] . '</td>';
            if($plr['last_login'] == '0000-00-00 00:00:00'){
              echo '<td>Never</td>';
            }else{
              echo '<td>' . date("m/d/y @ h:iA",strtotime($plr['last_login'])) . '</td>';
            }
            if($plr['inactive'] == 'Yes'){
              echo '<td><strong>Inactive</strong></td>';
            }else{
              echo '<td><strong>Active</strong></td>';
            }
        echo '<td>
                <div class="btn-group" role="group">
                  <button class="btn btn-default" type="button" data-toggle="modal" data-target="#newPortalUserModal" onclick="pu_load_modal(\'Edit\',' . $plr['ID'] . ');">Edit</button>
                  <button class="btn btn-danger" type="button" onclick="remove_portal_user(' . $plr['ID'] . ');">Remove</button>
                </div>
              </td>
            </tr>';
          }
       ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>