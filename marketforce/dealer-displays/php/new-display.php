<?php
include '../../php/connection.php';

//Load Variables...
$dealer_id = $_POST['dealer'];

$dq = "SELECT * FROM `dealers` WHERE `ID` = '" . $dealer_id . "'";
$dg = mysqli_query($conn, $dq) or die($conn->error);
$dr = mysqli_fetch_array($dg);
$dealer_name = mysqli_real_escape_string($conn,$dr['name']);

$display_type = $_POST['display_type'];
$length = $_POST['display_length'];
$width = $_POST['display_width'];
$height = $_POST['display_height'];
$color = $_POST['display_color'];

$status = 'Installed';

//Add new display...
$aq = "INSERT INTO `dealer_displays`
      (
      `date`,
      `time`,
      `dealer_id`,
      `dealer_name`,
      `display_type`,
      `length`,
      `width`,
      `height`,
      `color`,
      `rep_id`,
      `rep_name`,
      `status`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $dealer_id . "',
      '" . $dealer_name . "',
      '" . $display_type . "',
      '" . $length . "',
      '" . $width . "',
      '" . $height . "',
      '" . $color . "',
      '" . $_SESSION['user_id'] . "',
      '" . $_SESSION['full_name'] . "',
      '" . $status . "',
      'No'
      )";
mysqli_query($conn, $aq) or die($conn->error);

echo '<script>
      window.location = "../../dealer-displays.php";
      </script>';
?>