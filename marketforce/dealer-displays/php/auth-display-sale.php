<?php
include '../../php/connection.php';

//Load Variables...
$ddid = mysqli_real_escape_string($conn, $_GET['ddid']);
$rep_id = mysqli_real_escape_string($conn, $_GET['rep_id']);
$rep_name = mysqli_real_escape_string($conn, $_GET['rep_name']);
$da_amount = mysqli_real_escape_string($conn, $_GET['da_amount']);
$da_notes = mysqli_real_escape_string($conn, $_GET['da_notes']);

//Perform Action...
$q = "UPDATE `dealer_displays` SET 
    `notes` = '" . $da_notes . "',
		`sale_auth` = 'Yes',
		`sale_amount` = '" . $da_amount . "',
		`sale_auth_by_id` = '" . $rep_id . "',
		`sale_auth_by_name` = '" . $rep_name . "',
		`status` = 'For Sale'
		WHERE `ID` = '" . $ddid . "'";
mysqli_query($conn, $q) or die($conn->error);

echo 'Sale of Display has been successfully authorized.';

?>