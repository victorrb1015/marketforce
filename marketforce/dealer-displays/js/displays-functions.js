  //Encode Data for transmission via XHR...
  function encodeURL(url){
    url = url.replace(/&/g, '%26'); 
    url = url.replace(/#/g, '%23');
    return url;
  }

//Load Modal...
function load_display_modal(ddid){
  document.getElementById('da-error').innerHTML = '';
  document.getElementById('display-id').value = ddid;
}

//Authorize Dealer Display Sale...
function auth_display_sale(){
  var ddid = document.getElementById('display-id').value;
  var amount = document.getElementById('da_amount').value;
  if(amount === '' || amount < 0){
    document.getElementById('da-error').innerHTML = '*Please Enter The Amount Authorized For The Sale!';
    return;
  }
  amount = encodeURL(amount);
  var notes = document.getElementById('da_notes').value;
  if(notes === ''){
    document.getElementById('da-error').innerHTML = '*Please Enter Notes Regarding The Authorization of The Sale!';
    return;
  }
  notes = encodeURL(notes);

	if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","dealer-displays/php/auth-display-sale.php?ddid="+ddid+"&da_amount="+amount+"&da_notes="+notes+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
}


//Mark Display Sold...
function mark_display_sold(did){
  var conf = confirm("Are you sure you want to mark this display Sold?");
  if(conf === false){
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","dealer-displays/php/mark-display-sold.php?did="+did+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
}


//Remove Display...
function remove_display(did){
  var conf = confirm("Are you sure you want to remove this display?");
  if(conf === false){
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","dealer-displays/php/remove-display.php?did="+did+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
}


//Approve Display Order...
function approve_display_order(oid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","dealer-displays/php/display-order-handler.php?oid="+oid+"&mode=Approve",true);
  xmlhttp.send();
}


//Deny Display Order...
function approve_display_order(oid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","dealer-displays/php/display-order-handler.php?oid="+oid+"&mode=Deny",true);
  xmlhttp.send();
}