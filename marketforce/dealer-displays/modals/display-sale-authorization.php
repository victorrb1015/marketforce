 <div class="modal fade" role="dialog" tabindex="-1" id="display-sale-authorization">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Display Sale Authorization Form</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="display-id" name="display-id" value="" />
                    <p class="text-primary">Sale Amount:</p>
                    <div class="input-group">
                        <div class="input-group-addon"><span><i class="fa fa-dollar"></i></span></div><input id="da_amount" name="da_amount" class="form-control usd" type="text">
                        <div class="input-group-addon"></div>
                    </div>
                    <p class="text-primary"></p>
                    <p class="text-primary">Notes:</p><textarea id="da_notes" name="da_notes" class="form-control" style="width: 100%;"></textarea></div>
                <div class="modal-footer">
                    <p id="da-error" style="color:red;font-weight:bold;"></p>
                    <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="button" onclick="auth_display_sale();">Save</button></div>
                </div>
        </div>
    </div>