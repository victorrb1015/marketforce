<div id="newDisplaySaleModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
<form action="dealer-displays/php/new-display.php" method="post">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Display Sale Authorization</h4>
      </div>
      <div class="modal-body">
        <div class="row" style="margin-bottom:15px;">
          <div class="col-lg-6">
            <p>
              <b>Dealer</b>
            </p>
            <select id="dealer" name="dealer" class="form-control">
              <option value="">Select A Dealer</option>
              <?php
              $doq = "SELECT * FROM `dealers` WHERE `inactive` != 'Yes' AND `name` != '' ORDER BY `name` ASC";
              $dog = mysqli_query($conn, $doq) or die($conn->error);
              while($dor = mysqli_fetch_array($dog)){
                echo '<option value="' . $dor['ID'] . '">' . $dor['name'] . '</option>';
              }
              ?>
            </select>
          </div>
          <div class="col-lg-6">
            <p>
              <b>Display Type</b>
            </p>
            <select id="display_type" name="display_type" class="form-control">
              <option value="">Select Display Type</option>
              <option value="Two Car Garage">Two Car Garage</option>
              <option value="Garage">Garage</option>
              <option value="Combo Unit A-Frame">Combo Unit A-Frame</option>
              <option value="RV Port">RV Port</option>
              <option value="Carport Standard">Carport Standard</option>
              <option value="Carport Special">Carport Special</option>
              <option value="Vertical Building">Vertical Building</option>
              <option value="Utility - Front Railing">Utility - Front Railing</option>
              <option value="A-Frame Utility">A-Frame Utility</option>
              <option value="Utility Standard">Utility Standard</option>
              <option value="Vertical Utility">Vertical Utility</option>
            </select>
          </div>
        </div>
        
        <div class="row" style="margin-bottom:15px;">
          <div class="col-lg-4">
            <p>
              <b>Length</b>
            </p>
            <input type="text" id="display_length" name="display_length" class="form-control" />
          </div>
          <div class="col-lg-4">
            <p>
              <b>Width</b>
            </p>
            <input type="text" id="display_width" name="display_width" class="form-control" />
          </div>
          <div class="col-lg-4">
            <p>
              <b>Height</b>
            </p>
            <input type="text" id="display_height" name="display_height" class="form-control" />
          </div>
        </div>
        
        <div class="row">
          <div class="col-lg-6">
            <p>
              <b>Color</b>
            </p>
            <input type="text" id="display_color" name="display_color" class="form-control" />
          </div>
          <!--<div class="col-lg-6">
            <p>
              <b>Amount</b>
            </p>
            <input type="text" id="display_amount" name="display_amount" class="form-control usd" />
          </div>-->
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
    </form>
  </div>
</div>