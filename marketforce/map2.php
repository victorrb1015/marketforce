<?php
include 'security/session/session-settings.php';
include 'php/connection.php';

if(!isset($_SESSION['position'])){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="//marketforceapp.com";
				}, 2000);
				</script>';
	return;
}



$pageName = 'Dealer Map';//Set this equal to the name of the page you would like to set...
$pageIcon = 'fa fa-globe';//Set this equal to the class name of the icon you would like to set as the page icon...
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
    <!--JQuery CSS-->
    <link href="jquery/jquery-ui.css" rel="stylesheet">


	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
  </style>
	
	<style>
						/* This only works with JavaScript, 
if it's not present, don't show loader */
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	//background: url(img/loader-128x/Preloader_7.gif) center no-repeat #fff;
  background: url(img/loaders/loading-circle.gif) center no-repeat rgba(250,250,250,0.5);
}
	</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script>
//paste this code under the head tag or in a separate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
</script>
</head>

<body>
	<!--PRELOADER GIF-->
<!--<div class="se-pre-con"></div>-->
  
    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

              

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							<!--<div class="legend" style="text-align: center; margin: auto;">
								<table style="margin: auto;">
									<tr>
										<td style="padding-right: 10px;">
											<img src="img/map/check.png" style="height: 20px; width: 20px;" /> = New Dealer
										</td>
										<td style="padding-right: 10px;">
											<img src="img/map/pin.png" style="height: 30px; width: 20px;" /> = Dealer is trained and has displays
										</td>
										<td>
											<img src="img/map/star.png" style="height: 20px; width: 20px;" /> = Dealer has been visited this year
										</td>
									</tr>
								</table>
							</div>-->
              
              <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> <?php

                                switch($_SESSION['org_id']) {
                                    case "832122":
                                        echo "Customer ";
                                        break;
                                    case "123456":
                                    case '532748':
                                        echo "Client ";
                                        break;
                                    default:
                                        echo $pageName;
                                }
                                ?> </small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
														<li>
															<i class="<? echo $pageIcon; ?>"></i> <?php switch($_SESSION['org_id']) {
                        case "832122":
                        case "123456":
                            echo "Client ";
                            break;
                        default:
                            echo $pageName;
                    }?>
													</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
              
							<!--THIS IS THE OLD UNOPTIMIZED MAP (CHECK CHANGE LOG INSIDE OPTIMIZED MAP FOR CHANGES)
							<iframe src="//<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/map/mapv2.php?fn=<?php echo $_SESSION['full_name']; ?>&org_global_map=<?php echo $_SESSION['org_global_map']; ?>" style="width: 100%; height: 600px;"></iframe>
							-->
							<!--
							<iframe src="//<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/map/dealer-map.php?xx=<?php echo rand(10,1000); ?>&fn=<?php echo $_SESSION['full_name']; ?>&org_global_map=<?php echo $_SESSION['org_global_map']; ?>" style="width: 100%; height: 600px;"></iframe>
							-->
							<iframe src="//<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/map/dealer-map.php?xx=<?php echo rand(10,1000); ?>&fn=<?php echo $_SESSION['full_name']; ?>&org_global_map=<?php echo $_SESSION['org_global_map']; ?>" style="width: 100%; height: 75vh;"></iframe>
							<br>
							<div style="text-align:center;">
								<a href="//<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/map/dealer-map.php?xx=<?php echo rand(10,1000); ?>&fn=<?php echo $_SESSION['full_name']; ?>&org_global_map=<?php echo $_SESSION['org_global_map']; ?>" target="_blank">
									<button type="button" style="color:white;background:red;font-weight:bold;text-align:center;">
										Open Full Screen
									</button>
								</a>
							</div>
							
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
   <!-- <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>-->

 <?php include 'footer.html'; ?>
</body>

</html>