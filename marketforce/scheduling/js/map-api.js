function initAutocomplete(zx) {
    if (!zx) {
      zx = 5;
    }
    
    //Enable Geocoding Service
    geocoder = new google.maps.Geocoder();
    
    /*geocoder.geocode( { 'address': '109 wellington way middletown de 19709'}, function(results, status) {
      console.log(results);
      console.log(status);
      
    });*/
    
    document.getElementById('confirmRouteBTN').addEventListener('click', function(){
        var rrid = urlParams.get('edit_route');
        if(rrid){
          delete_route(rrid,'edit');
        }
        var rep_id = document.getElementById('rep_id_tag').value;
        var rep_name = document.getElementById('rep_name_tag').value;
        if(stop_id.length <= 0){
          alert("You have not added any invoices to the route!");
          return;
        }
        var sstop_name = JSON.stringify(stop_name);
        sstop_name = urlEncode(sstop_name);
        var sstop_id = JSON.stringify(stop_id);
        sstop_id = urlEncode(sstop_id);
        var sstop_address = JSON.stringify(stop_address);
        sstop_address = urlEncode(sstop_address);
        var sstop_cost = JSON.stringify(stop_cost);
        sstop_cost = urlEncode(sstop_cost);
        var sstop_location = JSON.stringify(stop_location);
        sstop_location = urlEncode(sstop_location);
        var sstop_type = JSON.stringify(stop_type);
        sstop_type = urlEncode(sstop_type);
        var swaypoint_order = JSON.stringify(waypoint_order);
        swaypoint_order = urlEncode(swaypoint_order);
        var smURL = urlEncode(mURL);
        
        
        if (window.XMLHttpRequest) {
          // code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
        } else {  // code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
          if (this.readyState==4 && this.status==200) {
            
            var r = JSON.parse(this.responseText);
            if(r.response === 'GOOD'){
              document.getElementById('exitCheck').value = 'Yes';
              //alert('Your Route has been confirmed!');
              alert(r.message);
              window.location = 'scheduling.php?bypass=y';
            }else if(r.response === 'ERROR'){
              toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
            }else{
              toast_alert('Something Went Wrong...','Unknown error during route confirmation','bottom-right','error');
            }
            
          }
        }
        var params = "scheduling/php/confirm-route.php?"+
                     "stop_name="+sstop_name+
                     "&stop_id="+sstop_id+
                     "&stop_address="+sstop_address+
                     "&stop_cost="+sstop_cost+
                     "&stop_location="+sstop_location+
                     "&stop_type="+sstop_type+
                     "&rep_id="+rep_id+
                     "&rep_name="+rep_name+
                     "&waypoint_order="+swaypoint_order+
                     "&mURL="+smURL+
                     "&directions_response="+directions_response;
        console.log(params);
        xmlhttp.open("POST",params,true);
        //Send the proper header information along with the request
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        if(rrid === ''){
          var to = 0;
        }else{
          var to = 3000;
        }
        setTimeout(function (){
          xmlhttp.send();
        },to);
        
    });
    
  
    //Starts the map...
    //center: new google.maps.LatLng(40.213362, -85.377858),
    map = new google.maps.Map(document.getElementById('map'), {
      center: new google.maps.LatLng(40.213362, -85.377858),
      zoom: zx,
      gestureHandling: 'greedy'
    });
  
  
    //Spider Mapping Capability...
    var oms = new OverlappingMarkerSpiderfier(map, {
      markersWontMove: true,
      markersWontHide: true,
      basicFormatEvents: true
    });
  
  
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(map);
  
    //Sets up the information window for when a marker is clicked...
    infoWindow = new google.maps.InfoWindow;
  
    if (schedule_state === 'Oklahoma') {
      var hpoint = new google.maps.LatLng(35.723101, -95.402094);
    } else {
      var hpoint = new google.maps.LatLng(40.213878, -85.377739);
    }
  
    //Setup the icon image for the Home Base...
    var himg = "//dynicon.com/icon.php?text=Main Office&bg=3C3C3C&tcolor=FFFFFF&i=home";
    var p_icon = "//dynicon.com/icon.php?text=Pending&bg=C6EF8C&tcolor=000000&i=clock";
  
    var hmarker = new google.maps.Marker({
      position: hpoint,
      //map: map,
      icon: himg,
      zIndex: 10000
    });
  
    //Add Market To SpiderMapping...
    oms.addMarker(hmarker);
  
    var hcont = document.createElement("div");
    hcont.id = "home_base";
    hcont.innerHTML = "<h1 style='color:black;'>Main Office</h1>";
  
    hmarker.addListener("click", function() {
      infoWindow.setContent(hcont);
      infoWindow.open(map, hmarker);
    });
  
    infoWindow.setContent(hcont);
    infoWindow.open(map, hmarker);
  
    function geocode_orders(md,address,city,state,zip){
      var g_address = address+' '+city+' '+state+' '+zip;
      geocoder.geocode( { 'address': g_address}, async function(results, status) {
        if(status === 'OK'){
          console.log('2nd Round');
          console.log(md.orderID);
          console.log(results);
          md.lat = results[0].geometry.location.lat();
          md.lng = results[0].geometry.location.lng();
          md.updateLatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng());
          var latlng = new google.maps.LatLng(md.lat,md.lng);
          window['marker' + md.orderID].setPosition(latlng);
        }else{
          console.log('2nd Round');
          console.error('An error occurred while geocoding address for repairID: '+md.orderID+'. ERROR: '+status);
          if(status === 'OVER_QUERY_LIMIT'){
            setTimeout(function(){
              geocode_orders(md,address,city,state,zip);
            },10000);
          }
          if(status === 'ZERO_RESULTS'){
            md.updateLatLng('Error','Error');
          }
        }
      });
    }
  
  
    //Fetch All Orders from Sales Portal...
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        // Typical action to be performed when the document is ready:
        var r = JSON.parse(this.responseText);
        if (r.response === 'GOOD') {
          toast_alert('Success!',r.message,'bottom-right','success');
          var ts = document.createElement('span');
          ts.setAttribute('class','badge badge-secondary');
          ts.setAttribute('style','color:black;');
          ts.innerHTML = 'Orders Last Synced: '+r.sync_timestamp+' -- ';
          var link = document.createElement('a');
          link.setAttribute('href','Javascript:reset_cache();window.location.reload();');
          link.setAttribute('style','color:blue;');
          link.innerHTML = 'Reload Map';
          ts.appendChild(link);
          document.getElementById('map-top-bar').appendChild(ts);
          order_results = r.data;
          r.data.forEach(async function(d) {
            var md = new marker_data_order(d);
            //console.log(md);
            if(md.lat === '' || md.lng === '' || md.lat === null || md.lng === null){
              var g_address = md.buildingAddress+' '+md.buildingCity+' '+md.buildingState+' '+md.buildingZipCode;
              await geocoder.geocode( { 'address': g_address}, function(results, status) {
                if(status === 'OK'){
                  console.log(results);
                  md.lat = results[0].geometry.location.lat();
                  md.lng = results[0].geometry.location.lng();
                  md.updateLatLng(md.lat,md.lng);
                  add_map_marker_order(md);
                }else{
                  console.error('An error occurred while geocoding address for orderID: '+md.orderID+' -- '+status);
                  add_map_marker_order(md);
                  if(status === 'OVER_QUERY_LIMIT'){
                    setTimeout(function(){
                      geocode_orders(md,d.buildingAddress,d.buildingCity,d.buildingState,d.buildingZipCode);
                    },10000);
                  }
                  if(status === 'ZERO_RESULTS'){
                    md.updateLatLng('Error','Error');
                  }
                }
              });

            }else{
              add_map_marker_order(md);
            }
            
          });
          console.log('Order Fetching Complete...');
          document.getElementById('map-loader').style.display = 'none';
          fetch_repairs();
        } else {
          console.error('ERROR Fetching Orders...');
        }
      }
    };
    xhttp.open("GET", "scheduling/php/fetch-current-orders.php?org_id="+org_id, true);
    xhttp.send();
    console.warn('Fetching Orders Now');
  
  
    function geocode_repairs(md,address,city,state,zip){
      var g_address = address+' '+city+' '+state+' '+zip;
      geocoder.geocode( { 'address': g_address}, async function(results, status) {
        if(status === 'OK'){
          console.log('2nd Round');
          console.log(md.orderID);
          console.log(results);
          md.lat = results[0].geometry.location.lat();
          md.lng = results[0].geometry.location.lng();
          md.updateLatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng());
          var latlng = new google.maps.LatLng(md.lat,md.lng);
          window['rmarker' + md.orderID].setPosition(latlng);
        }else{
          console.log('2nd Round');
          console.error('An error occurred while geocoding address for repairID: '+md.orderID+'. ERROR: '+status);
          if(status === 'OVER_QUERY_LIMIT'){
            setTimeout(function(){
              geocode_repairs(md,address,city,state,zip);
            },10000);
          }
          if(status === 'ZERO_RESULTS'){
            md.updateLatLng('Error','Error');
          }
        }
      });
    }
  
  
    function fetch_repairs(){
      //Fetch All Repairs from MarketForce...
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          // Typical action to be performed when the document is ready:
          var r = JSON.parse(this.responseText);
          if (r.response === 'GOOD') {
            toast_alert('Success!',r.message,'bottom-right','success');
            //r.orders.forEach(async function(d) {
            var ri = 0;
            var promise = Promise.resolve();
            var interval = 1000;
            repair_results = r.data;
            r.data.forEach(async function(d) {
              var md = new marker_data_repair(d);
              //console.log(md);
              if(d.lat === '' || d.lng === ''){
                var g_address = d.address+' '+d.city+' '+d.state+' '+d.zip;
                geocoder.geocode( { 'address': g_address}, async function(results, status) {
                  if(status === 'OK'){
                    ri++;
                    console.log('1st Round');
                    console.log(results);
                    md.lat = results[0].geometry.location.lat();
                    md.lng = results[0].geometry.location.lng();
                    md.updateLatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng());
                    add_map_marker_repair(md);
                  }else{
                    console.log('1st Round');
                    console.error('An error occurred while geocoding address for repairID: '+md.orderID+'. ERROR: '+status);
                    add_map_marker_repair(md);
                    if(status === 'OVER_QUERY_LIMIT'){
                      setTimeout(function(){
                        geocode_repairs(md,d.address,d.city,d.state,d.zip);
                      },10000);
                    }
                    if(status === 'ZERO_RESULTS'){
                      md.updateLatLng('Error','Error');
                    }
                  }
                });

              }else{
                add_map_marker_repair(md);
              }
              
            });
            console.log('Order Fetching Complete...');
            document.getElementById('map-loader').style.display = 'none';
          } else {
            console.error('ERROR Fetching Orders...');
          }
        }
      };
      xhttp.open("GET", "scheduling/php/fetch-current-repairs.php?org_id="+org_id, true);
      xhttp.send();
      console.warn('Fetching Repairs Now');
    }
  
  
    function add_map_marker_order(md) {
      if(debug === 'y'){console.warn(md);}
      var point = new google.maps.LatLng(md.lat, md.lng);
      if (md.in_use === 'Yes') {
        img = "//dynicon.com/icon.php?text=Assigned To Route&bg=9D9D9D&tcolor=FFFFFF&i=task-planning";
      } else {
        img = "//dynicon.com/icon.php?text=$" + md.totalSale + "&bg=" + md.mBG + "&tcolor=" + md.mFC + "&i=purchase-order";
      }
  
      window['marker' + md.orderID] = new google.maps.Marker({
        position: point,
        icon: img,
        id: md.orderID
      });
      //Add Marker To Spider Mapping...
      oms.addMarker(window['marker' + md.orderID]);
      //Add Marker to State array...
      switch (md.buildingState){
          case "IN":
            IN_markers.push(window['marker'+md.orderID]);
            break;
          case "IL":
            IL_markers.push(window['marker'+md.orderID]);
            break;
          case "TX":
            TX_markers.push(window['marker'+md.orderID]);
            break;
          case "AR":
            AR_markers.push(window['marker'+md.orderID]);
            break;
          case "KY":
            KY_markers.push(window['marker'+md.orderID]);
            break;
          case "MI":
            MI_markers.push(window['marker'+md.orderID]);
            break;
          case "MO":
            MO_markers.push(window['marker'+md.orderID]);
            break;
          case "OH":
            OH_markers.push(window['marker'+md.orderID]);
            break;
          case "OK":
            OK_markers.push(window['marker'+md.orderID]);
            break;
          case "TN":
            TN_markers.push(window['marker'+md.orderID]);
            break;
          case "KS":
            KS_markers.push(window['marker'+md.orderID]);
            break;
          case "MS":
            MS_markers.push(window['marker'+md.orderID]);
            break;
          case "AZ":
            AZ_markers.push(window['marker'+md.orderID]);
            break;
          case "WV":
            WV_markers.push(window['marker'+md.orderID]);
            break;
          case "PA":
            PA_markers.push(window['marker'+md.orderID]);
            break;
          case "MD":
            MD_markers.push(window['marker'+md.orderID]);
            break;
          case "NE":
            NE_markers.push(window['marker'+md.orderID]);
            break;
          default:
            //Do Nothing...
        }
      //Add Marker to global array...
      marker_array.push(window['marker' + md.orderID]);
  
      window['cont' + md.orderID] = document.createElement('div');
  
      var icon = document.createElement("i");
      icon.className = "fa fa-exclamation-triangle";
  
      window['cont' + md.orderID].id = md.orderID;
      window['cont' + md.orderID].innerHTML = '<h1 style="color:black;"><u>' + md.customer + '</u></h1>';
      window['cont' + md.orderID].innerHTML += '<p><b>Invoice#:</b> <b>' + md.orderID + '</b></p>';
      window['cont' + md.orderID].innerHTML += '<p><b>Total Sale:</b>: $' + md.totalSale + '</p>';
      window['cont' + md.orderID].innerHTML += '<p><b>Status</b>: ' + md.orderStatus + '</p>';
      window['cont' + md.orderID].innerHTML += '<p onclick="regeocode(\'' + md.orderID + '\');"><a href="Javascript:" style="color:blue;">Refresh Marker Location</a></p>';
      window['cont' + md.orderID].innerHTML += '<p><b>Address</b>: ' + md.buildingAddress + '</p>';
      window['cont' + md.orderID].innerHTML += '<p><b>City: </b>' + md.buildingCity + '</p>';
      window['cont' + md.orderID].innerHTML += '<p><b>State: </b>' + md.buildingState + '</p>';
      window['cont' + md.orderID].innerHTML += '<p><b>Zip: </b>' + md.buildingZipCode + '</p>';
  
      var btn = document.createElement("button");
      btn.type = "button";
      btn.classList.add("btn");
      btn.classList.add("btn-success");
      btn.setAttribute("onclick", "add_to_route('"+md.orderID+"','order');");
      btn.setAttribute("style", "background:#5cb85c;padding:5px;border-radius:4px;color:white;");
      btn.setAttribute("id", "btn_" + md.orderID);
      if (md.in_use === 'Yes') {
        btn.setAttribute("disabled", true);
      }
      btn.innerHTML = "Add To Route";
      window['cont' + md.orderID].appendChild(btn);
  
  
      window['marker' + md.orderID].addListener('click', function() {
        console.warn('TESTING');
        console.log(window['cont' + md.orderID]);
        infoWindow.setContent(window['cont' + md.orderID]);
        infoWindow.open(map, window['marker' + md.orderID]);
        //$("#btn_" + md.orderID).click(function() {
        document.getElementById('btn_'+md.orderID).addEventListener('click',function(){
          console.log('Before Icon Change');
          window['marker' + md.orderID].setIcon(p_icon); // set image path here...
          console.log('After Icon Change');
          stop_marker_array.push(window['marker' + md.orderID]);
          var al = stop_marker_array.length - 1;
          console.log("After Add Marker Array");
          console.log(stop_marker_array);
          setTimeout(function() {
            window.document.getElementById("repairBTN_" + md.orderID).addEventListener("click", function() {
              window['marker' + md.orderID].setIcon(img); // reset image path here...
              infoWindow.setContent(window['cont' + md.orderID]);
              infoWindow.open(map, window['marker' + md.orderID]);
              document.getElementById("btn_" + md.orderID).disabled = false;
              infoWindow.close();
              stop_marker_array.splice(al, 1);
              console.log("After Remove Marker Array");
              console.log(stop_marker_array);
            });
          }, 500);
          infoWindow.close();
        });//end
      });
  
      $("body").on("click", "#x" + md.orderID, function() {
        document.getElementById("results").style.visibility = "hidden";
        infoWindow.setContent(window['cont' + md.orderID]);
        infoWindow.open(map, window['marker' + md.orderID]);
        restore();
      });
  
    } //End add_map_marker_order() function
  
  
  function add_map_marker_repair(md) {
      if(debug === 'y'){console.warn(md);}
      var point = new google.maps.LatLng(md.lat, md.lng);
      if (md.in_use === 'Yes') {
        img = "//dynicon.com/icon.php?text=Assigned To Route&bg=9D9D9D&tcolor=FFFFFF&i=task-planning";
      } else {
        img = "//dynicon.com/icon.php?text=" + md.totalSale + " -> " + md.contractor + "&bg=" + md.mBG + "&tcolor=" + md.mFC + "&i=wrench";
      }
      if(debug === 'y'){console.log('RID: '+md.orderID);}
      window['rmarker' + md.orderID] = new google.maps.Marker({
        position: point,
        icon: img
      });
      //Add Marker To Spider Mapping...
      oms.addMarker(window['rmarker' + md.orderID]);
      //Add Marker to State array...
      switch (md.buildingState){
          case "IN":
            IN_markers.push(window['rmarker'+md.orderID]);
            break;
          case "IL":
            IL_markers.push(window['rmarker'+md.orderID]);
            break;
          case "TX":
            TX_markers.push(window['rmarker'+md.orderID]);
            break;
          case "AR":
            AR_markers.push(window['rmarker'+md.orderID]);
            break;
          case "KY":
            KY_markers.push(window['rmarker'+md.orderID]);
            break;
          case "MI":
            MI_markers.push(window['rmarker'+md.orderID]);
            break;
          case "MO":
            MO_markers.push(window['rmarker'+md.orderID]);
            break;
          case "OH":
            OH_markers.push(window['rmarker'+md.orderID]);
            break;
          case "OK":
            OK_markers.push(window['rmarker'+md.orderID]);
            break;
          case "TN":
            TN_markers.push(window['rmarker'+md.orderID]);
            break;
          case "KS":
            KS_markers.push(window['rmarker'+md.orderID]);
            break;
          case "MS":
            MS_markers.push(window['rmarker'+md.orderID]);
            break;
          case "AZ":
            AZ_markers.push(window['rmarker'+md.orderID]);
            break;
          case "WV":
            WV_markers.push(window['rmarker'+md.orderID]);
            break;
          case "PA":
            PA_markers.push(window['rmarker'+md.orderID]);
            break;
          case "MD":
            MD_markers.push(window['rmarker'+md.orderID]);
            break;
          case "NE":
            NE_markers.push(window['rmarker'+md.orderID]);
            break;
          default:
            //Do Nothing...
        }
      //Add Marker to global array...
      marker_array.push(window['rmarker' + md.orderID]);
  
      window['rcont' + md.orderID] = document.createElement('div');
  
      var icon = document.createElement("i");
      icon.className = "fa fa-exclamation-triangle";
  
      window['rcont' + md.orderID].id = md.orderID;
      window['rcont' + md.orderID].innerHTML = '<h1 style="color:black;"><u>' + md.customer + '</u></h1>';
      window['rcont' + md.orderID].innerHTML += '<p><b>Invoice#:</b> <b>' + md.orderID + '</b></p>';
      window['rcont' + md.orderID].innerHTML += '<p><b>Total Sale:</b>: $' + md.totalSale + '</p>';
      window['rcont' + md.orderID].innerHTML += '<p><b>Status</b>: ' + md.orderStatus + '</p>';
      window['rcont' + md.orderID].innerHTML += '<p><b>Address</b>: ' + md.buildingAddress + '</p>';
      window['rcont' + md.orderID].innerHTML += '<p><b>City: </b>' + md.buildingCity + '</p>';
      window['rcont' + md.orderID].innerHTML += '<p><b>State: </b>' + md.buildingState + '</p>';
      window['rcont' + md.orderID].innerHTML += '<p><b>Zip: </b>' + md.buildingZipCode + '</p>';
  
      var btn = document.createElement("button");
      btn.type = "button";
      btn.classList.add("btn");
      btn.classList.add("btn-success");
      btn.setAttribute("onclick", "add_to_route('"+md.orderID+"','repair');");
      btn.setAttribute("style", "background:#5cb85c;padding:5px;border-radius:4px;color:white;");
      btn.setAttribute("id", "rbtn_" + md.orderID);
      if (md.in_use === 'Yes') {
        btn.setAttribute("disabled", true);
      }
      btn.innerHTML = "Add To Route";
      window['rcont' + md.orderID].appendChild(btn);
  
  
      window['rmarker' + md.orderID].addListener('click', function() {
        console.warn('TESTING');
        console.log(window['rcont' + md.orderID]);
        infoWindow.setContent(window['rcont' + md.orderID]);
        infoWindow.open(map, window['rmarker' + md.orderID]);
        $("#btn_" + md.orderID).click(function() {
          window['rmarker' + md.orderID].setIcon(p_icon); // set image path here...
          stop_marker_array.push(window['rmarker' + md.orderID]);
          var al = stop_marker_array.length - 1;
          console.log("After Add Marker Array");
          console.log(stop_marker_array);
          setTimeout(function() {
            window.document.getElementById("repairBTN_" + md.orderID).addEventListener("click", function() {
              window['rmarker' + md.orderID].setIcon(img); // reset image path here...
              infoWindow.setContent(window['rcont' + md.orderID]);
              infoWindow.open(map, window['rmarker' + md.orderID]);
              document.getElementById("btn_" + md.orderID).disabled = false;
              infoWindow.close();
              stop_marker_array.splice(al, 1);
              console.log("After Remove Marker Array");
              console.log(stop_marker_array);
            });
          }, 500);
          infoWindow.close();
        });
      });
  
      $("body").on("click", "#x" + md.orderID, function() {
        document.getElementById("results").style.visibility = "hidden";
        infoWindow.setContent(window['rcont' + md.orderID]);
        infoWindow.open(map, window['rmarker' + md.orderID]);
        restore();
      });
  
    } //End add_map_marker_repair() function
  
  
    //*****************************************INSERT REPAIRS LOOPING HERE****************************************//
  
    
    //var urlParams = new URLSearchParams(window.location.search);
    var rid = urlParams.get('edit_route');
    if (window.XMLHttpRequest) {
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
  
        //alert(this.responseText);
        console.log('Get Route Details JSON Response:');
        console.log(this.responseText);
        var q = JSON.parse(this.responseText);
        console.log(q.stop.length);
        var i;
        console.log('STOP LENGTH: ' + q.stop.length);
        for (i = 0; i < q.stop.length; i++) {
          var qq = JSON.parse(q.stop[i]);
          var sid = qq.stop_id;
          console.log('stop ID: ' + sid);
          var smode = qq.stop_type.toLowerCase();
          console.log('Stop Type: ' + smode);
          //Add stops to the main display...
          var div = document.createElement("DIV");
          div.id = 'repair_' + qq.stop_id;
          div.classList.add("alert");
          div.classList.add("alert-warning");
          div.style.padding = '5px';
          //Button
          var btn = document.createElement('button');
          btn.setAttribute("type", "button");
          btn.id = 'repairBTN_' + qq.stop_id;
          btn.classList.add("close");
          btn.classList.add("repairBTN");
          btn.innerHTML = 'x';
          div.appendChild(btn);
          //Paragraph
          var p = document.createElement("p");
          p.innerHTML = '(?) ' + qq.stop_name;
          p.style.fontWeight = 'bold';
          div.appendChild(p);
          //City, State
          var ul = document.createElement("UL");
          var li = document.createElement("LI");
          li.innerHTML = qq.stop_location;
          ul.appendChild(li);
          //Cost
          var li2 = document.createElement("LI");
          li2.innerHTML = 'Cost: ' + qq.stop_cost;
          ul.appendChild(li2);
          div.appendChild(ul);
          document.getElementById('route_main').appendChild(div);
          //Setup array data in order to keep the ORDER...
          stop_id.push(qq.stop_id);
          stop_name.push(qq.stop_name);
          stop_cost.push(qq.stop_cost);
          stop_address.push(qq.stop_address);
          stop_location.push(qq.stop_location);
          if (smode === 'repair') {
            stop_type.push('Repair');
          } else {
            stop_type.push('Order');
          }
          route_total_cost = route_total_cost + Number(qq.stop_cost);
          document.getElementById('route_total').innerHTML = route_total_cost;
          update_map_directions();
  
          add_to_route(sid, smode, 'edit');
        }
  
      }
    }
    xmlhttp.open("GET", "scheduling/php/get-route-details-JSON.php?rid="+rid, true);
    if (rid) {
      xmlhttp.send();
    }
  
    //***************************************This is where lines 791-821 go for debugging purposes**********************//
  
  
    /*---------------------------------------------UPDATE THE MAP DIRECTIONS-----------------------------------------------------------------------------------------------------------*/
    function update_map_directions() {
      var mbURL = 'https://www.google.com/maps/dir/?api=1&origin=' + home_address + '&destination=' + home_address + '&travelmode=driving&waypoints=';
      //var mbURL = 'https://bing.com/maps/default.aspx?rtp=adr.'+home_address;
      for (var ix = 0; ix < stop_address.length; ix++) {
        if (ix > 0) {
          mbURL = mbURL + '%7C';
          //mbURL = mbURL + '~adr.';
        }
        mbURL = mbURL + stop_address[ix];
      }
      window.document.getElementById('mapurl_btn').href = mbURL;
    }
    
    
    /*---------------------------------------------ADD CUSTOM STOP TO THE MAP-----------------------------------------------------------------------------------------------------------*/
        var acs = document.getElementById("acs");
        acs.addEventListener("click", function() {
        var xx = stop_id.length;
        var uid = Math.floor((Math.random() * 999999) + 1);
        var icName = "Custom Stop";
        var icAddress = prompt("Please Enter The Address, City, State, and Zip Code For The Stop:");
        if(icAddress === ''){
          alert("You must enter an address for the stop!");
          return;
        }
        if (window.XMLHttpRequest) {
          // code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
        } else {  // code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
          if (this.readyState==4 && this.status==200) {
           
           //console.log(this.responseText);
           var jd = JSON.parse(this.responseText);
            if(jd.status === "ZERO_RESULTS"){
              //Address Not Good...
              alert("The Address you entered has been deemed invalid by Google. Please Try again...");
              return;
            }else{
              var lat = jd.results[0].geometry.location.lat;
              var lng = jd.results[0].geometry.location.lng;
              var comp = jd.results[0].address_components;
              for(var i = 0; i < comp.length; i++){
                //console.log(comp[i]);
                if(comp[i].types[0] === 'administrative_area_level_1'){
                  var icState = comp[i].long_name;
                }
                if(comp[i].types[0] === 'locality'){
                  var icCity = comp[i].long_name;
                }
              }
              //var icCity = jd.results[0].address_components[3].long_name;
              //var icState = jd.results[0].address_components[6].long_name;
              console.log("LAT: "+lat);
              console.log("LNG: "+lng);
              console.log("City: "+icCity);
              console.log("State: "+icState);
              var icStop_name = prompt('Please enter the name for this stop:');
              if(icStop_name === ''){
                icStop_name = 'Custom Stop '+uid;
              }
              
              //Add Marker for Custom Stop...
              var cpoint = new google.maps.LatLng(lat,lng);
              var cimg = "//marketforceapp.com/icons/icon.php?text=Custom Stop&bg=28F559&tcolor=000000";
              eval("var cmarker"+uid+" = new google.maps.Marker({position: cpoint, map: map, icon: cimg});");
              custom_uids.push(uid);
              eval("custom_markers.push(cmarker"+uid+");");
              
              //Add Stop Info to Route Info and Arrays...
              var div = document.createElement("DIV");
              div.id = 'custom_'+uid;
              div.classList.add("alert");
              div.classList.add("alert-warning");
              div.style.padding = '5px';
              //Button
              var btn = document.createElement('button');
              btn.setAttribute("type","button");
              btn.id = 'customBTN_'+uid;
              btn.classList.add("close");
              btn.classList.add("repairBTN");
              btn.innerHTML = 'x';
              div.appendChild(btn);
              //Paragraph
              var p = document.createElement("p");
              p.innerHTML = '(?) '+icStop_name;
              p.style.fontWeight = 'bold';
              div.appendChild(p);
              //City, State
              var ul = document.createElement("UL");
              var li = document.createElement("LI");
              li.innerHTML = icCity+', '+icState;
              ul.appendChild(li);
              //Cost
              var li2 = document.createElement("LI");
              li2.innerHTML = 'Cost: $0';
              ul.appendChild(li2);
              div.appendChild(ul);
              document.getElementById('route_main').appendChild(div);
    
              stop_id.push(uid);
              stop_name.push(icStop_name);
              stop_cost.push(0);
              stop_address.push(icAddress);
              stop_location.push(icCity+', '+icState);
              stop_type.push('Custom');
                    route_total_cost = route_total_cost + Number(0);
                    document.getElementById('route_total').innerHTML = route_total_cost;
              var mbURL = 'https://www.google.com/maps/dir/?api=1&origin='+home_address+'&destination='+home_address+'&travelmode=driving&waypoints=';
              for( var i = 0; i < stop_address.length; i++){
                if(i > 0){
                  mbURL = mbURL + '%7C';
                }
                mbURL = mbURL + stop_address[i];
              }
                    window.document.getElementById('mapurl_btn').href = mbURL;
              //Setup Removal Listener Script...
              document.getElementById('customBTN_'+uid).addEventListener("click", function(){
                var uidx = custom_uids.indexOf(uid);
                custom_markers[uidx].setMap(null);//Remove Marker...
                console.log('Waypoint '+uid+' was removed from the array');
                stop_id.splice(xx,1);
                console.log(stop_id);
                stop_name.splice(xx,1);
                console.log(stop_name);
                route_total_cost = route_total_cost - 0;
                stop_cost.splice(xx,1);
                stop_address.splice(xx,1);
                stop_location.splice(xx,1);
                stop_type.splice(xx,1);
                document.getElementById('custom_'+uid).remove();
                document.getElementById('route_total').innerHTML = route_total_cost;
                var mbURL = 'https://www.google.com/maps/dir/?api=1&origin='+home_address+'&destination='+home_address+'&travelmode=driving&waypoints=';
                for( var i = 0; i < stop_address.length; i++){
                  if(i > 0){
                    mbURL = mbURL + '%7C';
                  }
                  mbURL = mbURL + stop_address[i];
                }
                window.document.getElementById('mapurl_btn').href = mbURL;
                });
                console.log('Event Lister Added for custom stop#: '+uid);
              
            }
           
          }
        }
        xmlhttp.open("GET","https://maps.googleapis.com/maps/api/geocode/json?address="+icAddress+"&key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg",true);
        xmlhttp.send();
      });
      
  /*---------------------------------------------ENDCUSTOM STOP TO THE MAP-----------------------------------------------------------------------------------------------------------*/
     
    
  /*This is the Map State Layer Show/Hide Scripting & Departure Location Settings-----------------------------------------------------------------------------------------------------*/
  var IN_E = document.querySelector('input[name=IN_CB]');
  IN_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < IN_markers.length; i++){
        IN_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < IN_markers.length; i++){
        IN_markers[i].setMap(null);
      }
    }
  });   
  var IL_E = document.querySelector("input[name=IL_CB]");
  IL_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < IL_markers.length; i++){
        IL_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < IL_markers.length; i++){
        IL_markers[i].setMap(null);
      }
    }
  });    
  var TX_E = document.querySelector("input[name=TX_CB]");
  TX_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < TX_markers.length; i++){
        TX_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < TX_markers.length; i++){
        TX_markers[i].setMap(null);
      }
    }
  });   
  var AR_E = document.querySelector("input[name=AR_CB]");
  AR_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < AR_markers.length; i++){
        AR_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < AR_markers.length; i++){
        AR_markers[i].setMap(null);
      }
    }
  });   
  var KY_E = document.querySelector("input[name=KY_CB]");
  KY_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < KY_markers.length; i++){
        KY_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < KY_markers.length; i++){
        KY_markers[i].setMap(null);
      }
    }
  });   
  var MI_E = document.querySelector("input[name=MI_CB]");
  MI_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < MI_markers.length; i++){
        MI_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < MI_markers.length; i++){
        MI_markers[i].setMap(null);
      }
    }
  });   
  var MO_E = document.querySelector("input[name=MO_CB]");
  MO_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < MO_markers.length; i++){
        MO_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < MO_markers.length; i++){
        MO_markers[i].setMap(null);
      }
    }
  });   
  var OH_E = document.querySelector("input[name=OH_CB]");
  OH_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < OH_markers.length; i++){
        OH_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < OH_markers.length; i++){
        OH_markers[i].setMap(null);
      }
    }
  });   
  var OK_E = document.querySelector("input[name=OK_CB]");
  OK_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < OK_markers.length; i++){
        OK_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < OK_markers.length; i++){
        OK_markers[i].setMap(null);
      }
    }
  });   
  var TN_E = document.querySelector("input[name=TN_CB]");
  TN_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < TN_markers.length; i++){
        TN_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < TN_markers.length; i++){
        TN_markers[i].setMap(null);
      }
    }
  });
  var KS_E = document.querySelector("input[name=KS_CB]");
  KS_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < KS_markers.length; i++){
        KS_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < KS_markers.length; i++){
        KS_markers[i].setMap(null);
      }
    }
  });
  var MS_E = document.querySelector("input[name=MS_CB]");
  MS_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < MS_markers.length; i++){
        MS_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < MS_markers.length; i++){
        MS_markers[i].setMap(null);
      }
    }
  });
  var AZ_E = document.querySelector("input[name=AZ_CB]");
  AZ_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < AZ_markers.length; i++){
        AZ_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < AZ_markers.length; i++){
        AZ_markers[i].setMap(null);
      }
    }
  });
  var WV_E = document.querySelector("input[name=WV_CB]");
  WV_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < WV_markers.length; i++){
        WV_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < WV_markers.length; i++){
        WV_markers[i].setMap(null);
      }
    }
  });
  var PA_E = document.querySelector("input[name=PA_CB]");
  PA_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < PA_markers.length; i++){
        PA_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < PA_markers.length; i++){
        PA_markers[i].setMap(null);
      }
    }
  });
  var MD_E = document.querySelector("input[name=MD_CB]");
  MD_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < MD_markers.length; i++){
        MD_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < MD_markers.length; i++){
        MD_markers[i].setMap(null);
      }
    }
  });
  var NE_E = document.querySelector("input[name=NE_CB]");
  NE_E.addEventListener('change', function() {
    if(this.checked){
      //alert('Checked');
      //Displays all state markers on the map... 
      for (var i = 0; i < NE_markers.length; i++){
        NE_markers[i].setMap(map);
      }
    }else{
      //alert('Not Checked');
      //Hides all state markers on the map...
      for (var i = 0; i < NE_markers.length; i++){
        NE_markers[i].setMap(null);
      }
    }
  });
  
      
      
  document.getElementById('to_OK').addEventListener('click', function() {
  //Set the Home Office Marker to Oklahoma Office
  var OKhpoint = new google.maps.LatLng(35.723101,-95.402094);
  hmarker.setPosition(OKhpoint);
  home_address = OK_home_address;
  update_directions_link();
  document.getElementById('to_IN').style.display = "inline";
  document.getElementById('to_OK').style.display = "none";
  document.getElementById('depart_from').innerHTML = "Oklahoma Office";
  });
      
  document.getElementById('to_IN').addEventListener('click', function() {
  //Set the Home Office Marker to Oklahoma Office
  var INhpoint = new google.maps.LatLng(40.213878,-85.377739);
  hmarker.setPosition(INhpoint);
  home_address = IN_home_address;
  update_directions_link();
  document.getElementById('to_IN').style.display = "none";
  document.getElementById('to_OK').style.display = "inline";
  document.getElementById('depart_from').innerHTML = "Indiana Office";
  });
      
  
  document.getElementById('clear_all_layers').addEventListener('click', function(){
  /*IN_E.checked = false;
  IL_E.checked = false;
  TX_E.checked = false;
  AR_E.checked = false;
  KY_E.checked = false;
  MI_E.checked = false;
  MO_E.checked = false;
  OH_E.checked = false;
  OK_E.checked = false;
  TN_E.checked = false;*/
  //Click Simulation
  IN_E.click();
  IL_E.click();
  TX_E.click();
  AR_E.click();
  KY_E.click();
  MI_E.click();
  MO_E.click();
  OH_E.click();
  OK_E.click();
  TN_E.click();
  KS_E.click();
  MS_E.click();
  AZ_E.click();
  WV_E.click();
  PA_E.click();
  MD_E.click();
  NE_E.click();
  
  });
  
      
  function update_directions_link(){
  var update_mbURL = 'https://www.google.com/maps/dir/?api=1&origin='+home_address+'&destination='+home_address+'&travelmode=driving&waypoints=';
          for( var i = 0; i < stop_address.length; i++){
              if(i > 0){
                  update_mbURL = update_mbURL + '%7C';
              }
              update_mbURL = update_mbURL + stop_address[i];
          }
          document.getElementById('mapurl_btn').href = update_mbURL;
  }
      
  /*This is the end of the Map State Layer Show/Hide Scripting & Departure Location Settings-----------------------------------------------------------------------------------------------------*/
     
    
  /*This is the ROUTE OPTIMIZATION SCRIPT---------------------------------------------------------------------------------------------------------------*/
              
  
  //Listen for the "Optimize Route" button to be clicked...
  document.getElementById('opt_btn').addEventListener('click', function() {
  if(stop_id.length <= 0){
    alert('You have not added any invoices to the route!');
    return;
  }
  calculateAndDisplayRoute(directionsService, directionsDisplay);
  });
      
          
  /*End ROUTE OPTIMIZATION SCRIPT------------------------------------------------------------------------------------------------------------------------*/
      
    
  function restore(){
        document.getElementById("top").innerHTML = '<input id="pac-input" class="controls" type="text" onkeyup="searcher(this.value);" value="" placeholder="Search Box" style="text-align:center; margin:auto;"/>';
      //THIS IS THE START OF THE AUTOCOMPLETE SEGMENT FOR THE DIRECTION AND/OR CUSTOMER LOCATOR CAPABILITIES...
      // Create the search box and link it to the UI element.
      var input = document.getElementById('pac-input');
      var searchBox = new google.maps.places.SearchBox(input);
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      
      // Bias the SearchBox results towards current map's viewport.
      map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
      });
      
      var markers = [];
      // Listen for the event fired when the user selects a prediction and retrieve
      // more details for that place.
      searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();
  
        if (places.length == 0) {
          return;
        }
      
        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];
  
        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
          }
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };
  
          // Create a marker for each place.
          markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }));
  
          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
      });
      //THIS IS THE END OF THE AUTOCOMPLETE SEGMENT FOR THE PREDICTIONS ON RANDOM ADDRESS QUERIES...  
      }
    
    
    
    //THIS IS THE START OF THE AUTOCOMPLETE SEGMENT FOR THE DIRECTION AND/OR CUSTOMER LOCATOR CAPABILITIES...
      // Create the search box and link it to the UI element.
      var input = document.getElementById('pac-input');
      var searchBox = new google.maps.places.SearchBox(input);
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      
      // Bias the SearchBox results towards current map's viewport.
      map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
      });
      
      var markers = [];
      // Listen for the event fired when the user selects a prediction and retrieve
      // more details for that place.
      searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();
  
        if (places.length == 0) {
          return;
        }
      
        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];
  
        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
          }
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };
  
          // Create a marker for each place.
          markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }));
  
          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
      });
      //THIS IS THE END OF THE AUTOCOMPLETE SEGMENT FOR THE PREDICTIONS ON RANDOM ADDRESS QUERIES...  
      
    //}
    document.getElementById('results').style.visibility = 'hidden';
  
  
  /*OUTTER ROUTE OPTIMIZATION SCRIPT-------------------------------------------------------------------------------*/
          function calculateAndDisplayRoute(directionsService, directionsDisplay) {
      directionsDisplay.setMap(map);
      opt_status = true;
      //Hides all markers on the map...
      for (var i = 0; i < marker_array.length; i++){
        marker_array[i].setMap(null);
      }
      //Displays ONLY 
      for (var i = 0; i < stop_marker_array.length; i++){
        stop_marker_array[i].setMap(map);
      }
      //hmarker.setMap(map);
      
      //Switch the Optimize button for the View All Button...
      document.getElementById('opt_btn').style.display = "none";
      document.getElementById('confirmRouteBTN').style.display = "inline";
      document.getElementById('cancelRouteBTN').style.display = "inline";
      //document.getElementById('opt_btn2').style.display = "inline";
      
      //Set Optimize Button Listener
      document.getElementById('opt_btn2').addEventListener('click',function(){
        opt_status = false;
        for (var i = 0; i < marker_array.length; i++){
          marker_array[i].setMap(map);
        }
        document.getElementById('opt_btn2').style.display = "none";
        document.getElementById('opt_btn').style.display = "inline";
        var pt = new google.maps.LatLng(40.213362, -85.377858);
        map.setCenter(pt);
        map.setZoom(5);
        directionsDisplay.setMap(null);
      });
      
  var waypts = [];
              for(var i = 0; i < stop_id.length; i++){
                  waypts.push({
      location: stop_address[i],
      stopover: true
    });
              }
  /*var checkboxArray = document.getElementById('waypoints');
  for (var i = 0; i < checkboxArray.length; i++) {
  if (checkboxArray.options[i].selected) {
    waypts.push({
      location: checkboxArray[i].value,
      stopover: true
    });
  }
  }*/
  
  directionsService.route({
  origin: home_address,
  destination: home_address,
  waypoints: waypts,
  optimizeWaypoints: true,
  travelMode: 'DRIVING'
  }, function(response, status) {
  if (status === 'OK') {
    directionsDisplay.setDirections(response);//Function to draw route on the map...
    directions_response = response;
    var route = response.routes[0];
    waypoint_order = route.waypoint_order;
    console.log("Waypoint Order:");
    console.log(waypoint_order);
    var summaryPanel = document.getElementById('route_main');
    summaryPanel.innerHTML = '';
          //Setup MAPS URL to add link to button
          mURL = 'https://www.google.com/maps/dir/?api=1&origin='+home_address+'&destination='+home_address+'&travelmode=driving&waypoints=';
    // For each route, display summary information.
          var total_yards = 0;
          var total_dist = 0;
    var total_time = 0;
    for (var i = 0; i < route.legs.length; i++) {
              var x = route.waypoint_order[i];
              
              if(i < route.legs.length - 1){
                  if(i > 0){
                      mURL = mURL + '%7C';
                  }
                  mURL = mURL + stop_address[x];
              }
      console.log(mURL);
      var routeSegment = i + 1;
      /*summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
          '</b><br>';
      summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
      summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
      summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
              summaryPanel.innerHTML += route.waypoint_order + '<br>';*/
              total_yards = total_yards + route.legs[i].distance.value;
      total_time = total_time + route.legs[i].duration.value;
      leg_dur.push(route.legs[i].duration.value);
      
              
    }
          //alert(mURL);//ENDING MAP URL...
          window.document.getElementById('mapurl_btn').href = mURL;
          total_yards = total_yards / 1760;//1760 is the number of yards in a mile used for the conversion from yards to miles...
          total_dist = total_yards.toFixed(1);
          document.getElementById('route_miles').innerHTML = total_dist;
    
    //Format the Time
    var fTime = formatTime(total_time);
    document.getElementById('route_time').innerHTML = fTime;
      
          //var x = route.waypoint_order;
          for(var i = 0; i < route.waypoint_order.length; i++){
              var x = route.waypoint_order[i];
      var sid = stop_id[x];
              var snum = i + 1;
              /*summaryPanel.innerHTML += '<div class="alert alert-warning" style="padding:5px;" id="repair_'+sid+'">'+
                                  '<button id="repairBTN2_'+sid+'" type="button" class="close">x</button>'+	
                                                                       '<p><b>('+snum+') '+stop_name[x]+'</b></p>'+
                                                                       '<ul>'+
                                                                           '<li>Muskogee, Oklahoma</li>'+
                                                                           '<li>Cost: $'+stop_cost[x]+'</li>'+
                                                                       '</ul>'+
                                                                   '</div>';*/
    console.log("SID: "+sid);
    var div = document.createElement("DIV");
    div.id = 'repair_'+sid;
    div.classList.add("alert");
    div.classList.add("alert-warning");
    div.style.padding = '5px';
    //Button
    var btn = document.createElement('button');
    btn.setAttribute("type","button");
    btn.id = 'repairBTN_'+sid;
    btn.classList.add("close");
    btn.classList.add("repairBTN");
    btn.innerHTML = 'x';
    //div.appendChild(btn);
    //Paragraph
    var p = document.createElement("p");
    p.innerHTML = '('+snum+') '+stop_name[x];
    p.style.fontWeight = 'bold';
    div.appendChild(p);
    //City, State
    var ul = document.createElement("UL");
    var li = document.createElement("LI");
    li.innerHTML = stop_location[x];
    ul.appendChild(li);
    //Cost
    var li2 = document.createElement("LI");
    li2.innerHTML = 'Cost: $'+stop_cost[x];
    ul.appendChild(li2);
    div.appendChild(ul);
    
    var tt = formatTime(leg_dur[i]);
    var tp = document.createElement("span");
    tp.classList.add("badge");
    tp.innerHTML = "Travel Time: "+tt;
    div.appendChild(tp);
      
    document.getElementById('route_main').appendChild(div);
      
   
      
      
          /*document.getElementById('repairBTN2_'+sid).addEventListener("click", function(){
      console.log('Waypoint '+x+' was removed from the array');
      stop_id.splice(x,1);
      stop_name.splice(x,1);
      route_total_cost = route_total_cost - stop_cost[x];
      stop_cost.splice(x,1);
      stop_address.splice(x,1);
      stop_location.splice(x,1);
      stop_type.splice(x,1);
      document.getElementById('repair_'+sid).remove();
      document.getElementById('route_total').innerHTML = route_total_cost;
      if(opt_status === true){
        calculateAndDisplayRoute(directionsService, directionsDisplay);
      }
      stop_marker_array[x].setIcon
      if(eval("rmarker"+sid)){
          eval("rmarker"+sid).setIcon(["rimg"+sid]); // reset image path here...
          infoWindow.setContent(["rcont"+sid]);
          infoWindow.open(map, ["rmarker"+sid]);
          document.getElementById("rbtn_"+sid).disabled = false;
          infoWindow.close();
      }else if(eval("marker"+sid)){
          eval("marker"+sid).setIcon(["img"+sid]); // reset image path here...
          infoWindow.setContent(["cont"+sid]);
          infoWindow.open(map, ["marker"+sid]);
          document.getElementById("btn_"+sid).disabled = false;
          infoWindow.close();
      }else{
        console.log("Error! ID: "+sid);
        alert("There was an error removing the waypoint!");
      }
    });*/
      
    }
          
  } else {
    window.alert('Directions request failed due to ' + status);
  }
  });
  }		
    
          
  /* END OUTTER OPTIMIZATION SCRIPT ----------------------------------------------------------------------------*/
  
  
  
  
  
  
  } //End of initAutocomplete()