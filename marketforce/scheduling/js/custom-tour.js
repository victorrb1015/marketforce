//localStorage.clear();
//localStorage.removeItem("tour_current_step");
//localStorage.removeItem("tour_end");
// Instance the tour
var tour_template = `<div class="popover tour">
                    <div class="arrow" style="left: 50%;"></div>
                    <h3 class="popover-title" style="background:#b10058 !Important;text-align:center;"></h3>
                    <div class="popover-content"></div>
                    <div class="popover-navigation">
                      <div class="btn-group"> 
                        <button class="btn btn-sm btn-default disabled" data-role="prev" disabled="" tabindex="-1">« Prev</button> 
                        <button class="btn btn-sm btn-default" data-role="next">Next »</button> 
                      </div> 
                      <button class="btn btn-sm btn-default" data-role="end">Close</button>    
                    </div>
                  </div>`;

var tour = new Tour({
  steps: [
    {
      element: "",
      orphan: true,
      placement: "auto",
      title: "New MarketForce Scheduler!",
      content: `<p>MarketForce has been updated to version 3.14.0</p><br>
                <p>In this update, the following changes have been made:</p>
                <ol style="padding:5px;">
                  <li>Updated the Dealer-Lookup page's design</li>
                  <li>Added a "New Client Application" version of the "New Dealer Application"</li>
                </ol>
               `,
      template: tour_template
    }
  ],
  backdrop: true
});

// Initialize the tour
tour.init();

// Start the tour
tour.start();