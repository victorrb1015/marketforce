
function dispatch_route(rid){
  var mode = 'Dispatch';
  var istatus = 'Installing';
  var conf = confirm("Are you sure you want to dispatch this Route?");
  if(!conf){
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      alert(this.responseText);
      exitOK = true;
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","scheduling/php/change-route-status.php?rid="+rid+"&mode="+mode+"&rep_name="+rep_name+"&rep_id="+rep_id+"&istatus="+istatus,true);
  xmlhttp.send();
}


function cancel_dispatch(rid){
  var mode = 'Pending';
  var istatus = 'In Shop';
  var conf = confirm("Are you sure you want to cancel the dispatching of this route?");
  if(!conf){
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      alert(this.responseText);
      exitOK = true;
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","scheduling/php/change-route-status.php?rid="+rid+"&mode="+mode+"&rep_name="+rep_name+"&rep_id="+rep_id+"&istatus="+istatus,true);
  xmlhttp.send();
}


function complete_route(rid){
  document.getElementById('main_row').innerHTML = '';
  /*var mode = 'Complete';
  var istatus = 'Installed';
  var conf = confirm("Are you sure you want to mark this route Complete?");
  if(!conf){
    return;
  }*/
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      var r = JSON.parse(this.responseText);
      debug_r = r;
      document.getElementById('cRouteID').innerHTML = rid;
      document.getElementById('crid').value = rid;
      
      var icount = 0;
      for(var i = 0; i < r.stop.length; i++){
        var rr = JSON.parse(r.stop[i]);
      if(rr.stop_type !== 'Custom'){
      var lcol = document.createElement('div');
      lcol.setAttribute('class','col-6 text-center');
      var rcol = document.createElement('div');
      rcol.setAttribute('class','col-6 text-center');
      var p = document.createElement('h3');
      p.innerHTML = 'INV#: '+rr.stop_inv+' -- '+rr.stop_name;
      lcol.appendChild(p);
      var odiv = document.createElement('div');
      //Completed Option...
      var lbl = document.createElement('label');
      lbl.setAttribute('class','btn btn-success');
      lbl.setAttribute('style','margin:5px;');
      var opt = document.createElement('input');
      opt.setAttribute('type','radio');
      opt.setAttribute('id','complete_'+icount);
      opt.setAttribute('name','options_'+icount);
      opt.setAttribute('value','Completed');
      opt.setAttribute('onclick','coll_select(\'off\','+icount+');');
      opt.setAttribute('required','required');
      var ltext = document.createTextNode('Completed');
      lbl.appendChild(opt);
      lbl.appendChild(ltext);
      odiv.appendChild(lbl);
      //Not Completed Option...
      var lbl = document.createElement('label');
      lbl.setAttribute('class','btn btn-danger');
      lbl.setAttribute('style','margin:5px;');
      var opt = document.createElement('input');
      opt.setAttribute('type','radio');
      opt.setAttribute('id','not_complete_'+icount);
      opt.setAttribute('name','options_'+icount);
      opt.setAttribute('value','Not Completed');
      opt.setAttribute('onclick','coll_select(\'off\','+icount+');');
      opt.setAttribute('required','required');
      var ltext = document.createTextNode('Not Completed');
      lbl.appendChild(opt);
      lbl.appendChild(ltext);
      odiv.appendChild(lbl);
      //Collections Option...
      var lbl = document.createElement('label');
      lbl.setAttribute('class','btn btn-warning');
      lbl.setAttribute('style','margin:5px;');
      var opt = document.createElement('input');
      opt.setAttribute('type','radio');
      opt.setAttribute('id','collections_'+icount);
      opt.setAttribute('name','options_'+icount);
      opt.setAttribute('value','Collections');
      opt.setAttribute('onclick','coll_select(\'on\','+icount+');');
      opt.setAttribute('required','required');
      var ltext = document.createTextNode('Collections');
      lbl.appendChild(opt);
      lbl.appendChild(ltext);
      odiv.appendChild(lbl);
      rcol.appendChild(odiv);
        
      var hi = document.createElement('input');
      hi.setAttribute('type','hidden');
      hi.setAttribute('id','stop_id_'+icount);
      hi.setAttribute('name','stop_id_'+icount);
      hi.setAttribute('value',rr.stop_id);
      lcol.appendChild(hi);
      
      document.getElementById('main_row').appendChild(lcol);
      document.getElementById('main_row').appendChild(rcol);
      var div = document.createElement('div');
      div.setAttribute('class','col-12');
      var sdiv = document.createElement('div');
      sdiv.setAttribute('style','width:75%;text-align:center;margin:auto;');
      sdiv.setAttribute('id','select_'+icount);
      div.appendChild(sdiv);
      var hr = document.createElement('hr');
      div.appendChild(hr);
      document.getElementById('main_row').appendChild(div);
      
      icount++;
      }//End if Custom...
      
      }
      
      document.getElementById('csnum').value = icount;
      
    }
  }
  //xmlhttp.open("GET","scheduling/php/change-route-status.php?rid="+rid+"&mode="+mode+"&rep_name="+rep_name+"&rep_id="+rep_id+"&istatus="+istatus,true);
  xmlhttp.open("GET","scheduling/php/get-route-details-JSON.php?rid="+rid,true);
  xmlhttp.send();
}



function coll_select(mode,row){
  var cs = document.getElementById('select_'+row);
  if(mode === 'off'){
    cs.innerHTML = '';
  }else if(mode === 'on' && cs.innerHTML === ''){
    var s = document.createElement('select');
    s.setAttribute('id','coll_cat_'+row);
    s.setAttribute('name','coll_cat_'+row);
    s.setAttribute('class','form-control');
    s.setAttribute('required','required');
    var o = document.createElement('option');
    o.value = '';
    o.innerHTML = 'Select Collections Category';
    s.appendChild(o);
    var cat_array = ["ck_ca_cc","po","nsf_stop","repair","dealer_pmt","dealer_fin","legal","bli","ezpay","scoggin","double_d","dallen_legacy","rto_national"];
    for(var xx in cat_array){
      var o = document.createElement('option');
      o.value = cat_array[xx];
      o.innerHTML = cat_array[xx];
      s.appendChild(o);
    }
    document.getElementById('select_'+row).appendChild(s);
  }else{
    console.log('There was an error processing your request!');
  }
}



