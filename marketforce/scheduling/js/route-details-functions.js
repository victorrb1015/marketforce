function urlEncode(url) {
  url = url.replace(/&/g, '%26');
  url = url.replace(/#/g, '%23');
  return url;
}

function view_route(rid) {
  //Do nothing...
  console.log("view_route() has been called");

  //Get Route Details...
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {

      document.getElementById('detail_body').innerHTML = this.responseText;
      $(".date").datepicker();

    }
  }
  xmlhttp.open("GET", "scheduling/php/get-route-details.php?rid=" + rid, true);
  xmlhttp.send();
}


function view_inv(oid) {
  //View Invoice for Orders...
  //alert('This function is currently being built and is not yet ready!');
  window.open('scheduling/order-invoice.php?inv=' + oid, '_blank');
}

function view_pl(rid, mode) {
  //View Punch List for Repairs...
  //alert('This function is currently being built and is not yet ready!');
  if (mode === 'paid') {
    window.open('repairs/paid-punch-list.php?id=' + rid, '_blank');
  }
  if (mode === 'owes') {
    window.open('repairs/owes-punch-list.php?id=' + rid, '_blank');
  }
}


function save_route_details() {
  var params = '';
  var rid = document.getElementById('mrid').value;
  params += 'rid=' + rid;
  var iName = document.getElementById('iName').value;
  iName = urlEncode(iName);
  params += '&iName=' + iName;
  var lDate = document.getElementById('lDate').value;
  lDate = urlEncode(lDate);
  params += '&lDate=' + lDate;

  var stop_count = document.getElementById('stop_count').value;
  params += '&stop_count=' + stop_count;
  for (var i = 1; i <= stop_count; i++) {
    //Confirmed...
    var srid = document.getElementById('stop_rid_' + i).value; //Get records invNum...
    params += '&srid_' + i + '=' + srid;
    var iDate = document.getElementById('iDate_' + srid).value;
    params += '&iDate_' + srid + '=' + iDate;
    var notes = document.getElementById('notes_' + srid).value;
    params += '&notes_' + srid + '=' + notes;

    console.log("Form String Parameters:");
    console.log(params);
  }
  params += '&sandbox=false';
  //return;
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {

      //document.getElementById('detail_body').innerHTML = this.responseText;
      alert(this.responseText);
      exitOK = true;
      window.location.reload();

    }
  }
  xmlhttp.open("POST", "scheduling/php/save-route-details.php", true);
  xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xmlhttp.send(params);
}


function remove_repair(id) {
  document.getElementById('repair_' + id).remove();
}


function delete_route(rnum, mode) {
  var confirm_delete = confirm("Are you sure you want to delete?");
  if (!confirm_delete) {
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {

      alert(this.responseText);
      if (mode !== 'edit') {
        //exitOK = true;
        //window.location.reload();
        document.getElementById('route_'+rnum).remove();
      }

    }
  }
  xmlhttp.open("GET", "scheduling/php/delete-route.php?rnum=" + rnum + "&rep_name="+rep_name+"&rep_id="+rep_id, true);
  xmlhttp.send();
}


function edit_mode(rid) {
  var file = window.location.pathname;
  //alert(file);
  window.location = '../../' + file + '?bypass=y&edit_route=' + rid;
}