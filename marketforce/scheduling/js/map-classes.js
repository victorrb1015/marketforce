class marker_data_order {

  constructor(d) {

    //Set OrderID
    this.orderID = d.orderID;
    
    //Set building State
    this.buildingState = d.buildingState;
    switch (this.buildingState) {
      case "IN":
        this.color = "blue";
        this.colorh = "0000FF";
        break;
      case "IL":
        this.color = "brown";
        this.colorh = "A52A2A";
        break;
      case "TX":
        this.color = "darkgreen";
        this.colorh = "006400";
        break;
      case "AR":
        this.color = "green";
        this.colorh = "008000";
        break;
      case "KY":
        this.color = "orange";
        this.colorh = "FFA500";
        break;
      case "MI":
        this.color = "paleblue";
        this.colorh = "BDE4FE";
        break;
      case "MO":
        this.color = "pink";
        this.colorh = "FFC0CB";
        break;
      case "OH":
        this.color = "purple";
        this.colorh = "800080";
        break;
      case "OK":
        this.color = "red";
        this.colorh = "FF0000";
        break;
      case "TN":
        this.color = "yellow";
        this.colorh = "FFFF00";
        break;
      default:
        this.color = "";
        this.colorh = "";
    }
    
    
    //Set Marker Lat & Lng
    this.lat = d.lat;
    this.lng = d.lng;
    
    
    
    //Set Label Colors
    this.mFC = d.mFC;
    this.mBG = d.mBG;
    
    //Set Other Values
    this.totalSale = d.totalSale;
    this.customer = d.customer;
    this.orderStatus = d.orderStatus;
    this.buildingAddress = d.buildingAddress;
    this.buildingCity = d.buildingCity;
    //this.buildingState = d.buildingState;//Already set at above switch
    this.buildingZipCode = d.buildingZipCode;
    

  }//End Constructor
  
  updateLatLng(lat,lng){
    console.log('Updating LatLng on OrderID: '+this.orderID);
    var url = 'scheduling/php/update-latlng.php';
    var params = 'lat='+lat+'&lng='+lng+'&orderID='+this.orderID;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {//Call a function when the state changes.
        if(xhr.readyState == 4 && xhr.status == 200) {
            //Do Something...
          var r = JSON.parse(this.responseText);
          if(r.response === 'GOOD'){
            console.log(r);
            reset_cache();
          }
        }
    }
    xhr.open('POST', url, true);
    //Send the proper header information along with the request
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send(params); 
  }

}//End Class marker_data_order


class marker_data_repair {

  constructor(d) {

    //Set OrderID
    this.orderID = d.ID;
    
    //Set building State
    this.buildingState = d.state;
    switch (this.buildingState) {
      case "IN":
        this.color = "blue";
        this.colorh = "0000FF";
        break;
      case "IL":
        this.color = "brown";
        this.colorh = "A52A2A";
        break;
      case "TX":
        this.color = "darkgreen";
        this.colorh = "006400";
        break;
      case "AR":
        this.color = "green";
        this.colorh = "008000";
        break;
      case "KY":
        this.color = "orange";
        this.colorh = "FFA500";
        break;
      case "MI":
        this.color = "paleblue";
        this.colorh = "BDE4FE";
        break;
      case "MO":
        this.color = "pink";
        this.colorh = "FFC0CB";
        break;
      case "OH":
        this.color = "purple";
        this.colorh = "800080";
        break;
      case "OK":
        this.color = "red";
        this.colorh = "FF0000";
        break;
      case "TN":
        this.color = "yellow";
        this.colorh = "FFFF00";
        break;
      default:
        this.color = "";
        this.colorh = "";
    }
    
    
    //Set Marker Lat & Lng
    this.lat = d.lat;
    this.lng = d.lng;
    
    
    
    //Set Label Colors
    this.mFC = d.mFC;
    this.mBG = d.mBG;
    
    //Set Other Values
    this.contractor = d.contractor_name;
    this.totalSale = d.customer_balance;
    this.customer = d.dname;
    this.orderStatus = d.status;
    this.buildingAddress = d.address;
    this.buildingCity = d.city;
    //this.buildingState = d.buildingState;//Already set at above switch
    this.buildingZipCode = d.zip;
    

  }//End Constructor
  
  updateLatLng(lat,lng){
    console.log('Updating LatLng on RepairID: '+this.orderID);
    var url = 'scheduling/php/update-repair-latlng.php';
    var params = 'lat='+lat+'&lng='+lng+'&orderID='+this.orderID;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {//Call a function when the state changes.
        if(xhr.readyState == 4 && xhr.status == 200) {
            //Do Something...
          var r = JSON.parse(this.responseText);
          if(r.response === 'GOOD'){
            console.log(r);
          }
        }
    }
    xhr.open('POST', url, true);
    //Send the proper header information along with the request
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send(params); 
  }

}//End Class marker_data_repair