//var hhurl = "https://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=truck|bb|Pending|C6EF8C|000000";
var hhurl = "//marketforceapp.com/icons/icon.php?text=Pending&bg=C6EF8C&tcolor=000000";
var hhimg = new Image();
hhimg.src = hhurl;
//Global Variables
var map;
var geocoder;
var infoWindow;
var img;
var order_results;
var repair_results;
var stop_id = [];
var stop_name = [];
var stop_cost = [];
var stop_address = [];
var stop_location = [];
var stop_type = [];
var route_total_cost = 0;
var marker_array = [];
var stop_marker_array = [];
var stop_type = [];
var opt_status;
var home_marker = [];
var waypoint_order = [];
var mURL;
var IN_markers = [];
var IL_markers = [];
var TX_markers = [];
var AR_markers = [];
var KY_markers = [];
var MI_markers = [];
var MO_markers = [];
var OH_markers = [];
var OK_markers = [];
var TN_markers = [];
var KS_markers = [];
var MS_markers = [];
var AZ_markers = [];
var WV_markers = [];
var PA_markers = [];
var MD_markers = [];
var NE_markers = [];
var custom_uids = [];
var custom_markers = [];
var IN_home_address = '2200 N Granville Ave, Muncie, IN 47303';
var OK_home_address = '2520 S 32nd St, Muskogee, OK 74401';
var home_address = IN_home_address;
var urlParams = new URLSearchParams(window.location.search);
var schedule_state = urlParams.get('schedule_state');
var debug = urlParams.get('debug');
if (schedule_state === 'Oklahoma') {
  home_address = OK_home_address;
} else {
  home_address = IN_home_address;
}
console.log('Dispatch Location: ' + home_address);
var leg_dur = [];
var directions_response;

function urlEncode(url) {
  url = url.replace(/&/g, '%26');
  url = url.replace(/#/g, '%23');
  return url;
}

//Format Drive Time...
function formatTime(secs) {
  var sec_num = parseInt(secs, 10)
  var hours = Math.floor(sec_num / 3600) % 24
  var minutes = Math.floor(sec_num / 60) % 60
  var seconds = sec_num % 60
  return [hours, minutes, seconds]
    .map(v => v < 10 ? "0" + v : v)
    .filter((v, i) => v !== "00" || i > 0)
    .join(":")
}


//Confirm exit of page...
window.onbeforeunload = confirmExit;

function confirmExit() {
  var ec = document.getElementById('exitCheck').value;
  if (exitOK === true || ec === 'Yes') {
    exitOK = false;
    document.getElementById('exitCheck').value = 'No';
  } else {
    return "You have attempted to leave this page. Are you sure?";
  }

}


function perform_manual_sync(org_id){
  var conf = confirm("Are you sure you want to begin the manual Order Syncing process? This process can take several minutes!");
  if(conf === false){
    return;
  }
  window.open('/marketforce/scheduling/php/sync-orders.php?org_id='+org_id,'_blank');
}


function cancel_route() {
  exitOK = true;
  window.location = 'scheduling.php?bypass=y';
}



//This is the area for the search functionality...
function searcher(val) {
  var val = val.replace('&', '%26');
  if (val === "") {
    document.getElementById("results").innerHTML = "";
    document.getElementById("results").style.visibility = "hidden";
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById("results").style.visibility = "visible";
      }else if(r.response === 'Error') {
        document.getElementById("results").style.visibility = "visible";
      }else{
        toast_alert('Something Went Wrong...','An error occurred while searching for order results.','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET", "scheduling/php/searcher.php?str=" + val, true);
  xmlhttp.send();

}



function add_note(xid) {
  var rep = '';
  var note = document.getElementById("ta_" + xid).value;
  if (note === '') {
    alert("Please enter a note!");
    return;
  }

  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("ta_" + xid).value = '';
      //alert(xmlhttp.reponseText);
      document.getElementById("btn_" + xid).innerHTML = 'Note Added!';
    }
  }
  xmlhttp.open("GET", "map/php/add-note.php?id=" + xid + "&rep=" + rep + "&note=" + note, true);
  xmlhttp.send();
}

function update_bar(p) {
  document.getElementById("progress_bar").style.width = p + "%";

  if (p === 100 || p === '100') {
    document.getElementById("progress_bar_div").style.visibility = "hidden";
  }
}


function add_to_route(id, mode, type) {
  var xx = stop_id.length;
  //alert(id+", "+mode);
  if (type !== 'edit') {
    if (mode === 'repair') {
      document.getElementById('rbtn_' + id).disabled = true;
    } else {
      document.getElementById('btn_' + id).disabled = true;
    }
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp22 = new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp22 = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp22.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var q = JSON.parse(this.responseText);
      //alert(q);
      console.log(q);

      if (type !== 'edit') {
        var div = document.createElement("DIV");
        div.id = 'repair_' + q.orderID;
        div.classList.add("alert");
        div.classList.add("alert-warning");
        div.style.padding = '5px';
        //Button
        var btn = document.createElement('button');
        btn.setAttribute("type", "button");
        btn.id = 'repairBTN_' + q.orderID;
        btn.classList.add("close");
        btn.classList.add("repairBTN");
        btn.innerHTML = 'x';
        div.appendChild(btn);
        //Paragraph
        var p = document.createElement("p");
        p.innerHTML = '(?) ' + q.customer;
        p.style.fontWeight = 'bold';
        div.appendChild(p);
        //City, State
        var ul = document.createElement("UL");
        var li = document.createElement("LI");
        li.innerHTML = q.buildingCity + ', ' + q.buildingState;
        ul.appendChild(li);
        //Cost
        var li2 = document.createElement("LI");
        li2.innerHTML = 'Cost: ' + q.totalSale;
        ul.appendChild(li2);
        div.appendChild(ul);
        document.getElementById('route_main').appendChild(div);

        console.log(xx);
        stop_id.push(q.orderID);
        stop_name.push(q.customer);
        stop_cost.push(q.totalSale);
        stop_address.push(q.buildingAddress + ' ' + q.buildingCity + ', ' + q.buildingState + ' ' + q.buildingZipCode);
        stop_location.push(q.buildingCity + ', ' + q.buildingState);
        if (mode === 'repair') {
          stop_type.push('Repair');
        } else {
          stop_type.push('Order');
        }
        route_total_cost = route_total_cost + Number(q.totalSale);
        document.getElementById('route_total').innerHTML = route_total_cost;
        var mbURL = 'https://www.google.com/maps/dir/?api=1&origin=' + home_address + '&destination=' + home_address + '&travelmode=driving&waypoints=';
        for (var i = 0; i < stop_address.length; i++) {
          if (i > 0) {
            mbURL = mbURL + '%7C';
          }
          mbURL = mbURL + stop_address[i];
        }
        document.getElementById('mapurl_btn').href = mbURL;
      } //End if Edit...

      //alert(q.orderID);
      //Setup Removal Listener Script...
      document.getElementById('repairBTN_' + q.orderID).addEventListener("click", function() {
        console.log('Waypoint ' + q.orderID + ' was removed from the array');
        stop_id.splice(xx, 1);
        console.log(stop_id);
        stop_name.splice(xx, 1);
        console.log(stop_name);
        route_total_cost = route_total_cost - q.totalSale;
        stop_cost.splice(xx, 1);
        stop_address.splice(xx, 1);
        stop_location.splice(xx, 1);
        stop_type.splice(xx, 1);
        document.getElementById('repair_' + q.orderID).remove();
        document.getElementById('route_total').innerHTML = route_total_cost;
        var mbURL = 'https://www.google.com/maps/dir/?api=1&origin=' + home_address + '&destination=' + home_address + '&travelmode=driving&waypoints=';
        for (var i = 0; i < stop_address.length; i++) {
          if (i > 0) {
            mbURL = mbURL + '%7C';
          }
          mbURL = mbURL + stop_address[i];
        }
        document.getElementById('mapurl_btn').href = mbURL;
      });
      console.log('Event Lister Added for order#: ' + q.orderID);

    }
  }
  xmlhttp22.open("GET", "scheduling/php/fetch-order-info.php?id=" + id + "&mode=" + mode, true);
  xmlhttp22.send();
}


function regeocode(orderID){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200) {
         // Typical action to be performed when the document is ready:
         var r = JSON.parse(this.responseText);
        if(r.response === 'GOOD'){
          toast_alert('Success!',r.message,'bottom-right','success');
          var mi = 0;
          marker_array.forEach(function(m){
            if(m.id === orderID){
              console.log(m);
              console.log('Marker Array Position: '+mi);
              console.log(marker_array[mi]);
              marker_array[mi].setPosition({lat: r.lat, lng: r.lng});
              reset_cache();
            }
            mi++;
          });
          console.log('Loop Complete');
        }else{
          toast_alert('Something Went Wrong...','An error occurred while re-geocoding','bottom-right','error');
        }
      }
  };
  xhttp.open("GET", "scheduling/php/regeocode.php?oid="+orderID, true);
  xhttp.send();
}

function show_map_markers(){
  var mi = 0;
  marker_array.forEach(function(m){
    if(m.id === '75292'){
      console.log(m);
      console.log('Marker Array Position: '+mi);
      console.log(marker_array[mi]);
      marker_array[mi].setPosition({lat: 43.9695, lng: -99.9018});
    }
    mi++;
  });
  //console.log(marker_array[2]);
  //console.log(marker_array[2].id);
}


//paste this code under the head tag or in a separate js file.
// Wait for window load
$(window).load(function() {
  // Animate loader off screen
  $(".se-pre-con").fadeOut("slow");
});