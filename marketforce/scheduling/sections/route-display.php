<h4 class="alert alert-info"><i class="fas fa-route fa-fw"></i> Route Info</h4>

<section id="route_main"></section>
<!--End Route_Main-->

<div class="col-md-12 alert alert-success text-black">
  <p>Total: $<span id="route_total">0</span></p>
  <p>Est. Miles: <span id="route_miles"></span></p>
  <p>Est. Time: <span id="route_time"></span></p>
</div>

<br>

<div style="text-align:center;">
  <button type="button" id="opt_btn" class="btn btn-primary"><i class="fa fa-compass"></i> Optimize Route</button>
  <button type="button" id="opt_btn2" class="btn btn-danger" style="display:none;">View All</button>

  <button type="button" class="btn btn-success" id="confirmRouteBTN" style="display:none;"><i class="fa fa-check"></i> Confirm Route</button>
  <button type="button" class="btn btn-danger" id="cancelRouteBTN" onclick="cancel_route();" style="display:none;"><i class="fa fa-times"></i> Cancel Route</button>

  <a id="mapurl_btn" href="https://www.google.com/maps/dir/?api=1&origin=2200 North Granville Ave. Muncie,IN&destination=2200 North Granville Ave. Muncie,IN&travelmode=driving&waypoints=" target="_blank">
    <button type="button" class="btn btn-warning"><i class="fa fa-print"></i> Print Directions</button>
	</a>

  <input type="hidden" id="rep_id_tag" value="<?php echo $_SESSION['user_id']; ?>" />
  <input type="hidden" id="rep_name_tag" value="<?php echo $_SESSION['full_name']; ?>" />
</div>
<br>