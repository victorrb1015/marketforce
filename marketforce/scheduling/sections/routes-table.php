<?php
	echo '<div class="panel panel-primary card-view">
				<div class="panel-heading">
					<h3 class="panel-title">
						Pending Forms
					</h3>
				</div>
				<div class="panel-body" id="scroll">
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped packet-table">
					<thead>
						<tr>
              <th>Rep</th>
              <th>Date</th>
							<th>Route ID</th>
              <th>Stops</th>
							<th>Installer</th>
							<th>Status</th>
							<th>Notes</th>
							<th>Actions</th>
						</tr>
						</thead>
						<tbody>';
		
		$tdq = "SELECT * FROM `installer_routes` WHERE `inactive` != 'Yes' AND `status` != 'Completed'";
		$tdg = mysqli_query($conn, $tdq) or die($conn->error);
		while($tdr = mysqli_fetch_array($tdg)){
			
			$nsq = "SELECT * FROM `installer_route_orders` WHERE `routeID` = '" . $tdr['routeID'] . "' AND `inactive` != 'Yes'";
			$nsg = mysqli_query($conn, $nsq) or die($conn->error);
			$numStops = mysqli_num_rows($nsg);
      
			echo '<tr id="route_' . $tdr['routeID'] . '">
            <td>' . $tdr['rep_name'] . '</td>
            <td>' . date("m/d/y",strtotime($tdr['date'])) . '</td>
						<td>' . $tdr['routeID'] . '</td>
            <td>' . $numStops . '</td>
						<td>' . $tdr['installer'] . '</td>
						<td>' . $tdr['status'] . '</td>
						<td>' . $tdr['notes'] . '</td>
						<td>
            ';
			
			if($tdr['status'] == 'Pending'){
				echo '<button type="button" class="btn btn-warning btn-xs" onclick="edit_mode(\'' . $tdr['routeID'] . '\');"><i class="fa fa-pencil"></i> Edit</button>
				';
			}
			echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#routeDetails" onclick="view_route(\'' . $tdr['routeID'] . '\');"><i class="fa fa-eye"></i> View</button>
		
			<a href="' . $tdr['directionsURL'] . '" target="_blank">
			<button type="button" class="btn btn-warning btn-xs"><i class="fa fa-print"></i> Print Directions</button>
			</a>';
            
			if($tdr['status'] == 'Pending'){ 
			//echo '<button type="button" class="btn btn-success btn-xs" onclick="dispatch_route(\'' . $tdr['routeID'] . '\');"><i class="fa fa-truck"></i> Dispatch Route</button>
			//';
			}    
			if($tdr['status'] == 'Dispatch'){
			//echo '<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#completeRouteModal" onclick="complete_route(\'' . $tdr['routeID'] . '\');"><i class="fa fa-check"></i> Complete Route</button>
			//';
			//echo '<button type="button" class="btn btn-danger btn-xs" onclick="cancel_dispatch(\'' . $tdr['routeID'] . '\');"><i class="fa fa-times"></i> Cancel Dispatch</button>
			//';
    	}
      
		echo '<button type="button" class="btn btn-danger btn-xs" style="background:black;" onclick="delete_route(\'' . $tdr['routeID'] . '\');"><i class="fa fa-trash"></i> Delete</button>
							</td>';
				
					echo '</tr>';
				
			}
							
		echo '</tbody>
					</table>
					</div>
					</div>
				</div>';
?>