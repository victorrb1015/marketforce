<div>
  <div id="map-top-bar">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#layerModal">Dispatch & Layers</button>
    <button type="button" class="btn btn-success" id="acs"><i class="fa fa-plus"></i> Custom Stop</button>
    <span id="map-loader" class="controls badge badge-white" style="color:black;font-size:18px;"><i class="fas fa-spinner fa-pulse"></i> Loading Orders...</span>
  </div>
    <?php              
     //Count Address issues in Scheduling System...
     $eq = "SELECT
            `salesPortalLinked`.`ID`,
            `salesPortalLinked`.`orderID`,
            `salesPortalLinked`.`orderStatus`,
            `salesPortalLinked`.`archieved`,
            `salesPortalLinked`.`inactive`,
            `portal_relationship`.`lat`,
            `portal_relationship`.`lng`
            FROM `salesPortalLinked`
            LEFT JOIN `portal_relationship`
            ON `salesPortalLinked`.`orderID` = `portal_relationship`.`orderID`
            WHERE 
            `salesPortalLinked`.`orderStatus` = 'In Shop' 
            AND `salesPortalLinked`.`archieved` != 'Yes' 
            AND `salesPortalLinked`.`inactive` != 'Yes'
            AND 
            (`portal_relationship`.`lat` = ''
            OR
            `portal_relationship`.`lat` = 'Error'
            OR
            `portal_relationship`.`lng` = ''
            OR
            `portal_relationship`.`lng` = 'Error')";
     $eg = mysqli_query($conn, $eq) or die($conn->error);
     $ecount = mysqli_num_rows($eg);
     if($ecount > 0){
       echo '<div class="alert alert-danger" role="alert" style="margin-top:3px;margin-bottom:3px;">
               <a href="reports/scheduling/scheduling-error-report.php" target="_blank" class="">
                 <span style="font-weight:bold;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Warning:</span> 
                 <span style="background:red;color:white;padding:5px;border-radius:20px;">' . $ecount . '</span> 
                 Invoices have possible ADDRESS ISSUES and are not displayed on the map!
               </a>
             </div>';
     }
    ?>
  </div>

  