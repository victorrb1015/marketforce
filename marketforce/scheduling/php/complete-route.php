<?php
include '../../php/connection.php';

echo '<html>
      <head>
      <title>Completing Route...</title>
      <style>
      html{
        background:#000000;
        color:white;
      }
      .lds-ring {
        display: inline-block;
        position: relative;
        width: 64px;
        height: 64px;
        margin:auto;
      }
      .lds-ring div {
        box-sizing: border-box;
        display: block;
        position: absolute;
        width: 51px;
        height: 51px;
        margin: 6px;
        border: 6px solid #fff;
        border-radius: 50%;
        animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        border-color: #fff transparent transparent transparent;
      }
      .lds-ring div:nth-child(1) {
        animation-delay: -0.45s;
      }
      .lds-ring div:nth-child(2) {
        animation-delay: -0.3s;
      }
      .lds-ring div:nth-child(3) {
        animation-delay: -0.15s;
      }
      @keyframes lds-ring {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(360deg);
        }
      }
      </style>
      </head>
      <body>
      <br><br><br>
      <h1 style="text-align:center;">Completing Route</h1>
      <h3 style="text-align:center;">Do not exit page while Route is processing...</h3>
      <div style="margin:auto;text-align:center;">
        <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
      ';

//Load Variables...
$crid = $_POST['crid'];
$csnum = $_POST['csnum'];

//Setup SOAP Call Parameters...
$sc = new SoapClient("http://sales.allsteelcarports.com/publicapi/allsteelwebservice.asmx?WSDL");
$apiKey = "33f2ddee-724e-4bd1-9606-4408f5f95bdd";


//Loop Through Stops...
for($i = 0; $i < $csnum; $i++){
  $sid = $_POST['stop_id_'.$i];
  $opt = $_POST['options_'.$i];
  $cat = $_POST['coll_cat_'.$i];
  
  //Get Stop Details...
  $sq = "SELECT * FROM `installer_route_orders` WHERE `inactive` != 'Yes' AND `routeID` = '" . $crid . "' AND `stop_id` = '" . $sid . "'";
  $sg = mysqli_query($conn, $sq) or die($conn->error);
  $sr = mysqli_fetch_array($sg);
  $stop_type = $sr['stop_type'];
  
  switch ($opt){
    case 'Completed':
      
      //Complete Script...
      if($stop_type == 'Repair'){
        $id = $sid;
        $mode = 'Completed';
        $rid = $_SESSION['user_id'];
        $rname = $_SESSION['full_name'];
        $cname = '';
        $rnote = 'Done From Scheduling System';
        $note = 'This repair was marked as NOT DONE by ' . $rname . '. Notes: ' . $rnote;
        include 'repair-handler.php';
      }elseif($stop_type == 'Order'){
        //SoapClient Parameters for the CSKern Web Service API
        $params = array("apiKey" => $apiKey,"invoice" => $sid,"status" => 'Installed');
        //Update the invoice on the CSKern Sales Portal...
        $result = $sc->__soapCall("UpdateOrderStatus", array($params));
        $rx = json_decode($result->UpdateOrderStatusResult);
        if($rx != 'Order was successfully updated'){
          $error = true;
          $CSKernResponse = $rx;
          echo '<p>There was an error updating the status of orderID: ' . $sid . ' to "Installed"</p>';
        }
      }else{
        $error = 1;
        echo '<p>There was a general error processing your request!</p>';
      }
      break;
    case 'Not Completed':
      //Not Complete Script...
      if($stop_type == 'Repair'){
        $ruq = "UPDATE `repairs` SET `in_use` = '' WHERE `ID` = '" . $sid . "'";
        mysqli_query($conn, $ruq) or die($conn->error);
        $id = $sid;
        $mode = 'Not Done';
        $rid = $_SESSION['user_id'];
        $rname = $_SESSION['full_name'];
        $cname = '';
        $rnote = 'Done From Scheduling System';
        $note = 'This repair was marked as NOT DONE by ' . $rname . '. Notes: ' . $rnote;
        include 'repair-handler.php';
      }elseif($stop_type == 'Order'){
        $ouq = "UPDATE `salesPortalLinked` SET `in_use` = '' WHERE `orderID` = '" . $sid . "'";
        mysqli_query($conn, $ouq) or die($conn->error);
        //SoapClient Parameters for the CSKern Web Service API
        $params = array("apiKey" => $apiKey,"invoice" => $sid,"status" => 'In Shop');
        //Update the invoice on the CSKern Sales Portal...
        $result = $sc->__soapCall("UpdateOrderStatus", array($params));
        $rx = json_decode($result->UpdateOrderStatusResult);
        if($rx != 'Order was successfully updated'){
          $error = true;
          $CSKernResponse = $rx;
          echo '<p>There was an error updating the status of orderID: ' . $sid . ' to "In Shop"</p>';
        }
      }else{
        $error = 1;
        echo '<p>There was a general error processing your request!</p>';
      }
      break;
    case 'Collections':
      //Collections Script...
      if($stop_type == 'Repair'){
        $ruq = "UPDATE `repairs` SET `in_use` = 'Collections' WHERE `ID` = '" . $sid . "'";
        mysqli_query($conn, $ruq) or die($conn->error);
        $id = $sid;
        $mode = 'Completed';
        $rid = $_SESSION['user_id'];
        $rname = $_SESSION['full_name'];
        $cname = '';
        $rnote = 'Done From Scheduling System';
        $note = 'This repair was marked as NOT DONE by ' . $rname . '. Notes: ' . $rnote;
        include 'repair-handler.php';
        //Get Repair Info...
        $riq = "SELECT * FROM `repairs` WHERE `ID` = '" . $sid . "'";
        $rig = mysqli_query($conn, $riq) or die($conn->error);
        $rir = mysqli_fetch_array($rig);
        $dname = mysqli_real_escape_string($conn,$rir['dname']);
        //Get Dealer id...
        $diq = "SELECT * FROM `dealers` WHERE `name` = '" . $dname . "' AND `inactive` != 'Yes'";
        $dig = mysqli_query($conn, $diq) or die($conn->error);
        $dir = mysqli_fetch_array($dig);
        $did = $dir['ID'];
        $name = mysqli_real_escape_string($conn, $rir['cname']);
        $state = mysqli_real_escape_string($conn, $rir['state']);
        $phone = mysqli_real_escape_string($conn, $rir['cphone']);
        $email = mysqli_real_escape_string($conn, $rir['cemail']);
        $inv = mysqli_real_escape_string($conn, $rir['inv']);
        $pdate = date("Y-m-d",strtotime($rir['today_date']));
        $sprice = mysqli_real_escape_string($conn, $rir['customer_cost']);
        $tamount = '';
        $crate = '';
        $acoll = '';
        $namount = mysqli_real_escape_string($conn, $rir['customer_cost']);
        $note = 'This Collection Record was initiated from the Scheduler System';
        //Add Invoice to Collections...
        $ncat = $cat;
        include 'add-to-collections.php';
        echo '<p>Invoice: ' . $sid . ' Added to Collections</p>';
      }elseif($stop_type == 'Order'){
        //SoapClient Parameters for the CSKern Web Service API
        $params = array("apiKey" => $apiKey,"invoice" => $sid,"status" => 'Collections');
        //Update the invoice on the CSKern Sales Portal...
        $result = $sc->__soapCall("UpdateOrderStatus", array($params));
        $rx = json_decode($result->UpdateOrderStatusResult);
        if($rx != 'Order was successfully updated'){
          $error = true;
          $CSKernResponse = $rx;
          echo '<p>There was an error updating the status of orderID: ' . $sid . ' to "Installed"</p>';
        }
        //Get Order Info...
        //Update the invoice on the CSKern Sales Portal...
        $params = array("apiKey" => $apiKey,"invoice" => $sid);
        $result = $sc->__soapCall("GetOrdersByInvoice", array($params));
        $rx = json_decode($result->GetOrdersByInvoiceResult);
        $dname = mysqli_real_escape_string($conn,$rx->dealer);
        //Get Dealer id...
        $diq = "SELECT * FROM `dealers` WHERE `name` = '" . mysqli_real_escape_string($dname) . "' AND `inactive` != 'Yes'";
        $dig = mysqli_query($conn, $diq) or die($conn->error);
        if(mysqli_num_rows($dig) > 0){
        $dir = mysqli_fetch_array($dig);
        $did = $dir['ID'];
        }
        $name = mysqli_real_escape_string($conn,$rx->customer);
        $state = mysqli_real_escape_string($conn,$rx->buildingState);
        $phone = mysqli_real_escape_string($conn,$rx->customerPhone1);
        $email = mysqli_real_escape_string($conn,$rx->customerEmail);
        $inv = $sid;
        $pdate = date("Y-m-d",strtotime($rx->invoiceDate));
        $sprice = mysqli_real_escape_string($conn,$rx->totalSale);
        $tamount = mysqli_real_escape_string($conn,$rx->tax);
        $crate = '';
        $acoll = '';
        $namount = mysqli_real_escape_string($conn,$rx->balance);
        $note = 'This Collection Record was initiated from the Scheduler System';
        //Add Invoice to Collections...
        $ncat = $cat;
        include 'add-to-collections.php';
        echo '<p>Invoice: ' . $sid . ' Added to Collections</p>';
      }else{
        $error = 1;
        echo '<p>There was a general error processing your request!</p>';
      }
      break;
    default:
      //Default Error...
      echo '<p>There was a general error processing your request!</p>';
  }
  
}//End Loop through Stops...

$mrq = "UPDATE `installer_routes` SET `status` = 'Completed' WHERE `routeID` = '" . $crid . "'";
mysqli_query($conn, $mrq) or die($conn->error);


echo '</div>
      </body>
        <script>
        setTimeout(function(){
          window.location = "../../scheduling.php";
        },6000);
        </script>
      </html>';


?>