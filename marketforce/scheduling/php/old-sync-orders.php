<?php
if($_SERVER['HTTP_HOST'] == 'sandbox.marketforceapp.com'){
  error_reporting(E_ALL);
}
$dbName = $_GET['dbName'];
$mf_conn = mysqli_connect('localhost','marketfo_mf','#NgTFJQ!z@t8','marketfo_marketforce') or die($mf_conn->error);
if($dbName){
  $conn = mysqli_connect('localhost','marketfo_mf','#NgTFJQ!z@t8',$dbName) or die($conn->error);
}
//include '../../php/connection.php';

$rep_name = $_GET['fn'];
$fn = $_GET['fn'];
$rid = $_GET['edit_route'];

echo '<head>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <!--
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        -->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js" integrity="sha384-u/bQvRA/1bobcXlcEYpsEdFVK/vJs3+T+nXLsBYJthmdBuavHvAW6UsmqO2Gd/F9" crossorigin="anonymous"></script>
        <!-- Custom Fonts -->
        <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <style>
        .sf{
          font-family: "Montserrat";
        }
        </style>
        <script>
        document.origin = "http://marketforceapp.com";
        </script>
      </head>';
//Progress Bar...
echo '<br><br><br>
      <h1 style="color:red;font-weight:bold;text-align:center;">DO NOT LEAVE PAGE DURING LOADING PROCESS! 
      <br> 
      <i class="fa fa-exclamation-triangle" style="color:yellow;background:black;padding:8px;border-radius:30px;"></i> LEAVING MAY RESULT IN DATA CORRUPTION! <i class="fa fa-exclamation-triangle" style="color:yellow;background:black;padding:8px;border-radius:30px;"></i>
      </h1>
      <h2 style="text-align:center;">Initializing...</h2>
      <p class="sf" id="pib" style="text-align:center;"></p>
      <div class="progress" style="margin:auto;width:90%;">
        <div id="pb" class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      <br>
      <p class="sf" id="statusfeed" style="text-align:center;"></p>
      <p class="sf" id="geocodeStatus" style="text-align:center;"></p>';

//Make all invoices archieved...
$aq = "UPDATE `salesPortalLinked` SET `archieved` = 'Yes' WHERE `inactive` != 'Yes'";
mysqli_query($conn, $aq) or die($conn->error);

//CSKern SOAP CALL...
  $sc = new SoapClient("http://sales.allsteelcarports.com/publicapi/allsteelwebservice.asmx?WSDL");
  //$sc = new SoapClient($_SESSION['org_sales_portal_api_url']);
  $apiKey = "33f2ddee-724e-4bd1-9606-4408f5f95bdd";
  $status = 'In Shop';

  //SoapClient Parameters for the CSKern Web Service API
    $params = array(
      "apiKey" => $apiKey,
      "status" => $status
    );
  
    $result = $sc->__soapCall("GetOrdersByStatus", array($params));
    $rx = json_decode($result->GetOrdersByStatusResult);
    $nr = count($rx);

if($_SERVER['HTTP_HOST'] == 'marketforceapp.com'){
  //Log Activity
  $textfile = '../activity-log.txt';
  $oldContents = file_get_contents($textfile);
  $log = fopen("../activity-log.txt","w");
  fwrite($log, date("m/d/y h:iA") . " --> " . $rep_name . "\n");
  fwrite($log, "Initialized the Scheduler Loading Process (" . $nr . " Orders)" . "\n");
  fwrite($log, "---------------------------------------------------------------------------------------------\n");
  fwrite($log, $oldContents);
  fclose($log);
}



$i = 1;
  foreach($rx as $x){
    $changed = false;
    $orderID = mysqli_real_escape_string($conn, $x->orderID);
    $cname = mysqli_real_escape_string($conn, $x->customer);
    $address = mysqli_real_escape_string($conn, $x->buildingAddress);
    $city = mysqli_real_escape_string($conn, $x->buildingCity);
    $state = mysqli_real_escape_string($conn, $x->buildingState);
    $zip = mysqli_real_escape_string($conn, $x->buildingZipCode);
    
    //Check if invoice is in Database...
    $cq = "SELECT * FROM `salesPortalLinked` WHERE `orderID` = '" . $x->orderID . "'";
    $cg = mysqli_query($conn, $cq) or die($conn->error);
    if(mysqli_num_rows($cg) > 0){
      $r = mysqli_fetch_array($cg);
      //echo '<p>Order: ' . $x->orderID . ' has been updated!</p>';
      echo '<script>
            document.getElementById("statusfeed").innerHTML = "Order: ' . $x->orderID . ' is being updated in the Database";
          </script>';
      
      //Check if any location information has changed in the Sales Portal...
      if(mysqli_real_escape_string($conn,$r['buildingAddress']) != $address){
        $changed = true;
      }
      if(mysqli_real_escape_string($conn,$r['buildingCity']) != $city){
        $changed = true;
      }
      if(mysqli_real_escape_string($conn,$r['buildingState']) != $state){
        $changed = true;
      }
      if(mysqli_real_escape_string($conn,$r['buildingZipCode']) != $zip){
        $changed = true;
      }
      $gq = "SELECT * FROM `portal_relationship` WHERE `orderID` = '" . $orderID . "'";
      $gg = mysqli_query($conn, $gq) or die($conn->error);
      $gnr = mysqli_num_rows($gg);
      if($changed == true || $gnr <= 0){
        
        //Geocode the address...
      echo '<script>
      var address = "' . mysqli_real_escape_string($conn,str_replace("#","",$address)) . ',' . mysqli_real_escape_string($conn,$city) . ',' . $state . ',' . $zip . '";
      if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp' . $orderID . '=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp' . $orderID . '=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp' . $orderID . '.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
    
      var jd = JSON.parse(xmlhttp' . $orderID . '.responseText);
      
      //Send Error Report & Support Ticket...
      if(jd.status === "ZERO_RESULTS"){
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp12' . $orderID . '=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp12' . $orderID . '=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp12' . $orderID . '.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
    }
  }
  xmlhttp12' . $orderID . '.open("GET","../../scheduling/php/cron.php?error=true&name=' . $cname . '&id=' . $orderID . '&address=' . mysqli_real_escape_string($conn,str_replace("#","",$address)) . '&city=' . mysqli_real_escape_string($conn,$city) . '&state=' . $state . '&zip=' . $zip . '",true);
  xmlhttp12' . $orderID . '.withCredentials = true;
  xmlhttp12' . $orderID . '.send();
    }//End Error Report IF...
    
    var lat = jd.results[0].geometry.location.lat;
    var lng = jd.results[0].geometry.location.lng;
    
    var id = "' . $orderID . '";
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp2' . $orderID . '=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp2' . $orderID . '=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp2' . $orderID . '.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("geocodeStatus").innerHTML = xmlhttp2' . $orderID . '.responseText;
      console.log(xmlhttp2' . $orderID . ');
      //window.location.reload();
    }
  }
  xmlhttp2' . $orderID . '.open("GET","../../scheduling/php/insertlatlng.php?id="+id+"&lat="+lat+"&lng="+lng,true);
  xmlhttp2' . $orderID . '.withCredentials = true;
  xmlhttp2' . $orderID . '.send();
    }
  }
  xmlhttp' . $orderID . '.open("GET","https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg",true);
  xmlhttp' . $orderID . '.send();
  
  </script>';
        
      }//End if $changed == true...
      
      
      
      
      $uq = "UPDATE `salesPortalLinked` SET 
            `orderID` = '" . mysqli_real_escape_string($conn, $x->orderID) . "',
            `orderStatus` = '" . mysqli_real_escape_string($conn, $x->orderStatus) . "',
            `invoiceDate` = '" . mysqli_real_escape_string($conn, $x->invoiceDate) . "',
            `dealer` = '" . mysqli_real_escape_string($conn, $x->dealer) . "',
            `customer` = '" . mysqli_real_escape_string($conn, $x->customer) . "',
            `customerAddress` = '" . mysqli_real_escape_string($conn, $x->customerAddress) . "',
            `customerCity` = '" . mysqli_real_escape_string($conn, $x->customerCity) . "',
            `customerState` = '" . mysqli_real_escape_string($conn, $x->customerState) . "',
            `customerZipCode` = '" . mysqli_real_escape_string($conn, $x->customerZipCode) . "',
            `customerPhone1` = '" . mysqli_real_escape_string($conn, $x->customerPhone1) . "',
            `customerPhone2` = '" . mysqli_real_escape_string($conn, $x->customerPhone2) . "',
            `customerPhone3` = '" . mysqli_real_escape_string($conn, $x->customerPhone3) . "',
            `buildingAddress` = '" . mysqli_real_escape_string($conn, $x->buildingAddress) . "',
            `buildingCity` = '" . mysqli_real_escape_string($conn, $x->buildingCity) . "',
            `buildingState` = '" . mysqli_real_escape_string($conn, $x->buildingState) . "',
            `buildingZipCode` = '" . mysqli_real_escape_string($conn, $x->buildingZipCode) . "',
            `options1` = '" . mysqli_real_escape_string($conn, $x->options1) . "',
            `options2` = '" . mysqli_real_escape_string($conn, $x->options2) . "',
            `options3` = '" . mysqli_real_escape_string($conn, $x->options3) . "',
            `options4` = '" . mysqli_real_escape_string($conn, $x->options4) . "',
            `options5` = '" . mysqli_real_escape_string($conn, $x->options5) . "',
            `options6` = '" . mysqli_real_escape_string($conn, $x->options6) . "',
            `options7` = '" . mysqli_real_escape_string($conn, $x->options7) . "',
            `options8` = '" . mysqli_real_escape_string($conn, $x->options8) . "',
            `options9` = '" . mysqli_real_escape_string($conn, $x->options9) . "',
            `price` = '" . mysqli_real_escape_string($conn, $x->price) . "',
            `price1` = '" . mysqli_real_escape_string($conn, $x->price1) . "',
            `price2` = '" . mysqli_real_escape_string($conn, $x->price2) . "',
            `price3` = '" . mysqli_real_escape_string($conn, $x->price3) . "',
            `price4` = '" . mysqli_real_escape_string($conn, $x->price4) . "',
            `price5` = '" . mysqli_real_escape_string($conn, $x->price5) . "',
            `price6` = '" . mysqli_real_escape_string($conn, $x->price6) . "',
            `price7` = '" . mysqli_real_escape_string($conn, $x->price7) . "',
            `price8` = '" . mysqli_real_escape_string($conn, $x->price8) . "',
            `price9` = '" . mysqli_real_escape_string($conn, $x->price9) . "',
            `totalSale` = '" . mysqli_real_escape_string($conn, $x->totalSale) . "',
            `tax` = '" . mysqli_real_escape_string($conn, $x->tax) . "',
            `total` = '" . mysqli_real_escape_string($conn, $x->total) . "',
            `tenPercentDep` = '" . mysqli_real_escape_string($conn, $x->tenPercentDep) . "',
            `fiftyPercentDip` = '" . mysqli_real_escape_string($conn, $x->fiftyPercentDip) . "',
            `balance` = '" . mysqli_real_escape_string($conn, $x->balance) . "',
            `taxExemptNum` = '" . mysqli_real_escape_string($conn, $x->taxExemptNum) . "',
            `instructions` = '" . mysqli_real_escape_string($conn, $x->instructions) . "',
            `description` = '" . mysqli_real_escape_string($conn, $x->description) . "',
            `feesDescription` = '" . mysqli_real_escape_string($conn, $x->feesDescription) . "',
            `fees` = '" . mysqli_real_escape_string($conn, $x->fees) . "',
            `colorDescription` = '" . mysqli_real_escape_string($conn, $x->colorDescription) . "',
            `sidesDescription` = '" . mysqli_real_escape_string($conn, $x->sidesDescription) . "',
            `sidesPrice` = '" . mysqli_real_escape_string($conn, $x->sidesPrice) . "',
            `trimDescription` = '" . mysqli_real_escape_string($conn, $x->trimDescription) . "',
            `trimPrice` = '" . mysqli_real_escape_string($conn, $x->trimPrice) . "',
            `width` = '" . mysqli_real_escape_string($conn, $x->width) . "',
            `roof` = '" . mysqli_real_escape_string($conn, $x->roof) . "',
            `frame` = '" . mysqli_real_escape_string($conn, $x->frame) . "',
            `leg` = '" . mysqli_real_escape_string($conn, $x->leg) . "',
            `gauge` = '" . mysqli_real_escape_string($conn, $x->gauge) . "',
            `ground` = '" . mysqli_real_escape_string($conn, $x->ground) . "',
            `cement` = '" . mysqli_real_escape_string($conn, $x->cement) . "',
            `asphalt` = '" . mysqli_real_escape_string($conn, $x->asphalt) . "',
            `other` = '" . mysqli_real_escape_string($conn, $x->other) . "',
            `txtOther` = '" . mysqli_real_escape_string($conn, $x->txtOther) . "',
            `landlevelNo` = '" . mysqli_real_escape_string($conn, $x->landlevelNo) . "',
            `landlevelYes` = '" . mysqli_real_escape_string($conn, $x->landlevelYes) . "',
            `electricityYes` = '" . mysqli_real_escape_string($conn, $x->electricityYes) . "',
            `electricityNo` = '" . mysqli_real_escape_string($conn, $x->electricityNo) . "',
            `cash` = '" . mysqli_real_escape_string($conn, $x->cash) . "',
            `cCheck` = '" . mysqli_real_escape_string($conn, $x->cCheck) . "',
            `cc` = '" . mysqli_real_escape_string($conn, $x->cc) . "',
            `po` = '" . mysqli_real_escape_string($conn, $x->po) . "',
            `endsDescription` = '" . mysqli_real_escape_string($conn, $x->endsDescription) . "',
            `endsPrice` = '" . mysqli_real_escape_string($conn, $x->endsPrice) . "',
            `regFrame` = '" . mysqli_real_escape_string($conn, $x->regFrame) . "',
            `aFrame` = '" . mysqli_real_escape_string($conn, $x->aFrame) . "',
            `vRoof` = '" . mysqli_real_escape_string($conn, $x->vRoof) . "',
            `dealerPhone` = '" . mysqli_real_escape_string($conn, $x->dealerPhone) . "',
            `allVertical` = '" . mysqli_real_escape_string($conn, $x->allVertical) . "',
            `customerEmail` = '" . mysqli_real_escape_string($conn, $x->customerEmail) . "',
            `archieved` = 'No'
            WHERE `orderID` = '" . mysqli_real_escape_string($conn, $x->orderID) . "'";
          try{
            mysqli_query($conn, $uq) or die($conn->error);
          }catch(mysqli_sql_exception $e){
            throw $e;
          }
          
          
    }else{
      echo '<script>
            document.getElementById("statusfeed").innerHTML = "Order: ' . $x->orderID . ' is being inserted into the Database";
          </script>';
      
      //Geocode the address...
      echo '<script>
      var address = "' . mysqli_real_escape_string($conn,str_replace("#","",$address)) . ',' . mysqli_real_escape_string($conn,$city) . ',' . $state . ',' . $zip . '";
      if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp' . $orderID . '=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp' . $orderID . '=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp' . $orderID . '.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
    
      var jd = JSON.parse(xmlhttp' . $orderID . '.responseText);
      
      //Send Error Report & Support Ticket...
      if(jd.status === "ZERO_RESULTS"){
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp12' . $orderID . '=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp12' . $orderID . '=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp12' . $orderID . '.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
    }
  }
  xmlhttp12' . $orderID . '.open("GET","../../scheduling/php/cron.php?error=true&name=' . $cname . '&id=' . $orderID . '&address=' . mysqli_real_escape_string($conn,str_replace("#","",$address)) . '&city=' . mysqli_real_escape_string($conn,$city) . '&state=' . $state . '&zip=' . $zip . '",true);
  xmlhttp12' . $orderID . '.withCredentials = true;
  xmlhttp12' . $orderID . '.send();
    }//End Error Report IF...
    
    var lat = jd.results[0].geometry.location.lat;
    var lng = jd.results[0].geometry.location.lng;
    
    var id = "' . $orderID . '";
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp2' . $orderID . '=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp2' . $orderID . '=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp2' . $orderID . '.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("geocodeStatus").innerHTML = xmlhttp2' . $orderID . '.responseText;
      console.log(xmlhttp2' . $orderID . ');
      //window.location.reload();
    }
  }
  xmlhttp2' . $orderID . '.open("GET","../../scheduling/php/insertlatlng.php?id="+id+"&lat="+lat+"&lng="+lng,true);
  xmlhttp2' . $orderID . '.withCredentials = true;
  xmlhttp2' . $orderID . '.send();
    }
  }
  xmlhttp' . $orderID . '.open("GET","https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg",true);
  xmlhttp' . $orderID . '.send();
  
  </script>';
      
      $q = "INSERT INTO `salesPortalLinked`
            (
            `orderID`,
            `orderStatus`,
            `invoiceDate`,
            `dealer`,
            `customer`,
            `customerAddress`,
            `customerCity`,
            `customerState`,
            `customerZipCode`,
            `customerPhone1`,
            `customerPhone2`,
            `customerPhone3`,
            `buildingAddress`,
            `buildingCity`,
            `buildingState`,
            `buildingZipCode`,
            `options1`,
            `options2`,
            `options3`,
            `options4`,
            `options5`,
            `options6`,
            `options7`,
            `options8`,
            `options9`,
            `price`,
            `price1`,
            `price2`,
            `price3`,
            `price4`,
            `price5`,
            `price6`,
            `price7`,
            `price8`,
            `price9`,
            `totalSale`,
            `tax`,
            `total`,
            `tenPercentDep`,
            `fiftyPercentDip`,
            `balance`,
            `taxExemptNum`,
            `instructions`,
            `description`,
            `feesDescription`,
            `fees`,
            `colorDescription`,
            `sidesDescription`,
            `sidesPrice`,
            `trimDescription`,
            `trimPrice`,
            `width`,
            `roof`,
            `frame`,
            `leg`,
            `gauge`,
            `ground`,
            `cement`,
            `asphalt`,
            `other`,
            `txtOther`,
            `landlevelNo`,
            `landlevelYes`,
            `electricityYes`,
            `electricityNo`,
            `cash`,
            `cCheck`,
            `cc`,
            `po`,
            `endsDescription`,
            `endsPrice`,
            `regFrame`,
            `aFrame`,
            `vRoof`,
            `dealerPhone`,
            `allVertical`,
            `customerEmail`
            )
            VALUES
            (
            '" . mysqli_real_escape_string($conn, $x->orderID) . "',
            '" . mysqli_real_escape_string($conn, $x->orderStatus) . "',
            '" . mysqli_real_escape_string($conn, $x->invoiceDate) . "',
            '" . mysqli_real_escape_string($conn, $x->dealer) . "',
            '" . mysqli_real_escape_string($conn, $x->customer) . "',
            '" . mysqli_real_escape_string($conn, $x->customerAddress) . "',
            '" . mysqli_real_escape_string($conn, $x->customerCity) . "',
            '" . mysqli_real_escape_string($conn, $x->customerState) . "',
            '" . mysqli_real_escape_string($conn, $x->customerZipCode) . "',
            '" . mysqli_real_escape_string($conn, $x->customerPhone1) . "',
            '" . mysqli_real_escape_string($conn, $x->customerPhone2) . "',
            '" . mysqli_real_escape_string($conn, $x->customerPhone3) . "',
            '" . mysqli_real_escape_string($conn, $x->buildingAddress) . "',
            '" . mysqli_real_escape_string($conn, $x->buildingCity) . "',
            '" . mysqli_real_escape_string($conn, $x->buildingState) . "',
            '" . mysqli_real_escape_string($conn, $x->buildingZipCode) . "',
            '" . mysqli_real_escape_string($conn, $x->options1) . "',
            '" . mysqli_real_escape_string($conn, $x->options2) . "',
            '" . mysqli_real_escape_string($conn, $x->options3) . "',
            '" . mysqli_real_escape_string($conn, $x->options4) . "',
            '" . mysqli_real_escape_string($conn, $x->options5) . "',
            '" . mysqli_real_escape_string($conn, $x->options6) . "',
            '" . mysqli_real_escape_string($conn, $x->options7) . "',
            '" . mysqli_real_escape_string($conn, $x->options8) . "',
            '" . mysqli_real_escape_string($conn, $x->options9) . "',
            '" . mysqli_real_escape_string($conn, $x->price) . "',
            '" . mysqli_real_escape_string($conn, $x->price1) . "',
            '" . mysqli_real_escape_string($conn, $x->price2) . "',
            '" . mysqli_real_escape_string($conn, $x->price3) . "',
            '" . mysqli_real_escape_string($conn, $x->price4) . "',
            '" . mysqli_real_escape_string($conn, $x->price5) . "',
            '" . mysqli_real_escape_string($conn, $x->price6) . "',
            '" . mysqli_real_escape_string($conn, $x->price7) . "',
            '" . mysqli_real_escape_string($conn, $x->price8) . "',
            '" . mysqli_real_escape_string($conn, $x->price9) . "',
            '" . mysqli_real_escape_string($conn, $x->totalSale) . "',
            '" . mysqli_real_escape_string($conn, $x->tax) . "',
            '" . mysqli_real_escape_string($conn, $x->total) . "',
            '" . mysqli_real_escape_string($conn, $x->tenPercentDep) . "',
            '" . mysqli_real_escape_string($conn, $x->fiftyPercentDip) . "',
            '" . mysqli_real_escape_string($conn, $x->balance) . "',
            '" . mysqli_real_escape_string($conn, $x->taxExemptNum) . "',
            '" . mysqli_real_escape_string($conn, $x->instructions) . "',
            '" . mysqli_real_escape_string($conn, $x->description) . "',
            '" . mysqli_real_escape_string($conn, $x->feesDescription) . "',
            '" . mysqli_real_escape_string($conn, $x->fees) . "',
            '" . mysqli_real_escape_string($conn, $x->colorDescription) . "',
            '" . mysqli_real_escape_string($conn, $x->sidesDescription) . "',
            '" . mysqli_real_escape_string($conn, $x->sidesPrice) . "',
            '" . mysqli_real_escape_string($conn, $x->trimDescription) . "',
            '" . mysqli_real_escape_string($conn, $x->trimPrice) . "',
            '" . mysqli_real_escape_string($conn, $x->width) . "',
            '" . mysqli_real_escape_string($conn, $x->roof) . "',
            '" . mysqli_real_escape_string($conn, $x->frame) . "',
            '" . mysqli_real_escape_string($conn, $x->leg) . "',
            '" . mysqli_real_escape_string($conn, $x->gauge) . "',
            '" . mysqli_real_escape_string($conn, $x->ground) . "',
            '" . mysqli_real_escape_string($conn, $x->cement) . "',
            '" . mysqli_real_escape_string($conn, $x->asphalt) . "',
            '" . mysqli_real_escape_string($conn, $x->other) . "',
            '" . mysqli_real_escape_string($conn, $x->txtOther) . "',
            '" . mysqli_real_escape_string($conn, $x->landlevelNo) . "',
            '" . mysqli_real_escape_string($conn, $x->landlevelYes) . "',
            '" . mysqli_real_escape_string($conn, $x->electricityYes) . "',
            '" . mysqli_real_escape_string($conn, $x->electricityNo) . "',
            '" . mysqli_real_escape_string($conn, $x->cash) . "',
            '" . mysqli_real_escape_string($conn, $x->cCheck) . "',
            '" . mysqli_real_escape_string($conn, $x->cc) . "',
            '" . mysqli_real_escape_string($conn, $x->po) . "',
            '" . mysqli_real_escape_string($conn, $x->endsDescription) . "',
            '" . mysqli_real_escape_string($conn, $x->endsPrice) . "',
            '" . mysqli_real_escape_string($conn, $x->regFrame) . "',
            '" . mysqli_real_escape_string($conn, $x->aFrame) . "',
            '" . mysqli_real_escape_string($conn, $x->vRoof) . "',
            '" . mysqli_real_escape_string($conn, $x->dealerPhone) . "',
            '" . mysqli_real_escape_string($conn, $x->allVertical) . "',
            '" . mysqli_real_escape_string($conn, $x->customerEmail) . "'
            )";
       try{
            mysqli_query($conn, $q) or die($conn->error);
          }catch(mysqli_sql_exception $e){
            throw $e;
          }
      
      
      
    }
    
    
    //echo $i . ' = ' . $x->orderID . ' => ' . $x->customer . '<br>';
    $progress = $i / $nr * 100;
    echo '<script>
            //console.log("' . $progress . '");
            document.getElementById("pb").style.width = "' . $progress . '%";
            document.getElementById("pib").innerHTML = "' . number_format($i) . ' / ' . number_format($nr) . ' Orders Loaded";
          </script>';
    $i++;
    //echo $progress;
  }
  
  

echo '<script>
        setTimeout(function(){
        window.location = "../../map/scheduling-map.php?xx=' . rand() . '&fn=' . $fn . '&edit_route=' . $rid . '";
        },3000);
      </script>';

//End Of PHP Script...
?>