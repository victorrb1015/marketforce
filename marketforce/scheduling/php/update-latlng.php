<?php
header('Content-Type: application/json');
error_reporting(0);
include '../../php/connection.php';

//Load Variables...
$orderID = $_REQUEST['orderID'];
$lat = $_REQUEST['lat'];
$lng = $_REQUEST['lng'];


#Main Functions...
$q = "SELECT * FROM `portal_relationship` WHERE `inactive` != 'Yes' AND `orderID` = '" . $orderID . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
if(mysqli_num_rows($g) > 0){
  $uq = "UPDATE `portal_relationship` SET `date` = CURRENT_DATE, `lat` = '" . $lat . "', `lng` = '" . $lng . "' WHERE `inactive` != 'Yes' AND `orderID` = '" . $orderID . "'";
  mysqli_query($conn, $uq) or die($conn->error);
  $x->response = 'GOOD';
  $x->message = 'Geocoding Updated for OrderID: ' . $orderID;
}else{
  $iq = "INSERT INTO `portal_relationship` 
        (
        `orderID`,
        `date`,
        `lat`,
        `lng`
        )
        VALUES
        (
        '" . $orderID . "',
        CURRENT_DATE,
        '" . $lat . "',
        '" . $lng . "' 
        )";
  mysqli_query($conn, $iq) or die($conn->error);
  $x->response = 'GOOD';
  $x->message = 'Geocoding Added for OrderID: ' . $orderID;
}


$x->orderID = $orderID;
$x->lat = $lat;
$x->lng = $lng;


//Setup Response Output...
$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;