<?php
include '../../php/connection.php';

$error = false;

//Load Variables...
$rnum = $_GET['rnum'];
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];

//Setup SOAP Call Parameters...
  $sc = new SoapClient("http://sales.allsteelcarports.com/publicapi/allsteelwebservice.asmx?WSDL");
  $apiKey = "33f2ddee-724e-4bd1-9606-4408f5f95bdd";



//Get Route Details....
$rq = "SELECT * FROM `installer_routes` WHERE `routeID` = '" . $rnum . "'";
$rg = mysqli_query($conn, $rq) or die($conn->error);
if(mysqli_num_rows($rg) > 0){
  $rr = mysqli_fetch_array($rg);
  
  $dq = "SELECT * FROM `installer_route_orders` WHERE `routeID` = '" . $rnum . "'";
  $dg = mysqli_query($conn, $dq) or die($conn->error);
  while($dr = mysqli_fetch_array($dg)){
    //Set invoice in_use status to ""...
    if($dr['stop_type'] == 'Repair'){
      //Update Repairs database...
      $ruq = "UPDATE `repairs` SET `in_use` = '' WHERE `ID` = '" . $dr['stop_id'] . "'";
      mysqli_query($conn, $ruq) or die($conn->error);
      //Update installer_route_orders database...
      $iuq = "UPDATE `installer_route_orders` SET `inactive` = 'Yes' WHERE `ID` = '" . $dr['ID'] . "'";
      mysqli_query($conn, $iuq) or die($conn->error);
      
    }elseif($dr['stop_type'] == 'Order'){
      //Update salesPortalLinked database...
      $ouq = "UPDATE `salesPortalLinked` SET `in_use` = '' WHERE `orderID` = '" . $dr['stop_id'] . "'";
      mysqli_query($conn, $ouq) or die($conn->error);
      //Update installer_route_orders database...
      $iuq = "UPDATE `installer_route_orders` SET `inactive` = 'Yes' WHERE `ID` = '" . $dr['ID'] . "'";
      mysqli_query($conn, $iuq) or die($conn->error);
      
          
            /*$invoice = $dr['stop_id'];
            //SoapClient Parameters for the CSKern Web Service API
              $params = array(
                "apiKey" => $apiKey,
                "invoice" => $invoice,
                "status" => 'In Shop'
              );
          
            //Update the invoice on the CSKern Sales Portal...
              $result = $sc->__soapCall("UpdateOrderStatus", array($params));
              //$r = json_decode($result->GetOrdersByInvoiceResult);
              $rx = json_decode($result->UpdateOrderStatusResult);
            
            if($rx != 'Order was successfully updated'){
              $error = true;
              $CSKernResponse = $rx;
            }*/
      
    }elseif($dr['stop_type'] == 'Custom'){
      //Update installer_route_orders database...
      $iuq = "UPDATE `installer_route_orders` SET `inactive` = 'Yes' WHERE `ID` = '" . $dr['ID'] . "'";
      mysqli_query($conn, $iuq) or die($conn->error);
    }else{
      echo 'The Order Stop Type was not Found! Error Code: 32921';
    }
    
  }
  //Update installer_routes database...
  $uq = "UPDATE `installer_routes` SET `inactive` = 'Yes' WHERE `ID` = '" . $rr['ID'] . "'";
  mysqli_query($conn, $uq) or die($conn->error);
  
  
  echo 'RouteID: ' . $rnum . ' has been deleted!';
  if($error == true){
    echo ' ' . $CSKernResponse;
  }else{
    $CSKernResponse = $rx;
  }
  
//Log Activity
$textfile = '../activity-log.txt';
$oldContents = file_get_contents($textfile);
$log = fopen("../activity-log.txt","w");
fwrite($log, date("m/d/y h:iA") . " --> " . $rep_name . "\n");
fwrite($log, "Deleted RouteID: " . $rnum . "\n");
fwrite($log, "CSKern Portal Update Notes: " . $CSKernResponse . "\n");
fwrite($log, "---------------------------------------------------------------------------------------------\n");
fwrite($log, $oldContents);
fclose($log);
  
}else{
  echo 'RouteID Not Found! Error Code: 62852';
}