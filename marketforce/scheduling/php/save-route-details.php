<?php
include '../../php/connection.php';

//Load Variables
$rid = mysqli_real_escape_string($conn, $_POST['rid']);
$iName = mysqli_real_escape_string($conn, $_POST['iName']);
$rlDate = mysqli_real_escape_string($conn, $_POST['lDate']);
$lDate = date('Y-m-d',strtotime($rlDate));
$stop_count = mysqli_real_escape_string($conn, $_POST['stop_count']);


//Get Previous Details...
$pq = "SELECT * FROM `installer_routes` WHERE `routeID` = '" . $rid . "'";
$pg = mysqli_query($conn, $pq) or die($conn->error);
$pr = mysqli_fetch_array($pg);
$pinstaller = mysqli_real_escape_string($conn,$pr['installer']);
$plDate = date('Y-m-d',strtotime($pr['lDate']));

//Log Activity
$textfile = '../activity-log.txt';
$oldContents = file_get_contents($textfile);
$log = fopen("../activity-log.txt","w");
fwrite($log, date("m/d/y h:iA") . " --> " . $rep_name . "\n");
fwrite($log, "Saved Details of RouteID: " . $rid . "\n");
fwrite($log, "Changed Installer Name from (" . $pinstaller . ") to (" . $iName . ")" . "\n");
fwrite($log, "Changed Load Date from (" . $plDate . ") to (" . $lDate . ")" . "\n");


//Update installer_routes...
$mq = "UPDATE `installer_routes` SET `installer` = '" . $iName . "', `lDate` = '" . $lDate . "' WHERE `routeID` = '" . $rid . "'";
mysqli_query($conn, $mq) or die($conn->error);

//Update each route stop in installer_route_orders...
$i = 1;
while($i <= $stop_count){
  $srid = mysqli_real_escape_string($conn, $_POST['srid_'.$i]);
  $riDate = mysqli_real_escape_string($conn, $_POST['iDate_'.$srid]);
  $iDate = date('Y-m-d',strtotime($riDate));
  $notes = mysqli_real_escape_string($conn, $_POST['notes_'.$srid]);
  
  //Get Previous...
  $psq = "SELECT * FROM `installer_route_orders` WHERE `routeID` = '" . $rid . "' AND `stop_id` = '" . $srid . "'";
  $psg = mysqli_query($conn, $psq) or die($conn->error);
  $psr = mysqli_fetch_array($psg);
  $piDate = date('Y-m-d',strtotime($psr['iDate']));
  $pnotes = mysqli_real_escape_string($conn, $psr['notes']);
  
  $rq = "UPDATE `installer_route_orders` SET 
          `iDate` = '" . $iDate . "',
          `notes` = '" . $notes . "'
          WHERE 
          `stop_id` = '" . $srid . "'
          AND
          `routeID` = '" . $rid . "'";
  mysqli_query($conn, $rq) or die($conn->error);
  
//Log Activity  
fwrite($log, "Changes for Stop_ID: " . $srid . "\n");
fwrite($log, "*--Changed Install Date from (" . $piDate . ") to (" . $iDate . ")" . "\n");
fwrite($log, "*--Changed Notes from (" . $pnotes . ") to (" . $notes . ")" . "\n");

  
  $i++;
}

echo 'Route: ' . $rid . ' Details have been saved!';

//Log Activity
fwrite($log, "---------------------------------------------------------------------------------------------\n");
fwrite($log, $oldContents);
fclose($log);

?>