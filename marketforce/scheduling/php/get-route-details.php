<?php
include '../../php/connection.php';

//Load Variables...
$rid = $_GET['rid'];

//Get Route Info...
$q = "SELECT * FROM `installer_routes` WHERE `routeID` = '" . $rid . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
//$wo = json_decode($r['waypoint_order']);
$wo = $r['waypoint_order'];
$wo = str_replace('[', '', $wo);
$wo = str_replace(']', '', $wo);
//print_r($wo);

  echo '<section class="top-sheet-container">'; 
  echo '<input type="hidden" id="mrid" value="' . $rid . '" />';   
  echo '<h4 class="text-center">RouteID: ' . $rid . '</h4>';
  echo '<div class="row">';

//Installer Name
  echo '<div class="col-md-4 col-lg-4 col-xs-4" style="background-color:black;">
          <p class="" style="color:rgb(255,255,255);">Installer Name:</p>
          <input type="text" id="iName" placeholder="Installer Name" class="form-control" value="' . $r['installer'] . '" style="margin:15px 0px 10px;">
        </div>';

//Load Date...
  echo '<div class="col-md-4 col-lg-4 col-xs-4" style="background-color:black;">
          <p class="" style="color:rgb(255,255,255);">Load Date:</p>
          <input type="text" id="lDate" placeholder="Load Date" class="form-control date" value="' . $r['lDate'] . '" style="margin:15px 0px 10px;">
        </div>';

//Rep:
  echo '<div class="col-md-4 col-lg-4 col-xs-4" style="background-color:black;">
          <p class="" style="color:rgb(255,255,255);">Rep Name:</p>
          <input type="text" id="repName" placeholder="Rep Name" class="form-control" value="' . $r['rep_name'] . '" style="margin:15px 0px 10px;" readonly="readonly">
        </div>';

  echo '<br><hr><br>
       ';            

  echo '<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
        <table style="width:100%;">
          <thead>
            <tr>
              <th>ORDERS/DATE</th>
              <th>NAME/ADDRESS</th>
              <th>SIZE & OPTIONS</th>
              <th>NOTES</th>
            </tr>
          </thead>';


$aq = "SELECT * FROM `installer_route_orders` WHERE `routeID` = '" . $rid . "' ORDER BY FIELD(`waypoint_number`," . $wo . ")";
$ag = mysqli_query($conn, $aq) or die($conn->error);
$i = 1;
while($ar = mysqli_fetch_array($ag)){
  
  if($ar['stop_type'] == 'Repair'){
    $rq = "SELECT * FROM `repairs` WHERE `ID` = '" . $ar['stop_id'] . "'";
    $rg = mysqli_query($conn, $rq) or die($conn->error);
    $rr = mysqli_fetch_array($rg);
    $invNum = $rr['inv'];
    $si = $ar['stop_id'];
    if($rr['customer_balance'] > 0 || $rr['customer_cost'] > 0){
      $mode = 'owes';
    }else{
      $mode = 'paid';
    }
  }elseif($ar['stop_type'] == 'Order'){
    $invNum = $ar['stop_id'];
    $si = $ar['stop_id'];
    $oq = "SELECT * FROM `salesPortalLinked` WHERE `orderID` = '" . $invNum . "'";
    $og = mysqli_query($conn, $oq) or die($conn->error);
    $or = mysqli_fetch_array($og);
    $dname = mysqli_real_escape_string($conn,$or['dealer']);
    
    //CSKern SOAP CALL...
  $sc = new SoapClient("http://sales.allsteelcarports.com/publicapi/allsteelwebservice.asmx?WSDL");
  $apiKey = "33f2ddee-724e-4bd1-9606-4408f5f95bdd";
  $invoice = $invNum;

  //SoapClient Parameters for the CSKern Web Service API
    $params = array(
      "apiKey" => $apiKey,
      "invoice" => $invoice
    );

    $result = $sc->__soapCall("GetOrdersByInvoice", array($params));
    $rx = json_decode($result->GetOrdersByInvoiceResult);
    $rr = $rx[0];
  }else{
    
  }
  
  
  echo '<tr>
          <td>
            <p class="text-uppercase text-center" style="color:red;"><strong>Stop ' . $i . '</strong></p>
            <input type="text" class="form-control date" id="iDate_' . $invNum . '" value="' . $ar['iDate'] . '"><br>';
          if($ar['stop_type'] == 'Order'){
            echo '<b><p class="text-center"><a href="http://marketforceapp.com/marketforce/scheduling/beta-order-invoice.php?inv=' . $invNum . '" style="color:black;" target="_blank">#' . $invNum . '</a></p></b>';
          }elseif($ar['stop_type'] == 'Repair'){
            echo '<b><p class="text-center"><a href="Javascript:view_pl(' . $si . ',\'' . $mode . '\')" style="color:black;">#' . $invNum . '</a></p><br>REPAIR</b>';
          }else{
            echo '<b>CUSTOM STOP</b>';
          }
    echo '</td>
          <td contenteditable>
            <p class="text-center"><b>' . $ar['stop_name'] . '</b></p>';
                          if($rr->customerPhone1 != ''){
                            echo '<p class="text-center">Customer Phone: ' . $rr->customerPhone1 . '</p>';
                          }
                          if($rr->customerPhone2 != ''){
                            echo '<p class="text-center">Alt Customer Phone: ' . $rr->customerPhone2 . '</p>';
                          }
                          if($rr->customerPhone3 != ''){
                            echo '<p class="text-center">Alt Customer Phone: ' . $rr->customerPhone3 . '</p>';
                          }
                          if($rr->customerEmail != ''){
                            echo '<p class="text-center">Customer Email: ' . $rr->customerEmail . '</p>';
                          }
  if($ar['stop_type'] == 'Custom'){    
      echo '<p class="text-center">' . $ar['stop_location'] . '</p>';
  }else{
      echo '<p class="text-center">' . $rr->buildingCity . ', ' . $rr->buildingState . '</p>';
  }
      echo '</td>
          <td contenteditable>';
          if($ar['stop_type'] != 'Custom'){
            //Carport Type...
             $sType = '';
             if($rr->refFrame == 'True'){
               if($sType != ''){
                 $sType .= '/';
               }
               $sType .= 'REGULAR';
             }
             if($rr->aFrame == 'True'){
               if($sType != ''){
                 $sType .= '/';
               }
               $sType .= 'A-FRAME';
             }
             if($rr->vRoof == 'True'){
               if($sType != ''){
                 $sType .= '/';
               }
               $sType .= 'VERTICAL ROOF';
             }
             if($rr->allVertical == 'True'){
               if($sType != ''){
                 $sType .= '/';
               }
               $sType .= 'ALL VERTICAL';
             }
             echo '<p class="text-center"><b>' . $sType . '</b></p>';
  
            if($rr->options1 != ''){
              echo '<p class="text-center">' . $rr->options1 . '</p>';
            }
            if($rr->options2 != ''){
              echo '<p class="text-center">' . $rr->options2 . '</p>';
            }
            if($rr->options3 != ''){
              echo '<p class="text-center">' . $rr->options3 . '</p>';
            }
            if($rr->options4 != ''){
              echo '<p class="text-center">' . $rr->options4 . '</p>';
            }
            if($rr->options5 != ''){
              echo '<p class="text-center">' . $rr->options5 . '</p>';
            }
            if($rr->options6 != ''){
              echo '<p class="text-center">' . $rr->options6 . '</p>';
            }
            if($rr->options7 != ''){
              echo '<p class="text-center">' . $rr->options7 . '</p>';
            }
            if($rr->options8 != ''){
              echo '<p class="text-center">' . $rr->options8 . '</p>';
            }
            if($rr->options9 != ''){
              echo '<p class="text-center">' . $rr->options9 . '</p>';
            }
            //Building Dimensions...
            echo '<p class="text-center">' . ($rr->width + 0) . '\'x' . ($rr->roof + 0) . '\' ' . ($rr->frame + 0) . '\'OG ' . ($rr->leg + 0) . '\' ' . ($rr->gauge + 0) . 'GA</p>';
      }//END if Custom...
    echo '</td>
          <td>
            <textarea class="form-control" id="notes_' . $invNum . '" style="width:150px;min-height:100px;">' . $ar['notes'] . '</textarea>
            <input type="hidden" id="stop_rid_' . $i . '" value="' . $invNum . '">
          </td>
        </tr>';
               
  $i++;
  
}

echo '</tbody>
      </table>
      </div>
      </div>';
echo '<input type="hidden" id="stop_count" value="' . ($i - 1) . '" />';
echo '</section>';

?>