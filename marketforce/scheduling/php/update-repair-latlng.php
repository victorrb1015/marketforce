<?php
header('Content-Type: application/json');
error_reporting(0);
include '../../php/connection.php';

//Load Variables...
$orderID = $_REQUEST['orderID'];
$lat = $_REQUEST['lat'];
$lng = $_REQUEST['lng'];

#Main Functions...
$q = "SELECT * FROM `repairs` WHERE `inactive` != 'Yes' AND `ID` = '" . $orderID . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
if(mysqli_num_rows($g) > 0){
$uq = "UPDATE `repairs` SET `lat` = '" . $lat . "', `lng` = '" . $lng . "' WHERE `inactive` != 'Yes' AND `ID` = '" . $orderID . "'";
  mysqli_query($conn, $uq) or die($conn->error);
  $x->response = 'GOOD';
  $x->message = 'Geocoding Updated for RepairID: ' . $orderID;
}else{
  $x->response = 'ERROR';
  $x->message = 'RepairID: ' . $orderID . ' does not exist.';
}

$x->orderID = $orderID;
$x->lat = $lat;
$x->lng = $lng;

//Setup Response Output...
$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;