<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
try {
    $org_id = $_GET['org_id'];
    $mysqli = new mysqli('localhost', 'marketfo_mf', '#NgTFJQ!z@t8', 'marketfo_marketforce');
    /* verificar la conexión */
    if (mysqli_connect_errno()) {
        printf("Falló la conexión failed: %s\n", $mysqli->connect_error);
        exit();
    }
    $q = "SELECT * FROM `organizations` WHERE `org_id` = '" . $org_id . "'";
    $g = $mysqli->query($q);
    $r = $g->fetch_array();
    $org_sales_portal_api_url = $r['sales_portal_api_url'];
    $dbName = $r['db_name'];
    $conn = new mysqli('localhost', 'marketfo_mf', '#NgTFJQ!z@t8', $dbName);
    $aq = "UPDATE `salesPortalLinked` SET `archieved` = 'Yes' WHERE `inactive` != 'Yes'";
    $x2 = $conn->query($aq);
//CSKern SOAP CALL...
//$sc = new SoapClient("http://sales.allsteelcarports.com/publicapi/allsteelwebservice.asmx?WSDL");

    $sc = new SoapClient('https:' . $org_sales_portal_api_url);
    $apiKey = "33f2ddee-724e-4bd1-9606-4408f5f95bdd";
    $status = 'In Shop';

//SoapClient Parameters for the CSKern Web Service API
    $params = array(
        "apiKey" => $apiKey,
        "status" => $status
    );

    $result = $sc->__soapCall("GetOrdersByStatus", array($params));
    $rx = json_decode($result->GetOrdersByStatusResult);
    $nr = count($rx);
    foreach ($rx as $x) {
        $changed = false;
        $orderID = $conn->real_escape_string($x->orderID);
        $cname = $conn->real_escape_string($x->customer);
        $address = $conn->real_escape_string($x->buildingAddress);
        $city = $conn->real_escape_string($x->buildingCity);
        $state = $conn->real_escape_string($x->buildingState);
        $zip = $conn->real_escape_string($x->buildingZipCode);

        //Check if invoice is in Database...
        $cq = "SELECT * FROM `salesPortalLinked` WHERE `orderID` = '" . $orderID . "'";
        $cg = $conn->query($cq);
        if ($cg->num_rows > 0) {
            $r = $cg->fetch_array();

            //Check if any location information has changed in the Sales Portal...
            if ($address !== $conn->real_escape_string($r['buildingAddress'])) {
                $changed = true;
            }
            if ($conn->real_escape_string($r['buildingCity']) !== $city) {
                $changed = true;
            }
            if ($conn->real_escape_string($r['buildingState']) !== $state) {
                $changed = true;
            }
            if ($conn->real_escape_string($r['buildingZipCode']) !== $zip) {
                $changed = true;
            }
            //$gq = "SELECT * FROM `portal_relationship` WHERE `orderID` = '" . $orderID . "'";
            //$gg = mysqli_query($conn, $gq) or die($conn->error);
            //$gnr = mysqli_num_rows($gg);
            //if($changed == true || $gnr <= 0){
            if ($changed) {
                $uq = "UPDATE `salesPortalLinked` SET 
              `orderID` = '" . $conn->real_escape_string($x->orderID) . "',
              `orderStatus` = '" . $conn->real_escape_string($x->orderStatus) . "',
              `invoiceDate` = '" . $conn->real_escape_string($x->invoiceDate) . "',
              `dealer` = '" . $conn->real_escape_string($x->dealer) . "',
              `customer` = '" . $conn->real_escape_string($x->customer) . "',
              `customerAddress` = '" . $conn->real_escape_string($x->customerAddress) . "',
              `customerCity` = '" . $conn->real_escape_string($x->customerCity) . "',
              `customerState` = '" . $conn->real_escape_string($x->customerState) . "',
              `customerZipCode` = '" . $conn->real_escape_string($x->customerZipCode) . "',
              `customerPhone1` = '" . $conn->real_escape_string($x->customerPhone1) . "',
              `customerPhone2` = '" . $conn->real_escape_string($x->customerPhone2) . "',
              `customerPhone3` = '" . $conn->real_escape_string($x->customerPhone3) . "',
              `buildingAddress` = '" . $conn->real_escape_string($x->buildingAddress) . "',
              `buildingCity` = '" . $conn->real_escape_string($x->buildingCity) . "',
              `buildingState` = '" . $conn->real_escape_string($x->buildingState) . "',
              `buildingZipCode` = '" . $conn->real_escape_string($x->buildingZipCode) . "',
              `options1` = '" . $conn->real_escape_string($x->options1) . "',
              `options2` = '" . $conn->real_escape_string($x->options2) . "',
              `options3` = '" . $conn->real_escape_string($x->options3) . "',
              `options4` = '" . $conn->real_escape_string($x->options4) . "',
              `options5` = '" . $conn->real_escape_string($x->options5) . "',
              `options6` = '" . $conn->real_escape_string($x->options6) . "',
              `options7` = '" . $conn->real_escape_string($x->options7) . "',
              `options8` = '" . $conn->real_escape_string($x->options8) . "',
              `options9` = '" . $conn->real_escape_string($x->options9) . "',
              `price` = '" . $conn->real_escape_string($x->price) . "',
              `price1` = '" . $conn->real_escape_string($x->price1) . "',
              `price2` = '" . $conn->real_escape_string($x->price2) . "',
              `price3` = '" . $conn->real_escape_string($x->price3) . "',
              `price4` = '" . $conn->real_escape_string($x->price4) . "',
              `price5` = '" . $conn->real_escape_string($x->price5) . "',
              `price6` = '" . $conn->real_escape_string($x->price6) . "',
              `price7` = '" . $conn->real_escape_string($x->price7) . "',
              `price8` = '" . $conn->real_escape_string($x->price8) . "',
              `price9` = '" . $conn->real_escape_string($x->price9) . "',
              `totalSale` = '" . $conn->real_escape_string($x->totalSale) . "',
              `tax` = '" . $conn->real_escape_string($x->tax) . "',
              `total` = '" . $conn->real_escape_string($x->total) . "',
              `tenPercentDep` = '" . $conn->real_escape_string($x->tenPercentDep) . "',
              `fiftyPercentDip` = '" . $conn->real_escape_string($x->fiftyPercentDip) . "',
              `balance` = '" . $conn->real_escape_string($x->balance) . "',
              `taxExemptNum` = '" . $conn->real_escape_string($x->taxExemptNum) . "',
              `instructions` = '" . $conn->real_escape_string($x->instructions) . "',
              `description` = '" . $conn->real_escape_string($x->description) . "',
              `feesDescription` = '" . $conn->real_escape_string($x->feesDescription) . "',
              `fees` = '" . $conn->real_escape_string($x->fees) . "',
              `colorDescription` = '" . $conn->real_escape_string($x->colorDescription) . "',
              `sidesDescription` = '" . $conn->real_escape_string($x->sidesDescription) . "',
              `sidesPrice` = '" . $conn->real_escape_string($x->sidesPrice) . "',
              `trimDescription` = '" . $conn->real_escape_string($x->trimDescription) . "',
              `trimPrice` = '" . $conn->real_escape_string($x->trimPrice) . "',
              `width` = '" . $conn->real_escape_string($x->width) . "',
              `roof` = '" . $conn->real_escape_string($x->roof) . "',
              `frame` = '" . $conn->real_escape_string($x->frame) . "',
              `leg` = '" . $conn->real_escape_string($x->leg) . "',
              `gauge` = '" . $conn->real_escape_string($x->gauge) . "',
              `ground` = '" . $conn->real_escape_string($x->ground) . "',
              `cement` = '" . $conn->real_escape_string($x->cement) . "',
              `asphalt` = '" . $conn->real_escape_string($x->asphalt) . "',
              `other` = '" . $conn->real_escape_string($x->other) . "',
              `txtOther` = '" . $conn->real_escape_string($x->txtOther) . "',
              `landlevelNo` = '" . $conn->real_escape_string($x->landlevelNo) . "',
              `landlevelYes` = '" . $conn->real_escape_string($x->landlevelYes) . "',
              `electricityYes` = '" . $conn->real_escape_string($x->electricityYes) . "',
              `electricityNo` = '" . $conn->real_escape_string($x->electricityNo) . "',
              `cash` = '" . $conn->real_escape_string($x->cash) . "',
              `cCheck` = '" . $conn->real_escape_string($x->cCheck) . "',
              `cc` = '" . $conn->real_escape_string($x->cc) . "',
              `po` = '" . $conn->real_escape_string($x->po) . "',
              `endsDescription` = '" . $conn->real_escape_string($x->endsDescription) . "',
              `endsPrice` = '" . $conn->real_escape_string($x->endsPrice) . "',
              `regFrame` = '" . $conn->real_escape_string($x->regFrame) . "',
              `aFrame` = '" . $conn->real_escape_string($x->aFrame) . "',
              `vRoof` = '" . $conn->real_escape_string($x->vRoof) . "',
              `dealerPhone` = '" . $conn->real_escape_string($x->dealerPhone) . "',
              `allVertical` = '" . $conn->real_escape_string($x->allVertical) . "',
              `customerEmail` = '" . $conn->real_escape_string($x->customerEmail) . "',
              `archieved` = 'No',
              `ìnactive` = 'No',
              WHERE `orderID` = '" . $conn->real_escape_string($x->orderID) . "'";
                $xes = $conn->query($uq);
                $select = "SELECT * FROM `portal_relationship` WHERE orderID = '" . $orderID . "'";
                //echo $select . "<br/>";
                $resultado = $conn->query($select);
                $date = date("Y-m-d");
                $xesr = $resultado->fetch_array();
                if ($resultado->num_rows < 1) {
                    $addressURL = $conn->real_escape_string($x->buildingAddress) . " , " . $conn->real_escape_string($x->buildingCity) . " , " . $conn->real_escape_string($x->buildingState) . " , " . $conn->real_escape_string($x->buildingZipCode);
                    //echo $addressURL . "<br/>";
                    $Url = "https://maps.googleapis.com/maps/api/geocode/json?";
                    $datos = [
                        "address" => $addressURL,
                        "key" => "AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg"
                    ];
                    $datos = http_build_query($datos);
                    $Url .= $datos;
                    $aContext = array("http" =>
                        array(
                            "method" => "GET",
                            "header" =>
                                'Accept: application/json' . "\r\n" .
                                'Content-Type: application/json' . "\r\n",
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                        )
                    );
                    $cxContext = stream_context_create($aContext);
                    $sFile = file_get_contents($Url, false, $cxContext);
                    $jData = json_decode(json_encode($sFile), true);
                    $jdata = json_decode($jData);
                    if (strlen($jdata->status) > 3) {
                        $lat = "Error";
                        $lng = "Error";
                    } else {
                        $lat = $jdata->results[0]->geometry->location->lat;
                        $lng = $jdata->results[0]->geometry->location->lng;
                    }
                    if (strlen($lat) > 0 && strlen($lng) > 0) {
                        $insert = "INSERT INTO `portal_relationship` (`orderID`, `lat`, `lng`, `date`) VALUES ('" . $orderID . "', '" . $lat . "', '" . $lng . "', '" . $date . "')";
                        $conn->query($insert);
                    }
                    $insert = "INSERT INTO `portal_relationship` (`orderID`, `lat`, `lng`, `date`) VALUES ('" + $orderID + "', 'Error', 'Error', '" + $date + "')";
                    //$insert = "INSERT INTO `portal_relationship` (`orderID`, `lat`, `lng`) VALUES ('" . $orderID . "', '0', '0')";
                    $conn->query($insert);
                } else {
                    $addressURL = $conn->real_escape_string($x->buildingAddress) . " , " . $conn->real_escape_string($x->buildingCity) . " , " . $conn->real_escape_string($x->buildingState) . " , " . $conn->real_escape_string($x->buildingZipCode);
                    //echo $addressURL . "<br/>";
                    $Url = "https://maps.googleapis.com/maps/api/geocode/json?";
                    $datos = [
                        "address" => $addressURL,
                        "key" => "AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg"
                    ];
                    $datos = http_build_query($datos);
                    $Url .= $datos;
                    $aContext = array("http" =>
                        array(
                            "method" => "GET",
                            "header" =>
                                'Accept: application/json' . "\r\n" .
                                'Content-Type: application/json' . "\r\n",
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                        )
                    );
                    $cxContext = stream_context_create($aContext);
                    $sFile = file_get_contents($Url, false, $cxContext);
                    $jData = json_decode(json_encode($sFile), true);
                    $jdata = json_decode($jData);
                    if (strlen($jdata->status) > 3) {
                        $lat = "Error";
                        $lng = "Error";
                    } else {
                        $lat = $jdata->results[0]->geometry->location->lat;
                        $lng = $jdata->results[0]->geometry->location->lng;
                    }
                    $update = "UPDATE `portal_relationship` SET `date` = '" . $date . "', `lat` = '" . $lat . "', `lng` = '" . $lng . "' WHERE `orderID` = '" . $orderID . "'";
                    $conn->query($update);
                }
                //echo 'Order: ' . $x->orderID . ' is being updated in the Database <br/>';
            } else {//End if $changed == true...
                $uq = "UPDATE `salesPortalLinked` SET `archieved` = 'No', `inactive` = 'No' WHERE `orderID` = '" . $orderID . "'";
                $xes = $conn->query($uq);
                $select = "SELECT * FROM `portal_relationship` WHERE orderID = '" . $orderID . "'";
                //echo $select . "<br/>";
                $resultado = $conn->query($select);
                $date = date("Y-m-d");
                $xesr = $resultado->fetch_array();
                if ($resultado->num_rows < 1) {
                    $addressURL = $conn->real_escape_string($x->buildingAddress) . " , " . $conn->real_escape_string($x->buildingCity) . " , " . $conn->real_escape_string($x->buildingState) . " , " . $conn->real_escape_string($x->buildingZipCode);
                    //echo $addressURL . "<br/>";
                    $Url = "https://maps.googleapis.com/maps/api/geocode/json?";
                    $datos = [
                        "address" => $addressURL,
                        "key" => "AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg"
                    ];
                    $datos = http_build_query($datos);
                    $Url .= $datos;
                    $aContext = array("http" =>
                        array(
                            "method" => "GET",
                            "header" =>
                                'Accept: application/json' . "\r\n" .
                                'Content-Type: application/json' . "\r\n",
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                        )
                    );
                    $cxContext = stream_context_create($aContext);
                    $sFile = file_get_contents($Url, false, $cxContext);
                    $jData = json_decode(json_encode($sFile), true);
                    $jdata = json_decode($jData);
                    if (strlen($jdata->status) > 3) {
                        $lat = "Error";
                        $lng = "Error";
                    } else {
                        $lat = $jdata->results[0]->geometry->location->lat;
                        $lng = $jdata->results[0]->geometry->location->lng;
                    }
                    if (strlen($lat) > 0 && strlen($lng) > 0) {
                        $insert = "INSERT INTO `portal_relationship` (`orderID`, `lat`, `lng`, `date`) VALUES ('" . $orderID . "', '" . $lat . "', '" . $lng . "', '" . $date . "')";
                        $conn->query($insert);
                    }
                    $insert = "INSERT INTO `portal_relationship` (`orderID`, `lat`, `lng`, `date`) VALUES ('" + $orderID + "', 'Error', 'Error', '" + $date + "')";
                    //$insert = "INSERT INTO `portal_relationship` (`orderID`, `lat`, `lng`) VALUES ('" . $orderID . "', '0', '0')";
                    $conn->query($insert);
                } else {
                    $addressURL = $conn->real_escape_string($x->buildingAddress) . " , " . $conn->real_escape_string($x->buildingCity) . " , " . $conn->real_escape_string($x->buildingState) . " , " . $conn->real_escape_string($x->buildingZipCode);
                    //echo $addressURL . "<br/>";
                    $Url = "https://maps.googleapis.com/maps/api/geocode/json?";
                    $datos = [
                        "address" => $addressURL,
                        "key" => "AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg"
                    ];
                    $datos = http_build_query($datos);
                    $Url .= $datos;
                    $aContext = array("http" =>
                        array(
                            "method" => "GET",
                            "header" =>
                                'Accept: application/json' . "\r\n" .
                                'Content-Type: application/json' . "\r\n",
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                        )
                    );
                    $cxContext = stream_context_create($aContext);
                    $sFile = file_get_contents($Url, false, $cxContext);
                    $jData = json_decode(json_encode($sFile), true);
                    $jdata = json_decode($jData);
                    if (strlen($jdata->status) > 3) {
                        $lat = "Error";
                        $lng = "Error";
                    } else {
                        $lat = $jdata->results[0]->geometry->location->lat;
                        $lng = $jdata->results[0]->geometry->location->lng;
                    }
                    $update = "UPDATE `portal_relationship` SET `date` = '" . $date . "', `lat` = '" . $lat . "', `lng` = '" . $lng . "' WHERE `orderID` = '" . $orderID . "'";
                    $conn->query($update);
                }
                //echo 'Order: ' . $x->orderID . ' does not need updating in the Database <br/>';
            }//End if $changed != true...
        } else {
            //echo 'Order: ' . $x->orderID . ' is being inserted into the Database <br/>';

            $q = "INSERT INTO `salesPortalLinked`
            (
            `orderID`,
            `orderStatus`,
            `invoiceDate`,
            `dealer`,
            `customer`,
            `customerAddress`,
            `customerCity`,
            `customerState`,
            `customerZipCode`,
            `customerPhone1`,
            `customerPhone2`,
            `customerPhone3`,
            `buildingAddress`,
            `buildingCity`,
            `buildingState`,
            `buildingZipCode`,
            `options1`,
            `options2`,
            `options3`,
            `options4`,
            `options5`,
            `options6`,
            `options7`,
            `options8`,
            `options9`,
            `price`,
            `price1`,
            `price2`,
            `price3`,
            `price4`,
            `price5`,
            `price6`,
            `price7`,
            `price8`,
            `price9`,
            `totalSale`,
            `tax`,
            `total`,
            `tenPercentDep`,
            `fiftyPercentDip`,
            `balance`,
            `taxExemptNum`,
            `instructions`,
            `description`,
            `feesDescription`,
            `fees`,
            `colorDescription`,
            `sidesDescription`,
            `sidesPrice`,
            `trimDescription`,
            `trimPrice`,
            `width`,
            `roof`,
            `frame`,
            `leg`,
            `gauge`,
            `ground`,
            `cement`,
            `asphalt`,
            `other`,
            `txtOther`,
            `landlevelNo`,
            `landlevelYes`,
            `electricityYes`,
            `electricityNo`,
            `cash`,
            `cCheck`,
            `cc`,
            `po`,
            `endsDescription`,
            `endsPrice`,
            `regFrame`,
            `aFrame`,
            `vRoof`,
            `dealerPhone`,
            `allVertical`,
            `customerEmail`
            )
            VALUES
            (
            '" . $conn->real_escape_string($x->orderID) . "',
            '" . $conn->real_escape_string($x->orderStatus) . "',
            '" . $conn->real_escape_string($x->invoiceDate) . "',
            '" . $conn->real_escape_string($x->dealer) . "',
            '" . $conn->real_escape_string($x->customer) . "',
            '" . $conn->real_escape_string($x->customerAddress) . "',
            '" . $conn->real_escape_string($x->customerCity) . "',
            '" . $conn->real_escape_string($x->customerState) . "',
            '" . $conn->real_escape_string($x->customerZipCode) . "',
            '" . $conn->real_escape_string($x->customerPhone1) . "',
            '" . $conn->real_escape_string($x->customerPhone2) . "',
            '" . $conn->real_escape_string($x->customerPhone3) . "',
            '" . $conn->real_escape_string($x->buildingAddress) . "',
            '" . $conn->real_escape_string($x->buildingCity) . "',
            '" . $conn->real_escape_string($x->buildingState) . "',
            '" . $conn->real_escape_string($x->buildingZipCode) . "',
            '" . $conn->real_escape_string($x->options1) . "',
            '" . $conn->real_escape_string($x->options2) . "',
            '" . $conn->real_escape_string($x->options3) . "',
            '" . $conn->real_escape_string($x->options4) . "',
            '" . $conn->real_escape_string($x->options5) . "',
            '" . $conn->real_escape_string($x->options6) . "',
            '" . $conn->real_escape_string($x->options7) . "',
            '" . $conn->real_escape_string($x->options8) . "',
            '" . $conn->real_escape_string($x->options9) . "',
            '" . $conn->real_escape_string($x->price) . "',
            '" . $conn->real_escape_string($x->price1) . "',
            '" . $conn->real_escape_string($x->price2) . "',
            '" . $conn->real_escape_string($x->price3) . "',
            '" . $conn->real_escape_string($x->price4) . "',
            '" . $conn->real_escape_string($x->price5) . "',
            '" . $conn->real_escape_string($x->price6) . "',
            '" . $conn->real_escape_string($x->price7) . "',
            '" . $conn->real_escape_string($x->price8) . "',
            '" . $conn->real_escape_string($x->price9) . "',
            '" . $conn->real_escape_string($x->totalSale) . "',
            '" . $conn->real_escape_string($x->tax) . "',
            '" . $conn->real_escape_string($x->total) . "',
            '" . $conn->real_escape_string($x->tenPercentDep) . "',
            '" . $conn->real_escape_string($x->fiftyPercentDip) . "',
            '" . $conn->real_escape_string($x->balance) . "',
            '" . $conn->real_escape_string($x->taxExemptNum) . "',
            '" . $conn->real_escape_string($x->instructions) . "',
            '" . $conn->real_escape_string($x->description) . "',
            '" . $conn->real_escape_string($x->feesDescription) . "',
            '" . $conn->real_escape_string($x->fees) . "',
            '" . $conn->real_escape_string($x->colorDescription) . "',
            '" . $conn->real_escape_string($x->sidesDescription) . "',
            '" . $conn->real_escape_string($x->sidesPrice) . "',
            '" . $conn->real_escape_string($x->trimDescription) . "',
            '" . $conn->real_escape_string($x->trimPrice) . "',
            '" . $conn->real_escape_string($x->width) . "',
            '" . $conn->real_escape_string($x->roof) . "',
            '" . $conn->real_escape_string($x->frame) . "',
            '" . $conn->real_escape_string($x->leg) . "',
            '" . $conn->real_escape_string($x->gauge) . "',
            '" . $conn->real_escape_string($x->ground) . "',
            '" . $conn->real_escape_string($x->cement) . "',
            '" . $conn->real_escape_string($x->asphalt) . "',
            '" . $conn->real_escape_string($x->other) . "',
            '" . $conn->real_escape_string($x->txtOther) . "',
            '" . $conn->real_escape_string($x->landlevelNo) . "',
            '" . $conn->real_escape_string($x->landlevelYes) . "',
            '" . $conn->real_escape_string($x->electricityYes) . "',
            '" . $conn->real_escape_string($x->electricityNo) . "',
            '" . $conn->real_escape_string($x->cash) . "',
            '" . $conn->real_escape_string($x->cCheck) . "',
            '" . $conn->real_escape_string($x->cc) . "',
            '" . $conn->real_escape_string($x->po) . "',
            '" . $conn->real_escape_string($x->endsDescription) . "',
            '" . $conn->real_escape_string($x->endsPrice) . "',
            '" . $conn->real_escape_string($x->regFrame) . "',
            '" . $conn->real_escape_string($x->aFrame) . "',
            '" . $conn->real_escape_string($x->vRoof) . "',
            '" . $conn->real_escape_string($x->dealerPhone) . "',
            '" . $conn->real_escape_string($x->allVertical) . "',
            '" . $conn->real_escape_string($x->customerEmail) . "'
            )";
            $xes = $conn->query($q);
            $select = "SELECT * FROM `portal_relationship` WHERE orderID = '" . $orderID . "'";
            //echo $select . "<br/>";
            $resultado = $conn->query($select);
            $date = date("Y-m-d");
            $xesr = $resultado->fetch_array();
            if ($resultado->num_rows < 1) {
                $addressURL = $conn->real_escape_string($x->buildingAddress) . " , " . $conn->real_escape_string($x->buildingCity) . " , " . $conn->real_escape_string($x->buildingState) . " , " . $conn->real_escape_string($x->buildingZipCode);
                //echo $addressURL . "<br/>";
                $Url = "https://maps.googleapis.com/maps/api/geocode/json?";
                $datos = [
                    "address" => $addressURL,
                    "key" => "AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg"
                ];
                $datos = http_build_query($datos);
                $Url .= $datos;
                $aContext = array("http" =>
                    array(
                        "method" => "GET",
                        "header" =>
                            'Accept: application/json' . "\r\n" .
                            'Content-Type: application/json' . "\r\n",
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    )
                );
                $cxContext = stream_context_create($aContext);
                $sFile = file_get_contents($Url, false, $cxContext);
                $jData = json_decode(json_encode($sFile), true);
                $jdata = json_decode($jData);
                if (strlen($jdata->status) > 3) {
                    $lat = "Error";
                    $lng = "Error";
                } else {
                    $lat = $jdata->results[0]->geometry->location->lat;
                    $lng = $jdata->results[0]->geometry->location->lng;
                }
                if (strlen($lat) > 0 && strlen($lng) > 0) {
                    $insert = "INSERT INTO `portal_relationship` (`orderID`, `lat`, `lng`, `date`) VALUES ('" . $orderID . "', '" . $lat . "', '" . $lng . "', '" . $date . "')";
                    $conn->query($insert);
                }
                $insert = "INSERT INTO `portal_relationship` (`orderID`, `lat`, `lng`, `date`) VALUES ('" + $orderID + "', 'Error', 'Error', '" + $date + "')";
                //$insert = "INSERT INTO `portal_relationship` (`orderID`, `lat`, `lng`) VALUES ('" . $orderID . "', '0', '0')";
                $conn->query($insert);
            } else {
                $addressURL = $conn->real_escape_string($x->buildingAddress) . " , " . $conn->real_escape_string($x->buildingCity) . " , " . $conn->real_escape_string($x->buildingState) . " , " . $conn->real_escape_string($x->buildingZipCode);
                //echo $addressURL . "<br/>";
                $Url = "https://maps.googleapis.com/maps/api/geocode/json?";
                $datos = [
                    "address" => $addressURL,
                    "key" => "AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg"
                ];
                $datos = http_build_query($datos);
                $Url .= $datos;
                $aContext = array("http" =>
                    array(
                        "method" => "GET",
                        "header" =>
                            'Accept: application/json' . "\r\n" .
                            'Content-Type: application/json' . "\r\n",
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    )
                );
                $cxContext = stream_context_create($aContext);
                $sFile = file_get_contents($Url, false, $cxContext);
                $jData = json_decode(json_encode($sFile), true);
                $jdata = json_decode($jData);
                if (strlen($jdata->status) > 3) {
                    $lat = "Error";
                    $lng = "Error";
                } else {
                    $lat = $jdata->results[0]->geometry->location->lat;
                    $lng = $jdata->results[0]->geometry->location->lng;
                }
                $update = "UPDATE `portal_relationship` SET `date` = '" . $date . "', `lat` = '" . $lat . "', `lng` = '" . $lng . "' WHERE `orderID` = '" . $orderID . "'";
                $conn->query($update);
            }
        }
    }
//Insert the Updated Sync Complete info...
    $iq = "INSERT INTO `scheduler_sync` 
        (
        `date`,
        `time`,
        `total_orders`,
        `message`
        )
        VALUES
        (
        CURRENT_DATE,
        CURRENT_TIME,
        '" . $nr . "',
        'Scheduler Syncing Successfull'
        )";
    $last = $conn->query($iq);
    $last_up = "UPDATE `portal_relationship` SET `date` = CURRENT_DATE, `lat` = 'Error', `lng` = 'Error' WHERE `lat` = '{' AND `lng` = '{'";
    $conn->query($last_up);
    $conn->close();
    $mysqli->close();
} catch (Exception $e) {
    echo 'Excepción capturada: ', $e->getMessage(), "\n";
}
echo '<h1>Sync Complete</h1>';


