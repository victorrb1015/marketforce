<?php
header('Content-Type: application/json');
error_reporting(0);
include '../../php/connection.php';

//Load Variables
$rep_id = $_REQUEST['rep_id'];
$rep_name = $_REQUEST['rep_name'];
$stop_id = $_REQUEST['stop_id'];
$stop_name = $_REQUEST['stop_name'];
$stop_address = $_REQUEST['stop_address'];
$stop_location = $_REQUEST['stop_location'];
$stop_cost = $_REQUEST['stop_cost'];
$stop_type = $_REQUEST['stop_type'];
$waypoint_order = $_REQUEST['waypoint_order'];
$mURL = $_REQUEST['mURL'];
$dr = mysqli_real_escape_string($conn, $_REQUEST['directions_response']);
$uid = uniqid();

//JSON Decode...
$stop_id_a = json_decode($stop_id);
$stop_name_a = json_decode($stop_name);
$stop_address_a = json_decode($stop_address);
$stop_location_a = json_decode($stop_location);
$stop_cost_a = json_decode($stop_cost);
$stop_type_a = json_decode($stop_type);
$waypoint_order_a = json_decode($waypoint_order);


$ac = count($stop_id_a);
//echo $stop_id_a[0];
//echo $ac;

//INSERT Route into database...
$riq = "INSERT INTO `installer_routes`
        (
        `routeID`,
        `date`,
        `time`,
        `rep_id`,
        `rep_name`,
        `installer`,
        `waypoint_order`,
        `directionsURL`,
        `route_response`,
        `status`,
        `inactive`
        )
        VALUES
        (
        '" . $uid . "',
        CURRENT_DATE,
        CURRENT_TIME,
        '" . $rep_id . "',
        '" . $rep_name . "',
        'Unknown',
        '" . $waypoint_order . "',
        '" . $mURL . "',
        '" . $dr . "',
        'Pending',
        'No'
        )";
mysqli_query($conn, $riq) or die($conn->error);

  
//Insert Route stop details into the database...
for($i = 0; $i < $ac; $i++){
  
//if($stop_type_a[$i] != 'custom'){
  
$siq = "INSERT INTO `installer_route_orders`
        (
        `date`,
        `time`,
        `routeID`,
        `stop_id`,
        `stop_name`,
        `stop_address`,
        `stop_location`,
        `stop_cost`,
        `stop_type`,
        `waypoint_number`,
        `inactive`
        )
        VALUES
        (
        CURRENT_DATE,
        CURRENT_TIME,
        '" . $uid . "',
        '" . $stop_id_a[$i] . "',
        '" . $stop_name_a[$i] . "',
        '" . $stop_address_a[$i] . "',
        '" . $stop_location_a[$i] . "',
        '" . $stop_cost_a[$i] . "',
        '" . $stop_type_a[$i] . "',
        '" . $i . "',
        'No'
        )";
  mysqli_query($conn, $siq) or die($conn->error);
  
/*}else{
  //Get Custom Stop Info...
  $csq = "SELECT * FROM `scheduler_custom_stops` WHERE `stop_id` = '" . $stop_id_a[$i] . "'";
  $csg = mysqli_query($conn, $csq);
  $scr = mysqli_fetch_array($csg);
  
      $siq = "INSERT INTO `installer_route_orders`
        (
        `date`,
        `time`,
        `routeID`,
        `stop_id`,
        `stop_name`,
        `stop_address`,
        `stop_location`,
        `stop_cost`,
        `stop_type`,
        `waypoint_number`,
        `inactive`
        )
        VALUES
        (
        CURRENT_DATE,
        CURRENT_TIME,
        '" . $uid . "',
        '" . $stop_id_a[$i] . "',
        '" . $cStop_name . "',
        '" . $stop_address_a[$i] . "',
        '" . $stop_location_a[$i] . "',
        '" . $stop_cost_a[$i] . "',
        '" . $stop_type_a[$i] . "',
        '" . $waypoint_order_a[$i] . "',
        'No'
        )";
  mysqli_query($conn, $siq) or die($conn->error);
  
    }*/

  if($stop_type_a[$i] == 'Repair'){
    $ruq = "UPDATE `repairs` SET `in_use` = 'Yes' WHERE `ID` = '" . $stop_id_a[$i] . "'";
    mysqli_query($conn, $ruq) or die($conn->error);
  }elseif($stop_type_a[$i] == 'Order'){
    $ouq = "UPDATE `salesPortalLinked` SET `in_use` = 'Yes' WHERE `orderID` = '" . $stop_id_a[$i] . "'";
    mysqli_query($conn, $ouq) or die($conn->error);
  }elseif($stop_type_a[$i] == 'Custom'){
    //Do Nothing...
  }else{
    $error = 1;
  }
  
}

if($error == 1){
  $x->response = 'ERROR';
  $x->message = 'An error occured while confirming your route! Error Code: 94110';
}

if(!$error){
  $x->response = 'GOOD';
  $x->message = 'Your Route (ID:' . $uid . ') has been confirmed and is now set as PENDING!';
  //Log Activity
  $textfile = '../activity-log.txt';
  $oldContents = file_get_contents($textfile);
  $log = fopen("../activity-log.txt","w");
  fwrite($log, date("m/d/y h:iA") . " --> " . $rep_name . "\n");
  fwrite($log, "Confirmed RouteID: " . $uid . "\n");
  fwrite($log, "---------------------------------------------------------------------------------------------\n");
  fwrite($log, $oldContents);
  fclose($log);
}

$response = json_encode($x, JSON_PRETTY_PRINT);
echo $response;