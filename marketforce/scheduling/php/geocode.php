<?php
//header('Content-Type: application/json');
error_reporting(0);
include '../../php/connection.php';

//Load Variables...

#Main Functions...
function geocode_address($id,$address,$mode,$conn){
  $address = str_replace(' ', '%20', $address);
  $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg';

  // create curl resource
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  //curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  //The output string from execution
  $output = curl_exec($ch);
  $output = json_decode($output);
  // close curl resource to free up system resources
  curl_close($ch);

  //$x->result_count = count($output->results[0]->address_components);
  //$x->data = $output->results[0];
  
  $lat = $output->results[0]->geometry->location->lat;
  $lng = $output->results[0]->geometry->location->lng;
  
  if($mode == 'Update'){
    $uq = "UPDATE `portal_relationship` SET `lat` = '" . $lat . "', `lng` = '" . $lng . "' WHERE `orderID` = '" . $id . "'";
    mysqli_query($conn, $uq) or die('Update Connection Error: ' . $conn->error);
    echo 'orderID: ' . $id . ' Updated<br>';
  }
  
  if($mode == 'Insert'){
    $iq = "INSERT INTO `portal_relationship` 
          (
          `orderID`,
          `date`,
          `lat`,
          `lng`
          )
          VALUES
          (
          '" . $id . "',
          CURRENT_DATE,
          '" . $lat . "',
          '" . $lng . "'
          )";
    mysqli_query($conn, $iq) or die('Insert Connection Error: ' . $conn->error);
    echo 'orderID: ' . $id . ' Inserted<br>';
  }
}



$q = "SELECT 
      `salesPortalLinked`.`ID`,
      `salesPortalLinked`.`orderID` AS `link_orderID`,
      `salesPortalLinked`.`buildingAddress`,
      `salesPortalLinked`.`buildingCity`,
      `salesPortalLinked`.`buildingState`,
      `salesPortalLinked`.`buildingZipCode`,
      `salesPortalLinked`.`archieved`,
      `salesPortalLinked`.`inactive` AS `link_inactive`,
      `portal_relationship`.`orderID` AS `portal_orderID`,
      `portal_relationship`.`lat`,
      `portal_relationship`.`lng`,
      `portal_relationship`.`inactive` AS `portal_inactive`
      FROM `salesPortalLinked`
      LEFT JOIN `portal_relationship`
      ON `salesPortalLinked`.`orderID` = `portal_relationship`.`orderID` 
      WHERE `salesPortalLinked`.`archieved` != 'Yes'
      AND `salesPortalLinked`.`inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  if(mysqli_num_rows($g) <= 0){
    //No recored exists, do nothing...
    echo 'No Records Need Attention At This Time...<br>';
  }elseif(($r['lat'] == '' || $r['lng'] == '') && ($r['portal_inactive'] != 'Yes') && ($r['portal_orderID'] != '')){
    //Geocode incomplete, update record
    $address = mysqli_real_escape_string($conn,str_replace("#","",$r['buildingAddress'])) . ',' . mysqli_real_escape_string($conn,$r['buildingCity']) . ',' . $r['buildingState'] . ',' . $r['buildingZipCode'];
    $id = $r['link_orderID'];
    //print_r($r);
    geocode_address($id,$address,'Update',$conn);
  }elseif($r['portal_orderID'] == ''){
    $address = mysqli_real_escape_string($conn,str_replace("#","",$r['buildingAddress'])) . ',' . mysqli_real_escape_string($conn,$r['buildingCity']) . ',' . $r['buildingState'] . ',' . $r['buildingZipCode'];
    $id = $r['link_orderID'];
    geocode_address($id,$address,'Insert',$conn);
    //print_r($r);
  }else{
    //Geocode Exists, do nothing...
    echo 'orderID: ' . $r['link_orderID'] . ' Exists<br>';
    //print_r($r);
  }
}







//Setup Response Output...
//$response = json_encode($x,JSON_PRETTY_PRINT);
//echo $response;