<?php
header('Content-Type: application/json');
error_reporting(0);
include '../../php/connection.php';

//Load Variables...



#Main Functions...
//Repairs Query...
$rq = "SELECT * FROM `repairs` WHERE `inactive` != 'Yes' AND `status` != 'Completed' AND `status` != 'Cancelled' AND `status` != 'Draft' AND `lat` != 'Error' AND `lng` != 'Error'";
$rg = mysqli_query($conn, $rq) or die($conn->error);
$results_total = mysqli_num_rows($rg);

$x->response = 'GOOD';
$x->message = 'Repairs Fetched Successfully!';
$x->results_total = $results_total;
$x->data = [];
while($r = mysqli_fetch_object($rg)){
  
  //Date Color Coding...
  $td = date("m-d-y");
  //$d1 = strtotime($rr['today_date']);
  $d1 = strtotime($r->today_date);
  $d2 = strtotime("-2 week");
  $d4 = strtotime("-4 week");
  $d6 = strtotime("-6 week");
  $d8 = strtotime("-8 week");
    
  if($d8 >= $d1){
      $mBG = 'FF0000';//Red
      $mFC = '000000';//Black
  }else if($d6 >= $d1){
      $mBG = 'FFFF00';//Yellow
      $mFC = '000000';//Black
  }else if($d4 >= $d1){
      $mBG = '008000';//Green
      $mFC = 'FFFFFF';//White
  }else if($d2 >= $d1){
      $mBG = '0000FF';//Blue
      $mFC = 'FFFFFF';//White
  }else{
      $mBG = 'FFFFFF';//White
      $mFC = '000000';//Black
  }
  
  $r->mBG = $mBG;
  $r->mFC = $mFC;
  
  array_push($x->data, $r);
}


//Setup Response Output...
$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;