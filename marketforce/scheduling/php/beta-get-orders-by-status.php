<?php
header('Content-Type: application/json');
error_reporting(E_ALL);
include '../../php/connection.php';

//Load Variables...
//echo $_SESSION['org_sales_portal_api_url'];


#Main Functions...
//Portal Relationship Table Query
//$rq = "SELECT * FROM `portal_relationship` WHERE `orderID` = '" . $rrx->orderID . "' AND `lat` != 'Error'";
$rq = "SELECT * FROM `portal_relationship` WHERE `inactive` != 'Yes' AND `lat` != 'Error'";
$rg = mysqli_query($conn, $rq) or die('Portal Relationship Table Query Error: ' . $conn->error);
$arrayOfObjects = [];
while($rr = mysqli_fetch_object($rg)){
  array_push($arrayOfObjects,$rr);
}

//CSKern SOAP CALL...
$sc = new SoapClient($_SESSION['org_sales_portal_api_url']);
$apiKey = "33f2ddee-724e-4bd1-9606-4408f5f95bdd";
$status = 'In Shop';

//SoapClient Parameters for the CSKern Web Service API
$params = array(
  "apiKey" => $apiKey,
  "status" => $status
);

$result = $sc->__soapCall("GetOrdersByStatus", array($params));
$rx = json_decode($result->GetOrdersByStatusResult);
$nr = count($rx);
//$do = new object;
$x->orders = [];
$x->response = 'GOOD';
$x->message = $nr . ' orders fetched successfully!';
$x->total_results = $nr;
$i = 0;
foreach($rx as $rrx){
  $do = '';
  $do->orderID = $rrx->orderID;
  $do->buildingState = $rrx->buildingState;
  //Lat & Lng
  $searchedValue = $rrx->orderID;
  $neededObject = array_filter(
      $arrayOfObjects,
      function ($e) use (&$searchedValue) {
          return $e->orderID == $searchedValue;
      }
  );
  $neededObject = array_values($neededObject);
  //var_dump($neededObject[0]);
  $do->lat = $neededObject[0]->lat;
  $do->lng = $neededObject[0]->lng;
  //mBG & mFC
  //Date Color Coding...
  $td = date("m-d-y");
  //$d1 = strtotime($rr['today_date']);
  $d1 = strtotime($rrx->invoiceDate);
  $d2 = strtotime("-2 week");
  $d4 = strtotime("-4 week");
  $d6 = strtotime("-6 week");
  $d8 = strtotime("-8 week");
    
  if($d8 >= $d1){
      $mBG = 'FF0000';//Red
      $mFC = '000000';//Black
  }else if($d6 >= $d1){
      $mBG = 'FFFF00';//Yellow
      $mFC = '000000';//Black
  }else if($d4 >= $d1){
      $mBG = '008000';//Green
      $mFC = 'FFFFFF';//White
  }else if($d2 >= $d1){
      $mBG = '0000FF';//Blue
      $mFC = 'FFFFFF';//White
  }else{
      $mBG = 'FFFFFF';//White
      $mFC = '000000';//Black
  }
  
  $do->mBG = $mBG;
  $do->mFC = $mFC;
  $do->totalSale = $rrx->totalSale;
  $do->customer = $rrx->customer;
  $do->orderStatus = $rrx->orderStatus;
  $do->buildingAddress = $rrx->buildingAddress;
  $do->buildingCity = $rrx->buildingCity;
  $do->buildingZipCode = $rrx->buildingZipCode;
  array_push($x->orders,$do);
}
//array_push($rx, array('total_results' => $nr));
//$x->orders = $rx;
//var_dump($rx);


//Setup Response Output...
$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;