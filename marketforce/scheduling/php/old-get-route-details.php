<?php
include '../../php/connection.php';

//Load Variables...
$rid = $_GET['rid'];

//Get Route Info...
$q = "SELECT * FROM `installer_routes` WHERE `routeID` = '" . $rid . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);

              
  echo '<input type="hidden" id="mrid" value="' . $rid . '" />';   
  echo '<h4 class="text-center">RouteID: ' . $rid . '</h4>';
  echo '<div class="row" style="background-color:black;">
                          <div class="col-md-6">
                              <h3 class="text-right" style="color:rgb(255,255,255);">Installer Name:</h3>
                          </div>
                          <div class="col-md-6"><input type="text" id="iName" placeholder="Installer Name" class="form-control" value="' . $r['installer'] . '" style="margin:15px 0px 10px;"></div>
                      </div>
                      <hr>
                    ';            

$aq = "SELECT * FROM `installer_route_orders` WHERE `routeID` = '" . $rid . "' ORDER BY `waypoint_number` ASC";
$ag = mysqli_query($conn, $aq) or die($conn->error);
$i = 1;
while($ar = mysqli_fetch_array($ag)){
  
  if($ar['stop_type'] == 'Repair'){
    $rq = "SELECT * FROM `repairs` WHERE `ID` = '" . $ar['stop_id'] . "'";
    $rg = mysqli_query($conn, $rq) or die($conn->error);
    $rr = mysqli_fetch_array($rg);
    $invNum = $rr['inv'];
    $si = $ar['stop_id'];
    if($rr['customer_balance'] > 0 || $rr['customer_cost'] > 0){
      $mode = 'owes';
    }else{
      $mode = 'paid';
    }
  }else{
    $invNum = $ar['stop_id'];
    $si = $ar['stop_id'];
    $oq = "SELECT * FROM `salesPortalLinked` WHERE `orderID` = '" . $invNum . "'";
    $og = mysqli_query($conn, $oq) or die($conn->error);
    $or = mysqli_fetch_array($og);
    $dname = mysqli_real_escape_string($conn,$or['dealer']);
  }
  
               echo '<div class="row" style="border-top:2px solid black">
                        <input type="hidden" id="stop_id' . $i . '" value="' . $si . '" />
                         <div class="col-md-12">
                            <h4 class="text-uppercase text-center" style="color:red;"><strong>Stop ' . $i . '</strong></h4>
                            <p class="text-center" style="background:black;color:white;border-radius:10px;padding:5px;font-weight:bold;">
                              ' . strtoupper($ar['stop_type']) . '';
                 if($ar['stop_type'] == 'Repair'){
                        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                              <a href="Javascript:view_pl(' . $si . ',\'' . $mode . '\')" style="text-decoration:underline;"><i class="fa fa-file-text-o"></i> View Punch List</a>';
                 }
                        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              Invoice: ' . $invNum;
                         echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                              <a href="Javascript:view_inv(' . $invNum . ')" style="text-decoration:underline;"><i class="fa fa-file-pdf-o"></i> View Invoice</a>';
                      echo '</p>
                        </div>
                        <div class="col-md-6">
                            <h4 class="text-center">' . $ar['stop_name'] . '</h4>
                            <p>Dealer: ' . $dname . '</p>
                            <p><strong>Cost: $' . number_format($ar['stop_cost'],2) . '</strong></p>';
                          if($or['customerPhone1'] != ''){
                            echo '<p>Customer Phone: ' . $or['customerPhone1'] . '</p>';
                          }
                          if($or['customerPhone2'] != ''){
                            echo '<p>Alt Customer Phone: ' . $or['customerPhone2'] . '</p>';
                          }
                          if($or['customerPhone3'] != ''){
                            echo '<p>Alt Customer Phone: ' . $or['customerPhone3'] . '</p>';
                          }
                          if($or['customerEmail'] != ''){
                            echo '<p>Customer Email: ' . $or['customerEmail'] . '</p>';
                          }
  
  if($ar['confirmed'] == 'true'){
    $checked = 'checked';
  }else{
    $checked = '';
  }
                  echo '</div>
                        <div class="col-md-6">
                            <h4 class="text-center">Address</h4>
                            <p class="text-center">' . $ar['stop_address'] . '</p>
                            <!--<p class="text-center">Orlando, Fl 32789</p>-->
                        </div>
                    </div>
                    <div class="row" style="border-bottom:2px solid black;">
                        <div class="col-md-4" style="border-top:1px solid #9D9D9D;">
                            <div class="checkbox"><label><input type="checkbox" id="confirmed' . $i . '" value="confirmed" ' . $checked . '><mark>Customer Confirmed</mark></label></div>
                        </div>
                        <div class="col-md-8" style="border-top:1px solid #9D9D9D;">
                            <p class="text-center" style="font-weight:bold;margin:0px 0px 5px;">Customer Notes:</p>
                            <textarea class="form-control" style="margin-bottom:5px;" id="notes' . $i . '">' . $ar['notes'] . '</textarea>
                        </div>
                      </div>';
  
  
  $i++;
  
}
echo '</div>';
echo '<input type="hidden" id="stop_count" value="' . ($i - 1) . '" />';
?>