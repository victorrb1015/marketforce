<?php
header('Content-Type: application/json');
error_reporting(E_ALL);
include '../../php/connection.php';

//Load Variables...



#Main Functions...
//$q = "SELECT * FROM `salesPortalLinked` WHERE `orderStatus` = 'In Shop' AND `archieved` != 'Yes'";
/*$q = "SELECT 
`salesPortalLinked`.`ID`,
`salesPortalLinked`.`orderID`,
`salesPortalLinked`.`totalSale`,
`salesPortalLinked`.`customer`,
`salesPortalLinked`.`orderStatus`,
`salesPortalLinked`.`buildingAddress`,
`salesPortalLinked`.`buildingCity`,
`salesPortalLinked`.`buildingState`,
`salesPortalLinked`.`buildingZipCode`,
`salesPortalLinked`.`invoiceDate`,
`salesPortalLinked`.`archieved`,
`portal_relationship`.`lat`,
`portal_relationship`.`lng`
FROM `salesPortalLinked`
LEFT JOIN `portal_relationship`
ON `salesPortalLinked`.`orderID` = `portal_relationship`.`orderID`
WHERE `salesPortalLinked`.`orderStatus` = 'In Shop' AND `salesPortalLinked`.`archieved` != 'Yes'";*/
$q = "SELECT 
`salesPortalLinked`.`ID`,
`salesPortalLinked`.`orderID`,
`salesPortalLinked`.`totalSale`,
`salesPortalLinked`.`customer`,
`salesPortalLinked`.`orderStatus`,
`salesPortalLinked`.`buildingAddress`,
`salesPortalLinked`.`buildingCity`,
`salesPortalLinked`.`buildingState`,
`salesPortalLinked`.`buildingZipCode`,
`salesPortalLinked`.`invoiceDate`,
`salesPortalLinked`.`archieved`,
`salesPortalLinked`.`inactive`,
`portal_relationship`.`lat`,
`portal_relationship`.`lng`
FROM `salesPortalLinked`
LEFT JOIN `portal_relationship`
ON `salesPortalLinked`.`orderID` = `portal_relationship`.`orderID`
WHERE 
`salesPortalLinked`.`orderStatus` = 'In Shop' 
AND 
`salesPortalLinked`.`archieved` != 'Yes' 
AND 
`salesPortalLinked`.`inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
$results_total = mysqli_num_rows($g);

//Get timestamp for last data sync process
$dsq = "SELECT * FROM `scheduler_sync` ORDER BY `ID` DESC LIMIT 1";
$dsg = mysqli_query($conn, $dsq) or die($conn->error);
$dsr = mysqli_fetch_array($dsg);


$x->response = 'GOOD';
$x->message = 'Orders Fetched Successfully!';
$x->sync_timestamp = date("m/d/Y",strtotime($dsr['date'])) . ', ' . date("h:i:s A",strtotime($dsr['time']));
$x->cache_timestamp = date("m/d/Y, h:i:s A", strtotime('-4 hours'));
$x->results_total =  $results_total;
$x->data = [];
while($r = mysqli_fetch_object($g)){
  
  
  //Setup the icon image for the orders...
    //Date Color Coding...
    $td = date("m-d-y");
    //$d1 = strtotime($rr['today_date']);
    $d1 = strtotime($r->invoiceDate);
    $d2 = strtotime("-2 week");
    $d4 = strtotime("-4 week");
    $d6 = strtotime("-6 week");
    $d8 = strtotime("-8 week");
      
    if($d8 >= $d1){
        $mBG = 'FF0000';//Red
        $mFC = '000000';//Black
    }else if($d6 >= $d1){
        $mBG = 'FFFF00';//Yellow
        $mFC = '000000';//Black
    }else if($d4 >= $d1){
        $mBG = '008000';//Green
        $mFC = 'FFFFFF';//White
    }else if($d2 >= $d1){
        $mBG = '0000FF';//Blue
        $mFC = 'FFFFFF';//White
    }else{
        $mBG = 'FFFFFF';//White
        $mFC = '000000';//Black
    }
  
  $r->mBG = $mBG;
  $r->mFC = $mFC;
  
  
  array_push($x->data, $r);
}



//Setup Response Output...
$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;