<?php
include '../../php/connection.php';

$error = false;

//Load Variables
$rid = $_GET['rid'];
$mode = $_GET['mode'];
$rep_name = $_GET['rep_name'];
$rep_id = $_GET['rep_id'];
$istatus = $_GET['istatus'];


//Setup SOAP Call Parameters...
  $sc = new SoapClient("http://sales.allsteelcarports.com/publicapi/allsteelwebservice.asmx?WSDL");
  $apiKey = "33f2ddee-724e-4bd1-9606-4408f5f95bdd";
  $status = $istatus;

$sq = "SELECT * FROM `installer_route_orders` WHERE `routeID` = '" . $rid . "' AND `stop_type` = 'Order'";
$sg = mysqli_query($conn, $sq) or die($conn->error);
while($sr = mysqli_fetch_array($sg)){
  $invoice = $sr['stop_id'];
  //SoapClient Parameters for the CSKern Web Service API
    $params = array(
      "apiKey" => $apiKey,
      "invoice" => $invoice,
      "status" => $status
    );

  //Update the invoice on the CSKern Sales Portal...
    $result = $sc->__soapCall("UpdateOrderStatus", array($params));
    //$r = json_decode($result->GetOrdersByInvoiceResult);
    $rx = json_decode($result->UpdateOrderStatusResult);
  
  if($rx != 'Order was successfully updated'){
    $error = true;
    $CSKernResponse = $rx;
  }
}


//Get Current Status...
$cq = "SELECT * FROM `installer_routes` WHERE `routeID` = '" . $rid . "'";
$cg = mysqli_query($conn, $cq) or die($conn->error);
$cr = mysqli_fetch_array($cg);
$cstatus = $cr['status'];

//Change Route Status...
$dq = "UPDATE `installer_routes` SET `status` = '" . $mode . "' WHERE `routeID` = '" . $rid . "'";
mysqli_query($conn, $dq) or die($conn->error . " Error Code: 50579");

echo 'RouteID: ' . $rid . ' has been set to status: ' . $mode;
if($error == true){
  echo ' ' . $CSKernResponse;
}else{
  $CSKernResponse = $rx;
}

//Log Activity
$textfile = '../activity-log.txt';
$oldContents = file_get_contents($textfile);
$log = fopen("../activity-log.txt","w");
fwrite($log, date("m/d/y h:iA") . " --> " . $rep_name . "\n");
fwrite($log, "Changed Status of RouteID: " . $rid . " from (" . $cstatus . ") to (" . $mode . ")" . "\n");
fwrite($log, "CSKern Portal Update Notes: " . $CSKernResponse . "\n");
fwrite($log, "---------------------------------------------------------------------------------------------\n");
fwrite($log, $oldContents);
fclose($log);

?>