<?php
include '../php/connection.php';

//Load Variables...
$invNum = $_GET['inv'];

//Get Invoice Details...
$iq = "SELECT * FROM `salesPortalLinked` WHERE `orderID` = '" . $invNum . "'";
$ig = mysqli_query($conn, $iq) or die($conn->error);
$ir = mysqli_fetch_array($ig);

//CSKern SOAP CALL...
$sc = new SoapClient("http://sales.allsteelcarports.com/publicapi/allsteelwebservice.asmx?WSDL");
$apiKey = "33f2ddee-724e-4bd1-9606-4408f5f95bdd";
$invoice = $invNum;

//SoapClient Parameters for the CSKern Web Service API
$params = array(
    "apiKey" => $apiKey,
    "invoice" => $invoice
);

$result = $sc->__soapCall("GetOrdersByInvoice", array($params));
$rx = json_decode($result->GetOrdersByInvoiceResult);
$rr = $rx[0];
//echo $rr->orderID;
?>

<html>

<head>
    <title>Invoice# <?php echo $invNum; ?></title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Tfj13fqQQqqzQFuaZ81WDzmmOU610WeS08VMuHmElK5oI2f7NwojuL6VupYXR/jK" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js" integrity="sha384-4Kp4aQ6UNeqsJ/ithPcxYnnIGt/QJJ64J9QtfDAJZUTaePAIPm9aaBdu7Gw84oGs" crossorigin="anonymous"></script>
    <link href="css/site.css" rel="stylesheet" />
    <style>
        div {
            /*border: 1px solid red;*/
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="col-lg-12">

            <div class="panel panel-primary">
                <div class="panel-heading divNoPrint">
                    Order Form</div>
                <div class="panel-body" id="divPrint">
                    <div class="order-logo ">
                        <div style="width: 215px; float: left;">
                            <img src="<?php
                                        switch ($_SESSION['org_id']) {
                                            case "832122":
                                                echo 'http://marketforceapp.com/assets/img/brands/acero-news.png';
                                                break;
                                            case "738004":
                                                echo 'http://marketforceapp.com/assets/img/brands/acero-news.png';
                                                break;
                                            case "654321":
                                                echo 'http://marketforceapp.com/assets/img/brands/acero-news.png';
                                                break;
                                            case "162534":
                                                echo 'http://marketforceapp.com/assets/img/brands/NorthEdgeSteel-01.png';
                                                break;
                                            case "615243":
                                                echo 'http://marketforceapp.com/assets/img/brands/legacyMFlogo.png';
                                                break;
                                            default:
                                                echo 'http://marketforceapp.com/assets/img/brands/Logo-mailing-allsteel.png';
                                        }
                                        ?>" alt="<?php
                                        switch ($_SESSION['org_id']) {
                                            case "162534":
                                                echo 'Northedge Steel';
                                                break;
                                            case "615243":
                                                echo 'legacy buildings';
                                                break;
                                            default:
                                                echo 'All Steel Carports';
                                        }
                                        ?>" id="imgAllSteel" style="width:200px;" />
                        </div>
                        <div style="width: 310px; float: left;">
                        <?php
                                        switch ($_SESSION['org_id']) {
                                            case "162534":
                                                echo 'PO Box 459 Rural Hall, NC 27045 &nbsp; Phone: (765) 591-8080 
                                                <br />
                                                www.northedgesteel.us';
                                                break;
                                            default:
                                                echo '2200 North Granville Avenue Muncie, IN 47303 &nbsp; Phone: (765) 284-0694
                                                <br />
                                                Toll Free: (800) 730-7908 &nbsp; Fax: (765) 284-2689<br />
                                                www.allsteelcarports.com';
                                        }
                                        ?>
                        </div>
                        <div style="width: 380px; float: right;">
                            <strong>Date Created: </strong>
                            <span id="ContentPlaceHolder1_cntlManageOrder_lblDatCreated"><?php echo $rr->invoiceDate; ?></span><br />
                            <strong>Sales Order #: </strong>
                            <span id="ContentPlaceHolder1_cntlManageOrder_lblOrderID"><?php echo $rr->orderID; ?></span><br />
                            <!--<strong>Date Installed: </strong>
                        <span id="ContentPlaceHolder1_cntlManageOrder_lblDateInstalled"></span>-->
                        </div>
                        <div style="clear: both;" class="divNoPrint">
                        </div>
                    </div>
                    <br />
                    <div style="width: 960px;" class="small-top1">
                        <div style="width: 620px;" class="small-top2">
                            <div class="divRow divNoPrint">
                                <strong>Dealer: </strong>
                                <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtDealer" type="text" value="<?php echo $rr->dealer; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtDealer" disabled="disabled" class="aspNetDisabled dField2" style="width:550px;" />
                            </div>
                            <div class="divRow divNoPrint">
                                <strong>Dealer Phone:</strong>
                                <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtDealerPhone" type="text" value="<?php echo $rr->dealerPhone; ?>" maxlength="50" id="ContentPlaceHolder1_cntlManageOrder_txtDealerPhone" disabled="disabled" tabindex="3" class="aspNetDisabled" style="width:500px;" />
                            </div>
                            <div class="divRow divNoPrint">
                                <strong>Customer Name: </strong>
                                <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtCusName" type="text" value="<?php echo $rr->customer; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtCusName" class="dField2" style="width:490px;" disabled />
                            </div>
                            <div class="divRow divNoPrint">
                                <strong>Customer Email: </strong>
                                <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtCustomerEmail" type="text" value="<?php echo $rr->customerEmail; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtCustomerEmail" tabindex="3" style="width:490px;" />
                            </div>
                            <div class="divRow divNoPrint">
                                <strong>Address: </strong>
                                <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtAddress" type="text" value="<?php echo $rr->buildingAddress; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtAddress" class="dField2 divNoPrint" style="width:538px;" />
                            </div>
                            <div class="divRow divNoPrint">
                                <strong>City:</strong>
                                <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtCity" type="text" value="<?php echo $rr->buildingCity; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtCity" class="dField2" style="width:160px;" />&nbsp;&nbsp;
                                <strong>State:</strong>
                                <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$ddlState" value="<?php echo $rr->buildingState; ?>" id="ContentPlaceHolder1_cntlManageOrder_ddlState" class="dField2">
                                &nbsp;&nbsp; <strong>Zip Code:</strong>
                                <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtZip" type="text" value="<?php echo $rr->buildingZipCode; ?>" maxlength="5" id="ContentPlaceHolder1_cntlManageOrder_txtZip" class="dField2" style="width:120px;" />
                            </div>
                            <div class="divNoPrint">
                                <strong>Cell Phone:</strong>
                                <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPhone1" type="text" value="<?php echo $rr->customerPhone1; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtPhone1" class="dField2" style="width:120px;" />
                                &nbsp; &nbsp; <strong>Phone 2:</strong>
                                <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPhone2" type="text" value="<?php echo $rr->customerPhone2; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtPhone2" class="dField2" style="width:120px;" />
                                &nbsp; &nbsp; <strong>Phone 3:</strong>
                                <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPhone3" type="text" value="<?php echo $rr->customerPhone2; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtPhone3" class="dField2" style="width:120px;" />
                            </div>
                            <br />
                        </div>
                        <div id="ContentPlaceHolder1_cntlManageOrder_updateOrder">

                            <div style="width: 624px; float: left; clear: both; padding-right: 15px;">
                                <table id="tbl" class="tblDescandPrice">
                                    <tr>
                                        <td class="tbltd">
                                            <strong>Description</strong>
                                        </td>
                                        <td class="tbltd">
                                            <table cellpadding="8" cellspacing="4" width="100%">
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Width</strong><br />
                                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtWidth" type="text" value="<?php echo $rr->width; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtWidth" tabindex="11" class="dField4" style="width:50px;" /><br />
                                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv1" style="color:Red;display:none;">must be numeric</span>
                                                    </td>
                                                    <td width="20%">
                                                        &nbsp; <strong>Roof</strong><br />
                                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtRoof" type="text" value="<?php echo $rr->roof; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtRoof" tabindex="12" class="dField4" style="width:50px;" /><br />
                                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv2" style="color:Red;display:none;">must be numeric</span>
                                                    </td>
                                                    <td width="20%">
                                                        &nbsp; <strong>Frame</strong><br />
                                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtFrame" type="text" value="<?php echo $rr->frame; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtFrame" tabindex="13" class="dField4" style="width:50px;" /><br />
                                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv3" style="color:Red;display:none;">must be numeric</span>
                                                    </td>
                                                    <td width="20%">
                                                        &nbsp; <strong>Leg </strong>
                                                        <br />
                                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtLeg" type="text" value="<?php echo $rr->leg; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtLeg" tabindex="14" class="dField4" style="width:50px;" /><br />
                                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv4" style="color:Red;display:none;">must be numeric</span>
                                                    </td>
                                                    <td width="20%">
                                                        &nbsp;<strong>Gauge</strong><br />
                                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtGauge" type="text" value="<?php echo $rr->gauge; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtGauge" tabindex="15" class="dField4" style="width:50px;" /><br />
                                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv5" style="color:Red;display:none;">must be numeric</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="tbltd">
                                            <strong>Price</strong><br />
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice" type="text" value="<?php echo $rr->price; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice" tabindex="16" class="dField2" style="width:60px;" />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv6" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            <strong>Options</strong>
                                        </td>
                                        <td class="tbltd">
                                            <table width="100%">
                                                <tr>
                                                    <td align="center">
                                                        <span><strong>Regular Frame</strong></span><br />
                                                        <span class="chkPrnt"><input id="ContentPlaceHolder1_cntlManageOrder_chkRegFrame" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkRegFrame" tabindex="17" <?php if ($rr->refFrame == 'True') {
                                                                                                                                                                                                                                            echo 'checked';
                                                                                                                                                                                                                                        } ?> /></span>
                                                    </td>
                                                    <td align="center">
                                                        <span><strong>A-Frame</strong></span><br />
                                                        <span class="chkPrnt"><input id="ContentPlaceHolder1_cntlManageOrder_chkAFrame" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkAFrame" tabindex="18" <?php if ($rr->aFrame == 'True') {
                                                                                                                                                                                                                                        echo 'checked';
                                                                                                                                                                                                                                    } ?> /></span>
                                                    </td>
                                                    <td align="center">
                                                        <span><strong>Vertical Roof</strong></span><br />
                                                        <span class="chkPrnt"><input id="ContentPlaceHolder1_cntlManageOrder_chkVerticleRoof" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkVerticleRoof" tabindex="19" <?php if ($rr->vRoof == 'True') {
                                                                                                                                                                                                                                                    echo 'checked';
                                                                                                                                                                                                                                                } ?> /></span>
                                                    </td>
                                                    <td align="center">
                                                        <span><strong>All Vertical</strong></span><br />
                                                        <span class="chkPrnt"><input id="ContentPlaceHolder1_cntlManageOrder_chkAllVertical" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkAllVertical" tabindex="20" <?php if ($rr->allVertical == 'True') {
                                                                                                                                                                                                                                                echo 'checked';
                                                                                                                                                                                                                                            } ?> /></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice1" type="text" value="<?php echo $rr->price1; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice1\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice1" tabindex="20" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv7" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt2" type="text" value="<?php echo $rr->options2; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtOpt2" tabindex="21" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice2" type="text" value="<?php echo $rr->price2; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice2\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice2" tabindex="22" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv8" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt3" type="text" value="<?php echo $rr->options3; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtOpt3" tabindex="23" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice3" type="text" value="<?php echo $rr->price3; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice3\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice3" tabindex="24" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv9" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt4" type="text" value="<?php echo $rr->options4; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtOpt4" tabindex="25" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice4" type="text" value="<?php echo $rr->price4; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice4\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice4" tabindex="26" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv10" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt5" type="text" value="<?php echo $rr->options5; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtOpt5" tabindex="27" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice5" type="text" value="<?php echo $rr->price5; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice5\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice5" tabindex="28" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv11" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt6" type="text" value="<?php echo $rr->options6; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtOpt6" tabindex="29" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice6" type="text" value="<?php echo $rr->price6; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice6\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice6" tabindex="30" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv12" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt7" type="text" value="<?php echo $rr->options7; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtOpt7" tabindex="31" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice7" type="text" value="<?php echo $rr->price7; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice7\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice7" tabindex="32" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv13" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt8" type="text" value="<?php echo $rr->options8; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtOpt8" tabindex="33" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice8" type="text" value="<?php echo $rr->price8; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice8\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice8" tabindex="34" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv14" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt9" type="text" value="<?php echo $rr->options9; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtOpt9" tabindex="35" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice9" type="text" value="<?php echo $rr->price9; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice9\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice9" tabindex="36" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv15" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            <strong>Color / Top</strong>
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtColorTop" type="text" value="<?php echo $rr->colorDescription; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtColorTop" tabindex="37" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceColorTop" type="text" value="" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceColorTop\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPriceColorTop" tabindex="38" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv16" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            <strong>Sides</strong>
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtSides" type="text" value="<?php echo $rr->sidesDescription; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtSides" tabindex="39" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceSides" type="text" value="<?php echo $rr->sidesPrice; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceSides\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPriceSides" tabindex="40" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv17" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            <strong>Ends</strong>
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtEnds" type="text" value="<?php echo $rr->endsDescription; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtEnds" tabindex="41" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceEnds" type="text" value="<?php echo $rr->endsPrice; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceEnds\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPriceEnds" tabindex="42" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv18" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            <strong>Trim</strong>
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTrim" type="text" value="<?php echo $rr->trimDescription; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtTrim" tabindex="43" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceTrim" type="text" value="<?php echo $rr->trimPrice; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceTrim\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPriceTrim" tabindex="44" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv19" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            <strong>NON TAXABLE FEES:</strong>
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtNonTaxFees" type="text" value="<?php echo $rr->feesdescription; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtNonTaxFees" tabindex="45" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceNonTaxFees" type="text" value="<?php echo $rr->fees; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceNonTaxFees\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPriceNonTaxFees" tabindex="46" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv20" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--Tax Api Section-->
                            <div style="width: 320px; float: left;">
                                <div class="divRow" style="text-align: center; font-size: 15px;">
                                    <strong class="prntTitle">Installation Site</strong>
                                </div>
                                <div style="color: Red; text-align: center;" class="divNoPrint divRow">
                                    * Required for tax compulation
                                </div>
                                <div class="divNoPrint divRow">
                                    <strong>Same as Above</strong>&nbsp;<span style="font-weight:bold;"><input id="ContentPlaceHolder1_cntlManageOrder_chkSameAddress" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkSameAddress" onclick="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$chkSameAddress\&#39;,\&#39;\&#39;)&#39;, 0)" tabindex="47" /></span>
                                </div>
                                <div class="divRow divApiPrint">
                                    <strong>Address: </strong>
                                    <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtAddressApi" type="text" value="<?php echo $rr->buildingAddress; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtAddressApi" tabindex="48" class="dField3" style="width:250px;" />
                                    <span id="ContentPlaceHolder1_cntlManageOrder_reqAddressApi" style="color:Red;display:none;">Required</span>
                                </div>
                                <div class="divRow divApiPrint">
                                    <strong>City: </strong>
                                    <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtCityApi" type="text" value="<?php echo $rr->buildingCity; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtCityApi" tabindex="49" class="dField3" style="width:280px;" />
                                    <span id="ContentPlaceHolder1_cntlManageOrder_reqCityApi" style="color:Red;display:none;">Required</span>
                                </div>
                                <div class="divRow divApiPrint ">
                                    <strong>State: </strong>
                                    <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$ddlStateApi" id="ContentPlaceHolder1_cntlManageOrder_ddlStateApi" value="<?php echo $rr->buildingState; ?>" tabindex="50" class="dField3" style="width:270px;">
                                    <span id="ContentPlaceHolder1_cntlManageOrder_reqddlStateApi" style="color:Red;display:none;">Required</span>
                                </div>
                                <div class="divRow divApiPrint">
                                    <strong>Zip Code: </strong>
                                    <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtZipApi" type="text" value="<?php echo $rr->buildingZipCode; ?>" maxlength="5" id="ContentPlaceHolder1_cntlManageOrder_txtZipApi" tabindex="51" class="dField3" style="width:244px;" />
                                    <span id="ContentPlaceHolder1_cntlManageOrder_reqZipApi" style="color:Red;display:none;">Required</span>
                                </div>
                                <div class="divRow divApiPrint">
                                    <strong>Tax Exempt #</strong>
                                    <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTaxExemptAPI" type="text" valu="<?php echo $rr->taxExemptNum; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtTaxExemptAPI\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtTaxExemptAPI" tabindex="52" class="dField3" style="width:220px;" />
                                </div>
                                <div class="divRow divApiPrint">
                                    <strong>Total Tax: </strong>
                                    <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTaxRateApi" type="text" value="<?php echo $rr->tax; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtTaxRateApi" disabled="disabled" tabindex="53" class="aspNetDisabled dField3" style="width:244px;" />
                                </div>
                                <input type="button" name="ctl00$ContentPlaceHolder1$cntlManageOrder$btnGetTaxRate" value="Get Tax" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$cntlManageOrder$btnGetTaxRate&quot;, &quot;&quot;, true, &quot;api&quot;, &quot;&quot;, false, true))" id="ContentPlaceHolder1_cntlManageOrder_btnGetTaxRate" tabindex="54" class="btn btn-primary divNoPrint" /><br />

                                <div style="padding-top: 10px;">
                                    <div class="divRow divApiPrint">
                                        <strong>Total Sale: $ </strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTotalSale" type="text" value="<?php echo $rr->totalSale; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtTotalSale" disabled="disabled" tabindex="55" class="aspNetDisabled dField3" style="width:230px;" />
                                    </div>
                                    <div class="divRow divApiPrint">
                                        <strong>Tax: $</strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTax" type="text" value="<?php echo $rr->tax; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtTax" disabled="disabled" tabindex="56" class="aspNetDisabled dField3" style="width:270px;" />
                                    </div>
                                    <div class="divRow divApiPrint">
                                        <strong>Tax Exempt #: </strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTaxExempt" type="text" value="<?php echo $rr->taxExemptNum; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtTaxExempt\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtTaxExempt" tabindex="57" class="dField3" style="width:220px;" />
                                    </div>
                                    <div class="divRow divApiPrint">
                                        <strong>Total: $</strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTotal" type="text" value="<?php echo $rr->total; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtTotal" disabled="disabled" tabindex="58" class="aspNetDisabled dField3" style="width:261px;" />
                                    </div>
                                    <div class="divRow divApiPrint">
                                        <strong>10% Deposit: $</strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtDeposit10pct" type="text" value="<?php echo $rr->tenPercentDep; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtDeposit10pct\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtDeposit10pct" tabindex="59" class="dField3" style="width:211px;" /><br />
                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv21" style="color:Red;display:none;">must be numeric</span>
                                    </div>
                                    <div class="divRow divApiPrint">
                                        <strong>50% Deposit: $</strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtDeposit50pct" type="text" value="<?php echo $rr->fiftyPercentDep; ?>" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtDeposit50pct\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtDeposit50pct" tabindex="60" class="dField3" style="width:211px;" />
                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv22" style="color:Red;display:none;">must be numeric</span>
                                    </div>
                                    <div class="divRow divApiPrint">
                                        <strong>Balance: $</strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtBalance" type="text" value="<?php echo $rr->balance; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtBalance" disabled="disabled" tabindex="61" class="aspNetDisabled dField3" style="width:240px;" />
                                    </div>
                                </div>
                            </div>
                            <div id="ContentPlaceHolder1_cntlManageOrder_updateProgressApiCallEdit" style="display:none;">

                                <!--<img src="../images/spinner.gif" alt="" />-->

                            </div>
                            <!--End Tax Api Section-->

                        </div>
                        <div style="clear: both;">
                        </div>
                        <div style="padding-top: 10px;" class="divPrintLegal2">
                            <div class="divRow" id="divSpecs" style="float: left; clear: both; width: 624px;
                            padding-right: 15px;">
                                <table border="1" style="width: 600px;">
                                    <tr>
                                        <td style="padding: 3px;">
                                            <strong>Installation</strong>
                                        </td>
                                        <td style="padding: 3px;">
                                            <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkGround" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkGround" tabindex="62" <?php if ($rr->ground == 'true') {
                                                                                                                                                                                                                                        echo 'checked';
                                                                                                                                                                                                                                    } ?> /><label for="ContentPlaceHolder1_cntlManageOrder_chkGround">Ground</label></span>
                                            &nbsp;&nbsp;
                                            <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkCement" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkCement" tabindex="63" <?php if ($rr->cement == 'true') {
                                                                                                                                                                                                                                        echo 'checked';
                                                                                                                                                                                                                                    } ?> /><label for="ContentPlaceHolder1_cntlManageOrder_chkCement">Cement</label></span>
                                            &nbsp;&nbsp;
                                            <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkAsphalt" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkAsphalt" tabindex="64" <?php if ($rr->asphalt == 'true') {
                                                                                                                                                                                                                                        echo 'checked';
                                                                                                                                                                                                                                    } ?> /><label for="ContentPlaceHolder1_cntlManageOrder_chkAsphalt">Asphalt</label></span>
                                            &nbsp;&nbsp;
                                            <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkOther" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkOther" tabindex="65" <?php if ($rr->other == 'true') {
                                                                                                                                                                                                                                    echo 'checked';
                                                                                                                                                                                                                                } ?> /><label for="ContentPlaceHolder1_cntlManageOrder_chkOther">Other</label></span>
                                            &nbsp;&nbsp;
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOther" type="text" value="<?php echo $rr->txtOther; ?>" id="ContentPlaceHolder1_cntlManageOrder_txtOther" class="dField2" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px;">
                                            <strong>Land Level</strong>
                                        </td>
                                        <td style="padding: 2px;">
                                            <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkLandLevelY" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkLandLevelY" tabindex="66" <?php if ($rr->landLevelYes == 'true') {
                                                                                                                                                                                                                                                echo 'checked';
                                                                                                                                                                                                                                            } ?> /><label for="ContentPlaceHolder1_cntlManageOrder_chkLandLevelY">Yes</label></span>
                                            &nbsp;&nbsp;
                                            <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkLandLevelN" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkLandLevelN" tabindex="67" <?php if ($rr->landlevelNo == 'true') {
                                                                                                                                                                                                                                                echo 'checked';
                                                                                                                                                                                                                                            } ?> /><label for="ContentPlaceHolder1_cntlManageOrder_chkLandLevelN">No</label></span>
                                            &nbsp;&nbsp; (Building will be installed "as is")
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px;">
                                            <strong>Electricity</strong>
                                        </td>
                                        <td style="padding: 2px;">
                                            <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkElecYes" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkElecYes" tabindex="68" <?php if ($rr->electricityYes == 'true') {
                                                                                                                                                                                                                                        echo 'checked';
                                                                                                                                                                                                                                    } ?> /><label for="ContentPlaceHolder1_cntlManageOrder_chkElecYes">Yes</label></span>
                                            &nbsp;&nbsp;
                                            <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkElecNo" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkElecNo" tabindex="69" <?php if ($rr->electricityNo == 'true') {
                                                                                                                                                                                                                                        echo 'checked';
                                                                                                                                                                                                                                    } ?> /><label for="ContentPlaceHolder1_cntlManageOrder_chkElecNo">No</label></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px;">
                                            <strong>Payment Method</strong>
                                        </td>
                                        <td style="padding: 2px;">
                                            <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkCash" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkCash" tabindex="70" <?php if ($rr->cash == 'true') {
                                                                                                                                                                                                                                    echo 'checked';
                                                                                                                                                                                                                                } ?> /><label for="ContentPlaceHolder1_cntlManageOrder_chkCash">Cash</label></span>
                                            &nbsp;&nbsp;
                                            <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkCheck" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkCheck" tabindex="71" <?php if ($rr->check == 'true') {
                                                                                                                                                                                                                                    echo 'checked';
                                                                                                                                                                                                                                } ?> /><label for="ContentPlaceHolder1_cntlManageOrder_chkCheck">Check</label></span>
                                            &nbsp;&nbsp;
                                            <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkCC" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkCC" tabindex="72" <?php if ($rr->cc == 'true') {
                                                                                                                                                                                                                                echo 'checked';
                                                                                                                                                                                                                            } ?> /><label for="ContentPlaceHolder1_cntlManageOrder_chkCC">C.C.</label></span>
                                            &nbsp;&nbsp;
                                            <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkPO" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkPO" tabindex="73" <?php if ($rr->po == 'true') {
                                                                                                                                                                                                                                echo 'checked';
                                                                                                                                                                                                                            } ?> /><label for="ContentPlaceHolder1_cntlManageOrder_chkPO">P.O.</label></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="float: left; width: 320px;">
                                <strong>Special Instructions: </strong>
                                <br />
                                <textarea name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtInstructions" rows="4" cols="42" id="ContentPlaceHolder1_cntlManageOrder_txtInstructions" tabindex="74" class="dField2" disabled><?php echo $rr->instructions; ?></textarea>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div class="divPrintLegal">
                            <h4 style="font-weight: bold;">
                                Things You Should Know....</h4>
                            <p class="divPrintLegal">
                                <strong><?php
                                        switch ($_SESSION['org_id']) {
                                            case "162534":
                                                echo 'Northedge Steel is not responsible for permits, covenant searches or restrictions. Please contact your local Building Inspector or Homeowner’s Association for Information and notify our company of any requirements for which we must comply. The installation site must be level to qualify for our free installation. If it is not, it is the buyer’s responsibility to cover any additional labor and/or provide the materials necessary to make it so. Prior to installation, please have any underground cables,gas lines, or any utility lines located and marked. Northedge Steel will not be responsible for any damage to underground utility lines. NO MORE than the required deposit is to be paid to the dealer. Northedge Steel will not be responsible for any monies paid to the dealer above the required deposit amount and such payments will delay the process of your order until those funds have been paid in-full to Northedge Steel by the dealer. Paying in full to the dealer will not speed up your installation date. All deposits are nonrefundable. Please be advised that installation times are subject to change due to contractor availability and weather conditions. If the contractor is unable to complete your job due to site complications or inaccurate measurements a $200 or 5% return fee, whichever is greaterF';
                                                break;
                                            default:
                                                echo 'All Steel Carports, Inc., (hereinafter "ASC"). is not responsible for permits,
                                                covenanant searches or restrictions.</strong> Please contact your local Building
                                            Inspector or Homeowners Association for Information. <strong>Lot must be level</strong>
                                            or unit will be installed <strong>"AS IS"</strong> on lot. Prior to Installation,
                                            please have any underground cables, gas lines or any other utility lines located
                                            and marked. ASC will not be responsible for any damage to underground utility lines.
                                            <strong>Installation is subject to change due to contractor availability and weather
                                                conditions. A down payment of 10% (before tax) is required on all orders and 50%
                                                on special orders. Any credit card refunds will be charged 5%. Balance must be paid
                                                in full upon installation.</strong> No refunds on special orders. Deposits are
                                            non-refundable. A $50 return trip fee (subcontractor) applies. <strong>Additional fees
                                                may vary upon jobs (cuts, ground leveling, or anything additional the contractors
                                                have to do extra on site).</strong> A fee of $35 will apply on returned checks.
                                            <strong>All Steel Carports will not be held liable for any property damage don by self-employed
                                                contractors.</strong>';
                                        }
                                        ?>
                            </p>
                            <p class="divPrintLegal">
                            <?php
                                        switch ($_SESSION['org_id']) {
                                            case "162534":
                                                echo 'Buyer agrees that the balance shall be due and payable in full at the time of installation. If balances due and owing at the time of installation are not paid in full, buyer shall be in default under this agreement and subject to a $50 late fee. At its sole discretion Northedge Steel may assess interest at a rate of 18% per annum, on any unpaid balance. In the event of any unpaid balance buyer agrees to allow Northedge Steel access to the property to repossess the building. Buyer agrees that in the event of any default under this agreement, buyer shall be responsible for reasonable collection agency costs, any attorney’s fees and cost incurred because of the default. JURISDICTION, it is expressly agreed that in any dispute, suit, claim or legal proceeding of any nature arising from the obligations under this agreement, shall be led in a court of competent jurisdiction in Henry County , Indiana and be controlled by the law of the State of Indiana. Northedge Steel reserves the right to terminate this agreement at any time. Northedge Steel must approve all pricing before contract is valid.';
                                                break;
                                            default:
                                                echo 'Buyer agrees that the balance shall be due and payable at the time of installation.
                                                In the event that balances due and owing at the time of installation are not paid
                                                in full, buyer shall be in default under this agreement. ASC may elect to repossess
                                                the carport/garage and buyer hereby consent to allow ASC access to the carport/garage
                                                to repossess the carport/garage, or at its sole discretion ASC may assess interest
                                                at a rate of 18% per annum on any unpaid balance. Buyer agrees that in the event
                                                of any default under this agreement, buyer shall be responsible for reasonable collection
                                                agency costs, any attorney´s fees and costs incurred as a result of the default.
                                                <strong>JURISDICTION,</strong> it is expressly agreed that in any dispute, suit,
                                                claim or legal proceeding of any nature arising from the obligations under this
                                                agreement, shall be filed in a court of competent jurisdiction in Delaware County,
                                                Indiana and be controlled by the law of the State of Indiana. ASC reserves the right
                                                to terminate this agreement at any time.';
                                        }
                                        ?>
                                
                            </p>
                            <div style="text-align: center;" class="divPrintLegal">
                                <strong>I have read and completely understand the above information and give my approval
                                    for construction of the above.</strong><br />
                            </div>
                            <p>
                                <strong>Buyer:</strong>____________________________________________ &nbsp; &nbsp;
                                Date:_________
                                <br />
                                <br />
                                <strong>Contractor Name:</strong>__________________________________ &nbsp; &nbsp;
                                Date:__________
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</body>

<script>
    var inputs = document.getElementsByTagName("INPUT");
    for (var i = 0; i < inputs.length; i++) {
        //if (inputs[i].type === 'submit') {
        inputs[i].disabled = true;
        //}
    }
</script>

</html>