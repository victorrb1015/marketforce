<div class="modal fade" role="dialog" tabindex="-1" id="completeRouteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="text-center modal-title">Complete Route ID: <span id="cRouteID"></span></h3>
                </div>
              <form action="scheduling/php/complete-route.php" method="post">
                <input type="hidden" id="crid" name="crid" value="">
                <input type="hidden" id="csnum" name="csnum" value="">
                <div class="modal-body">
                  <div class="row" id="main_row">
                     
                  </div>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <button class="btn btn-success" type="submit">Complete Route</button>
                </div>
              </form>
            </div>
        </div>
    </div>