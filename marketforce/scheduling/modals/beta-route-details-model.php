<div class="modal fade" role="dialog" tabindex="-1" id="routeDetails">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="text-center modal-title">Route Details &nbsp; <button type="button" class="btn btn-success btn-sm" onclick="printDiv('routeDetails');"><i class="fa fa-print"></i> Print</button></h3>
                </div>
                <div class="modal-body" id="detail_body">
                    <!--<div class="row" style="background-color:rgb(51,122,183);">
                        <div class="col-md-6">
                            <h3 class="text-right" style="color:rgb(255,255,255);">Installer Name:</h3>
                        </div>
                        <div class="col-md-6"><input type="text" id="iName" placeholder="Installer Name" class="form-control" style="margin:15px 0px 10px;"></div>
                    </div>
                    <hr>
              <div>
                   <div class="row" style="border:2px solid rgb(51,122,183);">
                        <div class="col-md-12">
                            <h4 class="text-uppercase text-center" style="color:red;"><strong>Stop 1</strong></h4>
                            <p class="text-center" style="background:red;border-radius:10px;padding:5px;font-weight:bold;">
                              Invoice: 245018 
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                              <a href="#"><i class="fa fa-file-pdf-o"></i> View Invoice</a></p>
                        </div>
                        <div class="col-md-6">
                            <h4 class="text-center">FLORIDA STORAGE</h4>
                            <p class="text-center">Customer: John Smith</p>
                            <p class="text-center"><strong>Cost: $345</strong></p>
                        </div>
                        <div class="col-md-6">
                            <h4 class="text-center">Address</h4>
                            <p class="text-center">465 Sally Dr.</p>
                            <p class="text-center">Orlando, Fl 32789</p>
                        </div>
                        <div class="col-md-4" style="border-top:1px solid #9D9D9D;">
                            <div class="checkbox"><label><input type="checkbox">Customer Confirmed</label></div>
                        </div>
                        <div class="col-md-8" style="border-top:1px solid #9D9D9D;">
                            <p class="text-center" style="font-weight:bold;margin:0px 0px 5px;">Customer Notes:</p><textarea class="form-control" style="margin-bottom:5px;"></textarea></div>
                    </div>
                    <div class="row" style="border:2px solid rgb(51,122,183);">
                        <div class="col-md-12">
                            <h4 class="text-uppercase text-center" style="color:red;"><strong>Stop 1</strong></h4>
                            <p class="text-center" style="background:red;border-radius:10px;padding:5px;font-weight:bold;">
                              Invoice: 245018 
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                              <a href="#"><i class="fa fa-file-pdf-o"></i> View Invoice</a></p>
                        </div>
                        <div class="col-md-6">
                            <h4 class="text-center">FLORIDA STORAGE</h4>
                            <p class="text-center">Customer: John Smith</p>
                            <p class="text-center"><strong>Cost: $345</strong></p>
                        </div>
                        <div class="col-md-6">
                            <h4 class="text-center">Address</h4>
                            <p class="text-center">465 Sally Dr.</p>
                            <p class="text-center">Orlando, Fl 32789</p>
                        </div>
                        <div class="col-md-4" style="border-top:1px solid #9D9D9D;">
                            <div class="checkbox"><label><input type="checkbox">Customer Confirmed</label></div>
                        </div>
                        <div class="col-md-8" style="border-top:1px solid #9D9D9D;">
                            <p class="text-center" style="font-weight:bold;margin:0px 0px 5px;">Customer Notes:</p><textarea class="form-control" style="margin-bottom:5px;"></textarea></div>
                    </div>
                    <div class="row" style="border:2px solid rgb(51,122,183);">
                        <div class="col-md-12">
                            <h4 class="text-uppercase text-center" style="color:red;"><strong>Stop 1</strong></h4>
                            <p class="text-center" style="background:red;border-radius:10px;padding:5px;font-weight:bold;">
                              Invoice: 245018 
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                              <a href="#"><i class="fa fa-file-pdf-o"></i> View Invoice</a></p>
                        </div>
                        <div class="col-md-6">
                            <h4 class="text-center">FLORIDA STORAGE</h4>
                            <p class="text-center">Customer: John Smith</p>
                            <p class="text-center"><strong>Cost: $345</strong></p>
                        </div>
                        <div class="col-md-6">
                            <h4 class="text-center">Address</h4>
                            <p class="text-center">465 Sally Dr.</p>
                            <p class="text-center">Orlando, Fl 32789</p>
                        </div>
                        <div class="col-md-4" style="border-top:1px solid #9D9D9D;">
                            <div class="checkbox"><label><input type="checkbox">Customer Confirmed</label></div>
                        </div>
                        <div class="col-md-8" style="border-top:1px solid #9D9D9D;">
                            <p class="text-center" style="font-weight:bold;margin:0px 0px 5px;">Customer Notes:</p><textarea class="form-control" style="margin-bottom:5px;"></textarea></div>
                    </div>
                  </div>--><!--End Detail_Body-->
                </div>
                <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Close</button><button class="btn btn-success" type="button" onclick="save_route_details();">Save</button></div>
            </div>
        </div>
    </div>