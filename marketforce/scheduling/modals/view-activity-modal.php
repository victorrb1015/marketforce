<div class="modal fade" role="dialog" tabindex="-1" id="viewActivity">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="text-center modal-title">View Scheduler Activity</h3>
                </div>
                <div class="modal-body" style="max-height:80vh;overflow:scroll;">
                    <?php 
                        //echo file_get_contents( "scheduling/activity-log.txt" ); 
                        //include nl2br('scheduling/activity-log.txt');
                        $fh = fopen("scheduling/activity-log.txt", 'r');
                        $pageText = fread($fh, 25000);
                        echo nl2br($pageText);
                    ?>
                </div>
                <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Close</button></div>
            </div>
        </div>
    </div>