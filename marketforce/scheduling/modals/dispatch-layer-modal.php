<div class="modal fade" role="dialog" tabindex="-1" id="layerModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="text-center modal-title">Dispatch Location & State Layers</h3>
                </div>
                <div class="modal-body" style="max-height:80vh;overflow:scroll;">
                  
                  <!--State Selector Column-->
              <div class="col-lg-12">
                <h5 class="alert alert-warning"><i class="fa fa-location-arrow"></i> Dispatch Location</h5>
                <?php
                if($_SESSION['schedule_state'] == 'Oklahoma'){
                  echo '<h4><u>Depart From:</u> <br><br><mark id="depart_from">Oklahoma Office</mark></h4>
                        <div style="text-align:center;">
                          <br>
                          <button type="button" id="to_OK" class="btn btn-md btn-danger" style="display:none;">Switch To Oklahoma</button>
                          <button type="button" id="to_IN" class="btn btn-md btn-danger">Switch To Indiana</button>
                          <br><br>
                          </div>';
                }else{
                  echo '<h4><u>Depart From:</u> <br><br><mark id="depart_from">Indiana Office</mark></h4>
                        <div style="text-align:center;">
                          <br>
                          <button type="button" id="to_OK" class="btn btn-md btn-danger">Switch To Oklahoma</button>
                          <button type="button" id="to_IN" class="btn btn-md btn-danger" style="display:none;">Switch To Indiana</button>
                          <br><br>
                          </div>';
                }
                ?>
                </div>
                
              <div class="col-lg-12">
                <h5 class="alert alert-danger"><i class="fa fa-map-marker"></i> State Layers</h5>
                <button type="button" class="btn btn-warning btn-xs" id="clear_all_layers"><i class="fa fa-circle-o"></i> Click All Boxes</button>
                <br><br>
                <label title="Arizona"><input type="checkbox" value="Yes" name="AZ_CB" checked> Arizona</label><br>
                <label title="Arkansas"><input type="checkbox" value="Yes" name="AR_CB" checked> Arkansas</label><br>
                <label title="Illinois"><input type="checkbox" value="Yes" name="IL_CB" checked> Illinois</label><br>
                <label title="Indiana"><input type="checkbox" value="Yes" name="IN_CB" checked> Indiana</label><br>
                <label title="Kansas"><input type="checkbox" value="Yes" name="KS_CB" checked> Kansas</label><br>
                <label title="Kentucky"><input type="checkbox" value="Yes" name="KY_CB" checked> Kentucky</label><br>
                <label title="Maryland"><input type="checkbox" value="Yes" name="MD_CB" checked> Maryland</label><br>
                <label title="Michigan"><input type="checkbox" value="Yes" name="MI_CB" checked> Michigan</label><br>
                <label title="Mississippi"><input type="checkbox" value="Yes" name="MS_CB" checked> Mississippi</label><br>
                <label title="Missouri"><input type="checkbox" value="Yes" name="MO_CB" checked> Missouri</label><br>
                <label title="Nebraska"><input type="checkbox" value="Yes" name="NE_CB" checked> Nebraska</label><br>
                <label title="Oklahoma"><input type="checkbox" value="Yes" name="OK_CB" checked> Oklahoma</label><br>
                <label title="Ohio"><input type="checkbox" value="Yes" name="OH_CB" checked> Ohio</label><br>
                <label title="Pennsylvania"><input type="checkbox" value="Yes" name="PA_CB" checked> Pennsylvania</label><br>
                <label title="Tennessee"><input type="checkbox" value="Yes" name="TN_CB" checked> Tennessee</label><br>
                <label title="Texas"><input type="checkbox" value="Yes" name="TX_CB" checked> Texas</label><br>
                <label title="West Virginia"><input type="checkbox" value="Yes" name="WV_CB" checked> West Virginia</label><br>
              </div>
                  
                </div>
                <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Close</button></div>
            </div>
        </div>
    </div>



