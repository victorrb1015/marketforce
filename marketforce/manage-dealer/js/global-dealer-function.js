function mark_dealer_global(did){
	if(document.getElementById('global_cb').checked === true){
		var global = 'Yes';
		//alert('Yes');
	}else{
		var global = 'No';
		//alert('No');
	}
	
	if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
      	if(global === 'Yes'){
      		document.getElementById('save_message').innerHTML = 'Marked as Global';
      		setTimeout(function(){
      			document.getElementById('save_message').innerHTML = '';
      		},3000);
      	}else{
      		document.getElementById('save_message').innerHTML = 'Marked as Not Global';
      		setTimeout(function(){
      			document.getElementById('save_message').innerHTML = '';
      		},3000);
      	}
      }else{
      	alert('PROBLEM');
      	console.warn('There was an issue marking the dealer as global');
      }
    }
  }
  xmlhttp.open("GET","manage-dealer/php/mark-global-dealer.php?did="+did+"&global="+global,true);
  xmlhttp.send();
}