<script>
	//Email Forms...
	function email_form(did){
		var conf = confirm("Are you sure you want to email this form?");
		if(conf === false){
			return;
		}
		var email = prompt("Please enter the email address in which to send the form:");
		
		if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      //window.location.reload();
    }
  }
  xmlhttp.open("GET","manage-dealer/email/send-email.php?did="+did+"&email="+email+"&rep_id=<?php echo $_SESSION['user_id']; ?>",true);
  xmlhttp.send();
	}
	
	//Cancel Additional Forms...
  function cancel_form(fid){
		var conf = confirm("Are you sure you want to cancel this form?");
		if(conf === false){
			return;
		}
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","manage-dealer/php/cancel-form.php?fid="+fid,true);
  xmlhttp.send();
  }
	
	
		//Remove SitePlan...
  function remove_siteplan(sid){
		var conf = confirm("Are you sure you want to remove this Site Plan?");
		if(conf === false){
			return;
		}
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","manage-dealer/php/remove-siteplan.php?sid="+sid,true);
  xmlhttp.send();
  }
	
	
	function send_completed(id){
      var rep_id = '<?php echo $_SESSION['user_id']; ?>';
      var rep_name = '<?php echo $_SESSION['full_name']; ?>';
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			alert(this.responseText);
      //window.location.reload();
      
    }
  }
  xmlhttp.open("GET","forms/new-dealer-form/php/send-complete-packet.php?id="+id+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
		}
</script>