
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Addresses</h3>
                </div>
                <div class="panel-body"  style="height: 232px;">
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading text-center">
                                <h3 class="panel-title">Physical</h3>
                            </div>
                            <div class="panel-body" style="height: 142px;">
                                <div class="col-md-12">
                                    <p class="text-primary">Address:</p><input type="text" id="mdo_address1_physical" style="width:100%;" name="mdo_address1_physical" placeholder="Address"></div>
                                <div class="col-md-4">
                                    <p class="text-primary">City:</p><input type="text" id="mdo_city1_physical" style="width:100%;" name="mdo_city1_physical" placeholder="City"></div>
                                <div class="col-md-4">
                                    <p class="text-primary">State:</p><input type="text" id="mdo_state1_physical" style="width:100%;" name="mdo_state1_physical" placeholder="State"></div>
                                <div class="col-md-4">
                                    <p class="text-primary">Zip:</p><input type="text" id="mdo_zip1_physical" style="width:100%;" name="mdo_zip1_physical" placeholder="Zip Code"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading text-center">
                                <h3 class="panel-title">Mailing</h3>
                            </div>
                            <div class="panel-body" style="height: 142px;">
                                <div class="col-md-12">
                                    <p class="text-primary">Address:</p><input type="text" id="mdo_address2_mailing" style="width:100%;" name="mdo_address2_mailing" placeholder="Address"></div>
                                <div class="col-md-4">
                                    <p class="text-primary">City:</p><input type="text" id="mdo_city2_mailing" style="width:100%;" name="mdo_city2_mailing" placeholder="City"></div>
                                <div class="col-md-4">
                                    <p class="text-primary">State:</p><input type="text" id="mdo_state2_mailing" style="width:100%;" name="mdo_state2_mailing" placeholder="State"></div>
                                <div class="col-md-4">
                                    <p class="text-primary">Zip:</p><input type="text" id="mdo_zip2_mailing" style="width:100%;" name="mdo_zip2_mailing" placeholder="Zip Code"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading text-center">
                                <h3 class="panel-title">Shipping</h3>
                            </div>
                            <div class="panel-body" style="height: 142px;">
                                <div class="col-md-12">
                                    <p class="text-primary">Address:</p><input type="text" id="mdo_address3_shipping" style="width:100%;" name="mdo_address3_shipping" placeholder="Address"></div>
                                <div class="col-md-4">
                                    <p class="text-primary">City:</p><input type="text" id="mdo_city3_shipping" style="width:100%;" name="mdo_city3_shipping" placeholder="City"></div>
                                <div class="col-md-4">
                                    <p class="text-primary">State:</p><input type="text" id="mdo_state3_shipping" style="width:100%;" name="mdo_state3_shipping" placeholder="State"></div>
                                <div class="col-md-4">
                                    <p class="text-primary">Zip:</p><input type="text" id="mdo_zip3_shipping" style="width:100%;" name="mdo_zip3_shipping" placeholder="Zip Code"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>