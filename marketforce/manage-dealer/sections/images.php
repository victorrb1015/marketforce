<div class="col-lg-6">
 <div class="panel panel-primary">
   <div class="panel-heading">
       <h3 class="panel-title">Dealer Lot Images</h3>
   </div>
   <div class="panel-body">
	<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
	  <div class="carousel-inner">
      <?php
      $diq = "SELECT * FROM `dealer_imgs` WHERE `inactive` != 'Yes' AND `dealer_id` = '" . $_GET['did'] . "'";
      $dig = mysqli_query($conn, $diq) or die($conn->error);
      $ii = 1;
      while($dir = mysqli_fetch_array($dig)){
        if($ii == 1){
          $active = 'active';
        }else{
          $active = '';
        }
	      echo '<div class="item ' . $active . '">
                <a href="' . $dir['url'] . '" target="_blank"><img class="d-block w-100" style="max-height:350px;text-align:center;margin:auto;" src="' . $dir['url'] . '" alt="Image slide"></a>
              </div>';
        $ii++;
      }
      ?>
	  </div>
	  <a class="left carousel-control" href="#carouselExampleControls" role="button" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="right carousel-control" href="#carouselExampleControls" role="button" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
	</div>
</div>
</div>
</div>