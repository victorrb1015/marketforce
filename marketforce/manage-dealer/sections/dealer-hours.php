<?php
$q = "SELECT * FROM `dealers` WHERE `ID` = '" . $_GET['did'] . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
?>
 <div class="col-lg-6 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Office Hours</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive" style="height: 367px;">
                        <table class="table">
                            <thead>
                                <tr></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="info">Day</td>
                                    <td class="info">Open</td>
                                    <td class="info">Close</td>
                                    <td class="info">Not Open</td>
                                </tr>
                                <tr>
                                    <td>Sunday</td>
                                    <td class="active"><input type="text" id="sun_open_mdo" value="<?php echo $r['sun_open']; ?>" style="width:75%;" name="sun_open_mdo"></td>
                                    <td class="active"><input type="text" id="sun_close_mdo" style="width:75%;" name="sun_close_mdo" value="<?php echo $r['sun_close']; ?>"></td>
                                    <td><input type="checkbox" class="form-control" id="sun_not_open_683" name="sun_not_open_683" <?php if($r['sun_status'] == 'Closed'){ echo 'checked';} ?>></td>
                                </tr>
                                <tr>
                                    <td>Monday</td>
                                    <td class="active"><input type="text" id="mon_open_mdo" style="width:75%;" name="mon_open_mdo" value="<?php echo $r['mon_open']; ?>"></td>
                                    <td class="active"><input type="text" id="mon_close_mdo" style="width:75%;" name="mon_close_mdo" value="<?php echo $r['mon_close']; ?>"></td>
                                    <td><input type="checkbox" class="form-control" id="mon_not_open_683" name="mon_not_open_683" <?php if($r['mon_status'] == 'Closed'){ echo 'checked';} ?>></td>
                                </tr>
                                <tr>
                                    <td>Tuesday</td>
                                    <td class="active"><input type="text" id="tue_open_mdo" style="width:75%;" name="tue_open_mdo"  value="<?php echo $r['tue_open']; ?>"></td>
                                    <td class="active"><input type="text" id="tue_close_mdo" style="width:75%;" name="tue_close_mdo" value="<?php echo $r['tue_close']; ?>"></td>
                                    <td><input type="checkbox" class="form-control" id="tue_not_open_683" name="tue_not_open_683" <?php if($r['tue_status'] == 'Closed'){ echo 'checked';} ?>></td>
                                </tr>
                                <tr>
                                    <td>Wednesday</td>
                                    <td class="active"><input type="text" id="wed_open_mdo" style="width:75%;" name="wed_open_mdo" value="<?php echo $r['wed_open']; ?>"></td>
                                    <td class="active"><input type="text" id="wed_close_mdo" style="width:75%;" name="wed_close_mdo" value="<?php echo $r['wed_close']; ?>"></td>
                                    <td><input type="checkbox" class="form-control" id="wed_not_open_683" name="wed_not_open_683" <?php if($r['wed_status'] == 'Closed'){ echo 'checked';} ?>></td>
                                </tr>
                                <tr>
                                    <td>Thursday</td>
                                    <td class="active"><input type="text" id="thu_open_mdo" style="width:75%;" name="thu_open_mdo" value="<?php echo $r['thu_open']; ?>"></td>
                                    <td class="active"><input type="text" id="thu_close_mdo" style="width:75%;" name="thu_close_mdo" value="<?php echo $r['thu_close']; ?>"></td>
                                    <td><input type="checkbox" class="form-control" id="thu_not_open_683" name="thu_not_open_683" <?php if($r['thu_status'] == 'Closed'){ echo 'checked';} ?>></td>
                                </tr>
                                <tr>
                                    <td>Friday</td>
                                    <td class="active"><input type="text" id="fri_open_mdo" style="width:75%;" name="fri_open_mdo" value="<?php echo $r['fri_open']; ?>"></td>
                                    <td class="active"><input type="text" id="fri_close_mdo" style="width:75%;" name="fri_close_mdo" value="<?php echo $r['fri_close']; ?>"></td>
                                    <td><input type="checkbox" class="form-control" id="fri_not_open_683" name="fri_not_open_683" <?php if($r['fri_status'] == 'Closed'){ echo 'checked';} ?>></td>
                                </tr>
                                <tr>
                                    <td>Saturday</td>
                                    <td class="active"><input type="text" id="sat_open_mdo" style="width:75%;" name="sat_open_mdo" value="<?php echo $r['sat_open']; ?>"></td>
                                    <td class="active"><input type="text" id="sat_close_mdo" style="width:75%;" name="sat_close_mdo" value="<?php echo $r['sat_close']; ?>"></td>
                                    <td><input type="checkbox" class="form-control" id="sat_not_open_683" name="sat_not_open_683" <?php if($r['sat_status'] == 'Closed'){ echo 'checked';} ?>></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>