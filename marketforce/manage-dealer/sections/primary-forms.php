<div class="col-lg-4 col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Primary Forms</h3>
                </div>
                <div class="panel-body" style="overflow:scroll;">
                  <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped packet-table">
                      <thead>
                        <tr>
                          <th>Form Name</th>
                          <th>Status</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $dfq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $_GET['did'] . "'";
                        $dfg = mysqli_query($conn, $dfq) or die($conn->error);
                        $dfr = mysqli_fetch_array($dfg);
                        $dsq = "SELECT * FROM `signatures` WHERE `dealer_id` = '" . $_GET['did'] . "'";
                        $dsg = mysqli_query($conn, $dsq) or die($conn->error);
                        $dsr = mysqli_fetch_array($dsg);
                        ?>
                        <tr>
                          <td>Dealer Info Form</td>
                          <td class="text-center"><i class="fa fa-check" style="color:green;font-size:24px;"></i></td>
                          <td>
                            <a href="forms/new-dealer-form/dealer-info-form.php?id=<?php echo $_GET['did']; ?>" target="_blank">
                              <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</button>
                            </a>
                            <a href="forms/editing_forms/index.php?id=<?php echo $_GET['did']; ?>&user=<?php echo $_SESSION['user_id']; ?>" target="_blank">
                              <button type="button" class="btn btn-warning btn-sm" style="color:black;"><i class="fa fa-pencil"></i> Edit</button>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>Dealer Contract Form</td>
                          <?php
                          //Get Contract Form Name...
                          $noDate = strtotime("0000-00-00");
                          $ddate = strtotime($dsr['ddate']);
                          $cdate = strtotime("30 May 2019");
                          if($cdate > $ddate && $ddate != $noDate){
                            $contract_form_name = 'dealer-contract.php';
                          }else{
                            $contract_form_name = 'dealer-contract-2019.php';
                          }
                          $pComplete = true;
                            if($dfr['contract_form'] == ''){
                              $url = 'forms/new-dealer-form/' . $contract_form_name . '?id=' . $_GET['did'];
                              if($dsr['dsign'] == '{"lines":[]}'){
                                $sIcon = 'fa fa-times';
                                $sColor = 'red';
                                $pComplete = false;
                              }else{
                                $sIcon = 'fa fa-check';
                                $sColor = 'green';
                              }
                            }else{
                              $url = $dfr['contract_form'];
                              $sIcon = 'fa fa-check';
                              $sColor = 'green';
                            }
                            ?>
                          <td class="text-center"><i class="<?php echo $sIcon; ?>" style="color:<?php echo $sColor; ?>;font-size:24px;"></i></td>
                          <td>
                            <a href="<?php echo $url; ?>" target="_blank">
                              <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</button>
                            </a>
                            <a href="<?php echo $url; ?>" target="_blank">
                              <button type="button" class="btn btn-warning btn-sm" style="color:black;"><i class="fa fa-pencil"></i> Edit</button>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>Landlord Consent Form</td>
                          <?php
                            if($dfr['consent_form'] == ''){
                              $url = 'forms/new-dealer-form/landlord-consent-form.php?id=' . $_GET['did'];
                              if($dsr['lsign'] == '{"lines":[]}'){
                                $sIcon = 'fa fa-times';
                                $sColor = 'red';
                                $pComplete = false;
                              }else{
                                $sIcon = 'fa fa-check';
                                $sColor = 'green';
                              }
                            }else{
                              $url = $dfr['consent_form'];
                              $sIcon = 'fa fa-check';
                              $sColor = 'green';
                            }
                            ?>
                          <td class="text-center"><i class="<?php echo $sIcon; ?>" style="color:<?php echo $sColor; ?>;font-size:24px;"></i></td>
                          <td>
                            <a href="<?php echo $url; ?>" target="_blank">
                              <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</button>
                            </a>
                            <a href="<?php echo $url; ?>" target="_blank">
                              <button type="button" class="btn btn-warning btn-sm" style="color:black;"><i class="fa fa-pencil"></i> Edit</button>
                            </a>
                            <?php
                            if($dfr['consent_form'] == ''){
                              echo '<form id="myForm" action="forms/new-dealer-form/php/upload-image.php" method="post" enctype="multipart/form-data">';
      												echo '<input style="display:none;" id="consent_form_upload" name="fileToUpload" type="file" onchange="form.submit();" required/><br>';
      												echo '<input type="hidden" id="id" name="id" value="' . $_GET['did'] . '" />';
      												echo '<input type="hidden" id="rr" name="rr" value="md" />';
    													echo '</form>';
                              echo '<button type="button" class="btn btn-success btn-sm" onclick="document.getElementById(\'consent_form_upload\').click();"><i class="fa fa-upload"></i> Upload</button>';
                            }else{
                              echo '<button type="button" class="btn btn-success btn-sm" onclick="email_form(' . $dfr['ID'] . ');"><i class="fa fa-send"></i> Email</button>';
                            }
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>Display Order Form</td>
                          <?php
                          $doq = "SELECT * FROM `display_orders` WHERE `ID` = '" . $_GET['did'] . "'";
                          $dog = mysqli_query($conn, $doq) or die($conn->error);
                          $dor = mysqli_fetch_array($dog);
                          if($dor['tcg'] == false && $dor['ip'] == false && $dor['g'] == false && $dor['cua'] == false && $dor['cur'] == false && $dor['rvp'] == false
                             && $dor['cst'] == false && $dor['csp'] == false && $dor['vb'] == false && $dor['u'] == false && $dor['au'] == false && $dor['us'] == false 
                             && $dor['vu'] == false && $dor['notes'] == ''){
                            $url = 'forms/new-dealer-form/display-order-form.php?id=' . $_GET['did'];
                            $sIcon = 'fa fa-times';
                            $sColor = 'red';
                          }else{
                            $url = 'forms/new-dealer-form/display-order-form.php?id=' . $_GET['did'];
                            $sIcon = 'fa fa-check';
                            $sColor = 'green';
                          }
                          echo '<td class="text-center"><i class="'. $sIcon . '" style="color:' . $sColor . ';font-size:24px;"></i></td>';
                          ?>
                          <td>
                            <a href="<?php echo $url; ?>" target="_blank">
                              <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</button>
                            </a>
                            <a href="<?php echo $url; ?>" target="_blank">
                              <button type="button" class="btn btn-warning btn-sm" style="color:black;"><i class="fa fa-pencil"></i> Edit</button>
                            </a>
                          </td>
                        <tr>
                          <td>Site Plan</td>
                          <?php
                            if($dfr['site_plan'] == ''){
                              $sIcon = 'fa fa-times';
                              $sColor = 'red';
                              $pComplete = false;
                              echo '<td class="text-center"><i class="'. $sIcon . '" style="color:' . $sColor . ';font-size:24px;"></i></td>';
                              echo '<form id="myForm" action="forms/new-dealer-form/php/upload-image.php" method="post" enctype="multipart/form-data">';
      												echo '<input style="display:none;" id="fileToUpload" name="fileToUpload" type="file" onchange="form.submit();" required/><br>';
      												echo '<input type="hidden" id="id" name="id" value="' . $_GET['did'] . '" />';
      												echo '<input type="hidden" id="rr" name="rr" value="md" />';
    													echo '</form>';
                              echo '<td><button type="button" class="btn btn-success" onclick="document.getElementById(\'fileToUpload\').click();"><i class="fa fa-upload"></i> Upload</button></td>';
                            }else{
                              $url = $dfr['site_plan'];
                              $sIcon = 'fa fa-check';
                              $sColor = 'green';
                              echo '<td class="text-center"><i class="'. $sIcon . '" style="color:' . $sColor . ';font-size:24px;"></i></td>
                                    <td>
                                      <a href="' . $url . '" target="_blank">
                                        <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</button>
                                      </a>
                                        <button type="button" class="btn btn-danger btn-sm" onclick="remove_siteplan(' . $_GET['did'] . ');"><i class="fa fa-trash"></i> Remove</button>
                                    </td>';
                            }
                            ?>
                        </tr>
                        <tr>
                          <td>Completed Dealer Packet</td>
                          <?php 
                          $cpq = "SELECT * FROM `new_packets` WHERE `inactive` != 'Yes' AND `form_type` = 'New Dealer Packet' AND `dealer_id` = '" . $_GET['did'] . "'";
                          $cpg = mysqli_query($conn, $cpq) or die($conn->error);
                          $cp = mysqli_num_rows($cpg);
                          
                          if($pComplete == true){
                            if($cp > 0){
                              $sIcon = 'fa fa-check';
                              $sColor = 'green';
                              echo '<td class="text-center"><i class="'. $sIcon . '" style="color:' . $sColor . ';font-size:24px;"></i></td>';
                              echo '<td>
                                        <a href="forms/new-dealer-form/print-packet.php?id=' . $_GET['did'] . '" target="_blank">
                                        <button type="button" class="btn btn-primary"><i class="fa fa-eye"></i> View Packet</button>
                                        </a>
                                    </td>';
                            }else{
                              $sIcon = 'fa fa-times';
                              $sColor = 'red';
                              echo '<td class="text-center"><i class="'. $sIcon . '" style="color:' . $sColor . ';font-size:24px;"></i></td>';
                              echo '<td><button type="button" class="btn btn-success" onclick="send_completed(' . $_GET['did'] . ');"><i class="fa fa-send"></i> Submit</button></td>';
                            }
                            
                          }else{
                            $sIcon = 'fa fa-times';
                            $sColor = 'red';
                            echo '<td class="text-center"><i class="'. $sIcon . '" style="color:' . $sColor . ';font-size:24px;"></i></td>';
                            echo '<td>Please complete all forms in order to submit.</td>';
                          }
                          ?>
                          
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>