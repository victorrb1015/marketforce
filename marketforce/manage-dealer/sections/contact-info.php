<div class="col-lg-4 col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Contact Information
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-success btn-xs" onclick="update_contact();" disabled><i class="fa fa-refresh"></i> Save Changes</button>
                    </h3>
                </div>
                <div class="panel-body">
                    <div>
                        <p><strong>Owner's Name:</strong></p>
                        <input type="text" id="mdo_owner_name" class="form-control" name="mdo_owner_name" value="<?php echo $ndir['ownername']; ?>">
                        <br>
                        <p><strong>Email:</strong></p>
                        <input type="text" id="mdo_email" class="form-control" name="mdo_email" value="<?php echo $ndir['email']; ?>">
                        <br>
                        <p><strong>Fax Number:</strong></p>
                        <input type="text" id="mdo_fax" class="form-control" name="mdo_fax" value="<?php echo $ndir['fax']; ?>">
                        <br>
                        <p><strong>Main Phone Number:</strong></p>
                        <input type="text" id="mdo_phone_cell" class="form-control" name="mdo_phone_cell" value="<?php echo $ndir['phone']; ?>">
                    </div>
                </div>
            </div>
        </div>