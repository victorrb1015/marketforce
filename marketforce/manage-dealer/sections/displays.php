<div class="col-lg-8">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Dealer Displays
                 <!-- &nbsp;&nbsp;
                  <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#newDisplaySaleModal" onclick="">
                    <i class="fa fa-plus"></i> New Display
                  </button>-->
                </h3>
            </div>
            <div class="panel-body" style="overflow:scroll;">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped packet-table">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <!--<th>Dealer</th>-->
                                            <th>Display Type</th>
                                            <th>Dimensions</th>
                                            <th>Display Color</th>
                                            <th>Status</th>
                                            <!--<th>Actions</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                            <?php
                            $ddq = "SELECT * FROM `dealer_displays` WHERE `inactive` != 'Yes' AND `status` != 'Sold' AND `dealer_id` = '" . $_GET['did'] . "'";
                            $ddg = mysqli_query($conn, $ddq) or die($conn->error);
                            while($ddr = mysqli_fetch_array($ddg)){
                                echo '<tr>
                                            <td>' . date("m/d/y",strtotime($ddr['date'])) . '</td>
                                            <!--<td>' . $ddr['dealer_name'] . '</td>-->
                                            <td>' . $ddr['display_type'] . '</td>
                                            <td>' . $ddr['length'] . 'x' . $ddr['width'] . 'x' . $ddr['height'] . '</td>
                                            <td>' . $ddr['color'] . '</td>';
                              if($ddr['sale_auth'] != 'Yes'){
                                      echo '<td><b>' . $ddr['status'] . '</b></td>';
                              }else{
                                      echo '<td><b>' . $ddr['status'] . '</b><br>$' . $ddr['sale_amount'] . '<hr>' . $ddr['notes'] . '</td>';
                              }
                             /* if($ddr['sale_auth'] != 'Yes'){
                                  echo '<td>
                                  <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#display-sale-authorization" onclick="load_display_modal(' . $ddr['ID'] . ');">Authorize Sale</button>
                                  <button class="btn btn-danger btn-sm" type="button" onclick="remove_display(' . $ddr['ID'] . ');"><i class="fa fa-times"></i> Remove</button>
                                  </td>';
                              }else{
                                  echo '<td>
                                  <button class="btn btn-success btn-sm" type="button" onclick="mark_display_sold(' . $ddr['ID'] . ');"><i class="fa fa-check"></i> Mark Sold</button>
                                  <button class="btn btn-danger btn-sm" type="button" onclick="remove_display(' . $ddr['ID'] . ');"><i class="fa fa-times"></i> Remove</button>
                                  </td>';
                              }*/
                                       echo '</tr>';
                            }
                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>