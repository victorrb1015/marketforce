<div class="col-lg-4 col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Additional Forms
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#newAdditionalFormModal"><i class="fa fa-plus"></i> New Form</button>
                    </h3>
                </div>
                <div class="panel-body" style="overflow:scroll;">
                    <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped packet-table">
                      <thead>
                        <tr>
                          <th>Date</th>
                          <th>Form Name</th>
                          <th>Status</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $dpq = "SELECT * FROM `new_packets` WHERE `inactive` != 'Yes' AND `dealer_id` = '" . $_GET['did'] . "' AND `form_type` != 'New Dealer Packet' ORDER BY `ID` DESC";
                        $dpg = mysqli_query($conn, $dpq) or die($conn->error);
                        while($dpr = mysqli_fetch_array($dpg)){
                         echo '<tr>
                                <td>' . date("m/d/y", strtotime($dpr['date'])) . '</td>
                                <td>' . $dpr['form_type'] . '</td>';
                         if($dpr['status'] == 'Pending Installation' || $dpr['status'] == 'Pending'){
                           echo '<td class="text-center"><i class="fa fa-clock-o" style="font-size:24px;" title="Pending"></i></td>';
                         }elseif($dpr['status'] == 'Completed'){
                           echo '<td class="text-center"><i class="fa fa-check" style="color:green;font-size:24px;" title="Completed"></i></td>';
                         }elseif($dpr['status'] == 'Cancelled'){
                           echo '<td class="text-center"><i class="fa fa-times" style="color:red;font-size:24px;" title="Cancelled"></i></td>';
                         }else{
                           echo '<td class="text-center"><i class="fa fa-exclamation-triangle" style="color:orange;font-size:24px;" title="Error"></i></td>';
                         }
                         if($dpr['form_type'] == 'Display Addon Form'){
                           $url = 'forms/new-dealer-form/display-addon-form.php?id=' . $_GET['did'];
                         }
                         if($dpr['form_type'] == 'Display Removal Form'){
                           $url = 'forms/new-dealer-form/display-removal-form.php?id=' . $_GET['did'];
                         }
                         if($dpr['form_type'] == 'Display Relocation Form'){
                           $url = 'forms/new-dealer-form/display-relocation-form.php?id=' . $_GET['did'];
                         }
                         if($dpr['form_type'] == 'Ownership Change Form'){
                           $url = 'forms/new-dealer-form/change-of-ownership-form.php?id=' . $_GET['did'];
                         }
                         if($dpr['form_type'] == 'Dealer Closing Form'){
                           $url = 'forms/new-dealer-form/dealer-closing-form.php?id=' . $_GET['did'];
                         }
                         echo '<td>
                                 <a href="' . $url . '" target="_blank">
                                   <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</button>
                                 </a>
                                 <button type="button" class="btn btn-danger btn-sm" onclick="cancel_form(' . $dpr['ID'] . ');"><i class="fa fa-times"></i> Cancel</button>
                                </td>';
                        }
                        ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>