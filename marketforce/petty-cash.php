<?php
include 'security/session/session-settings.php';

if(!isset($_SESSION['in'])){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';

//This is the GEOLOCATION Function that gave the system a hard time loading....
/*$user_ip = getenv('REMOTE_ADDR');
$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
$icountry = $geo["geoplugin_countryName"];
$icity = $geo["geoplugin_city"];
$istate = $geo["geoplugin_regionName"];
$idate = date("m/d/Y");*/

$RID = $_GET['rid'];

if($_SESSION['user_id'] == '1'){
  //error_reporting(E_ALL);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
		th, td{
			padding: 10px;
		}
		#dl, dc{
			height: 430px;
		}
		.panel-body{
			height: 382px;
		}
		#scroll{
			overflow: scroll;
		}
		::-webkit-scrollbar {
    width: 0px;  /* remove scrollbar space */
    background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
		
		
		.packet-table td, th{
			border: 1px solid black;
		}
		.packet-table{
			margin:auto;
		}
  </style>
  <script>
		
		function load_modal(id,name,mode){
			if(mode === 'Completed'){
				document.getElementById('completed_id').value = id;
				document.getElementById('ccrName').innerHTML = name;
			}
			if(mode === 'Need Info'){
				document.getElementById('info_id').value = id;
				document.getElementById('info_name').innerHTML = name;
			}
			if(mode === 'collection payment'){
				document.getElementById('approve_id').value = id;
			}
		}
		
		
		function encodeURL(url){
		url = url.replace(/&/g, '%26'); 
		url = url.replace(/#/g, '%23');
		return url;
	}
		
		
		function remove_expense(id){
			if(confirm("Are You Sure You Want To Delete This Item?")){
				
			}else{
				return;
			}
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","expenses/remove-expense.php?id="+id,true);
  xmlhttp.send();
		}
		
		
		function remove_report(id){
			if(confirm("Are You Sure You Want To Delete This Entire Report And All It's Contents?")){
				
			}else{
				return;
			}
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location = 'expense-reports.php';
    }
  }
  xmlhttp.open("GET","expenses/remove-report.php?id="+id,true);
  xmlhttp.send();
		}
    
		
		function submit_report(){
			var rep_id = '<? echo $_SESSION['user_id']; ?>';
			var rep_name = '<? echo $_SESSION['full_name']; ?>';
			
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","expenses/submit-report.php?rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
		}
		
		
		
		function view_report(rid){
			var rep_id = '<? echo $_SESSION['user_id']; ?>';
			var rep_name = '<? echo $_SESSION['full_name']; ?>';
			
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			//alert(this.responseText);
      //window.location.reload();
			document.getElementById('view_contents').innerHTML = this.responseText;
			<?php
				if($_SESSION['accountant'] != 'Yes'){
					echo '//alert("disable");
								$(".disable").prop("disabled", true);';
				}
			?>
    }
  }
  xmlhttp.open("GET","expenses/view-contents.php?rid="+rid+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
		}
   

		
		function cat_check(){
			var ccat = document.getElementById('cat').value;
			if(ccat === 'Mileage'){
				document.getElementById('amount-miles').innerHTML = '<p>Miles</p>'+
						'<input type="text" class="form-control" id="miles" name="miles" placeholder="0" required>';
				document.getElementById('iname').value = 'Mileage';
			}else{
				document.getElementById('amount-miles').innerHTML = '<p>Amount</p>'+
						'<input type="text" class="form-control usd" id="amount" name="amount" placeholder="0.00" required>';
				document.getElementById('iname').value = '';
			}
      
      if(ccat === 'Vehicle/Gas'){
      	document.getElementById('extra-row').innerHTML = '<div class="col-lg-6"><p>Price Per Gallon</p>'+
						'<input type="text" class="form-control usd" id="ppg" name="ppg" placeholder="0.00" required></div>'+
          '<div class="col-lg-6"><p># of Gallons</p><input type="text" class="form-control number" id="num_gallons" name="num_gallons" placeholder="0" required /></div>';
				document.getElementById('iname').value = 'Gas';
			}else{
				document.getElementById('extra-row').innerHTML = '';
				document.getElementById('iname').value = '';
			}
      }
			
		
		
		function check_item(id){
			var cstat = document.getElementById('approve_'+id).checked;
			
				if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			//alert(this.responseText);
      //window.location.reload();
			document.getElementById('check_res_'+id).innerHTML = this.responseText;
    }
  }
  xmlhttp.open("GET","expenses/check-item.php?id="+id+"&checkstat="+cstat,true);
  xmlhttp.send();
		}
		

		function edit_report(id,mode){
			if(mode === 'prior'){
				var nurl = 'expense-reports.php?rid='+id;
			}else{
				var nurl = 'expense-reports.php';
			}
			
			window.location = nurl;
		}
		
		
		function approve_report(id){
			var rep_name = '<? echo $_SESSION['full_name']; ?>';
			var rep_id = '<? echo $_SESSION['rep_id']; ?>';
			
				if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
			
    }
  }
  xmlhttp.open("GET","expenses/approve-report.php?id="+id+"&rep_name="+rep_name+"&rep_id="+rep_id,true);
  xmlhttp.send();
		}

  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> Petty Cash</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-usd"></i> Petty Cash
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							<?php
							echo '<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-danger">
										<div class="panel-heading">
											<h3 class="panel-title">
												Itemized Expenses &nbsp;&nbsp; 
												<button type="button" class="btn btn-success" data-toggle="modal" data-target="#newExpense">New Expense</button>';
							if(isset($_GET['rid'])){
								echo '&nbsp;&nbsp;
											<a href="expense-reports.php">
											<button type="button" class="btn btn-primary">View Current Expenses</button>
											</a>';
							}
										echo '</h3>
										</div>
										<div class="panel-body table-responsive" style="height:auto;">
											<!--<div class="table-responsive">-->
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
												<tr>
													<th>Date</th>
													<th>Item</th>
													<th>Category</th>
													<th>Location</th>
													<th>Amount</th>
													<th>Miles</th>
													<th>Notes</th>
													<th>Actions</th>
												</tr>
												</thead>
												<tbody>';
									$tamount = 0;
									$tmiles = 0;
							if(isset($_GET['rid'])){
									$eq = "SELECT * FROM `pc_transactions` WHERE `reportID` = '" . $_GET['rid'] . "' AND `inactive` != 'Yes' ORDER BY `item_date` ASC";
							}else{
									$eq = "SELECT * FROM `pc_transactions` WHERE `rep_id` = '" . $_SESSION['user_id'] . "' AND `reportID` = '' AND `inactive` != 'Yes' ORDER BY `item_date` ASC";
							}
								$eg = mysqli_query($conn, $eq) or die($conn->error);
								while($tdr = mysqli_fetch_array($eg)){
									echo '<tr>
												<td>' . date("m/d/Y", strtotime($tdr['item_date'])) . '</td>
									      <td>' . $tdr['item'] . '</td>
												<td>' . $tdr['category'] . '</td>
												<td>' . $tdr['city'] . ' ' . $tdr['state'] . '</td>
												<td>$' . $tdr['amount'] . '</td>
												<td>' . $tdr['miles'] . '</td>
												<td>' . $tdr['notes'] . '</td>
												<td>
													<button type="button" class="btn btn-danger btn-xs" onclick="remove_expense(' . $tdr['ID'] . ');"><i class="fa fa-times"></i></button>';
                  if($tdr['receipt_img'] != ''){
										echo '<a href="' . $tdr['receipt_img'] . '" target="_blank">
													<button type="button" class="btn btn-info btn-xs" title="View Receipt"><i class="fa fa-image"></i></button>
													</a>';
                  }else{
                    echo '&nbsp;<button type="button" class="btn btn-default btn-xs" title="View Receipt" disabled><i class="fa fa-image"></i></button>'; 
                  }
												echo '</td>
												</tr>';
									$tamount = $tamount + $tdr['amount'];
									$tmiles = $tmiles + $tdr['miles'];
								}
									echo '</tbody>';
								if(mysqli_num_rows($eg) > 0){
										echo '<tfoot>
										<tr>
											<td></td>
											<td><b>Total</b></td>
											<td></td>
											<td></td>
											<td><b>$' . $tamount . '</b></td>
											<td><b>' . $tmiles . ' Miles</b></td>
											<td></td>
											<td>';
									if(isset($_GET['rid'])){
										
									}else{
										echo '<button type="button" class="btn btn-success btn-sm" onclick="submit_report();">Submit</button>';
									}
										echo '</td>
										</tfoot>';
								}
									echo '</table>
										<!--</div>-->
										</div>
									</div>
								</div>
							</div><!--End Row-->';
							
						
							
							
						echo '<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Previous Expense Reports
											</h3>
										</div>
										<div class="panel-body table-responsive" style="height:auto;overflow:scroll;">
											<!--<div class="table-responsive">-->
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
												<tr>
													<th>Date</th>
													<th>Report ID</th>
													<th>Submitted By</th>
													<th>Approved By</th>
													<th>Approved Date</th>
													<th>Notes</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
												</thead>
												<tbody>';
							if($_SESSION['accountant'] == 'Yes'){
									$erq = "SELECT * FROM `pc_reports` WHERE `inactive` != 'Yes' ORDER BY `sub_date` DESC";
							}else{
									$erq = "SELECT * FROM `pc_reports` WHERE `rep_id` = '" . $_SESSION['user_id'] . "' AND `inactive` != 'Yes' ORDER BY `sub_date` DESC";
							}
								$erg = mysqli_query($conn, $erq) or die($conn->error);
								while($err = mysqli_fetch_array($erg)){
									if($err['approved_date'] == '0000-00-00'){
										$adate = '';
									}else{
										$adate = date("m/d/Y", strtotime($err['approved_date']));
									}
									if($err['status'] == 'Approved'){
										$approved = 'style="background:green;color:white;font-weight:bold;"';
									}else{
										$approved = '';
									}
									echo '<tr ' . $approved . '>
												<td>' . date("m/d/Y", strtotime($err['sub_date'])) . '</td>
									      <td>' . $err['reportID'] . '</td>
												<td>' . $err['rep_name'] . '</td>
												<td>' . $err['approved_by_name'] . '</td>
												<td>' . $adate . '</td>
												<td>
												
												</td>
												<td>' . $err['status'] . '</td>
												<td>';
									
									if($_SESSION['user_id'] == $err['rep_id'] && $err['status'] != 'Approved'){
										echo '<button type="button" class="btn btn-danger btn-xs" onclick="remove_report(\'' . $err['reportID'] . '\');"><i class="fa fa-times"></i></button>
													<button type="button" class="btn btn-warning btn-xs" onclick="edit_report(\'' . $err['reportID'] . '\', \'prior\');"><i class="fa fa-pencil"></i></button>
													';
									}
										echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#viewReport" onclick="view_report(\'' . $err['reportID'] . '\');"><i class="fa fa-eye"></i></button>
										';
									if($_SESSION['accountant'] == 'Yes' && $err['status'] != 'Approved'){
										echo '<button type="button" class="btn btn-success btn-xs" onclick="approve_report(\'' . $err['reportID'] . '\');"><i class="fa fa-check"></i></button>
										';
									}
                  if($_SESSION['accountant'] == 'Yes' || $err['status'] == 'Approved'){
                    echo '<a href="http://marketforceapp.com/marketforce/expenses/view-expense-report.php?rid=' . $err['reportID'] . '" target="_blank"><button type="button" class="btn btn-default btn-xs"><i class="fa fa-print"></i></button>';
                  }
										        
										echo '</td>
												</tr>';
								}
									echo '</tbody>';
									echo '</table>
										<!--</div>-->
										</div>
									</div>
								</div>
							</div><!--End Row-->';
							
							
							?>
							
							
							
					
		
			 <!-- Modal -->
  <div class="modal fade" id="newExpense" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">New Expense Item</h4>
        </div>
					<form id="addForm" action="expenses/expense.php" method="post" enctype="multipart/form-data">
        <div class="modal-body">
				<div class="row">
						<div class="col-lg-6">
         			  <p>Date:</p>
								  <input type="text" class="form-control date" id="date" name="date" value="<? echo $idate; ?>" placeholder="mm/dd/yyyy" required>
						</div>
						<div class="col-lg-6">
								<p>Category:</p>
								  <select class="form-control" id="cat" name="cat" onchange="cat_check();" required>
										<option value="">Select One</option>
										<option value="Food">Food</option>
										<option value="Hotel">Hotel</option>
										<option value="Office Supplies">Office Supplies</option>
										<option value="Mileage">Mileage</option>
										<option value="Vehicle/Gas">Vehicle/Gas</option>
										<option value="Misc">Misc.</option>
									</select>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-6">
								<p>City</p>
								  <input type="text" class="form-control" id="city" name="city" value="<? echo $icity; ?>" required>
						</div>
						<div class="col-lg-6">
							<p>State</p>
							<select class="form-control" id="state" name="state" required>
								<option value="">Select One</option>
								<?php
								$sq = "SELECT * FROM `states`";
								$sg = mysqli_query($conn, $sq) or die($conn->error);
								while($sr = mysqli_fetch_array($sg)){
									if($sr['state'] == $istate){
										$selected = 'selected';
									}else{
										$selected = '';
									}
									echo '<option value="' . $sr['state'] . '" ' . $selected . '>' . $sr['state'] . '</option>';
								}
								?>
							</select>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-6">
							<p>Item Name:</p>
							<input type="text" class="form-control" id="iname" name="iname" required>
						</div>
						<div class="col-lg-6">
							<div id="amount-miles">
								<p>Amount</p>
								<input type="text" class="form-control usd" id="amount" name="amount" placeholder="0.00" required>
							</div>
						</div>
					</div>
          <br>
          <div class="row" id="extra-row"></div>
						<hr />
					<div class="row">
						<div class="col-lg-12">
							<p>Notes:</p>
							<textarea id="notes" name="notes" class="form-control" style="height:150px;"></textarea>
							<br><br>
							<p id="error_msg" style="color:red;font-weight:bold;"></p>
						</div>
						<div class="col-lg-12">
							<p>Attach Receipt Image:</p>
							<input type="file" class="form-control" id="fileToUpload" name="fileToUpload">
						</div>
					</div>
						<input type="hidden" id="mode" name="mode" value="Add" />
						<input type="hidden" id="rep_id" name="rep_id" value="<? echo $_SESSION['user_id']; ?>" />
						<input type="hidden" id="rep_name" name="rep_name" value="<? echo $_SESSION['full_name']; ?>" />
						<input type="hidden" id="rID" name="rID" value="<? echo $RID; ?>" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" name="submit" class="btn btn-success">Add</button>
        </div>
					</form>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	

							
	
		 <!-- Modal -->
  <div class="modal fade" id="viewReport" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">New Expense Item</h4>
        </div>
					<form id="noteForm" action="petty-cash/add-transaction.php" method="post">
        <div class="modal-body">
					<div id="view_contents">
						<!--Contents of View Report Go Here-->
					</div>
					<br>
					<div class="row">
						<div class="col-lg-12">
							<p>Notes:</p>
							<textarea id="notes" name="notes" class="form-control" style="height:150px;"></textarea>
							<br><br>
							<p id="view_error_msg" style="color:red;font-weight:bold;"></p>
						</div>
					</div>
						<input type="hidden" id="mode" name="mode" value="notes" />
						<input type="hidden" id="rep_id" name="rep_id" value="<? echo $_SESSION['user_id']; ?>" />
						<input type="hidden" id="rep_name" name="rep_name" value="<? echo $_SESSION['full_name']; ?>" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success">Save</button>
        </div>
      </div>
				</form>
    </div>
  </div>
	<!-- End Modal -->
							
							
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

  

 <?php include 'footer.html'; ?>
							
							
  <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <!--<script src="js/plugins/morris/morris-data.js"></script>-->
</body>


</html>