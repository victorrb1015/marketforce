<?php
ini_set('session.save_path', '/home/marketforce/mf_temp');
session_start();
error_reporting(0);
$db_host = 'localhost';
$db_user = 'marketfo_mf';
$db_pass = '#NgTFJQ!z@t8';
$db_mf_db = 'marketfo_marketforce';
$db_db = $_SESSION['org_db_name'];

if ($_SESSION['org_db_name']) {
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_db) or die('Main Connection Error: ' . $conn->error);
}
include 'security/session/session-settings.php';
$org_id = $_GET['org_id'];
$user_name = $_GET['user_name'];
$user_id = $_GET['user_id'];
$meeting = array();
$reservations = array();
$select = "SELECT * FROM `meeting_room`";
$res1 = mysqli_query($conn, $select) or die($conn->error);
while($r = mysqli_fetch_array($res1)) {
    $val = 'Active';
    if($r['inactive']){
        $val = 'Inactive';
    }
    $array = [
        'name' => $r['name'],
        'location' => $r['location'],
        'description' => $r['description'],
        'inactive' => $val,
        'ID' => $r['ID'],
    ];
    $meeting[] = $array;
}
$select_res = "SELECT * FROM `reservations` where incative is false";
$res2 = mysqli_query($conn, $select_res) or die($conn->error);
while($x = mysqli_fetch_array($res2)){
    $array2 =[
        'ID' => $x['ID'],
        'title' => $x['observations'],
        'start' => $x['start'],
        'end' => $x['end']
    ];
    $reservations[] = $array2;
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.10.1/main.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <title>Calendario</title>
    <style>
        body{
            background-color: black;
        }
        #calendar{
            max-height: 800px;
        }
    </style>
</head>
<body>
<div class="row m-2">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <div id='calendar'></div>
                <div id='table'>
                    <table class="table" id="table_room">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">location</th>
                            <th scope="col">Description</th>
                            <th scope="col">Active</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($meeting as $clave=>$value)
                        {
                            echo '<tr>
                                      <td>' . $value['name'] . '</td>
                                      <td>' . $value['location'] . '</td>
                                      <td>' . $value['description'] . '</td>
                                      <td>' . $value['inactive'] . '</td>
                                      <td><button type="button" class="btn btn-danger btnDelete">Delete</button></td>
                                  </tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card" id="reservations">
            <div class="card-body">
                <div class="row">
                    <div class="col-8">
                        <h4>Reservations</h4>
                    </div>
                    <div class="col-4">
                        <button type="button" class="btn btn-primary" onclick="meeting_room()">Meeting Rooms</button>
                    </div>
                </div>
                <div class="row">
                    <form>
                        <div class="row">
                            <div class="mb-3">
                                <label for="meeting-room" class="form-label">Meeting Room</label>
                                <select class="form-select" aria-label="Default select example" id="meeting-room">
                                    <?php
                                    foreach ($meeting as $clave=>$value)
                                    {
                                        echo '<option value="'.$value['ID'].'">'.$value['name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1" class="form-label">Reservation under which name:</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" value="<?php echo $user_name;?>" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <label class="form-label">Start</label>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="datepicker" class="form-label">Date</label>
                                        <input type="text" id="datepicker">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="time" class="form-label">Time </label>
                                        <input type="text" id="time"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <label class="form-label">End</label>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="datepicker" class="form-label">Date</label>
                                        <input type="text" id="datepicker2">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="time" class="form-label">Time </label>
                                        <input type="text" id="time2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="observation" class="form-label">Observations</label>
                            <textarea class="form-control" id="observation" rows="3"></textarea>
                        </div>
                        <input type="hidden" value="<? echo $user_id;?>">
                        <div class="d-grid gap-2">
                            <button class="btn btn-primary" onclick="new_reservation()" type="button">Reservation</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="card" id="meeting_room">
            <div class="card-body">
                <div class="row">
                    <div class="col-8">
                        <h4>Meeting Rooms</h4>
                    </div>
                    <div class="col-4">
                        <button type="button" class="btn btn-primary" onclick="meeting_room()">Reservation</button>
                    </div>
                </div>
                <form>
                    <div class="row">
                        <div class="mb-3">
                            <label for="name" class="form-label">Name:</label>
                            <input type="text" class="form-control" id="name">
                        </div>
                        <div class="mb-3">
                            <label for="location_room" class="form-label">Location:</label>
                            <input type="text" class="form-control" id="location_room">
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">Descripción:</label>
                            <input type="text" class="form-control" id="description">
                        </div>
                        <div class="mb-3">
                            <label  class="form-label">Active:</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>
                        </div>
                        <div class="d-grid gap-2">
                            <button class="btn btn-primary" type="button" onclick="new_rooms()">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card" id="reservations_detail">
            <div class="card-body">
                <div class="row">
                    <div class="col-8">
                        <h4>Reservation Detail</h4>
                    </div>
                    <div class="col-4">
                        <button type="button" class="btn btn-primary" onclick="meeting_room()">Meeting Rooms</button>
                    </div>
                </div>
                <div class="row">
                    <form>
                        <div class="row">
                            <div class="mb-3">
                                <label for="meeting-room" class="form-label">Meeting Room</label>
                                <input type="text" class="form-control" id="room" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1" class="form-label">Reservation under which name:</label>
                                <input type="text" class="form-control" id="user" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="datepicker3" class="form-label">Start</label>
                                        <input type="text" id="datepicker3">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="datepicker4" class="form-label">End</label>
                                        <input type="text" id="datepicker4"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="observation" class="form-label">Title</label>
                            <textarea class="form-control" id="observation" rows="3"></textarea>
                        </div>
                        <input type="hidden" value="<? echo $user_id;?>">
                        <div class="d-grid gap-2">
                            <button class="btn btn-primary" onclick="new_reservation()" type="button">Reservation</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.10.1/main.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
<script>

    $(function () {
        $('#meeting_room').toggle();
        $('#table').toggle();
        $('#reservations_detail').hide();
        $("#datepicker").datepicker({
            dateFormat: "yy-mm-dd"
        });
        $('#time').timepicker({
            timeFormat: 'HH:mm',
            interval: 30,
            'scrollDefault': 'now'
        });
        $("#datepicker2").datepicker({
            dateFormat: "yy-mm-dd",
        });
        $('#time2').timepicker({
            timeFormat: 'HH:mm',
            interval: 30,
            'scrollDefault': 'now'
        });

        $("#table_room").on('click','.btnDelete',function(){
            let name = $(this).closest('tr').find('td').eq(0).html();
            let location_line = $(this).closest('tr').find('td').eq(1).html();
            let table = $(this).closest('tr');
            $.ajax({
                type: "POST",
                url: "php/delete-rooms.php",
                dataType: "json",
                data: {
                    "name": name,
                    "location": location_line
                },
                success: function(response){
                    //console.log(response);
                    table.remove();
                }
            });
        });
    });

    function meeting_room (){
        $('#meeting_room').toggle();
        $('#reservations').toggle();
        $('#table').toggle();
        $('#calendar').toggle();
    }

    document.addEventListener('DOMContentLoaded', function () {
        let calendarEl = document.getElementById('calendar');
        let calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'dayGridMonth',
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            //editable: true,
            dayMaxEvents: true,
            events: [
                <?php
                foreach ($reservations as $clave=>$value){
                    echo "{ ID:".$value['ID']." , title: '".$value['title']."', start: '".$value['start']."', end: '".$value['end']."' },";
                }
                ?>
            ],
            /*eventClick: function(info) {
                //alert('ID: '+info.event.);
                $('#reservations').hide();
                $('#reservations_detail').show();
                $.ajax({
                    type: "POST",
                    url: "php/detail-reservations.php",
                    dataType: "json",
                    data: {
                        "ID": info.event.extendedProps['ID']
                    },
                    success: function(response){
                        console.log(response);
                    }
                });
                // change the border color just for fun
                info.el.style.borderColor = 'red';
            }*/
        });
        calendar.render();
    });

    function new_rooms(org_id){
        const name = $("#name").val();
        const location_room = $("#location_room").val();
        const description = $('#description').val();
        let inactive = 0;
        let val_active = 'Active';
        if($("#inlineRadio2").prop('checked') === true){
            inactive = 1;
            val_active = 'Inactive';
        }
        let tableBody = $("table tbody");
        $.ajax({
            type: "POST",
            url: "php/add-rooms.php",
            dataType: "json",
            data: {
                "org_id": org_id,
                "name": name,
                "location": location_room,
                "description": description,
                "inactive": inactive
            },
            success: function(response){
                console.log(response);
                let markup = "<tr> "+
                    "<td>"+name+"</td> " +
                    "<td>"+location_room+"</td>" +
                    "<td>"+description+"</td>" +
                    "<td>"+val_active+"</td>" +
                    "<td><button type='button' class='btn btn-danger btnDelete'>Delete</button></td>" +
                    "</tr>";
                tableBody.append(markup);
                $("#name").val("");
                $("#location_room").val("");
                $("#description").val("");
            }
        });
    }

    function new_reservation(){
        const room_id = $("#meeting-room").val();
        const user_id = <?php echo $_SESSION['user_id']?>;
        const date_start = $("#datepicker").val();
        const time_start = $("#time").val();
        const date_end = $("#datepicker2").val();
        const time_end = $("#time2").val();
        const observation = $("#observation").val();
        console.log("Room_id : "+room_id+" | user_id : "+ user_id+" | date_start :  "+date_start+" | time_start : "+ time_start+" | date_end : "+date_end+" | time_end : "+time_end+ " | observations : "+observation);
        $.ajax({
            type: "POST",
            url: "php/add-reservations.php",
            dataType: "json",
            data: {
                "room_id": room_id,
                "user_id": user_id,
                "date_start": date_start,
                "time_start": time_start,
                "date_end": date_end,
                "time_end": time_end,
                "observation": observation
            },
            success: function(response){
                console.log(response);
                $("#datepicker").val("");
                $("#time").val("");
                $("#datepicker2").val("");
                $("#time2").val("");
                $("#observation").val("");
                location.reload();
            }
        });
    }
</script>
</body>
</html>
