<?php
include 'connection.php';

//Load Variables
$emp = $_POST['eid'];
$idate = mysqli_real_escape_string($conn, date("Y-m-d", strtotime($_POST['idate'])));
$wtype = $_POST['type'];
$det = mysqli_real_escape_string($conn, $_POST['det']);
$action = mysqli_real_escape_string($conn, $_POST['action']);
$sup_id = $_POST['sup_id'];
$sup_name = $_POST['sup_name'];

$enq = "SELECT * FROM `users` WHERE `ID` = '" . $emp . "'";
$eng = mysqli_query($conn, $enq) or die($conn->error);
$enr = mysqli_fetch_array($eng);
$ename = $enr['fname'] . ' ' . $enr['lname'];
$email = $enr['email'];
$position = $enr['position'];

$type = 'Warning';
//INSERT WARNING into database

$q = "INSERT INTO `requests` 
      (
      `date`,
      `time`,
      `user`,
      `user_id`,
      `email`,
      `position`,
      `type`,
      `reason`,
      `idate`,
      `wtype`,
      `details`,
      `action`,
      `supervisor_id`,
      `supervisor_name`,
      `status`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $ename . "',
      '" . $emp . "',
      '" . $email . "',
      '" . $position . "',
      '" . $type . "',
      '" . $det . "',
      '" . $idate . "',
      '" . $wtype . "',
      '" . $det . "',
      '" . $action . "',
      '" . $sup_id . "',
      '" . $sup_name . "',
      'Pending'
      )";

$g = mysqli_query($conn, $q) or die($conn->error);

$lidq = "SELECT * FROM `requests` ORDER BY `ID` DESC LIMIT 1";
$lidg = mysqli_query($conn, $lidq) or die($conn->error);
$lidr = mysqli_fetch_array($lidg);
$lid = $lidr['ID'];
$array = array();
$array['error'] = false;

if (count($_FILES) != 0) {

    $id = $lid;
    $uid = uniqid('', true);
    $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/docs-files/docs-employeeCenter/" . $id . "/";
    //$target_dir = "/home/marketforce/doc-files/docs-customer/".$id."/";
    //$target_dir = "/".$id."/";
    foreach ($_FILES as $file) {
        $target_file2 = $target_dir . basename($file["name"]);

        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0755, true);
        }
        $imageFileType = pathinfo($target_file2, PATHINFO_EXTENSION);
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "pdf"
            && $imageFileType != "PDF") {
            $array['msg'] = 'ERROR!- only JPG, JPEG, PNG & GIF files are allowed.';
            $array['error'] = true;

        } else {
            if (file_exists($target_file2)) {
                $array['msg'] = 'The file ' . basename($file["name"]) . ' already exists.';
                $array['error'] = true;
            } else {
                if (move_uploaded_file($file["tmp_name"], $target_file2)) {

                    $name = $file["name"];
                    //INSERT FILE PATH INTO DATABASE
                    $ruta = "/docs-files/docs-employeeCenter/" . $id . "/" . $name;
                    $sql = "INSERT INTO doc_ec (id_ec, rute, name) values(" . $id . ", '" . $ruta . "', '" . $name . "')";
                    $res = mysqli_query($conn, $sql) or die($conn->error);
                } else {
                    $array['msg'] = "ERROR!- there was an error uploading your file.";
                    $array['error'] = true;
                }
            }

        }
    }
}

if ($array['error'] === false) {
    include 'phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer();
    include 'phpmailsettings.php';
    $mail->setFrom('no-reply@allsteelcarports.com', 'Market Force');
    $mail->addAddress('michael@burtonsolution.tech');
    $mail->addAddress($email);
    $mail->addBCC('archive@ignition-innovations.com');
    $mail->Subject = 'Corrective Action Notice!';
    $mail->Body = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Alerts e.g. approaching your limit</title>


<style type="text/css">
/* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
* {
  margin: 0;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  box-sizing: border-box;
  font-size: 14px;
}

img {
  max-width: 100%;
}

body {
  -webkit-font-smoothing: antialiased;
  -webkit-text-size-adjust: none;
  width: 100% !important;
  height: 100%;
  line-height: 1.6em;
  /* 1.6em * 14px = 22.4px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 22px;*/
}

/* Let\'s make sure all tables have defaults */
table td {
  vertical-align: top;
}

/* -------------------------------------
    BODY & CONTAINER
------------------------------------- */
body {
  background-color: #f6f6f6;
}

.body-wrap {
  background-color: #f6f6f6;
  width: 100%;
}

.container {
  display: block !important;
  max-width: 600px !important;
  margin: 0 auto !important;
  /* makes it centered */
  clear: both !important;
}

.content {
  max-width: 600px;
  margin: 0 auto;
  display: block;
  padding: 20px;
}

/* -------------------------------------
    HEADER, FOOTER, MAIN
------------------------------------- */
.main {
  background-color: #fff;
  border: 1px solid #e9e9e9;
  border-radius: 3px;
}

.content-wrap {
  padding: 20px;
}

.content-block {
  padding: 0 0 20px;
}

.header {
  width: 100%;
  margin-bottom: 20px;
}

.footer {
  width: 100%;
  clear: both;
  color: #999;
  padding: 20px;
}
.footer p, .footer a, .footer td {
  color: #999;
  font-size: 12px;
}

/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1, h2, h3 {
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  color: #000;
  margin: 40px 0 0;
  line-height: 1.2em;
  font-weight: 400;
}

h1 {
  font-size: 32px;
  font-weight: 500;
  /* 1.2em * 32px = 38.4px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 38px;*/
}

h2 {
  font-size: 24px;
  /* 1.2em * 24px = 28.8px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 29px;*/
}

h3 {
  font-size: 18px;
  /* 1.2em * 18px = 21.6px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 22px;*/
}

h4 {
  font-size: 14px;
  font-weight: 600;
}

p, ul, ol {
  margin-bottom: 10px;
  font-weight: normal;
}
p li, ul li, ol li {
  margin-left: 5px;
  list-style-position: inside;
}

/* -------------------------------------
    LINKS & BUTTONS
------------------------------------- */
a {
  color: #348eda;
  text-decoration: underline;
}

.btn-primary {
  text-decoration: none;
  color: #FFF;
  background-color: #348eda;
  border: solid #348eda;
  border-width: 10px 20px;
  line-height: 2em;
  /* 2em * 14px = 28px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 28px;*/
  font-weight: bold;
  text-align: center;
  cursor: pointer;
  display: inline-block;
  border-radius: 5px;
  text-transform: capitalize;
}

/* -------------------------------------
    OTHER STYLES THAT MIGHT BE USEFUL
------------------------------------- */
.last {
  margin-bottom: 0;
}

.first {
  margin-top: 0;
}

.aligncenter {
  text-align: center;
}

.alignright {
  text-align: right;
}

.alignleft {
  text-align: left;
}

.clear {
  clear: both;
}

/* -------------------------------------
    ALERTS
    Change the class depending on warning email, good email or bad email
------------------------------------- */
.alert {
  font-size: 16px;
  color: #fff;
  font-weight: 500;
  padding: 20px;
  text-align: center;
  border-radius: 3px 3px 0 0;
}
.alert a {
  color: #fff;
  text-decoration: none;
  font-weight: 500;
  font-size: 16px;
}
.alert.alert-warning {
  background-color: #FF9F00;
}
.alert.alert-bad {
  background-color: #D0021B;
}
.alert.alert-good {
  background-color: #68B90F;
}

/* -------------------------------------
    INVOICE
    Styles for the billing table
------------------------------------- */
.invoice {
  margin: 40px auto;
  text-align: left;
  width: 80%;
}
.invoice td {
  padding: 5px 0;
}
.invoice .invoice-items {
  width: 100%;
}
.invoice .invoice-items td {
  border-top: #eee 1px solid;
}
.invoice .invoice-items .total td {
  border-top: 2px solid #333;
  border-bottom: 2px solid #333;
  font-weight: 700;
}

/* -------------------------------------
    RESPONSIVE AND MOBILE FRIENDLY STYLES
------------------------------------- */
@media only screen and (max-width: 640px) {
  body {
    padding: 0 !important;
  }

  h1, h2, h3, h4 {
    font-weight: 800 !important;
    margin: 20px 0 5px !important;
  }

  h1 {
    font-size: 22px !important;
  }

  h2 {
    font-size: 18px !important;
  }

  h3 {
    font-size: 16px !important;
  }

  .container {
    padding: 0 !important;
    width: 100% !important;
  }

  .content {
    padding: 0 !important;
  }

  .content-wrap {
    padding: 10px !important;
  }

  .invoice {
    width: 100% !important;
  }
}

/*# sourceMappingURL=styles.css.map */
</style>
</head>

<body itemscope itemtype="http://schema.org/EmailMessage" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">

<div style="text-align:center; margin:auto;">';
    switch ($_SESSION['org_id']) {
        case "738004":
        case "654321":
        case "832122":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="http://dev.marketforceapp.com/assets/img/brands/acero-news.png" style="max-width: 1000px;"><br>';
            break;
        case "162534":
            $mail->Body .= '<td align="center" valign="top" class="textContent" style="Background-color: #A0A2A6">
                                                          <img src="http://dev.marketforceapp.com/assets/img/brands/NorthEdgeSteel-01.png" style="max-width: 350px;"><br>';
            break;
        case "615243":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="http://dev.marketforceapp.com/assets/img/brands/legacyMFlogo.png" style="max-width: 1000px;"><br>';
            break;

        case "329698":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="https://marketforceapp.com/assets/img/brands/integrity-logo2.png" style="max-width: 1000px;"><br>';
            break;

        case "532748":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="https://marketforceapp.com/assets/img/brands/JMLogo.png" style="max-width: 600px;"><br>';
            break;
        default:
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="http://beta.marketforceapp.com/assets/img/brands/Logo-mailing-allsteel.png" style="max-width: 1000px;"><br>';
    }
    $mail->Body .= '
</div>

<table class="body-wrap" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6"><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
		<td class="container" width="600" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
			<div class="content" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
				<table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff"><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert alert-warning" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 25px; font-weight:bold; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #C9302C; margin: 0; padding: 20px;" align="center" bgcolor="#C9302C" valign="top">
							Employee Warning Notice
						</td>
					</tr><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-wrap" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
							<table width="100%" cellpadding="0" cellspacing="0" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
										<strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">' . $sup_name . '</strong> has issued you an Official Warning. You can find the details below:
									</td>
								</tr><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
										<strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Warning Type:</strong> ' . $wtype . '<br>
                    <strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Date of Infraction:</strong> ' . $idate . '<br><br>
                    <strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Infraction Details:</strong><br>
                    ' . $det . '<br><br>
                    <strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Corrective Action Required:</strong><br>
                    ' . $action . '<br><br>
                    <strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Please Enter Your Response Here (If none, please type that you have recieved this email):</strong><br>
                    <form action="http://marketforceapp.com/marketforce/ec/php/form-response.php" method="get">
                    <textarea name="response" style="width:600px;height:200px;"></textarea>
                    <input type="hidden" name="id" value="' . $lid . '" />
									</td>
								</tr><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
										<button type="submit" class="btn-primary" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;">Send Response</button>
									</form>
                  </td>
								</tr><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
										Thanks,
                    All Steel Carports
									</td>
								</tr></table></td>
					</tr></table><div class="footer" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">
					<table width="100%" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="aligncenter content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">This message is strictly confidential and all information contained in it is protected by Market Force\'s built in security features.</td>
						</tr></table></div></div>
		</td>
		<td style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
	</tr></table></body>
</html>';
    try {
        if ($mail->send()) {
            $array['msg'] = "Message has been sent";
            $array['error'] = false;
        } else {
            $array['msg'] = 'Message could not be sent. /n' . $mail->ErrorInfo;
            $array['error'] = true;
        }
    } catch (phpmailerException $e) {
        $array['msg'] = $e->errorMessage();
        $array['error'] = true;
    } catch (Exception $e) {
        $array['msg'] = $e->getMessage();
        $array['error'] = true;
    }
}


echo json_encode($array, true);
