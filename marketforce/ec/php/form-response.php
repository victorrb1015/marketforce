<?php
include 'connection.php';

//Load Variables
$fid = $_GET['id'];
$response = mysqli_real_escape_string($conn, $_GET['response']);

$vq = "SELECT * FROM `requests` WHERE `ID` = '" . $fid . "' AND `emp_response` = ''";
$vg = mysqli_query($conn, $vq) or die($conn->error);
if(mysqli_num_rows($vg) != 0){
  $q = "UPDATE `requests` SET `notes` = '" . $response . "', `emp_response` = '" . $response . "', `response_date` = CURRENT_DATE, `status` = 'Responded' WHERE `ID` = '" . $fid . "'";
  $g = mysqli_query($conn, $q) or die($conn->error);
  echo 'Your Response has been accepted. Thank You!';
}else{
  echo 'There was an error processing your response. If you have already responded, you will not be able to change or add an additional response!';
}

?>