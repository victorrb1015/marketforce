<?php
include 'connection.php';

//Load Variables
$id = $_GET['id'];

//Load Form Info
$q = "SELECT * FROM `requests` WHERE `ID` = '" . $id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);

          echo '<html>
                 <head>
                 <style>
                 .main{
                  text-align: center;
                 }
                 img{
                  width: 50%;
                 }
                 ul{
                  list-style-type: none;
                  text-align: left;
                 }
                 li{
                  padding: 5px;
                 }
                 .n{
                  text-decoration: none;
                  color:black;
                 }
                 .button-a {
                   background-color: ForestGreen;  
                   border-radius: 5px;
                   color: white;
                   padding: .5em;
                   text-decoration: none;
                 }

                 .button-a:focus,
                 .button-a:hover {
                   background-color: DarkGreen;
                 }
                 .button-d {
                   background-color: red;  
                   border-radius: 5px;
                   color: white;
                   padding: .5em;
                   text-decoration: none;
                 }

                 .button-d:focus,
                 .button-d:hover {
                   background-color: DarkRed;
                 }
                 div{
                  //border: 2px solid black;
                 }
                 </style>
                 </head>
                 <body>
                  <div class="main">
                    <img src="http://marketforceapp.com/marketforce/ec/images/mfl2.png" />
                    <h1>Time Off Request:</h1>
                    <div style="width: 50%; margin:auto;">
                    <ul>
                      <li><b>Name:</b> ' . $r['user'] . '</li>
                      <li><b>Position:</b> ' . $r['position'] . '</li>
                      <li><b>Supervisor Name:</b> ' . $r['supervisor_name'] . '</li>
                      <li><b>Request submitted at least 2 weeks prior to time off?</b> ';
                $vsdate = strtotime($r['sdate'] . "-2 Week");
                $cdate = strtotime("now");
                if($cdate <= $vsdate){
                  echo '<span style="color:green;"><b>YES</b></span>';
                }else{
                  echo '<span style="color:red;"><b>NO</b></span>';
                }
                
                    echo '</li>
                        <li><b>Dates requested off:</b> ' . date("l F d, Y",strtotime($r['sdate'])) . ' - ' . date("l F d, Y",strtotime($r['edate'])) . '</li>
                        <li><b>Date Returning to Work:</b> ' . date("l F d, Y",strtotime($r['edate'])) . '</li>
                        <li><b>Number of Days Requested Off:</b> ' . $r['num_days'] . '</li>
                        <li><b>Use:</b> ' . $r['pay'] . '</li>
                        <li><b>Reason For Request:</b> ' . $r['reason'] . '</li>
                       </ul>
                       </div>
                 </div>
                 </body>
                 </html>';

?>