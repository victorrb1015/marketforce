<?php
include 'connection.php';

include 'phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include 'phpmailsettings.php';

//Load Variables
$id = $_GET['id'];
$remote = $_GET['remote'];
$status = $_GET['status'];
$note = mysqli_real_escape_string($conn,$_GET['note']);
$snote = mysqli_real_escape_string($conn,$_GET['snote']);

//Change the status
$csq = "UPDATE `requests` SET `status` = '" . $status . "', `notes` = '" . $note . "', `snotes` = '" . $snote . "' WHERE `ID` = '" . $id . "'";

$csg = mysqli_query($conn, $csq) or die($conn->error);

//Load request info
$riq = "SELECT * FROM `requests` WHERE `ID` = '" . $id . "'";
$rig = mysqli_query($conn, $riq) or die($conn->error);
$rir = mysqli_fetch_array($rig);
$supid = $rir['supervisor_id'];
$userid = $rir['user_id'];

//Get user's country
$uciq = "SELECT * FROM `users` WHERE `ID` = '" . $userid . "'";
$ucig = mysqli_query($conn, $uciq) or die($conn->error);
$ucir = mysqli_fetch_array($ucig);
$ecountry = $ucir['country'];

//Get Supervisor mail
$smiq = "SELECT * FROM `users` WHERE `ID` = '" . $supid . "'";
$smig = mysqli_query($conn, $smiq) or die($conn->error);
$smir = mysqli_fetch_array($smig);
$supmail = $smir['email'];

//Send email response to requestee
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
$mail->addAddress($rir['email']);
$mail->addAddress($smir['email']);

if($ucir['country'] != 'Mexico'){
$mail->addCC('michael@allsteelcarports.com');
}

$mail->addBCC('archive@ignition-innovations.com');
$mail->Subject = 'Request Response!';
$mail->Body = '<html>
                 <head>
                 <style>
                 .main{
                  text-align: center;
                 }
                 img{
                  width: 50%;
                 }
                 ul{
                  list-style-type: none;
                  text-align: left;
                 }
                 li{
                  padding: 5px;
                 }
                 .n{
                  text-decoration: none;
                  color:black;
                 }
                 .button-a {
                   background-color: ForestGreen;  
                   border-radius: 5px;
                   color: white;
                   padding: .5em;
                   text-decoration: none;
                 }

                 .button-a:focus,
                 .button-a:hover {
                   background-color: DarkGreen;
                 }
                 .button-d {
                   background-color: red;  
                   border-radius: 5px;
                   color: white;
                   padding: .5em;
                   text-decoration: none;
                 }

                 .button-d:focus,
                 .button-d:hover {
                   background-color: DarkRed;
                 }
                 div{
                  //border: 2px solid black;
                 }
                 </style>
                 </head>
                 <body>
                  <div class="main">
                    <img src="https://marketforceapp.com/marketforce/ec/images/mfl2.png" />
                    <h1>Request Response:</h1>
                    <p><b>' . $rir['user'] . '</b>, your request for time off from <b>' . date("m/d/Y",strtotime($rir['sdate'])) . '</b> to <b>' . date("m/d/Y",strtotime($rir['edate'])) . '</b> has been moved to <b>';
                if($rir['status'] == 'Approved'){
                  $mail->Body .= '<span style="color:green;">Approved</span>';
                }
                if($rir['status'] == 'Declined'){
                  $mail->Body .= '<span style="color:red;">Declined</span>';
                }
                if($rir['status'] == 'Pending'){
                  $mail->Body .= '<span style="color:red;">Pending</span>';
                }
 $mail->Body .= '</b></p>
                 <p style="color: black;">You have requested to use: <b>' . $rir['pay'] . '</b></p>
                 <br>
                 <p style="color: black;"><b>Notes:</b></p> ' . $rir['notes'] . '
                 </div>
                 </body>
                 </html>';

$mail->send();



//This is the email to Scott for Accounting the paid days off (RH)
if($rir['status'] == 'Approved'){
  $mail->ClearAddresses();
    if($ucir['country'] == 'Mexico'){
    $mail->addAddress('alan@allamericanbuildings.com');
  }
    if($ucir['country'] != 'Mexico'){
   // $mail->addAddress('scott@allsteelcarports.com');
    $mail->addAddress('sharon@allsteelcarports.com');
  }
  $mail->addAddress('info@burtonsolution.tech');
  $mail->addBCC('archive@ignition-innovations.com');
  $mail->Subject = 'Time Off Request Info';
$mail->Body = '<html>
                 <head>
                 <style>
                 .main{
                  text-align: center;
                 }
                 img{
                  width: 50%;
                 }
                 ul{
                  list-style-type: none;
                  text-align: left;
                 }
                 li{
                  padding: 5px;
                 }
                 .n{
                  text-decoration: none;
                  color:black;
                 }
                 .button-a {
                   background-color: ForestGreen;  
                   border-radius: 5px;
                   color: white;
                   padding: .5em;
                   text-decoration: none;
                 }

                 .button-a:focus,
                 .button-a:hover {
                   background-color: DarkGreen;
                 }
                 .button-d {
                   background-color: red;  
                   border-radius: 5px;
                   color: white;
                   padding: .5em;
                   text-decoration: none;
                 }

                 .button-d:focus,
                 .button-d:hover {
                   background-color: DarkRed;
                 }
                 div{
                  //border: 2px solid black;
                 }
                 </style>
                 </head>
                 <body>
                  <div class="main">
                    <img src="https://marketforceapp.com/marketforce/ec/images/mfl2.png" />
                    <h1>Request Information:</h1>
                    <p><b>' . $rir['user'] . '</b>, has been APPROVED for time off from <b>' . date("m/d/Y",strtotime($rir['sdate'])) . '</b> to <b>' . date("m/d/Y",strtotime($rir['edate'])) . '</b></p>
                    <br>
                    <p>They have requested to use: <b>' . $rir['pay'] . '</b></p>
                    <br>
                    <p><b>Total number of days requested off:</b> ' . $rir['num_days'] . '</p>
                    <br>
                    <p><b>Notes For Accounting:</b> ' . $rir['snotes'] . '</p>
                    </div>
                    </body>
                    </html>';
                    
   $mail->send();
   
}


echo 'Your response has been sent!';

?>