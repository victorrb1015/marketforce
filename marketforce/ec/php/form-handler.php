<?php
include 'connection.php';
//Load PHPMailer Program
include 'phpmailer/PHPMailerAutoload.php';

//include 'day-counter.php';
//Day Counter Application Script
//The function returns the no. of business days between two dates and it skips the holidays
function getWorkingDays($startDate, $endDate, $holidays)
{
    // do strtotime calculations just once
    $endDate = strtotime($endDate);
    $startDate = strtotime($startDate);


    //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    //We add one to inlude both dates in the interval.
    $days = ($endDate - $startDate) / 86400;//Add 1 to if ending date is also requested off...

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N", $startDate);
    $the_last_day_of_week = date("N", $endDate);

    //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week <= $the_last_day_of_week) {
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    } else {
        // (edit by Tokes to fix an edge case where the start day was a Sunday
        // and the end day was NOT a Saturday)

        // the day of the week for start is later than the day of the week for end
        if ($the_first_day_of_week == 7) {
            // if the start date is a Sunday, then we definitely subtract 1 day
            $no_remaining_days--;

            if ($the_last_day_of_week == 6) {
                // if the end date is a Saturday, then we subtract another day
                $no_remaining_days--;
            }
        } else {
            // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
            // so we skip an entire weekend and subtract 2 days
            $no_remaining_days -= 2;
        }
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
    $workingDays = $no_full_weeks * 5;
    if ($no_remaining_days > 0) {
        $workingDays += $no_remaining_days;
    }

    //We subtract the holidays
    foreach ($holidays as $holiday) {
        $time_stamp = strtotime($holiday);
        //If the holiday doesn't fall in weekend
        if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7)
            $workingDays--;

    }

    return $workingDays;
}

function mail_request($to, $subject, $body,$ecountry,$emmail,$rhemail)
{
    $mail = new PHPMailer;
    include 'phpmailsettings.php';
    //Email request to ADMIN
    $mail->setFrom('no-reply@allsteelcarports.com', 'Market Force');
  
  if($ecountry == 'Mexico'){
    $mail->addAddress($emmail);
  }
  if($ecountry != 'Mexico'){
    $mail->addAddress('michael@allsteelcarports.com');
    $mail->addAddress('tiffany@allsteelcarports.com');
  }
    $mail->addAddress($to);
    //$mail->addAddress($rhemail);
    $mail->addBCC('archive@ignition-innovations.com');
    //$mail->addBCC('victor@grupocomunicado.com');
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->send();
}


//Example:
//Sets holidays to not include...
$holidays = array("2018-12-25", "2019-12-25", "2020-12-25");
//Gets the total num of days (Excluding weekends and holidays)
$numDays = getWorkingDays($_POST['Sdate'], $_POST['Edate'], $holidays);


//Load Form Variables
$user_id = $_POST['user_id'];
$name = mysqli_real_escape_string($conn, $_POST['Name']);
$email = mysqli_real_escape_string($conn, $_POST['Email']);
$sdate = date("Y-m-d", strtotime($_POST['Sdate']));
$edate = date("Y-m-d", strtotime($_POST['Edate']));
$reason = mysqli_real_escape_string($conn, $_POST['Reason']);
$emid = $_POST['emid'];
$type = $_POST['type'];
$position = $_POST['position'];
$pay = $_POST['pay'];

//Get Employee's Manager name and mail
$emnq = "SELECT * FROM `users` WHERE `ID` = '" . $emid . "'";
$emng = mysqli_query($conn, $emnq) or die($conn->error);
$emnr = mysqli_fetch_array($emng);
$emname = $emnr['fname'] . ' ' . $emnr['lname'];
$emmail = $emnr['email'];

//Get employee's country
$ecq = "SELECT * FROM `users` WHERE `ID` = '" . $user_id . "'";
$ecg = mysqli_query($conn, $ecq) or die($conn->error);
$ecr = mysqli_fetch_array($ecg);
$ename = $ecr['fname'] . ' ' . $ecr['lname'];
$ecountry = $ecr['country'];

//Testing date conversion...
//echo date("Y-m-d",strtotime($sdate)) . '<br>';
//echo date("Y-m-d",strtotime($edate)) . '<br>';
//break;

// ------Load requests into DB------
$irq = "INSERT INTO `requests` 
      (
      `date`,
      `time`,
      `user`,
      `user_id`,
      `email`,
      `position`,
      `type`,
      `sdate`,
      `edate`,
      `num_days`,
      `pay`,
      `reason`,
      `notes`,
      `supervisor_id`,
      `supervisor_name`,
      `status`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $name . "',
      '" . $user_id . "',
      '" . $email . "',
      '" . $position . "',
      '" . $type . "',
      '" . $sdate . "',
      '" . $edate . "',
      '" . $numDays . "',
      '" . $pay . "',
      '" . $reason . "',
      ' ',
      '" . $emid . "',
      '" . $emname . "',";
if ($type == 'Time Off Request' && $pay == 'Vacation Days'){
    $irq .= "'Validating'
      )";
}
else{
    $irq .= "'Pending'
      )";
}
$irg = mysqli_query($conn, $irq) or die($conn->error);


//Get Form ID
$fiq = "SELECT * FROM `requests` LIMIT 1";
$fig = mysqli_query($conn, $fiq) or die($conn->error);
$fir = mysqli_fetch_array($fig);

//      -----TIME OFF REQUEST Vacation type-----
if ($type == 'Time Off Request' && $pay == 'Vacation Days') {
    $subject = 'New Vacation Days Request!';
    $body = '<html>
                   <head>
                   <style>
                   .main{
                    text-align: center;
                   }
                   img{
                    width: 50%;
                   }
                   ul{
                    list-style-type: none;
                    text-align: left;
                   }
                   li{
                    padding: 5px;
                    color: black;
                   }
                   .n{
                    text-decoration: none;
                    color:black;
                   }
                   .button-a {
                     background-color: ForestGreen;
                     border-radius: 5px;
                     color: white;
                     padding: .5em;
                     text-decoration: none;
                   }

                   .button-a:focus,
                   .button-a:hover {
                     background-color: DarkGreen;
                   }
                   .button-d {
                     background-color: red;
                     border-radius: 5px;
                     color: white;
                     padding: .5em;
                     text-decoration: none;
                   }

                   .button-d:focus,
                   .button-d:hover {
                     background-color: DarkRed;
                   }
                   div{
                    //border: 2px solid black;
                   }
                   </style>
                   </head>
                   <body>
                    <div class="main">
                      <img src="https://marketforceapp.com/marketforce/ec/images/mfl2.png" />
                      <h1>Time Off Request:</h1>
                      <div style="width: 100%;">
                      <ul>
                        <li><b>Name:</b> ' . $name . '</li>
                        <li><b>Position:</b> ' . $position . '</li>
                        <li><b>Supervisor Name:</b> ' . $emname . '</li>
                        <li><b>Request submitted at least 2 weeks prior to time off?</b> ';

    // ir sumando el body hasta el final
    //// --------------------------<>--------------------------------
    $vsdate = strtotime($sdate . "-2 Week");
    $cdate = strtotime("now");
    if ($cdate <= $vsdate) {
        $body .= '<span style="color:green;"><b>YES</b></span>';
    } else {
        $body .= '<span style="color:red;"><b>NO</b></span>';
    }
    //This code will calculate TOTAL num of days (Including weekends and holidays)
    $b = strtotime($sdate);
    $e = strtotime($edate);
    $diff = $e - $b;
    $daysDiff = floor($diff / (60 * 60 * 24));
    $daysDiff++;
  if($ecr['country'] == 'Mexico'){
    $rhemail = 'alan@allamericanbuildings.com';
  }
  if($ecr['country'] != 'Mexico'){
    $rhemail = 'sharon@allsteelcarports.com';
  }
    $body .= '</li>
                        <li><b>Dates requested off:</b> ' . date("l F d, Y", strtotime($sdate)) . ' - ' . date("l F d, Y", strtotime($edate)) . '</li>
                        <li><b>Date Returning to Work:</b> ' . date("l F d, Y", strtotime($edate)) . '</li>
                        <li><b>Number of Days Off Requested:</b> ' . $numDays . '</li>
                        <li><b>Use:</b> ' . $pay . '</li>
                        <li><b>Reason For Request:</b> ' . $reason . '</li>
                       </ul>
                       </div>
                       <br><br>
                      <!-- <div style="width: 100%;" class="buttons">
                     <a class="button-a" href="https://marketforceapp.com/marketforce/ec/php/approve-decline.php?status=Approved&id=' . $fir['ID'] . '" target="_blank">Approve</a>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <a class="button-d" href="https://marketforceapp.com/marketforce/ec/php/approve-decline.php?status=Declined&id=' . $fir['ID'] . '" target="_blank">Decline</a>
                 </div>-->
                 </div>
                 </body>
                 </html>';
    mail_request($email,$subject,$body,$ecountry,$emmail,$rhemail);

}
//End TIME OFF REQUEST Vacation type

if ($type == 'Time Off Request' && $pay != 'Vacation Days') {

    $rhemail = '';
    //Email request to ADMIN
    //$mail->setFrom('no-reply@allsteelcarports.com', 'Market Force');
    //$mail->addAddress('michael@allsteelcarports.com');
    //$mail->addAddress('tiffany@allsteelcarports.com');
    //$mail->addAddress('sharon@allsteelcarports.com');
    //$mail->addAddress('zaret@grupocomunicado.com');
    // if($_SESSION['org_id'] == '123456' || $_SESSION['org_id'] == '654321' || $_SESSION['org_id'] == '987874'){
    //$mail->addAddress('santiago@grupocomunicado.com');
    // $mail->addBCC('archive@ignition-innovations.com');
    $subject = 'New Time Off Request!';
    $body = '<html>
                 <head>
                 <style>
                 .main{
                  text-align: center;
                 }
                 img{
                  width: 50%;
                 }
                 ul{
                  list-style-type: none;
                  text-align: left;
                 }
                 li{
                  padding: 5px;
                 }
                 .n{
                  text-decoration: none;
                  color:black;
                 }
                 .button-a {
                   background-color: ForestGreen;  
                   border-radius: 5px;
                   color: white;
                   padding: .5em;
                   text-decoration: none;
                 }

                 .button-a:focus,
                 .button-a:hover {
                   background-color: DarkGreen;
                 }
                 .button-d {
                   background-color: red;  
                   border-radius: 5px;
                   color: white;
                   padding: .5em;
                   text-decoration: none;
                 }

                 .button-d:focus,
                 .button-d:hover {
                   background-color: DarkRed;
                 }
                 div{
                  //border: 2px solid black;
                 }
                 </style>
                 </head>
                 <body>
                  <div class="main">
                    <img src="https://marketforceapp.com/marketforce/ec/images/mfl2.png" />
                    <h1>Time Off Request:</h1>
                    <div style="width: 100%;">
                    <ul>
                      <li><b>Name:</b> ' . $name . '</li>
                      <li><b>Position:</b> ' . $position . '</li>
                      <li><b>Supervisor Name:</b> ' . $emname . '</li>
                      <li><b>Request submitted at least 2 weeks prior to time off?</b> ';
    $vsdate = strtotime($sdate . "-2 Week");
    $cdate = strtotime("now");
    if ($cdate <= $vsdate) {
        $body .= '<span style="color:green;"><b>YES</b></span>';
    } else {
        $body .= '<span style="color:red;"><b>NO</b></span>';
    }
//This code will calculate TOTAL num of days (Including weekends and holidays)
    $b = strtotime($sdate);
    $e = strtotime($edate);
    $diff = $e - $b;
    $daysDiff = floor($diff / (60 * 60 * 24));
    $daysDiff++;
    $body .= '</li>
                        <li><b>Dates requested off:</b> ' . date("l F d, Y", strtotime($sdate)) . ' - ' . date("l F d, Y", strtotime($edate)) . '</li>
                        <li><b>Date Returning to Work:</b> ' . date("l F d, Y", strtotime($edate)) . '</li>
                        <li><b>Number of Days Off Requested:</b> ' . $numDays . '</li>
                        <li><b>Use:</b> ' . $pay . '</li>
                        <li><b>Reason For Request:</b> ' . $reason . '</li>
                       </ul>
                       </div>
                       <br><br>
                      <!-- <div style="width: 100%;" class="buttons">
                     <a class="button-a" href="https://marketforceapp.com/marketforce/ec/php/approve-decline.php?status=Approved&id=' . $fir['ID'] . '" target="_blank">Approve</a>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <a class="button-d" href="https://marketforceapp.com/marketforce/ec/php/approve-decline.php?status=Declined&id=' . $fir['ID'] . '" target="_blank">Decline</a>
                 </div>-->
                 </div>
                 </body>
                 </html>';
    mail_request($email,$subject,$body,$ecountry,$emmail,$rhemail);

}

echo 'Your Time Off Request Has Been Submitted!';
/*echo '<html>
      <head>
        <title>Thank You!</title>
        <style>
          .main{
            text-align:center;
          }
        </style>
        <script>
        function cw(){
          window.close();
        }
        </script>
      </head>
      <body>
        <div class="main">
          <img src="../images/mfl2.png" />
          <h1>Thank You!</h1>
          <h2>You&apos;re request has been submitted. You will receive a response within 48 hours.</h2>
          
          <button type="button" onclick="cw();">Close Window</button>
        </div>
      </body>
      </html>';*/
?>