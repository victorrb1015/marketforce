<?php
session_start();
include 'php/connection.php';

//Load User Info
if($_SESSION['user_id'] == '3' || $_SESSION['user_id'] == '1'){
	if(!$_POST['xx']){
		echo '<html>
					<div style="text-align:center;">
					<h1>Please select an employee to submit a Time-Off Request for:</h1>
					<form action="time-off.php" method="post">
					<select name="xx">
					<option value="">Select An Employee...</option>';
		$aq = "SELECT * FROM `users`";
		$ag = mysqli_query($conn, $aq) or die($conn->error);
		while($ar = mysqli_fetch_array($ag)){
			echo '<option value="' . $ar['ID'] . '">' . $ar['fname'] . ' ' . $ar['lname'] . '</option>';
		}
		echo '</select>
					<br><br>
					<input type="submit" value="Submit" />
					</form>
					</div>
					</html>';
		
		break;
					
	}else{
		$uiq = "SELECT * FROM `users` WHERE `ID` = '" . $_POST['xx'] . "'";
	}
}else{
$uiq = "SELECT * FROM `users` WHERE `ID` = '" . $_SESSION['user_id'] . "'";
}
$uig = mysqli_query($conn, $uiq) or die($conn->error);
$uir = mysqli_fetch_array($uig);
?>
<!DOCTYPE html>
<html>
<head>
<title>Time Off Request Form</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Easy Multiple Forms Widget Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!-- js 
<script src="js/jquery-1.11.1.min.js"></script> 
<!-- //js -->
	
    <!--JQuery-->
	<!--
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
-->

    <!-- Bootstrap Core CSS -->
		<link rel="stylesheet" href="css/bootstrap.css"/>
		<link rel="stylesheet" href="http://getbootstrap.com/2.3.2/assets/css/bootstrap-responsive.css"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css"/>
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		
		<!--
    <link href="../css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  	-->
	
 <!--Rock Salt Google Font-->
<link href="https://fonts.googleapis.com/css?family=Rock+Salt" rel="stylesheet">
	
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href='//fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
  <style>
    .bs{
      text-decoration: none;
      font-family: Rock Salt, Cursive;
    }
  </style>
</head>
<body>
	<div class="main">
		<br>
		<h1>Time Off Request Form</h1>
		<div class="w3l_main_grids">
			<div class="w3l_main_grids_left" style="margin:auto; width:85%; float:none;">
				<div class="w3l_main_grids_left_grid1">
					<form action="php/form-handler.php" method="post">
						Name: <input style="width:85%;" type="text" name="Name" placeholder="Your Name" value="<?php echo $uir['fname'] . ' ' . $uir['lname']; ?>" required=" " readonly>
            <br>Email: <input style="width:86%;" type="email" name="Email" placeholder="Your Email Address" value="<?php echo $uir['email']; ?>" required=" "><br>
						1st Date Off: <input style="width:90%;" type="text" class="datepicker" placeholder="mm/dd/yyyy" name="Sdate"  required=" "><br><br>
						Return To Work: <input style="width:90%;" type="text" class="datepicker" placeholder="mm/dd/yyyy" name="Edate" required=" ">
            <br><br>
						I Am Requesting To Use:&nbsp;
						<input type="radio" name="pay" value="Vacation Days" required=" " /> Vacation Days
						&nbsp;&nbsp;
						<input type="radio" name="pay" value="Sick Days" /> Sick Days
						&nbsp;&nbsp;
						<input type="radio" name="pay" value="Non Paid Days" /> Non Paid Days
						<br><br><br>
            Reason For Request:
						<textarea name="Reason" placeholder="Type Your Reason Here...." required=" "></textarea>
            <input type="hidden" name="type" value="Time Off Request" />
            <input type="hidden" name="position" value="<?php echo $uir['position']; ?>" />
            <input type="hidden" name="user_id" value="<?php echo $uir['ID']; ?>" />
            
						<!--
    						<input type="text"  class="datepicker">
						-->
						<br><br>
						<input type="submit" value="Submit">
					</form>
					<!--<div class="close"> </div>-->
				</div>
			</div>
				<script>
					$(document).ready(function(c) {
						$('.close').on('click', function(c){
							$('.w3l_main_grids_left_grid1').fadeOut('slow', function(c){
								$('.w3l_main_grids_left_grid1').remove();
							});
						});	  
					});
				</script>
      
			<div class="w3l_main_grids_right"></div>
			
			<br><br><br><br><br>
			<br><br><br><br><br>
			<br><br><br><br><br>
			<br><br><br><br><br>
			
		</div>
		<div class="copyright">
			<p style="color:black;">© 2017 Market Force Employee Center. All rights reserved | Implemented by <a class="bs" href="http://burtonsolution.tech">Burton Solution</a></p>
		</div>
	</div>
</body>
	<script>
		$('.datepicker').datepicker();
	</script>
</html>