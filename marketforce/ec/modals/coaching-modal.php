<div class="modal fade" role="dialog" tabindex="-1" id="coachingForm" style="margin-top:0px;margin-left:0px;padding-top:0px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Employee Coaching Form</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <p>Employee Name:</p>
                            <select id="ecename" class="form-control" name="ecename">
                              <option value="">Select One...</option>
                              <?php
                              $enq = "SELECT * FROM `users` WHERE `inactive` != 'Yes' ORDER BY `fname` ASC";
                              $eng = mysqli_query($conn, $enq) or die($conn->error);
                              while($enr = mysqli_fetch_array($eng)){
                                echo '<option value="' . $enr['ID'] . '">' . $enr['fname'] . ' ' . $enr['lname'] . '</option>';
                              }
                              ?>
                            </select>
                        </div>
                        <div
                            class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <p>Today's Date:</p><input type="text" placeholder="mm/dd/yyyy" id="ecdate" class="form-control date" name="ecdate"></div>
                    <div class="col-md-12"><br></div>
                    <div class="col-md-12"><br>
                        <p>Message:</p><textarea id="ecmess" class="form-control" name="ecmess"></textarea></div>
                </div>
            </div>
            <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Close</button><button class="btn btn-success" type="button" id="coach_sub_btn" onclick="coach();">Submit</button></div>
        </div>
    </div>
    </div>