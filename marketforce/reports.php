<?php
include 'security/session/session-settings.php';

if(!isset($_SESSION['in'])){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
		th, td{
			padding: 10px;
		}
		#dl, dc{
			height: 430px;
		}
		.panel-body{
			height: 382px;
		}
		#scroll{
			overflow: scroll;
		}
		::-webkit-scrollbar {
    width: 0px;  /* remove scrollbar space */
    background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
		
		
		.packet-table td, th{
			border: 1px solid black;
		}
		.packet-table{
			margin:auto;
		}
  </style>
  <script>
    
  function clr_res(id,s){
    //alert("test");
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			//alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://library.burtonsolution.tech/php/reserve.php?id="+id+"&s="+s,true);
  xmlhttp.send();
  }
		
		
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
       <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> Reports</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
														<li>
															<i class="fa fa-file"></i> Reports
													</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							
							
							
							
							<?php
							echo '<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-red">
										<div class="panel-heading">
											<h3 class="panel-title">
											 Market Force System Reports
											</h3>
										</div>
										<div class="panel-body" style="height:auto;">
											<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
												<tr style="background: gray;">
													<th>Report Name</th>
													<th>Report Description</th>
													<th>View</th>
												</tr>
												</thead>
												<tbody>';
								
									echo '<tr>
												<!--<td><b>Pre-Owned Subscribers</b></td>
												<td>This report displays all the subscribers on the pre-owned website</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/posub.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>-->
												
												<tr>
												<td><b>Inactive ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customers ";
                                    break;
                                case "123456":
                                    echo "Clients ";
                                    break;
                                default:
                                    echo 'Dealers ';
                            }
                            echo '</b></td>
												<td>This report displays all the ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customers";
                                    break;
                                case "123456":
                                    echo "Clients";
                                    break;
                                default:
                                    echo 'Dealers';
                            }
                            echo ' that have been set to inactive.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/view-inactive-dealers.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>
												
												<!--<tr>
												<td><b>Dealer Contacts By State</b></td>
												<td>This report displays all the dealer&apos;s contact information by State.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/dealers_by_state.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>-->';
								
								if($_SESSION['manager'] == 'Yes' || $_SESSION['admin'] == 'Yes'){
									echo '<tr>
												<td><b>Master Survey List</b></td>
												<td>This report displays all the surveys that were submitted for each OSR visit.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/master-survey-list.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
                }
              if($_SESSION['check_request'] == 'Yes' || $_SESSION['accountant'] == 'Yes'){
									echo '<tr>
												<td><b>Completed Check Requests</b></td>
												<td>This report displays all completed check requests by All Steel Carports.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/view-completed-check-requests.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
								}
												
							echo '<tr>
												<td><b>Completed Packets</b></td>
												<td>This report displays all the New ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customer ";
                                    break;
                                case "123456":
                                    echo "Client ";
                                    break;
                                default:
                                    echo 'Dealer ';
                            }
                            echo ' Packets and additional documents that have been completed.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/view-completed-packets.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
							
							
							echo '<!--<tr>
												<td><b>New Dealers</b></td>
												<td>This report displays all the new dealers that have not been visited or trained yet.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/view-new-dealers.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>-->';
							
//Check number of errors...							
$aiq = "SELECT * FROM `dealers` 
      WHERE 
      `address` = '' OR 
      `city` = '' OR 
      `state` = '' OR 
      `zip` = '' OR 
      `lat` = '' OR 
      `lat` = 'Error' OR 
      `lng` = '' OR 
      `lng` = 'Error' ORDER BY `name` ASC";
$aig = mysqli_query($conn, $aiq) or die($conn->error);
$ainum = mysqli_num_rows($aig);
							
							echo '<tr>
												<td><b>';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customer ";
                                    break;
                                case "123456":
                                    echo "Client ";
                                    break;
                                default:
                                    echo 'Dealer ';
                            }
                            echo ' Address Issues</b>&nbsp;<span style="background:red;color:white;padding:5px;border-radius:20px;">' . $ainum . '</span></td>
												<td>This report displays all the ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customers";
                                    break;
                                case "123456":
                                    echo "Clients";
                                    break;
                                default:
                                    echo 'Dealers';
                            }
                            echo ' with address issues.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/view-address-issues.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
							
							
							echo '<!--<tr>
												<td><b>Unmanned Lots</b></td>
												<td>This report displays all the dealer locations that are unmanned lots.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/view-unmanned-lots.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>-->';
							
							
							echo '<!--<tr>
												<td><b>Dealer Commission</b></td>
												<td>This report displays all the commission rates for each dealer.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/view-commision-rates.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>-->';
							
							
							echo '<tr>
												<td><b>Internal Map Notes</b></td>
												<td>This report displays all the internal notes added from the ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customer ";
                                    break;
                                case "123456":
                                    echo "Client ";
                                    break;
                                default:
                                    echo 'Dealer ';
                            }
                            echo ' map.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/view-map-notes.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
							
							if($_SESSION['collections'] == 'Yes'){
								echo '<tr>
												<td><b>Collections Paid Report</b></td>
												<td>This report displays all the customers who have paid all of their debt to the collections department.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/collections-paid.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
                
                echo '<tr>
												<td><b>Collections Deleted/Inactive Report</b></td>
												<td>This report displays all the customers who have been removed or deleted from the collections department.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/collections-inactive.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
								
								echo '<tr>
												<td><b>Collection Payments By Date</b></td>
												<td>This report displays all the payments made in Collections on a selected date.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/collection-payments-by-day.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
							}
							
							echo '<tr>
												<td><b>Collection Contact Info</b></td>
												<td>This report displays all of the customers with missing contact info in Collections.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/collection-no-emails.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
							
							
							echo '<!--<tr>
												<td><b>Untrained Dealer List By State</b></td>
												<td>This report displays all the dealers, by state, that have not been trained on the new pricing.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/view-untrained-dealers.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>-->';
							
							
							echo '<tr>
												<td><b>Repairs By State</b></td>
												<td>This report displays all the repairs by state.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/repairs-by-state.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
							
							
							echo '<tr>
												<td><b>Repairs By Contractor</b></td>
												<td>This report displays all the repairs by contractor.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/repairs-by-contractor.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
							
							
							echo '<!--<tr>
												<td><b>2017 New Dealers</b></td>
												<td>This report displays all the New Dealers in 2017.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/2017-new.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>-->';
              
              
              echo '<!--<tr>
												<td><b>2018 New Dealers</b></td>
												<td>This report displays all the New Dealers in 2018.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/dealers/2018-new.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>-->';
              
              
              echo '<tr>
												<td><b>2021 New ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customers ";
                                    break;
                                case "123456":
                                    echo "Clients ";
                                    break;
                                default:
                                    echo 'Dealers ';
                            }
                            echo '</b></td>
												<td>This report displays all the New ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customer";
                                    break;
                                case "123456":
                                    echo "Client";
                                    break;
                                default:
                                    echo 'Dealer';
                            }
                            echo ' in 2021.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/dealers/2021-new.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
							
							
							echo '<tr>
												<td><b>Master ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customer ";
                                    break;
                                case "123456":
                                    echo "Client ";
                                    break;
                                default:
                                    echo 'Dealer ';
                            }
                            echo ' List</b></td>
												<td>This report displays all ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customers";
                                    break;
                                case "123456":
                                    echo "Clients";
                                    break;
                                default:
                                    echo 'Dealers';
                            }
                            echo ' in Market Force.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/master-dealer-list.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
							
							
							echo '<!--<tr>
												<td><b>Dealer Count By State</b></td>
												<td>This report displays the dealer count for each state in Market Force.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/dealer-count.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>-->';
              
//Count Address issues in Scheduling System...
$eq = "SELECT * FROM `salesPortalLinked` WHERE `archieved` != 'Yes'";
$eg = mysqli_query($conn, $eq) or die($conn->error);
$ecount = 0;
while($er = mysqli_fetch_array($eg)){
  $aq = "SELECT * FROM `portal_relationship` WHERE `orderID` = '" . $er['orderID'] . "'";
  $ag = mysqli_query($conn, $aq) or die($conn->error);
  $ar = mysqli_fetch_array($ag);
  if(mysqli_num_rows($ag) <= 0 || $ar['lat'] == '' || $ar['lat'] == 'Error' || $ar['lng'] == '' | $ar['lng'] == 'Error'){
    $ecount++;
  }
}
              
              echo '<tr>
												<td><b>Scheduling Address Issue Report</b>&nbsp;<span style="background:red;color:white;padding:5px;border-radius:20px;">' . $ecount . '</span></td>
												<td>This report displays the invoices that contain possible issues with the address listed.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/scheduling/scheduling-error-report.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
              
              
              
              echo '<tr>
												<td><b>Closed ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customer";
                                    break;
                                case "123456":
                                    echo "Client";
                                    break;
                                default:
                                    echo 'Dealer';
                            }
                            echo ' Report</b></td>
												<td>This report displays the ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customers";
                                    break;
                                case "123456":
                                    echo "Clients";
                                    break;
                                default:
                                    echo 'Dealers';
                            }
                            echo ' who were closed in Market Force by year and state.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/view-closed-dealers.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
              
              
              echo '<tr>
												<td><b>Inactive Repairs Report</b></td>
												<td>This report displays all the inactive Repairs in MarketForce.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/repairs/view-inactive-repairs.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
              
              
              echo '<tr>
												<td><b>';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customer ";
                                    break;
                                case "123456":
                                    echo "Client ";
                                    break;
                                default:
                                    echo 'Dealer ';
                            }
                            echo ' Displays Report</b></td>
												<td>This report shows all the Displays for each ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customer ";
                                    break;
                                case "123456":
                                    echo "Client ";
                                    break;
                                default:
                                    echo 'Dealer ';
                            }
                            echo ' in MarketForce by state.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/view-dealer-displays.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
              
              
              echo '<tr>
												<td><b>Master ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customer ";
                                    break;
                                case "123456":
                                    echo "Client ";
                                    break;
                                default:
                                    echo 'Dealer ';
                            }
                            echo ' Assignment Report</b></td>
												<td>This report shows all active ';
                            switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customers";
                                    break;
                                case "123456":
                                    echo "Clients";
                                    break;
                                default:
                                    echo 'Dealers';
                            }
                            echo ' with their location and assigned OSTR.</td>
												<td><a href="http://marketforceapp.com/marketforce/reports/master-dealer-assignments.php" target="_blank">
												<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>
												</tr>';
              
							
												
							echo '</tbody>
										</table>
										</div>
										</div>
									</div>
								</div>
							</div><!--End Row-->';
							
							
				
							?>
							
							
							</div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>

	

 <?php include 'footer.html'; ?>
</body>

</html>