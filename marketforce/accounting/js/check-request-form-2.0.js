    function submit_nc_form(){
      
      var version = document.getElementById('ncversion').value;
      if(version === 'General Payment'){
        var user = '<?php echo $_SESSION['full_name']; ?>';
        var user_id = '<?php echo $_SESSION['user_id']; ?>';
        var amount = document.getElementById('gncamount').value;
        var to = document.getElementById('gncpayto').value;
        var address = document.getElementById('gncaddress').value;
        var apt = document.getElementById('gncapt').value;
        var city = document.getElementById('gnccity').value;
        var state = document.getElementById('gncstate').value;
        var zip = document.getElementById('gnczip').value;
        var phone = document.getElementById('gncphone').value;
        var inv = document.getElementById('gncinvnum').value;
        var gncnotes = document.getElementById('gncnotes').value;
        var ta1 = '';
        var ta2 = '';
        var ta3 = '';
        var ta4 = '';
      }else{
        var user = '<?php echo $_SESSION['full_name']; ?>';
        var user_id = '<?php echo $_SESSION['user_id']; ?>';
        var amount = document.getElementById('ncamount').value;
        var to = document.getElementById('ncpayto').value;
        var address = document.getElementById('ncaddress').value;
        var apt = document.getElementById('ncapt').value;
        var city = document.getElementById('nccity').value;
        var state = document.getElementById('ncstate').value;
        var zip = document.getElementById('nczip').value;
        var phone = document.getElementById('ncphone').value;
        var inv = document.getElementById('ncinvnum').value;
        var ta1 = document.getElementById('ncta1').value;
        var ta2 = document.getElementById('ncta2').value;
        var ta3 = document.getElementById('ncta3').value;
        var ta4 = document.getElementById('ncta4').value;
        var gncnotes = '';
      }
      
  
	
	//Required Fields...
	if(user.value === '' || user_id.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'An error has occured processing this request! Please contact your system administrator!';
		return;
	}
	if(amount.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please enter the amount requested!';
		return;
	}
   amount = encodeURL(amount);
	if(to.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please enter the name of the payee!';
		return;
	}
    to = encodeURL(to);
	if(address.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please enter the payee address!';
		return;
	}
    address = encodeURL(address);
	if(city.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please enter the city!';
		return;
	}
   city = encodeURL(city);
	if(zip.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please enter the zip code!';
		return;
	}
   zip = encodeURL(zip);
	if(phone.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please enter the payee phone number!';
		return;
	}
   phone = encodeURL(phone);
      
if(version === 'Refund'){
	if(ta1.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please ensure all questions are answered!';
		return;
	}
   ta1 = encodeURL(ta1);
	if(ta2.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please ensure all questions are answered!';
		return;
	}
   ta2 = encodeURL(ta2);
	if(ta3.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please ensure all questions are answered!';
		return;
	}
   ta3 = encodeURL(ta3);
	if(ta4.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please ensure all questions are answered!';
		return;
	}
   ta4 = encodeURL(ta4);
}else{
  if(gncnotes.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please add a quick note about the check request!';
		return;
	}
   gncnotes = encodeURL(gncnotes);
}
  
      $("#newCheckRequest").modal("hide");
      

  
	//Disable button after initial submit...
  //document.getElementById('sub_button').disabled = true;
	
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","accounting/php/submit-check-request-form.php?"+
               "user="+user+
               "&user_id="+user_id+
               "&amount="+amount+
               "&to="+to+
               "&address="+address+
               "&apt="+apt+
               "&city="+city+
               "&state="+state+
               "&zip="+zip+
               "&phone="+phone+
							 "&inv="+inv+
               "&ta1="+ta1+
               "&ta2="+ta2+
               //"&test=True"+//Comment This line out when not in test mode (turns off email notifications only)
               "&ta3="+ta3+
               "&ta4="+ta4+
               "&gncnotes="+gncnotes+
               "&version="+version,true);
  xmlhttp.send();
  }