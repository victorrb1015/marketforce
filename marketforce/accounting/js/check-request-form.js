function submit_form(){
  var user = document.getElementById('user');
  var user_id = document.getElementById('user_id');
  var amount = document.getElementById('amount');
  var to = document.getElementById('to');
  var address = document.getElementById('address');
  var apt = document.getElementById('apt');
  var city = document.getElementById('city');
  var state = document.getElementById('state');
  var zip = document.getElementById('zip');
  var phone = document.getElementById('phone');
	var inv = document.getElementById('inv');
  var ta1 = document.getElementById('ta1');
  var ta2 = document.getElementById('ta2');
  var ta3 = document.getElementById('ta3');
  var ta4 = document.getElementById('ta4');
	
	//Required Fields...
	if(user.value === '' || user_id.value === ''){
		alert('An error has occured processing this request! Please contact your system administrator!');
		return;
	}
	if(amount.value === ''){
		alert('Please enter the amount requested!');
		return;
	}
	if(to.value === ''){
		alert('Please enter the name of the payee!');
		return;
	}
	if(address.value === ''){
		alert('Please enter the payee address!');
		return;
	}
	if(city.value === ''){
		alert('Please enter the city!');
		return;
	}
	if(zip.value === ''){
		alert('Please enter the zip code!');
		return;
	}
	if(phone.value === ''){
		alert('Please enter the payee phone number!');
		return;
	}
	if(ta1.value === ''){
		alert('Please ensure all questions are answered!');
		return;
	}
	if(ta2.value === ''){
		alert('Please ensure all questions are answered!');
		return;
	}
	if(ta3.value === ''){
		alert('Please ensure all questions are answered!');
		return;
	}
	if(ta4.value === ''){
		alert('Please ensure all questions are answered!');
		return;
	}
  
  //Parse for the & symbol...
  var userv = user.value.replace('&','%26');
  var tov = to.value.replace('&','%26');
  var addressv = address.value.replace('&','%26');
  var aptv = apt.value.replace('&','%26');
  var cityv = city.value.replace('&','%26');
  var statev = state.value.replace('&','%26');
  var phonev = phone.value.replace('&','%26');
  var ta1v = ta1.value.replace('&','%26');
  var ta2v = ta2.value.replace('&','%26');
  var ta3v = ta3.value.replace('&','%26');
  var ta4v = ta4.value.replace('&','%26');
	var inv = inv.value.replace('&','%26');
	var inv = inv.replace('#','%23');
	var userv = userv.replace('#','%23');
  var tov = tov.replace('#','%23');
  var addressv = addressv.replace('#','%23');
  var aptv = aptv.replace('#','%23');
  var cityv = cityv.replace('#','%23');
  var statev = statev.replace('#','%23');
  var phonev = phonev.replace('#','%23');
  var ta1v = ta1v.replace('#','%23');
  var ta2v = ta2v.replace('#','%23');
  var ta3v = ta3v.replace('#','%23');
  var ta4v = ta4v.replace('#','%23');
  
	//Disable button after initial submit...
  document.getElementById('sub_button').disabled = true;
	
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      
    }
  }
  xmlhttp.open("GET","accounting/php/submit-check-request-form.php?"+
               "user="+userv+
               "&user_id="+user_id.value+
               "&amount="+amount.value+
               "&to="+tov+
               "&address="+addressv+
               "&apt="+aptv+
               "&city="+cityv+
               "&state="+statev+
               "&zip="+zip.value+
               "&phone="+phonev+
							 "&inv="+inv+
               "&ta1="+ta1v+
               "&ta2="+ta2v+
               "&ta3="+ta3v+
               "&ta4="+ta4v,true);
  xmlhttp.send();
  }