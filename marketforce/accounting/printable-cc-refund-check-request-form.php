<?php
include '../php/connection.php';
//Security Code Here...
if($_SESSION['check_request'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com/";
				}, 2000);
				</script>';
	return;
}


$q = "SELECT * FROM `check_requests` WHERE `ID` = '" . $_GET['id'] . "'";
$g = mysqli_query($conn,$q) or die('Error: ' . $conn->error);
$r = mysqli_fetch_array($g);

echo '<html>
<head>
<script src="js/check-request-form.js"></script>
<title> Check Request Form </title>
<style>
html{
  -webkit-print-color-adjust:exact;
}
.red{
  font-weight:bold;
  background-color:red;
  padding: 4px;
  color:white;
}
.blue{
  font-weight:bold;
  background-color:blue;
  padding: 4px;
  color:white;
}
.green{
  font-weight:bold;
  background-color:green;
  padding: 4px;
  color:white;
}
h2{
text-align:center;
}
#button{
padding: 5px;
background-color: green;
color: white;
font-weight: bold;
}
</style>
<script>
function close_window(){
	window.close();
}
</script>
<body>

<h2><u> All Steel ' . $r['version'] . ' Check Request</u></h2>
<h3>Submit Date: ' . date("m/d/Y", strtotime($r['date'])) . '</h3>

<p><b>Employee Submitting This Request:</b> ' . $r['user'] . '</p>

<p><b>Check Amount:</b> $' . $r['amount'] . '</p>

<p><b>Payable To:</b> ' . $r['to'] . '</p>

<p><b>Invoice#:</b> ' . $r['inv'] . '</p>

<p><b>STATUS: </b>';
if($r['status'] == 'Pending'){
  echo '<span class="blue">';
}
if($r['status'] == 'Need Info'){
  echo '<span class="red">';
}
if($r['status'] == 'Completed'){
  echo '<span class="green">';
}

echo $r['status'] . '
</span></p>

<p>
<b>Who authorized this refund?</b><br>
' . $r['ta2'] . '
</p>
<p>
<b>Explanation of Refund Authorization:</b><br>
' . $r['ta3'] . '
</p>';

if($r['status'] == 'Completed'){
  echo '<br>
        <h3><u>Check Details:</u></h3>
        <p><b>Check#:</b> ' . $r['cnum'] . '</p>
        <p><b>Check Amount:</b> ' . $r['camount'] . '</p>
        <p><b>Date Check Was Mailed:</b> ' . $r['mdate'] . '</p>';
}

echo '</body>
</html>';

?>