<?php
include '../php/connection.php';
//Security Code Here...
if($_SESSION['check_request'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com/";
				}, 2000);
				</script>';
	return;
}


echo '<html>
<head>
<script src="js/check-request-form.js"></script>
<title> Check Request Form </title>
<style>
.red{
  font-weight:bold;
  background-color:red;
  padding: 4px;
  color:white;
}
.blue{
  font-weight:bold;
  background-color:blue;
  padding: 4px;
  color:white;
}
h2{
text-align:center;
}
#button{
padding: 5px;
background-color: green;
color: white;
font-weight: bold;
}
</style>
<script>
function close_window(){
	window.close();
}
</script>
<body>

<h2><u> ALL STEEL REFUND/DISCOUNT: CHECK REQUEST </u></h2>
<h3>Today&apos;s Date: ' . date("m/d/Y") . '</h3>

<p><b>Employee Submitting This Request:</b> <input type="text" id="user" name="user" value="' . $_SESSION['full_name'] . '" disabled/></p>
<input type="hidden" id="user_id" name="user_id" value="' . $_SESSION['user_id'] . '" />

<p><b>Check Amount:</b> $<input type="text" name="amount" id="amount" />(Only numbers and decimals)</p>

<p><b>Payable To:</b> <input type="text" name="to" id="to" /></p>

<p>
<b>Pay Address:</b> <input type="text" name="address" id="address" style="width:200px;" /> &nbsp;&nbsp; 
<b>Apt/Unit#</b> <input type="text" id="apt"/>
</p>

<p>
<b>City:</b> <input type="text" id="city" name="city" /> &nbsp;&nbsp; 
<b>State:</b> <input type="text" id="state"/>  &nbsp;&nbsp; 
<b>Zip:</b> <input type="text" id="zip"/>
</p>

<p><b>Phone:</b> <input type="text" name="phone" id="phone" /></p>

<p><b>Invoice#:</b> <input type="text" name="inv" id="inv" /></p>

<!--<p><b>SUPERVISOR APPROVAL:</b>  <span class="blue">Not Submitted</span></p>-->

<p>
<b>Who communicated the discount/refund to the customer?</b><br>
<textarea id="ta1" style="width:500px;height:75px;"></textarea>
</p>

<p>
<b>Who are the parties who approved the discount/refund?</b><br>
<textarea id="ta2" style="width:500px;height:75px;"></textarea>
</p>

<p>
<b>What were the circumstances that lead to this decision?</b><br>
<textarea id="ta3" style="width:500px;height:75px;"></textarea>
</p>

<p>
<b>Are there any additional notes in the system on this issue that can be copied here?</b><br>
<textarea id="ta4" style="width:500px;height:75px;"></textarea>
</p>

<div id="button-div">
<button id="sub_button" type="button" onclick="submit_form();">Submit Form</button>
&nbsp;&nbsp;
<button type="button" onclick="close_window();">Close Window</button>
</div>

</body>
</html>';

?>