<?php
include '../../php/connection.php';

//Load Variables
$user = mysqli_real_escape_string($conn,$_GET['user']);
$user_id = $_GET['user_id'];
$amount = mysqli_real_escape_string($conn,$_GET['amount']);
$to = mysqli_real_escape_string($conn,$_GET['to']);
$address = mysqli_real_escape_string($conn,$_GET['address']);
$apt = mysqli_real_escape_string($conn,$_GET['apt']);
$city = mysqli_real_escape_string($conn,$_GET['city']);
$state = mysqli_real_escape_string($conn,$_GET['state']);
$zip = mysqli_real_escape_string($conn,$_GET['zip']);
$phone = mysqli_real_escape_string($conn,$_GET['phone']);
$inv = mysqli_real_escape_string($conn,$_GET['inv']);
$ta1 = mysqli_real_escape_string($conn,$_GET['ta1']);
$ta2 = mysqli_real_escape_string($conn,$_GET['ta2']);
$ta3 = mysqli_real_escape_string($conn,$_GET['ta3']);
$ta4 = mysqli_real_escape_string($conn,$_GET['ta4']);


//Check if exists in database...
$q = "INSERT INTO `check_requests`
      (
      `date`,
      `time`,
      `type`,
      `user`,
      `user_id`,
      `amount`,
      `to`,
      `address`,
      `apt`,
      `city`,
      `state`,
      `zip`,
      `phone`,
      `inv`,
      `ta1`,
      `ta2`,
      `ta3`,
      `ta4`,
      `status`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      'Check Request',
      '" . $user . "',
      '" . $user_id . "',
      '" . $amount . "',
      '" . $to . "',
      '" . $address . "',
      '" . $apt . "',
      '" . $city . "',
      '" . $state . "',
      '" . $zip . "',
      '" . $phone . "',
      '" . $inv . "',
      '" . $ta1 . "',
      '" . $ta2 . "',
      '" . $ta3 . "',
      '" . $ta4 . "',
      'Pending'
      )";

$g = mysqli_query($conn, $q) or die($conn->error);

//Get user info
$q = "SELECT * FROM `users` WHERE `ID` = '" . $user_id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
$email = $r['email'];

//Setup Email System
include '../../../phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include '../../../phpmailsettings.php';

$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
//$mail->addAddress('scott@allsteelcarports.com');
$mail->addAddress('ap@allsteelcarports.com');
//$mail->addAddress('michael@burtonsolution.tech');//Test mode only...
$mail->addBCC('archive@ignition-innovations.com');
$mail->Subject = 'Check Request!';

$mail->Body = '<html>
            <head>
            <style>
            img{
              width:50%;
            }
            .main{
              text-align:center;
            }
            </style>
            </head>
            <body>
            <div class="main">
            <img src="http://marketforceapp.com/marketforce/img/Market%20Force%20Logo%202.png"/>
            <h1>Check Request</h1>
            <h3>You have a new check request from <mark>' . $user . '</mark> in Market Force. Please Log In to process this request!</h3>
            </div>
            </body>
            </html>';

if($_GET['test'] != 'True') {
  
    if( $mail->send() ){
   
    echo 'Your Check Request Has Been Submitted!';
    }else{
      echo 'There was an error processing your request. Please contact your system administrator!';
    }
  
}else{
  echo 'Testing was successful!';
}



?>
      