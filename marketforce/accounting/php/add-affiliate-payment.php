<?php
include '../../php/connection.php';

//Load Variables...
$invNum = mysqli_real_escape_string($conn, $_REQUEST['invNum']);
$checkNum = mysqli_real_escape_string($conn, $_REQUEST['checkNum']);
$checkDate = mysqli_real_escape_string($conn, $_REQUEST['checkDate']);
$cdate = date("m/d/y",strtotime($checkDate));
$cid = mysqli_real_escape_string($conn, $_REQUEST['cid']);
$paymentAmount = mysqli_real_escape_string($conn, $_REQUEST['payAmount']);

//Add Payment Details...
$q = "INSERT INTO `order_invoice_affiliate_payments`
      (
      `date`,
      `time`,
      `rep_id`,
      `rep_name`,
      `cid`,
      `inv_num`,
      `payment_type`,
      `payment_ref`,
      `payment_amount`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $_SESSION['user_id'] . "',
      '" . $_SESSION['full_name'] . "',
      '" . $cid . "',
      '" . $invNum . "',
      'Check',
      'Check Number: " . $checkNum . " paid on " . $cdate . "',
      '" . $paymentAmount . "',
      'No'
      )";
  mysqli_query($conn, $q) or die($conn->error);

echo '<script>
        window.location = "../../accounting.php?tab=a";
      </script>';