    <div class="modal fade" role="dialog" tabindex="-1" id="newCheckRequest">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">New Check Request</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <ul class="nav nav-tabs nav-justified" role="tablist">
                            <li class="nav-item"><a class="nav-link active" href="#tab-1" role="tab" data-toggle="tab" style="" onclick="change_version('Refund');">Refund and Commission</a></li>
                            <li class="nav-item"><a class="nav-link" href="#tab-2" role="tab" data-toggle="tab" style="" onclick="change_version('General Payment');">General Payments</a></li>
                            <li class="nav-item"><a class="nav-link" href="#tab-3" role="tab" data-toggle="tab" style="" onclick="change_version('Credit Card');">Credit Card Refund</a></li>
                        </ul>
                        <div class="tab-content">
                          <div class="tab-pane" role="tabpanel" id="tab-3"><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Credit Card Refund</h3><br></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Refund Amount:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span><i class="fa fa-dollar"></i></span></div><input class="form-control" type="text" id="ccramount">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Customer Name:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span><i class="fa fa-user"></i></span></div><input class="form-control" type="text" id="ccrcname">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Invoice Number:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span><i class="fa fa-hashtag"></i></span></div><input class="form-control" type="text" id="ccrinvnum">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Who authorized this refund?</p><textarea id="ccrwhoauthorized" class="form-control"></textarea></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Explanation of Refund Authorization:</p><textarea id="ccrauthexplanation" class="form-control" style="width:100%;"></textarea></div>
                                </div>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="tab-2"><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="text-center">General Payments</h3><br></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Check Amount:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span><i class="fa fa-dollar"></i></span></div><input class="form-control" type="text" id="gncamount">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Payable To:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span><i class="fa fa-user"></i></span></div><input class="form-control" type="text" id="gncpayto">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Invoice Number:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span><i class="fa fa-hashtag"></i></span></div><input class="form-control" type="text" id="gncinvnum">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Payee Address:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span></span></div><input class="form-control" type="text" id="gncaddress">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Appartment: (Optional)</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span></span></div><input class="form-control" type="text" id="gncapt">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>City:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span></span></div><input class="form-control" type="text" id="gnccity">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>State:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span></span></div><input class="form-control" type="text" id="gncstate">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Zip Code:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span></span></div><input class="form-control" type="text" id="gnczip">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Phone Number:&nbsp;</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span><i class="fa fa-phone"></i></span></div><input class="form-control" type="text" id="gncphone">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Notes:</p><textarea id="gncnotes" class="form-control" style="width:100%;"></textarea></div>
                                </div>
                            </div>
                            <div class="tab-pane active" role="tabpanel" id="tab-1"><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Refund and Commission</h3><br></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Check Amount:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span><i class="fa fa-dollar"></i></span></div><input class="form-control" type="text" id="ncamount">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Payable To:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span><i class="fa fa-user"></i></span></div><input class="form-control" type="text" id="ncpayto">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Invoice Number:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span><i class="fa fa-hashtag"></i></span></div><input class="form-control" type="text" id="ncinvnum">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>Payee Address:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span></span></div><input class="form-control" type="text" id="ncaddress">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Appartment: (Optional)</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span></span></div><input class="form-control" type="text" id="ncapt">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>City:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span></span></div><input class="form-control" type="text" id="nccity">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>State:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span></span></div><input class="form-control" type="text" id="ncstate">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Zip Code:</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span></span></div><input class="form-control" type="text" id="nczip">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Phone Number:&nbsp;</p>
                                        <div class="input-group">
                                            <div class="input-group-addon"><span><i class="fa fa-phone"></i></span></div><input class="form-control" type="text" id="ncphone">
                                            <div class="input-group-addon"></div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Who communicated the discount/refund to the customer?</p><textarea id="ncta1" class="form-control" style="width:100%;"></textarea></div>
                                    <div class="col-md-12"><br></div>
                                    <div class="col-md-12">
                                        <p>Who are the parties approving the discount/refund to the customer?</p><textarea id="ncta2" class="form-control" style="width:100%;"></textarea></div>
                                    <div class="col-md-12"><br></div>
                                    <div class="col-md-12">
                                        <p>What were the circumstances that lead to this decision?</p><textarea id="ncta3" class="form-control" style="width:100%;"></textarea></div>
                                    <div class="col-md-12"><br></div>
                                    <div class="col-md-12">
                                        <p>Are there any additional notes in the system on this issue that can be copied here?</p><textarea id="ncta4" class="form-control" style="width:100%;"></textarea></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <input type="hidden" id="version" value="Refund" />
                  <p id="nc_error_msg" style="color:red;font-weight:bold;"></p>
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button><button class="btn btn-primary" type="button" onclick="submit_nc_form();">Save</button></div>
            </div>
        </div>
    </div>