    <div class="modal fade" role="dialog" tabindex="-1" id="crdocUpload">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Upload Attachment</h4>
                </div>
              <form enctype="multipart/form-data" action="accounting/php/upload-cr-file.php" method="post">
                <div class="modal-body">
                  <h3>File To Attach:</h3>
                  <input type="file" id="crfile" name="crfile" class="form-control" />
                </div>
                <input type="hidden" id="crID" name="crID" />
                <input type="hidden" id="crorgID" name="crorgID" />
                <div class="modal-footer">
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Upload</button>
                </div>
              </form>
            </div>
        </div>
    </div>