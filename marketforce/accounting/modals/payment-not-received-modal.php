  <div class="modal fade" id="notReceived" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Payment Not Received!</h4>
        </div>
        <div class="modal-body">
					<p>Please Enter Details:</p>
					<textarea id="approve_note" class="form-control"></textarea>
					<input type="hidden" id="approve_id" />
					<span style="color:red;font-weight:bold;" id="approve_error_msg"></span>
        </div>
        <div class="modal-footer">
					<button type="button" class="btn btn-success"  onclick="c_payment('decline');">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>