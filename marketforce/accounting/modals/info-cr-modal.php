			 <!-- Modal -->
  <div class="modal fade" id="checkNeedInfo" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Request Additional Info for <span id="info_name" style="color:red;font-weight:bold;"></span></h4>
        </div>
        <div class="modal-body">
          <p>What Additional Information Do You Require?</p>
					<textarea id="add_info" class="form-control"></textarea>
					
					<input type="hidden" id="info_id" value="" />
          <input type="hidden" id="info_orgID" />
					<br><br>
					<p id="info_error_msg" style="color:red;font-weight:bold;"></p>
        </div>
        <div class="modal-footer">
					<button type="button" class="btn btn-success" onclick="check_request('',2);">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->