			 <!-- Modal -->
  <div class="modal fade" id="completeCheck" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Complete Check Request For <span id="ccrName" style="color:red;font-weight:bold;"></span></h4>
        </div>
        <div class="modal-body">
					<table>
						<tr>
							<td>
         			  <p>Check #:</p>
							  <input type="text" class="form-control" id="cnum" />
							</td>
							<td>
								<p>Check Amount:</p>
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">$</span>
								  <input type="text" class="form-control" id="camount" aria-describedby="basic-addon1">
								</div>
							</td>
							<td>
								<p>Date Check Was Mailed</p>
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
								  <input type="date" class="form-control" id="mdate" aria-describedby="basic-addon1">
								</div>
							</td>
						</tr>
					</table>
					<p>Notes:</p>
					<textarea id="completed_notes" class="form-control"></textarea>
					<input type="hidden" id="completed_id" />
          <input type="hidden" id="completed_orgID" />
					<br><br>
					<p id="completed_error_msg" style="color:red;font-weight:bold;"></p>
        </div>
        <div class="modal-footer">
					<button type="button" class="btn btn-success" onclick="check_request('',1);">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->