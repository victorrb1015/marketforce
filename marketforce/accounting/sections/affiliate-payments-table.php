<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-danger card-view">
			<div class="panel-heading">
				<h3 class="panel-title">
					Pending Affiliate Payments
				</h3>
			</div>
			<div class="panel-body" id="scroll" style="height:auto;">
				<div class="table-responsive">
				  <table class="table table-bordered table-hover table-striped packet-table">
            <thead>
              <tr>
                <th>Inv#</th>
                <th>Customer</th>
                <th>Internal Amount</th>
                <th>External Amount</th>
                <th>Payout Amount</th>
                <th>Payout Details</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
            <?php
              $oq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `status` = 'paid'";
              $og = mysqli_query($conn, $oq) or die($conn->error);
              while($or = mysqli_fetch_array($og)){
                $iq = "SELECT * FROM `order_invoices` WHERE `inactive` != 'Yes' AND `affiliate_mode` = 'Yes' AND `inv_num` = '" . $or['inv'] . "'";
                $ig = mysqli_query($conn, $iq) or die($conn->error);
                $ir = mysqli_fetch_array($ig);
                if(mysqli_num_rows($ig) > 0){
                
                  $apq = "SELECT * FROM `order_invoice_affiliate_payments` WHERE `inv_num` = '" . $ir['inv_num'] . "' AND `cid` = '" . $ir['cid'] . "'";
                  $apg = mysqli_query($conn, $apq) or die($conn->error);
                  if(mysqli_num_rows($apg) <= 0){
                    $nTotal = 0;
                    $aTotal = 0;
                    //Get Invoice Total...
                    $iiq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $or['inv'] . "'";
                    $iig = mysqli_query($conn, $iiq) or die($conn->error);
                    while($iir = mysqli_fetch_array($iig)){

                      //Check if item is a coil...
                      $ccq = "SELECT * FROM `inventory_items` WHERE `id` = '" . $iir['pid'] . "'";
                      $ccg = mysqli_query($conn, $ccq) or die($conn->error);
                      $ccr = mysqli_fetch_array($ccg);

                      $nTotal = $nTotal + (number_format($iir['item_rate'],2) * $iir['item_qty']);
                      $aTotal = $aTotal + (number_format($iir['item_arate'],2) * $iir['item_qty']);

                    }

                    echo '<tr style="color:#FFF;">
                            <td>' . $ir['inv_num'] . '</td>
                            <td>' . $ir['cname'] . '</td>
                            <td>$' . number_format($nTotal,2) . '</td>
                            <td>$' . number_format($aTotal,2) . '</td>
                            <td style="background:red;">$' . number_format(($aTotal - $nTotal),2) . '</td>
                            <td>
                              <form action="accounting/php/add-affiliate-payment.php" method="POST">
                                <input type="hidden" id="invNum" name="invNum" value="' . $ir['inv_num'] . '" />
                                <input type="hidden" id="payAmount" name="payAmount" value="' . number_format(($aTotal - $nTotal),2) . '" />
                                <input type="hidden" id="cid" name="cid" value="' . $ir['cid'] . '" />
                                <label for="checkNum">
                                  Check#
                                  <input type="text" class="form-control" name="checkNum" placeholder="Check#" autocomplete="off" required />
                                </label>
                                <label for="checkDate">
                                  Payment Date
                                  <input type="text" class="form-control date" name="checkDate" placeholder="MM/DD/YYYY" autocomplete="off" required />
                                </label>
                                <br><br>
                                <button type="submit" name="submit" class="btn btn-success btn-sm">Mark Affiliate as Paid</button>
                              </form>
                            </td>
                            <td>
                              <a class="btn btn-primary btn-sm" href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/orders/invoice/invoice.php?org_id=' . $_SESSION['org_id'] . '&mode=APrint&inv=' . $ir['inv_num'] . '" target="_blank" />
                                View Invoice
                              </a>
                            </td>
                          </tr>';
                  }
                }
              }
            ?>
            </tbody>
			    </table>
			  </div>
			</div>
		</div>
	</div>
</div><!--End Row-->
							