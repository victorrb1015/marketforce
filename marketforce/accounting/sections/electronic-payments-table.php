<?php
							echo '<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-danger card-view">
										<div class="panel-heading">
											<h3 class="panel-title">
												Electronic Collections Payment Confirmation Requests
											</h3>
										</div>
										<div class="panel-body" id="scroll" style="height:auto;">
											<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
												<tr>
													<th>From</th>
													<th>Document Type</th>
													<th>To</th>
													<th>Date</th>
													<th>Time</th>
													<th>Status</th>
													<th>Notes</th>
													<th>Actions</th>
												</tr>
												</thead>
												<tbody>';
								if($_SESSION['accountant'] == 'Yes'){
									$tdq = "SELECT * FROM `check_requests` WHERE `type` = 'Collections Payment' AND `status` != 'Completed' AND `status` != 'Cancelled'";
								}else{
									$tdq = "SELECT * FROM `check_requests` WHERE `type` = 'Collections Payment' AND `status` != 'Completed' AND `status` != 'Cancelled' AND `user_id` = '" . $_SESSION['user_id'] . "'";
								}
								$tdg = mysqli_query($conn, $tdq) or die($conn->error);
								while($tdr = mysqli_fetch_array($tdg)){
									$ornq = "SELECT * FROM `users` WHERE `ID` = '" . $tdr['rep'] . "'";
									$orng = mysqli_query($conn, $ornq) or die($conn->error);
									$ornr = mysqli_fetch_array($orng);
									
									//escape dealer name
									$dname = mysqli_real_escape_string($conn,$tdr['to']);
									
									if($tdr['type'] != 'Collections Payment'){
										echo '<tr>';
									}else{
										//echo '<tr style="background:yellow;font-weight:bold;">';
										echo '<tr>';
									}
									echo '<td>' . $tdr['user'] . '</td>
												<td>' . $tdr['type'] . '</td>
												<td>' . $tdr['to'] . '</td>
												<td>' . date("m/d/Y", strtotime($tdr['date'])) . '</td>
												<td>' . date("h:iA", strtotime($tdr['time'])) . '</td>
												<td>' . $tdr['status'] . '</td>
												<td>' . $tdr['note'] . '</td>';
                  echo '<td>';
									if($_SESSION['accountant'] == 'Yes'){
										if($tdr['type'] != 'Collections Payment'){
										echo '<a onclick="load_modal(' . $tdr['ID'] . ',\'' . $dname . '\',\'Completed\');" data-toggle="modal" data-target="#completeCheck">
												<button type="button" class="btn btn-success">Completed</button>
												</a>
                        
												<a onclick="load_modal(' . $tdr['ID'] . ',\'' . $dname . '\',\'Need Info\');" data-toggle="modal" data-target="#checkNeedInfo">
												<button type="button" class="btn btn-warning">Need-Info</button>
												</a>';
										}else{
											echo '
												<a onclick="c_payment(\'approve\',' . $tdr['pid'] . ');">
												<button type="button" class="btn btn-success">Confirmed</button>
												</a>
                        
												<a onclick="load_modal(' . $tdr['pid'] . ',\'\',\'collection payment\');" data-toggle="modal" data-target="#notReceived">
												<button type="button" class="btn btn-danger">Not Received</button>
												</a>';
										}
									}
									
								if($tdr['type'] != 'Collections Payment'){
									echo '
											<a href="http://marketforceapp.com/marketforce/accounting/printable-check-request-form.php?id=' . $tdr['ID'] . '" target="_blank">
											<button type="button" class="btn btn-primary"><i class="fa fa-eye"></i> View</button>
												</a>
                        
												<a onclick="check_request(' . $tdr['ID'] . ',3);">
												<button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
												</a>';
								}
									echo '</td></tr>';
								}
												
							echo '</tbody>
										</table>
										</div>
										</div>
									</div>
								</div>
							</div><!--End Row-->';
							