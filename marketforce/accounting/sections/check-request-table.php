<?php
							echo '<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-danger card-view">
										<div class="panel-heading">
											<h3 class="panel-title">
												Pending Check Requests &nbsp;&nbsp; <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newCheckRequest">New Payment Request</button>
											</h3>
										</div>
										<div class="panel-body" id="scroll" style="height:auto;">
											<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
												<tr>
                          <th>ORG</th>
													<th>From</th>
													<th>Document Type</th>
                          <th>Version</th>
													<th>To</th>
													<th>Date</th>
													<th>Time</th>
													<th>Status</th>
													<th>Notes</th>
													<th>Actions</th>
												</tr>
												</thead>
												<tbody>';
                
     
      $org_array = [];
      $org_name = [];
      $org_id = [];
      $org_bg = [];
      $i = 0;
     if($_SESSION['super_user'] == 'Yes'){
       $oaq = "SELECT * FROM `organizations` WHERE `super_group` = '" . $_SESSION['super_group'] . "' AND `inactive` != 'Yes'";
       $oag = mysqli_query($mf_conn, $oaq) or die($conn->error);
       while($oar = mysqli_fetch_array($oag)){
         array_push($org_array, $oar['db_name']);
         array_push($org_name, $oar['org_name']);
         array_push($org_id, $oar['org_id']);
         array_push($org_bg, $oar['org_bg']);
         $i++;
       }
     }else{
       array_push($org_array,$_SESSION['org_db_name']);
       array_push($org_name, $_SESSION['org_name']);
       array_push($org_id, $_SESSION['org_id']);
       array_push($org_bg, $_SESSION['org_bg']);
       $i++;
     }
       
                $ii = 0;
          while($ii < $i){
            
            $db_name = $org_array[$ii];
            $org = $org_name[$ii];
            $oid = $org_id[$ii];
            $bg_color = $org_bg[$ii];
            
            $aconn = mysqli_connect('localhost','marketfo_mf','#NgTFJQ!z@t8',$db_name) or die($aconn->error);
            
								if($_SESSION['accountant'] == 'Yes'){
									$tdq = "SELECT * FROM `check_requests` WHERE `type` = 'Check Request' AND `status` != 'Completed' AND `status` != 'Cancelled'";
								}else{
									$tdq = "SELECT * FROM `check_requests` WHERE `type` = 'Check Request' AND `status` != 'Completed' AND `status` != 'Cancelled' AND `user_id` = '" . $_SESSION['user_id'] . "'";
								}
								$tdg = mysqli_query($aconn, $tdq) or die($aconn->error);
								while($tdr = mysqli_fetch_array($tdg)){
									$ornq = "SELECT * FROM `users` WHERE `ID` = '" . $tdr['rep'] . "'";
									$orng = mysqli_query($aconn, $ornq) or die($aconn->error);
									$ornr = mysqli_fetch_array($orng);
									
									//escape dealer name
									$dname = mysqli_real_escape_string($aconn,$tdr['to']);
									
									if($tdr['type'] != 'Collections Payment'){
										echo '<tr style="background:' . $bg_color . ';">';
									}else{
										echo '<tr style="background:yellow;font-weight:bold;">';
									}
									echo '<td style="font-weight:bold;">' . $org . '</td>
                        <td>' . $tdr['user'] . '</td>
												<td>' . $tdr['type'] . '</td>
                        <th>' . $tdr['version'] . '</td>
												<td>' . $tdr['to'] . '</td>
												<td>' . date("m/d/Y", strtotime($tdr['date'])) . '</td>
												<td>' . date("h:iA", strtotime($tdr['time'])) . '</td>
												<td>' . $tdr['status'] . '</td>
												<td>' . $tdr['note'] . '</td>';
                  echo '<td>';
									if($_SESSION['accountant'] == 'Yes'){
										if($tdr['type'] != 'Collections Payment'){
										echo '<a onclick="load_modal(' . $tdr['ID'] . ',\'' . $dname . '\',\'Completed\',\'' . $oid . '\');" data-toggle="modal" data-target="#completeCheck">
												<button type="button" class="btn btn-success">Completed</button>
												</a>
                        
												<a onclick="load_modal(' . $tdr['ID'] . ',\'' . $dname . '\',\'Need Info\',\'' . $oid . '\');" data-toggle="modal" data-target="#checkNeedInfo">
												<button type="button" class="btn btn-warning">Need-Info</button>
												</a>';
										}else{
											echo '
												<a onclick="c_payment(\'approve\',' . $tdr['pid'] . ');">
												<button type="button" class="btn btn-success">Confirmed</button>
												</a>
												</td>
												<td>
												<a onclick="load_modal(' . $tdr['pid'] . ',\'\',\'collection payment\');" data-toggle="modal" data-target="#notReceived">
												<button type="button" class="btn btn-danger">Not Received</button>
												</a>';
										}
									}
									
								if($tdr['type'] != 'Collections Payment'){
                  if($tdr['version'] == 'Refund'){
									echo '<a href="http://marketforceapp.com/marketforce/accounting/printable-refund-check-request-form.php?id=' . $tdr['ID'] . '" target="_blank">
											    <button type="button" class="btn btn-primary"><i class="fa fa-eye"></i> View Request</button>
												</a>';
                  }else if($tdr['version'] == 'General Payment'){
                    echo '<a href="http://marketforceapp.com/marketforce/accounting/printable-general-check-request-form.php?id=' . $tdr['ID'] . '" target="_blank">
											    <button type="button" class="btn btn-primary"><i class="fa fa-eye"></i> View Request</button>
												</a>';
                  }else{
                    echo '<a href="http://marketforceapp.com/marketforce/accounting/printable-cc-refund-check-request-form.php?id=' . $tdr['ID'] . '" target="_blank">
											    <button type="button" class="btn btn-primary"><i class="fa fa-eye"></i> View Request</button>
												</a>';
                  }
                  
                  //Check for Document...
                  if($tdr['file_url'] != ''){
                    echo '<a href="' . $tdr['file_url'] . '" target="_blank">
                          <button type="button" class="btn btn-primary"><i class="fa fa-file"></i> View Document</button>
                          </a>';
                  }else{
                    echo '<a onclick="upload_crdoc(' . $tdr['ID'] . ',\'' . $oid . '\');" data-toggle="modal" data-target="#crdocUpload">
                          <button type="button" class="btn btn-primary" style="background:#999999;"><i class="fa fa-upload"></i> Upload Document</button>
                          </a>';
                  }
                    
                  echo '<a onclick="check_request(' . $tdr['ID'] . ',3,\'' . $oid . '\');">
												<button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
												</a>';
								}
                  echo '</td>
                       </tr>';
									
								}
            $ii++;
          }//END WHILE LOOP...
												
							echo '</tbody>
										</table>
										</div>
										</div>
									</div>
								</div>
							</div><!--End Row-->';