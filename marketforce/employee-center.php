<?php
include 'security/session/session-settings.php';

if (!isset($_SESSION['position'])) {
    session_destroy();
    echo 'You do not have authentication for this site...<br>';
    echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
    return;
}

include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="http://getbootstrap.com/2.3.2/assets/css/bootstrap-responsive.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css"/>
    <!--<script src="http://code.jquery.com/jquery.js"></script>-->
    <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .badge:hover {
            cursor: pointer;
        }

        .n {
            text-decoration: none;
            color: black;
        }

        ul {
            list-style-type: none;
        }

        .table-space td {
            padding: 10px;
        }
    </style>

    <?php
    //Get the users Country
    $useremail = $_SESSION['email'];
    $ucq = "SELECT * FROM `users` WHERE `email` = '" . $useremail . "'";
    $ucg = mysqli_query($conn, $ucq) or die($conn->error);
    $ucr = mysqli_fetch_array($ucg);
    $uname = $ucr['fname'] . ' ' . $ucr['lname'];
    $ucountry = $ucr['country'];
    $vaildaux = $ucr['rrhh'];
    ?>

    <script>

        function approve() {
            var id = document.getElementById('approve_id').value;
            var note = document.getElementById('approve_note').value;
            if (note === '') {
                document.getElementById('approve_error_msg').innerHTML = 'Please Enter An Approval Note!';
                return;
            }
            note = urlEncode(note);

            var snote = document.getElementById('approve_snote').value;
            if (snote === '') {
                document.getElementById('approve_error_msg').innerHTML = 'Please Enter A Note For Accounting!';
                return;
            }
            snote = urlEncode(snote);

            $("#approve").modal("hide");

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {  // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    //document.getElementById("livesearch").innerHTML=this.responseText;
                    alert(this.responseText);
                    window.location.reload();

                }
            }
            xmlhttp.open("GET", "ec/php/approve-decline.php?id=" + id + "&status=Approved&note=" + note + "&snote=" + snote, true);
            xmlhttp.send();
        }

        function approve_validate() {
            var id = document.getElementById('approve_id_v').value;
            var note = document.getElementById('approve_note_v').value;
            if (note === '') {
                document.getElementById('approve_error_msg_v').innerHTML = 'Please Enter An Approval Note!';
                return;
            }
            note = urlEncode(note);

            var snote = document.getElementById('approve_snote_v').value;
            if (snote === '') {
                document.getElementById('approve_error_msg_v').innerHTML = 'Please Enter A Note For Accounting!';
                return;
            }
            snote = urlEncode(snote);

            $("#approve").modal("hide");

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {  // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    //document.getElementById("livesearch").innerHTML=this.responseText;
                    alert(this.responseText);
                    window.location.reload();

                }
            }
            xmlhttp.open("GET", "ec/php/approve-decline.php?id=" + id + "&status=Pending&note=" + note + "&snote=" + snote, true);
            xmlhttp.send();
        }


        function decline() {
            var id = document.getElementById('decline_id').value;
            var note = document.getElementById('decline_note').value;
            if (note === '') {
                document.getElementById('decline_error_msg').innerHTML = 'Your must enter a reason for declining this request!';
                return;
            }
            note = urlEncode(note);

            $("#decline").modal("hide");

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {  // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    //document.getElementById("livesearch").innerHTML=this.responseText;
                    alert(this.responseText);
                    window.location.reload();

                }
            }
            xmlhttp.open("GET", "ec/php/approve-decline.php?id=" + id + "&status=Declined&note=" + note, true);
            xmlhttp.send();
        }


        function new_time_off() {
            var Name = document.getElementById('name').value;
            if (Name === '') {
                document.getElementById('new_error_msg').innerHTML = 'Please Enter Your Name!';
                return;
            }
            Name = urlEncode(Name);

            var Email = document.getElementById('email').value;
            if (Email === '') {
                document.getElementById('new_error_msg').innerHTML = 'Please Enter Your Email!';
                return;
            }
            Email = urlEncode(Email);

            var Sdate = document.getElementById('sdate').value;
            if (Sdate === '') {
                document.getElementById('new_error_msg').innerHTML = 'Please Enter Your Start Date!';
                return;
            }
            Sdate = urlEncode(Sdate);

            var Edate = document.getElementById('edate').value;
            if (Edate === '') {
                document.getElementById('new_error_msg').innerHTML = 'Please Enter Your Return To Work Date!';
                return;
            }
            Edate = urlEncode(Edate);

            var Reason = document.getElementById('reason').value;
            if (Reason === '') {
                document.getElementById('new_error_msg').innerHTML = 'Please Enter The Reason For Your Request!';
                return;
            }
            Reason = urlEncode(Reason);

            var emid = document.getElementById('time_off_emid').value;
            if (emid === '') {
                document.getElementById('new_error_msg').innerHTML = 'Please Select Your Supervisor!';
                return;
            }

            var type = document.getElementById('type').value;
            var position = document.getElementById('position').value;
            var pay = document.querySelector('input[name="pay"]:checked').value;
            var user_id = document.getElementById('user_id').value;

            $("#timeOff").modal("hide");

            var pURL = "ec/php/form-handler.php";
            var params = "user_id=" + user_id + "&Name=" + Name + "&Email=" + Email + "&Sdate=" + Sdate + "&Edate=" + Edate + "&Reason=" + Reason + "&emid=" + emid + "&type=" + type + "&position=" + position + "&pay=" + pay;


            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {  // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                    alert(this.responseText);
                    window.location.reload();

                }
            }
            xmlhttp.open("POST", pURL, true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(params);
        }


        function warn() {
            var eid = document.getElementById('warning_eid').value;
            if (eid === '') {
                document.getElementById('warning_error_msg').innerHTML = 'Please Select An Employee!';
                return;
            }
            var idate = document.getElementById('warning_idate').value;
            if (idate === '') {
                document.getElementById('warning_error_msg').innerHTML = 'Please Enter The Date Of The Infraction!';
                return;
            }
            var type = document.querySelector('input[name="warning_type"]:checked').value;
            if (type === '') {
                document.getElementById('warning_error_msg').innerHTML = 'Please Select The Warning Type!';
                return;
            }
            var det = document.getElementById('warning_details').value;
            if (det === '') {
                document.getElementById('warning_error_msg').innerHTML = 'Please Enter The Infraction Details!';
                return;
            }
            det = urlEncode(det);
            var action = document.getElementById('warning_action_req').value;
            if (action === '') {
                document.getElementById('warning_error_msg').innerHTML = 'Please Enter The Corrective Action Required!';
                return;
            }
            action = urlEncode(action);
            var sup_id = '<?php echo $_SESSION['user_id']; ?>';
            var sup_name = '<?php echo $_SESSION['full_name']; ?>';

            var idFiles=document.getElementById("document");
            var archivos=idFiles.files;
            var data=new FormData();

            for(var i=0;i<archivos.length;i++)
            {
                // Al objeto data, le pasamos clave,valor
                data.append("archivo"+i,archivos[i]);
                data.append("action",'upload_file');
            }
            data.append("eid",eid);
            data.append("idate",idate);
            data.append("type",type);
            data.append("det",det);
            data.append("action",action);
            data.append("sup_id",sup_id);
            data.append("sup_name",sup_name);

            $.ajax({
                type: "POST",
                url: "ec/php/warn.php",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(objeto){
                    console.log("Enviando...");
                    console.log(data);
                    $("#warn").modal("hide");
                },
                success: function (data) {
                    alert(data.msg);
                    window.location.reload();
                }
            });
        }


        function respond() {
            var id = document.getElementById('response_id').value;
            var res = document.getElementById('ca_response').value;
            if (res === '') {
                document.getElementById('response_error_msg').innerHTML = 'Please Enter Your Response!';
                return;
            }
            res = urlEncode(res);

            $("#response").modal("hide");

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {  // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    //document.getElementById("livesearch").innerHTML=this.responseText;
                    alert(this.responseText);
                    window.location.reload();

                }
            }
            xmlhttp.open("GET", "ec/php/form-response.php?id=" + id + "&response=" + res, true);
            xmlhttp.send();
        }


        function coach() {
            var rep_id = '<? echo $_SESSION['user_id']; ?>';
            var rep_name = '<? echo $_SESSION['full_name']; ?>';
            var eid = document.getElementById('ecename').value;
            var tdate = document.getElementById('ecdate').value;
            var mess = document.getElementById('ecmess').value;
            var error = document.getElementById('ec_error_msg');

            if (eid === '') {
                error.innerHTML = 'Please Select An Employee!';
                return;
            }
            eid = urlEncode(eid);
            if (tdate === '') {
                error.innerHTML = 'Please Enter The Date of Infraction!';
                return;
            }
            tdate = urlEncode(tdate);
            if (mess === '') {
                error.innerHTML = 'Please Enter A Message!';
                return;
            }
            mess = urlEncode(mess);


            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {  // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    //document.getElementById("livesearch").innerHTML=this.responseText;
                    alert(this.responseText);
                    window.location.reload();

                }
            }
            xmlhttp.open("GET", "ec/php/coach.php?eid=" + eid + "&tdate=" + tdate + "&mess=" + mess + "&rep_id=" + rep_id + "&rep_name=" + rep_name, true);
            xmlhttp.send();
        }


        function load_modal(id, name, mode) {
            if (mode === 'approve') {
                document.getElementById('approve_id').value = id;
                document.getElementById('approve_name').innerHTML = name;
            }
            if (mode === 'decline') {
                document.getElementById('decline_id').value = id;
                document.getElementById('decline_name').innerHTML = name;
            }
            if (mode === 'Response') {
                document.getElementById('response_id').value = id;
            }
            if (mode === 'validate') {
                document.getElementById('approve_id_v').value = id;
                document.getElementById('approve_name_v').innerHTML = name;
            }
        }


        function urlEncode(url) {
            url = url.replace(/&/g, '%26');
            url = url.replace(/#/g, '%23');
            url = url.replace(/</g, '%3C');
            return url;
        }

    </script>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include 'nav.php'; ?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <!-- Dashboard <small>Statistics Overview</small>-->
                        <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;"><small> Employee
                            Center</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i> Dashboard
                        </li>
                        <li class="active">
                            <i class="fa fa-file"></i> Employee Center
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->


            <!-- This is a notification -->
            <?php
            $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
            $notget = mysqli_query($conn, $notq);
            while ($notr = mysqli_fetch_array($notget)) {
                echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
            }
            ?>
            <!-- /.row -->


            <!--This is where real content for the page will go-->
            <div class="form_selection">
                <table class="table-space">
                    <tr>
                        <!--<td><a class="n" href="ec/time-off.php" target="_blank"><button type="button" class="btn btn-success" href="ec/time-off.php" target="_blank">Time Off Request Form</button></a></td>-->
                        <td>
                            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#timeOff">New
                                Time Off Request
                            </button>
                        </td>
                        <?php
                        if ($_SESSION['EC'] == 'Yes') {
                            echo '<td><button class="btn btn-warning" data-toggle="modal" data-target="#coachingForm">Employee Coaching</button></td>';
                            echo '<td><button class="btn btn-danger" data-toggle="modal" data-target="#emp_warning">Employee Warning</button></td>';
                        }
                        ?>
                    </tr>
                </table>


            </div>

            <br><br>

            <div class="row">
                <?php


                if ($_SESSION['EC'] == 'Yes') {
                    echo '<div class="col-lg-12" >
                        <div class="panel panel-primary" id="accordion">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                <i class="fa fa-clock-o fa-fw"></i> Form Status Panel 
                                &nbsp;&nbsp;';
                    if ($_GET['t'] != 'ALL') {
                        echo '<a href="employee-center.php?t=ALL"><button type="button" class="btn btn-danger btn-sm">Show All</button></a>';
                    } else {
                        echo '<a href="employee-center.php?t=current"><button type="button" class="btn btn-danger btn-sm">Show Current</button></a>';
                    }

                    echo '<span style="float:right;"><small><a href="employee-center.php"><i class="fa fa-refresh"></i> Refresh</a></small></span>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped">
                                    <thead>
                                    <tr>
										                    <th>Form ID#</th>
										                    <th>Employee</th>
										                    <th>Submit Date</th>
                                        <th>Supervisor</th>
                                        <th>Payment</th>
                                        <th>Form Type</th>
                                        <th>Reason</th>
                                        <th>Notes</th>
										                    <th>Status</th>
										                    <th></th>';
                    if ($_SESSION['accountant'] == 'Yes') {
                        echo '          <th>Days</th>';
                    }
                    echo '
										                    <th>Actions</th>
                                     </tr>
                                    </thead>
                                    <tbody>';


                    if ($_GET['t'] != 'ALL') {
                        $rq = "SELECT * FROM `requests` WHERE WEEK(`date`) = WEEK(CURRENT_DATE) AND YEAR(`date`) = YEAR(CURRENT_DATE) AND `status` != 'Approved' OR `status` != 'Declined' ORDER BY `ID` DESC";//Query to show only pending or current week...
                    } else {
                        $rq = "SELECT * FROM `requests` ORDER BY `ID` DESC";
                    }
                    $rget = mysqli_query($conn, $rq) or die($conn->error);
                    while ($rr = mysqli_fetch_array($rget)) {
                        echo '<tr>
													 					<td>' . $rr['ID'] . '</td>
													 					<td>' . $rr['user'] . '</td>
													 					<td>' . date("m/d/Y", strtotime($rr['date'])) . '</td>' .
                            //<td>' . date("h:i A", (strtotime($rr['time']) + 3600)) . '</td>
                            '<td>' . $rr['supervisor_name'] . '</td>
									<td>' . $rr['pay'] . '</td>
                                    <td>';
                        if ($rr['type'] == "Time Off Request") {
                            echo '<span style="background-color:blue;color:white;font-weight:bold;padding:3px;white-space: nowrap;">Time Off</span>';
                        }
                        if ($rr['type'] == "Warning") {
                            echo '<span style="background-color:red;font-weight:bold;padding:3px;">' . $rr['type'] . '</span>';
                        }
                        if ($rr['type'] == "Coaching") {
                            echo '<span style="background-color:yellow;font-weight:bold;padding:3px;">' . $rr['type'] . '</span>';
                        }
                        echo '</td>
                                    <td>' . $rr['reason'] . '</td>
                                    <td>' . $rr['notes'] . '</td>
                                    <td>';

                        if ($rr['status'] == 'Pending') {
                            echo '<span style="color:blue;"><b>' . $rr['status'] . '</b></span>';
                        }
                        if ($rr['status'] == 'Approved') {
                            echo '<span style="color:green;"><b>' . $rr['status'] . '</b></span>';
                        }
                        if ($rr['status'] == 'Declined') {
                            echo '<span style="color:red;"><b>' . $rr['status'] . '</b></span>';
                        }
                        if ($rr['status'] == 'Responded') {
                            echo '<span style="color:green;"><b>' . $rr['status'] . '</b></span>';
                        }
                        if ($rr['status'] == 'Validating') {
                            echo '<span style="color:saddlebrown;"><b>' . $rr['status'] . '</b></span>';
                        }

                        echo '</td>
																		<td><a style="color:black;" href="ec/php/view-form.php?id=' . $rr['ID'] . '" target="_blank"><i class="fa fa-eye"></i> View</a></td>';
                        //if Accountant
                        if ($_SESSION['accountant'] == 'Yes') {
                            echo '<td>' . $rr['num_days'] . '</td>';
                        }

                        // For users in Mexico
                        if ($ucr['country'] == 'Mexico') {
                            //final approve only available to rrhh
                            if ($rr['status'] == 'Pending' && $rr['type'] == 'Time Off Request' && $ucr['rrhh'] == 'Yes') {
                                echo '<td><a onclick="load_modal(' . $rr['ID'] . ',\'' . $rr['user'] . '\',\'approve\');" data-toggle="modal" data-target="#approve"><button type="button" class="btn btn-success btn-xs">Approve</button></a>    
																		&nbsp;<a onclick="load_modal(' . $rr['ID'] . ',\'' . $rr['user'] . '\',\'decline\');" data-toggle="modal" data-target="#decline"><button type="button" class="btn btn-danger btn-xs">Decline</button></a></td>';
                            }
                            // Validation for managers
                            if ($rr['status'] == 'Validating' && $rr['type'] == 'Time Off Request') {
                                echo '<td><a onclick="load_modal(' . $rr['ID'] . ',\'' . $rr['user'] . '\',\'validate\');" data-toggle="modal" data-target="#validate"><button type="button" class="btn btn-info btn-xs">Validate</button></a>    
																		&nbsp;<a onclick="load_modal(' . $rr['ID'] . ',\'' . $rr['user'] . '\',\'decline\');" data-toggle="modal" data-target="#decline"><button type="button" class="btn btn-danger btn-xs">Decline</button></a></td>';
                            }
                        }

                        //For everyone else
                        if ($ucr['country'] != 'Mexico') {
                            if ($rr['status'] == 'Pending' && $rr['type'] == 'Time Off Request') {
                                echo '<td><a onclick="load_modal(' . $rr['ID'] . ',\'' . $rr['user'] . '\',\'approve\');" data-toggle="modal" data-target="#approve"><button type="button" class="btn btn-success btn-xs">Approve</button></a>    
																		&nbsp;<a onclick="load_modal(' . $rr['ID'] . ',\'' . $rr['user'] . '\',\'decline\');" data-toggle="modal" data-target="#decline"><button type="button" class="btn btn-danger btn-xs">Decline</button></a></td>';
                            }
                            if ($rr['status'] == 'Validating' && $rr['type'] == 'Time Off Request') {
                                echo '<td><a onclick="load_modal(' . $rr['ID'] . ',\'' . $rr['user'] . '\',\'validate\');" data-toggle="modal" data-target="#validate"><button type="button" class="btn btn-info btn-xs">Validate</button></a>    
																		&nbsp;<a onclick="load_modal(' . $rr['ID'] . ',\'' . $rr['user'] . '\',\'decline\');" data-toggle="modal" data-target="#decline"><button type="button" class="btn btn-danger btn-xs">Decline</button></a></td>';
                            }
                        }
                        echo '<td>';
                        if ($rr['type'] == 'Warning' && $rr['status'] == 'Pending') {
                            echo '<button type="button" class="btn btn-success" onclick="load_modal(' . $rr['ID'] . ',\'\',\'Response\');" data-toggle="modal" data-target="#response">Enter Response</button>';
                        }
                        if ($rr['type'] == 'Coaching' && $rr['status'] == 'Pending') {
                            echo '<button type="button" class="btn btn-success" onclick="load_modal(' . $rr['ID'] . ',\'\',\'Response\');" data-toggle="modal" data-target="#response">Enter Response</button>';
                        }
                        if ($rr['type'] == 'Warning'){
                            $sel = "SELECT * FROM `doc_ec` WHERE `id_ec` = '" . $rr['ID'] . "'";
                            $get = mysqli_query($conn, $sel) or die($conn->error);
                            if(mysqli_num_rows($get) > 0){
                                $sr = mysqli_fetch_array($get);
                                echo '<a class="btn btn-info" href="'. $sr['rute'] . '" target="_blank">Document</a>';
                            }

                        }
                        /*if($rr['status'] == 'Pending'){
                    echo '<td>
                                <a href="Javascript: clr_res(' . $rr['ID'] . ',&apos;p&apos;);">
                                <span class="badge">Pulled</span></a>&nbsp;&nbsp;
                                <a href="Javascript: cancel_res(' . $rr['ID'] . ');">
                                <span class="badge">Cancel</span></a></td>';
                        }
                        if($rr['status'] == 'Pulled'){
                    echo '<td><a href="Javascript: clr_res(' . $rr['ID'] . ',&apos;f&apos;);">
                                <span class="badge">Pick-Up</span></a>&nbsp;&nbsp;
                                <a href="Javascript: cancel_res(' . $rr['ID'] . ');">
                                <span class="badge">Cancel</span></a></td>';
                        }*/
                        echo '</td></tr>';
                    }


                    echo '</tbody>
                                </table>
                              </div>
                            </div>
                        	</div>
                    		</div>
                    	</div>
                <!-- /.row -->';
                } else {
                    echo '<div class="col-lg-12" >
                        <div class="panel panel-primary" id="accordion">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Form Status Panel <span style="float:right;"><small><a href="employee-center.php"><i class="fa fa-refresh"></i> Refresh</a></small></span></h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped">
                                    <thead>
                                    <tr>
																		  <th>Form ID#</th>
																			<th>Submit Date</th>
                                      <th>Submit Time</th>
                                      <th>Form Type</th>
                                      <th>Reason</th>
                                      <th>Notes</th>
																			<th>Status</th>
                                     </tr>
                                    </thead>
                                    <tbody>';


                    $rq = "SELECT * FROM `requests` WHERE `user_id` = '" . $_SESSION['user_id'] . "' ORDER BY `ID` DESC";
                    $rget = mysqli_query($conn, $rq) or die($conn->error);
                    while ($rr = mysqli_fetch_array($rget)) {
                        echo '<tr>
													 					<td>' . $rr['ID'] . '</td>
													 					<td>' . date("m/d/Y", strtotime($rr['date'])) . '</td>
                                    <td>' . date("h:i A", (strtotime($rr['time']) + 3600)) . '</td>
                                    <td>' . $rr['type'] . '</td>
                                    <td>' . $rr['reason'] . '</td>
                                    <td>' . $rr['notes'] . '</td>
                                    <td>';

                        if ($rr['status'] == 'Pending') {
                            echo '<span style="color:blue;"><b>' . $rr['status'] . '</b></span>';
                        }
                        if ($rr['status'] == 'Approved') {
                            echo '<span style="color:green;"><b>' . $rr['status'] . '</b></span>';
                        }
                        if ($rr['status'] == 'Declined') {
                            echo '<span style="color:red;"><b>' . $rr['status'] . '</b></span>';
                        }
                        if ($rr['status'] == 'Validating') {
                            echo '<span style="color:cyan;"><b>' . $rr['status'] . '</b></span>';
                        }

                        echo '</td>
																		<td><a style="color:black;" href="ec/php/view-form.php?id=' . $rr['ID'] . '" target="_blank"><i class="fa fa-eye"></i> View</a></td>';
                        /*if($rr['status'] == 'Pending'){
echo '<td>
                                <a href="Javascript: clr_res(' . $rr['ID'] . ',&apos;p&apos;);">
                                <span class="badge">Pulled</span></a>&nbsp;&nbsp;
                                <a href="Javascript: cancel_res(' . $rr['ID'] . ');">
                                <span class="badge">Cancel</span></a></td>';
                        }
                        if($rr['status'] == 'Pulled'){
                    echo '<td><a href="Javascript: clr_res(' . $rr['ID'] . ',&apos;f&apos;);">
                                <span class="badge">Pick-Up</span></a>&nbsp;&nbsp;
                                <a href="Javascript: cancel_res(' . $rr['ID'] . ');">
                                <span class="badge">Cancel</span></a></td>';
                        }*/
                        if ($rr['type'] == 'Warning' && $rr['status'] == 'Pending') {
                            echo '<td><button type="button" class="btn btn-success" onclick="load_modal(' . $rr['ID'] . ',\'\',\'Response\');" data-toggle="modal" data-target="#response">Enter Response</button></td>';
                        }
                        echo '</tr>';
                    }


                    echo '</tbody>
                                </table>
                              </div>
                            </div>
                        	</div>
                    		</div>
                    	</div>
                <!-- /.row -->';
                }
                ?>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


    <!-- Modal -->
    <div class="modal fade" id="approve" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Approve Time Off Request For <span id="approve_name"
                                                                               style="color:red;font-weight:bold;"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>Approval Notes:</p>
                    <textarea id="approve_note" class="form-control"></textarea>
                    <br><br>
                    <p>Notes For Accounting:</p>
                    <textarea id="approve_snote" class="form-control"></textarea>
                    <br><br>
                    <input type="hidden" id="approve_id" value=""/>
                    <p id="approve_error_msg" style="color:red;font-weight:bold;"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="approve();">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- End Modal -->

    <!-- Modal -->
    <div class="modal fade" id="validate" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Validate Vacation Days Request For <span id="approve_name_v"
                                                                                     style="color:red;font-weight:bold;"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>Approval Notes:</p>
                    <textarea id="approve_note_v" class="form-control"></textarea>
                    <br><br>
                    <p>Notes For Supervisor:</p>
                    <textarea id="approve_snote_v" class="form-control"></textarea>
                    <br><br>
                    <input type="hidden" id="approve_id_v" value=""/>
                    <p id="approve_error_msg_v" style="color:red;font-weight:bold;"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="approve_validate();">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- End Modal -->


    <!-- Modal -->
    <div class="modal fade" id="decline" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Decline Time Off Request For <span id="decline_name"
                                                                               style="color:red;font-weight:bold;"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <p>Reason Declined:</p>
                    <textarea id="decline_note" class="form-control"></textarea>
                    <br><br>
                    <input type="hidden" id="decline_id" value=""/>
                    <p id="decline_error_msg" style="color:red;font-weight:bold;"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="decline();">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- End Modal -->


    <!-- Modal -->
    <div class="modal fade" id="timeOff" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Time Off Request Form</h4>
                </div>
                <div class="modal-body">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <p>Name:</p>
                                <input type="text" id="name" class="form-control"
                                       value="<?php echo $_SESSION['full_name']; ?>" disabled/>
                                <input type="hidden" id="user_id" value="<?php echo $_SESSION['user_id']; ?>"/>
                            </td>
                            <td>
                                <p>Email:</p>
                                <input type="email" id="email" class="form-control"
                                       value="<?php echo $_SESSION['email']; ?>" disabled/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>1st Date Off</p>
                                <input type="text" id="sdate" class="form-control date" placeholder="mm/dd/yyyy"
                                       autocomplete="off"/>
                            </td>
                            <td>
                                <p>Return To Work:</p>
                                <input type="text" id="edate" class="form-control date" placeholder="mm/dd/yyyy"
                                       autocomplete="off"/>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <b>I am requesting to use:</b>
                    <input type="radio" name="pay" value="Vacation Days" required=" "/> Vacation Days
                    &nbsp;&nbsp;
                    <input type="radio" name="pay" value="Sick Days"/> Sick Days
                    &nbsp;&nbsp;
                    <input type="radio" name="pay" value="Non Paid Days"/> Non Paid Days
                    <br><br>
                    <p>Reason For Time Off:</p>
                    <textarea id="reason" class="form-control"></textarea>
                    <br>
                    <input type="hidden" id="type" value="Time Off Request"/>
                    <input type="hidden" id="position" value="<?php echo $_SESSION['position']; ?>"/>
                    <p>Supervisor Name:</p>
                    <select id="time_off_emid" class="form-control">
                        <option value="">Select Employee</option>
                        <?php
                        $empmanq = "SELECT * FROM `users` WHERE `inactive` != 'Yes' AND `ec` = 'Yes'";
                        $empmang = mysqli_query($conn, $empmanq) or die($conn->error);
                        while ($empmanr = mysqli_fetch_array($empmang)) {
                            echo '<option value="' . $empmanr['ID'] . '">' . $empmanr['fname'] . ' ' . $empmanr['lname'] . '</option>';
                        }
                        ?>
                    </select>
                    <br>
                    <p id="new_error_msg" style="color:red;font-weight:bold;"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="new_time_off();">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- End Modal -->


    <!-- Modal -->
    <div class="modal fade" id="emp_warning" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Employee Warning Form</h4>
                </div>
                <div class="modal-body">
                    <table style="width:100%;" class="table-space">
                        <tr>
                            <td>
                                <p>Employee Name:</p>
                                <select id="warning_eid" class="form-control">
                                    <option value="">Select Employee</option>
                                    <?php
                                    $empq = "SELECT * FROM `users` WHERE `inactive` != 'Yes'";
                                    $empg = mysqli_query($conn, $empq) or die($conn->error);
                                    while ($empr = mysqli_fetch_array($empg)) {
                                        echo '<option value="' . $empr['ID'] . '">' . $empr['fname'] . ' ' . $empr['lname'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                            <td>
                                <p>Date of Infraction:</p>
                                <input type="text" class="form-control date" id="warning_idate"
                                       placeholder="mm/dd/yyyy">
                            </td>
                        </tr>
                    </table>
                    <br>
                    <p>Warning Type:</p>
                    <input type="radio" name="warning_type" value="Attendance"/> Attendance
                    &nbsp;&nbsp;
                    <input type="radio" name="warning_type" value="Dress Violation"/> Dress Violation
                    &nbsp;&nbsp;
                    <input type="radio" name="warning_type" value="Work Performance"/> Work Performance
                    &nbsp;&nbsp;
                    <input type="radio" name="warning_type" value="Employee Relations"/> Employee Relations
                    &nbsp;&nbsp;<br>
                    <input type="radio" name="warning_type" value="Other"/> Other
                    &nbsp;&nbsp;
                    <br><br>
                    <p>Warning Details:</p>
                    <textarea id="warning_details" class="form-control"></textarea>

                    <p>Corrective Action Required:</p>
                    <textarea id="warning_action_req" class="form-control"></textarea>

                    <p>Documents</p>
                    <input type="file" name="document" id="document">

                    <p id="warning_error_msg" style="color:red;font-weight:bold;"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="warn();">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- End Modal -->


    <!-- Modal -->
    <div class="modal fade" id="response" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Corrective Action Reponse</h4>
                </div>
                <div class="modal-body">
                    <p>Your Reponse:</p>
                    <textarea id="ca_response" class="form-control"></textarea>
                    <br><br>
                    <input type="hidden" id="response_id" value=""/>
                    <p id="response_error_msg" style="color:red;font-weight:bold;"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="respond();">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- End Modal -->


    <!--Employee Coaching Modal-->
    <? include 'ec/modals/coaching-modal.php'; ?>


    <!-- jQuery -->
    <script src="js/jquery.js"></script>


    <!-- Morris Charts JavaScript -->
    <!--
		<script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
		-->

    <?php include 'footer.html'; ?>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>