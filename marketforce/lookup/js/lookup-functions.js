function set_rep(){
  var nrid = document.getElementById('rep_choice').value;
  if(nrid === ''){
    alert('Please Select A Rep To Assign');
    return;
  }
  var did = document.getElementById('did').value;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      var q = JSON.parse(this.responseText);
      if(q.status === '200'){
        document.getElementById('repLabel'+did).innerHTML = nrid;
        document.getElementById('rep_choice').value = '';
        alert('The OSTR has been reassigned successfully!');
      }else{
        alert(q.error);
      }
      
    }
  }
  xmlhttp.open("GET","lookup/php/reassign-rep.php?did="+did+"&nrid="+nrid,true);
  xmlhttp.send();
}


function assign_rep(id){
  var did = document.getElementById('did').value = id;
}