<div class="modal fade" role="dialog" tabindex="-1" id="repAssign">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="text-center modal-title">Reassign OSTR</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <select class="form-control" id="rep_choice" name="rep_choice">
          <option value="">Select An OSTR</option>
          <?php
            $mrnq = "SELECT * FROM `users` WHERE `position` = 'Outside Sales Rep' AND `inactive` != 'Yes'";
            $mrng = mysqli_query($conn, $mrnq) or die($conn->error);
            while($mrnr = mysqli_fetch_array($mrng)){
              echo '<option value="' . $mrnr['fname'] . ' ' . $mrnr['lname'] . '">' . $mrnr['fname'] . ' ' . $mrnr['lname'] . '</option>';
            }
          ?>
        </select>
      </div>
      <input type="hidden" id="did" name="did" />
      <div class="modal-footer">
        <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="button" onclick="set_rep();">Assign</button>
      </div>
    </div>
  </div>
</div>