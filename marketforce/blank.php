<?php
include 'security/session/session-settings.php';
include 'php/connection.php';
$pageName = 'blank';
$pageIcon = 'fas fa-file';

echo '<script>
        var rep_id = "' . $_SESSION['user_id'] . '";
        var rep_name = "' . $_SESSION['full_name'] . '";
      </script>';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>MarketForce | All Steel</title>
  <?php include 'global/sections/head.php'; ?>
</head>

<body>
  <!-- Preloader -->
  <?php include 'global/sections/preloader.php'; ?>
  <!-- /Preloader -->
  <div class="wrapper theme-4-active pimary-color-red">

    <!--Navigation-->
    <?php include 'global/sections/nav.php'; ?>


    <!-- Main Content -->
    <div class="page-wrapper">
      <!--Includes Footer-->

      <div class="container-fluid pt-25">
        <?php include 'global/sections/page-title-bar.php'; ?>

        <!--Main Content Here-->

        <!-- Footer -->
        <?php include 'global/sections/footer.php'; ?>
        <!-- /Footer -->

      </div>
      <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->

    <!--Footer-->
    <?php include 'global/sections/includes.php'; ?>
</body>
<!-- JS Files -->
  
<!--Modals-->

</html>