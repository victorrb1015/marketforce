<?php
include 'security/session/session-settings.php';

if(!isset($_SESSION['position'])){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}



include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
    <!--JQuery CSS-->
    <link href="jquery/jquery-ui.css" rel="stylesheet">


	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
    
		/* This only works with JavaScript, 
if it's not present, don't show loader */
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	//background: url(img/loader-128x/Preloader_7.gif) center no-repeat #fff;
  background: url(img/loaders/loading-chem.gif) center no-repeat #fff;
}
  </style>
  <script>
    
  function clr_res(id,s){
    //alert("test");
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			//alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://library.burtonsolution.tech/php/reserve.php?id="+id+"&s="+s,true);
  xmlhttp.send();
  }
		
		
	
		
		
  </script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script>
//paste this code under the head tag or in a separate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
	</script>
</head>

<body>
	<!--PRELOADER GIF-->
<!--<div class="se-pre-con"></div>-->
  
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="img/logo-allsteel.png" ></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['full_name']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="../index.php?logout=1"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard
											<?php
													if($_SESSION['admin'] == 'Yes'){
														$ndq = "SELECT * FROM `new_packets` WHERE `status` = 'Pending'";
													}else{
														$ndq = "SELECT * FROM `new_packets` WHERE `status` = 'Pending' AND `rep` = '" . $_SESSION['user_id'] . "'";
													}
													$ndg = mysqli_query($conn, $ndq);
													$ndn = mysqli_num_rows($ndg);
													if($ndn != 0){
													echo '&nbsp; <span style="background:red;color:white;padding:5px;border-radius:20px;">' . $ndn . '</span>';
													}
													?>
												</a>
                    </li>
										<li>
											<a href="reports.php"><i class="fa fa-fw fa-file"></i> Reports</a>
										</li>
										<li>
											<a href="employee-center.php">
												<i class="fa fa-fw fa-file"></i> Employee Center 
												<?php
												if($_SESSION['admin'] == 'Yes'){
												$ecq = "SELECT * FROM `requests` WHERE `status` = 'Pending'";
												}else{
													$ecq = "SELECT * FROM `requests` WHERE `status` = 'Pending' AND `user_id` = '" . $_SESSION['user_id'] . "'";
												}
												$ecg = mysqli_query($conn, $ecq);
												$ecn = mysqli_num_rows($ecg);
												if($ecn == 0){
													$ecn = '';
												}else{
													echo '&nbsp;<span style="background:red;color:white;padding:5px;border-radius:20px;">' . $ecn . '</span>';
												}
												?>
												
											</a>
										</li>
                    <li>
                        <a href="notes.php"><i class="fa fa-fw fa-file"></i> Add Visit Notes</a>
                    </li>
										<li>
												<a href="view-notes.php"><i class="fa fa-fw fa-file"></i> View Notes</a>
										</li>
										<li>
                      <a href="dealer-lookup.php"><i class="fa fa-fw fa-file"></i> Dealer Lookup</a>
                    </li>
										<li>
												<a href="new-dealer.php"><i class="fa fa-fw fa-file"></i> New Dealer Application</a>
										</li>
                    <li>
												<a href="print-forms.php"><i class="fa fa-fw fa-file"></i> Forms</a>
										</li>
									<?php
										if($_SESSION['admin'] == 'Yes'){
									echo	'<li>
												<a href="send-survey.php"><i class="fa fa-fw fa-file"></i> Send A Survey</a>
										</li>
										<li>
                        <a href="dealer-update.php"><i class=" fa fa-fw fa-file"></i> Send Dealer Update</a>
                    </li>
										<li>
                        <a href="osr-update.php"><i class="fa fa-fw fa-file"></i> Send OSR Update</a>
                    </li>
										<li>
                        <a href="preowned-listings.php"><i class="fa fa-fw fa-file"></i> Pre-Owned Listings</a>
                    </li>
										<li>
												<a href="notification.php"><i class="fa fa-fw fa-file"></i> Create Notification</a>
										</li>';
										}
									?>
									<li class="active">
												<a href="map2.php"><i class="fa fa-globe"></i> Dealer Map v2.0</a>
									</li>
									<li>
												<a href="https://burtonsolution.on.spiceworks.com/portal/tickets" target="_blank"><i class="fa fa-bug"></i> Report An Issue</a>
									</li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

              

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							<!--<div class="legend" style="text-align: center; margin: auto;">
								<table style="margin: auto;">
									<tr>
										<td style="padding-right: 10px;">
											<img src="img/map/check.png" style="height: 20px; width: 20px;" /> = New Dealer
										</td>
										<td style="padding-right: 10px;">
											<img src="img/map/pin.png" style="height: 30px; width: 20px;" /> = Dealer is trained and has displays
										</td>
										<td>
											<img src="img/map/star.png" style="height: 20px; width: 20px;" /> = Dealer has been visited this year
										</td>
									</tr>
								</table>
							</div>-->
							<!--<div id="progress_bar_div" style="width:80%;">
								<div class="container">
  								<h2>Loading Dealer Icons:</h2>
  								<div class="progress">
  								  <div id="progress_bar" class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:0%">
  								    <span id="progress_bar_label" class="sr-only">70% Complete</span>
  								  </div>
  								</div>
								</div>
							</div>-->
							<!--THIS IS THE OLD UNOPTIMIZED MAP (CHECK CHANGE LOG INSIDE OPTIMIZED MAP FOR CHANGES)
							<iframe src="http://marketforceapp.com/marketforce/map/mapv2.php?fn=<?php echo $_SESSION['full_name']; ?>" style="width: 100%; height: 600px;"></iframe>
							-->
							<iframe src="http://marketforceapp.com/marketforce/map/optimized-map.php?fn=<?php echo $_SESSION['full_name']; ?>" style="width: 100%; height: 600px;"></iframe>
							
							<br>
							<div style="text-align:center;">
								<a href="http://marketforceapp.com/marketforce/map/mapv2.php" target="_blank">
									<button type="button" style="color:white;background:red;font-weight:bold;text-align:center;">
										Open Full Screen
									</button>
								</a>
							</div>
							
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
   <!-- <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>-->

 <?php include 'footer.html'; ?>
</body>

</html>