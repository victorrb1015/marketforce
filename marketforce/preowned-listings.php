<?php
include 'security/session/session-settings.php';

if($_SESSION['preowned'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
  </style>
  <script>
    
  function clr_res(id,s){
    //alert("test");
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			//alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://library.burtonsolution.tech/php/reserve.php?id="+id+"&s="+s,true);
  xmlhttp.send();
  }
    
  
  function edit_inv(x){
    //alert('Worked!');
    var inum = document.getElementById('inv').value;
    
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("inv_body").innerHTML=this.responseText;
			//alert(this.responseText);
      //window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/php/listing-editor.php?q="+x+"&i="+inum,true);
  xmlhttp.send();
  }
   
  function res_edit(){
    window.location.reload();
  }
    
  
  function check(s){
		if(s === 'new'){
		document.getElementById('adder_btn').disabled = true;
		}
    var i = document.getElementById('invoice_num').value;
    var cat = document.querySelector('input[name="cat"]:checked').value;
    var roof_style = document.querySelector('input[name="roof_style"]:checked').value;
    var width = document.getElementById('width').value;
    var roof_length = document.getElementById('roof_length').value;
    var frame_length = document.getElementById('frame_length').value;
    var leg_height = document.getElementById('leg_height').value;
    var gauge = document.getElementById('gauge').value;
    var roof_color = document.getElementById('roof_color').value;
    var price = document.getElementById('price').value;
    var ex1 = document.getElementById('ex1').value;
    var ex2 = document.getElementById('ex2').value;
    var ex3 = document.getElementById('ex3').value;
    var ex4 = document.getElementById('ex4').value;
    var ex5 = document.getElementById('ex5').value;
    var state = document.querySelector('input[name="state"]:checked').value;
    var city = document.getElementById('city').value;
    var sold = document.querySelector('input[name="sold"]:checked').value;
    var id = document.getElementById('id').value;
   
    
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("inv_body").innerHTML=this.responseText;
			//alert(this.responseText);
      //window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/php/listing-editor.php?s="+s+
                      "&i="+i+
                      "&cat="+cat+
                      "&roof_style="+roof_style+
                      "&width="+width+
                      "&roof_length="+roof_length+
                      "&frame_length="+frame_length+
                      "&leg_height="+leg_height+
                      "&gauge="+gauge+
                      "&roof_color="+roof_color+
                      "&price="+price+
                      "&ex1="+ex1+
                      "&ex2="+ex2+
                      "&ex3="+ex3+
                      "&ex4="+ex4+
                      "&ex5="+ex5+
                      "&state="+state+
                      "&city="+city+
                      "&sold="+sold+
                      "&id="+id,true);
  xmlhttp.send();
  
  }
		
function send_ad(v,s){
	var i = document.getElementById('invnum').value;
	
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("inv_body").innerHTML=this.responseText;
			//alert(this.responseText);
      //window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/forms/new-listing-ad.php?listing="+i+
							 				"&view="+v+
							 				"&send="+s,true);
  xmlhttp.send();
} 
  </script>
  <style>
    table td{
      padding: 5px;
    }
  </style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> Pre-Owned Listings</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Pre-Owned Listings
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							<div class="row">
                <div class="col-lg-12">
                  <div class="panel panel-info">
                    <div class="panel panel-heading">
                      <h3 class="panel-title">
                        Create / Edit A Pre-Owned Listing
                      </h3>
                    </div>
                    <div class="panel-body" id="inv_body">
                      <div style="text-align: center;">
                        <strong>Please Enter An Invoice#:</strong><br>
                        <input type="text" id="inv" placeholder="Invoice #..." autofocus/><br><br>
                        <button type="button" onclick="edit_inv('edit');">Edit Invoice</button>
                        <button type="button" onclick="edit_inv('new');">Add Invoice</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
							
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

 <?php include 'footer.html'; ?>
</body>

</html>