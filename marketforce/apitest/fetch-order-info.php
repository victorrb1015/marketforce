<?php
include '../php/connection.php';

//Load Variables
$id = $_GET['id'];


//Get info...
$q = "SELECT * FROM `salesPortalLinked` WHERE `orderID` = '" . $id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
if(mysqli_num_rows($g) > 0){
  $x->response = "OK";
  $x->orderID = mysqli_real_escape_string($conn, $r['orderID']);
  $x->orderStatus = mysqli_real_escape_string($conn, $r['orderStatus']);
  $x->dealer = mysqli_real_escape_string($conn, $r['dealer']);
  $x->customer = mysqli_real_escape_string($conn, $r['customer']);
  $x->customerAddress = mysqli_real_escape_string($conn, $r['customerAddress']);
  $x->customerCity = mysqli_real_escape_string($conn, $r['customerCity']);
  $x->customerState = mysqli_real_escape_string($conn, $r['customerState']);
  $x->customerZipCode = mysqli_real_escape_string($conn, $r['customerZipCode']);
  $x->customerPhone1 = mysqli_real_escape_string($conn, $r['customerPhone1']);
  $x->customerPhone2 = mysqli_real_escape_string($conn, $r['customerPhone2']);
  $x->customerPhone3 = mysqli_real_escape_string($conn, $r['customerPhone3']);
  $x->buildingAddress = mysqli_real_escape_string($conn, $r['buildingAddress']);
  $x->buildingCity = mysqli_real_escape_string($conn, $r['buildingCity']);
  $x->buildingState = mysqli_real_escape_string($conn, $r['buildingState']);
  $x->buildingZipCode = mysqli_real_escape_string($conn, $r['buildingZipCode']);
  $x->options1 = mysqli_real_escape_string($conn, $r['options1']);
  $x->options2 = mysqli_real_escape_string($conn, $r['options2']);
  $x->options3 = mysqli_real_escape_string($conn, $r['options3']);
  $x->options4 = mysqli_real_escape_string($conn, $r['options4']);
  $x->options5 = mysqli_real_escape_string($conn, $r['options5']);
  $x->options6 = mysqli_real_escape_string($conn, $r['options6']);
  $x->options7 = mysqli_real_escape_string($conn, $r['options7']);
  $x->options8 = mysqli_real_escape_string($conn, $r['options8']);
  $x->options9 = mysqli_real_escape_string($conn, $r['options9']);
  $x->price = mysqli_real_escape_string($conn, $r['price']);
  $x->price1 = mysqli_real_escape_string($conn, $r['price1']);
  $x->price2 = mysqli_real_escape_string($conn, $r['price1']);
  $x->price3 = mysqli_real_escape_string($conn, $r['price1']);
  $x->price4 = mysqli_real_escape_string($conn, $r['price1']);
  $x->price5 = mysqli_real_escape_string($conn, $r['price1']);
  $x->price6 = mysqli_real_escape_string($conn, $r['price1']);
  $x->price7 = mysqli_real_escape_string($conn, $r['price1']);
  $x->price8 = mysqli_real_escape_string($conn, $r['price1']);
  $x->price9 = mysqli_real_escape_string($conn, $r['price1']);
  $x->totalSale = mysqli_real_escape_string($conn, $r['totalSale']);
  $x->tax = mysqli_real_escape_string($conn, $r['tax']);
  $x->total = mysqli_real_escape_string($conn, $r['total']);
  $x->tenPercentDep = mysqli_real_escape_string($conn, $r['tenPercentDep']);
  $x->fiftyPercentDip = mysqli_real_escape_string($conn, $r['fiftyPercentDip']);
  $x->balance = mysqli_real_escape_string($conn, $r['balance']);
  $x->taxExemptNum = mysqli_real_escape_string($conn, $r['taxExemptNum']);
  $x->instructions = mysqli_real_escape_string($conn, $r['instructions']);
  $x->description = mysqli_real_escape_string($conn, $r['description']);
  $x->feesDescription = mysqli_real_escape_string($conn, $r['feesDescription']);
  $x->fees = mysqli_real_escape_string($conn, $r['fees']);
  $x->colorDescription = mysqli_real_escape_string($conn, $r['colorDescription']);
  $x->sidesDescription = mysqli_real_escape_string($conn, $r['description']);
  $x->sidesPrice = mysqli_real_escape_string($conn, $r['sidesPrice']);
  $x->trimDescription = mysqli_real_escape_string($conn, $r['trimDescription']);
  $x->trimPrice = mysqli_real_escape_string($conn, $r['trimPrice']);
  $x->width = mysqli_real_escape_string($conn, $r['width']);
  $x->roof = mysqli_real_escape_string($conn, $r['roof']);
  $x->frame = mysqli_real_escape_string($conn, $r['frame']);
  $x->leg = mysqli_real_escape_string($conn, $r['leg']);
  $x->gauge = mysqli_real_escape_string($conn, $r['gauge']);
  $x->ground = mysqli_real_escape_string($conn, $r['ground']);
  $x->cement = mysqli_real_escape_string($conn, $r['cement']);
  $x->asphalt = mysqli_real_escape_string($conn, $r['asphalt']);
  $x->other = mysqli_real_escape_string($conn, $r['other']);
  $x->txtOther = mysqli_real_escape_string($conn, $r['txtOther']);
  $x->landlevelNo = mysqli_real_escape_string($conn, $r['landlevelNo']);
  $x->landlevelYes = mysqli_real_escape_string($conn, $r['landlevelYes']);
  $x->electricityYes = mysqli_real_escape_string($conn, $r['electricityYes']);
  $x->electricityNo = mysqli_real_escape_string($conn, $r['electricityNo']);
  $x->cash = mysqli_real_escape_string($conn, $r['cash']);
  $x->cCheck = mysqli_real_escape_string($conn, $r['cCheck']);
  $x->cc = mysqli_real_escape_string($conn, $r['cc']);
  $x->po = mysqli_real_escape_string($conn, $r['po']);
  $x->endsDescription = mysqli_real_escape_string($conn, $r['endsDescription']);
  $x->endsPrice = mysqli_real_escape_string($conn, $r['endsPrice']);
  $x->regFrame = mysqli_real_escape_string($conn, $r['regFrame']);
  $x->aFrame = mysqli_real_escape_string($conn, $r['aFrame']);
  $x->vRoof = mysqli_real_escape_string($conn, $r['vRoof']);
  $x->dealerPhone = mysqli_real_escape_string($conn, $r['dealerPhone']);
  $x->allVertical = mysqli_real_escape_string($conn, $r['allVertical']);
  $x->customerEmail = mysqli_real_escape_string($conn, $r['customerEmail']);
}else{
  $x->response = "No Data";
}
  

$response = json_encode($x);
echo $response;
  
?>