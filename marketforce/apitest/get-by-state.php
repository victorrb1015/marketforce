<?php
include '../php/connection.php';

/*This is a testing script for the new CSKern API Webservice*/

  $sc = new SoapClient("http://sales.allsteelcarports.com/publicapi/allsteelwebservice.asmx?WSDL");
  $apiKey = "33f2ddee-724e-4bd1-9606-4408f5f95bdd";
  $invoice = $_GET['invoice'];
  //$status = $_GET['status'];
  //$status = 'In Shop';
  $state = $_GET['state'];

$tnum = 0;
$ototal = 0;
$q = "SELECT DISTINCT `state` FROM `dealers` WHERE `inactive` != 'Yes' AND `state` != ''";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
$fstate = $r['state'];
$sq = "SELECT * FROM `states` WHERE `state` = '" . $fstate . "'";
$sg = mysqli_query($conn, $sq) or die($conn->error);
$sr = mysqli_fetch_array($sg);
$state = $sr['abrev'];
  //SoapClient Parameters for the CSKern Web Service API
    $params = array(
      "apiKey" => $apiKey,
      //"invoice" => $invoice,
      //"status" => $status,
      "state" => $state
    );
  
  
    $result = $sc->__soapCall("GetOrdersByState", array($params));
    //$r = json_decode($result->GetOrdersByInvoiceResult);
    $rx = json_decode($result->GetOrdersByStateResult);
    
//var_dump($r);
//var_dump($result);
  
echo '<h1><u>' . $r['state'] . ':</u></h1>';
  
$total = 0;
  foreach($rx as $x){
    //if($x->buildingState == 'OK'){
    echo $x->orderID . ' => ' . $x->customer . ' <b>($' . $x->totalSale . ')</b><br>';
      $total += $x->totalSale;
      $ototal += $x->totalSale;
    $tnum++;
  }

echo '<br><b>Total State Sales: $' . number_format($total,2) . '</b><br>';
  
}
echo '<b>Total Overall Sales: $' . number_format($ototal,2) . '</b><br>';
echo '<b>Total Number Of Orders: ' . $tnum . '</b>';
?>