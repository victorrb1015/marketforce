<?php
header('Content-Type: application/json');
error_reporting(E_ALL);
include '../php/connection.php';

//Load Variables...
//echo $_SESSION['org_sales_portal_api_url'];


#Main Functions...
//CSKern SOAP CALL...
$sc = new SoapClient('https:' . $_SESSION['org_sales_portal_api_url']);
$apiKey = "33f2ddee-724e-4bd1-9606-4408f5f95bdd";
$status = 'In Shop';

//SoapClient Parameters for the CSKern Web Service API
$params = array(
  "apiKey" => $apiKey,
  "status" => $status
);

$result = $sc->__soapCall("GetOrdersByStatus", array($params));
$rx = json_decode($result->GetOrdersByStatusResult);
$nr = count($rx);
$x->orders = [];
//array_push($x->orders,$rx);
//array_push($rx, array('total_results' => $nr));
$x->response = 'GOOD';
$x->message = $nr . ' orders fetched successfully!';
$x->total_results = $nr;
$x->orders = $rx;
//var_dump($rx);


//Setup Response Output...
$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;