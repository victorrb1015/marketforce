<?php
//echo 'Start<br><br>';
error_reporting(0);
/*This is a testing script for the new CSKern API Webservice*/

  $sc = new SoapClient("http://sales.allsteelcarports.com/publicapi/allsteelwebservice.asmx?WSDL");
  $apiKey = "33f2ddee-724e-4bd1-9606-4408f5f95bdd";
  $invoice = $_GET['invoice'];
  //$status = $_GET['status'];

echo '<html>
      <head>
      <title>Daily Sales Report</title>
      <style>
      table{
        margin:auto;
      }
      th,td{
        border:1px solid black;
        padding: 5px;
      }
      </style>
      </head>
      <body>';

echo '<table>
      <thead>
      <tr>
      <th>Invoice</th>
      <th>Status</th>
      <th>Dealer</th>
      <th>Customer</th>
      <th>City</th>
      <th>State</th>
      <th>Date Created</th>
      <th>Created By</th>
      <th>Total Sale</th>
      <th>Tax</th>
      </tr>
      </thead>
      <tbody>';


$sales = 0;
$tSales = 0;

  //SoapClient Parameters for the CSKern Web Service API
    $params = array(
      "apiKey" => $apiKey,
      "invoice" => $invoice,
      "status" => 'In Shop'
    );
    $result = $sc->__soapCall("GetOrdersByStatus", array($params));
    //$r = json_decode($result->GetOrdersByInvoiceResult);
    $rx = json_decode($result->GetOrdersByStatusResult);
    

  $i = 1;
  foreach($rx as $x){
    $d = date("m/d/Y", strtotime($x->invoiceDate));
    $td = date("m/d/Y", strtotime("now"));
    //$td = date("m/d/Y", strtotime("-1 days"));
    //echo $i . ' = ' . $x->orderID . ' => ' . $x->customer . ' => ' . $x->buildingAddress . ' => ' . date("m/d/Y", strtotime($x->invoiceDate)) . ' => ' . $x->orderStatus . ' => ' . $x->total . '<br>';
    //echo $i . ' => ' . strtotime($d) . ' => ' . strtotime($td) . '<br>';
    if($d == $td){
      $tSales = $tSales + $x->totalSale;
      echo '<tr>
            <td>' . $x->orderID . '</td>
            <td>' . $x->orderStatus . '</td>
            <td>' . $x->dealer . '</td>
            <td>' . $x->customer . '</td>
            <td>' . $x->buildingCity . '</td>
            <td>' . $x->buildingState . '</td>
            <td>' . $x->invoiceDate . '</td>
            <td>' . $x->createdBy . '</td>
            <td>' . $x->totalSale . '</td>
            <td>' . $x->tax . '</td>
            </tr>';
    }
    $sales = $sales + $x->totalSale;
    
    $i++;
  }


  //SoapClient Parameters for the CSKern Web Service API
    $params = array(
      "apiKey" => $apiKey,
      "invoice" => $invoice,
      "status" => 'Installing'
    );
    $rresult = $sc->__soapCall("GetOrdersByStatus", array($params));
    //$r = json_decode($result->GetOrdersByInvoiceResult);
    $rrx = json_decode($rresult->GetOrdersByStatusResult);
    

  $i = 1;
  foreach($rrx as $x){
    $d = date("m/d/Y", strtotime($x->invoiceDate));
    $td = date("m/d/Y", strtotime("now"));
    //$td = date("m/d/Y", strtotime("-1 days"));
    //echo $i . ' = ' . $x->orderID . ' => ' . $x->customer . ' => ' . $x->buildingAddress . ' => ' . date("m/d/Y", strtotime($x->invoiceDate)) . ' => ' . $x->orderStatus . ' => ' . $x->total . '<br>';
    //echo $i . ' => ' . strtotime($d) . ' => ' . strtotime($td) . '<br>';
    if($d == $td){
      $tSales = $tSales + $x->totalSale;
      echo '<tr>
            <td>' . $x->orderID . '</td>
            <td>' . $x->orderStatus . '</td>
            <td>' . $x->dealer . '</td>
            <td>' . $x->customer . '</td>
            <td>' . $x->buildingCity . '</td>
            <td>' . $x->buildingState . '</td>
            <td>' . $x->invoiceDate . '</td>
            <td>' . $x->createdBy . '</td>
            <td>' . $x->totalSale . '</td>
            <td>' . $x->tax . '</td>
            </tr>';
    }
    //$sales = $sales + $x->totalSale;
    
    $i++;
  }


echo '</tbody>
      </table>
      </body>
      </html>';
//echo '<h1>Total Pending Orders: $' . number_format($sales,2) . '</h1>';
//echo '<h1>Sales Today: $' . number_format($tSales,2) . '</h1>';
  
$r->pendingOrders = number_format($sales,2);
$r->salesToday = number_format($tSales,2);
$response = json_encode($r);
//echo $response;
?>