<div class="panel panel-default card-view">
  <div class="panel-heading">
    <!--<div class="pull-left">
      <h6 class="panel-title txt-dark">MarketForce Support Tickets</h6>
    </div>-->
    <div class="clearfix"></div>
  </div>
  <div class="panel-wrapper collapse in">
    <div class="panel-body row pa-0">
      <div class="table-wrap">
        <div class="table-responsive">
          <div id="support_table_wrapper" class="dataTables_wrapper no-footer">
            <table class="table display product-overview border-none dataTable no-footer" id="support_table" role="grid">
              <thead>
                <tr role="row">
                  <th>ticket ID</th>
                  <th>User</th>
                  <th>Created</th>
                  <th>Type</th>
                  <th>Ticket Info</th>
                  <th>Last Updated</th>
                  <th>Agent Assigned</th>
                  <th>Priority</th>
                  <th>Status</th>
                  <!--<th>Actions</th>-->
                </tr>
              </thead>
              <tbody id="ticket_table_tbody">
                <!--<tr role="row" class="odd">
                  <td class="sorting_1">#85457891</td>
                  <td>Sue Woodger</td>
                  <td>Pogody header</td>
                  <td>Oct 17</td>
                  <td>
                    <span class="label label-danger">pending</span>
                  </td>
                  <td><a href="javascript:void(0)" class="pr-10" data-toggle="tooltip" title="completed"><i class="zmdi zmdi-check"></i></a> <a href="javascript:void(0)" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i></a></td>
                </tr>-->
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>