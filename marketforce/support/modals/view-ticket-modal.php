<div class="modal fade" role="dialog" tabindex="-1" id="viewTicketModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Support Ticket #<span id="ticket_num"></span> Activity</h4>
      </div>
      <div class="modal-body">
        <div id="ticket_notes_box" class="note-box" style="">
          <div class="alert alert-default">
            <i class="fas fa-info-circle mr-15"></i> No Activity Found...
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>