<!-- sample modal content -->
<div id="confirmReceivingModal" class="modal fade" tabindex="-1" role="modal" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria="true">×</button>
        <h5 class="modal-title" id="myModalLabel">Confirm Receiving</h5>
      </div>
      <div class="modal-body">

        <div class="panel-wrapper collapse in">
          <div class="panel-body">
            <h6 class="text-warning">
              Reference Number:
              <input type="text" id="rec_ref" name="example-input1-group2" class="form-control" placeholder="Reference Number Here...">
            </h6>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="confirm_receiving();">Submit</button>
        <span id="rec_ref_error" style="color:red;font-weight:bold;"></span>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->