<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default card-view">
      <div class="panel-wrapper collapse in">
        <div class="panel-body row pa-0">
          <div class="table-wrap">
            <div class="table-responsive">
              <table class="table display product-overview border-none" id="support_table">
                <thead>
                  <tr>
                    <th>Barcode</th>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody id="items_table_body">
                  <!--<tr>
                    <td>#85457898</td>
                    <td>Jens Brincker</td>
                    <td class="txt-success">27</td>
                    <td><a href="javascript:void(0)" class="text-danger" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i></a></td>
                  </tr>-->
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>