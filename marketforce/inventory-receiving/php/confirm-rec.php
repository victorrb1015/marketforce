<?php
include '../../php/connection.php';

//Load Variables...
$rec_ref = mysqli_real_escape_string($conn, $_GET['rec_ref']);
$rn = uniqid();
$inv_zone = $_SESSION['inventory_zone'];

if($inv_zone == ''){
  $x->response = 'Error';
  $x->message = 'You do not have a zone assigned to your User ID.';
  $response = json_encode($x);
  echo $response;
  die();
}

//Make Inventory Adjustments...
$q = "SELECT DISTINCT `item_id` FROM `inv_rec_items` WHERE `inactive` != 'Yes' AND `receiving_number` = '' AND `user_id` = '" . $_SESSION['user_id'] . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  $iq = "SELECT * FROM `inv_rec_items` WHERE `inactive` != 'Yes' AND `receiving_number` = '' AND `user_id` = '" . $_SESSION['user_id'] . "' AND `item_id` = '" . $r['item_id'] . "'";
  $ig = mysqli_query($conn, $iq) or die($conn->error);
  $ir = mysqli_fetch_array($ig);
  $iqty = mysqli_num_rows($ig);
  
  //Get current inventory level...
  $ciq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $r['item_id'] . "'";
  $cig = mysqli_query($conn, $ciq) or die($conn->error);
  $cir = mysqli_fetch_array($cig);
  $cqty = $cir['qty'];
  $czqty = $cir['qty_'.$inv_zone];
  $nqty = $cqty + $iqty;
  $adjustment = $nqty - $cqty;
  //Make Adjustment to DB...
  $iuq = "UPDATE `inventory_items` SET `qty` = (`qty` + '" . $iqty . "'), `qty_" . $inv_zone . "` = (`qty_" . $inv_zone . "` + " . $iqty . ") WHERE `ID` = '" . $r['item_id'] . "'";
  mysqli_query($conn, $iuq) or die($conn->error);
  
  $iaq = "INSERT INTO `inventory_adjustments`
          (
          `date`,
          `time`,
          `pid`,
          `current_qty`,
          `new_qty`,
          `adjustment`,
          `type`,
          `receiving_number`,
          `user_id`,
          `user_name`,
          `status`,
          `inactive`
          )
          VALUES
          (
          CURRENT_DATE,
          CURRENT_TIME,
          '" . $r['item_id'] . "',
          '" . $cqty . "',
          '" . $nqty . "',
          '" . $adjustment . "',
          'Receiving',
          '" . $rn . "',
          '" . $_SESSION['user_id'] . "',
          '" . $_SESSION['full_name'] . "',
          'Pending',
          'No'
          )";
  mysqli_query($conn, $iaq) or die($conn->error);
}


//Confirm Receiving Items...
$uq = "UPDATE `inv_rec_items` SET 
        `confirmed_date` = CURRENT_DATE,
        `confirmed_time` = CURRENT_TIME,
        `receiving_number` = '" . $rn . "' 
        WHERE `inactive` != 'Yes' AND `receiving_number` = '' AND `user_id` = '" . $_SESSION['user_id'] . "'";
mysqli_query($conn, $uq) or die($conn->error);


//Add receiving manifest record...
$iq = "INSERT INTO `inv_rec_manifests`
      (
      `date`,
      `time`,
      `receiving_number`,
      `rec_ref`,
      `user_id`,
      `user_name`,
      `status`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $rn . "',
      '" . $rec_ref . "',
      '" . $_SESSION['user_id'] . "',
      '" . $_SESSION['full_name'] . "',
      'Pending',
      'No'
      )";
mysqli_query($conn, $iq) or die($conn->error);

$x->response = 'GOOD';
$x->message = 'The manifest has been confirmed!';

$response = json_encode($x);
echo $response;


?>
