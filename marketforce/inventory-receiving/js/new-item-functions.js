function new_item(upc){
  var isRaw;
  var isComponent;
  var isProduct;
  var type;
  
  var proceed = confirm("Would you like to add this Barcode as a new item?");
  if(proceed === false || proceed === '' || proceed === null){
    return;
  }
  
  var isCoil = confirm("Is this new item a Coil Item?");
  if(isCoil === false || isCoil === '' || isCoil === null){
    isCoil = false;
  }else{
    isCoil = true;
    type = 'Coil';
  }
  
  if(isCoil === false){
    var isRaw = confirm("Is this new item a RAW Item?");
    if(isRaw === false || isRaw === '' || isRaw === null){
      isRaw = false;
    }else{
      isRaw = true;
      type = 'Raw';
    }
  }
  
  if(isRaw === false){
    isComponent = confirm("Is this new item a COMPONENT Item?");
    if(isComponent === false || isComponent === '' || isComponent === null){
      isComponent = false;
    }else{
      isComponent = true;
      type = 'Component';
    }
  }
  
  if(isComponent === false){
    isProduct = confirm("Is this new item a PRODUCT Item?");
    if(isProduct === false || isProduct === '' || isProduct === null){
      isProduct = false;
    }else{
      isProduct = true;
      type = 'Product';
    }
  }
  
  if(isCoil === false && isRaw === false && isComponent === false && isProduct === false){
    return;
  }
  
  $('#itemModal').modal('show');
  
  document.getElementById('product_mode').value = "New";
  document.getElementById('product_bc').value = upc;
  document.getElementById('product_name').value = '';
  document.getElementById('min_qty').value = 0;
  document.getElementById('product_info').value = '';
  document.getElementById('product_units').value = '';
  document.getElementById('product_cost').value = '';
  document.getElementById('product_price').value = '';
  document.getElementById('product_weight').value = '';
  $('input[name=length_choice]').attr('checked',false);
  $('input[name=color_choice]').attr('checked',false);
  document.getElementById('color_list_choice').value = '';
  document.getElementById('color_list_box').style.display = 'none';
  document.getElementById('product_name').focus();
  document.getElementById('item_type').value = type;
}


function save_item(){
  var pbc = document.getElementById('product_bc').value;
  if(pbc === ''){
    alert('Please Enter The Products Barcode!');
    return;
  }
  pbc = urlEncode(pbc);
  var pname = document.getElementById('product_name').value;
  if(pname === ''){
    alert('Please Enter The Products Name!');
    return;
  }
  pname = urlEncode(pname);
  var pinfo = document.getElementById('product_info').value;
  if(pinfo === ''){
    alert('Please Enter The Products Info!');
    return;
  }
  pinfo = urlEncode(pinfo);
  var punits = document.getElementById('product_units').value;
  if(punits === ''){
    alert('Please Enter The Products Units!');
    return;
  }
  punits = urlEncode(punits);
  var pcost = document.getElementById('product_cost').value;
  if(pcost === ''){
    alert('Please Enter The Products Cost!');
    return;
  }
  pcost = urlEncode(pcost);
  var pprice = document.getElementById('product_price').value;
  if(pprice === ''){
    alert('Please Enter The Products Price!');
    return;
  }
  pprice = urlEncode(pprice);
  var pweight = document.getElementById('product_weight').value;
  if(pweight === ''){
    alert('Please Enter The Product Weight!');
    return;
  }
  pweight = urlEncode(pweight);
  
  var ilength = document.querySelectorAll('input[name="length_choice"]:checked');
  if(ilength.length < 1){
    alert('Please Indicate Whether Length Is Required On The Invoice!');
    return;
  }
  var length = ilength[0].value;
  if(length === '' || length === 'undefined'){
    alert('Please Indicate Whether Length Is Required On The Invoice!');
    return;
  }
  var icolor = document.querySelectorAll('input[name="color_choice"]:checked');
  if(icolor.length < 1){
    alert('Please Indicate Whether Color Is Required On The Invoice!');
    return;
  }
  var color = icolor[0].value;
  if(color === '' || color === 'undefined'){
    alert('Please Indicate Whether Color Is Required On The Invoice!');
    return;
  }
  
  var color_list = document.getElementById('color_list_choice').value;
  if(color_list === '' && color === 'Yes'){
    alert('Please Select The Color List For This Product!');
    return;
  }
  
  var zone = document.getElementById('zone_list_choice').value;
  if(zone === ''){
    alert('Please Select The Zone For This Product!');
    return;
  }
  
  
  var mode = document.getElementById('product_mode').value;
  var pid = document.getElementById('product_id').value;
  var item_type = document.getElementById('item_type').value;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        toast_alert('Item Added!',r.message,'bottom-right','success');
        $('#itemModal').modal('hide');
        setTimeout(function(){
          bc_lookup(r.bc);
        },1000);
        //bc_lookup(r.bc);
      }else{
        toast_alert('Something Went Wrong...','An Error Occurred While Adding Item...','bottom-right','error');
      }
      
    }
  }
  xmlhttp.open("GET","inventory/php/add-edit-item.php?pbc="+pbc+"&pname="+pname+"&pinfo="+pinfo+"&punits="+punits+"&pcost="+pcost+"&pprice="+pprice+"&pweight="+pweight+"&length="+length+"&color="+color+"&color_list="+color_list+"&mode="+mode+"&id="+pid+"&item_type="+item_type+"&zone="+zone,true);
  xmlhttp.send();
}




$(".qty_box").keypress(function(e){ return e.which != 13; });




function color_list_option(){
  var icolor = document.querySelectorAll('input[name="color_choice"]:checked');
  var color_box = document.getElementById('color_list_box');
  if(icolor.length < 1){
    color_box.style.display = 'none';
  }
  var color = icolor[0].value;
  if(color === 'Yes'){
    color_box.style.display = 'inline';
  }else{
    color_box.style.display = 'none';
  }
}