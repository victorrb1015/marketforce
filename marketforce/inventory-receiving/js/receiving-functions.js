document.getElementById('item_barcode').addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    var bc = document.getElementById('item_barcode').value;
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        //document.getElementById('item_count').innerHTML = r.item_count;
        document.getElementById('item_barcode').value = '';
        //add_item(bc,r.item_id,r.item_name,'1');
        load_items();
        console.warn('Item added successfully!');
      }else{
        toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
        document.getElementById('item_barcode').value = '';
        new_item(bc);
      }
    }
  }
  xmlhttp.open("GET","inventory-receiving/php/add-rec-item.php?bc="+bc,true);
  xmlhttp.send();
  }
});


function bc_lookup(bc) {
    //var bc = document.getElementById('item_barcode').value;
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        //document.getElementById('item_count').innerHTML = r.item_count;
        document.getElementById('item_barcode').value = '';
        //add_item(bc,r.item_id,r.item_name,'1');
        load_items();
        console.warn('Item added successfully!');
      }else{
        toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
        document.getElementById('item_barcode').value = '';
        new_item(bc);
      }
    }
  }
  xmlhttp.open("GET","inventory-receiving/php/add-rec-item.php?bc="+bc,true);
  xmlhttp.send();
}

function load_items(){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById('items_table_body').innerHTML = '';
        document.getElementById('item_count').innerHTML = r.item_count;
        console.log(this.responseText);
        console.log(r.item);
        for(var i = 0; i < r.item.length; i++){
          var x = r.item[i];
          add_item(x.item_barcode,x.item_id,x.item_name,x.item_qty);
        }
        console.warn('Items added successfully!');
      }else{
        toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory-receiving/php/get-rec-items.php",true);
  xmlhttp.send();
}

(function loader(){
  load_items();
})();


function add_item(item_barcode,item_id,item_name,item_qty){
  var tbody = document.getElementById('items_table_body');
  //Create Row...
  var row = tbody.insertRow(0);
  row.setAttribute('id',item_id+'_item_row');
  //Create Columns...
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  var cell4 = row.insertCell(3);
  //Fill the Cells...
  cell1.innerHTML = item_barcode;
  cell2.innerHTML = item_name;
  cell3.innerHTML = item_qty;
  cell3.setAttribute('class','txt-success');
  var a = document.createElement('a');
  a.setAttribute('href','Javascript: remove_item('+item_id+');');
  a.setAttribute('class','text-danger mr-20');
  a.setAttribute('title','Delete');
  a.setAttribute('data-toggle','tooltip');
  a.setAttribute('data-placement','top');
  var i = document.createElement('i');
  i.setAttribute('class','zmdi zmdi-delete');
  a.appendChild(i);
  var a2 = document.createElement('a');
  a2.setAttribute('href','Javascript: edit_item_qty('+item_id+');');
  a2.setAttribute('class','text-warning mr-20');
  a2.setAttribute('title','Edit');
  a2.setAttribute('data-toggle','tooltip');
  a2.setAttribute('data-placement','top');
  var i2 = document.createElement('i');
  i2.setAttribute('class','zmdi zmdi-edit');
  a2.appendChild(i2);
  cell4.appendChild(a2)
  cell4.appendChild(a);
  //Initialize Tooltip functionality...
  init_tooltips();
}



function edit_item_qty(iid){
  var nqty = prompt('Please Enter The New Quantity:');
  if(isNaN(nqty) || nqty === '' || nqty === null){
    toast_alert('Error','The New Quantity Must Be A Number!','bottom-right','error');
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        toast_alert('Success',r.message,'bottom-right','success');
        load_items();
      }else{
        toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
        document.getElementById('item_barcode').value = '';
      }
    }
  }
  xmlhttp.open("GET","inventory-receiving/php/edit-item-qty.php?iid="+iid+"&nqty="+nqty,true);
  xmlhttp.send();
}


function remove_item(iid){
  var conf = confirm('Are you sure you want to remove this item?');
  if(conf === '' || conf === null || conf === false){
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        toast_alert('Success',r.message,'bottom-right','success');
        load_items();
      }else{
        toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
        document.getElementById('item_barcode').value = '';
      }
    }
  }
  xmlhttp.open("GET","inventory-receiving/php/remove-rec-item.php?iid="+iid,true);
  xmlhttp.send();
}


function clear_items(){
  var conf = confirm('Are you sure you want to clear all items?');
  if(conf === '' || conf === null || conf === false){
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        toast_alert('Success',r.message,'bottom-right','success');
        load_items();
      }else{
        toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
        document.getElementById('item_barcode').value = '';
      }
    }
  }
  xmlhttp.open("GET","inventory-receiving/php/remove-rec-item.php?iid=ALL",true);
  xmlhttp.send();
}



function confirm_receiving(){
  var conf = confirm('Are you sure you want to confirm all items?');
  if(conf === '' || conf === null || conf === false){
    return;
  }
  var rec_ref = document.getElementById('rec_ref').value;
  if(rec_ref === ''){
    document.getElementById('rec_ref_error').innerHTML = 'Please Enter a reference Number';
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        toast_alert('Success',r.message,'bottom-right','success');
        document.getElementById('rec_ref').value = '';
        $('#confirmReceivingModal').modal('toggle');
        load_items();
      }else{
        toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
        document.getElementById('item_barcode').value = '';
      }
    }
  }
  xmlhttp.open("GET","inventory-receiving/php/confirm-rec.php?rec_ref="+rec_ref,true);
  xmlhttp.send();
}
