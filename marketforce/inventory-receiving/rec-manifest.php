<?php
//session_start();
include '../php/connection.php';


//Load Variables
$id = $_GET['id'];

//Load Manifest Info...
$mq = "SELECT * FROM `inv_rec_manifests` WHERE `ID` = '" . $id . "'";
$mg = mysqli_query($conn, $mq) or die($conn->error);
$mr = mysqli_fetch_array($mg);
?>
  <html>

  <head>
    <title>Receiving Manifest</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js" integrity="sha384-u/bQvRA/1bobcXlcEYpsEdFVK/vJs3+T+nXLsBYJthmdBuavHvAW6UsmqO2Gd/F9" crossorigin="anonymous"></script>
    <style>
      /*
      html { font: 16px/1 "Open Sans", sans-serif; overflow: auto; padding: 0.5in; }
      html { background: #999; cursor: default; }

      body { box-sizing: border-box; min-height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
      body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }
      */
      body {
        -webkit-print-color-adjust: exact !important;
      }

      div {
        //border: 2px solid black;
      }

      .borders div {
        border: 2px solid black;
      }

      @media print {
        @page {
          margin: 0;
        }
        body {
          margin: 1.6cm;
        }
        footer {
          page-break-after: always;
        }
      }
    </style>
  </head>

  <body>

    <div class="main" style="width:80%;margin:auto;">
      <br><br>

      <div class="row">
        <div class="col-4" style="text-align:center;">
          <img src="<?php echo $_SESSION['org_logo_url_alt']; ?>" alt="Logo" style="width:100%;" />
        </div>
        <div class="col-8" style="text-align:right;">
          <h3><?php echo $_SESSION['org_full_name']; ?></h3>
          <p><?php echo $_SESSION['org_address'] . ' ' . $_SESSION['org_city'] . ' ' . $_SESSION['org_state'] . ' ' . $_SESSION['org_zip']; ?></p>
          <p>Phone: <?php echo $_SESSION['org_phone']; ?></p>
          <p>Email: <?php echo $_SESSION['org_email']; ?></p>
        </div>
      </div>

      <div class="row">
        <div class="col-12" style="text-align:center;">
          <h1>Receiving Manifest</h1>
          <p style="font-weight:bold;">
            Created By: <?php echo $mr['user_name']; ?>
          </p>
          <p>Submitted:
            <?php echo date("l, F dS Y", strtotime($mr['sub_date'])); ?>
          </p>
          <p>Manifest ID:
            <?php echo $mr['receiving_number']; ?>
          </p>
        </div>
      </div>


      <div class="row" style="font-weight:bold;font-size:20px;color:black;background:#727272;">
        <div class="col">
          Date
        </div>
        <div class="col">
          Barcode
        </div>
        <div class="col">
          Item Name
        </div>
        <div class="col">
          Description
        </div>
        <div class="col">
          Qty
        </div>
        <!--<div class="col">
          Price/Gal.
        </div>
        <div class="col">
          # Gallons
        </div>
        <div class="col">
          Amount
        </div>-->
      </div>


      <?php
    $rnum = 1;
    $q = "SELECT * FROM `inv_rec_items` WHERE `inactive` != 'Yes' AND `receiving_number` = '" . $mr['receiving_number'] . "'";
    $g = mysqli_query($conn, $q) or die($conn->error);
    $item_total = mysqli_num_rows($g);
    while($r = mysqli_fetch_array($g)){
      if($rnum % 2 == 0){
        $style = 'style="background:#B2B2B2;"';
      }else{
        $style = '';
      }
      echo '<div class="row" ' . $style . '>
              <div class="col">
                ' . date("m/d/y",strtotime($r['date'])) . '
              </div>
              <div class="col">
                ' . $r['item_barcode'] . '
              </div>
              <div class="col">
                ' . $r['item_name'] . '
              </div>
              <div class="col">
                ' . $r['item_info'] . '
              </div>
              <div class="col">
                1
              </div>
            </div>';
      $rnum++;
    }
    
    ?>


        <br><br>

        <div class="row">
          <!--<div class="col-9">
            <?php
          $sjson = json_decode($rr['notes']);

          echo '<div class="row" style="width:95%;margin:auto;">
                <h5>Notes:</h5>';
                foreach($sjson as $x){
                  if($x->Note != ''){
                  echo '<div class="col-12 card bg-light" style="color:black;">
                            <h6>' . $x->Stamp . '</h6>
                            <p>' . $x->Note . '</p>
                            </div>';
                  }
                }
               echo '</div>';
        ?>
          </div>-->
          <div class="col-9"></div>

          <div class="col-3" class="borders">

            <div class="row">
              <div class="col" style="background:#B2B2B2;border: 1px solid black;">
                <b>Total Items</b>
              </div>
              <div class="col" style="border: 1px solid black;">
                <b><?php echo $item_total; ?></b>
              </div>
            </div>

            <!--<div class="row">
              <div class="col" style="background:#B2B2B2;border: 1px solid black;">
                <b> Miles</b>
              </div>
              <div class="col" style="border: 1px solid black;">
                <?php echo number_format($tm); ?>
              </div>
            </div>

            <div class="row">
              <div class="col" style="background:#B2B2B2;border: 1px solid black;">
                <b>Food</b>
              </div>
              <div class="col" style="border: 1px solid black;">
                $
                <?php echo number_format($food,2); ?>
              </div>
            </div>

            <div class="row">
              <div class="col" style="background:#B2B2B2;border: 1px solid black;">
                <b>Gas</b>
              </div>
              <div class="col" style="border: 1px solid black;">
                $
                <?php echo number_format($gas,2); ?>
              </div>
            </div>

            <div class="row">
              <div class="col" style="background:#DCDCDC;border: 1px solid black;">
                &nbsp;&nbsp;&nbsp;&nbsp;<b>Avg. Price/Gal.</b>
              </div>
              <div class="col" style="border: 1px solid black;">
                $
                <?php
            $nr = $rnum - 1;
            //$apg = $pg / $nr;
            $apg = $cgas / $ng;
            echo number_format($apg,2); 
            ?>
              </div>
            </div>

            <div class="row">
              <div class="col" style="background:#DCDCDC;border: 1px solid black;">
                &nbsp;&nbsp;&nbsp;&nbsp;<b># Gallons</b>
              </div>
              <div class="col" style="border: 1px solid black;">
                <?php echo number_format($ng); ?>
              </div>
            </div>

            <div class="row">
              <div class="col" style="background:#B2B2B2;border: 1px solid black;">
                <b>Hotel</b>
              </div>
              <div class="col" style="border: 1px solid black;">
                $
                <?php echo number_format($hotel,2); ?>
              </div>
            </div>

            <div class="row">
              <div class="col" style="background:#B2B2B2;border: 1px solid black;">
                <b>Office Supplies</b>
              </div>
              <div class="col" style="border: 1px solid black;">
                $
                <?php echo number_format($supplies,2); ?>
              </div>
            </div>

            <div class="row">
              <div class="col" style="background:#B2B2B2;border: 1px solid black;">
                <b>Misc.</b>
              </div>
              <div class="col" style="border: 1px solid black;">
                $
                <?php echo number_format($misc,2); ?>
              </div>
            </div>-->

          </div>

        </div>
<br><br><br>
    </div>
  </body>

  </html>