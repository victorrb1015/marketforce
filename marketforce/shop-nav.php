<?php

?>
<style>
  #form-box {
  border:2px solid rgb(51,122,183);
  background:rgba(51,122,183,0.1);
}
  mark{
    background: yellow;
  }

</style>
<link href="https://fonts.googleapis.com/css?family=Rock+Salt" rel="stylesheet">
<!--JQuery CSS-->
    <link href="jquery/jquery-ui.css" rel="stylesheet">
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="img/logo-allsteel.png" ></a>
            </div>
            <!-- Top Menu Items -->

            <?
              //if($_SESSION['user_id'] == '1'){
                echo '<h4 style="color:green;font-weight:bold;text-align:center;float:left;margin-left:10%;">Need Help? Call Us: 1(866) 791-6131</h4>';
              //}
              ?>
  
            <ul class="nav navbar-right top-nav">
                
								<li style="display:table;">
									<button type="button" data-toggle="modal" data-target="#newTask" onclick="clear_newTask_modal();" class="btn btn-success btn-sm" style="margin-top:10px;">New Task</button>
								</li>
								<li>
									<a href="#" data-toggle="modal" data-target="#phoneLookup" style="color:red;font-weight:bold;">
										<i class="fa fa-phone fa-fw"></i>
									</a>
								</li>
                <li>
									<a href="#" data-toggle="modal" data-target="#dealerCalc" style="color:red;font-weight:bold;">
										<i class="fa fa-calculator fa-fw"></i>
									</a>
								</li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['full_name']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
											<?php
											if($_SESSION['admin'] == 'Yes'){
												echo '<li>
													<a href="access.php"><i class="fa fa-users"></i> Manage Users</a>
											  </li>';
											}
											?>
											
												<li>
													<a href="settings.php"><i class="fa fa-cogs"></i> My Account</a>
												</li>
											
                      <?
                      if($_SESSION['user_id'] == '1'){
                        echo '<li>
                                <a href="knowledge-base.php"><i class="fa fa-graduation-cap"></i> Training</a>
                              </li>';
                      }
                      ?>
											
											 <li>
												 <a href="../cron/labels.php" target="_blank"><i class="fa fa-tags"></i> Print Labels</a>
											 </li>
												
												<li class="divider"></li>
                        <li>
                            <a href="../index.php?logout=1"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav" style="flex-direction:inherit;">
                  
     
           <!-------------------------------------------------------------------DASHBOARD-------------------------------------------------------------------------------->
                    <li>
                        <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard
											<?php
													
													if($_SESSION['doc_approval'] == 'Yes'){
														$ndq = "SELECT * FROM `new_packets` WHERE `status` = 'Pending'";
													}else{
														$ndq = "SELECT * FROM `new_packets` WHERE `status` = 'Pending' AND `rep` = '" . $_SESSION['user_id'] . "'";
													}
													$ndg = mysqli_query($conn, $ndq);
													$ndn = mysqli_num_rows($ndg);
													if($ndn != 0){
													echo '&nbsp; <span style="background:red;color:white;padding:5px;border-radius:20px;">' . $ndn . '</span>';
													}
													?>
												</a>
                    </li>
          
                  
           <!-------------------------------------------------------------------Orders-------------------------------------------------------------------------------->
                  <?php
                   if($_SESSION['orders'] == 'Yes'){
                    echo '<li>
                              <a href="orders.php"><i class="fa fa-fw fa-shopping-cart"></i> Orders</a>
                          </li>';
                   }
                  ?>
            
                  
           <!-------------------------------------------------------------------Make A Suggestion-------------------------------------------------------------------------------->
									<li style="background:#6E6E6E;border:1px solid #222222;">
												<a href="suggestion.php"><i class="fa fa-lightbulb-o fa-fw"></i> Make A Suggestion</a>
									</li>
   
           <!-------------------------------------------------------------------Report An Issue-------------------------------------------------------------------------------->
                  <li style="background:#6E6E6E;border:1px solid #222222;">
												<a href="https://burtonsolution.on.spiceworks.com/portal/tickets" target="_blank"><i class="fa fa-bug fa-fw"></i> Report An Issue</a>
									</li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

<!--Market Force Error Notification-->
<!--<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-exclamation-triangle"></i>  
            <strong>
              NOTICE: Market Force is currently experiencing an issue with outgoing email communications. 
              Due to this issue, you may not receive some of the emails you would normally receive from Market Force. 
              We are working to correct the issue and you will be notified when this has been completely resolved!
            </strong>
        </div>
    </div>
</div>-->
