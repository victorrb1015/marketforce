<?php
include 'security/session/session-settings.php';

include 'php/connection.php';

if(!isset($_SESSION['in'])){
  session_destroy();
  echo 'You do not have authentication for this site...<br>';
  echo '<script>
        setInterval(function(){
        window.location="https://marketforceapp.com";
        }, 2000);
        </script>';
  return;
}

if($_GET['abrev']){
  $ssq = "SELECT * FROM `states` WHERE `abrev` = '" . $_GET['abrev'] . "'";
  $ssg = mysqli_query($conn, $ssq) or die($conn->error);
  $ssr = mysqli_fetch_array($ssg);
  $state = $ssr['state'];
  echo '<script>
        window.location = "notes.php?state=' . $state . '";
        </script>';
}

  $state = $_GET['state'];

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
   

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <!--<link href="css/plugins/morris.css" rel="stylesheet">-->

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--JQuery CSS-->
    <link href="jquery/jquery-ui.css" rel="stylesheet">

  <?php
    echo '<!--JQuery US-Map Scripts-->
    <script src="visit-notes/us-map/lib/raphael.js"></script>
    <!-- <script src="visit-notes/us-map/scale.raphael.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.js"></script>
    <script src="visit-notes/us-map/color.jquery.js"></script>
    <script src="visit-notes/us-map/lib/jquery.usmap.js"></script>';
    
  
  ?>
  
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
    .col-lg-4{
      //border: 2px solid black;
      height: 300px;
    }
    .col-lg-4 p{
      font-weight: bold;
      text-decoration: underline;
    }
    #form-box{
      //display: inline-block;
    }
    
    /* This only works with JavaScript, 
if it's not present, don't show loader */
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  //background: url(img/loader-128x/Preloader_7.gif) center no-repeat #fff;
  //background: url(img/loaders/high-tech-gif.gif) center no-repeat #fff;
  background: url(img/loaders/loading-circle.gif) center no-repeat rgba(250,250,250,0.5);
}
    
    .hours-table td, th{
      padding:5px;
      border: 1px solid black
    }
    
    .scroll{
      overflow: scroll;
      height: 350px;
    }
    
    .list-box-custom{
      height:250px;
    }
  </style>
  <script>
    
  function clr_res(id,s){
    //alert("test");
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
      //alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","<?php if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') { echo 'https'; }else{ echo 'http'; } ?>://library.burtonsolution.tech/php/reserve.php?id="+id+"&s="+s,true);
  xmlhttp.send();
  }
    
    
    
    
      function get_dealers(){
        var state = document.getElementById('ss').value;
        if(state == ''){
          alert('Please Select A State First!');
          return;
        }
        window.location = 'notes.php?state=' + state;
      }
    
    function main_menu(){
      window.location = 'notes.php';
    }
    
    
    //Top 20 & Strong Possibility Functions...
    function cOn(x,mode){
      if(mode === '7'){
        x = 'CCP'+x;
      }
      document.getElementById('icon_'+x).classList.remove('fa-square-o');
      document.getElementById('icon_'+x).classList.add('fa-check-square-o');
    }
    
    function cOff(x,mode){
      if(mode === '7'){
        x = 'CCP'+x;
      }
      document.getElementById('icon_'+x).classList.remove('fa-check-square-o');
      document.getElementById('icon_'+x).classList.add('fa-square-o');
    }
    
    
    function set_top(id,mode){

      if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      var q = JSON.parse(this.responseText);
      if(q.status === 'OK'){
        
        if(mode === 'true'){
        document.getElementById('icon_container_'+id).innerHTML = '<a style="color:black;" onclick="set_top('+id+',\'false\');"><i class="fa fa-check-square-o" id="icon_'+id+'" style="font-size:25px;" onmouseover="cOff('+id+');" onmouseout="cOn('+id+');"></i></a>';
        }else{
        document.getElementById('icon_container_'+id).innerHTML = '<a style="color:black;" onclick="set_top('+id+',\'true\');"><i class="fa fa-square-o" id="icon_'+id+'" style="font-size:25px;" onmouseover="cOn('+id+');" onmouseout="cOff('+id+');"></i></a>';
        }
        
      }else if(q.status === 'FULL'){
        alert('There are already 20 Dealers set as a Top20! Please unselect another Dealer if you wish to add this Dealer!');
      }else{
        alert('There was an error proccessing your request!');
      }
      
    }
  }
  xmlhttp.open("GET","visit-notes/php/set-top20.php?id="+id+"&mode="+mode,true);
  xmlhttp.send();
    }
    
    
    
    function set_poss(id,mode){
      
      //Parse ID for re-insertion
      if(id.indexOf("CCP") < 0){
        id = 'CCP'+id;
      }
        var rxid = id.split("CCP");
        var xid = rxid[1];
      //alert(xid);
      if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //alert(id);
      var q = JSON.parse(this.responseText);
      if(q.status === 'OK'){
        window.location.reload();
        if(mode === 'true'){
        document.getElementById('icon_container_'+id).innerHTML = '<a style="color:black;" onclick="set_poss('+xid+',\'false\');"><i class="fa fa-check-square-o" id="icon_'+id+'" style="font-size:25px;" onmouseover="cOff('+xid+',7);" onmouseout="cOn('+xid+',7);"></i></a>';
        }else{
        document.getElementById('icon_container_'+id).innerHTML = '<a style="color:black;" onclick="set_poss('+xid+',\'true\');"><i class="fa fa-square-o" id="icon_'+id+'" style="font-size:25px;" onmouseover="cOn('+xid+',7);" onmouseout="cOff('+xid+',7);"></i></a>';
        }
        
      }else if(q.status === 'FULL'){
        alert('There are too many Strong Possibilities! Please unselect another Dealer if you wish to add this Dealer!');
      }else{
        alert('There was an error proccessing your request!');
      }
      
    }
  }
  xmlhttp.open("GET","visit-notes/php/set-poss.php?id="+id+"&mode="+mode,true);
  xmlhttp.send();
    }
    
    
    
      function urlEncode(url){
    url = url.replace(/&/g, '%26'); 
    url = url.replace(/#/g, '%23');
    return url;
  }
    
    
  function toggle_height(t){
    if(t === 'prospects'){
      var pi = document.getElementById('prospect_icon');
      var pt = document.getElementById('prospect_table');
      if(pi.classList.contains('fa-chevron-down') === true){
        pi.classList.remove('fa-chevron-down');
        pi.classList.add('fa-chevron-up');
        pt.classList.remove('scroll');
      }else{
        pi.classList.add('fa-chevron-down');
        pi.classList.remove('fa-chevron-up');
        pt.classList.add('scroll');
      }
    }
    
    if(t === 'main'){
      var mi = document.getElementById('main_icon');
      var mt = document.getElementById('main_table');
      if(mi.classList.contains('fa-chevron-down') === true){
        mi.classList.remove('fa-chevron-down');
        mi.classList.add('fa-chevron-up');
        mt.classList.remove('scroll');
      }else{
        mi.classList.add('fa-chevron-down');
        mi.classList.remove('fa-chevron-up');
        mt.classList.add('scroll');
      }
    }
  }
  </script>
  
  <!--Form Functionalities-->
  <?php
  include 'visit-notes/js/visit-note-form-functions.php';
  include 'visit-notes/js/cold-call-form-functions.php';
  include 'visit-notes/js/dealer-hours.php';
  include 'visit-notes/js/prospect-form-functions.php';
  include 'repairs/js/display-repair-form-functions.php';
  ?>
  
  
  <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>-->
<script>
//paste this code under the head tag or in a separate js file.
  // Wait for window load
  $(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");;
  });
  </script>
</head>

<body>
  <!--PRELOADER GIF-->
<!--<div class="se-pre-con"></div>-->
  
    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> Add Visit Notes</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Add Visit Notes
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
              <!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
              
    <!--This is where real content for the page will go-->
             <div class="row">
             <div class="col-lg-12">
               <?php
               
               if(!$state){
                 
                 /*if($_SESSION['user_id'] != '1'){
               echo '<select id="ss">
                 <option value="">Select A State...</option>';
                 
                 $sq = "SELECT DISTINCT `state` FROM `dealers` ORDER BY `state` ASC";
                 $sg = mysqli_query($conn, $sq) or die($conn->error);
                 while($sr = mysqli_fetch_array($sg)){
                   echo '<option value="' . $sr['state'] . '">' . $sr['state'] . '</option>';
                 }
                 
               echo '</select><br><br>
                      <button type="button" onclick="get_dealers();">View Dealers</button>';
                 }else{*/
                   
                    echo "<script>
  $(document).ready(function() {
    $('#map').usmap({
    ";
    $ssq = "SELECT * FROM `states`";
    $ssg = mysqli_query($conn, $ssq) or die($conn->error);
    $ssnr = mysqli_num_rows($ssg);
      echo "'stateSpecificStyles': {";
    while($ssr = mysqli_fetch_array($ssg)){
      $csq = "SELECT * FROM `dealers` WHERE `state` = '" . $ssr['state'] . "' AND `inactive` != 'Yes'";
      $csg = mysqli_query($conn, $csq) or die($conn->error);
      $csrn = mysqli_num_rows($csg);
      $i = 0;
      if($csrn > 0){
        echo "'" . $ssr['abrev'] . "' : {fill: '#f00'}";
      }else{
        echo "'" . $ssr['abrev'] . "' : {fill: '#333'}";
      }
      $i++;
      if($i < $ssnr){
        echo ",";
      }
      
    }
      echo "},";
                
                   
    $ssq = "SELECT * FROM `states`";
    $ssg = mysqli_query($conn, $ssq) or die($conn->error);
    $ssnr = mysqli_num_rows($ssg);
     echo "'stateSpecificHoverStyles': {";
    while($ssr = mysqli_fetch_array($ssg)){
      $csq = "SELECT * FROM `dealers` WHERE `state` = '" . $ssr['state'] . "' AND `inactive` != 'Yes'";
      $csg = mysqli_query($conn, $csq) or die($conn->error);
      $csrn = mysqli_num_rows($csg);
      $ii = 0;              
      if($csrn > 0){
        echo "'" . $ssr['abrev'] . "' : {fill: '#33c'}";
      }else{
        echo "'" . $ssr['abrev'] . "' : {fill: '#333'}";
      }
      $ii++;
      if($ii < $ssnr){
        echo ",";
      }
      
    }
      echo "},";
                   
                   
                   
    $ssq = "SELECT * FROM `states`";
    $ssg = mysqli_query($conn, $ssq) or die($conn->error);
    $ssnr = mysqli_num_rows($ssg);                
echo "'mouseoverState': {";
     while($ssr = mysqli_fetch_array($ssg)){
      $csq = "SELECT * FROM `dealers` WHERE `state` = '" . $ssr['state'] . "' AND `inactive` != 'Yes'";
      $csg = mysqli_query($conn, $csq) or die($conn->error);
      $csrn = mysqli_num_rows($csg);
      $ii = 0;              
      if($csrn > 0){
        echo "'" . $ssr['abrev'] . "' : function(event, data) {
          $('#alert')
          .text('" . $ssr['state'] . "')
          .stop()
          .css('backgroundColor', '#fff')
          .animate({backgroundColor: '#fff'}, 1000);
        }";
      }else{
        echo "'" . $ssr['abrev'] . "' : function(event, data) {
          $('#alert')
          .text('Inactive State')
          .stop()
          .css('backgroundColor', '#fff')
          .animate({backgroundColor: '#fff'}, 1000);
        }";
      }
      $ii++;
      if($ii < $ssnr){
        echo ",";
      }
      
    }
echo "},";
     
      
echo "'click' : function(event, data) {
        window.location = 'notes.php?abrev='+data.name;
        
      }
    });
    
    
    
    $('#over-md').click(function(event){
      $('#map').usmap('trigger', 'MD', 'mouseover', event);
    });
    
    $('#out-md').click(function(event){
      $('#map').usmap('trigger', 'MD', 'mouseout', event);
    });
  });
  </script>";
                   
                   echo '<h1 style="text-align:center;">Select A State</h1>
                         <div id="alert" style="margin:auto;text-align:center;font-size:20px;color:blue;font-weight:bold;"></div>
                         <div id="map" style="margin:auto; width: 930px; height: 630px;"></div>';
                   
                 //}
                 
                 
                 
               }else{
                 
                 
              echo '<h1>Dealer Visit Information 
                    &nbsp;&nbsp; 
                    <a style="color:black;" href="#" data-toggle="modal" data-target="#dealerVisitInfo"><small style="color:blue;"><i class="fa fa-graduation-cap"></i> <i class="fa fa-video-camera"></i> Learn More About How This Works</small></a></h1>
                    <h2 style="color:rgb(122,2,2);">' . $state . '</h2>';
                 
              //Prospect Visits (Cold Call)       
              echo '<button class="btn btn-danger btn-xs" type="button" onclick="main_menu();">Back to Main Menu</button>
                    <br><br>
                    <div class="panel panel-danger">
                    <div class="panel-heading">
                      <h3 class="panel-title">
                        <a href="Javascript:toggle_height(\'prospects\');" style="border:3px solid #a94442;color:#a94442;border-radius:50%;padding:5px;width:100%;height:100%;"><i id="prospect_icon" class="fa fa-chevron-down"></i></a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Prospect List For ' . $state . ' 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#newProspect">New Prospect</button>
                      </h3>
                    </div>
                    <div id="prospect_table" class="panel-body scroll">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr class="info">
                                    <th style="font-size:18px;">Strong Possibility</th>
                                    <th style="font-size:18px;">Business Name</th>
                                    <th style="font-size:18px;">Owner/Contact Name</th>
                                    <th style="font-size:18px;">Location</th>
                                    <th style="font-size:18px;">Status</th>
                                    <th style="font-size:18px;">Last Visit</th>
                                    <th style="font-size:18px;">Initial Notes</th>
                                    <th style="font-size:18px;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>';
                 $pq = "SELECT * FROM `prospects` WHERE `inactive` != 'Yes' AND `converted` = 'No' AND `state` = '" . $state . "'";
                 $pg = mysqli_query($conn, $pq) or die($conn->error);
                 while($pr = mysqli_fetch_array($pg)){
                          echo '<tr>';
                            if($pr['strong_possibility'] == 'Yes'){
                                    echo '<td style="text-align:center;" id="icon_container_' . $pr['PID'] . '"><a style="color:black;" onclick="set_poss(\'' . $pr['PID'] . '\',\'false\');"><i class="fa fa-check-square-o" id="icon_' . $pr['PID'] . '" style="font-size:25px;" onmouseover="cOff(\'' . $pr['PID'] . '\');" onmouseout="cOn(\'' . $pr['PID'] . '\');"></i></td>';
                                }else{
                                    echo '<td style="text-align:center;" id="icon_container_' . $pr['PID'] . '"><a style="color:black;" onclick="set_poss(\'' . $pr['PID'] . '\',\'true\');"><i class="fa fa-square-o" id="icon_' . $pr['PID'] . '" style="font-size:25px;" onmouseover="cOn(\'' . $pr['PID'] . '\');" onmouseout="cOff(\'' . $pr['PID'] . '\');"></i></td>';
                                }
                              echo '<td><b>' . str_replace('-xxx-','&',$pr['bizname']) . '</b></td>
                                    <td>' . $pr['owner_name'] . '</td>
                                    <td>' . $pr['city'] . ', ' . $pr['state'] . '</td>
                                    <td style="color:rgb(41,97,146);"><strong>Visited</strong></td>';
                                    
                     //Get last Visit date
                     $lpvq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $pr['PID'] . "' ORDER BY `ID` DESC LIMIT 1";
                     $lpvg = mysqli_query($conn, $lpvq) or die($conn->error);
                     $lpvr = mysqli_fetch_array($lpvg);
                      
                          if(mysqli_num_rows($lpvg) > 0){
                            echo '<td>' . date("m/d/y", strtotime($lpvr['date'])) . '</td>';
                          }else{
                            echo '<td> - - - - - - </td>';
                          }
                                    
                              echo '<td style="max-width:300px;"><strong>' . str_replace('-xxx-','&',$pr['initial_notes']) . '</strong></td>
                                    <td>
                                          <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#coldVisitNote" onclick="load_cold(\'' . $pr['PID'] . '\');">Add Visit Note</button>
                                          <a href="https://marketforceapp.com/marketforce/view-notes.php?id=' . $pr['PID'] . '" target="_blank"><button class="btn btn-default" type="button">View Notes</button></a>
                                          <button type="button" class="btn btn-danger" onclick="remove_prospect(\'' . $pr['PID'] . '\',\'Not Interested\');">Not Interested</button>
                                          <button type="button" class="btn btn-success" onclick="remove_prospect(\'' . $pr['PID'] . '\',\'Converted\');">Converted</button>';
                              
                                  echo '
                                    </td>
                                </tr>';
                 }
                      echo '</tbody>
                        </table>
                      </div>
                    </div>
                   </div>';
                 
              
                 
              //Master Dealer Visit List
              echo '<button class="btn btn-danger btn-xs" type="button" onclick="main_menu();">Back to Main Menu</button>
                    <br><br>
                    <div class="panel panel-primary">
                    <div class="panel-heading" style="padding-top:20px;padding-bottom:20px;>
                      <h3 class="panel-title">
                        <a href="Javascript:toggle_height(\'main\');" style="border:3px solid white;color:white;border-radius:50%;padding:5px;width:100%;height:100%;"><i id="main_icon" class="fa fa-chevron-down"></i></a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Master Dealer List For ' . $state . '
                      </h3>
                    </div>
                    <div id="main_table" class="panel-body scroll">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr class="info">
                                    <th style="font-size:18px;">Top 20</th>
                                    <th style="font-size:18px;">Dealer</th>
                                    <th style="font-size:18px;">Location</th>
                                    <th style="font-size:18px;">Status</th>
                                    <th style="font-size:18px;">Last Visit</th>
                                    <th style="font-size:18px;">Actions</th>
                                    <th style="font-size:18px;">Additional Comments</th>
                                </tr>
                            </thead>
                            <tbody>';
                    $dq = "SELECT * FROM `dealers` WHERE `inactive` != 'Yes' AND `state` = '" . $state . "' ORDER BY `name` ASC";
                    $dg = mysqli_query($conn, $dq) or die($conn->error);
                    while($dr = mysqli_fetch_array($dg)){
                          echo '<tr>';
                                if($dr['top20'] == 'Yes'){
                                    echo '<td style="text-align:center;" id="icon_container_' . $dr['ID'] . '"><a style="color:black;" onclick="set_top(' . $dr['ID'] . ',\'false\');"><i class="fa fa-check-square-o" id="icon_' . $dr['ID'] . '" style="font-size:25px;" onmouseover="cOff(' . $dr['ID'] . ');" onmouseout="cOn(' . $dr['ID'] . ');"></i></a></td>';
                                }else{
                                    echo '<td style="text-align:center;" id="icon_container_' . $dr['ID'] . '"><a style="color:black;" onclick="set_top(' . $dr['ID'] . ',\'true\');"><i class="fa fa-square-o" id="icon_' . $dr['ID'] . '" style="font-size:25px;" onmouseover="cOn(' . $dr['ID'] . ');" onmouseout="cOff(' . $dr['ID'] . ');"></i></a></td>';
                                }
                              echo '<td><b>' . $dr['name'] . '</b></td>
                                    <td>' . $dr['city'] . ', ' . $dr['state'] . '</td>';
                              if($dr['status'] == 'New'){
                                echo '<td style="color:red;"><strong>New</strong></td>';
                              }elseif($dr['status'] == 'Trained'){
                                echo '<td style="color:blue;"><strong>Trained</strong></td>';
                              }elseif($dr['status'] == 'Visited'){
                                echo '<td style="color:green;"><strong>Visited</strong></td>';
                              }else{
                                echo '<td style="color:black;"><strong>' . $dr['status'] . '</strong></td>';
                              }
                     //Get last Visit date
                     $lvq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $dr['ID'] . "' AND `vtype` != 'Internal Note' ORDER BY `ID` DESC LIMIT 1";
                     $lvg = mysqli_query($conn, $lvq) or die($conn->error);
                     $lvr = mysqli_fetch_array($lvg);
                      
                          if(mysqli_num_rows($lvg) > 0){
                            echo '<td>' . date("m/d/y", strtotime($lvr['date'])) . '</td>';
                          }else{
                            echo '<td> - - - - - - </td>';
                          }
                      
                      //Action Buttons...
                        echo '<td>
                              <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#visitNoteForm" onclick="setDets(' . $dr['ID'] . ',\'' . mysqli_real_escape_string($conn,$dr['name']) . '\');">Add Visit Note</button>';
                      
                      if($_SESSION['user_id'] == '1'){
                        echo '<button class="btn btn-default" data-toggle="modal" data-target="#previousNotes" type="button" onclick="get_prev_notes(' . $dr['ID'] . ',\'' . mysqli_real_escape_string($conn, $dr['name']) . '\');">View Notes</button>';
                      }else{
                        echo '<a href="https://marketforceapp.com/marketforce/view-notes.php?id=' . $dr['ID'] . '" target="_blank"><button class="btn btn-default" type="button">View Notes</button></a>';  
                      }
                      //if($_SESSION['user_id'] == '1' || $_SESSION['user_id'] == '3'){
                                    echo '<button class="btn btn-warning" type="button" data-toggle="modal" data-target="#displayRepair" onclick="load_repair_dets(' . $dr['ID'] . ');">Display Repair</button>';
                            //  }
                                  echo '</td>
                                    <td style="color:rgb(213,19,7);">';
                  //Check if dealer has lot images uploaded to Market Force
                  $liq = "SELECT * FROM `dealer_imgs` WHERE `dealer_id` = '" . $dr['ID'] . "'";
                  $lig = mysqli_query($conn, $liq) or die($conn->error);
                  $linum = mysqli_num_rows($lig);
                  if($linum > 0){
                    $imgr = '';
                  }else{
                      $val = '';
                      switch($_SESSION['org_id']) {
                          case "832122":
                              $val = "Customer ";
                              break;
                          case "123456":
                              $val = "Client ";
                              break;
                          default:
                              $val = 'Dealer ';
                      }
                    $imgr = '*'.$val .' Needs Lot Images Taken*';
                  }
                                echo '<strong>' . $imgr . '</strong>
                                    </td>
                                </tr>';
                      
                    }
                      echo '</tbody>
                        </table>
                      </div>
                    </div>
                   </div>
                   <button class="btn btn-danger" type="button" onclick="main_menu();">Back to Main Menu</button>';
               
               }
               ?>
               
               
               </div>
               
              </div><!--//Row-->
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

  
  
  <?php include 'visit-notes/modals/visit-note-form.php'; ?>
  <?php include 'visit-notes/modals/cold-call-form.php'; ?>
  <?php include 'visit-notes/modals/new-prospect-form.php'; ?>
  <?php include 'knowledgebase/modals/dealer-visit-info.php'; ?>
  <?php include 'repairs/modals/display-repair-modal.php'; ?>
  <?php include 'visit-notes/modals/previous-notes-modal.php'; ?>
  
    <!-- jQuery -->
  <?php
  if($state){
    echo '<script src="js/jquery.js"></script>';
  }
  ?>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

  
   

 <?php 
  if($_GET['state']){
  include 'footer.html'; 
  }
  ?>
</body>

</html>