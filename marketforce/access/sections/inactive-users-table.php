<?php
          if($_SESSION['admin'] == 'Yes'){
							echo '<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-danger">
										<div class="panel-heading">
											<h3 class="panel-title">
												Manage User Settings
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newUserModal"><i class="fa fa-plus"></i> Add User</button>
											</h3>
										</div>
										<div class="panel-body" id="scroll">
											<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Position</th>
													<th>Last Login</th>
													<th>Admin</th>
													<th>Manager</th>
													<th>Employee Status</th>
                          <th>Actions</th>
												</tr>
												</thead>
												<tbody>';
            
								if($_SESSION['user_id'] == '1'){
                  $q = "SELECT * FROM `users` WHERE `inactive` = 'Yes' ORDER BY `fname` ASC";
                }else{
								  $q = "SELECT * FROM `users` WHERE `position` != 'Developer' AND `inactive` = 'Yes' ORDER BY `fname` ASC";
                }
								$g = mysqli_query($conn, $q) or die($conn->error);
								while($r = mysqli_fetch_array($g)){
									if($r['inactive'] == 'Yes'){
                    $style = 'style="background:grey;"';
                  }else{
                    $style = '';
                  }
									
									echo '<tr ' . $style . '>
												<td><b>' . $r['fname'] . ' ' . $r['lname'] . '</b></td>
												<td>' . $r['position'] . '</td>
												<td>';
                  if($r['last_login'] == '0000-00-00'){
                    echo 'Never</td>';
                  }else{
                    echo date("m/d/Y",strtotime($r['last_login'])) . '</td>';
                  }
									echo '<td>' . $r['admin'] . '</td>';
                  echo '<td>' . $r['manager'] . '</td>';
                  if($r['inactive'] == 'Yes'){
                    echo '<td><b style="color:red;">INACTIVE</b></td>';
                  }else{
                    echo '<td><b style="color:green;">ACTIVE</b></td>';
                  }
                  echo '<td>';
                  echo '<button type="button" style="margin-right:10px;" class="btn btn-warning btn-md" data-toggle="modal" data-target="#userSettingsModal" onclick="get_data(' . $r['ID'] . ');"><i class="fa fa-lock"></i> Edit Access</button>';
                  if($r['inactive'] == 'Yes'){
                    echo '<button type="button" style="margin-right:10px;" class="btn btn-success btn-md" onclick="activate_user(' . $r['ID'] . ',1);"><i class="fa fa-check"></i> Re-Active</button>';
                  }else{
                    echo '<button type="button" style="margin-right:10px;" class="btn btn-danger btn-md" onclick="deactivate_user(' . $r['ID'] . ',0);"><i class="fa fa-close"></i> Deactivate</button>';
                  }
                  echo '</tr>';
									
								}
												
							echo '</tbody>
										</table>
                    <div style="text-align:center;">';
            if($_SESSION['admin'] == 'Yes'){
              echo '<input type="hidden" id="admin" name="admin" value="Yes" />';
            }else{
              echo '<input type="hidden" id="admin" name="admin" value="No" />';
            }
                    
              echo '<!--<input type="submit" value="Save Settings" style="background-color:green;color:white;font-weight:bold;" />-->
                    </div>
										</div>
										</div>
									</div>
								</div>
							</div><!--End Row-->';
							}
              
              ?>