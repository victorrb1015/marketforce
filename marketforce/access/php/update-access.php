<?php
include '../../php/connection.php';

$error = false;

//Load Variables...
$uid = $_POST['uid'];
if($uid == '' || $uid <= 0){
  $x->status = 'ERROR';
  $x->response = 'User ID not recognized...';
  $error = true;
}
$admin = $_POST['admin'];
$man = $_POST['manager'];
$ostr = $_POST['ostr_sup'];
$ec = $_POST['EC'];
$acc = $_POST['accountant'];
$cr = $_POST['check_request'];
$doc = $_POST['doc_approval'];
$survey = $_POST['send_survey'];
$pre = $_POST['preowned'];
$da = $_POST['dealer_activation'];
$coll = $_POST['collections'];
$coll2 = $_POST['collections2'];
$rep = $_POST['repairs'];
$rep_sub = $_POST['repairs_submit'];
$repos = $_POST['repos'];
$orders = $_POST['orders'];
$inventory = $_POST['inventory'];


//UPDATE USER ACCESS...
$q = "UPDATE `users` SET 
      `manager` = '" . $man . "',
      `ostr_supervisor` = '" . $ostr . "',
      `EC` = '" . $ec . "',
      `accountant` = '" . $acc . "',
      `check_request` = '" . $cr . "',
      `doc_approval` = '" . $doc . "',
      `send_survey` = '" . $survey . "',
      `preowned` = '" . $pre . "',
      `dealer_activation` = '" . $da . "',
      `collections` = '" . $coll . "',
      `collections2` = '" . $coll2 . "',
      `repairs` = '" . $rep . "',
      `repairs_submit` = '" . $rep_sub . "',
      `repos` = '" . $repos . "',
      `orders` = '" . $orders . "',
      `inventory` = '" . $inventory . "'
      WHERE `ID` = '" . $uid . "'";
if($error == false){
  mysqli_query($conn, $q) or die($conn->error);
  $x->status = 'GOOD';
  $x->response = 'This users access has been updated. The user will need to log out and then log back in for these changes to take effect.';
}

$response = json_encode($x);
echo $response;
?>
