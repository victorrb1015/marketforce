function get_data(uid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      document.getElementById("user_name").innerHTML = r.name;
      document.getElementById('uid').value = uid;
      
      //Load Settings
      /*if(r.admin === 'Yes'){
        document.getElementById('admin_stat').checked = true;
      }else{
        document.getElementById('admin_stat').checked = false;
      }*/
      if(r.manager === 'Yes'){
        document.getElementById('manager_stat').checked = true;
      }else{
        document.getElementById('manager_stat').checked = false;
      }
      if(r.ostr_sup === 'Yes'){
        document.getElementById('ostr_sup_stat').checked = true;
      }else{
        document.getElementById('ostr_sup_stat').checked = false;
      }
      if(r.EC === 'Yes'){
        document.getElementById('EC_stat').checked = true;
      }else{
        document.getElementById('EC_stat').checked = false;
      }
      if(r.accountant === 'Yes'){
        document.getElementById('accountant_stat').checked = true;
      }else{
        document.getElementById('accountant_stat').checked = false;
      }
      if(r.check_request === 'Yes'){
        document.getElementById('check_request_stat').checked = true;
      }else{
        document.getElementById('check_request_stat').checked = false;
      }
      if(r.doc_approval === 'Yes'){
        document.getElementById('doc_approval_stat').checked = true;
      }else{
        document.getElementById('doc_approval_stat').checked = false;
      }
      if(r.send_surveys === 'Yes'){
        document.getElementById('send_surveys_stat').checked = true;
      }else{
        document.getElementById('send_surveys_stat').checked = false;
      }
      if(r.preowned === 'Yes'){
        document.getElementById('preowned_stat').checked = true;
      }else{
        document.getElementById('preowned_stat').checked = false;
      }
      if(r.dealer_activation === 'Yes'){
        document.getElementById('dealer_activation_stat').checked = true;
      }else{
        document.getElementById('dealer_activation_stat').checked = false;
      }
      if(r.collections === 'Yes'){
        document.getElementById('collections_stat').checked = true;
      }else{
        document.getElementById('collections_stat').checked = false;
      }
      if(r.collections2 === 'Yes'){
        document.getElementById('collections2_stat').checked = true;
      }else{
        document.getElementById('collections2_stat').checked = false;
      }
      if(r.repairs === 'Yes'){
        document.getElementById('repairs_stat').checked = true;
      }else{
        document.getElementById('repairs_stat').checked = false;
      }
      if(r.repairs_submit === 'Yes'){
        document.getElementById('repairs_submit_stat').checked = true;
      }else{
        document.getElementById('repairs_submit_stat').checked = false;
      }
      if(r.repos === 'Yes'){
        document.getElementById('repos_stat').checked = true;
      }else{
        document.getElementById('repos_stat').checked = false;
      }
      if(r.orders === 'Yes'){
        document.getElementById('orders_stat').checked = true;
      }else{
        document.getElementById('orders_stat').checked = false;
      }
      if(r.inventory === 'Yes'){
        document.getElementById('inventory_stat').checked = true;
      }else{
        document.getElementById('inventory_stat').checked = false;
      }
      
      console.log('Access has been loaded...');
    }
  }
  xmlhttp.open("GET","access/php/get-user-access.php?uid="+uid,true);
  xmlhttp.send();
}


function save_settings(){
  var params = '';
  
  var uid = document.getElementById('uid').value;
  params += 'uid='+uid;
  
  //if(document.getElementById('admin_stat').checked){var admin = 'Yes';}else{var admin = 'No';}
  //params += '&admin='+admin;//Turned off until proper security has been set...
  if(document.getElementById('manager_stat').checked){var manager = 'Yes';}else{var manager = 'No';}
  params += '&manager='+manager;
  if(document.getElementById('ostr_sup_stat').checked){var ostr_sup = 'Yes';}else{var ostr_sup = 'No';}
  params += '&ostr_sup='+ostr_sup;
  if(document.getElementById('EC_stat').checked){var EC = 'Yes';}else{var EC = 'No';}
  params += '&EC='+EC;
  if(document.getElementById('accountant_stat').checked){var accountant = 'Yes';}else{var accountant = 'No';}
  params += '&accountant='+accountant;
  if(document.getElementById('check_request_stat').checked){var check_request = 'Yes';}else{var check_request = 'No';}
  params += '&check_request='+check_request;
  if(document.getElementById('doc_approval_stat').checked){var doc_approval = 'Yes';}else{var doc_approval = 'No';}
  params += '&doc_approval='+doc_approval;
  if(document.getElementById('send_surveys_stat').checked){var send_survey = 'Yes';}else{var send_survey = 'No';}
  params += '&send_survey='+send_survey;
  if(document.getElementById('preowned_stat').checked){var preowned = 'Yes';}else{var preowned = 'No';}
  params += '&preowned='+preowned;
  if(document.getElementById('dealer_activation_stat').checked){var dealer_activation = 'Yes';}else{var dealer_activation = 'No';}
  params += '&dealer_activation='+dealer_activation;
  if(document.getElementById('collections_stat').checked){var collections = 'Yes';}else{var collections = 'No';}
  params += '&collections='+collections;
  if(document.getElementById('collections2_stat').checked){var collections2 = 'Yes';}else{var collections2 = 'No';}
  params += '&collections2='+collections2;
  if(document.getElementById('repairs_stat').checked){var repairs = 'Yes';}else{var repairs = 'No';}
  params += '&repairs='+repairs;
  if(document.getElementById('repairs_submit_stat').checked){var repairs_submit = 'Yes';}else{var repairs_submit = 'No';}
  params += '&repairs_submit='+repairs_submit;
  if(document.getElementById('repos_stat').checked){var repos = 'Yes';}else{var repos = 'No';}
  params += '&repos='+repos;
  if(document.getElementById('orders_stat').checked){var orders = 'Yes';}else{var orders = 'No';}
  params += '&orders='+orders;
  if(document.getElementById('inventory_stat').checked){var inventory = 'Yes';}else{var inventory = 'No';}
  params += '&inventory='+inventory;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      if(r.status === 'GOOD' || r.status === 'ERROR'){
        alert(r.response);
      }else{
        alert('There was an error processing your request!');
      }
      window.location.reload();
      
    }
  }
  xmlhttp.open("POST","access/php/update-access.php",true);
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  xmlhttp.send(params);
}



function activate_user(uid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      if(r.status === 'GOOD' || r.status === 'ERROR'){
        alert(r.response);
      }else{
        alert('There was an error processing your request!');
      }
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","access/php/toggle-user.php?uid="+uid+"&mode=Activate",true);
  xmlhttp.send();
}


function deactivate_user(uid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      if(r.status === 'GOOD' || r.status === 'ERROR'){
        alert(r.response);
      }else{
        alert('There was an error processing your request!');
      }
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","access/php/toggle-user.php?uid="+uid+"&mode=Deactivate",true);
  xmlhttp.send();
}



function add_user(){
  var ep = document.getElementById('ep');
  ep.innerHTML = '';
  var fname = document.getElementById('fname').value;
  if(fname === ''){
    ep.innerHTML = 'Please Enter The First Name!';
    return;
  }
  var lname = document.getElementById('lname').value;
  if(lname === ''){
    ep.innerHTML = 'Please Enter The Last Name!';
    return;
  }
  var email = document.getElementById('uemail').value;
  if(email === ''){
    ep.innerHTML = 'Please Enter The Email Address!';
    return;
  }
  /*if(!email.includes('@allsteelcarports.com') && !email.includes('@acero.industries') && !email.includes('@allamericanbuildings.com')){
    ep.innerHTML = 'The Email Address Used MUST Be An @allsteelcarports.com, @acero.industries or @allamericanbuildings.com Email Address!';
    return;
  }*/
  
  var position = document.getElementById('position').value;
  if(position === ''){
    ep.innerHTML = 'Please Select The User Position!';
    return;
  }
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      if(r.status === 'GOOD' || r.status === 'ERROR'){
        alert(r.response);
      }else{
        alert('There was an error processing your request!');
      }
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","access/php/add-user.php?fname="+fname+"&lname="+lname+"&email="+email+"&position="+position,true);
  xmlhttp.send();
}