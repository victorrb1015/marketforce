			 <!-- Modal -->
  <div class="modal fade" id="userSettingsModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Market Force User Settings For: <span id="user_name" style="color:blue;"></span></h4>
        </div>
        <div class="modal-body">
          <table class="access_table">
            <!--<tr>
              <td>
                <h4>Admin Access</h4>
              </td>
              <td>
                <label title="Admin">
                  <input class="toggles" type="checkbox" value="Yes" id="admin_stat" name="admin_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>-->
            <tr>
              <td>
                <h4>Manager Access</h4>
              </td>
              <td>
                <label title="Manager">
                  <input class="toggles" type="checkbox" value="Yes" id="manager_stat" name="manager_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
						<tr>
              <td>
                <h4>OSTR Supervisor</h4>
              </td>
              <td>
                <label title="OSTR Supervisor">
                  <input class="toggles" type="checkbox" value="Yes" id="ostr_sup_stat" name="ostr_sup_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Employee Center</h4>
              </td>
              <td>
                <label title="Employee Center">
                  <input class="toggles" type="checkbox" value="Yes" id="EC_stat" name="EC_stat"  data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Accountant</h4>
              </td>
              <td>
                <label title="Accountant">
                 <input class="toggles" type="checkbox" value="Yes" id="accountant_stat" name="accountant_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Check Request</h4>
              </td>
              <td>
                <label title="Check Request">
                  <input class="toggles" type="checkbox" value="Yes" id="check_request_stat" name="check_request_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Document Approval</h4>
              </td>
              <td>
                <label title="Document Approval">
                  <input class="toggles" type="checkbox" value="Yes" id="doc_approval_stat" name="doc_approval_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Send Surveys</h4>
              </td>
              <td>
                <label title="Send Surveys">
                  <input class="toggles" type="checkbox" value="Yes" id="send_surveys_stat" name="send_surveys_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Pre-Owned Listings</h4>
              </td>
              <td>
                <label title="Pre-Owned Listings">
                  <input class="toggles" type="checkbox" value="Yes" id="preowned_stat" name="preowned_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Dealer Activation</h4>
              </td>
              <td>
                <label title="Dealer Activation">
                  <input class="toggles" type="checkbox" value="Yes" id="dealer_activation_stat" name="dealer_activation_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Collections</h4>
              </td>
              <td>
                <label title="Collections">
                  <input class="toggles" type="checkbox" value="Yes" id="collections_stat" name="collections_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Collections2</h4>
              </td>
              <td>
                <label title="Collections2">
                  <input class="toggles" type="checkbox" value="Yes" id="collections2_stat" name="collections2_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Repairs</h4>
              </td>
              <td>
                <label title="Repairs">
                  <input class="toggles" type="checkbox" value="Yes" id="repairs_stat" name="repairs_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Repairs Submit</h4>
              </td>
              <td>
                <label title="Repairs Submit">
                  <input class="toggles" type="checkbox" value="Yes" id="repairs_submit_stat" name="repairs_submit_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>repos</h4>
              </td>
              <td>
                <label title="repos">
                  <input class="toggles" type="checkbox" value="Yes" id="repos_stat" name="repos_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Orders</h4>
              </td>
              <td>
                <label title="Orders">
                  <input class="toggles" type="checkbox" value="Yes" id="orders_stat" name="orders_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <h4>Inventory</h4>
              </td>
              <td>
                <label title="Inventory">
                  <input class="toggles" type="checkbox" value="Yes" id="inventory_stat" name="inventory_stat" data-toggle="toggle">
                </label>
              </td>
            </tr>
          </table>
            
          <input type="hidden" value="" id="uid" name="uid" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success" onclick="save_settings();">Save</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->