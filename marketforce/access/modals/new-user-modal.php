			 <!-- Modal -->
  <div class="modal fade" id="newUserModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New MarketForce User Form</h4>
        </div>
        <div class="modal-body">
          
          <table class="access_table">
            <tr>
              <td><h4>First Name</h4></td>
              <td><input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" /></td>
            </tr>
            <tr>
              <td><h4>Last Name</h4></td>
              <td><input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" /></td>
            </tr>
            <tr>
              <td><h4>Email</h4></td>
              <td><input type="text" class="form-control" id="uemail" name="uemail" placeholder="Email" /></td>
            </tr>
            <tr>
              <td><h4>Position</h4></td>
              <td>
                <select class="form-control" id="position" name="position">
                  <option value="Office Employee">Office Employee</option>
                  <option value="Outside Sales Rep">Outside Sales Rep</option>
                  <option value="Inventory Consignment User">Inventory Consignment User</option>
                </select>
              </td>
            </tr>
          </table>
          <p id="ep" style="text-align:center;color:red;font-weight:bold;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success" onclick="add_user();">Add</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->