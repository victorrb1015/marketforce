<?php
include 'security/session/session-settings.php';
include 'php/connection.php';

$pageName = 'Manufacturing';
$pageIcon = 'fas fa-city';

$cache_code = uniqid();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>MarketForce | All Steel</title>
    <?php include 'global/sections/head.php'; ?>
  <!--<script src="//labelwriter.com/software/dls/sdk/js/DYMO.Label.Framework.latest.js" type="text/javascript" charset="UTF-8"></script>-->
  <script src="inventory/js/DYMO.Label.Framework.Latest.js" type="text/javascript" charset="UTF-8"></script>
</head>

<body>
	<!-- Preloader -->
	<?php include 'global/sections/preloader.php'; ?>
	<!-- /Preloader -->
    <div class="wrapper theme-4-active pimary-color-red">

    	<!--Navigation-->
    	<?php include 'global/sections/nav.php'; ?>
		
		
        <!-- Main Content -->
		<div class="page-wrapper"><!--Includes Footer-->

      <div class="container-fluid pt-25">
				<?php include 'global/sections/page-title-bar.php'; ?>
        
        <!--Main Content Here-->
          
		<!--Manufacturing Header-->
		<div class="col-lg-8 col-md-4 col-sm-4 col-xs-12">
		  <?php include 'inventory-manufacturing/sections/section-head.php'; ?>
		<!--Panel Tiles-->
			<?php include 'inventory-manufacturing/sections/process-tiles.php'; ?>
		</div>
				
		<!--Pending Process-->
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<?php include 'inventory-manufacturing/sections/pending-processes.php'; ?>
		</div>

					
		
    <!-- Modals -->
    <?php include 'inventory-manufacturing/modals/add-process-modal.php'; ?>
    <?php include 'inventory-manufacturing/modals/process-options-modal.php'; ?>
			
		<!-- Footer -->
		<?php include 'global/sections/footer.php'; ?>
		<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!--Footer-->
	<?php include 'global/sections/includes.php'; ?>
  <script src="inventory-manufacturing/js/process-tile-functions.js?cb=<?php echo $cache_code; ?>"></script>
  <script src="inventory-manufacturing/js/process-build-functions.js?cb=<?php echo $cache_code; ?>"></script>
  <script src="inventory-manufacturing/js/process-list-functions.js?cb=<?php echo $cache_code; ?>"></script>
  <script src="inventory-manufacturing/js/barcode-print-functions.js?cb=<?php echo $cache_code; ?>"></script>
  <script src="inventory-manufacturing/js/process-option-functions.js?cb=<?php echo $cache_code; ?>"></script>
</body>

</html>
