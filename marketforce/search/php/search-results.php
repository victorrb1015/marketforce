<?php
include '../../php/connection.php';

//Load Variables...
$str = mysqli_real_escape_string($conn,$_GET['str']);

$r_array = array();
$i = 0;
$xi = 0;

//Get Search Results...
echo '<div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                	<th>Record Type</th>
                    <th>Customer Name</th>
                    <th>Invoice Number</th>
                    <th>Phone</th>
                    <th>Date</th>
                    <th>Location</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>';


//Search SalesPortal...
  $sc = new SoapClient("http://sales.allsteelcarports.com/publicapi/allsteelwebservice.asmx?WSDL");
  $apiKey = "33f2ddee-724e-4bd1-9606-4408f5f95bdd";
  if(is_numeric($str)){
    $invoice = $str;

    //SoapClient Parameters for the CSKern Web Service API
    $params = array(
      "apiKey" => $apiKey,
      "invoice" => $invoice
    );
    $result = $sc->__soapCall("GetOrdersByInvoice", array($params));
    $rx = json_decode($result->GetOrdersByInvoiceResult);

}else{

    $customer = $str;

    //SoapClient Parameters for the CSKern Web Service API
    $params = array(
      "apiKey" => $apiKey,
      "customer" => $customer
    );
    $result = $sc->__soapCall("GetOrdersByCustomer", array($params));
    $rx = json_decode($result->GetOrdersByCustomerResult);

}

   foreach($rx as $x){
        $a[$i]->ID = $x->orderID;
        $a[$i]->type = 'Orders';
        $a[$i]->name = $x->customer;
        $a[$i]->inv = $x->orderID;
        $a[$i]->phone = $x->customerPhone1;
        $a[$i]->date = $x->invoiceDate;
        $a[$i]->state = $x->buildingState;
        $a[$i]->status = $x->orderStatus;
        if($x->orderStatus == 'In Shop' || $x->orderStatus == 'Installing'){
            $bg_style = 'blue';
        }elseif($x->orderStatus == 'Installed'){
            $bg_style = 'green';
        }else{
            $bg_style = 'red';
        }
        $a[$i]->bg_style = $bg_style;
        $i++;
    }


//Search Collections...
$cq = "SELECT 
		*
		FROM `collections` 
		WHERE 
		`name` LIKE '%" . $str . "%' 
		OR 
		`inv#` LIKE '%" . $str . "%' 
		OR 
		REPLACE(REPLACE(REPLACE(REPLACE(`phone`,'(',''),')',''),'-',''),' ','') LIKE '%" . $str . "%'
		LIMIT 10";

$cg = mysqli_query($conn, $cq) or die($conn->error);

while($cr = mysqli_fetch_array($cg)){
                    $bg_style = 'green';
                    if($cr['paid'] == 'Yes'){
                        $s1 = 'PAID';
                    }else{
                        $s1 = 'NOT PAID';
                        $bg_style = 'red';
                    }
                    if($cr['inactive'] == 'Yes'){
                        $s2 = 'INACTIVE';
                        $bg_style = 'red';
                    }else{
                        $s2 = 'ACTIVE';
                    }
	/*echo '<tr>
					<td><a href="collections/customer-report.php?cid=' . $cr['ID'] . '" target="_blank" style="color:red;"><b>Collections</b></a></td>
                    <td>' . $cr['name'] . '</td>
                    <td>' . $cr['inv#'] . '</td>
                    <td>' . $cr['phone'] . '</td>
                    <td>' . date("m/d/y",strtotime($cr['date'])) . '</td>
                    <td>' . $cr['state'] . '</td>
                    <td>
                    <span style="color:' . $bg_style . ';font-weight:bold;">' . $s1 . ' - ' . $s2 . '</span>
                    </td>
                </tr>';*/
                $a[$i]->ID = $cr['ID'];
                $a[$i]->type = 'Collections';
                $a[$i]->name = $cr['name'];
                $a[$i]->inv = $cr['inv#'];
                $a[$i]->phone = $cr['phone'];
                $a[$i]->date = $cr['date'];
                $a[$i]->state = $cr['state'];
                $a[$i]->status = $s1 . ' - ' . $s2;
                $a[$i]->bg_style = $bg_style;
                $a[$i]->s1 = $s1;
                $a[$i]->s2 = $s2;
                //array_push($r_array, $a);
                $i++;
}


//Check Repairs...
$rq = "SELECT 
		*
		FROM `repairs` 
		WHERE 
		`cname` LIKE '%" . $str . "%' 
		OR 
		`inv` LIKE '%" . $str . "%' 
		OR 
		REPLACE(REPLACE(REPLACE(REPLACE(`cphone`,'(',''),')',''),'-',''),' ','') LIKE '%" . $str . "%'
		LIMIT 10";

$rg = mysqli_query($conn, $rq) or die($conn->error);

while($rr = mysqli_fetch_array($rg)){
    $bg_style = 'green';
                    $s1 = $rr['status'];
                    if($rr['inactive'] == 'Yes'){
                        $s2 = 'INACTIVE';
                        $bg_style = 'red';
                    }else{
                        $s2 = 'ACTIVE';
                    }
	/*echo '<tr>
					<td><a href="repairs.php?rid=' . $rr['ID'] . '" target="_blank" style="color:blue;"><b>Repairs</b></a></td>
                    <td>' . $rr['cname'] . '</td>
                    <td>' . $rr['inv'] . '</td>
                    <td>' . $rr['cphone'] . '</td>
                    <td>' . date("m/d/y",strtotime($rr['today_date'])) . '</td>
                    <td>' . $rr['state'] . '</td>
                    <td>
                    <span style="color:' . $bg_style . ';font-weight:bold;">' . $s1 . ' - ' . $s2 . '</span>
                    </td>
                </tr>';*/
                $a[$i]->ID = $rr['ID'];
                $a[$i]->type = 'Repairs';
                $a[$i]->name = $rr['cname'];
                $a[$i]->inv = $rr['inv'];
                $a[$i]->phone = $rr['cphone'];
                $a[$i]->date = $rr['today_date'];
                $a[$i]->state = $rr['state'];
                $a[$i]->status = $s1 . ' - ' . $s2;
                $a[$i]->bg_style = $bg_style;
                $a[$i]->s2 = $s2;
                $i++;
}


//sort($r_array);
//print_r($a);
$count = count($a);
asort($a);
for($xi = 0; $xi < $count; $xi++) {

            $x = $a[$xi];
            $bg_style = $x->bg_style;
            echo '<tr>
					<td>';
            if($x->type == 'Collections'){
                echo '<a href="collections/customer-report.php?cid=' . $x->ID . '" target="_blank" style="color:blue;"><b>' . $x->type . '</b></a>';
            }elseif($x->type == 'Repairs'){
                if($x->s2 == 'ACTIVE'){
                    echo '<a href="repairs.php" target="_blank" style="color:blue;"><b>' . $x->type . '</b></a>';
                }else{
                    echo '<a href="reports/repairs/view-inactive-repairs.php" target="_blank" style="color:blue;"><b>' . $x->type . '</b></a>';
                }
            }elseif($x->type == 'Orders'){
                echo '<a href="scheduling/beta-order-invoice.php?inv=' . $x->ID . '" target="_blank" style="color:blue;"><b>' . $x->type . '</b></a>';
            }else{
                echo '<a style="color:grey;"><b>' . $x->type . '</b></a>';
            }

              echo '</td>
                    <td>' . $x->name . '</td>
                    <td>' . $x->inv . '</td>
                    <td>' . $x->phone . '</td>
                    <td>' . date("m/d/y",strtotime($x->today_date)) . '</td>
                    <td>' . $x->state . '</td>
                    <td>
                        <span style="color:' . $bg_style . ';font-weight:bold;">' . $x->status . '</span>
                    </td>
                </tr>';

}

echo '</tbody>
        </table>
    </div>';

//print_r($r_array);


?>