<?php
include 'security/session/session-settings.php';
include 'php/connection.php';
$pageName = ' Open Support Tickets';
$pageIcon = 'fas fa-file';

$cache_buster = uniqid();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $pageName; ?> | MarketForce</title>
    <?php include 'global/sections/head.php'; ?>
</head>

<body>
	<!-- Preloader -->
	<?php include 'global/sections/preloader.php'; ?>
	<!-- /Preloader -->
    <div class="wrapper theme-4-active pimary-color-red">

    	<!--Navigation-->
    	<?php include 'global/sections/nav.php'; ?>
		
		
        <!-- Main Content -->
		<div class="page-wrapper"><!--Includes Footer-->

      <div class="container-fluid pt-25"><!--Main Content Here-->
				<?php include 'global/sections/page-title-bar.php'; ?>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <?php include 'support/sections/tickets-display.php'; ?>
        </div>
        
			</div>
			
			
			<!-- Footer -->
			<?php include 'global/sections/footer.php'; ?>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
  <?php include 'support/modals/view-ticket-modal.php'; ?>
	
	<!--Footer-->
	<?php include 'global/sections/includes.php'; ?>
  <!--<script src="support/js/jira-ticket-functions.js?cb=<?php echo $cache_buster; ?>"></script>-->
  <script src="support/js/teamwork-ticket-functions.js?cb=<?php echo $cache_buster; ?>"></script>
</body>

</html>
