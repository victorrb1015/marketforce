<?php
include 'security/session/session-settings.php';
include 'php/connection.php';
$pageName = 'Orders';
$pageIcon = 'fas fa-file';

if($_SESSION['in'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://' . $_SERVER['HTTP_HOST'] . '";
				}, 2000);
				</script>';
	return;
}

//Cache Buster...
$cache_code = uniqid();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $pageName; ?> - Market Force | All Steel</title>
  <?php include 'global/sections/head.php'; ?>
  <!--Form Steps Bootstrap CSS-->
  <link href="orders/css/form-steps.css" rel="stylesheet" type="text/css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
    .dataTables_filter {
       float: left !important;
    }
  </style>
</head>

<body>
	<!-- Preloader -->
	<?php include 'global/sections/preloader.php'; ?>
	<!-- /Preloader -->
    <div class="wrapper theme-4-active pimary-color-red">

    	<!--Navigation-->
       <?php 
        if($_SESSION['position'] == 'Shop Employee'){
          include 'shop-nav.php';
        }else{
          include 'global/sections/nav.php';
        }
       ?>
		
		
        <!-- Main Content -->
		<div class="page-wrapper"><!--Includes Footer-->

      <div class="container-fluid pt-25"><!--Main Content Here-->
				<?php include 'global/sections/page-title-bar.php'; ?>

      <?php
         $orders_aria = 'aria-expanded="false"';
         $customers_aria = 'aria-expanded="false"';
         $products_aria = 'aria-expanded="false"';
         $financing_aria = 'aria-expanded="false"';
         $reports_aria = 'aria-expanded="false"';
         $portal_users_aria = 'aria-expanded="false"';
         switch ($_GET['tab']){
           case 'orders':
             $orders_tab = 'active';
             $orders_section = 'active in';
             $orders_aria = 'aria-expanded="true"';
             break;
           case 'customers':
             $customers_tab = 'active';
             $customers_section = 'active in';
             $customers_aria = 'aria-expanded="true"';
             break;
           case 'products':
             $products_tab = 'active';
             $products_section = 'active in';
             $products_aria = 'aria-expanded="true"';
             break;
           case 'financing':
             $financing_tab = 'active';
             $financing_section = 'active in';
             $financing_aria = 'aria-expanded="true"';
             break;
           case 'reports':
             $reports_tab = 'active';
             $reports_section = 'active in';
             $reports_aria = 'aria-expanded="true"';
             break;
           case 'portal_users':
             $portal_users_tab = 'active';
             $portal_users_section = 'active in';
             $portal_users_aria = 'aria-expanded="true"';
             break;
           case 'portal_docs':
             $portal_docs_tab = 'active';
             $portal_docs_section = 'active in';
             $portal_docs_aria = 'aria-expanded="true"';
             break;
           case 'vendors':
             $vendors_tab = 'active';
             $vendors_section = 'active in';
             $vendors_aria = 'aria-expanded="true"';
             break;
           default:
             $orders_tab = 'active';
             $orders_section = 'active in';
             $orders_aria = 'aria-expanded="true"';
         }
       ?>
         
      <ul class="nav nav-tabs">
          <li class="nav-item <?php echo $orders_tab; ?>"><a data-toggle="tab" href="#orders" style="color:#337ab7;" onclick="add_url_param('tab','orders');" <?php echo $orders_aria; ?>><span class="glyphicon glyphicon-shopping-cart"></span>  Orders</a></li>
          <li class="nav-item <?php echo $vendors_tab; ?>"><a data-toggle="tab" href="#vendors" style="color:#337ab7;" onclick="add_url_param('tab','vendors');" <?php echo $vendors_aria; ?>><span class="glyphicon glyphicon-book"></span> Vendors</a></li>
          <li class="nav-item <?php echo $customers_tab; ?>"><a data-toggle="tab" href="#customers" style="color:#337ab7;" onclick="add_url_param('tab','customers');" <?php echo $customers_aria; ?>><span class="glyphicon glyphicon-user"></span> Customers</a></li>
          <li class="nav-item <?php echo $financing_tab; ?>"><a data-toggle="tab" href="#financial" style="color:#337ab7;" onclick="add_url_param('tab','financial');" <?php echo $financing_aria; ?>><span class="glyphicon glyphicon-piggy-bank"></span> Financing</a></li>
          <li class="nav-item <?php echo $reports_tab; ?>"><a data-toggle="tab" href="#reports" style="color:#337ab7;" onclick="add_url_param('tab','reports');" <?php echo $reports_aria; ?>><span class="glyphicon glyphicon-list-alt"></span> Reports</a></li>
          <li class="nav-item <?php echo $portal_users_tab; ?>"><a data-toggle="tab" href="#portal_users" style="color:#337ab7;" onclick="add_url_param('tab','portal_users');" <?php echo $portal_users_aria; ?>><i class="fa fa-group"></i> Portal Users</a></li>
          <li class="nav-item <?php echo $portal_docs_tab; ?>"><a data-toggle="tab" href="#portal_docs" style="color:#337ab7;" onclick="add_url_param('tab','portal_docs');" <?php echo $portal_docs_aria; ?>><i class="fa fa-file"></i> Portal Docs</a></li>
      </ul>
      
      <div class="tab-content" id="myTabContent">

        
        <div class="tab-pane fade <?php echo $orders_section; ?>" id="orders" role="tabpanel"><!-----------------------------START ORDERS TAB CONTENT----------------------------->
          <div class="tab-content" id="mySubTabContent">
          <ul class="nav nav-tabs">
            <li class="nav-item active"><a data-toggle="tab" href="#est" style="color:#337ab7;"><span class="glyphicon glyphicon-shopping-cart"></span> Pending Orders</a></li>
            <li class="nav-item"><a data-toggle="tab" href="#invoiced" style="color:#337ab7;"><span class="glyphicon glyphicon-user"></span> Invoiced Orders</a></li>
            <li class="nav-item"><a data-toggle="tab" href="#paid" style="color:#337ab7;"><span class="glyphicon glyphicon-barcode"></span> Paid Orders</a></li>
            <li class="nav-item"><a data-toggle="tab" href="#out_time" style="color:#337ab7;"><span class="glyphicon glyphicon-time"></span> Out of Time</a></li>
            <li class="nav-item"><a data-toggle="tab" href="#ser" style="color:#337ab7;"><span class="glyphicon glyphicon-search"></span> Search Orders</a></li>
          </ul>

          <div class="tab-pane active in" id="est" role="tabpanel">
          <?php
              //Add Order Tracking Sections Here...
              include 'orders/sections/orders-display-table.php';
           ?>
         </div>
         <div class="tab-pane fade" id="invoiced" role="tabpanel">
          <?php
            include 'orders/sections/invoiced-orders-display-table.php';
          ?>
          </div>
         <div class="tab-pane fade" id="paid" role="tabpanel">
          <?php
            include 'orders/sections/paid-orders-display-table.php';
          ?>
          </div>
          <div class="tab-pane fade" id="out_time" role="tabpanel">
              <?php
              include 'orders/sections/out-time-orders-display-table.php';
              ?>
          </div>
          <div class="tab-pane fade" id="ser" role="tabpanel">
          <?php
            include 'orders/sections/search-display-table.php';
          ?>
          </div>

        </div>
          
        </div><!-----------------------------END ORDERS TAB CONTENT----------------------------->
        
        
          <!-----------------------------START CUSTOMERS TAB CONTENT----------------------------->
        <div class="tab-pane fade <?php echo $customers_section; ?>" id="customers" role="tabpanel">
          <?php
             //Add customer display table
             include 'orders/sections/customer-display-table.php';
           ?>
          
        </div><!-----------------------------END CUSTOMERS TAB CONTENT----------------------------->
        
        <!-----------------------------START VENDORS TAB CONTENT----------------------------->
        <div class="tab-pane fade <?php echo $vendors_section; ?>" id="vendors" role="tabpanel">
          <?php
             //Add vendors display table
             include 'orders/sections/vendors-display-table.php';
           ?>
          
        </div><!-----------------------------END VENDORS TAB CONTENT----------------------------->
        
        <!-----------------------------START PRODUCTS TAB CONTENT----------------------------->
        <!--<div class="tab-pane fade <?php echo $products_section; ?>" id="products" role="tabpanel">
          <?php
             //Add product display table
             include 'orders/sections/product-display-table.php';
           ?>
        </div>--><!-----------------------------END PRODUCTS TAB CONTENT----------------------------->
        
        <div class="tab-pane fade <?php echo $financing_section; ?>" id="financial" role="tabpanel"><!-----------------------------START FINANCING TAB CONTENT----------------------------->
          <?php
             //Add credit line display table
             include 'orders/sections/credit-line-display-table.php';
           ?>
        </div><!-----------------------------END FINANCING TAB CONTENT----------------------------->
        
        <div class="tab-pane fade <?php echo $reports_section; ?>" id="reports" role="tabpanel"><!-----------------------------START FINANCING TAB CONTENT----------------------------->
          <?php
             //Add credit line display table
             include 'orders/sections/reports-display-table.php';
           ?>
        </div><!-----------------------------END REPORTS TAB CONTENT----------------------------->
        
        <div class="tab-pane fade <?php echo $portal_users_section; ?>" id="portal_users" role="tabpanel"><!-----------------------------START FINANCING TAB CONTENT----------------------------->
          <?php
             //Add Portal User display table
             include 'orders/sections/portal-users-display-table.php';
           ?>
        </div><!-----------------------------END REPORTS TAB CONTENT----------------------------->
        
        <div class="tab-pane fade <?php echo $portal_docs_section; ?>" id="portal_docs" role="tabpanel"><!-----------------------------START FINANCING TAB CONTENT----------------------------->
          <?php
             //Add Portal Documents display table
             include 'orders/sections/portal-documents-table.php';
           ?>
        </div><!-----------------------------END REPORTS TAB CONTENT----------------------------->
        
    </div>
        
			</div>
			
  <script>
    $('#newOrderForm').submit(function(){
      document.getElementById('newSubBTN').disabled = true;
    });
  </script>
  
      <script src="orders/js/order-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="orders/js/customer-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="orders/js/product-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="orders/js/credit-line-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="orders/js/special-pricing-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="orders/js/email-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="orders/js/color-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="orders/js/ship-address-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="orders/js/portal-user-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="orders/js/portal-doc-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="orders/js/export-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="orders/js/vendor-functions.js?cb=<?php echo $cache_code; ?>"></script>
  

  <!----------------------------------------------------------Modals Go Here------------------------------------------------>
	<?php include 'orders/modals/add-note-modal.php'; ?>
  <?php include 'orders/modals/new-order-modal.php'; ?>
  <?php include 'orders/modals/info-box-modal.php'; ?>
  <?php include 'orders/modals/new-credit-line-modal.php'; ?>
  <?php include 'orders/modals/new-product-modal.php'; ?>
  <?php include 'orders/modals/new-special-pricing-modal.php'; ?>
  <?php include 'orders/modals/new-customer-modal.php'; ?>
  <?php include 'orders/modals/colors-modal.php'; ?>
  <?php include 'orders/modals/ship-address-modal.php'; ?>
  <?php include 'orders/modals/add-edit-portal-user-modal.php'; ?>
  <?php include 'orders/modals/new-order-date-modal.php'; ?>
  <?php include 'orders/modals/new-inv-number-modal.php'; ?>
  <?php include 'orders/modals/add-portal-doc-modal.php'; ?>
  <?php include 'orders/modals/new-vendor-modal.php'; ?>
			
			<!-- Footer -->
			<?php include 'global/sections/footer.php'; ?>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!--Footer-->
	<?php include 'global/sections/includes.php'; ?>
</body>
  
  <!-- jQuery Datatables -->
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script>
    $("#vendors_table").dataTable({
      "paging": false,
    });
  </script>

</html>
