<?php
include 'security/session/session-settings.php';

if(!isset($_SESSION['in'])){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';

$pageName = 'Dealer Displays';//Set this equal to the name of the page you would like to set...
$pageIcon = 'fa fa-home';//Set this equal to the class name of the icon you would like to set as the page icon...

$cache_buster = uniqid();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
		th, td{
			padding: 10px;
		}
		#dl, dc{
			height: 430px;
		}
		.panel-body{
			height: 382px;
		}
		#scroll{
			overflow: scroll;
		}
		::-webkit-scrollbar {
    width: 0px;  /* remove scrollbar space */
    background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
		
		
		.packet-table td, th{
			border: 1px solid black;
		}
		.packet-table{
			margin:auto;
		}
  </style>
  <script>
    
    //Globals...
    var rep_id = '<?php echo $_SESSION['user_id']; ?>';
    var rep_name = '<?php echo $_SESSION['full_name']; ?>';
		
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
       <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> <?php switch ($_SESSION['org_id']) {
                                    case "832122":
                                        echo "Customer Displays";
                                        break;
                                    case "123456":
                                    case '532748':
                                        echo "Client Displays";
                                        break;
                                    default:
                                        echo $pageName;
                                } ?></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
														<li>
															<i class="<?php echo $pageIcon; ?>"></i><?
                                                            switch ($_SESSION['org_id']) {
                                                                case "832122":
                                                                    echo "Customer Displays";
                                                                    break;
                                                                case "123456":
                                                                case '532748':
                                                                    echo "Client Displays";
                                                                    break;
                                                                default:
                                                                    echo $pageName;
                                                            }
                                                            ?>
													</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title"><?php
                    switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }?> Displays
                  &nbsp;&nbsp;
                  <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#newDisplaySaleModal" onclick="">
                    <i class="fa fa-plus"></i> New Display
                  </button>
                </h3>
            </div>
            <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped packet-table">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th><?php switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }?></th>
                                            <th>Display Type</th>
                                            <th>Dimensions</th>
                                            <th>Display Color</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            <?php
                            $ddq = "SELECT * FROM `dealer_displays` WHERE `inactive` != 'Yes' AND `status` != 'Sold'";
                            $ddg = mysqli_query($conn, $ddq) or die($conn->error);
                            while($ddr = mysqli_fetch_array($ddg)){
                                echo '<tr>
                                            <td>' . date("m/d/y",strtotime($ddr['date'])) . '</td>
                                            <td>' . $ddr['dealer_name'] . '</td>
                                            <td>' . $ddr['display_type'] . '</td>
                                            <td>' . $ddr['length'] . 'x' . $ddr['width'] . 'x' . $ddr['height'] . '</td>
                                            <td>' . $ddr['color'] . '</td>';
                              if($ddr['sale_auth'] != 'Yes'){
                                      echo '<td><b>' . $ddr['status'] . '</b></td>';
                              }else{
                                      echo '<td><b>' . $ddr['status'] . '</b><br>$' . $ddr['sale_amount'] . '<hr>' . $ddr['notes'] . '</td>';
                              }
                              if($ddr['sale_auth'] != 'Yes' && $_SESSION['doc_approval'] == 'Yes'){
                                  echo '<td>
                                  <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#display-sale-authorization" onclick="load_display_modal(' . $ddr['ID'] . ');">Authorize Sale</button>
                                  <button class="btn btn-danger btn-sm" type="button" onclick="remove_display(' . $ddr['ID'] . ');"><i class="fa fa-times"></i> Remove</button>
                                  </td>';
                              }elseif($_SESSION['doc_approval'] == 'Yes'){
                                  echo '<td>
                                  <button class="btn btn-success btn-sm" type="button" onclick="mark_display_sold(' . $ddr['ID'] . ');"><i class="fa fa-check"></i> Mark Sold</button>
                                  <button class="btn btn-danger btn-sm" type="button" onclick="remove_display(' . $ddr['ID'] . ');"><i class="fa fa-times"></i> Remove</button>
                                  </td>';
                              }
                                       echo '</tr>';
                            }
                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>		
    </div>					
							
							
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!--<script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>-->

	<script src="dealer-displays/js/displays-functions.js?cb=<?php echo $cache_buster; ?>"></script>
    <?php include 'dealer-displays/modals/display-sale-authorization.php'; ?>
    <?php include 'dealer-displays/modals/new-display-sale-modal.php'; ?>
    <?php include 'footer.html'; ?>
</body>

</html>