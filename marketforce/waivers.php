<?php
include 'security/session/session-settings.php';

if(!isset($_SESSION['in'])){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
		th, td{
			padding: 10px;
		}
		#dl, dc{
			height: 430px;
		}
		.panel-body{
			height: 382px;
		}
		#scroll{
			overflow: scroll;
		}
		::-webkit-scrollbar {
    width: 0px;  /* remove scrollbar space */
    background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
		
		
		.packet-table td, th{
			border: 1px solid black;
		}
		.packet-table{
			margin:auto;
		}
  </style>
  <script>
		
		function load_modal(id,name,mode){
			if(mode === 'Completed'){
				document.getElementById('completed_id').value = id;
				document.getElementById('ccrName').innerHTML = name;
			}
			if(mode === 'Need Info'){
				document.getElementById('info_id').value = id;
				document.getElementById('info_name').innerHTML = name;
			}
			if(mode === 'collection payment'){
				document.getElementById('approve_id').value = id;
			}
			if(mode === 'wnote'){
				document.getElementById('wnoteID').value = id;
			}
		}
		
		
		function encodeURL(url){
		url = url.replace(/&/g, '%26'); 
		url = url.replace(/#/g, '%23');
		return url;
	}
		
		
		function seen(alert,vid){
			var id = '<?php echo $_SESSION['user_id']; ?>';
			
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      $("#alert1").modal("hide");
      $("#alert2").modal("hide");
      $("#alert3").modal("hide");
			if(vid === 'Yes'){
			pause_vid();
			}
      window.location.reload();
    }
  }
  xmlhttp.open("GET","php/seen.php?id="+id+"&note="+alert,true);
  xmlhttp.send();
		}
    
    
    function add_waiver(){
			//Disable button after initial submit...
  		document.getElementById('wsub_button').disabled = true;
			
  		var rep_name = '<?php echo $_SESSION['full_name']; ?>';
  		var rep_id = '<?php echo $_SESSION['user_id']; ?>';
  		
			var wcname = document.getElementById('wcname').value;
			if(wcname === ''){
				document.getElementById('waiver_error').innerHTML = 'Please Enter The Customer Name!';
				return;
			}
			var wcemail = document.getElementById('wcemail').value;
			if(wcemail === ''){
				document.getElementById('waiver_error').innerHTML = 'Please Enter The Customer Email Address!';
				return;
			}
			var winv = document.getElementById('winv').value;
			if(winv === ''){
				document.getElementById('waiver_error').innerHTML = 'Please Enter The Customer Invoice Number!';
				return;
			}
			var wtype = document.getElementById('wform_type').value;
			if(wtype === ''){
				document.getElementById('waiver_error').innerHTML = 'Please Select The Waiver Type!';
				return;
			}
  
      $("#newWaiver").modal("hide");
			//Clear Fields...
			document.getElementById('wcname').value = '';
			document.getElementById('wcemail').value = '';
			document.getElementById('winv').value = '';
			document.getElementById('wform_type').value = '';
			
      
 
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/waivers/send-waiver.php?"+
               "rep_name="+rep_name+
               "&rep_id="+rep_id+
               "&cname="+wcname+
               "&cemail="+wcemail+
               "&inv="+winv+
               "&type="+wtype,true);
  xmlhttp.send();
  }
		
		
		
		
	 function preview_waiver(){
			
			/*var wcname = document.getElementById('wcname').value;
			if(wcname === ''){
				document.getElementById('waiver_error').innerHTML = 'Please Enter The Customer Name!';
				return;
			}
			var wcemail = document.getElementById('wcemail').value;
			if(wcemail === ''){
				document.getElementById('waiver_error').innerHTML = 'Please Enter The Customer Email Address!';
				return;
			}
			var winv = document.getElementById('winv').value;
			if(winv === ''){
				document.getElementById('waiver_error').innerHTML = 'Please Enter The Customer Invoice Number!';
				return;
			}*/
			var wtype = document.getElementById('wform_type').value;
			if(wtype === ''){
				document.getElementById('waiver_error').innerHTML = 'Please Select The Waiver Type!';
				return;
			}
  
 //Setup Email Template...
switch (wtype) {
  case "Build Over Waiver":
    //Do Something...
    window.open('http://marketforceapp.com/marketforce/waivers/templates/build-over-waiver.php?preview=yes&wcname='+wcname+'&wcemail='+wcemail+'&winv='+winv+'&wtype='+wtype, '_blank');
    break;
  case "Extension":
    //Do Something...
    window.open('http://marketforceapp.com/marketforce/waivers/templates/extension-waiver.php?preview=yes&wcname='+wcname+'&wcemail='+wcemail+'&winv='+winv+'&wtype='+wtype, '_blank');
		break;
  case "Vertical Metal Waiver":
    //Do Something...
    window.open('http://marketforceapp.com/marketforce/waivers/templates/vertical-metal-waiver.php?preview=yes&wcname='+wcname+'&wcemail='+wcemail+'&winv='+winv+'&wtype='+wtype, '_blank');
		break;
  case "Warranty Info":
    //Do Something...
    window.open('http://marketforceapp.com/marketforce/waivers/templates/warranty-info-form.php?preview=yes&wcname='+wcname+'&wcemail='+wcemail+'&winv='+winv+'&wtype='+wtype, '_blank');
		break;
  case "Customer Form":
    //Do Something...
    window.open('http://marketforceapp.com/marketforce/waivers/templates/customer-form.php?preview=yes&wcname='+wcname+'&wcemail='+wcemail+'&winv='+winv+'&wtype='+wtype, '_blank');
		break;
  case "door-placement-sheet":
    //Do Something...
    window.open('http://marketforceapp.com/marketforce/waivers/templates/door-placement-sheet.php?preview=yes&wcname='+wcname+'&wcemail='+wcemail+'&winv='+winv+'&wtype='+wtype, '_blank');
    break;
  default:
    //$error = true;
}
		 
  }
	
		
		
	function add_waiver_notes(){
		var rep_name = '<? echo $_SESSION['full_name']; ?>';
		var rep_id = '<? echo $_SESSION['user_id']; ?>';
		var wid = document.getElementById('wnoteID').value;
		var wnote = document.getElementById('wnotes').value;
		
			
			document.getElementById('wnote_sub_button').disabled = true;
		 $("#newWaiver").modal("hide");
			//Clear Fields...
			document.getElementById('wnoteID').value = '';
			document.getElementById('wnotes').value = '';
			
 
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			//alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/waivers/php/add-note.php?"+
               "rep_name="+rep_name+
               "&rep_id="+rep_id+
               "&wid="+wid+
               "&wnote="+wnote,true);
  xmlhttp.send();
  }
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> Waivers</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                            <li>
                                <i class="fa fa-exclamation-triangle"></i> Waivers
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							<?php
							
							echo '<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-danger">
										<div class="panel-heading">
											<h3 class="panel-title">
												Pending Waivers &nbsp;&nbsp; <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newWaiver">New Waiver</button>
											</h3>
										</div>
										<div class="panel-body" style="height:100%;">
											<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
												<tr>
													<th>Customer Name</th>
													<th>Document Type</th>
													<th>Sent By</th>
													<th>Date</th>
													<th>Time</th>
													<th>Status</th>
													<th>Notes</th>
													<th>Actions</th>';
										echo '</tr>
												</thead>
												<tbody>';
							
								$tdq = "SELECT * FROM `waivers` WHERE `inactive` != 'Yes' AND `status` != 'Cancelled' ORDER BY `ID` DESC";
								$tdg = mysqli_query($conn, $tdq) or die($conn->error);
								while($tdr = mysqli_fetch_array($tdg)){
									$ornq = "SELECT * FROM `users` WHERE `ID` = '" . $tdr['rep'] . "'";
									$orng = mysqli_query($conn, $ornq) or die($conn->error);
									$ornr = mysqli_fetch_array($orng);
									
									//escape dealer name
									$dname = mysqli_real_escape_string($conn,$tdr['to']);
									
									echo '<tr>';
									echo '<td>' . $tdr['cname'] . '</td>
												<td>' . $tdr['type'] . '</td>
												<td>' . $tdr['sent_by_rep'] . '</td>
												<td>' . date("m/d/Y", strtotime($tdr['date'])) . '</td>
												<td>' . date("h:iA", strtotime($tdr['time'])) . '</td>
												<td>' . $tdr['status'] . '</td>';
									
													$nq = "SELECT * FROM `waiver_notes` WHERE `waiver_id` = '" . $tdr['ID'] . "' AND `inactive` != 'Yes' ORDER BY `ID` DESC";
													$ng = mysqli_query($conn, $nq) or die($conn->error);
													
													echo '<td style="max-height:160px;">
																<div style="max-width:250px;max-height:150px;overflow:scroll;color:black;">';
													while($nr = mysqli_fetch_array($ng)){
														echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-comments-o"></i> ' . $nr['rep_name'] . ' ' . date("m/d/y",strtotime($nr['date'])) . ' ' . date("h:i A",strtotime($nr['time'])) . '
													</span></h4>
														<div class="well well-sm">
														' . $nr['note'] . '
													</div>';
													}
													echo '</div>
																</td>';
												
//Setup Viewer Link...
$url = '';
switch ($tdr['type']) {
  case "Build Over Waiver":
    //Do Something...
    $url = 'http://marketforceapp.com/marketforce/waivers/templates/build-over-waiver.php?id=' . $tdr['ID'];
    break;
  case "Extension":
    //Do Something...
    $url = 'http://marketforceapp.com/marketforce/waivers/templates/extension-waiver.php?id=' . $tdr['ID'];
    break;
  case "Vertical Metal Waiver":
    //Do Something...
    $url = 'http://marketforceapp.com/marketforce/waivers/templates/vertical-metal-waiver.php?id=' . $tdr['ID'];
    break;
  case "Warranty Info":
    //Do Something...
    $url = 'http://marketforceapp.com/marketforce/waivers/templates/warranty-info-form.php?id=' . $tdr['ID'];
    break;
      case "Customer Form":
    //Do Something...
    $url = 'http://marketforceapp.com/marketforce/waivers/templates/customer-form.php?id=' . $tdr['ID'];
    break;
  case "door-placement-sheet":
    //Do Something... 
    $url = 'http://marketforceapp.com/marketforce/waivers/templates/door-placement-sheet.php?id=' . $tdr['ID'];
    break;
  default:
    //$error = true;
}
									
									echo '<td>
												<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#newWaiverNote" onclick="load_modal(' . $tdr['ID'] . ',\'\',\'wnote\');" title="Add Note"><i class="fa fa-comments"></i></button>
												<a href="' . $url . '" target="_blank">
												<button type="button" class="btn btn-primary" title="View Form"><i class="fa fa-eye"></i></button>
												</a>
												<!--<button type="button" class="btn btn-danger" title="Cancel"><i class="fa fa-times"></i></button>-->
												</td>
												</tr>';
									
								}
												
							echo '</tbody>
										</table>
										</div>
										</div>
									</div>
								</div>
							</div><!--End Row-->';
							
							
							?>
							
							
			
    
			 <!-- Modal -->
  <div class="modal fade" id="newWaiver" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">New Waiver</h4>
        </div>
        <div class="modal-body">
					<div class="row">
            <div class="col-lg-6">
              <p>
                Customer Name:
              </p>
              <input type="text" id="wcname" name="wcname" class="form-control" />
            </div>
            <div class="col-lg-6">
              <p>
                Customer Email:
              </p>
              <input type="text" id="wcemail" name="wcemail" class="form-control" />
            </div>
          </div>
          <br>
          <div class="row">
						<div class="col-lg-6">
							<p>
								Invoice#:
							</p>
							<input type="text" id="winv" name="winv" class="form-control" />
						</div>
            <div class="col-lg-6">
              <p>
                Waiver Form:
              </p>
              <select id="wform_type" name="wform_type" class="form-control">
                <option value="">Select One</option>
								<option value="Build Over Waiver">Build Over Waiver</option>
								<option value="Extension">Extension</option>
								<option value="Vertical Metal Waiver">Vertical Metal Waiver</option>
								<option value="Warranty Info">Warranty Info Form</option>
								<option value="Customer Form">Customer Form</option>
                <option value="door-placement-sheet">Door Placement Sheet</option> 

              </select>
            </div>
          </div>
          <br>
          <p id="waiver_error" style="color:red;font-weight:bold;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-warning" onclick="preview_waiver();"><i class="fa fa-eye"></i> Preview</button>
					<button type="button" class="btn btn-success" id="wsub_button" onclick="add_waiver();">Submit</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	
	
							
			 <!-- Modal -->
  <div class="modal fade" id="newWaiverNote" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Notes</h4>
        </div>
        <div class="modal-body">
          <div class="row">
						<div class="col-lg-12">
							<p>
								Notes:
							</p>
							<textarea id="wnotes" name="wnotes" class="form-control" style="height:150px;"></textarea>
						</div>
          </div>
          <br>
          <p id="waiver_note_error" style="color:red;font-weight:bold;"></p>
					<input type="hidden" id="wnoteID" value="" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success" id="wnote_sub_button" onclick="add_waiver_notes();">Submit</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	
	
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

  

 <?php include 'footer.html'; ?>
							
							
  <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <!--<script src="js/plugins/morris/morris-data.js"></script>-->
</body>


</html>