<?php
include 'php/connection.php';

if($_SESSION['admin'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com/";
				}, 2000);
				</script>';
	return;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--NEW BOOTSTRAP FOR TOGGLE SWITCHES-->
    <!--
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
	  -->
  
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
    .access_table{
      margin:auto;
    }
    .access_table td{
      padding: 15px;
    }
    .toggles{
      height:25px;
      width:25px;
    }
  </style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

							 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> User Access <span style="color:red; font-weight:bold;"><?php echo $_GET['response']; ?></span></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
														<li class="active">
															<i class="fa fa-file"></i> User Access
													</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
              

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
				<div class="tab-content" id="mySubTabContent">
          <ul class="nav nav-tabs">
            <li class="nav-item active"><a data-toggle="tab" href="#activeUsers" style="color:#DC0031;"><span class="glyphicon glyphicon-menu-hamburger"></span> Active Users</a></li>
            <li class="nav-item"><a data-toggle="tab" href="#inactiveUsers" style="color:#DC0031;"><span class="glyphicon glyphicon-th-large"></span> Inactive Users</a></li>
          </ul> 
            
            <!-- Raw Materials Tab -->
            <div class="tab-pane active in" id="activeUsers" role="tabpanel">
              <?php include 'access/sections/active-users-table.php'; ?>  
            </div>

            <!-- Components Tab -->
            <div class="tab-pane" id="inactiveUsers" role="tabpanel">
              <?php include 'access/sections/inactive-users-table.php'; ?>
            </div>
          
          </div>
              

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
      
    </div>
    <!-- /#wrapper -->
  
  
    <!-- jQuery -->
    <script src="<?php if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') { echo 'https'; }else{ echo 'http'; } ?>://marketforceapp.com/marketforce/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') { echo 'https'; }else{ echo 'http'; } ?>://marketforceapp.com/marketforce/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') { echo 'https'; }else{ echo 'http'; } ?>://marketforceapp.com/marketforce/js/plugins/morris/raphael.min.js"></script>
    <script src="<?php if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') { echo 'https'; }else{ echo 'http'; } ?>://marketforceapp.com/marketforce/js/plugins/morris/morris.min.js"></script>
    <script src="<?php if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') { echo 'https'; }else{ echo 'http'; } ?>://marketforceapp.com/marketforce/js/plugins/morris/morris-data.js"></script>

 <?php include 'footer.html'; ?>
  
</body>
  
  <? include 'access/modals/user-settings-modal.php'; ?>
  <? include 'access/modals/new-user-modal.php'; ?>
  <script src="access/js/access-functions.js?cb=<?php echo uniqid(); ?>"></script>
</html>