<div class="pl-15 pr-15 mb-15">
  <div class="pull-left">
    <i class="zmdi zmdi-check inline-block mr-10 font-16"></i>
    <i class="zmdi zmdi-delete inline-block mr-10 font-16"></i>
    <span class="inline-block txt-warning txt-dark">Process One</span>
    <span class="inline-block txt-primary txt-dark">User</span>
  </div>
  <span class="inline-block txt-success pull-right weight-500">12</span>
  <div class="clearfix"></div>
</div>
<hr class="light-grey-hr mt-0 mb-15" />