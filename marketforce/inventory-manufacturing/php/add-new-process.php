<?php
include '../../php/connection.php';

//Load Variables...
$process_id = uniqid();

//Add New Process...
$iq = "INSERT INTO `inv_man_processes`
      (
      `date`,
      `time`,
      `process_id`,
      `process_name`,
      `user_id`,
      `user_name`,
      `status`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $process_id . "',
      'Untitled',
      '" . $_SESSION['user_id'] . "',
      '" . $_SESSION['full_name'] . "',
      'Open',
      'No'
      )";
mysqli_query($conn, $iq) or die($conn->error);

$x->response = 'GOOD';
$x->message = '"Untitled" process has been added with ID: ' . $process_id;
$x->process_id = $process_id;
$x->process_name = 'Untitled';

$response = json_encode($x);
echo $response;

?>