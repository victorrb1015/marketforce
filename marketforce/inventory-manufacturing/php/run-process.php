<?php
header('Content-Type: application/json');
error_reporting(0);
include '../../php/connection.php';

//Load Variables...
$pid = mysqli_real_escape_string($conn, $_REQUEST['pid']);//Process ID
$prid = mysqli_real_escape_string($conn, $_REQUEST['prid']);//Process Run ID
$qty = mysqli_real_escape_string($conn, $_REQUEST['pqty']);//Process Qty
$new_qty = mysqli_real_escape_string($conn, $_REQUEST['new_qty']);//New Qty for Pending Process
$mode = mysqli_real_escape_string($conn, $_REQUEST['mode']);//Process Mode [Start, Complete, Cancel]
$oid = mysqli_real_escape_string($conn, $_REQUEST['oid']);//Process Option ID

//Lot Number Info...
$lot_num = 'L' . rand(1000000,99999999);

//Get Process Name...
$nq = "SELECT * FROM `inv_man_processes` WHERE `process_id` = '" . $pid . "'";
$ng = mysqli_query($conn, $nq) or die($conn->error);
$nr = mysqli_fetch_array($ng);
$pname = $nr['process_name'];


if($mode == 'Start'){
  $prid = uniqid();
  $iq = "INSERT INTO `inv_man_processes_run`
        (
        `date`,
        `time`,
        `lot_number`,
        `process_run_id`,
        `process_id`,
        `option_id`,
        `process_qty`,
        `started_by_id`,
        `started_by_name`,
        `status`,
        `inactive`
        )
        VALUES
        (
        CURRENT_DATE,
        CURRENT_TIME,
        '" . $lot_num . "',
        '" . $prid . "',
        '" . $pid . "',
        '" . $oid . "',
        '" . $qty . "',
        '" . $_SESSION['user_id'] . "',
        '" . $_SESSION['full_name'] . "',
        'Pending',
        'No'
        )";
  mysqli_query($conn, $iq) or die($conn->error);
  
  
  $x->response = 'GOOD';
  $x->message = 'Process: ' . $pname . ' Was Sucessfully Started!';
  $x->process_run_id = $prid;
  $x->process_name = $pname;
  $x->process_qty = $qty;
  $x->process_user = $_SESSION['full_name'];
  $x->lot_num = $lot_num;
}//End Start....



if($mode == 'Complete'){
//Get Process Run Details...
$rq = "SELECT * FROM `inv_man_processes_run` WHERE `process_run_id` = '" . $prid . "'";
$rg = mysqli_query($conn, $rq) or die($conn->error);
$rr = mysqli_fetch_array($rg);
$qty = $rr['process_qty'];
$pid = $rr['process_id'];
$option_id = $rr['option_id'];
  
  
//Get Process Details...
$dq = "SELECT * FROM `inv_man_process_items` WHERE `inactive` != 'Yes' AND `process_id` = '" . $pid . "'";
$dg = mysqli_query($conn, $dq) or die($conn->error);
$items = array();
while($dr = mysqli_fetch_array($dg)){
  $adjustment = 0;
  
  
  if($dr['mode'] == 'Create'){
    $adjustment = $dr['qty'] * $qty;  
  }elseif($dr['mode'] == 'Use'){
    $adjustment = ($dr['qty'] * $qty) * -1;
  }elseif($dr['mode'] == 'Option'){
    if($dr['item_id'] == $option_id){
      $adjustment = ($dr['qty'] * $qty) * -1;
    }else{
      $adjustment = 0;
    }
  }else{
    echo 'THERE WAS AN ISSUE!!!';
  }
  
  //Get current item details...
  $ciq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $dr['item_id'] . "'";
  $cig = mysqli_query($conn, $ciq) or die($conn->error);
  $cir = mysqli_fetch_array($cig);
  $cqty = $cir['qty'];
  
  $uq = "UPDATE `inventory_items` SET `qty_" . $_SESSION['inventory_zone'] . "` = (`qty_" . $_SESSION['inventory_zone'] . "` + '" . $adjustment . "'), `qty` = (`qty` + '" . $adjustment . "'), `last_adjustment` = CURRENT_DATE, `last_adjustment_by` = '" . $_SESSION['user_id'] . "' WHERE `ID` = '" . $dr['item_id'] . "'";
  mysqli_query($conn, $uq) or die($conn->error);
  
  //Get current item details...
  $niq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $dr['item_id'] . "'";
  $nig = mysqli_query($conn, $niq) or die($conn->error);
  $nir = mysqli_fetch_array($nig);
  $nqty = $nir['qty'];
  
  $aq = "INSERT INTO `inventory_adjustments` 
          (
          `date`,
          `time`,
          `pid`,
          `current_qty`,
          `new_qty`,
          `adjustment`,
          `type`,
          `process_run_id`,
          `user_id`,
          `user_name`,
          `status`,
          `approval`,
          `inactive`
          )
          VALUES
          (
          CURRENT_DATE,
          CURRENT_TIME,
          '" . $dr['item_id'] . "',
          '" . $cqty . "',
          '" . $nqty . "',
          '" . $adjustment . "',
          'Manufacturing Process',
          '" . $prid . "',
          '" . $_SESSION['user_id'] . "',
          '" . $_SESSION['full_name'] . "',
          'Pending',
          '',
          'No'
          )";
  mysqli_query($conn, $aq) or die($conn->error);
  
  if($dr['mode'] == 'Create'){
    array_push($items, array(
      'title' => $nir['item_name'],
      'barcode' => $nir['barcode'],
      'qty' => $dr['qty'] * $rr['process_qty']
    ));
  }
  
}
$uq = "UPDATE `inv_man_processes_run` SET 
      `status` = 'Complete', 
      `completed_date` = CURRENT_DATE, 
      `completed_time` = CURRENT_TIME,
      `completed_by_id` = '" . $_SESSION['user_id'] . "',
      `completed_by_name` = '" . $_SESSION['full_name'] . "'
      WHERE `process_run_id` = '" . $prid . "'";
 mysqli_query($conn, $uq) or die($conn->error);

$x->response = 'GOOD';
$x->message = 'Process Was Sucessfully Completed!';
$x->lot_num = $lot_num;
$x->items = $items;
}//End Complete
  


if($mode == 'Cancel'){
  $uq = "UPDATE `inv_man_processes_run` SET 
      `status` = 'Cancelled', 
      `cancelled_date` = CURRENT_DATE, 
      `cancelled_time` = CURRENT_TIME,
      `cancelled_by_id` = '" . $_SESSION['user_id'] . "',
      `cancelled_by_name` = '" . $_SESSION['full_name'] . "'
      WHERE `process_run_id` = '" . $prid . "'";
  mysqli_query($conn, $uq) or die($conn->error);
  
  $x->response = 'GOOD';
  $x->message = 'Process Was Sucessfully Cancelled!';
}


if($mode == 'Edit'){
  $uq = "UPDATE `inv_man_processes_run` SET 
        `process_qty` = '" . $new_qty . "'
      WHERE `process_run_id` = '" . $prid . "'";
  mysqli_query($conn, $uq) or die($conn->error);
  
  $x->response = 'GOOD';
  $x->message = 'Process Was Sucessfully Edited!';
}
  

$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;
  
?>