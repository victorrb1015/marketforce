<?php
include '../../php/connection.php';

//Load Variables...
$pid = $_REQUEST['pid'];
$new_process_id = uniqid();

//Get Current Process Details...
$q = "SELECT * FROM `inv_man_processes` WHERE `process_id` = '" . $pid . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
$new_title = $r['process_name'] . ' (Copy)';

$piq = "SELECT * FROM `inv_man_process_items` WHERE `inactive` != 'Yes' AND `process_id` = '" . $pid . "'";
$pig = mysqli_query($conn, $piq) or die($conn->error);
while($pir = mysqli_fetch_array($pig)){
  $iiq = "INSERT INTO `inv_man_process_items`
      (
      `date`,
      `time`,
      `process_id`,
      `item_id`,
      `qty`,
      `mode`,
      `user_id`,
      `user_name`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $new_process_id . "',
      '" . $pir['item_id'] . "',
      '" . $pir['qty'] . "',
      '" . $pir['mode'] . "',
      '" . $_SESSION['user_id'] . "',
      '" . $_SESSION['user_name'] . "',
      'No'
      )";
  mysqli_query($conn, $iiq) or die($conn->error);
}


//Add New Process...
$iq = "INSERT INTO `inv_man_processes`
      (
      `date`,
      `time`,
      `process_id`,
      `process_name`,
      `user_id`,
      `user_name`,
      `status`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $new_process_id . "',
      '" . $new_title . "',
      '" . $_SESSION['user_id'] . "',
      '" . $_SESSION['full_name'] . "',
      'Open',
      'No'
      )";
mysqli_query($conn, $iq) or die($conn->error);

$x->response = 'GOOD';
$x->message = '"' . $new_title . '" process has been added with ID: ' . $new_process_id;
$x->process_id = $new_process_id;
$x->process_name = $new_title;

$response = json_encode($x);
echo $response;

?>