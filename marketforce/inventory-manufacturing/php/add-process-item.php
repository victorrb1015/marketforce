<?php
header('Content-Type: application/json');
error_reporting(0);
include '../../php/connection.php';

//Load Variables...
$pid = mysqli_real_escape_string($conn, $_GET['pid']);
$qty = mysqli_real_escape_string($conn, $_GET['qty']);
$item = mysqli_real_escape_string($conn, $_GET['item']);
$mode = mysqli_real_escape_string($conn, $_GET['mode']);
  
//Get Item Name...
$nq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $item . "'";
$ng = mysqli_query($conn, $nq) or die($conn->error);
$nr = mysqli_fetch_array($ng);
$item_name = $nr['item_name'];

//Add Item...
$iq = "INSERT INTO `inv_man_process_items`
      (
      `date`,
      `time`,
      `process_id`,
      `item_id`,
      `qty`,
      `mode`,
      `user_id`,
      `user_name`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $pid . "',
      '" . $item . "',
      '" . $qty . "',
      '" . $mode . "',
      '" . $_SESSION['user_id'] . "',
      '" . $_SESSION['full_name'] . "',
      'No'
      )";
  mysqli_query($conn, $iq) or die($conn->error);

//Get Item ID...
//$iiq = "SELECT * FROM `inv_man_process_items` WHERE `inactive` != 'Yes' ORDER BY `ID` DESC LIMIT 1";
//$iig = mysqli_query($conn, $iiq) or die($conn->error);
//$iir = mysqli_fetch_array($iig);
//$iid = $iir['ID'];
$iid = $conn->insert_id;

$x->response = 'GOOD';
$x->message = 'Item Added to Process!';
$x->item_name = $item_name;
$x->qty = $qty;
$x->item_id = $iid;
$x->mode = $mode;

$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;


?>