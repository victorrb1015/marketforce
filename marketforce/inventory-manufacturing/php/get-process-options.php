<?php
header('Content-Type: application/json');
error_reporting(0);
include '../../php/connection.php';

//Load Variables...
$pid = $_REQUEST['pid'];


#Main Functions...

 //Get Options
  $oq = "SELECT
        `inv_man_process_items`.`item_id`,
        `inv_man_process_items`.`process_id`,
        `inv_man_process_items`.`inactive`,
        `inv_man_process_items`.`mode`,
        `inv_man_process_items`.`qty`,
        `inventory_items`.`item_name`
        FROM `inv_man_process_items` 
        LEFT JOIN `inventory_items`
        ON `inv_man_process_items`.`item_id` = `inventory_items`.`ID`
        WHERE `inv_man_process_items`.`process_id` = '" . $pid . "' 
        AND `inv_man_process_items`.`inactive` != 'Yes' 
        AND `inv_man_process_items`.`mode` = 'Option'";
  $og = mysqli_query($conn, $oq) or die($conn->error);
  $items = [];
  while($or = mysqli_fetch_array($og)){
    array_push($items, array("item_id" => $or['item_id'], "item_name" => $or['item_name'], "qty" => $or['qty']));
  }

$x->response = 'GOOD';
$x->message = 'Process Options Retrieved';
$x->process_id = $pid;
$x->options = $items;

//Setup Response Output...
$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;