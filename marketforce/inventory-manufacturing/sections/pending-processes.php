<div class="panel panel-default card-view">
  <div class="panel-heading">
    <div class="pull-left">
      <h6 class="panel-title txt-dark">Pending Processes</h6>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="panel-wrapper collapse in">
    <div class="panel-body row">
      <div class="" id="pending_processes_display">
        <?php
        $ppq = "SELECT * FROM `inv_man_processes_run` WHERE `inactive` != 'Yes' AND `status` = 'Pending'";
        $ppg = mysqli_query($conn, $ppq) or die($conn->error);
        while($ppr = mysqli_fetch_array($ppg)){
          //Get Process Name...
          $pnq = "SELECT * FROM `inv_man_processes` WHERE `process_id` = '" . $ppr['process_id'] . "'";
          $png = mysqli_query($conn, $pnq) or die($conn->error);
          $pnr = mysqli_fetch_array($png);
          $pname = $pnr['process_name'];
          
          echo '<div id="' . $ppr['process_run_id'] . '_pending_item" class="pl-15 pr-15 mb-15">
          <div class="pull-left">
            <a href="Javascript: complete_process(\'' . $ppr['process_run_id'] . '\');">
              <i class="zmdi zmdi-check inline-block mr-10 font-16" style="color:green;font-weight:bold;"></i>
            </a>
            <a href="Javascript: cancel_process(\'' . $ppr['process_run_id'] . '\');">
              <i class="zmdi zmdi-delete inline-block mr-10 font-16" style="color:red;font-weight:bold;"></i>
            </a>
            <a href="Javascript: edit_pending_process(\'' . $ppr['process_run_id'] . '\');">
              <i class="zmdi zmdi-edit inline-block mr-10 font-16" style="color:red;font-weight:bold;"></i>
            </a>
            <span class="inline-block txt-warning txt-dark mr-15">' . $pname . '</span>
            <span class="inline-block txt-primary txt-dark mr-15">' . $ppr['started_by_name'] . '</span>
          </div>
          <span class="inline-block txt-success pull-right weight-500" id="pending_process_qty">' . $ppr['process_qty'] . '</span>
          <div class="clearfix"></div>
        </div>
        <hr id="' . $ppr['process_run_id'] . '_pending_item_hr" class="light-grey-hr mt-0 mb-15" />';
        }
        ?>
        </div>
      </div>
    </div>
  </div>
</div>