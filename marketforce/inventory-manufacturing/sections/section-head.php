<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary card-view">
      <div class="panel-heading">
        <h1 class="panel-title">Manufacturing Process &nbsp;
          <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#addProcessModal" onclick="new_process();"><i class="fa fa-plus"></i> Add Process</button> &nbsp;&nbsp;
        </h1>
      </div>

    </div>
  </div>
</div>