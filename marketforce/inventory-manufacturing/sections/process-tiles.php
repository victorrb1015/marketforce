<div id="tile_area">
<?php
$ptq = "SELECT * FROM `inv_man_processes` WHERE `inactive` != 'Yes'";
$ptg = mysqli_query($conn, $ptq) or die($conn->error);
while($ptr = mysqli_fetch_array($ptg)){
  
  echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="' . $ptr['process_id'] . '_tile">
  <div class="panel panel-default card-view panel-refresh">
    <div class="refresh-container">
      <div class="la-anim-1"></div>
    </div>
    <div class="panel-heading">
      <div class="text-center">
        <h6 class="panel-title txt-dark txt-warning" id="' . $ptr['process_id'] . '_tile_title">' . $ptr['process_name'] . '</h6>
      </div>
      <div class="text-center">
        <a class="inline-block mr-15" href="#" aria-expanded="true">
					<i class="zmdi zmdi-edit" style="font-size:25px;" data-toggle="modal" data-target="#addProcessModal" onclick="load_process(\'' . $ptr['process_id'] . '\');"></i>
				</a>
          <a class="inline-block mr-15" href="#" aria-expanded="false" role="button">
						<i class="zmdi zmdi-delete" onclick="remove_process(\'' . $ptr['process_id'] . '\');" style="font-size:25px;"></i>
					</a>
        
        <a class="inline-block mr-15" href="#">
					<i class="fas fa-copy" onclick="copy_process(\'' . $ptr['process_id'] . '\');" style="font-size:25px;"></i>
				</a>
        <a class="inline-block mr-15" href="#">
					<i class="zmdi zmdi-play-circle-outline" onclick="start_process(\'' . $ptr['process_id'] . '\');" style="font-size:25px;"></i>
				</a>
        <!--<a class="inline-block close-panel" href="#" data-effect="fadeOut">
					<i class="zmdi zmdi-close" style="font-size:25px;"></i>
				</a>-->
      </div>
      <div class="clearfix"></div>
    </div>
    <div id="collapse_1" class="panel-wrapper collapse in">
      <div class="panel-body text-center">
        <i class="fas fa-tools" style="font-size: 35px;"></i>
      </div>
    </div>
  </div>
</div>';
  
}
  ?>
</div>
