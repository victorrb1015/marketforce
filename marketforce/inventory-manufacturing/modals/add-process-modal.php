<!-- sample modal content -->
<div id="addProcessModal" class="modal fade" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria="true">×</button>
        <h5 class="modal-title" id="myModalLabel">Add/Edit Process</h5>
      </div>
      <div class="modal-body">

        <div class="panel-wrapper collapse in">
          <div class="panel-body">
            <h6 class="text-muted">Name of Process: 
              <input type="text" name="process_name" id="process_name" class="form-control" placeholder="Write Process Name Here" onkeyup="save_process_name();">
              <input type="hidden" id="process_id" value="" />
            </h6>
            <div class="tab-struct custom-tab-2 mt-40">
              <ul role="tablist" class="nav nav-tabs" id="myTabs_15" style="background:transparent;">
                <li class="active" role="presentation">
                  <a aria-expanded="true" data-toggle="tab" role="tab" id="items_created_tab" href="#items_created" style="border: 1px solid rgba(255, 255, 255, 0.12)">
                          Items Created
                        </a>
                </li>
                <li role="presentation">
                  <a data-toggle="tab" id="items_used_tab" role="tab" href="#items_used" aria-expanded="false" style="border: 1px solid rgba(255, 255, 255, 0.12)">
                          Items Used
                        </a>
                </li>
                <li role="presentation">
                  <a data-toggle="tab" id="item_options_tab" role="tab" href="#item_options" aria-expanded="false" style="border: 1px solid rgba(255, 255, 255, 0.12)">
                          Item Options
                        </a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent_15">
                <div id="items_created" class="tab-pane fade active in" role="tabpanel" style="background: transparent; border: 1px solid rgba(255, 255, 255, 0.12)">
                  <div class="row">
                    <div class="col-lg-6">
                      <select class="form-control" id="created_item">
                        <option value="">Choose Item To Create</option>
                        <?php
                          $iq = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND `item_type` != 'Coil' ORDER BY `item_name` ASC";
                          $ig = mysqli_query($conn, $iq) or die($conn->error);
                          while($ir = mysqli_fetch_array($ig)){
                            echo '<option value="' . $ir['ID'] . '">' . $ir['item_name'] . '</option>';
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-lg-3">
                      <input type="text" class="form-control" id="created_qty" placeholder="Qty" />
                    </div>
                    <div class="col-lg-3">
                      <button type="button" class="btn btn-success" id="created_add_btn" onclick="add_process_item('Create');">
                        Add Item
                      </button>
                    </div>
                  </div>
                  <!--Table for displaying items-->
                  <div class="row mt-50" id="created_display_header">
                    <div class="col-lg-1">
                      <!--<i class="fas fa-times" style="color:red;font-weight:bold;" onclick="remove_create_item();"></i>-->
                    </div>
                    <div class="col-lg-6">
                      Item Name
                    </div>
                    <div class="col-lg-5">
                      Item Qty
                    </div>
                  </div>
                  <div class="row" id="create_display_items">
                    
                  </div>
                </div>
                
                <div id="items_used" class="tab-pane fade" role="tabpanel" style="background: transparent; border: 1px solid rgba(255, 255, 255, 0.12);">
                  <div class="row">
                    <div class="col-lg-6">
                      <select class="form-control" id="use_item">
                        <option value="">Choose Item To Use</option>
                        <?php
                          $iq = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND `item_type` != 'Coil' ORDER BY `item_name` ASC";
                          $ig = mysqli_query($conn, $iq) or die($conn->error);
                          while($ir = mysqli_fetch_array($ig)){
                            echo '<option value="' . $ir['ID'] . '">' . $ir['item_name'] . '</option>';
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-lg-3">
                      <input type="text" class="form-control" id="use_qty" placeholder="Qty" />
                    </div>
                    <div class="col-lg-3">
                      <button type="button" class="btn btn-success" id="use_add_btn" onclick="add_process_item('Use');">
                        Add Item
                      </button>
                    </div>
                  </div>
                  <!--Table for displaying items-->
                  <div class="row mt-50" id="use_display_header">
                    <div class="col-lg-1">
                      <!--<i class="fas fa-times" style="color:red;font-weight:bold;" onclick="remove_create_item();"></i>-->
                    </div>
                    <div class="col-lg-6">
                      Item Name
                    </div>
                    <div class="col-lg-5">
                      Item Qty
                    </div>
                  </div>
                  <div class="row" id="use_display_items">
                    
                  </div>
                </div>
                
                <div id="item_options" class="tab-pane fade" role="tabpanel" style="background: transparent; border: 1px solid rgba(255, 255, 255, 0.12);">
                  <div class="row">
                    <div class="col-lg-6">
                      <select class="form-control" id="option_item">
                        <option value="">Add Item Option</option>
                        <?php
                          $iq = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND `item_type` != 'Coil' ORDER BY `item_name` ASC";
                          $ig = mysqli_query($conn, $iq) or die($conn->error);
                          while($ir = mysqli_fetch_array($ig)){
                            echo '<option value="' . $ir['ID'] . '">' . $ir['item_name'] . '</option>';
                          }
                        ?>
                      </select>
                    </div>
                    <div class="col-lg-3">
                      <input type="text" class="form-control" id="option_qty" placeholder="Qty" />
                    </div>
                    <div class="col-lg-3">
                      <button type="button" class="btn btn-success" id="option_add_btn" onclick="add_process_item('Option');">
                        Add Item
                      </button>
                    </div>
                  </div>
                  <!--Table for displaying items-->
                  <div class="row mt-50" id="option_display_header">
                    <div class="col-lg-1">
                      <!--<i class="fas fa-times" style="color:red;font-weight:bold;" onclick="remove_create_item();"></i>-->
                    </div>
                    <div class="col-lg-6">
                      Item Name
                    </div>
                    <div class="col-lg-5">
                      Item Qty
                    </div>
                  </div>
                  <div class="row" id="option_display_items">
                    
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->