<!-- sample modal content -->
<div id="processOptionModal" class="modal fade" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria="true">×</button>
        <h5 class="modal-title" id="myModalLabel">Select Process Option</h5>
      </div>
      <div class="modal-body">

        <div id="options_row">
          <!--<div class="col-lg-8">
            This is an option
          </div>
          <div class="col-lg-4">
            <button type="button" class="btn btn-primary">
              Select Option
            </button>
          </div>-->
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->