function add_process_item(mode){
  
  var pid = document.getElementById('process_id').value;
  if(mode === 'Create'){
    var item = document.getElementById('created_item').value;
    var qty = document.getElementById('created_qty').value;
  }else if(mode === 'Use'){
    var item = document.getElementById('use_item').value;
    var qty = document.getElementById('use_qty').value;
  }else if(mode === 'Option'){
    var item = document.getElementById('option_item').value;
    var qty = document.getElementById('option_qty').value;
  }else{
    toast_alert('Something Went Wrong...','An error ocurred while adding this item as an option.','bottom-right','error');
    return;
  }
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        
			  load_process_item(mode,r.item_id,r.item_name,qty);
        
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory-manufacturing/php/add-process-item.php?pid="+pid+"&qty="+qty+"&item="+item+"&mode="+mode,true);
  xmlhttp.send();
}

function save_process_name(){
  
  var pid = document.getElementById('process_id').value;
  var name = document.getElementById('process_name').value;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById(pid+'_tile_title').innerHTML = name;
        console.log('Process Name was Saved!');
      }else{
        toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory-manufacturing/php/save-process-name.php?pid="+pid+"&name="+name,true);
  xmlhttp.send();
}

function remove_process_item(pid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        console.log(r.message);
        document.getElementById(pid+'_row').remove();
      }else{
        toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory-manufacturing/php/remove-process-item.php?pid="+pid,true);
  xmlhttp.send();
}

function load_process_item(mode,iid,name,qty){
        var create_table = document.getElementById('create_display_items');
        var use_table = document.getElementById('use_display_items');
        var option_table = document.getElementById('option_display_items');
        
        var row = document.createElement('div');
        //row.setAttribute('class','row');
        row.setAttribute('id',iid+'_row');
        //X Column...
        var col = document.createElement('div');
        col.setAttribute('class','col-lg-1');
        var a = document.createElement('a');
        a.setAttribute('href','Javascript: remove_process_item('+iid+');');
        var icon = document.createElement('i');
        icon.setAttribute('class','fas fa-times');
        icon.setAttribute('style','color:red;font-weight:bold;');
        a.appendChild(icon);
        col.appendChild(a);
        row.appendChild(col);

        //Name Column...
        var col = document.createElement('div');
        col.setAttribute('class','col-lg-6');
        col.innerHTML = name;
        row.appendChild(col);

        //Qty Column...
        var col = document.createElement('div');
        col.setAttribute('class','col-lg-5');
        col.innerHTML = qty;
        row.appendChild(col);
  
        //ClearFix
        var col = document.createElement('div');
        col.setAttribute('class','clearfix');
        row.appendChild(col);
  
        if(mode === 'Create'){
          create_table.appendChild(row);
          //Reset the Form...
          document.getElementById('created_item').value = '';
          document.getElementById('created_qty').value = '';
        }else if(mode === 'Use'){
          use_table.appendChild(row);
          //Reset the Form...
          document.getElementById('use_item').value = '';
          document.getElementById('use_qty').value = '';
        }else if(mode === 'Option'){
          option_table.appendChild(row);
          //Reset the Form...
          document.getElementById('option_item').value = '';
          document.getElementById('option_qty').value = '';
        }else{
          toast_alert('Something Went Wrong...','An error ocurred while loading your item.','bottom-right','error');
        }
}