function new_process(){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        
        var maindiv = document.createElement('div');
        maindiv.setAttribute('class','col-lg-4 col-md-4 col-sm-4 col-xs-12');
        maindiv.setAttribute('id',r.process_id+'_tile');
        var tilediv = document.createElement('div');
        tilediv.setAttribute('class','panel panel-default card-view panel-refresh');
        //Refresh Button...
        var refresh = document.createElement('div');
        refresh.setAttribute('class','refresh-container');
        var anim = document.createElement('div');
        anim.setAttribute('class','la-anim-1');
        refresh.appendChild(anim);
        //Heading...
        var head = document.createElement('div');
        head.setAttribute('class','panel-heading');
        var div = document.createElement('div');
        div.setAttribute('class','text-center');
        var h6 = document.createElement('h6');
        h6.setAttribute('class','panel-title txt-dark txt-warning');
        h6.setAttribute('id',r.process_id+'_tile_title');
        h6.innerHTML = r.process_name;
        div.appendChild(h6);
        var div2 = document.createElement('div');
        div2.setAttribute('class','text-center');
        var a = document.createElement('a');
        a.setAttribute('class','inline-block mr-15');
        a.setAttribute('href','#');
        a.setAttribute('aria-expanded','true');
        var i = document.createElement('i');
        i.setAttribute('class','zmdi zmdi-edit');
        i.setAttribute('style','font-size:25px;');
        i.setAttribute('data-toggle','modal');
        i.setAttribute('data-target','#addProcessModal');
        i.setAttribute('onclick','load_process(\''+r.process_id+'\');');
        a.appendChild(i);
        div2.appendChild(a);
        var a = document.createElement('a');
        a.setAttribute('class','inline-block mr-15');
        a.setAttribute('href','#');
        a.setAttribute('aria-expanded','false');
        var i = document.createElement('i');
        i.setAttribute('class','zmdi zmdi-delete');
        i.setAttribute('style','font-size:25px;');
        i.setAttribute('onclick','remove_process(\''+r.process_id+'\');');
        a.appendChild(i);
        div2.appendChild(a);
        var a = document.createElement('a');
        a.setAttribute('class','inline-block mr-15');
        a.setAttribute('href','#');
        a.setAttribute('aria-expanded','false');
        var i = document.createElement('i');
        i.setAttribute('class','fas fa-copy');
        i.setAttribute('style','font-size:25px;');
        i.setAttribute('onclick','copy_process(\''+r.process_id+'\');');
        a.appendChild(i);
        div2.appendChild(a);
        var a = document.createElement('a');
        a.setAttribute('class','inline-block mr-15');
        a.setAttribute('href','#');
        a.setAttribute('aria-expanded','false');
        var i = document.createElement('i');
        i.setAttribute('class','zmdi zmdi-play-circle-outline');
        i.setAttribute('style','font-size:25px;');
        i.setAttribute('onclick','start_process(\''+r.process_id+'\');');
        a.appendChild(i);
        div2.appendChild(a);
        /*var a = document.createElement('a');
        a.setAttribute('class','inline-block mr-15');
        a.setAttribute('href','#');
        a.setAttribute('aria-expanded','false');
        var i = document.createElement('i');
        i.setAttribute('class','zmdi zmdi-close');
        i.setAttribute('style','font-size:25px;');
        i.setAttribute('onclick','');
        a.appendChild(i);
        div2.appendChild(a);*/
        var clearfix = document.createElement('div');
        clearfix.setAttribute('class','clearfix');
        head.appendChild(div);
        head.appendChild(div2);
        head.appendChild(clearfix);
        //Body of Tile...
        var body = document.createElement('div');
        body.setAttribute('id','collapse_1');
        body.setAttribute('class','panel-wrapper collapse in');
        var div = document.createElement('div');
        div.setAttribute('class','panel-body text-center');
        var i = document.createElement('i');
        i.setAttribute('class','fas fa-tools');
        i.setAttribute('style','font-size:35px;');
        div.appendChild(i);
        body.appendChild(div);
        //Put all together...
        tilediv.appendChild(head);
        tilediv.appendChild(body);
        maindiv.appendChild(tilediv);
        document.getElementById('tile_area').appendChild(maindiv);
        //Alert the Success...
        toast_alert('Success!',r.message,'bottom-right','success');
        //Load Details to Modal...
        document.getElementById('process_name').value = r.process_name;
        document.getElementById('process_id').value = r.process_id;
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory-manufacturing/php/add-new-process.php",true);
  xmlhttp.send();
}


function load_process(pid){
  document.getElementById('process_id').value = '';
  document.getElementById('process_name').value = '';
  document.getElementById('use_display_items').innerHTML = '';
  document.getElementById('create_display_items').innerHTML = '';
  document.getElementById('option_display_items').innerHTML = '';
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        
        document.getElementById('process_id').value = r.process_id;
        document.getElementById('process_name').value = r.process_name;
        for(var i = 0; i < r.item.length; i++){
          var x = r.item[i];
          load_process_item(x.mode,x.item_id,x.item_name,x.qty);
        }
			  
        
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory-manufacturing/php/get-tile-details.php?pid="+pid,true);
  xmlhttp.send();
}


function remove_process(pid){
  var conf = confirm('Are you sure you want to remove this process? (¿Seguro que quieres eliminar este proceso?)');
  if(conf === '' || conf === null || conf === false){
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById(pid+'_tile').remove();
        toast_alert('Success',r.message,'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory-manufacturing/php/remove-process.php?pid="+pid,true);
  xmlhttp.send();
}


function start_process(pid){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      // Typical action to be performed when the document is ready:
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById('options_row').innerHTML = '';
        if(r.options.length > 0){
          $('#processOptionModal').modal('show');
          r.options.forEach(function(o){
            add_process_option(pid,o.item_id,o.item_name,o.qty);
          });
        }else{
          log_start(pid);
        }
      }else{
        toast_alert('Something Went Wrong...','An error ocurred while processing your request.','bottom-right','error');
      }
    }
  };
  xhttp.open("GET", "inventory-manufacturing/php/get-process-options.php?pid="+pid, true);
  xhttp.send();
}


function log_start(pid,oid){
  var conf = confirm('Are you sure you want to start this process?');
  if(conf === '' || conf === null || conf === false){
    return;
  }
  var pqty = prompt('How many times will you be performing this process?');
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        add_pending_process(r.process_run_id,r.process_name,r.process_user,r.process_qty);
      	$('#processOptionModal').modal('hide');
        toast_alert('Success',r.message,'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory-manufacturing/php/run-process.php?pid="+pid+"&pqty="+pqty+"&mode=Start&oid="+oid,true);
  xmlhttp.send();
}


function copy_process(pid){
  var conf = confirm('Are you sure you want to copy this process?');
  if(conf === '' || conf === null || conf === false){
    return;
  }
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        toast_alert('Success',r.message,'bottom-right','success');
        window.location.reload();
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory-manufacturing/php/copy-process.php?pid="+pid,true);
  xmlhttp.send();
}