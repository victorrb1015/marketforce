function add_process_option(prid,oid,oname,oqty){
  var options = document.getElementById('options_row');
  //Create Row
  var row = document.createElement('div');
  row.setAttribute('class','row');
  //Name Column
  var div1 = document.createElement('div');
  div1.setAttribute('class','col-lg-8');
  div1.innerHTML = oname+' (Qty: '+oqty+')';
  //Button Column
  var div2 = document.createElement('div');
  div2.setAttribute('class','col-lg-4');
  var btn = document.createElement('button');
  btn.setAttribute('type','button');
  btn.setAttribute('class','btn btn-primary');
  btn.setAttribute('onclick','log_start(\''+prid+'\','+oid+');');
  btn.innerHTML = 'Select Option';
  //Line Break
  var hr = document.createElement('hr');
  //Compile and Add Row
  div2.appendChild(btn);
  row.appendChild(div1);
  row.appendChild(div2);
  options.appendChild(row);
  options.appendChild(hr);
}