function complete_process(prid){
  var conf = confirm('Are you sure you want to complete this process?');
  if(conf === '' || conf === null || conf === false){
    return;
  }
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById(prid+'_pending_item').remove();
        document.getElementById(prid+'_pending_item_hr').remove();
        toast_alert('Success',r.message,'bottom-right','success');
        for(var i = 0; i < r.items.length; i++){
          var item = r.items[i];
          print_label(item.barcode,item.title,r.lot_num,item.qty);
        }
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory-manufacturing/php/run-process.php?prid="+prid+"&mode=Complete",true);
  xmlhttp.send();
}

function cancel_process(prid){
  var conf = confirm('Are you sure you want to cancel this process (¿Estas Seguro que quieres cancelar este proceso?)?');
  if(conf === '' || conf === null || conf === false){
    return;
  }
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById(prid+'_pending_item').remove();
        document.getElementById(prid+'_pending_item_hr').remove();
        toast_alert('Success',r.message,'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory-manufacturing/php/run-process.php?prid="+prid+"&mode=Cancel",true);
  xmlhttp.send();
}


function edit_pending_process(prid){
  var conf = confirm('Are you sure you want to edit this pending process?');
  if(conf === '' || conf === null || conf === false){
    return;//Cancels the process...
  }
  
  var new_qty = prompt('How many time will this process be run?');
  if(new_qty === '' || new_qty === null || new_qty <= 0){
    alert("You must enter a new quantity greater than 0!");
    return;
  }
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById('pending_process_qty').innerHTML = new_qty;
        toast_alert('Success',r.message,'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory-manufacturing/php/run-process.php?prid="+prid+"&mode=Edit&new_qty="+new_qty,true);
  xmlhttp.send();
}


function add_pending_process(process_run_id,process_name,user,qty){
  var cdiv = document.createElement('div');
  cdiv.setAttribute('class','pl-15 pr-15 mb-15');
  cdiv.setAttribute('id',process_run_id+'_pending_item');
  var ldiv = document.createElement('div');
  ldiv.setAttribute('class','pull-left');
  var a1 = document.createElement('a');
  a1.setAttribute('href','Javascript: complete_process(\''+process_run_id+'\');');
  var icon1 = document.createElement('i');
  icon1.setAttribute('class','zmdi zmdi-check inline-block mr-10 font-16');
  icon1.setAttribute('style','color:green;font-weight:bold;');
  a1.appendChild(icon1);
  var a2 = document.createElement('a');
  a2.setAttribute('href','Javascript: cancel_process(\''+process_run_id+'\');');
  var icon2 = document.createElement('i');
  icon2.setAttribute('class','zmdi zmdi-delete inline-block mr-10 font-16');
  icon2.setAttribute('style','color:red;font-weight:bold;');
  a2.appendChild(icon2);
  var span1 = document.createElement('span');
  span1.setAttribute('class','inline-block txt-warning txt-dark mr-15');
  span1.innerHTML = process_name;
  var span2 = document.createElement('span');
  span2.setAttribute('class','inline-block txt-primary txt-dark');
  span2.innerHTML = user;
  ldiv.appendChild(a1);
  ldiv.appendChild(a2);
  ldiv.appendChild(span1);
  ldiv.appendChild(span2);
  var span3 = document.createElement('span');
  span3.setAttribute('class','inline-block txt-success pull-right weight-500');
  span3.innerHTML = qty;
  var cf = document.createElement('div');
  cf.setAttribute('class','clearfix');
  cdiv.appendChild(ldiv);
  cdiv.appendChild(span3);
  cdiv.appendChild(cf);
  var hr = document.createElement('hr');
  hr.setAttribute('class','light-grey-hr mt-0 mb-15');
  hr.setAttribute('id',process_run_id+'_pending_item_hr');
  document.getElementById('pending_processes_display').appendChild(cdiv);
  document.getElementById('pending_processes_display').appendChild(hr);
}

















