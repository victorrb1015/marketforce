<?php
include 'security/session/session-settings.php';

if (!isset($_SESSION['in'])) {
  session_destroy();
  echo 'You do not have authentication for this site...<br>';
  echo '<script>
				setInterval(function(){
				window.location="http://' . $_SERVER['HTTP_HOST'] . '";
				}, 2000);
				</script>';
  return;
}

include 'php/connection.php';

//Get Contract Form Name...
$cfq = "SELECT * FROM `signatures` WHERE `ID` = '" . $_GET['ider'] . "'";
$cfg = mysqli_query($conn, $cfq) or die($conn->error);
$cfr = mysqli_fetch_array($cfg);
$noDate = strtotime("0000-00-00");
$ddate = strtotime($cfr['ddate']);
$cdate = strtotime("30 May 2019");
if ($cdate > $ddate && $ddate != $noDate) {
  $contract_form_name = 'dealer-contract.php';
} else {
  $contract_form_name = 'dealer-contract-2019.php';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Market Force | All Steel</title>
  <!--JQuery-->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <!-- Bootstrap Core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/sb-admin.css" rel="stylesheet">

  <!-- Custom JS -->
  <script src="js/new/viewed.js"></script>

  <!-- Morris Charts CSS -->
  <link href="css/plugins/morris.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover {
      cursor: pointer;
    }

    .n {
      text-decoration: none;
      color: black;
    }
  </style>
  <script>
    function get_forms(ps) {
      <?php
      if (!isset($_GET['ider'])) {
        echo "var id = document.getElementById('dealer').value;
    if(id === 'default'){
      document.getElementById('response').innerHTML = 'Please Select A Dealer!';
      return;
    }";
      } else {
        echo 'var id = "' . $_GET['ider'] . '";
						var ps = "' . $_SESSION['full_name'] . '";';
      }
      ?>
      document.getElementById('replace').innerHTML = '<h3>New Dealer Forms</h3>' +
        '<br>' +
        '<a class="n" title="Click here to print a copy of the New Dealer Packet" href="forms/new-dealer-form/print-packet.php?id=' + id + '" target="_blank"><button type="button">View/Print Entire Packet</button></a>' +
        '<a class="n" href="forms/new-dealer-form/print-packet.php?id=' + id + '&ps=1" target="_blank"><button id="vis" type="button">View/Print Multi-Copy Packet</button></a>' +
        '<br><br>' +
        '<a class="n" title="Click here to view/print a copy of the Dealer Info Form" href="forms/new-dealer-form/dealer-info-form.php?id=' + id + '" target="_blank"><button type="button">Dealer Info Form</button></a>&nbsp;&nbsp;' +
        '<a class="n" title="Click here to edit the Dealer Info Form" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/forms/editing_forms/index.php?id=' + id + '&user=<?php echo $_SESSION['user_id']; ?>" target="_blank"><button type="button" style="background:red;">Edit Dealer Info</button></a>' +
        '<br><br>' +
        '<strong>Upload Dealer Contract Image</strong><br>' +
        '<form id="myForm" action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/forms/new-dealer-form/php/upload-dealer-contract.php" method="post" enctype="multipart/form-data">' +
        '<input title="Upload a copy of the Dealer Contract" id="fileToUpload" name="fileToUpload" type="file" onchange="form.submit();" required/><br>' +
        '<input type="hidden" id="id" name="id" value="' + id + '" />' +
        '</form>' +
        '<a class="n" title="View/Print a copy of your uploaded Dealer Contract" href="forms/new-dealer-form/php/view-dealer-contract.php?id=' + id + '" target="_blank"><button type="button">View Uploaded Dealer Contract</button></a>' +
        '<br><br>' +
        '<a class="n" title="Click here to fill out / view / print a copy of the Dealer Contract" href="forms/new-dealer-form/<?php echo $contract_form_name; ?>?id=' + id + '" target="_blank"><button type="button">Dealer Contract Form</button></a>' +
        '<br><br>' +
        '<a class="n" title="Click here to fill out / view / print a copy of the Landlord Consent Form" href="forms/new-dealer-form/landlord-consent-form.php?id=' + id + '" target="_blank"><button type="button">Landlord Consent Form</button></a>' +
        '<br><br>' +
        '<strong>Upload Consent Form Image</strong><br>' +
        '<form id="myForm1" action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/forms/new-dealer-form/php/upload-consent-form.php" method="post" enctype="multipart/form-data">' +
        '<input title="Use this to upload a hard copy of a Landlord Consent form if you did not do it electronically" id="fileToUpload" name="fileToUpload" type="file" onchange="form.submit();" required/><br>' +
        '<input type="hidden" id="id" name="id" value="' + id + '" />' +
        '</form>' +
        '<a class="n" title="View/Print a copy of your uploaded Landlord Consent Form" href="forms/new-dealer-form/php/view-consent-form.php?id=' + id + '" target="_blank"><button type="button">View Uploaded Consent Form</button></a>' +
        '<br><br>' +
        '<a class="n" title="Use this form to order displays for a NEW DEALER" href="forms/new-dealer-form/display-order-form.php?id=' + id + '" target="_blank"><button type="button">Display Order Form</button></a>' +
        '<br><br>' +
        '<strong>Upload Site Plan Image</strong><br>' +
        '<form id="myForm" action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/forms/new-dealer-form/php/upload-image.php" method="post" enctype="multipart/form-data">' +
        '<input title="Upload a copy of the display Site Plan" id="fileToUpload" name="fileToUpload" type="file" onchange="form.submit();" required/><br>' +
        '<input type="hidden" id="id" name="id" value="' + id + '" />' +
        //'<input type="submit" value="Upload Site Plan" />'+
        '</form>' +
        '<a class="n" title="View/Print a copy of your uploaded display Site Plan" href="forms/new-dealer-form/php/view-site-plan.php?id=' + id + '" target="_blank"><button type="button">View Uploaded Site Plan</button></a>' +
        '<br><br>' +
        '<button title="Click here when you have completed all the necessary forms in the New Dealer Packet" type="button" onclick="send_completed(' + id + ');" style="background-color:red;"><b>Send Completed Packet</b></button>';


      document.getElementById('replace2').innerHTML = '<h3>Additional Forms</h3>' +
        '<br>' +
        '<a class="n" title="Use this form to order ADDITIONAL displays for an EXISTING dealer" href="forms/new-dealer-form/display-addon-form.php?id=' + id + '" target="_blank"><button type="button">Display Addon Form</button></a>' +
        '<br><br>' +
        '<a class="n" title="Use this form in the event that a display needs to be moved to a different location and the dealer is remaining" href="forms/new-dealer-form/display-relocation-form.php?id=' + id + '" target="_blank"><button type="button">Dealer Relocation Form</button></a>' +
        '<br><br>' +
        '<a class="n" title="Use this form in the event that a display needs to be removed and the dealer is remaining" href="forms/new-dealer-form/display-removal-form.php?id=' + id + '" target="_blank"><button type="button">Display Removal Form</button></a>' +
        '<br><br>' +
        '<a class="n" title="This form is completed when a current dealer is closing and the displays are to be removed.  There will no longer be a dealer at this location." href="forms/new-dealer-form/dealer-closing-form.php?id=' + id + '" target="_blank"><button type="button">Dealer Closing Form</button></a>' +
        '<br><br>' +
        '<a class="n" title="This is the form to be used when a current dealer changes name / sells their business or transfers ownership to another party.  The displays will remain at the location." href="forms/new-dealer-form/change-of-ownership-form.php?id=' + id + '" target="_blank"><button type="button">Dealer Ownership Change Form</button></a>' +
        '<br><br>' +
        '<strong>Upload Image of Dealer Location</strong>' +
        '<br>' +
        '<form enctype="multipart/form-data" action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/forms/new-dealer-form/php/upload-dealer-imgs.php" method="post">' +
        '<input type="file" name="fileToUpload" id="fileToUpload" onchange="form.submit();" required>' +
        '<input type="hidden" value="' + id + '" name="id" id="id">' +
        '<input type="hidden" value="<?php echo $_SESSION['user_id']; ?>" name="uid" id="uid">' +
        '<span style="color:red;font-weight:bold;" id="up_response"></span>' +
        '</form>' +
        '<br><br>';
      <?php
      if ($_GET['uploaded'] == '1') {
        echo "document.getElementById('up_response').innerHTML = 'Your last photo uploaded successfully!';";
      }
      ?>
      document.getElementById('vis').style.visibility = 'hidden';
      if (ps == 'Mike Burton') {
        document.getElementById('vis').style.visibility = 'visible';
      }
      if (ps == 'Michael Burton') {
        document.getElementById('vis').style.visibility = 'visible';
      }
    }

    function get_blanks() {
      <?php
      switch ($_SESSION['org_id']) {
        case "162534":
          echo 'var dealer_info_form = "forms/files/northedge/information_form.pdf";';
          echo 'var dealer_contract_form = "forms/files/northedge/dealer_services_contract_2020.pdf";';
          echo 'var landlord_consent_form = "forms/files/northedge/landlord_consent.pdf";';
          echo 'var blank_site_pan = "forms/files/northedge/site_plan.pdf";';
          echo 'var display_order_form = "forms/files/northedge/display_order_form_2021.pdf";';
          echo 'var display_addon_form = "forms/files/northedge/current_dealer_add_on_form-2021.pdf";';
          echo 'var display_relocation_form = "forms/files/northedge/display_relocation_form.pdf";';
          echo 'var display_removal_form = "forms/files/northedge/display_removal_order_form.pdf";';
          echo 'var ownership_change_form = "forms/files/northedge/ownership_change_form.pdf";';
          echo 'var all_package = "forms/files/northedge/all_package.pdf";';
          break;
        case "615243":
          echo 'var dealer_info_form = "forms/files/legacy/New_Dealer_Form.pdf";';
          echo 'var dealer_contract_form = "forms/files/legacy/New_Dealer_Contract.pdf";';
          echo 'var display_order_form = "forms/files/legacy/Dealer_Display_Order_Form.pdf";';
          //All Steel
          echo 'var landlord_consent_form = "forms/files/landlord-consent.pdf";';
          echo 'var display_order_form = "forms/files/display-order-form-2018(2).pdf";';
          echo 'var display_addon_form = "forms/files/display-addon.pdf";';
          echo 'var display_relocation_form = "forms/files/dealer-relocation-form.pdf";';
          echo 'var display_removal_form = "forms/files/dealer-relocation-form.pdf";';
          echo 'var blank_site_pan = "forms/files/ASC-site-plan.pdf";';
          echo 'var ownership_change_form = "forms/files/Ownership-Change-Form.pdf";';
          echo 'var all_package = "forms/files/new-dealer-packet.pdf";';
          break;
          case "329698":
              //Integrity
              echo 'var dealer_info_form = "forms/files/Integrity/Information Form.pdf";';
              echo 'var display_removal_form = "forms/files/Integrity/Display removal.pdf";';
              echo 'var landlord_consent_form = "forms/files/Integrity/Old landlord consent.pdf";';
              echo 'var ownership_change_form = "forms/files/Integrity/Ownership Change Form.pdf";';
              echo 'var all_package = "forms/files/Integrity/Integrity_pack.pdf";';
              //All Steel
              echo 'var dealer_contract_form = "#";';
              echo 'var display_order_form = "#";';
              echo 'var display_addon_form = "#";';
              echo 'var display_relocation_form = "#";';
              echo 'var blank_site_pan = "#";';
              break;
          case "191554":
              echo 'var all_package = "forms/files/new-dealer-packet.pdf";';
              echo 'var dealer_info_form = "forms/files/Infinity/Information_Form.pdf";';
              echo 'var dealer_contract_form = "forms/files/dealer-contract-2019.pdf";';
              echo 'var landlord_consent_form = "forms/files/landlord-consent.pdf";';
              echo 'var display_order_form = "forms/files/display-order-form-2018(2).pdf";';
              echo 'var display_addon_form = "forms/files/display-addon.pdf";';
              echo 'var display_relocation_form = "forms/files/dealer-relocation-form.pdf";';
              //echo 'var display_removal_form = "forms/files/dealer-relocation-form.pdf";';
              echo 'var blank_site_pan = "forms/files/Infinity/ASC-site-plan.pdf";';
              echo 'var ownership_change_form = "forms/files/Infinity/Ownership-Change-Form.pdf";';
              break;
        default:
          echo 'var all_package = "forms/files/new-dealer-packet.pdf";';
          echo 'var dealer_info_form = "forms/files/dealer-info-form.pdf";';
          echo 'var dealer_contract_form = "forms/files/dealer-contract-2019.pdf";';
          echo 'var landlord_consent_form = "forms/files/landlord-consent.pdf";';
          echo 'var display_order_form = "forms/files/display-order-form-2018(2).pdf";';
          echo 'var display_addon_form = "forms/files/display-addon.pdf";';
          echo 'var display_relocation_form = "forms/files/dealer-relocation-form.pdf";';
          echo 'var display_removal_form = "forms/files/dealer-relocation-form.pdf";';
          echo 'var blank_site_pan = "forms/files/ASC-site-plan.pdf";';
          echo 'var ownership_change_form = "forms/files/Ownership-Change-Form.pdf";';
      }
      ?>
      document.getElementById('replace').innerHTML = '<h3>Select A Form To Print</h3>' +
        '<br>' +
        '<a class="n" href="' + all_package + '" target="_blank"><button type="button">Print Entire Packet (Blank)</button></a>' +
        '<br><br>' +
        '<a class="n" href="' + dealer_info_form + '" target="_blank"><button type="button">Blank Dealer Info Form</button></a>' +
        '<br><br>' +
        '<a class="n" href="' + dealer_contract_form + '" target="_blank"><button type="button">Blank Dealer Contract Form</button></a>' +
        '<br><br>' +
        '<a class="n" href="' + landlord_consent_form + '" target="_blank"><button type="button">Blank Landlord Consent Form</button></a>' +
        '<br><br>' +
        '<a class="n" href="' + display_order_form + '" target="_blank"><button type="button">Blank Display Order Form</button></a>' +
        '<br><br>' +
        '<a class="n" href="' + display_addon_form + '" target="_blank"><button type="button">Blank Display Addon Form</button></a>' +
        '<br><br>' +
        '<a class="n" href="' + display_relocation_form + '" target="_blank"><button type="button">Blank Display Relocation Form</button></a>' +
        '<br><br>' +
        '<a class="n" href="' + display_removal_form + '" target="_blank"><button type="button">Blank Display Removal Form</button></a>' +
        '<br><br>' +
        '<a class="n" href="' + blank_site_pan + '" target="_blank"><button type="button">Blank Site Pan</button></a>' +
        '<br><br>' +
        '<a class="n" href="' + ownership_change_form + '" target="_blank"><button type="button">Blank Ownership Change Form</button></a>';
    }

    function get_blanks_v2(org_id){
        let all_package = "#";
        let dealer_info_form = "#";
        let dealer_contract_form = "#";
        let landlord_consent_form = "#";
        let display_order_form = "#";
        let display_addon_form = "#";
        let display_relocation_form = "#";
        let display_removal_form = "#";
        let blank_site_pan = "#";
        let ownership_change_form = "#";
        let ESTIMATED_DELIVERY_DATES = "#";
        switch (org_id){
            case 162534:
                dealer_info_form = "forms/files/northedge/information_form.pdf";
                dealer_contract_form = "forms/files/northedge/dealer_services_contract_2020.pdf";
                landlord_consent_form = "forms/files/northedge/landlord_consent.pdf";
                blank_site_pan = "forms/files/northedge/site_plan.pdf";
                display_order_form = "forms/files/northedge/display_order_form_2021.pdf";
                display_addon_form = "forms/files/northedge/current_dealer_add_on_form-2021.pdf";
                display_relocation_form = "forms/files/northedge/display_relocation_form.pdf";
                display_removal_form = "forms/files/northedge/display_removal_order_form.pdf";
                ownership_change_form = "forms/files/northedge/ownership_change_form.pdf";
                all_package = "forms/files/northedge/all_package.pdf";
                document.getElementById('replace').innerHTML = '<h3>Select A Form To Print</h3>' +
                    '<br>' +
                    '<a class="n" href="' + all_package + '" target="_blank"><button type="button">Print Entire Packet (Blank)</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + dealer_info_form + '" target="_blank"><button type="button">Blank Dealer Info Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + dealer_contract_form + '" target="_blank"><button type="button">Blank Dealer Contract Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + landlord_consent_form + '" target="_blank"><button type="button">Blank Landlord Consent Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_order_form + '" target="_blank"><button type="button">Blank Display Order Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_addon_form + '" target="_blank"><button type="button">Blank Display Addon Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_relocation_form + '" target="_blank"><button type="button">Blank Display Relocation Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_removal_form + '" target="_blank"><button type="button">Blank Display Removal Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + blank_site_pan + '" target="_blank"><button type="button">Blank Site Pan</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + ownership_change_form + '" target="_blank"><button type="button">Blank Ownership Change Form</button></a>';

                break;
            case 615243:
                dealer_info_form = "forms/files/legacy/New_Dealer_Form.pdf";
                dealer_contract_form = "forms/files/legacy/New_Dealer_Contract.pdf";
                display_order_form = "forms/files/legacy/Dealer_Display_Order_Form.pdf";
                //All Steel
                landlord_consent_form = "forms/files/landlord-consent.pdf";
                display_order_form = "forms/files/display-order-form-2018(2).pdf";
                display_addon_form = "forms/files/display-addon.pdf";
                display_relocation_form = "forms/files/dealer-relocation-form.pdf";
                display_removal_form = "forms/files/dealer-relocation-form.pdf";
                blank_site_pan = "forms/files/ASC-site-plan.pdf";
                ownership_change_form = "forms/files/Ownership-Change-Form.pdf";
                all_package = "forms/files/new-dealer-packet.pdf";
                document.getElementById('replace').innerHTML = '<h3>Select A Form To Print</h3>' +
                    '<br>' +
                    '<a class="n" href="' + all_package + '" target="_blank"><button type="button">Print Entire Packet (Blank)</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + dealer_info_form + '" target="_blank"><button type="button">Blank Dealer Info Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + dealer_contract_form + '" target="_blank"><button type="button">Blank Dealer Contract Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + landlord_consent_form + '" target="_blank"><button type="button">Blank Landlord Consent Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_order_form + '" target="_blank"><button type="button">Blank Display Order Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_addon_form + '" target="_blank"><button type="button">Blank Display Addon Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_relocation_form + '" target="_blank"><button type="button">Blank Display Relocation Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_removal_form + '" target="_blank"><button type="button">Blank Display Removal Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + blank_site_pan + '" target="_blank"><button type="button">Blank Site Pan</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + ownership_change_form + '" target="_blank"><button type="button">Blank Ownership Change Form</button></a>';
                break;
            case 329698:
                //Integrity
                dealer_info_form = "forms/files/Integrity/Information Form.pdf";
                display_removal_form = "forms/files/Integrity/Display removal.pdf";
                landlord_consent_form = "forms/files/Integrity/Old landlord consent.pdf";
                ownership_change_form = "forms/files/Integrity/Ownership Change Form.pdf";
                all_package = "forms/files/Integrity/Integrity_pack.pdf";
                ESTIMATED_DELIVERY_DATES = "forms/files/Integrity/images.pdf";
                blank_site_pan = "forms/files/Integrity/Site_plan.pdf";
                document.getElementById('replace').innerHTML = '<h3>Select A Form To Print</h3>' +
                    '<br>' +
                    '<a class="n" href="' + all_package + '" target="_blank"><button type="button">Print Entire Packet (Blank)</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + dealer_info_form + '" target="_blank"><button type="button">Blank Dealer Info Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + landlord_consent_form + '" target="_blank"><button type="button">Blank Landlord Consent Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_removal_form + '" target="_blank"><button type="button">Blank Display Removal Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + ownership_change_form + '" target="_blank"><button type="button">Blank Ownership Change Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + ESTIMATED_DELIVERY_DATES + '" target="_blank"><button type="button">Blank Estimated Delivery Dates Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + blank_site_pan + '" target="_blank"><button type="button">Blank Site Pan</button></a>';
                break;
            case 191554:
                all_package = "forms/files/Infinity/new-dealer-packet.pdf";
                dealer_info_form = "forms/files/Infinity/Information_Form.pdf";
                dealer_contract_form = "forms/files/Infinity/Dealer_services_contract.pdf";
                landlord_consent_form = "forms/files/Infinity/landlord _consent.pdf"
                display_addon_form = "forms/files/Infinity/Estimate_delivery.pdf";
                display_relocation_form = "forms/files/Infinity/Time_off_request.pdf";
                display_removal_form = "forms/files/Infinity/Display_removal.pdf";
                blank_site_pan = "forms/files/Infinity/ASC-site-plan.pdf";
                ownership_change_form = "forms/files/Infinity/Ownership-Change-Form.pdf";
                document.getElementById('replace').innerHTML = '<h3>Select A Form To Print</h3>' +
                    '<br>' +
                    '<a class="n" href="' + all_package + '" target="_blank"><button type="button">Print Entire Packet (Blank)</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + dealer_info_form + '" target="_blank"><button type="button">Blank Dealer Info Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + dealer_contract_form + '" target="_blank"><button type="button">Blank Dealer Contract Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + landlord_consent_form + '" target="_blank"><button type="button">Blank Landlord Consent Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_addon_form + '" target="_blank"><button type="button">Blank Estimate Delivery Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_relocation_form + '" target="_blank"><button type="button">Blank Time off Request Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_removal_form + '" target="_blank"><button type="button">Blank Display Removal Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + blank_site_pan + '" target="_blank"><button type="button">Blank Site Pan</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + ownership_change_form + '" target="_blank"><button type="button">Blank Ownership Change Form</button></a>';
                break;
                case 841963:
                    // star
                    dealer_contract_form = "forms/files/star/Property_owner_consent.pdf";
                    landlord_consent_form = "forms/files/star/images.pdf";
                    all_package = "forms/files/star/all_pack.pdf";
                    display_removal_form = "forms/files/star/Display_removal.pdf";
                    dealer_info_form = "forms/files/star/Information_Form.pdf";
                    blank_site_pan = "forms/files/star/SitePlan.pdf";
                    ownership_change_form = "forms/files/star/Ownership_Change_Form.pdf";
                    document.getElementById('replace').innerHTML = '<h3>Select A Form To Print</h3>' +
                        '<br>' +
                        '<a class="n" href="' + all_package + '" target="_blank"><button type="button">Print Entire Packet (Blank)</button></a>' +
                        '<br><br>' +
                        '<a class="n" href="' + dealer_info_form + '" target="_blank"><button type="button">Blank Dealer Info Form</button></a>' +
                        '<br><br>' +
                        '<a class="n" href="' + dealer_contract_form + '" target="_blank"><button type="button">Blank Property Owner Consent Form</button></a>' +
                        '<br><br>' +
                        '<a class="n" href="' + landlord_consent_form + '" target="_blank"><button type="button">Blank Estimated Delivery Dates Form</button></a>' +
                        '<br><br>' +
                        '<a class="n" href="' + display_removal_form + '" target="_blank"><button type="button">Blank Display Removal Form</button></a>' +
                        '<br><br>' +
                        '<a class="n" href="' + blank_site_pan + '" target="_blank"><button type="button">Blank Site Pan</button></a>' +
                        '<br><br>' +
                        '<a class="n" href="' + ownership_change_form + '" target="_blank"><button type="button">Blank Ownership Change Form</button></a>';
                    break;
            default:
                all_package = "forms/files/new-dealer-packet.pdf";
                dealer_info_form = "forms/files/dealer-info-form.pdf";
                dealer_contract_form = "forms/files/dealer-contract-2019.pdf";
                landlord_consent_form = "forms/files/landlord-consent.pdf";
                display_order_form = "forms/files/display-order-form-2018(2).pdf";
                display_addon_form = "forms/files/display-addon.pdf";
                display_relocation_form = "forms/files/dealer-relocation-form.pdf";
                display_removal_form = "forms/files/dealer-relocation-form.pdf";
                blank_site_pan = "forms/files/ASC-site-plan.pdf";
                ownership_change_form = "forms/files/Ownership-Change-Form.pdf";
                document.getElementById('replace').innerHTML = '<h3>Select A Form To Print</h3>' +
                    '<br>' +
                    '<a class="n" href="' + all_package + '" target="_blank"><button type="button">Print Entire Packet (Blank)</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + dealer_info_form + '" target="_blank"><button type="button">Blank Dealer Info Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + dealer_contract_form + '" target="_blank"><button type="button">Blank Dealer Contract Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + landlord_consent_form + '" target="_blank"><button type="button">Blank Landlord Consent Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_order_form + '" target="_blank"><button type="button">Blank Display Order Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_addon_form + '" target="_blank"><button type="button">Blank Display Addon Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_relocation_form + '" target="_blank"><button type="button">Blank Display Relocation Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + display_removal_form + '" target="_blank"><button type="button">Blank Display Removal Form</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + blank_site_pan + '" target="_blank"><button type="button">Blank Site Pan</button></a>' +
                    '<br><br>' +
                    '<a class="n" href="' + ownership_change_form + '" target="_blank"><button type="button">Blank Ownership Change Form</button></a>';
        }
    }

    function send_completed(id) {
      var rep_id = '<?php echo $_SESSION['user_id']; ?>';
      var rep_name = '<?php echo $_SESSION['full_name']; ?>';
      if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
      } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          //document.getElementById("livesearch").innerHTML=this.responseText;
          alert(this.responseText);
          //window.location.reload();

        }
      }
      xmlhttp.open("GET", "<?php if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') { echo 'https'; }else{ echo 'http'; } ?>://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/forms/new-dealer-form/php/send-complete-packet.php?id=" + id + "&rep_id=" + rep_id + "&rep_name=" + rep_name, true);
      xmlhttp.send();
    }

    function get_dealers(state) {
      if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
      } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("d_s").innerHTML = this.responseText;
        }
      }
      xmlhttp.open("GET", "<?php if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') { echo 'https'; }else{ echo 'http'; } ?>://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/php/get_dealers_by_state.php?state=" + state, true);
      xmlhttp.send();
    }
  </script>
</head>

<body>

  <div id="wrapper">

    <!-- Navigation -->
    <?php include 'nav.php'; ?>

    <div id="page-wrapper">

      <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;"><small> Print Forms</small>
            </h1>
            <ol class="breadcrumb">
              <li>
                <i class="fa fa-dashboard"></i> Dashboard
              </li>
              <li class="active">
                <i class="fa fa-file"></i> Print Forms
              </li>
            </ol>
          </div>
        </div>
        <!-- /.row -->

        <!-- This is a notification -->
        <?php
        $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
        $notget = mysqli_query($conn, $notq);
        while ($notr = mysqli_fetch_array($notget)) {
          echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
        }
        ?>
        <!-- /.row -->


        <!--This is where real content for the page will go-->
        <div class="col-lg-6" id="replace">
          <u>
            <h4>Please Select a <?php switch($_SESSION['org_id']) {
                    case "832122":
                        echo "Customer ";
                        break;
                    case "123456":
                    case '532748':
                        echo "Client ";
                        break;
                    default:
                        echo 'Dealer ';
                } ?>:</h4>
          </u>
          <select onchange="get_dealers(this.value);">
            <option value="default">Select A State...</option>
            <?php
            $dlq = "SELECT DISTINCT `state` FROM `new_dealer_form` WHERE `inactive` != 'Yes' ORDER BY `state`";
            $dlg = mysqli_query($conn, $dlq) or die($conn->error);
            while ($dlr = mysqli_fetch_array($dlg)) {
              echo '<option value="' . $dlr['state'] . '">' . $dlr['state'] . '</option>';
            }
            ?>
          </select>
          <br><br>
          <span id="d_s"></span>
          <br><br>
          <button type="button" onclick="get_forms('<?php echo $_SESSION['full_name']; ?>');">Get Electronic Forms</button>
          <button type="button" onclick="get_blanks_v2(<?php
            $cadena2 =str_replace(' ', '', $_SESSION['org_id']);
            echo $cadena2;
            ?>);">Get Blank Forms</button>
          <br>
          <span style="color:red;" id="response"></span>
          <br><br>
          <!--<div>
									<u><h4>Employee Forms:</h4></u>
									<a class="n" href="forms/files/time-off-request.pdf" target="_blank"><button type="button">Time Off Request Form</button></a>
								</div>-->
        </div>
        <div class="col-lg-6" id="replace2">

        </div>

        <!--
							<iframe src="http://www.marketforceapp.com/marketforce/forms/new-dealer-form/index.html" style="width: 100%; height: 800px;">

              </iframe>
							-->
      </div>
      <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- jQuery -->
  <script src="js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="js/bootstrap.min.js"></script>

  <!-- Morris Charts JavaScript -->
  <script src="js/plugins/morris/raphael.min.js"></script>
  <script src="js/plugins/morris/morris.min.js"></script>
  <script src="js/plugins/morris/morris-data.js"></script>



  <?php include 'footer.html';
  if ($_GET['ider']) {
    echo '<script>
					get_forms();
					</script>';
  }
  ?>
</body>

</html>