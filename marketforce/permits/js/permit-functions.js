//Encode Data for transmission via XHR...
function encodeURL(url){
  url = url.replace(/&/g, '%26'); 
  url = url.replace(/#/g, '%23');
  return url;
}

//Load Permit Modal Form...
function load_permit_modal(mode){
	document.getElementById('permit_id').value = mode;
}

//Save Permit...
function save_permit(){
	var pid = document.getElementById('permit_id').value;
	var permit_unit = document.getElementById('permit_unit').value;
  if(permit_unit === ''){
    document.getElementById('permit_error').innerHTML = '*Please Enter The Unit Information!';
    return;
  }
  permit_unit = encodeURL(permit_unit);
	var permit_registration = document.getElementById('permit_registration').value;
  if(permit_registration === ''){
    document.getElementById('permit_error').innerHTML = '*Please Enter The Registration Information!';
    return;
  }
  permit_registration = encodeURL(permit_registration);
	var permit_street1 = document.getElementById('permit_street1').value;
  if(permit_street1 === ''){
    document.getElementById('permit_error').innerHTML = '*Please Enter The Street1 Address!';
    return;
  }
  permit_street1 = encodeURL(permit_street1);
	var permit_street2 = document.getElementById('permit_street2').value;
  /*if(permit_street2 === ''){
    document.getElementById('permit_error').innerHTML = '*Please Enter The Street2 Address!';
    return;
  }*/
  permit_street2 = encodeURL(permit_street2);
	var permit_city = document.getElementById('permit_city').value;
  if(permit_city === ''){
    document.getElementById('permit_error').innerHTML = '*Please Enter The City!';
    return;
  }
  permit_city = encodeURL(permit_city);
	var permit_state = document.getElementById('permit_state').value;
  if(permit_state === ''){
    document.getElementById('permit_error').innerHTML = '*Please Enter The State!';
    return;
  }
  permit_state = encodeURL(permit_state);
	var permit_zip = document.getElementById('permit_zip').value;
  if(permit_zip === ''){
    document.getElementById('permit_error').innerHTML = '*Please Enter The Zip Code!';
    return;
  }
  permit_zip = encodeURL(permit_zip);
  var permit_registration_exp = document.getElementById('permit_registration_exp').value;
  if(permit_registration_exp === ''){
    document.getElementById('permit_error').innerHTML = '*Please Enter The Registration Information!';
    return;
  }
  permit_registration_exp = encodeURL(permit_registration_exp);
	var permit_bond = document.getElementById('permit_bond').value;
  if(permit_bond === ''){
    document.getElementById('permit_error').innerHTML = '*Please Enter The Bond Information!';
    return;
  }
  permit_bond = encodeURL(permit_bond);
	var permit_bond_exp = document.getElementById('permit_bond_exp').value;
  if(permit_bond_exp === ''){
    document.getElementById('permit_error').innerHTML = '*Please Enter The Bond Expiration!';
    return;
  }
  permit_bond_exp = encodeURL(permit_bond_exp);
	var permit_fee = document.getElementById('permit_fee').value;
  if(permit_fee === ''){
    document.getElementById('permit_error').innerHTML = '*Please Enter The Permit Fee!';
    return;
  }
  permit_fee = encodeURL(permit_fee);
	var permit_note = document.getElementById('permit_notes').value;
  /*if(permit_note === ''){
    document.getElementById('permit_error').innerHTML = '*Please Enter Notes Regarding The Permit!';
    return;
  }*/
  permit_note = encodeURL(permit_note);

  //Close Modal...
  document.getElementById('permit_close_btn').click();

	if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
		alert(this.responseText);
    	window.location.reload();
    }
  }
  xmlhttp.open("GET","permits/php/save-permit.php?pid="+pid+
  	"&rep_id="+rep_id+
  	"&rep_name="+rep_name+
  	"&permit_unit="+permit_unit+
  	"&permit_registration="+permit_registration+
  	"&permit_street1="+permit_street1+
  	"&permit_street2="+permit_street2+
  	"&permit_city="+permit_city+
    "&permit_state="+permit_state+
    "&permit_zip="+permit_zip+
    "&permit_registration_exp="+permit_registration_exp+
    "&permit_bond="+permit_bond+
    "&permit_bond_exp="+permit_bond_exp+
    "&permit_fee="+permit_fee+
    "&permit_note="+permit_note,true);
  xmlhttp.send();
}


//Edit Permit Function...
function edit_permit(pid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {

      var r = JSON.parse(this.responseText);
      if(r.http_status === 'GOOD'){
        //Good Code...
        console.log(r);
      }else if(r.http_status === 'ERROR'){
        alert(r.http_response);
      }else{
        alert('There was an error processing your request! Please Contact Support: (866) 791-6131');
      }

    }
  }
  xmlhttp.open("GET","permits/php/fetch-permit.php?pid="+pid,true);
  xmlhttp.send();
}