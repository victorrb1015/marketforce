 <div class="modal fade" role="dialog" tabindex="-1" id="add-permit-form">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="modal-title text-center">Add Permit</h3>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <input type="hidden" id="permit_id" name="permit_id" value="" class="form-control"  />
                    <div class="col-md-12">
                        <p class="text-primary">Unit:</p><input type="text" id="permit_unit" style="width:100%;" name="permit_unit" placeholder="Unit" class="form-control">
                        <p style="color: rgba(51,51,51,0);">Paragraph</p>
                    </div>
                    <div class="col-md-12">
                        <p class="text-primary">Address:</p><input type="text" id="permit_street1" style="width:100%;" name="permit_street1" placeholder="Street 1" class="form-control">
                        <p></p><input type="text" id="permit_street2" style="width:100%;" name="permit_street2" placeholder="Street 2" class="form-control">
                        <p></p>
                    </div>
                    <div class="col-md-6">
                        <p class="text-primary">County:</p><input type="text" id="permit_city" style="width:100%;" name="permit_county" placeholder="County" class="form-control">
                        <p style="color: rgba(51,51,51,0);">Paragraph</p>
                    </div>
                    <div class="col-md-6">
                        <p class="text-primary">City:</p><input type="text" id="permit_city" style="width:100%;" name="permit_city" placeholder="City" class="form-control">
                        <p style="color: rgba(51,51,51,0);">Paragraph</p>
                    </div>
                    <div class="col-md-6">
                        <p class="text-primary">State:</p>
                        <select class="input-sm form-control" id="permit_state" name="permit_state" style="width: 100%;">
                            <option value="">Select State</option>
                            <?php
                            $ssq = "SELECT * FROM `states` ORDER BY `state` ASC";
                            $ssg = mysqli_query($conn, $ssq) or die($conn->error);
                            while($ssr = mysqli_fetch_array($ssg)){
                                echo '<option value="' . $ssr['state'] . '">' . $ssr['state'] . '</option>';
                            }
                            ?>
                        </select>
                        <p style="color: rgba(51,51,51,0);">Paragraph</p>
                    </div>
                    <div class="col-md-6">
                        <p class="text-primary">Zip Code:</p>
                        <input type="text" id="permit_zip" style="width:100%;" name="permit_zip" placeholder="Zip Code" class="form-control">
                        <p style="color: rgba(51,51,51,0);">Paragraph</p>
                    </div>
                    <div class="col-md-12">
                        <p class="text-primary">Registration:</p>
                        <input type="text" id="permit_registration" style="width: 100%;" name="permit_registration" placeholder="Registration Number" class="form-control">
                        <p style="color: rgba(51,51,51,0);">Paragraph</p>
                    </div>
                    <div class="col-md-6">
                        <p class="text-primary">Expiration:</p>
                        <input type="text" id="permit_registration_exp" style="width: 100%;" name="permit_registration_exp" placeholder="Enter Expiration Date" class="form-control date">
                        <p style="color: rgba(51,51,51,0);">Paragraph</p>
                    </div>
                    <div class="col-md-6">
                        <p class="text-primary">Bond Number:</p><input type="text" id="permit_bond" style="width: 100%;" name="permit_bond" placeholder="Bond Number" class="form-control">
                        <p style="color: rgba(51,51,51,0);">Paragraph</p>
                    </div>
                    <div class="col-md-6">
                        <p class="text-primary">Bond Expiration:</p>
                        <input type="text" id="permit_bond_exp" style="width: 100%;" name="permit_bond_exp" placeholder="Expiration Date" class="form-control date">
                        <p style="color: rgba(51,51,51,0);">Paragraph</p>
                    </div>
                    <div class="col-md-6">
                        <p class="text-primary">Fee:</p>
                        <input type="text" id="permit_fee" name="permit_fee" style="width: 100%;" placeholder="Enter Fee Amount" class="form-control">
                        <p style="color: rgba(51,51,51,0);">Paragraph</p>
                    </div>
                    <div class="col-md-12">
                        <p class="text-primary">Notes:</p>
                        <textarea id="permit_notes" style="width: 100%;" name="permit_notes" placeholder="Enter Notes Here..." class="form-control"></textarea>
                        <p></p>
                    </div>
                    <p style="color: rgba(51,51,51,0);">Paragraph</p>
                  </div>
                </div>
                <div class="modal-footer">
                    <p id="permit_error" style="color:red;font-weight:bold;"></p>
                    <button class="btn btn-default" data-dismiss="modal" type="button" id="permit_close_btn">Close</button>
                    <button class="btn btn-primary" type="button" onclick="save_permit();">Save</button>
                </div>
            </div>
        </div>
    </div>