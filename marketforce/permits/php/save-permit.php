<?php
include '../../php/connection.php';

//Load Variables...
$pid = mysqli_real_escape_string($conn, $_GET['pid']);
$rep_id = mysqli_real_escape_string($conn, $_GET['rep_id']);
$rep_name = mysqli_real_escape_string($conn, $_GET['rep_name']);
$permit_unit = mysqli_real_escape_string($conn, $_GET['permit_unit']);
$permit_registration = mysqli_real_escape_string($conn, $_GET['permit_registration']);
$permit_registration_exp = date("Y-m-d",strtotime($_GET['permit_registration_exp']));
$permit_street1 = mysqli_real_escape_string($conn, $_GET['permit_street1']);
$permit_street2 = mysqli_real_escape_string($conn, $_GET['permit_street2']);
$permit_city = mysqli_real_escape_string($conn, $_GET['permit_city']);
$permit_state = mysqli_real_escape_string($conn, $_GET['permit_state']);
$permit_zip = mysqli_real_escape_string($conn, $_GET['permit_zip']);
$permit_bond = mysqli_real_escape_string($conn, $_GET['permit_bond']);
$permit_bond_exp = date("Y-m-d",strtotime($_GET['permit_bond_exp']));
$permit_fee = mysqli_real_escape_string($conn, $_GET['permit_fee']);
$permit_notes = mysqli_real_escape_string($conn, $_GET['permit_note']);

//Perform Work...

//Check if Request is New...
if($pid == 'New'){
	//Add To Database...
	$iq = "INSERT INTO `building_permits`
			(
			`date`,
			`time`,
			`rep_id`,
			`rep_name`,
			`permit_unit`,
			`permit_registration`,
			`permit_street1`,
			`permit_street2`,
			`permit_city`,
			`permit_state`,
			`permit_zip`,
			`permit_bond`,
			`permit_bond_exp`,
			`permit_fee`,
			`permit_notes`,
			`status`,
			`inactive`
			)
			VALUES
			(
			CURRENT_DATE,
			CURRENT_TIME,
			'" . $rep_id . "',
			'" . $rep_name . "',
			'" . $permit_unit . "',
			'" . $permit_registration . "',
			'" . $permit_street1 . "',
			'" . $permit_street2 . "',
			'" . $permit_city . "',
			'" . $permit_state . "',
			'" . $permit_zip . "',
			'" . $permit_bond . "',
			'" . $permit_bond_exp . "',
			'" . $permit_fee . "',
			'" . $permit_notes . "',
			'Active',
			'No'
			)";
mysqli_query($conn, $iq) or die($conn->error);

	echo 'Your Permit has been added!';

}else{
	//UPDATE Database...
	$uq = "UPDATE `building_permits` SET
			`permit_unit` = '" . $permit_unit . "',
			`permit_registration` = '" . $permit_registration . "',
			`permit_street1` = '" . $permit_street1 . "',
			`permit_street2` = '" . $permit_street2 . "',
			`permit_city` = '" . $permit_city . "',
			`permit_state` = '" . $permit_state . "',
			`permit_zip` = '" . $permit_zip . "',
			`permit_bond` = '" . $permit_bond . "',
			`permit_bond_exp` = '" . $permit_bond_exp . "',
			`permit_fee` = '" . $permit_fee . "',
			`permit_notes` = '" . $permit_notes . "'
			WHERE `ID` = '" . $pid . "'";
	mysqli_query($conn, $uq) or die($conn->error);

	echo 'Your Permit Information has been updated!';

}


?>