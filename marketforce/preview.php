<?php
include 'security/session/session-settings.php';
include 'php/connection.php';
$pageName = 'Preview';
$pageIcon = 'fas fa-file';

echo '<script>
        var rep_id = "' . $_SESSION['user_id'] . '";
        var rep_name = "' . $_SESSION['full_name'] . '";
      </script>';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>MarketForce | All Steel</title>
  <?php include 'global/sections/head.php'; ?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css" integrity="sha512-8bHTC73gkZ7rZ7vpqUQThUDhqcNFyYi2xgDgPDHc+GXVGHXq+xPjynxIopALmOPqzo9JZj0k6OqqewdGO3EsrQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>

<body>
  <!-- Preloader -->
  <?php include 'global/sections/preloader.php'; ?>
  <!-- /Preloader -->
  <div class="wrapper theme-4-active pimary-color-red">

    <!--Navigation-->
    <?php include 'global/sections/nav.php'; ?>


    <!-- Main Content -->
    <div class="page-wrapper">
      <!--Includes Footer-->

      <div class="container-fluid pt-25">
        <?php include 'global/sections/page-title-bar.php'; ?>

        <!--Main Content Here-->
        <div class="row">
		    <!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
            <div class="col-lg-12">
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                    <strong>
                      
                      <!-- Message Preview Text -->
<p><i class="fa fa-code-fork"></i> UPDATE: MarketForce has been updated to Version 3.5.0 which includes:</p>
<br>
<ol style="padding-left:20px;">
  <li>New, improved version of the Scheduler application.</li>
</ol>
<br>
<p>If you have any questions, please contact Support @ <a href="mailto:support@marketforceapp.com" style="color:white;">support@marketforceapp.com</a></p>
<p>Your feedback helps us provide the best tools for you in the workplace! If you ever experience issues, please raise a support ticket.</p>
                      <!-- END Message Preview Text -->  
                      
                  </strong>
                </div>
            </div>
        </div>
        
        <!-- jQuery Live Search -->
        <div class="ui search">
        	<input class="prompt" type="text" placeholder="Search...">
        	<div class="results"></div>
    	</div>
    	
    	

        <!-- Footer -->
        <?php include 'global/sections/footer.php'; ?>
        <!-- /Footer -->

      </div>
      <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->

    <!--Footer-->
    <?php include 'global/sections/includes.php'; ?> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.js" integrity="sha512-dqw6X88iGgZlTsONxZK9ePmJEFrmHwpuMrsUChjAw1mRUhUITE5QU9pkcSox+ynfLhL15Sv2al5A0LVyDCmtUw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
    	$('.ui.search').search({
		    type: 'category',
        apiSettings   : {
        onResponse: function(searchResponse) {
          var
            response = {
              results : {}
            }
          ;
          // translate GitHub API response to work with search
          $.each(searchResponse, function(index, item) {
            console.log('Main: '+index);
            // create new language category
            var list_name = index;
            if(response.results[list_name] === undefined) {
              response.results[list_name] = {
                name    : list_name,
                results : []
              };
            }
            $.each(item.colors, function(index,item){
              console.log('Sub: '+item);
              response.results[list_name].results.push({title:item});
            });
          });
          return response;
        },
        url: 'orders/php/get-colors.php'
        },
        minCharacters: 3
		  });
    	</script>
</body>
<!-- JS Files -->
  
<!--Modals-->

</html>