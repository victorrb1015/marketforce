<?php
include '../php/connection.php';

//Load Variables
$mode = $_POST['mode'];
$rep_id = $_POST['rep_id'];
$rep_name = $_POST['rep_name'];
$date = date("Y-m-d",strtotime($_POST['date']));
$idate = date("mdY",strtotime($_POST['date']));
$cat = $_POST['cat'];
$city = mysqli_real_escape_string($conn, $_POST['city']);
$state = $_POST['state'];
$item = mysqli_real_escape_string($conn, $_POST['iname']);
$amount = mysqli_real_escape_string($conn, $_POST['amount']);
$miles = mysqli_real_escape_string($conn, $_POST['miles']);
$ppg = mysqli_real_escape_string($conn, $_POST['ppg']);
$num_gallons = mysqli_real_escape_string($conn, $_POST['num_gallons']);
$notes = mysqli_real_escape_string($conn, $_POST['notes']);
$rID = $_POST['rID'];

//Receipt Image Uploader...
if($_FILES['fileToUpload']['size'] != 0) {
$uid = uniqid();
$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/../img-storage/pc_receipts/";
$target_file2 = $target_dir . basename($_FILES["fileToUpload"]["name"]);


$uploadOk = 1;
$imageFileType = pathinfo($target_file2,PATHINFO_EXTENSION);
$target_file = $target_dir . $uid . "_receipt_" . $idate . "." . $imageFileType;

echo $target_file2;
$log = fopen("activity-log.txt","a");
fwrite($log, date("m/d/y h:iA") . " --> " . $rep_name . "\n");
fwrite($log, "Uploaded: " . $target_file2 . " Converted to: " . $target_file . "\n");
fwrite($log, "---------------------------------------------------------------------------------------------\n");
fclose($log);

// Check if image file is a actual image or fake image
//if(isset($_POST['mode'])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
      //  echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
//}
// Check if file already exists
/*if (file_exists($target_file)) {
    echo "<br>ERROR!- file already exists.";
    $uploadOk = 0;
}*/
// Check file size (turned off for now)
/*if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "<br>Sorry, your file is too large.";
    $uploadOk = 0;
}*/
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "JPG" ) {
    echo "<br>ERROR!- only JPG, JPEG, PNG & GIF files are allowed.";
    //echo "<br><script>alert('" . $imageFileType . "');</script>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "<br>ERROR!- your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      
      //INSERT FILE PATH INTO DATABASE
      $file_name_1 = 'http://burtonsolution.tech/img-storage/receipts/' . $uid . "_receipt_" . $idate . "." . $imageFileType;
      
        echo "<br>The file '". basename( $_FILES["fileToUpload"]["name"]). "' has been uploaded.";
        
    }else{
      echo '<br>There was an error moving your file from the temporary location to the permanent location.';
    }
}
}//End File Size Check to see if file exists...


//INSERT Expense Item...
if($mode == 'Add'){
$q = "INSERT INTO `pc_transactions`
      (
      `rep_id`,
      `rep_name`,
      `date`,
      `time`,
      `item_date`,
      `category`,
      `city`,
      `state`,
      `item`,
      `amount`,
      `miles`,
      `ppg`,
      `num_gallons`,
      `notes`,
      `receipt_img`,
      `reportID`
      )
      VALUES
      (
      '" . $rep_id . "',
      '" . $rep_name . "',
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $date . "',
      '" . $cat . "',
      '" . $city . "',
      '" . $state . "',
      '" . $item . "',
      '" . $amount . "',
      '" . $miles . "',
      '" . $ppg . "',
      '" . $num_gallons . "',
      '" . $notes . "',
      '" . $file_name_1 . "',
      '" . $rID . "'
      )";
  mysqli_query($conn, $q) or die($conn->error);
  
}

//Re-Direct page back to main Petty Cash Page...
if($rID == ''){
 echo '<script>
      window.location = "../petty-cash.php";
      </script>';
}else{
echo '<script>
      window.location = "../petty-cash.php?rid=' . $rID . '";
      </script>';
}
?>