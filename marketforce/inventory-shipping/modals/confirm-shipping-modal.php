	<!-- sample modal content -->
		<div id="confirmShippingModal" class="modal fade" tabindex="-1" role="modal" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria="true">×</button>
						<h5 class="modal-title" id="myModalLabel">Confirm Shipping</h5>
					</div>
					<div class="modal-body">

						<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<h6 class="text-warning">
                    Reference Number: 
                    <input type="text" id="ship_ref" name="example-input1-group2" class="form-control" placeholder="Reference Number Here...">
                  </h6>
                    <br>
                  <h6 class="text-warning">
                    Customer: 
                    <select id="customer_id" class="form-control">
                      <option value="">Select Customer</option>
                   <?php
                      $cq = "SELECT * FROM `order_customers` WHERE `inactive` != 'Yes' ORDER BY `company_name` ASC";
                      $cg = mysqli_query($conn, $cq) or die($conn->error);
                      while($cr = mysqli_fetch_array($cg)){
                        echo '<option value="' . $cr['ID'] . '">' . $cr['company_name'] . '</option>';
                      }
                   ?>
                    </select>
                  </h6>
								</div>
							</div>
						</div>	
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary" onclick="confirm_shipping();">Submit</button>
                <span id="ship_ref_error" style="color:red;font-weight:bold;"></span>
									</div>
										</div>
											<!-- /.modal-content -->
											</div>
												<!-- /.modal-dialog -->
													</div>
														<!-- /.modal -->