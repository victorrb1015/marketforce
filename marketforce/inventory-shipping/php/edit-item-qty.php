<?php
include '../../php/connection.php';

//Load Variables...
$iid = $_GET['iid'];
$nqty = $_GET['nqty'];

//Get Current Qty...
$cq = "SELECT * FROM `inv_ship_items` WHERE `inactive` != 'Yes' AND `shipping_number` = '' AND `user_id` = '" . $_SESSION['user_id'] . "' AND `item_id` = '" . $iid . "'";
$cg = mysqli_query($conn, $cq) or die($conn->error);
$cr = mysqli_fetch_array($cg);
$cqty = mysqli_num_rows($cg);

$adjustment = $nqty - $cqty;

if($adjustment == 0){
  //Do Nothing...
}elseif($adjustment > 0){
  $i = 0;
  while($i < $adjustment){
    $iq = "INSERT INTO `inv_ship_items`
           (
           `date`,
           `time`,
           `item_id`,
           `item_name`,
           `item_barcode`,
           `user_id`,
           `user_name`,
           `inactive`
           )
           VALUES
           (
           CURRENT_DATE,
           CURRENT_TIME,
           '" . $cr['item_id'] . "',
           '" . $cr['item_name'] . "',
           '" . $cr['item_barcode'] . "',
           '" . $_SESSION['user_id'] . "',
           '" . $_SESSION['full_name'] . "',
           'No'
           )";
    mysqli_query($conn, $iq) or die($conn->error);
    $i++;
  }
}elseif($adjustment < 0){
  $ai = $adjustment;
  while($ai < 0){
    $iq = "UPDATE `inv_ship_items` SET `inactive` = 'Yes' WHERE `inactive` != 'Yes' AND `user_id` = '" . $_SESSION['user_id'] . "' AND `item_id` = '" . $cr['item_id'] . "' LIMIT 1";
    mysqli_query($conn, $iq) or die($conn->error);
    $ai = $ai + 1;
  }
}

$x->response = 'GOOD';
$x->message = 'Qty was updated to ' . $nqty;

$response = json_encode($x);
echo $response;


?>