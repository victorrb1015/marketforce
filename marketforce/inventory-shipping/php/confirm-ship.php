<?php
include '../../php/connection.php';

//Load Variables...
$ship_ref = mysqli_real_escape_string($conn, $_GET['ship_ref']);
$cid = mysqli_real_escape_string($conn, $_GET['cid']);
$inv_zone = $_SESSION['inventory_zone'];
$sn = uniqid();


//Make Inventory Adjustments...
$q = "SELECT DISTINCT `item_id` FROM `inv_ship_items` WHERE `inactive` != 'Yes' AND `shipping_number` = '' AND `user_id` = '" . $_SESSION['user_id'] . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  $iq = "SELECT * FROM `inv_ship_items` WHERE `inactive` != 'Yes' AND `shipping_number` = '' AND `user_id` = '" . $_SESSION['user_id'] . "' AND `item_id` = '" . $r['item_id'] . "'";
  $ig = mysqli_query($conn, $iq) or die($conn->error);
  $ir = mysqli_fetch_array($ig);
  $iqty = mysqli_num_rows($ig);
  
  //Get current inventory level...
  $ciq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $r['item_id'] . "'";
  $cig = mysqli_query($conn, $ciq) or die($conn->error);
  $cir = mysqli_fetch_array($cig);
  $cqty = $cir['qty'];
  $nqty = $cqty - $iqty;
  $adjustment = $nqty - $cqty;
  //Make Adjustment to DB...
  $iuq = "UPDATE `inventory_items` SET `qty` = (`qty` - '" . $iqty . "'), `qty_" . $inv_zone . "` = (`qty_" . $inv_zone . "` - " . $iqty . ") WHERE `ID` = '" . $r['item_id'] . "'";
  mysqli_query($conn, $iuq) or die($conn->error);
  
  $iaq = "INSERT INTO `inventory_adjustments`
          (
          `date`,
          `time`,
          `pid`,
          `current_qty`,
          `new_qty`,
          `adjustment`,
          `type`,
          `shipping_number`,
          `user_id`,
          `user_name`,
          `status`,
          `inactive`
          )
          VALUES
          (
          CURRENT_DATE,
          CURRENT_TIME,
          '" . $r['item_id'] . "',
          '" . $cqty . "',
          '" . $nqty . "',
          '" . $adjustment . "',
          'Shipping',
          '" . $sn . "',
          '" . $_SESSION['user_id'] . "',
          '" . $_SESSION['full_name'] . "',
          'Pending',
          'No'
          )";
  mysqli_query($conn, $iaq) or die($conn->error);
}

//Confirm Receiving Items...
$uq = "UPDATE `inv_ship_items` SET 
        `confirmed_date` = CURRENT_DATE,
        `confirmed_time` = CURRENT_TIME,
        `shipping_number` = '" . $sn . "' 
        WHERE `inactive` != 'Yes' AND `shipping_number` = '' AND `user_id` = '" . $_SESSION['user_id'] . "'";
mysqli_query($conn, $uq) or die($conn->error);


//Add receiving manifest record...
$iq = "INSERT INTO `inv_ship_manifests`
      (
      `date`,
      `time`,
      `shipping_number`,
      `ship_ref`,
      `customer_id`,
      `user_id`,
      `user_name`,
      `status`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $sn . "',
      '" . $ship_ref . "',
      '" . $cid . "',
      '" . $_SESSION['user_id'] . "',
      '" . $_SESSION['full_name'] . "',
      'Pending',
      'No'
      )";
mysqli_query($conn, $iq) or die($conn->error);

$x->response = 'GOOD';
$x->message = 'The manifest has been confirmed!';

$response = json_encode($x);
echo $response;


?>
