<?php
include '../../php/connection.php';

//Load Variables...
$bc = mysqli_real_escape_string($conn,$_GET['bc']);


//Get Items Details...
$q = "SELECT * FROM `inventory_items` WHERE `barcode` = '" . $bc . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
if(mysqli_num_rows($g) > 0){
  
$r = mysqli_fetch_array($g);
//Add to rec DB...
$iq = "INSERT INTO `inv_ship_items` 
      (
      `date`,
      `time`,
      `item_id`,
      `item_name`,
      `item_barcode`,
      `item_zone`,
      `user_id`,
      `user_name`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $r['ID'] . "',
      '" . $r['item_name'] . "',
      '" . $bc . "',
      ' ',
      '" . $_SESSION['user_id'] . "',
      '" . $_SESSION['full_name'] . "',
      'No'
      )";
  mysqli_query($conn, $iq) or die($conn->error);
  
  //Get current item count...
  $cq = "SELECT * FROM `inv_ship_items` WHERE `inactive` != 'Yes' AND `shipping_number` = '' AND `user_id` = '" . $_SESSION['user_id'] . "'";
  $cg = mysqli_query($conn, $cq) or die($conn->error);
  $icount = mysqli_num_rows($cg);
      
  $x->response = 'GOOD';
  $x->message = $r['item_name'] . ' has been added to the shipping list.';
  $x->item_id = $r['ID'];
  $x->item_name = $r['item_name'];
  $x->item_count = $icount;
  
}else{
  $x->response = 'ERROR';
  $x->message = 'No Item Found With Barcode: ' . $bc;
}

$response = json_encode($x);
echo $response;

?>