<?php
include 'security/session/session-settings.php';
include 'php/connection.php';

$pageName = 'Receiving';
$pageIcon = 'fas fa-dolly';
$cache_buster = uniqid();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>MarketForce | All Steel</title>
    <?php include 'global/sections/head.php'; ?>
</head>

<body>
	<!-- Preloader -->
	<?php include 'global/sections/preloader.php'; ?>
	<!-- /Preloader -->
    <div class="wrapper theme-4-active pimary-color-red">

    	<!--Navigation-->
    	<?php include 'global/sections/nav.php'; ?>
		
		
        <!-- Main Content -->
		<div class="page-wrapper"><!--Includes Footer-->

      <div class="container-fluid pt-25">
				<?php include 'global/sections/page-title-bar.php'; ?>
        
        <!--Main Content Here-->

    <!-- Title -->
    <?php include 'inventory-receiving/sections/title.php'; ?>
		<!-- Barcode -->
		<?php include 'inventory-receiving/sections/barcode.php'; ?>
        
			<!-- Table -->
		<?php include 'inventory-receiving/sections/items-table.php'; ?>
        
			
					<button type="button" class="btn btn-default" onclick="clear_items();">Clear</button>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmReceivingModal">Confirm</button>
		
				<!-- /Table -->

				<?php include 'inventory-receiving/modals/confirm-receiving-modal.php'; ?>
				<?php include 'inventory/modals/add-edit-item-modal.php'; ?>
			
			<!-- Footer -->
			<?php include 'global/sections/footer.php'; ?>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!--Footer-->
	<?php include 'global/sections/includes.php'; ?>
  <script src="inventory-receiving/js/receiving-functions.js?cd=<?php echo $cache_buster; ?>"></script>
  <script src="inventory-receiving/js/new-item-functions.js?cd=<?php echo $cache_buster; ?>"></script>
</body>

</html>
