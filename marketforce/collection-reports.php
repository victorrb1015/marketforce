<?php
include 'security/session/session-settings.php';

if($_SESSION['collections'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
		th, td{
			padding: 10px;
		}
		#dl, dc{
			height: 430px;
		}
		.panel-body{
			height: 382px;
		}
		#scroll{
			overflow: scroll;
		}
		::-webkit-scrollbar {
    width: 0px;  /* remove scrollbar space */
    background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
		
		
		.packet-table td, th{
			border: 1px solid black;
		}
		.packet-table{
			margin:auto;
		}
    
    #hoverGreen{
      color:blue;
    }
    #hoverGreen:hover{
      color:green;
    }
  </style>
  <script>
    var rep_id = '<?php echo $_SESSION['user_id']; ?>';
    var rep_name = '<?php echo $_SESSION['full_name']; ?>';
    
  function clr_res(id,s){
    //alert("test");
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			//alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://library.burtonsolution.tech/php/reserve.php?id="+id+"&s="+s,true);
  xmlhttp.send();
  }
		
	function report_filter(){
		var year = document.getElementById('f_year').value;
		var quarter = document.getElementById('f_quarter').value;
		var month = document.getElementById('f_month').value;
		var week = document.getElementById('f_week').value;
		var day = document.getElementById('f_day').value;
		if(day != ''){
			year = '';
			quarter = '';
			month = '';
			week = '';
		}
		if(week != ''){
			year = '';
			quarter = '';
			month = '';
			day = '';
		}
		if(quarter != ''){
			month = '';
			week = '';
			day = '';
		}
		
		var URL = "collection-reports.php?year="+year+"&quarter="+quarter+"&month="+month+"&week="+week+"&day="+day;
		var eURL = URL;
	 
      window.location = eURL;
	}
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
       <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> Collection Reports</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
														<li>
															<i class="fa fa-line-chart"></i> Collection Reports
													</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							
							
							
							<!--<div class="row" id="filtered-reports">
								<ol class="breadcrumb" style="text-align:center;">
									<li>Filtered Graphical Reporting</li>
								</ol>
							</div>-->
							
							<div class="row" style="background-color:#E7404C;">
								<table style="margin:auto;">
									<tr>
										<td>
											<i class="fa fa-filter fa-2x"></i> 
										</td>
									
										<!--<td>
											<button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#filterModal">
												Filter Data
											</button>
										</td>-->
										<td>
											<b>Year:</b>
										<select id="f_year">
										<option value="">N/A</option>
											<?php
									$i = 0;
									$y = 2016;
					while($i <= 9){
						$y = $y + 1;
						echo '<option value="' . $y . '">' . $y . '</option>';
						$i++;
					}
											?>
										</select>
									</td>
										<td>
						<b>Quarter:</b>
						<select id="f_quarter">
							<option value="">N/A</option>
							<option value="1">Q1</option>
							<option value="2">Q2</option>
							<option value="3">Q3</option>
							<option value="4">Q4</option>
						</select>
					</td>
						<td>
							<b>Month:</b>
							<select id="f_month">
								<option value="">N/A</option>
								<option value="1">January</option>
								<option value="2">February</option>
								<option value="3">March</option>
								<option value="4">April</option>
								<option value="5">May</option>
								<option value="6">June</option>
								<option value="7">July</option>
								<option value="8">August</option>
								<option value="9">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>
					</td>
					<td>
						<b>Week of:</b>
						<!--<select id="f_week">
							<option value="">N/A</option>
							<option value="1">Week 1</option>
							<option value="2">Week 2</option>
							<option value="3">Week 3</option>
							<option value="4">Week 4</option>
						</select>-->
						<input type="date" id="f_week" />
					</td>
					<td>
						<b>Day:</b>
						<input type="date" id="f_day" />
						<!--<select id="f_day">
							<option value="">N/A</option>
							<?php
							$i = 1;
							while($i <= 31){
								echo '<option value="' . $i . '">' . $i . '</option>';
								$i++;
							}
							?>
						</select>-->
					</td>
										<td>
												<button type="button" class="btn btn-sm btn-default" onclick="report_filter();">
													<i class="fa fa-filter"></i> Filter
												</button>
										</td>
										<td>
											<a href="collection-reports.php">
												<button type="button" class="btn btn-sm btn-default" style="">
													<i class="fa fa-times"></i> Clear
												</button>
											</a>
										</td>
									</tr>
									<tr>
										<td></td>
									<?php
										$c = 0;
									if($_GET['year'] != ''){
										echo '<td><span style="font-size:12px;" class="label label-primary">Year: ' . $_GET['year'] . '</span></td>';
										$c++;
									}
									if($_GET['quarter'] != ''){
										echo '<td><span style="font-size:12px;" class="label label-primary">Quarter: ' . $_GET['quarter'] . '</span></td>';
										$c++;
									}
									if($_GET['month'] != ''){
										echo '<td><span style="font-size:12px;" class="label label-primary">Month: ' . $_GET['month'] . '</span></td>';
										$c++;
									}
									if($_GET['week'] != ''){
										echo '<td><span style="font-size:12px;" class="label label-primary">Week of: ' . $_GET['week'] . '</span></td>';
										$c++;
									}
									if($_GET['day'] != ''){
										echo '<td><span style="font-size:12px;" class="label label-primary">Day: ' . $_GET['day'] . '</span></td>';
										$c++;
									}
									if($c == 0){
										//echo '<td><span style="font-size:12px;" class="label label-primary">Day: Today&apos;s Date</span></td>';
										echo '<td><span style="font-size:12px;" class="label label-primary">Month: This Month</span></td>';
									}
									//if($_GET['year'] != '' && $_GET['quarter'] != '' && $_GET['month'] != '' && $_GET['week'] != '' && $_GET['day'] != ''){
									/*if(!$_GET['year'] && !$_GET['quarter'] && !$_GET['month'] && !$_GET['week'] && !$_GET['day']){
									
									}else{
										echo '<li><a href="collection-reports.php#filtered-reports"><button type="button" class="btn btn-sm btn-default" style=""><i class="fa fa-times"></i> Clear Filter</button></a></li>';
									}*/
									?>
									</tr>
								</table>
							</div><!--END ROW-->
							<br>
							
							<div class="row">
								
								<div class="col-lg-6">
									<div class="panel panel-danger">
										<div class="panel-heading">
											<h3 class="panel-title">
												Report For Items In Collections 
									<?php
									$cc = 0;
									if($_GET['year'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Year: ' . $_GET['year'] . '</span>';
										$cc++;
									}
									if($_GET['quarter'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Quarter: ' . $_GET['quarter'] . '</span>';
										$cc++;
									}
									if($_GET['month'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Month: ' . $_GET['month'] . '</span>';
										$cc++;
									}
									if($_GET['week'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Week of: ' . $_GET['week'] . '</span>';
										$cc++;
									}
									if($_GET['day'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Day: ' . $_GET['day'] . '</span>';
										$cc++;
									}
									if($cc == 0){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Month: This Month</span>';
									}
								?>
											</h3>
										</div>
										<div class="panel-body" id="in-collections-report" style="overflow:scroll;">
											
										</div>
									</div>
								</div>
								
								
								<div class="col-lg-6">
									<div class="panel panel-success">
										<div class="panel-heading">
											<h3 class="panel-title">
												Report For Items Collected 
								<?php
									$cc = 0;
									if($_GET['year'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Year: ' . $_GET['year'] . '</span>';
										$cc++;
									}
									if($_GET['quarter'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Quarter: ' . $_GET['quarter'] . '</span>';
										$cc++;
									}
									if($_GET['month'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Month: ' . $_GET['month'] . '</span>';
										$cc++;
									}
									if($_GET['week'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Week of: ' . $_GET['week'] . '</span>';
										$cc++;
									}
									if($_GET['day'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Day: ' . $_GET['day'] . '</span>';
										$cc++;
									}
									if($cc == 0){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Month: This Month</span>';
									}
								?>
											</h3>
										</div>
										<div class="panel-body" id="collected-report" style="overflow:scroll;">
											
										</div>
									</div>
								</div>
								
							</div><!--END ROW-->
								
								
								
								<div class="row">
								
								<!--Graphical Report 1-->
								<div class="col-lg-4">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Collections By Category 
								<?php
									$cc = 0;
									if($_GET['year'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Year: ' . $_GET['year'] . '</span>';
										$cc++;
									}
									if($_GET['quarter'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Quarter: ' . $_GET['quarter'] . '</span>';
										$cc++;
									}
									if($_GET['month'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Month: ' . $_GET['month'] . '</span>';
										$cc++;
									}
									if($_GET['week'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Week of: ' . $_GET['week'] . '</span>';
										$cc++;
									}
									if($_GET['day'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Day: ' . $_GET['day'] . '</span>';
										$cc++;
									}
									if($cc == 0){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Month: This Month</span>';
									}
								?>
											</h3>
										</div>
										<div class="panel-body" id="by_cat">
											
										</div>
									</div>
								</div>
								<!--END Graphical Report 1-->
									
								<!--Graphical Report 2-->
								<div class="col-lg-4">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Financed vs. Unfinanced
								<?php
									$cc = 0;
									if($_GET['year'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Year: ' . $_GET['year'] . '</span>';
										$cc++;
									}
									if($_GET['quarter'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Quarter: ' . $_GET['quarter'] . '</span>';
										$cc++;
									}
									if($_GET['month'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Month: ' . $_GET['month'] . '</span>';
										$cc++;
									}
									if($_GET['week'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Week of: ' . $_GET['week'] . '</span>';
										$cc++;
									}
									if($_GET['day'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Day: ' . $_GET['day'] . '</span>';
										$cc++;
									}
									if($cc == 0){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Month: This Month</span>';
									}
								?>
											</h3>
										</div>
										<div class="panel-body" id="fvuf">
											
										</div>
									</div>
								</div>
								<!--END Graphical Report 2-->
                
                
                <!--Graphical Report 3-->
								<div class="col-lg-4">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Paid vs Unpaid
								<?php
									$cc = 0;
									if($_GET['year'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Year: ' . $_GET['year'] . '</span>';
										$cc++;
									}
									if($_GET['quarter'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Quarter: ' . $_GET['quarter'] . '</span>';
										$cc++;
									}
									if($_GET['month'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Month: ' . $_GET['month'] . '</span>';
										$cc++;
									}
									if($_GET['week'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Week of: ' . $_GET['week'] . '</span>';
										$cc++;
									}
									if($_GET['day'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Day: ' . $_GET['day'] . '</span>';
										$cc++;
									}
									if($cc == 0){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Month: This Month</span>';
									}
								?>
											</h3>
										</div>
										<div class="panel-body" id="pvup">
											
										</div>
									</div>
								</div>
								<!--END Graphical Report 3-->
                  
                  
							
              </div><!--End Row-->
							
							
							
							<div class="row">
								
								<!--Graphical Report 1-->
								<div class="col-lg-6">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Communications &nbsp;&nbsp;&nbsp;&nbsp;
												<a href="collections/view-calls.php?year=<? echo $_GET['year']; ?>&quarter=<? echo $_GET['quarter']; ?>&month=<? echo $_GET['month']; ?>&week=<? echo $_GET['week']; ?>&day=<? echo $_GET['day']; ?>" target="_blank">
													<button type="button" class="btn btn-warning btn-xs">View Call Breakdown</button>
												</a>
								<?php
									$cc = 0;
									if($_GET['year'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Year: ' . $_GET['year'] . '</span>';
										$cc++;
									}
									if($_GET['quarter'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Quarter: ' . $_GET['quarter'] . '</span>';
										$cc++;
									}
									if($_GET['month'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Month: ' . $_GET['month'] . '</span>';
										$cc++;
									}
									if($_GET['week'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Week of: ' . $_GET['week'] . '</span>';
										$cc++;
									}
									if($_GET['day'] != ''){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Day: ' . $_GET['day'] . '</span>';
										$cc++;
									}
									if($cc == 0){
										echo '&nbsp;&nbsp;<span style="font-size:12px;" class="label label-primary">Month: This Month</span>';
									}
								?>
											</h3>
										</div>
										<div class="panel-body" id="comms">
											
										</div>
									</div>
								</div>
								<!--END Graphical Report 1-->
								
								
								<!--Graphical Report 1-->
								<div class="col-lg-6">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Latest Activity
											</h3>
										</div>
										<div class="panel-body" id="scroll">
											<?php
                      
                      $collnq = "SELECT * FROM `collections_calls` ORDER BY `date` DESC LIMIT 30";
									
									$collng = mysqli_query($conn, $collnq) or die($conn->error);
									
									while($collnr = mysqli_fetch_array($collng)){
										
										if($collnr['type'] == 'Note'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-comments-o"></i> ' . $collnr['rep_name'] . ' ' . date("m/d/y h:i A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . mysqli_real_escape_string($conn,$collnr['note']) . '
													</div>';
                      $notes++;
										}
										if($collnr['type'] == 'Phone Call'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-phone"></i> ' . $collnr['rep_name'] . ' ' . date("m/d/y h:i A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . $collnr['rep_name'] . ' attempted calling ' . $collnr['name'] . ' @ ' . $collnr['phone_tried'] . '. ' . $collnr['note'] . '
													</div>';
                      $calls++;
										}
                    if($collnr['type'] == 'Email'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-envelope"></i> ' . $collnr['rep_name'] . ' ' . date("m/d/y h:i A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . $collnr['rep_name'] . ' Automatically Emailed ' . $collnr['name'] . '.
													</div>';
                      $emails++;
										}
                    if($collnr['type'] == 'Text'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-mobile"></i> ' . $collnr['rep_name'] . ' ' . date("m/d/y h:i A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . $collnr['rep_name'] . ' Automatically SMS Texted ' . $collnr['name'] . '.
													</div>';
                      $texts++;
										}
										if($collnr['type'] == 'Payment'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-usd"></i> ' . $collnr['rep_name'] . ' ' . date("m/d/y h:i A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . $collnr['rep_name'] . ' recorded a ' . $collnr['method'] . ' payment for ' . $collnr['name'] . ' in the amount of $' . $collnr['amount'] . '.';
											if($collnr['method'] == 'Check'){
												echo '<br>Check#: ' . $collnr['check_num'];
											}
											if($collnr['method'] == 'Credit Card'){
												echo '<br>Confirmation#: ' . $collnr['cc_conf'];
											}
													echo '</div>';
                      $payments++;
										}
									}
                      
                      ?>
										</div>
									</div>
								</div>
								<!--END Graphical Report 1-->
								
								
							</div><!--END ROW-->
							
							
							
							<div class="row">
								
								<!--Graphical Report 1-->
								<div class="col-lg-12">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Monthly In-Collections Totals
											</h3>
										</div>
										<div class="panel-body" id="monthly">
											
										</div>
									</div>
								</div>
								<!--END Graphical Report 1-->
							
								
              </div><!--End Row-->
							
							
							
							</div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>

  <?php 
  include 'collections/calc-total.php'; 
  
  	echo '<script>
				//alert("' . $dain . '");
				</script>';
	
		echo '<script>
						document.getElementById("in-collections-report").innerHTML = "<table class=\"table-bordered\" style=\"width:100%;\"><tr>"+
																																					"<th>By Category</th><th>Totals</th></tr><tr>"+
																																					"<td>"+
																																					"<p><b>ck_ca_cc:</b> $' . $coll_ck_ca_cc . '</p>"+
																																					"</td><td>"+
																																					"<p><b>Financed Total:</b> $' . $coll_fin_total . '</p>"+
																																					"</td></tr><tr><td>"+
																																					"<p><b>po:</b> $' . $coll_po . '</p>"+
																																					"</td><td>"+
																																					"<p><b>Non-Financed Total:</b> $' . $coll_non_fin_total . '</p>"+
																																					"</td></tr><tr><td>"+
																																					"<p><b>nsf_stop:</b> $' . $coll_nsf_stop . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>repair:</b> $' . $coll_repair . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>dealer_pmt:</b> $' . $coll_dealer . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>dealer_fin:</b> $' . $coll_dealer_fin . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>legal:</b> $' . $coll_legal . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>bli:</b> $' . $coll_bli . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>ezpay:</b> $' . $coll_ezpay . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>scoggin:</b> $' . $coll_scoggin . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>double_d:</b> $' . $coll_double_d . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>dallen_legacy:</b> $' . $coll_dallen_legacy . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>rto_national:</b> $' . $coll_rto_national . '</p>"+
																																					"</td><td>"+
																																					"</td></tr></table>";
																																					
						document.getElementById("collected-report").innerHTML = "<table class=\"table-bordered\" style=\"width:100%;\"><tr>"+
																																					"<th>By Category</th><th>Totals</th></tr><tr>"+
																																					"<td>"+
																																					"<p><b>ck_ca_cc:</b> $' . $paid_ck_ca_cc . '</p>"+
																																					"</td><td>"+
																																					"<p><b>Financed Total:</b> $' . $paid_fin_total . '</p>"+
																																					"</td></tr><tr><td>"+
																																					"<p><b>po:</b> $' . $paid_po . '</p>"+
																																					"</td><td>"+
																																					"<p><b>Non-Financed Total:</b> $' . $paid_non_fin_total . '</p>"+
																																					"</td></tr><tr><td>"+
																																					"<p><b>nsf_stop:</b> $' . $paid_nsf_stop . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>repair:</b> $' . $paid_repair . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>dealer_pmt:</b> $' . $paid_dealer . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>dealer_fin:</b> $' . $paid_dealer_fin . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>legal:</b> $' . $paid_legal . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>bli:</b> $' . $paid_bli . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>ezpay:</b> $' . $paid_ezpay . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>scoggin:</b> $' . $paid_scoggin . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>double_d:</b> $' . $paid_double_d . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>dallen_legacy:</b> $' . $paid_dallen_legacy . '</p>"+
																																					"</td><td></td></tr><tr><td>"+
																																					"<p><b>rto_national:</b> $' . $paid_rto_national . '</p>"+
																																					"</td><td>"+
																																					"</td></tr></table>";
					</script>';
	
									echo'<script>
												// Donut Chart
  										  Morris.Donut({
  										      element: "pvup",
  										      data: [{
  										          label: "Collected",
  										          value: ' . $paid_total . ',
                                color: "green"
  										      }, {
  										          label: "In Collections",
  										          value: ' . $coll_total . ',
                                color: "red"
  										      }],
  										      resize: true
  										  });
                        
                        // Donut Chart
  										  Morris.Donut({
  										      element: "fvuf",
  										      data: [{
  										          label: "Financed",
  										          value: ' . $coll_fin_total . ',
                                color: "green"
  										      }, {
  										          label: "Not Financed",
  										          value: ' . $coll_non_fin_total . ',
                                color: "red"
  										      }],
  										      resize: true
  										  });
												
												 // Donut Chart
  										  Morris.Donut({
  										      element: "by_cat",
  										      data: [{
  										          label: "ck_ca_cc",
  										          value: ' . $coll_ck_ca_cc . ',
																color: "red"
  										      }, {
  										          label: "po",
  										          value: ' . $coll_po . ',
																color: "orange"
  										      }, {
  										          label: "nsf_stop",
  										          value: ' . $coll_nsf_stop . ',
																color: "yellow"
  										      }, {
  										          label: "repair",
  										          value: ' . $coll_repair . ',
																color: "lightgreen"
  										      }, {
  										          label: "dealer_pmt",
  										          value: ' . $coll_dealer . ',
																color: "green"
  										      }, {
  										          label: "dealer_fin",
  										          value: ' . $coll_dealer_fin . ',
																color: "darkgreen"
  										      }, {
  										          label: "legal",
  										          value: ' . $coll_legal . ',
																color: "cyan"
  										      }, {
  										          label: "bli",
  										          value: ' . $coll_bli . ',
																color: "purple"
  										      }, {
  										          label: "ezpay",
  										          value: ' . $coll_ezpay . ',
																color: "blue"
  										      }, {
  										          label: "scoggin",
  										          value: ' . $coll_scoggin . ',
																color: "violet"
  										      }, {
  										          label: "double_d",
  										          value: ' . $coll_double_d . ',
																color: "magenta"
  										      }, {
  										          label: "dallen_legacy",
  										          value: ' . $coll_dallen_legacy . ',
																color: "rose"
  										      },  {
  										          label: "rto_national",
  										          value: ' . $coll_rto_national . ',
																color: "black"
  										      }],
  										      resize: true
  										  });
												</script>';
  
  
 include 'collections/calc-comms.php';
  
  echo '<script>
                        // Bar Chart
  										  Morris.Bar({
  										      element: "comms",
  										      data: [{
                                device: "Calls Made",
                                geekbench: ' . $calls . '
                            }, {
                                device: "Notes Added",
                                geekbench: ' . $notes . '
                            }, {
                                device: "Emails Sent",
                                geekbench: ' . $emails . '
                            }, {
                                device: "Texts Sent",
                                geekbench: ' . $texts . '
                            },
														 {
                                device: "Payments Made",
                                geekbench: ' . $payments . '
                            }],
                            xkey: "device",
                            ykeys: ["geekbench"],
                            labels: ["QTY"],
                            barRatio: 0.4,
                            xLabelAngle: 25,
                            hideHover: "auto",
                            barColors: ["blue"],
  										      resize: true
  										  });
                        
                </script>';
  
  include 'collections/calc-monthly-totals.php';
  
  echo '<script>
   Morris.Bar({
    element: "monthly",
    data: [{
        month: "Jan",
        financed: ' . $jan_f . ',
        unfinanced: ' . $jan_u . ',
				total: ' . $jan . '
    }, {
        month: "Feb",
        financed: ' . $feb_f . ',
        unfinanced: ' . $feb_u . ',
				total: ' . $feb . '
    }, {
        month: "Mar",
        financed: ' . $mar_f . ',
        unfinanced: ' . $mar_u . ',
				total: ' . $mar . '
    }, {
        month: "Apr",
        financed: ' . $apr_f . ',
        unfinanced: ' . $apr_u . ',
				total: ' . $apr . '
    }, {
        month: "May",
        financed: ' . $may_f . ',
        unfinanced: ' . $may_u . ',
				total: ' . $may . '
    }, {
        month: "Jun",
        financed: ' . $jun_f . ',
        unfinanced: ' . $jun_u . ',
				total: ' . $jun . '
    }, {
        month: "Jul",
        financed: ' . $jul_f . ',
        unfinanced: ' . $jul_u . ',
				total: ' . $jul . '
    }, {
        month: "Aug",
        financed: ' . $jan_f . ',
        unfinanced: ' . $jan_f . ',
				total: ' . $jan_f . '
    }, {
        month: "Sep",
        financed: ' . $sep_f . ',
        unfinanced: ' . $sep_u . ',
				total: ' . $sep . '
    }, {
        month: "Oct",
        financed: ' . $oct_f . ',
        unfinanced: ' . $oct_u . ',
				total: ' . $oct . '
    }, {
        month: "Nov",
        financed: ' . $nov_f . ',
        unfinanced: ' . $nov_u . ',
				total: ' . $nov . '
    }, {
        month: "Dec",
        financed: ' . $dec_f . ',
        unfinanced: ' . $dec_u . ',
				total: ' . $dec . '
    }],
    xkey: "month",
    ykeys: ["financed", "unfinanced", "total"],
    labels: ["Financed", "Non-Financed", "Total"],
    barRatio: 0.4,
    xLabelAngle: 35,
    hideHover: "auto",
    barColors: ["green","red","grey"],
    resize: true
});
</script>';
  
  ?>
	
 <!-- Modal -->
 <!-- <div class="modal fade" id="filterModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <!--<div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Filter Reports</h4>
        </div>
        <div class="modal-body">
					<?php
					$foq = "SELECT YEAR(`date`) as `year` FROM `collections` WHERE `inactive` != 'Yes'";
					//$fog = mysqli_query($conn, $foq) or die($conn->error);
					echo '<table>
								<tr>
									<td>
										<p>Year:</p>
										<select id="f_year">
										<option value="">N/A</option>';
									$i = 0;
									$y = 2017;
					while($i <= 9){
						$y = $y + $i;
						echo '<option value="' . $y . '">' . $y . '</option>';
						$i++;
					}
					echo '</select>
							</td>';
					?>
					<td>
						<p>Quarter:</p>
						<select id="f_quarter">
							<option value="">N/A</option>
							<option value="1">Q1</option>
							<option value="2">Q2</option>
							<option value="3">Q3</option>
							<option value="4">Q4</option>
						</select>
					</td>
						<td>
							<p>Month:</p>
							<select id="f_month">
								<option value="">N/A</option>
								<option value="1">January</option>
								<option value="2">February</option>
								<option value="3">March</option>
								<option value="4">April</option>
								<option value="5">May</option>
								<option value="6">June</option>
								<option value="7">July</option>
								<option value="8">August</option>
								<option value="9">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>
					</td>
					<td>
						<p>Week of:</p>
						<!--<select id="f_week">
							<option value="">N/A</option>
							<option value="1">Week 1</option>
							<option value="2">Week 2</option>
							<option value="3">Week 3</option>
							<option value="4">Week 4</option>
						</select>-->
					<!--	<input type="date" class="form-control" id="f_week" />
					</td>
					<td>
						<p>Day:</p>
						<select id="f_day">
							<option value="">N/A</option>
							<?php
							$i = 1;
							while($i <= 31){
								echo '<option value="' . $i . '">' . $i . '</option>';
								$i++;
							}
							?>
						</select>
					</td>
				</tr>
									
					</table>
					<br><br>
					<p id="error_msg" style="color:red;font-weight:bold;"></p>
        </div>
        <div class="modal-footer">
					<button type="button" class="btn btn-success" onclick="report_filter();">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>-->
	<!-- End Modal -->
	
	

	
 <?php include 'footer.html'; ?>
</body>

</html>