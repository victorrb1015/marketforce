<div class="modal fade" role="dialog" tabindex="-1" id="newRepair">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">New Repo Form</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="text-center text-primary bg-primary">Today's Date</p>
                            <p><strong>7/26/2018</strong></p>
                        </div>
                        <div class="col-md-6">
                            <p class="text-center text-primary bg-primary">Invoice Number:</p><input type="text" id="inv" style="width:100%;" name="inv"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><br>
                            <h4 class="text-center bg-primary">Contact Information</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p><strong>Customer Name</strong></p><input type="text" placeholder="Name" id="cname" style="width:100%;" name="cname">
                            <p>Customer Cell</p><input type="text" placeholder="Cell Phone" id="cphone" style="width:100%;" name="cphone">
                            <p>Customer Email</p><input type="text" placeholder="Email" id="cemail" style="width:100%;" name="cemail"><br></div>
                        <div class="col-md-6" style="background:rgba(51,122,183,0.1);">
                            <p class="text-primary">Repo Address:</p><input type="text" placeholder="Address" id="raddress" style="width:100%;" name="raddress"></div>
                        <div class="col-md-6" style="background:rgba(51,122,183,0.1);">
                            <p class="text-primary">City:</p><input type="text" placeholder="City" id="rcity" style="width:100%;" name="rcity"></div>
                        <div class="col-md-3" style="background:rgba(51,122,183,0.1);">
                            <p class="text-primary">State:</p><input type="text" placeholder="State" id="state" style="width:100%;" name="state"><br><br></div>
                        <div class="col-md-3" style="background:rgba(51,122,183,0.1);">
                            <p class="text-primary">Zip Code:</p><input type="text" placeholder="Zip Code" id="rzip" style="width:100%;" name="rzip"><br><br></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p><strong>Dealer Name</strong></p><select id="dname" name="dname"><optgroup label="This is a group"><option value="12" selected="">This is item 1</option><option value="13">This is item 2</option><option value="14">This is item 3</option></optgroup></select>
                            <p><strong>Contractor's Name</strong></p><input type="text" id="contractor_name" style="width:100%;" name="contractor_name"></div>
                        <div class="col-md-6" style="/*border:2px solid rgb(55,124,184);*/">
                            <p>Sale Date:</p><input type="date" id="sale_date" style="width:100%;" name="sale_date">
                            <p>Date Installed:</p><input type="date" id="install_date" style="width:100%;" name="install_date"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><br>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center bg-primary">Financial Information</h4>
                                </div>
                                <div class="col-md-4">
                                    <p>Finance Company:</p><select id="finance_company" name="finance_company"><optgroup label="This is a group"><option value="12" selected="">This is item 1</option><option value="13">This is item 2</option><option value="14">This is item 3</option></optgroup></select></div>
                                <div
                                    class="col-md-4">
                                    <p>Building Amount Financed:</p>
                                    <div class="input-group">
                                        <div class="input-group-addon"><span><i class="fa fa-dollar"></i></span></div><input class="form-control" type="text" id="amount_financed" name="amount_financed">
                                        <div class="input-group-addon"></div>
                                    </div>
                            </div>
                            <div class="col-md-4">
                                <p>Balance Due to CXL Repo:</p>
                                <div class="input-group">
                                    <div class="input-group-addon"><span><i class="fa fa-dollar"></i></span></div><input class="form-control" type="text" id="balance_due" name="balance_due">
                                    <div class="input-group-addon"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><br>
                        <h4 class="text-center bg-primary">Repo Information</h4>
                    </div>
                    <div class="col-md-6">
                        <p>Add Files:&nbsp;</p><input type="file" id="file_one" name="file_one"><input type="file" id="file_two" name="file_two"><input type="file" id="file_three" name="file_three"><input type="file" id="file_four" name="file_four">
                        <input
                            type="file" id="file_five" name="file_five"></div>
                    <div class="col-md-6">
                        <p>Repo Details:</p><textarea id="repo_details" style="width:100%;height:95px;" name="repo_details"></textarea></div>
                </div>
            </div>
            <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Close</button><button class="btn btn-primary" type="button">Save</button></div>
        </div>
    </div>
    </div>