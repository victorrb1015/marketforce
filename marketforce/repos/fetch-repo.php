<?php
include 'php/connection.php';

//Load Variables
$id = $_GET['id'];


$q = "SELECT * FROM `repos` WHERE `ID` = '" . $id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
$r = str_replace('"', '&quot;', $r);
$r = str_replace("'", "&apos;", $r);

if($r['sale_date'] == '' || $r['sale_date'] == '1969-12-31'){
  $sale_date = '';
}else{
  $sale_date = date("m/d/Y", strtotime($r['sale_date']));
}
if($r['install_date'] == '' || $r['install_date'] == '1969-12-31'){
  $install_date = '';
}else{
  $install_date = date("m/d/Y", strtotime($r['install_date']));
}

$response = '{
              "ID" : "' . $r['ID'] . '",
              "cname" : "' . $r['cname'] . '",
              "today_date" : "' . date("m/d/Y", strtotime($r['today_date'])) . '",
              "inv" : "' . $r['inv'] . '",
              "dname" : "' . $r['dname'] . '",
              "sale_date" : "' . $sale_date . '",
              "contractor_name" : "' . $r['contractor_name'] . '",
              "install_date" : "' . $install_date . '",
              "address" : "' . $r['address'] . '",
              "city" : "' . $r['city'] . '",
              "state" : "' . $r['state'] . '",
              "zip" : "' . $r['zip'] . '",
              "details" : "' . str_replace("\r\n", " ", $r['details']) . '",
              "cphone" : "' . $r['cphone'] . '",
              "cemail" : "' . $r['cemail'] . '",
              "contractor" : "' . $r['contractor'] . '",
              "did" : "' . $r['did'] . '",
              "img1" : "' . $r['img1'] . '",
              "img2" : "' . $r['img2'] . '",
              "img3" : "' . $r['img3'] . '",
              "img4" : "' . $r['img4'] . '",
              "img5" : "' . $r['img5'] . '"
              }';
              

echo $response;

?>            