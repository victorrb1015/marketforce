<?php
include '../php/connection.php';
$_SESSION['org_db_name'] = 'mburton9_mf_allsteelcarports';
include '../php/connection.php';
//error_reporting(E_ALL);
/*$paid = 0;
//Get Items All Invoices...
$iq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `status` = 'paid' AND `paid_date` = CURRENT_DATE";
$ig = mysqli_query($conn, $iq) or die($conn->error);
while($ir = mysqli_fetch_array($ig)){
  $iiq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $ir['inv'] . "'";
  $iig = mysqli_query($conn, $iiq) or die($conn->error);
  while($iir = mysqli_fetch_array($iig)){
    $paid = $paid + $iir['item_price'];
  }
}

echo 'PAID: ' . number_format($paid,2) . '<br>';*/
echo '<html>
      <head>
      <title>Wholesale Breakdown Report</title>
      <style>
      html{
        background:#3c3c3c;
      }
      body{
        background:white;
        max-width: 8.5in;
        min-height: 11in;
        margin:auto;
        padding:10px;
      }
      table{
        margin:auto;
      }
      th,td{
        border:1px solid black;
        padding:5px;
      }
      </style>
      </head>
      <body>';

echo '<h1 style="text-align:center;">Wholesale Breakdown Report</h1>';

echo '<table>
      <thead>
      <tr>
      <th>Date</th>
      <th>Customer</th>
      <th>Inv#</th>
      <th>Amount</th>
      <th>Status</th>
      <th>View</th>
      </tr>
      </thead>
      <tbody>';

$owed = 0;
//Get Items All Invoices...
$iq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes'";
$ig = mysqli_query($conn, $iq) or die($conn->error);
while($ir = mysqli_fetch_array($ig)){
  echo '<tr>';
  echo '<td>' . date('m/d/y',strtotime($ir['date'])) . '</td>';
  echo '<td>' . $ir['cname'] . '</td>';
  echo '<td>' . $ir['inv'] . '</td>';
  $iiq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $ir['inv'] . "'";
  $iig = mysqli_query($conn, $iiq) or die($conn->error);
  $owed = 0;
  while($iir = mysqli_fetch_array($iig)){
    $owed = $owed + $iir['item_price'];
  }
  echo '<td><b>' . number_format($owed,2) . '</b></td>';
  echo '<td><b>' . $ir['status'] . '</b></td>';
  echo '<td><a href="' . $ir['inv_file_url'] . '" target="_blank" style="cursor:pointer;background:green;font-weight:bold;padding:5px;color:black;border-radius:1.5rem;">View Invoice</a></td>';
  echo '</tr>';
  
}
echo '</tbody>
      </table>
      </body>
      </html>';
//echo 'OWED: ' . number_format($owed,2) . '<br>';
?>