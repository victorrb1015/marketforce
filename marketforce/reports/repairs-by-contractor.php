<?php
include '../php/connection.php';
error_reporting(0);


if(!$_GET['state']){
  echo '<html style="text-align:center;">
        <h1>Select A Contractor</h1>
        <form action="repairs-by-contractor.php" method="get">
        <select name="state">
        <option value="ALL">*ALL</option>';
  
  $q = "SELECT DISTINCT `contractor_name` FROM `repairs` WHERE `inactive` != 'Yes' ORDER BY `contractor_name` ASC";
  $g = mysqli_query($conn, $q) or die($conn->error);
  while($r = mysqli_fetch_array($g)){
    echo '<option value="' . $r['contractor_name'] . '">' . $r['contractor_name'] . '</option>';
  }
  
  echo '</select>
        <br><br>
        <input type="submit" value="Submit" />
        </form>
        </html>';
  
  break;
}

echo '<html>
      <head>
        <title>Repairs By Contractor</title>
        <style>
          table{
            border: 1px solid black;
            margin:auto;
          }
          th, td{
            border: 1px solid black;
            padding: 10px;
          }
        </style>
      </head>
      <body>';


if($_GET['state'] == 'ALL'){
echo '<h1 style="text-align:center;">Master Repairs List By Contractor</h1>';

  $dq = "SELECT * FROM `repairs` WHERE `inactive` != 'Yes' ORDER BY `cname` ASC";
  $dg = mysqli_query($conn, $dq) or die($conn->error);
  echo '<table>
          <tr>
            <th>Name</th>
            <th>Date</th>
            <th>Location</th>
            <th>Contractor</th>
            <th>Repair Cost</th>
            <th>Status</th>
          </tr>';
  while($dr = mysqli_fetch_array($dg)){
  echo '<tr>
          <td>' . $dr['cname'] . '</td>
          <td>' . date("m/d/y", strtotime($dr['today_date'])) . '</td>
          <td>' . $dr['city'] . ', ' . $dr['state'] . '</td>
          <td>' . $dr['contractor_name'] . '</td>
          <td>$' . $dr['total_cost'] . '</td>
          <td>' . $dr['status'] . '</td>
        </tr>';
  }
  echo '</table>';

}else{//If equals ALL -- END

 echo '<h1 style="text-align:center;">Master Repairs List for ' . $_GET['state'] . '</h1>';

$q = "SELECT * FROM `repairs` WHERE `contractor_name` = '" . $_GET['state'] . "' AND `inactive` != 'Yes' ORDER BY `cname` ASC";
$g = mysqli_query($conn, $q) or die($conn->error);
  echo '<table>
          <tr>
            <th>Name</th>
            <th>Date</th>
            <th>Location</th>
            <th>Contractor</th>
            <th>Repair Cost</th>
            <th>Status</th>
          </tr>';
while($dr = mysqli_fetch_array($g)){
  
  echo '<tr>
          <td>' . $dr['cname'] . '</td>
          <td>' . date("m/d/y", strtotime($dr['today_date'])) . '</td>
          <td>' . $dr['city'] . ', ' . $dr['state'] . '</td>
          <td>' . $dr['contractor_name'] . '</td>
          <td>$' . $dr['total_cost'] . '</td>
          <td>' . $dr['status'] . '</td>
        </tr>';
  }
  echo '</table>';
}
  
echo '</body>
      </html>';



?>