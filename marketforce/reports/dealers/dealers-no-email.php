<?php
include '../php/connection.php';

echo '<html>
    <head>
      <title>Dealers With No Emails And No Fax</title>
      <style>
      html{
        background: #3C3C3C;
      }
      body{
        margin: 25px auto;
        width: 8.5in;
        min-height: 11in;
        background:#FFFFFF;
      }
      table{
        border: 1px solid black;
      }
      td{
        border: 1px solid black;
        padding: 10px;
      }
      </style>
    </head>
    <body>
    <h1>Master Survey List</h1>
      <table>
      <thead>
        <tr>
          <th>Dealer Name</th>
          <th>Email</th>
          <th>Dealer Phone#</th>
          <th>Dealer Cell#</th>
          <th>Dealer Fax#</th>
        </tr>
        </thead>';

$sq = "SELECT * FROM `new_dealer_form` WHERE `inactive` != 'Yes' AND `bizname` != '' AND (`email` = '' OR `email` = 'none' OR `fax` = '' OR `fax` = 'none') ORDER BY `bizname` ASC";
$sg = mysqli_query($conn, $sq) or die($conn->error);
while($sr = mysqli_fetch_array($sg)){
  
  echo '<tbody><tr>
          <td>' . $sr['bizname'] . '</td>
          <td>' . $sr['email'] . '</td>
          <td>' . $sr['phone'] . '</td>
          <td>' . $sr['dcphone'] . '</td>
          <td style="whitespace:nowrap;">' . $sr['fax'] . '</td>
       </tr></tbody>';
  
}

echo '</table>
</body>
</html>';

?>