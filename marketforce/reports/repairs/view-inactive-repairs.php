<?php
include '../php/connection.php';

echo '<html>
    <head>
      <title>Repairs Inactive Report</title>
      <style>
      html{
        background:#3c3c3c;
      }
      body{
        background:white;
        max-width: 8.5in;
        min-height: 11in;
        margin:auto;
      }
      table{
        border: 1px solid black;
        margin:auto;
      }
      td{
        border: 1px solid black;
        padding: 10px;
      }
      </style>
      <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
    <h1 style="text-align:center;">Repairs Inactive Report</h1>
      <table>
      <thead>
        <tr>
          <th>Customer Name</th>
          <th>Inv#</th>
          <th>Date</th>
          <th>Status</th>
          <th>View</th>';
      if($_SESSION['repairs_submit'] == 'Yes'){
          echo '<th>Restore</th>';
      }
  echo '</tr>
        </thead>
        <tbody>';

$sq = "SELECT * FROM `repairs` WHERE `inactive` = 'Yes' OR `status` = 'Cancelled' OR `status` = 'Completed' ORDER BY `cname` ASC";
$sg = mysqli_query($conn, $sq) or die($conn->error);
while($sr = mysqli_fetch_array($sg)){
  echo '<tr>
          <td style="padding: 10px;">' . $sr['cname'] . '</td>
          <td style="padding: 10px;">' . $sr['inv'] . '</td>
          <td style="padding: 10px;">' . date("m/d/y", strtotime($sr['today_date'])) . '</td>
          <td style="padding: 10px;"><b>' . $sr['status'] . '</b></td>
          <td style="padding: 10px;">
            <a href="../../repairs/view-repair.php?id=' . $sr['ID'] . '" target="_blank">
              <button type="button" class="btn btn-primary btn-md" style="background:blue;color:white;font-weight:bold;cursor:pointer;">View Repair</button>
            </a>
          </td>';
  
      if($_SESSION['repairs_submit'] == 'Yes'){
          echo '<td style="padding: 10px;">
            <a href="../../repairs/php/restore-repair.php?rid=' . $sr['ID'] . '" target="_blank">
              <button type="button" class="btn btn-primary btn-md" style="background:green;color:white;font-weight:bold;cursor:pointer;">Restore To Draft</button>
            </a>
          </td>';
      }
       echo '</tr>';
  
  $style = '';
}

echo '</tbody>
</table>
</body>
</html>';

?>