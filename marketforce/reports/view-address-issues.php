<?php
include '../php/connection.php';
error_reporting(0);


if(!$_GET['state']){
  echo '<html style="text-align:center;">
        <h1>Select A State</h1>
        <form action="view-address-issues.php" method="get">
        <select name="state">
        <option value="ALL">*ALL</option>';
  
  $q = "SELECT DISTINCT `state` FROM `dealers` WHERE `inactive` != 'Yes'";
  $g = mysqli_query($conn, $q) or die($conn->error);
  while($r = mysqli_fetch_array($g)){
    echo '<option value="' . $r['state'] . '">' . $r['state'] . '</option>';
  }
  
  echo '</select>
        <br><br>
        <input type="submit" value="Submit" />
        </form>
        </html>';
  
  break;
}


if($_GET['state'] == 'ALL'){

echo '<html>
      <head>
        <title>Dealer Address Issues</title>
        <style>
          table{
            border: 1px solid black;
          }
          td, th{
            border: 1px solid black;
            padding: 5px;
          }
        </style>
      </head>
      <body>
      <h1>Dealer Address Issues:</h1>
        <table>
          <tr>
            <th>Dealer Name</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip</th>
            <th>Latitude</th>
            <th>Longitude</th>
          </tr>';

$q = "SELECT * FROM `dealers` 
      WHERE 
      `inactive` != 'Yes' AND 
      `address` = '' OR 
      `city` = '' OR 
      `state` = '' OR 
      `zip` = '' OR 
      `lat` = '' OR 
      `lat` = 'Error' OR 
      `lng` = '' OR 
      `lng` = 'Error' ORDER BY `name` ASC";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  
  echo '<tr>
          <td>' . $r['name'] . '</td>
          <td>' . $r['address'] . '</td>
          <td>' . $r['city'] . '</td>
          <td>' . $r['state'] . '</td>
          <td>' . $r['zip'] . '</td>
          <td>' . $r['lat'] . '</td>
          <td>' . $r['lng'] . '</td>
        </tr>';
}

echo '</table>
</body>
</html>';
}else{//Specific State Selected...
  
  echo '<html>
      <head>
        <title>Dealer Address Issues</title>
        <style>
          table{
            border: 1px solid black;
          }
          td, th{
            border: 1px solid black;
            padding: 5px;
          }
        </style>
      </head>
      <body>
      <h1>Dealer Address Issues:</h1>
        <table>
          <tr>
            <th>Dealer Name</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip</th>
            <th>Latitude</th>
            <th>Longitude</th>
          </tr>';

$q2 = "SELECT * FROM `dealers` 
      WHERE 
      `inactive` != 'Yes' AND 
      `internet_dealer` != 'Yes' AND
      `state` = '" . $_GET['state'] . "' 
      AND
      (`address` = '' OR 
      `city` = '' OR 
      `zip` = '' OR 
      `lat` = '' OR 
      `lat` = 'Error' OR 
      `lng` = '' OR 
      `lng` = 'Error') 
      ORDER BY `name` ASC";
$g2 = mysqli_query($conn, $q2) or die($conn->error);
while($r2 = mysqli_fetch_array($g2)){
  
  echo '<tr>
          <td>' . $r2['name'] . '</td>
          <td>' . $r2['address'] . '</td>
          <td>' . $r2['city'] . '</td>
          <td>' . $r2['state'] . '</td>
          <td>' . $r2['zip'] . '</td>
          <td>' . $r2['lat'] . '</td>
          <td>' . $r2['lng'] . '</td>
        </tr>';
}

echo '</table>
</body>
</html>';
  
}
?>