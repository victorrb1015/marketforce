<?php
include '../php/connection.php';

echo '<html>
    <head>
      <title>Collection Customers With No Emails</title>
      <style>
      table{
        border: 1px solid black;
      }
      td{
        border: 1px solid black;
        padding: 10px;
      }
      </style>
    </head>
    <body>
    <h1>Master Survey List</h1>
      <table>
      <thead>
        <tr>
          <th>Customer Name</th>
          <th>Date</th>
          <th>Dealer Name</th>
          <th>Email</th>
          <th>Cust. Phone#</th>
          <th>Dealer Cell#</th>
          <th>Dealer Fax#</th>
        </tr>
        </thead>';

$sq = "SELECT * FROM `collections` WHERE `inactive` != 'Yes' AND `collection` = 'Yes' ORDER BY `name` ASC";
$sg = mysqli_query($conn, $sq) or die($conn->error);
while($sr = mysqli_fetch_array($sg)){
  
  $diq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $sr['ID'] . "'";
  $dig = mysqli_query($conn, $diq) or die($conn->error);
  $dir = mysqli_fetch_array($dig);
  
  echo '<tbody><tr>
          <td>' . $sr['name'] . '</td>
          <td>' . date("m/d/y", strtotime($sr['date'])) . '</td>
          <td>' . $sr['dname'] . '</td>
          <td>' . $sr['email'] . '</td>
          <td>' . $sr['phone'] . '</td>
          <td>' . $dir['dcphone'] . '</td>
          <td>' . $dir['fax'] . '</td>
       </tr></tbody>';
  
}

echo '</table>
</body>
</html>';

?>