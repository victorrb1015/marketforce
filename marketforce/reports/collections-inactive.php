<?php
include '../php/connection.php';

echo '<html>
    <head>
      <title>Collections Inactive Report</title>
      <style>
      table{
        border: 1px solid black;
      }
      td{
        border: 1px solid black;
        padding: 10px;
      }
      </style>
      <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
    <h1>Collections Inactive Report</h1>
      <table>
        <tr>
          <th>Customer Name</th>
          <th>Inv#</th>
          <th>Date</th>
          <th>Category</th>
          <th>Initial Debt Amount</th>
          <th>Amount Paid</th>
          <th>Balance</th>
          <th>Paid?</th>
          <th>View</th>
        </tr>';

$sq = "SELECT * FROM `collections` WHERE `inactive` = 'Yes' ORDER BY `name` ASC";
$sg = mysqli_query($conn, $sq) or die($conn->error);
while($sr = mysqli_fetch_array($sg)){
  echo '<tr>
          <td style="padding: 10px;">' . $sr['name'] . '</td>
          <td style="padding: 10px;">' . $sr['inv#'] . '</td>
          <td style="padding: 10px;">' . date("m/d/y", strtotime($sr['date'])) . '</td>';
  
                    $i = 0;
                  if($sr['ck_ca_cc'] != ''){
                    $cat = 'ck_ca_cc';
                    $i++;
                  }
                  if($sr['po'] != ''){
                    $cat = 'po';
                    $i++;
                  }
                  if($sr['nsf_stop'] != ''){
                    $cat = 'nsf_stop';
                    $i++;
                  }
                  if($sr['repair'] != ''){
                    $cat = 'repair';
                    $i++;
                  }
                  if($sr['dealer_pmt'] != ''){
                    $cat = 'dealer_pmt';
                    $i++;
                  }
                  if($sr['dealer_fin'] != ''){
                    $cat = 'dealer_fin';
                    $i++;
                  }
                  if($sr['legal'] != ''){
                    $cat = 'legal';
                    $i++;
                  }
                  if($sr['bli'] != ''){
                    $cat = 'bli';
                    $i++;
                  }
                  if($sr['ezpay'] != ''){
                    $cat = 'ezpay';
                    $i++;
                  }
                  if($sr['scoggin'] != ''){
                    $cat = 'scoggin';
                    $i++;
                  }
                  if($sr['double_d'] != ''){
                    $cat = 'double_d';
                    $i++;
                  }
                  if($sr['dallen_legacy'] != ''){
                    $cat = 'dallen_legacy';
                    $i++;
                  }
									if($sr['rto_national'] != ''){
										$cat = 'rto_national';
										$i++;
									}
  
  if($sr[$cat] != ''){
    $paid = $sr[$cat];
  }else{
    $paid = 0.00;
  }
 $balance = $sr['initial_debt'] - $paid;
  
  
  echo '<td style="padding: 10px;">' . $cat . '</td>';
  echo '<td style="padding: 10px;"> $' . number_format($sr['initial_debt'],2) . '</td>
        <td style="padding: 10px;"> $' . number_format($paid,2) . '</td>
        <td style="padding: 10px;"> $' . number_format($balance,2) . '</td>
        <td style="padding: 10px;">' . $sr['paid'] . '</td>
        <td style="padding: 10px;">
        <a href="../collections/customer-report.php?remote=yes&cid=' . $sr['ID'] . '" target="_blank">
        <!--<span style="background:blue;color:white;padding:5px;border-radius:25px;">View Survey</span>-->
        <button type="button" class="btn btn-primary btn-md">View Report</button>
        </a>
        </td>
       </tr>';
  
  $style = '';
}

echo '</table>
</body>
</html>';

?>