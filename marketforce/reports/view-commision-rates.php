<?php
include '../php/connection.php';

echo '<html>
      <head>
      <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <title>Dealer Commission Rates</title>
        <style>
          table{
            border: 1px solid black;
          }
          td, th{
            border: 1px solid black;
            padding: 5px;
          }
        </style>
        <script>
          function change(id){
            var rate = prompt("Please enter the new Commission Rate:");
            if(rate == "" || rate == null){
              alert("You did not enter a new commission rate!");
              return;
            }
						
						rate = rate.replace("&"," and ");
						
           /* var isnum = /^\d+$/.test(rate);
            if(isnum == false){
              alert("You may only enter numbers! Do not add the percent sign!");
              return;
            }*/
            if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","https://marketforceapp.com/marketforce/reports/php/change-commision-rate.php?id="+id+"&rate="+rate,true);
  xmlhttp.send();
          }
        </script>
      </head>
      <body>
      <h1>Dealer Commission Rates:</h1>
        <table>
          <tr>
            <th>Dealer Name</th>
            <th>City</th>
            <th>State</th>
            <th>Commission %</th>';

//if($_SESSION['user_id'] == '3' || $_SESSION['user_id'] == '1' || $_SESSION['user_id'] == '18' || $_SESSION['user_id'] == '120' || $_SESSION['user_id'] == '57' || $_SESSION['user_id'] == '51' || $_SESSION['user_id'] == '77'){

  echo '<th>Change % Rate</th>';
//}
          echo '</tr>';

$q = "SELECT * FROM `dealers` WHERE `inactive` != 'Yes' ORDER BY `name` ASC";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  
  echo '<tr>
          <td>' . $r['name'] . '</td>
          <td>' . $r['city'] . '</td>
          <td>' . $r['state'] . '</td>
          <td>' . $r['commision'] . '</td>';

  //if($_SESSION['user_id'] == '3' || $_SESSION['user_id'] == '1' || $_SESSION['user_id'] == '18' || $_SESSION['user_id'] == '57' || $_SESSION['user_id'] == '51' || $_SESSION['user_id'] == '77'){

      echo '<td style="text-align:center;">
            <button type="button" value="' . $r['ID'] . '" onclick="change(this.value);" style="width:50%;background:red;font-size:15px;border-radius:25px;">
            <i class="fa fa-pencil"></i>
            </button>
            </td>';
  //}
   echo '</tr>';
}

echo '</table>
</body>
</html>';

?>