<?php
include '../php/connection.php';

echo '<html>
      <head>
        <title>View Completed Packets</title>
        <style>
          table{
            border: 1px solid black;
          }
          td, th{
            border: 1px solid black;
            padding: 5px;
          }
        </style>
      </head>
      <body>
      <h1>Completed Packets</h1>
        <table>
          <tr>
            <th>Dealer Name</th>
            <th>Rep Name</th>
            <th>Document Type</th>
            <th>Date</th>
            <th>Notes</th>
          </tr>';

$cq = "SELECT * FROM `new_packets` WHERE `status` = 'Completed' ORDER BY `name` ASC";
$cg = mysqli_query($conn, $cq) or die($conn->error);
while($cr = mysqli_fetch_array($cg)){
  //Get dealer name...
  $dnq = "SELECT * FROM `dealers` WHERE `ID` = '" . $cr['dealer_id'] . "'";
  $dng = mysqli_query($conn, $dnq) or die($conn->error);
  $dnr = mysqli_fetch_array($dng);
  
  //Get Rep's name...
  $rnq = "SELECT * FROM `users` WHERE `ID` = '" . $cr['rep'] . "'";
  $rng = mysqli_query($conn, $rnq) or die($conn->error);
  $rnr = mysqli_fetch_array($rng);
  
  echo '<tr>
          <td>' . $dnr['name'] . '</td>
          <td>' . $rnr['fname'] . ' ' . $rnr['lname'] . '</td>
          <td>' . $cr['form_type'] . '</td>
          <td>' . date("m/d/Y", strtotime($cr['date'])) . '</td>
          <td>' . $cr['note'] . '</td>
        </tr>';
}

echo '</table>
</body>
</html>';

?>