<?php
include '../php/connection.php';

echo '<html>
      <head>
        <title>View Unmanned Lots</title>
        <style>
          table{
            border: 1px solid black;
          }
          td, th{
            border: 1px solid black;
            padding: 5px;
          }
        </style>
      </head>
      <body>
      <h1>Unmanned Lots:</h1>
        <table>
          <tr>
            <th>Dealer Name</th>
            <th>City</th>
            <th>State</th>
          </tr>';

$q = "SELECT * FROM `dealers` WHERE `status` = 'Unmanned Lot' AND `inactive` != 'Yes' ORDER BY `name` ASC";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  
  echo '<tr>
          <td>' . $r['name'] . '</td>
          <td>' . $r['city'] . '</td>
          <td>' . $r['state'] . '</td>
        </tr>';
}

echo '</table>
</body>
</html>';

?>