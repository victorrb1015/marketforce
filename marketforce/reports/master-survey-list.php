<?php
include '../php/connection.php';

echo '<html>
    <head>
      <title>Master Survey List</title>
      <style>
      table{
        border: 1px solid black;
      }
      td{
        border: 1px solid black;
        padding: 10px;
      }
      </style>
    </head>
    <body>
    <h1>Master Survey List</h1>
      <table>
        <tr>
          <th>Rep Name</th>
          <th>Date</th>
          <th>Dealer Name</th>
          <th>Overall Grade</th>
          <th>View</th>
        </tr>';

$sq = "SELECT * FROM `rep_survey` ORDER BY `ID` DESC";
$sg = mysqli_query($conn, $sq) or die($conn->error);
while($sr = mysqli_fetch_array($sg)){
  echo '<tr>
          <td>' . $sr['rep_name'] . '</td>
          <td>' . $sr['sub_date'] . '</td>';
  
  $dnq = "SELECT * FROM `dealers` WHERE `ID` = '" . $sr['dealer_name'] . "'";
  $dng = mysqli_query($conn, $dnq) or die($conn->error);
  $dnr = mysqli_fetch_array($dng);
  
  echo '<td>' . $dnr['name'] . '</td>';
  
  //Calculate Survey Overall Grade...
  $igrade = $sr['q1'] + $sr['q2'] + $sr['q3'] + $sr['q4'] + $sr['q5'] + $sr['q6'] + $sr['q7'] + $sr['q8'] + $sr['q9'] + $sr['q10'];
  $grade = $igrade / 50 * 100;
  
  if($grade <= 80){
    $style = 'style="background:red;"';
  }
  
  echo '<td ' . $style . '>' . $grade . '</td>
        <td>
        <a href="view-rep-surveys.php?id=' . $sr['ID'] . '" target="_blank">
        <span style="background:blue;color:white;padding:5px;border-radius:25px;">View Survey</span>
        </a>
        </td>
       </tr>';
  
  $style = '';
}

echo '</table>
</body>
</html>';

?>