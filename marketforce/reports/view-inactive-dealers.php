<?php
include '../php/connection.php';

echo '<html>
      <head>
      <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <title>Inactive Dealers</title>
      <style>
      td{
        border: 1px solid black;
        padding: 10px;
      }
      </style>
      </head>
      <body>
      <h1>Inactive Dealers</h1>';

echo '<table>
      <tr>
        <th>Name</th>
        <th>Location</th>
        <th>Replacement Note</th>
        <th>Rep</th>';
if($_SESSION['dealer_activation'] == 'Yes'){
        echo '<th>Re-Activate</th>';
}
    echo '</tr>';

$q = "SELECT * FROM `dealers` WHERE `inactive` = 'Yes' ORDER BY `name`";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  echo '<tr>
          <td>' . $r['name'] . '</td>
          <td>' . $r['city'] . ', ' . $r['state'] . '</td>
          <td style="max-width:500px;">' . $r['hide_note'] . '</td>
          <td>' . $r['hide_rep'] . '</td>';
      if($_SESSION['dealer_activation'] == 'Yes'){
          echo '<td>
          <a href="activate_dealer.php?id=' . $r['ID'] . '" target="_blank">
          <span style="background:green;font-weight:bold;padding:5px;border-radius:25px;color:white;">
          <i class="fa fa-plus"></i> Re-Activate
          </span>
          </a>
          </td>';
      }
       echo '</tr>';
}

echo '</table>
      </body>
      </html>';

?>