<?php
include '../php/connection.php';

//Load Variables
$id = $_GET['id'];

//Query Info...
$siq = "SELECT * FROM `rep_survey` WHERE `ID` = '" . $id . "'";
$sig = mysqli_query($conn, $siq) or die($conn->error);
$sir = mysqli_fetch_array($sig);

//Get the Dealers name from the ID
$dnq = "SELECT * FROM `dealers` WHERE `ID` = '" . $sir['dealer_name'] . "'";
$dng = mysqli_query($conn, $dnq);
$dnr = mysqli_fetch_array($dng);


//Setup Variables
$dealer_name = $dnr['name'];
$dealer_email = $dnr['email'];
$vd = $sir['visit_date'];
$hh = $sir['hrs'];
$m = $sir['mins'];
$q1 = $sir['q1'];
$q2 = $sir['q2'];
$q3 = $sir['q3'];
$q4 = $sir['q4'];
$q5 = $sir['q5'];
$q6 = $sir['q6'];
$q7 = $sir['q7'];
$q8 = $sir['q8'];
$q9 = $sir['q9'];
$q10 = $sir['q10'];
$ta1 = mysqli_real_escape_string($conn, $sir['ta1']);
$ta2 = mysqli_real_escape_string($conn, $sir['ta2']);
$rep_name = $sir['rep_name'];

//Change number values to grade values
$numsa = array($q1,$q2,$q3,$q4,$q5,$q6,$q7,$q8,$q9,$q10);
  $i = 0;
 while($i <= 9){
switch ($numsa[$i]) {
    case "1":
        $numsa[$i] = "F";
        break;
    case "2":
        $numsa[$i] = "D";
        break;
    case "3":
        $numsa[$i] = "C";
        break;
    case "4":
        $numsa[$i] = "B";
        break;
    case "5":
        $numsa[$i] = "A";
        break;
    default:
        echo "ERROR!";
}
   $i++;
 }

    echo '<html>
          <head>
          <style>
          .header{
            text-align: center;
          }
          .conf{
            margin-top: -4%;
            margin-bottom: -4%;
          }
          </style>
          </head>
          <body>
          <div class="main">
          <div class="header">
          <img src="http://marketforceapp.com/marketforce/img/logo-allsteel.png" style="text-align: center;"><br>
          <img src="http://marketforceapp.com/marketforce/img/confidential.png" class="conf" style="transform: rotate(20deg); width: 20%; text-align: center;">
          <h2 style="text-align: center;">Survey Information:</h2>
          </div>
          <br>
          <p>Dealer Name: <span style="color:red;">' . $dealer_name . '</span></p>
          <p>Rep Name: <span style="color:red;">' . $rep_name . '</span></p>
          <p>Visit Date: ' . $vd . '</p>
          <p>Visit Duration: ' . $hh . ' HR(S) ' . $m . ' MIN(S)</p>
          <p>Q1: My Outside Sales Representative Made the visit well worth my time.</p>
          <p><strong>Grade: ' . $numsa[0] . '</strong></p>
          <p>Q2: My Outside Sales Representative Was able to answer the questions I had with their knowledge of the product and sales procedures.</p>
          <p><strong>Grade: ' . $numsa[1] . '</strong></p>
          <p>Q3: My Outside Sales Representative Took the time to make sure we understood and could work with the information.</p>
          <p><strong>Grade: ' . $numsa[2] . '</strong></p>
          <p>Q4: My Outside Sales Representative Replaced and/or provided me with brochures and sales materials including order forms that we needed.</p>
          <p><strong>Grade: ' . $numsa[3] . '</strong></p>
          <p>Q5: My Outside Sales Representative Invited me to call or email with any questions or assistance I needed for the future.</p>
          <p><strong>Grade: ' . $numsa[4] . '</strong></p>
          <p>Q6: My Outside Sales Representative Would help me any way possible if needed.</p>
          <p><strong>Grade: ' . $numsa[5] . '</strong></p>
          <p>Q7: My Outside Sales Representative Seems genuinely interested in our success in sales for All Steel Carports.</p>
          <p><strong>Grade: ' . $numsa[6] . '</strong></p>
          <p>Q8: My Outside Sales Representative Provides me with accurate and timely responses when I call, text or email.</p>
          <p><strong>Grade: ' . $numsa[7] . '</strong></p>
          <p>Q9: My Outside Sales Representative Visits my location or communicates with me frequently via phone or email.</p>
          <p><strong>Grade: ' . $numsa[8] . '</strong></p>
          <p>Q10: My Outside Sales Representative Would be a top choice if I were hiring for my company.</p>
          <p><strong>Grade: ' . $numsa[9] . '</strong></p>
          <br>
          <p><strong>Share any additional comments about this visit: <span style="color:red;">(CONFIDENTIAL)</span></strong></p>
          <p><strong>Customer: </strong>' . $ta1 . '</p>
          <br>
          <p><strong>Please let me know how we can better assist you and your sales team:</strong></p>
          <p><strong>Customer: </strong>' . $ta2 . '</p>
          <br>
          <!--<h3>Thanks,<br><strong>Market Force</strong></h3>-->
         
          </div>
          </body>
          </html>';

?>