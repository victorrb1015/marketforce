<?php
include '../php/connection.php';
error_reporting(0);


if(!$_GET['date']){
  echo '<html style="text-align:center;">
        <h1>Select A Date</h1>
        <form action="collection-payments-by-day.php" method="get">
        <input type="date" name="date" placeholder="mm/dd/yyyy" />
        <br><br>
        <input type="submit" value="Submit" />
        </form>
        </html>';
  
  break;
}else{
  $qdate = date("Y-m-d", strtotime($_GET['date']));
  $date = date("m/d/y", strtotime($_GET['date']));
}

echo '<html>
      <head>
        <title>Collection Payments By Date</title>
        <style>
          table{
            border: 1px solid black;
            margin:auto;
          }
          th, td{
            border: 1px solid black;
            padding: 10px;
          }
        </style>
      </head>
      <body>';


if($_GET['state'] == 'ALL'){
echo '<h1 style="text-align:center;">Collections Payments For ' . $date . '</h1>';

  $dq = "SELECT * FROM `collections_calls` WHERE `inactive` != 'Yes' ORDER BY `ID` ASC";
  $dg = mysqli_query($conn, $dq) or die($conn->error);
  echo '<table>
          <tr>
            <th>Name</th>
            <th>Time</th>
            <th>Type</th>
            <th>Amount</th>
            <th>Conf/Check#</th>
            <th>Note</th>
            <th>Rep</th>
            <th>View</th>
          </tr>';
  while($dr = mysqli_fetch_array($dg)){
  echo '<tr>
          <td>' . $dr['name'] . '</td>
          <td>' . date("h:i A", strtotime($dr['date'])) . '</td>
          <td>' . $dr['method'] . '</td>
          <td>$' . $dr['amount'] . '</td>
          <td>' . $dr['check_num'] . ' ' . $dr['cc_conf'] . '</td>
          <td>' . $dr['edn'] . '</td>
          <td>' . $dr['rep_name'] . '</td>
          <td>
            <a href="../collections/customer-report.php?remote=yes&cid=' . $dr['customer_id'] . '" target="_blank">
              <button type="button" class="btn btn-primary btn-md">View Report</button>
            </a>
          </td>
        </tr>';
  }
  echo '</table>';

}else{//If equals ALL -- END

 echo '<h1 style="text-align:center;">Collections Payments for ' . $_GET['date'] . '</h1>';

$q = "SELECT * FROM `collections_calls` WHERE `inactive` != 'Yes' AND DATE(`date`) = DATE('" . $qdate . "') AND `type` = 'Payment' ORDER BY `ID` ASC";
$g = mysqli_query($conn, $q) or die($conn->error);
  echo '<table>
          <tr>
            <th>Name</th>
            <th>Time</th>
            <th>Type</th>
            <th>Amount</th>
            <th>Conf/Check#</th>
            <th>Note</th>
            <th>Rep</th>
            <th>View</th>
          </tr>';
  while($dr = mysqli_fetch_array($g)){
  echo '<tr>
          <td>' . $dr['name'] . '</td>
          <td>' . date("h:i A", strtotime($dr['date'])) . '</td>
          <td>' . $dr['method'] . '</td>
          <td>$' . $dr['amount'] . '</td>
          <td>' . $dr['check_num'] . ' ' . $dr['cc_conf'] . '</td>
          <td>' . $dr['edn'] . '</td>
          <td>' . $dr['rep_name'] . '</td>
          <td>
            <a href="../collections/customer-report.php?remote=yes&cid=' . $dr['customer_id'] . '" target="_blank">
              <button type="button" class="btn btn-primary btn-md">View Report</button>
            </a>
          </td>
        </tr>';
  }
  echo '</table>';
}
echo '<br><br><div style="text-align:center;">
      <a href="collection-payments-by-day.php">Select A Different Date</a>
      </div>';
  
echo '</body>
      </html>';



?>