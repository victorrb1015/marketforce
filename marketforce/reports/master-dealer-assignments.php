<?php
include '../php/connection.php';

echo '<html>
      <head>
      <title>Master Dealer Assignments</title>
      <style>
      html{
        background: #3C3C3C;
      }
      body{
        width: 8.5in;
        min-height: 11in;
        background: #FFFFFF;
        margin:25px auto;
      }
      table{
        margin:auto;
      }
      td,th{
        border: 1px solid black;
        padding: 5px;
      }
      </style>
      </head>
      <body>
      <h1 style="text-align:center;">Master Dealer Assignment Report</h1>
      <table>
      <thead>
      <tr>
      <th>Dealer</th>
      <th>Location</th>
      <th>Assigned OSTR</th>
      </tr>
      </thead>
      <tbody>';

$q = "SELECT * FROM `new_dealer_form` WHERE `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  echo '<tr>
        <td>' . $r['bizname'] . '</td>
        <td>' . $r['city'] . ', ' . $r['state'] . '</td>
        <td>' . $r['user'] . '</td>
        </tr>';
}

echo '</tbody>
      </table>
      </html>';

?>