<?php
include '../php/connection.php';
//var_dump($_SESSION);
//session_start();
//Security Code Here...



echo '<html>
      <head>
      <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <title>Completed Check Requests</title>
        <style>
          table{
            border: 1px solid black;
          }
          td, th{
            border: 1px solid black;
            padding: 5px;
          }
        </style>
      </head>
      <body>
      <h1>Completed Check Requests:</h1>
        <table>
          <tr>
            <th>Date</th>
            <th>Payee</th>
            <th>Requested Amount</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Paid Amount</th>
            <th>Check#</th>
            <th>Date Mailed</th>
            <th>Notes</th>
            <th>View Request</th>
          </tr>';

$q = "SELECT * FROM `check_requests` WHERE `type` = 'Check Request' AND (`status` = 'Completed' OR `status` = 'Cancelled') ORDER BY `date` DESC";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  
  echo '<tr>
          <td>' . date("m/d/Y",strtotime($r['date'])) . '</td>
          <td>' . $r['to'] . '</td>
          <td>' . $r['amount'] . '</td>
          <td>
          ' . $r['address'] . '<br>
          ' . $r['apt'] . '<br>
          ' . $r['city'] . ', ' . $r['state'] . ' ' . $r['zip'] . '
          </td>
          <td>' . $r['phone'] . '</td>
          <td>' . $r['camount'] . '</td>
          <td>' . $r['cnum'] . '</td>
          <td>' . $r['mdate'] . '</td>
          <td>' . $r['note'] . '</td>
          <td style="text-align:center;">';
  if($r['version'] == 'Refund'){
    echo '<a href="../accounting/printable-refund-check-request-form.php?id=' . $r['ID'] . '" target="_blank">';
  }else{
    echo '<a href="../accounting/printable-general-check-request-form.php?id=' . $r['ID'] . '" target="_blank">';
  }
          
    echo '<h2><span style="background-color:blue;font-weight:bold;padding:5px;border-radius:25px;color:white;">
          <i class="fa fa-eye"></i>
          </span></h2>
          </a>
        </tr>';
}

echo '</table>
</body>
</html>';

?>