<?php
include '../php/connection.php';

echo '<html>
    <head>
      <title>Dealer Display Report</title>
      <style>
      html{
        background:#3c3c3c;
      }
      body{
        background:white;
        max-width: 8.5in;
        min-height: 11in;
        margin:auto;
        padding:10px;
      }
      table{
        border: 1px solid black;
        margin:auto;
      }
      td{
        border: 1px solid black;
        padding: 10px;
      }
      </style>
    </head>
    <body>
    <h1 style="text-align:center;">Dealer Display Report</h1>';
     

//States...
$sq = "SELECT DISTINCT `state` FROM `dealers` WHERE `inactive` != 'Yes' AND `state` != '' ORDER BY `state` ASC";
$sg = mysqli_query($conn, $sq) or die($conn->error);
while($sr = mysqli_fetch_array($sg)){
  echo '<h2><b>' . $sr['state'] . '</b></h2>';
  //Dealers...
  $dq = "SELECT * FROM `dealers` WHERE `state` = '" . $sr['state'] . "' AND `inactive` != 'Yes' ORDER BY `name` ASC";
  $dg = mysqli_query($conn, $dq) or die($conn->error);
  while($dr = mysqli_fetch_array($dg)){
    echo '<h3><b><u>' . $dr['name'] . '</u></b></h3>';
    //Displays...
    $ddq = "SELECT * FROM `display_orders` WHERE `ID` = '" . $dr['ID'] . "'";
    $ddg = mysqli_query($conn, $ddq) or die($conn->error);
    while($ddr = mysqli_fetch_array($ddg)){
      //Display Info
      if($ddr['tcg'] == 'true'){
        echo '<p>Two Car Garage: ' . $ddr['tcg_v1'] . 'x' . $ddr['tcg_v2'] . 'x' . $ddr['tcg_v3'] . ' - $' . $ddr['tcg_price'] . '</p>';
      }
      if($ddr['g'] == 'true'){
        echo '<p>Garage: ' . $ddr['g_v1'] . 'x' . $ddr['g_v2'] . 'x' . $ddr['g_v3'] . ' - $' . $ddr['g_price'] . '</p>';
      }
      if($ddr['cua'] == 'true'){
        echo '<p>Combo Unit A-Frame: ' . $ddr['cua_v1'] . 'x' . $ddr['cua_v2'] . 'x' . $ddr['cua_v3'] . ' - $' . $ddr['cua_price'] . '</p>';
      }
      if($ddr['cur'] == 'true'){
        echo '<p>Combo Unit Regular: ' . $ddr['cur_v1'] . 'x' . $ddr['cur_v2'] . 'x' . $ddr['cur_v3'] . ' - $' . $ddr['cur_price'] . '</p>';
      }
      if($ddr['rvp'] == 'true'){
        echo '<p>RV Port: ' . $ddr['rvp_v1'] . 'x' . $ddr['rvp_v2'] . 'x' . $ddr['rvp_v3'] . ' - $' . $ddr['rvp_price'] . '</p>';
      }
      if($ddr['cst'] == 'true'){
        echo '<p>Carport Standard: ' . $ddr['cst_v1'] . 'x' . $ddr['cst_v2'] . 'x' . $ddr['cst_v3'] . ' - $' . $ddr['cst_price'] . '</p>';
      }
      if($ddr['csp'] == 'true'){
        echo '<p>Carport Special: ' . $ddr['csp_v1'] . 'x' . $ddr['csp_v2'] . 'x' . $ddr['csp_v3'] . ' - $' . $ddr['csp_price'] . '</p>';
      }
      if($ddr['vb'] == 'true'){
        echo '<p>Vertical Building: ' . $ddr['vb_v1'] . 'x' . $ddr['vb_v2'] . 'x' . $ddr['vb_v3'] . ' - $' . $ddr['vb_price'] . '</p>';
      }
      if($ddr['u'] == 'true'){
        echo '<p>Utility - Front Railing: ' . $ddr['u_v1'] . 'x' . $ddr['u_v2'] . 'x' . $ddr['u_v3'] . ' - $' . $ddr['u_price'] . '</p>';
      }
      if($ddr['au'] == 'true'){
        echo '<p>Two Car Garage: ' . $ddr['au_v1'] . 'x' . $ddr['au_v2'] . 'x' . $ddr['au_v3'] . ' - $' . $ddr['au_price'] . '</p>';
      }
      if($ddr['us'] == 'true'){
        echo '<p>A-Frame Utility: ' . $ddr['us_v1'] . 'x' . $ddr['us_v2'] . 'x' . $ddr['us_v3'] . ' - $' . $ddr['us_price'] . '</p>';
      }
      if($ddr['vu'] == 'true'){
        echo '<p>Vertical Utility: ' . $ddr['vu_v1'] . 'x' . $ddr['vu_v2'] . 'x' . $ddr['vu_v3'] . ' - $' . $ddr['vu_price'] . '</p>';
      }
      if($ddr['notes'] != ''){
        echo '<p>Notes: ' . $ddr['notes'] . '</p>';
      }
      
  
    }
  }
}

echo '</body>
</html>';

?>