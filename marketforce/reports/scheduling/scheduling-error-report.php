<?php
include '../../php/connection.php';

echo '<html>
        <head>
          <title>Scheduling Address Error Report</title>
          <style>
            table td, th{
              border: 1px solid black;
              padding: 5px;
            }
            @media print {
              footer {
                page-break-after: always;
              }
            }
          </style>
        </head>
        <body>';
echo '<h1>Scheduling Address Error Report:</h1>';
echo '<table>
        <tr>
          <th>Invoice#</th>
          <th>Customer Name</th>
          <th>Building Address</th>
          <th>Building City</th>
          <th>Building State</th>
          <th>Building Zip</th>
          <th>Latitude</th>
          <th>Longitude</th>
        </tr>';
//Get Data for Error Report...
//$eq = "SELECT * FROM `portal_relationship` WHERE `lat` = 'Error' OR `lng` = 'Error'";
$eq = "SELECT * FROM `salesPortalLinked` WHERE `archieved` != 'Yes'";
$eg = mysqli_query($conn, $eq) or die($conn->error);
while($er = mysqli_fetch_array($eg)){
  //$aq = "SELECT * FROM `salesPortalLinked` WHERE `orderID` = '" . $er['orderID'] . "'";
  $aq = "SELECT * FROM `portal_relationship` WHERE `orderID` = '" . $er['orderID'] . "'";
  $ag = mysqli_query($conn, $aq) or die($conn->error);
  $ar = mysqli_fetch_array($ag);
  
  if(mysqli_num_rows($ag) <= 0 || $ar['lat'] == '' || $ar['lat'] == 'Error' || $ar['lng'] == '' | $ar['lng'] == 'Error'){
  
  echo '<tr>
          <td>' . $er['orderID'] . '</td>
          <td>' . $er['customer'] . '</td>
          <td>' . $er['buildingAddress'] . '</td>
          <td>' . $er['buildingCity'] . '</td>
          <td>' . $er['buildingState'] . '</td>
          <td>' . $er['buildingZipCode'] . '</td>
          <td>' . $ar['lat'] . '</td>
          <td>' . $ar['lng'] . '</td>
        </tr>';
  }
}

echo '</table>';


//echo '<footer></footer>';




echo '</body>
      </html>';
?>