<?php
include '../../php/connection.php';


echo '<html>
        <head>
          <title>Scheduling State Error Report</title>
          <style>
            table td, th{
              border: 1px solid black;
              padding: 5px;
            }
            @media print {
              footer {
                page-break-after: always;
              }
              mark{
                background:yellow;
              }
            }
          </style>
        </head>
        <body>';
echo '<h1>Invoice State Error Report:</h1>';
echo '<table>
        <tr>
          <th>Invoice#</th>
          <th>Customer Name</th>
          <th>Building Address</th>
          <th>Building City</th>
          <th>Building State</th>
          <th>Building Zip</th>
          <th>Error Explaination</th>
        </tr>';

$oq = "SELECT * FROM `salesPortalLinked` WHERE `archieved` != 'Yes'";
$og = mysqli_query($conn, $oq) or die($conn->error);
while($or = mysqli_fetch_array($og)){
  $sq = "SELECT * FROM `states` WHERE `abrev` = '" . $or['buildingState'] . "'";
  $sg = mysqli_query($conn, $sq) or die($conn->error);
  $sr = mysqli_fetch_array($sg);
  if(mysqli_num_rows($sg) <= 0){
    //Not Found...
    echo '<tr>
          <td>' . $or['orderID'] . '</td>
          <td>' . $or['customer'] . '</td>
          <td>' . $or['buildingAddress'] . '</td>
          <td>' . $or['buildingCity'] . '</td>
          <td><mark>' . $or['buildingState'] . '</mark></td>
          <td>' . $or['buildingZipCode'] . '</td>
          <td>The provided State Abreviation is invalid! Please use the State Abreviation Chart at the bottom of this report for reference.</td>
        </tr>';
  }else{
    //Found...
    $ssq = "SELECT * FROM `dealers` WHERE `state` = '" . $sr['state'] . "'";
    $ssg = mysqli_query($conn, $ssq) or die($conn->error);
    if(mysqli_num_rows($ssg) <= 0){
    echo '<tr>
          <td>' . $or['orderID'] . '</td>
          <td>' . $or['customer'] . '</td>
          <td>' . $or['buildingAddress'] . '</td>
          <td>' . $or['buildingCity'] . '</td>
          <td style="background:red;">' . $or['buildingState'] . '</td>
          <td>' . $or['buildingZipCode'] . '</td>
          <td>The invoice in question is listed in a state for which we do not have any dealers registered (as recorded in Market Force)</td>
        </tr>';
    }
  }
}

echo '</table>';

echo '<h1>State Chart:</h1>';
echo '<table>
        <tr>
          <th>State</th>
          <th>Abreviation</th>
          <th>Dealers Registered in Market Force?</th>
        </tr>';
$xsq = "SELECT * FROM `states`";
$xsg = mysqli_query($conn, $xsq) or die($conn->error);
while($xsr = mysqli_fetch_array($xsg)){
  $xssq = "SELECT * FROM `dealers` WHERE `state` = '" . $xsr['state'] . "'";
  $xssg = mysqli_query($conn, $xssq) or die($conn->error);
  if(mysqli_num_rows($xssg) > 0){
    $dr = 'Yes';
    $bg = 'style="background:yellow;"';
  }else{
    $dr = 'No';
    $bg = 'style="background:white;"';
  }
  echo '<tr ' . $bg . '>
          <td>' . $xsr['state'] . '</td>
          <td>' . $xsr['abrev'] . '</td>
          <td>' . $dr . '</td>
        </tr>';
}
echo '</table>
      </body>
      </html>';
