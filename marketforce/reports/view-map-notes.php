<?php
include '../php/connection.php';

echo '<html>
      <head>
      <title>Map Notes</title>
      </head>
      <style>
      table{
        border: 1px solid black;
      }
      td,th{
        border: 1px solid black;
        padding: 10px;
      }
      </style>
      </head>
      <body>
      <h1>Internal Map Notes:</h1>
      <table>
      <tr>
      <th>Date Added</th>
      <th>Dealer Name</th>
      <th>Location</th>
      <th>Rep Name</th>
      <th>Note</th>
      </tr>';

$q = "SELECT * FROM `internal_notes`";
$g = mysqli_query($conn, $q) or die($conn->error);

  while($r = mysqli_fetch_array($g)){
    
    $q2 = "SELECT * FROM `dealers` WHERE `ID` = '" . $r['dealer_id'] . "'";
    $g2 = mysqli_query($conn, $q2) or die($conn->error);
    $r2 = mysqli_fetch_array($g2);
  
  echo '<tr>
          <td>' . date("m/d/Y",strtotime($r['date'])) . '</td>
          <td>' . $r2['name'] . '</td>
          <td>' . $r2['city'] . ', ' . $r2['state'] . '</td>
          <td>' . $r['rep'] . '</td>
          <td>' . $r['note'] . '</td>
        </tr>';
  }
  echo '</table>
  </body>
  </html>';


?>