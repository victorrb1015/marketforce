<?php
include '../php/connection.php';

?>
<html>
  <head>
    <title>Closed Dealer Report</title>
    <style>
      html{
        background:#3c3c3c;
      }
      body{
        background:white;
        max-width: 8.5in;
        min-height: 11in;
        margin:auto;
      }
      table{
        padding:20px;
        margin:auto;
      }
      th, td{
        border:1px solid black;
        padding: 5px;
      }
      th{
        background:lightgrey;
      }
      .main_btn{
        position: fixed;
        top:25px;
        left:250px;
      }
      .btn{
        background:blue;
        color:white;
        padding:15px;
        border-radius:1rem;
        cursor:pointer;
      }
      @media print{
        .main_btn{
          display:none !Important;
        }
      }
    </style>
  </head>
  <body>
    <div class="main_btn">
      <a href="view-closed-dealers.php">
        <button type="button" class="btn">Edit Parameters</button>
      </a>
    </div>
    
<?
if(!$_GET['submit']){
  echo '<div style="text-align:center;">
        <h1>Select State and Year</h1>
        <form action="view-closed-dealers.php" method="get">
        <select name="state">
        <option value="ALL">*ALL States</option>';
  
  $q = "SELECT DISTINCT `state` FROM `new_dealer_form` WHERE `inactive` != 'Yes'";
  $g = mysqli_query($conn, $q) or die($conn->error);
  while($r = mysqli_fetch_array($g)){
    echo '<option value="' . $r['state'] . '">' . $r['state'] . '</option>';
  }
  
  echo '</select>
        <br><br>
        <select name="year">
          <option value="ALL">*ALL Years</option>
          <option value="2019">2019</option>
          <option value="2018">2018</option>
          <option value="2017">2017</option>
          <option value="2016">2016</option>
        </select>
        <br><br>
        <input type="submit" name="submit" value="Submit" />
        </form>
        </div>';
  
  echo '</body>
  </html>';
  
  break;
  
}else{
  
  echo '<h1 style="text-align:center;">Closed Dealers</h1>';
  echo '<p style="text-align:center;">State: ' . $_GET['state'] . ' &nbsp;&nbsp; Year: ' . $_GET['year'] . '</p>';
  echo '<table>
  <thead>
  <tr>
  <th>Dealer Name</th>
  <th>Location</th>
  <th>Date Closed</th>
  <th>Rep Name</th>
  <th>Reason</th>
  </tr>
  </thead>
  <tbody>';
  
  $params = '';
  $state = $_GET['state'];
  
  if($_GET['year'] == 'ALL'){
    $params .= '';
  }elseif($_GET['year'] != 'ALL'){
    $params .= " AND YEAR(`date`) = '" . mysqli_real_escape_string($conn, $_GET['year']) . "'";
  }
  $dcq = "SELECT * FROM `new_packets` WHERE `form_type` = 'Dealer Closing Form' AND `status` = 'Completed'" . $params . " ORDER BY `name` ASC";
  $dcg = mysqli_query($conn, $dcq) or die($conn->error);
  while($dcr = mysqli_fetch_array($dcg)){
    //Get Rep Name...
    $rq = "SELECT * FROM `users` WHERE `ID` = '" . $dcr['rep'] . "'";
    $rg = mysqli_query($conn, $rq) or die($conn->error);
    $rr = mysqli_fetch_array($rg);
    $rep_name = $rr['fname'] . ' ' . $rr['lname'];
    //Get full info...
    $fq = "SELECT * FROM `dealers` WHERE `ID` = '" . $dcr['dealer_id'] . "'";
    $fg = mysqli_query($conn, $fq) or die($conn->error);
    $fr = mysqli_fetch_array($fg);
    
   
    
    //Check State Parameters...
    if($state != 'ALL' && $state == $fr['state']){
      //Display Info To Table...
      echo '<tr>
      <td>' . $fr['name'] . '</td>
      <td>' . $fr['city'] . ', ' . $fr['state'] . '</td>
      <td>' . date("m/d/y", strtotime($dcr['date'])) . '</td>
      <td>' . $rep_name . '</td>
      <td>' . $fr['hide_note'] . '</td>
      </tr>';
    }elseif($state == 'ALL'){
      //Display Info To Table...
      echo '<tr>
      <td>' . $fr['name'] . '</td>
      <td>' . $fr['city'] . ', ' . $fr['state'] . '</td>
      <td>' . date("m/d/y", strtotime($dcr['date'])) . '</td>
      <td>' . $rep_name . '</td>
      <td>' . $fr['hide_note'] . '</td>
      </tr>';
    }
    
  }
  
  
  echo '</body>
  </html>';
  
}
?>