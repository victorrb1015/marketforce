<?php
include '../php/connection.php';
error_reporting(0);

if(!$_GET['state']){
  echo '<html style="text-align:center;">
        <h1>Select A State</h1>
        <form action="view-new-dealers.php" method="get">
        <select name="state">
        <option value="ALL">*ALL</option>';
  
  $q = "SELECT DISTINCT `state` FROM `dealers` WHERE `inactive` != 'Yes'";
  $g = mysqli_query($conn, $q) or die($conn->error);
  while($r = mysqli_fetch_array($g)){
    echo '<option value="' . $r['state'] . '">' . $r['state'] . '</option>';
  }
  
  echo '</select>
        <br><br>
        <input type="submit" value="Submit" />
        </form>
        </html>';
  
  break;
}

if($_GET['state'] == 'ALL'){
echo '<html>
      <head>
      <title>New Dealer Report</title>
      </head>
      <style>
      table{
        border: 1px solid black;
      }
      td,th{
        border: 1px solid black;
        padding: 10px;
      }
      </style>
      </head>
      <body>
      <h1>Dealers With "NEW" Status</h1>
      <table>
      <tr>
      <th>Date Added</th>
      <th>Dealer Name</th>
      <th>Location</th>
      </tr>';

$q = "SELECT * FROM `dealers` WHERE `status` = 'New' AND `inactive` != 'Yes' ORDER BY `ID` ASC";
$g = mysqli_query($conn, $q) or die($conn->error);

if(mysqli_num_rows($g) == 0){
  echo '<tr>
          <td>NO NEW DEALERS</td>
          <td>NO NEW DEALERS</td>
          <td>NO NEW DEALERS</td>
        </tr>';
}else{
  while($r = mysqli_fetch_array($g)){
  $q2 = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $r['ID'] . "'";
  $g2 = mysqli_query($conn, $q2) or die($conn->error);
  $r2 = mysqli_fetch_array($g2);
  echo '<tr>
          <td>' . date("m/d/Y",strtotime($r2['date'])) . '</td>
          <td>' . $r['name'] . '</td>
          <td>' . $r['city'] . ', ' . $r['state'] . '</td>
        </tr>';
  }
  echo '</table>
  </body>
  </html>';
}
}else{//END ALL Query

echo '<html>
      <head>
      <title>New Dealer Report</title>
      </head>
      <style>
      table{
        border: 1px solid black;
      }
      td,th{
        border: 1px solid black;
        padding: 10px;
      }
      </style>
      </head>
      <body>
      <h1>Dealers With "NEW" Status For ' . $_GET['state'] . '</h1>
      <table>
      <tr>
      <th>Date Added</th>
      <th>Dealer Name</th>
      <th>Location</th>
      </tr>';

$q = "SELECT * FROM `dealers` WHERE `state` = '" . $_GET['state'] . "' AND `status` = 'New' AND `inactive` != 'Yes' ORDER BY `ID` ASC";
$g = mysqli_query($conn, $q) or die($conn->error);

if(mysqli_num_rows($g) == 0){
  echo '<tr>
          <td>NO NEW DEALERS</td>
          <td>NO NEW DEALERS</td>
          <td>NO NEW DEALERS</td>
        </tr>';
}else{
  while($r = mysqli_fetch_array($g)){
  $q2 = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $r['ID'] . "'";
  $g2 = mysqli_query($conn, $q2) or die($conn->error);
  $r2 = mysqli_fetch_array($g2);
  echo '<tr>
          <td>' . date("m/d/Y",strtotime($r2['date'])) . '</td>
          <td>' . $r['name'] . '</td>
          <td>' . $r['city'] . ', ' . $r['state'] . '</td>
        </tr>';
  }
  echo '</table>
  </body>
  </html>';
}

}

?>