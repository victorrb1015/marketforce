<?php
include '../php/connection.php';
error_reporting(0);

if($_GET['state'] == '' || $_GET['year'] == '' || $_GET['order'] == ''){
  echo '<html style="text-align:center;">
        <head>
        <style>
        p{
          font-size:5px;
        }
        section{
          border: 1px solid black;
        }
        </style>
        </head>
        <h1>Filter Report</h1>
        <form action="master-dealer-list.php" method="get">
        State:
        <select name="state">
        <option value="ALL">*ALL</option>';
  
  $q = "SELECT DISTINCT `state` FROM `new_dealer_form` WHERE `inactive` != 'Yes'";
  $g = mysqli_query($conn, $q) or die($conn->error);
  while($r = mysqli_fetch_array($g)){
    echo '<option value="' . $r['state'] . '">' . $r['state'] . '</option>';
  }
  
  echo '</select>
        <br><br>
        Year:
        <select name="year">
          <option value="ALL">*ALL</option>
          <option value="2017">2017</option>
          <option value="2018">2018</option>
        </select>
        <br><br>
        Order By:
        <select name="order">
          <option value="Name">Name</option>
          <option value="Date">Date</option>
        </select>
        <br><br>
        <input type="submit" value="Submit" />
        </form>
        </html>';
  
  break;
}

echo '<head>
        <style>
        p{
          padding-left:5px;
        }
        section{
          border: 1px solid black;
        }
        </style>
        </head>';

if($_GET['state'] == 'ALL'){
echo '<p>
      <a href="http://marketforceapp.com/marketforce/reports/master-dealer-list.php">
      <button type="button" style="background:blue;color:white;padding:5px;"><- Back</button>
      </a>
      </p>
      <h1 style="text-align:center;">Master Dealer List By State</h1>';

$q = "SELECT DISTINCT `state` FROM `dealers` WHERE `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  echo '<h2><u>' . $r['state'] . '</u></h2>';
  $dq = "SELECT 
          `dealers`.`ID` as `ID`,
          `dealers`.`name` as `name`, 
          `dealers`.`2016_sales` as `2016_sales`, 
          `dealers`.`status` as `status`, 
          `new_dealer_form`.`date` as `date`
          FROM `dealers`
          LEFT JOIN `new_dealer_form` ON `dealers`.`ID` = `new_dealer_form`.`ID`
          WHERE `dealers`.`state` = '" . $r['state'] . "' AND `dealers`.`inactive` != 'Yes'";
  if($_GET['year'] != 'ALL'){
    $dq .= " AND YEAR(`new_dealer_form`.`date`) = '" . $_GET['year'] . "'";
  }
  if($_GET['order'] == 'Name'){
    $dq .= " ORDER BY `dealers`.`name` ASC";
  }else{
    $dq .= " ORDER BY `new_dealer_form`.`date` ASC";
  }
  //echo $dq;
  $dg = mysqli_query($conn, $dq) or die($conn->error);
  
  while($dr = mysqli_fetch_array($dg)){
  echo '<section>
        <p>
        <b>Dealer Name:</b> ' . $dr['name'] . '&nbsp;&nbsp;&nbsp;&nbsp;
        <b>2016 Sales:</b> ' . $dr['2016_sales'] . '&nbsp;&nbsp;&nbsp;&nbsp;
        <b>Status:</b> ' . $dr['status'] . '&nbsp;&nbsp;&nbsp;&nbsp;
        <b>Date:</b> ' . date("m/d/y", strtotime($dr['date'])) . '&nbsp;&nbsp;&nbsp;&nbsp;
        </p>';
    
    $diq = "SELECT * FROM `display_orders` WHERE `ID` = '" . $dr['ID'] . "'";
    $dig = mysqli_query($conn, $diq) or die($conn->error);
    while($dir = mysqli_fetch_array($dig)){
      
      if($dir['tcg'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Two Car Garage:</b> ' . $dir['tcg_v1'] . 'X' . $dir['tcg_v2'] . 'X' . $dir['tcg_v3'] . '</p>';
      }
      
      if($dir['g'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Garage:</b> ' . $dir['g_v1'] . 'X' . $dir['g_v2'] . 'X' . $dir['g_v3'] . '</p>';
      }
      
      if($dir['cua'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Combo Unit A-Frame:</b> ' . $dir['cua_v1'] . 'X' . $dir['cua_v2'] . 'X' . $dir['cua_v3'] . '</p>';
      }
      
      if($dir['cur'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Combo Unit Regular:</b> ' . $dir['cur_v1'] . 'X' . $dir['cur_v2'] . 'X' . $dir['cur_v3'] . '</p>';
      }
      
      if($dir['rvp'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>RV Port:</b> ' . $dir['rvp_v1'] . 'X' . $dir['rvp_v2'] . 'X' . $dir['rvp_v3'] . '</p>';
      }
      
      if($dir['cst'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Carport Standard:</b> ' . $dir['cst_v1'] . 'X' . $dir['cst_v2'] . 'X' . $dir['cst_v3'] . '</p>';
      }
      
      if($dir['csp'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Carport Special:</b> ' . $dir['csp_v1'] . 'X' . $dir['csp_v2'] . 'X' . $dir['csp_v3'] . '</p>';
      }
      
      if($dir['vb'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Vertical Building:</b> ' . $dir['vb_v1'] . 'X' . $dir['vb_v2'] . 'X' . $dir['vb_v3'] . '</p>';
      }
      
      if($dir['u'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Utility - Front Railing:</b> ' . $dir['u_v1'] . 'X' . $dir['u_v2'] . 'X' . $dir['u_v3'] . '</p>';
      }
      
      if($dir['au'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>A-Frame Utility:</b> ' . $dir['au_v1'] . 'X' . $dir['au_v2'] . 'X' . $dir['au_v3'] . '</p>';
      }
      
      if($dir['us'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Utility Standard:</b> ' . $dir['us_v1'] . 'X' . $dir['us_v2'] . 'X' . $dir['us_v3'] . '</p>';
      }
      
      if($dir['vu'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Vertical Utility:</b> ' . $dir['vu_v1'] . 'X' . $dir['vu_v2'] . 'X' . $dir['vu_v3'] . '</p>';
      }
      
      if($dir['notes'] != ''){
        echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Notes:</b> ' . $dir['notes'] . '</p>';
      }
    }
    
       echo '</section>';
  }
  
}
}else{//If equals ALL -- END

 echo '<p>
      <a href="http://marketforceapp.com/marketforce/reports/master-dealer-list.php">
      <button type="button" style="background:blue;color:white;padding:5px;"><- Back</button>
      </a>
      </p>
      <h1 style="text-align:center;">Master Dealer List for ' . $_GET['state'] . '</h1>';

//$q = "SELECT * FROM `dealers` WHERE `state` = '" . $_GET['state'] . "' AND `inactive` != 'Yes' ORDER BY `name` ASC";
$q = "SELECT 
          `dealers`.`ID` as `ID`,
          `dealers`.`name` as `name`, 
          `dealers`.`2016_sales` as `2016_sales`, 
          `dealers`.`status` as `status`, 
          `new_dealer_form`.`date` as `date`
          FROM `dealers`
          LEFT JOIN `new_dealer_form` ON `dealers`.`ID` = `new_dealer_form`.`ID`
          WHERE `dealers`.`state` = '" . $_GET['state'] . "' AND `dealers`.`inactive` != 'Yes'";
  if($_GET['year'] != 'ALL'){
    $q .= " AND YEAR(`new_dealer_form`.`date`) = '" . $_GET['year'] . "'";
  }
  if($_GET['order'] == 'Name'){
    $q .= " ORDER BY `dealers`.`name` ASC";
  }else{
    $q .= " ORDER BY `new_dealer_form`.`date` ASC";
  }
  //echo $dq;
$g = mysqli_query($conn, $q) or die($conn->error);
while($dr = mysqli_fetch_array($g)){
  
  echo '<section>
        <p>
        <b>Dealer Name:</b> ' . $dr['name'] . '&nbsp;&nbsp;&nbsp;&nbsp;
        <b>2016 Sales:</b> ' . $dr['2016_sales'] . '&nbsp;&nbsp;&nbsp;&nbsp;
        <b>Status:</b> ' . $dr['status'] . '&nbsp;&nbsp;&nbsp;&nbsp;
        <b>Date:</b> ' . date("m/d/y", strtotime($dr['date'])) . '&nbsp;&nbsp;&nbsp;&nbsp;
        </p>';
  
    $diq = "SELECT * FROM `display_orders` WHERE `ID` = '" . $dr['ID'] . "'";
    $dig = mysqli_query($conn, $diq) or die($conn->error);
    while($dir = mysqli_fetch_array($dig)){
      
      if($dir['tcg'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Two Car Garage:</b> ' . $dir['tcg_v1'] . 'X' . $dir['tcg_v2'] . 'X' . $dir['tcg_v3'] . '</p>';
      }
      
      if($dir['g'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Garage:</b> ' . $dir['g_v1'] . 'X' . $dir['g_v2'] . 'X' . $dir['g_v3'] . '</p>';
      }
      
      if($dir['cua'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Combo Unit A-Frame:</b> ' . $dir['cua_v1'] . 'X' . $dir['cua_v2'] . 'X' . $dir['cua_v3'] . '</p>';
      }
      
      if($dir['cur'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Combo Unit Regular:</b> ' . $dir['cur_v1'] . 'X' . $dir['cur_v2'] . 'X' . $dir['cur_v3'] . '</p>';
      }
      
      if($dir['rvp'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>RV Port:</b> ' . $dir['rvp_v1'] . 'X' . $dir['rvp_v2'] . 'X' . $dir['rvp_v3'] . '</p>';
      }
      
      if($dir['cst'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Carport Standard:</b> ' . $dir['cst_v1'] . 'X' . $dir['cst_v2'] . 'X' . $dir['cst_v3'] . '</p>';
      }
      
      if($dir['csp'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Carport Special:</b> ' . $dir['csp_v1'] . 'X' . $dir['csp_v2'] . 'X' . $dir['csp_v3'] . '</p>';
      }
      
      if($dir['vb'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Vertical Building:</b> ' . $dir['vb_v1'] . 'X' . $dir['vb_v2'] . 'X' . $dir['vb_v3'] . '</p>';
      }
      
      if($dir['u'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Utility - Front Railing:</b> ' . $dir['u_v1'] . 'X' . $dir['u_v2'] . 'X' . $dir['u_v3'] . '</p>';
      }
      
      if($dir['au'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>A-Frame Utility:</b> ' . $dir['au_v1'] . 'X' . $dir['au_v2'] . 'X' . $dir['au_v3'] . '</p>';
      }
      
      if($dir['us'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Utility Standard:</b> ' . $dir['us_v1'] . 'X' . $dir['us_v2'] . 'X' . $dir['us_v3'] . '</p>';
      }
      
      if($dir['vu'] == 'true'){
      echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Vertical Utility:</b> ' . $dir['vu_v1'] . 'X' . $dir['vu_v2'] . 'X' . $dir['vu_v3'] . '</p>';
      }
      
      if($dir['notes'] != ''){
        echo '<p style="background-color:yellow;color:black;margin:0px;"><b>Notes:</b> ' . $dir['notes'] . '</p>';
      }
    }
    echo '</section>';
  
}
  
}



?>