<?php
include 'security/session/session-settings.php';

if(!isset($_SESSION['in'])){
  session_destroy();
  echo 'You do not have authentication for this site...<br>';
  echo '<script>
        setInterval(function(){
        window.location="http://marketforceapp.com";
        }, 2000);
        </script>';
  return;
}

include 'php/connection.php';

$pageName = 'Manage Dealer Overview <b style="color:red;">[BETA]</b>';//Set this equal to the name of the page you would like to set...
$pageIcon = 'fa fa-file';//Set this equal to the class name of the icon you would like to set as the page icon...


//Get Dealer Info...
if(!isset($_GET['did'])){
    echo '<script>
            alert("Invalid Dealer ID");
            window.history.back();
          </script>';
}else{
    $did = $_GET['did'];
    //New Dealer Form DB...
    $ndiq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $did . "'";
    $ndig = mysqli_query($conn, $ndiq) or die($conn->error);
    $ndir = mysqli_fetch_array($ndig);
    //Dealers DB...
    $diq = "SELECT * FROM `dealers` WHERE `ID` = '" . $did . "'";
    $dig = mysqli_query($conn, $diq) or die($conn->error);
    $dir = mysqli_fetch_array($dig);
	$global_dealer = $dir['global_dealer'];
	if($global_dealer == 'Yes'){
		$global_checked = 'checked';
	}else{
		$global_checked = '';
	}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


  
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
    th, td{
      padding: 10px;
    }
    #dl, dc{
      height: 430px;
    }
    .panel-body{
      height: 382px;
    }
    #scroll{
      overflow: scroll;
    }
    ::-webkit-scrollbar {
    width: 0px;  /* remove scrollbar space */
    background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
    
    
    .packet-table td, th{
      border: 1px solid black;
    }
    .packet-table{
      margin:auto;
    }
  </style>
  <script>
    

    
    
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
       <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> <?php echo $pageName; ?> </small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                            <li>
                              <i class="<?php echo $pageIcon; ?>"></i> <?php echo $pageName; ?>
                          </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
              <!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              

              
    <!--This is where real content for the page will go-->
    <h1 class="text-center"><?php echo $dir['name']; ?></h1>
    <h3 class="text-center">
    	<input type="checkbox" id="global_cb" name="global_cb" onchange="mark_dealer_global(<?php echo $_GET['did']; ?>);" <?php echo $global_checked; ?>/> 
    	Global Dealer
    	<br>
    	<i><small style="color:green;" id="save_message"></small></i>
    </h3>
    <br>
              
<div class="row">
    <?php include 'manage-dealer/sections/visit-notes.php'; ?>
    <?php include 'manage-dealer/sections/primary-forms.php'; ?>
    <?php include 'manage-dealer/sections/addon-forms.php'; ?>
</div>
              
<div class="row">
    <?php include 'manage-dealer/sections/contact-info.php'; ?>
    <?php include 'manage-dealer/sections/displays.php'; ?>
</div>

<div class="row">
    <?php include 'manage-dealer/sections/dealer-hours.php'; ?>
    <?php include 'manage-dealer/sections/images.php'; ?>
</div>

<div class="row">
    <?php include 'manage-dealer/sections/addresses.php'; ?>
</div>



    <!--<div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Sales Overview</h3>
                </div>
                <div class="panel-body"><span>CONTENT GOES HERE</span></div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Recent Activity</h3>
                </div>
                <div class="panel-body"><span>CONTENT GOES HERE</span></div>
            </div>
        </div>
    </div> 
     <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Options and Preferences</h3>
                </div>
                <div class="panel-body"><span>CONTENT GOES HERE</span></div>
            </div>
        </div>
    </div>-->


            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
  
  
<?php include 'manage-dealer/modals/new-additional-form-modal.php'; ?>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!--<script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>-->
  
<?php include 'manage-dealer/js/forms-functions.php'; ?>
<script src="manage-dealer/js/global-dealer-function.js"></script>

 <?php include 'footer.html'; ?>
</body>

</html>