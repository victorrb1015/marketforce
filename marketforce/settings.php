<?php
include 'security/session/session-settings.php';

if($_SESSION['in'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com/";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="http://marketforceapp.com/marketforce/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="http://marketforceapp.com/marketforce/css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="http://marketforceapp.com/marketforce/js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="http://marketforceapp.com/marketforce/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="http://marketforceapp.com/marketforce/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--NEW BOOTSTRAP FOR TOGGLE SWITCHES-->
    <!--<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>-->
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
    table{
      text-align:center;
    }
  </style>
  <script>
    
  function clr_res(id,s){
    //alert("test");
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			//alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://library.burtonsolution.tech/php/reserve.php?id="+id+"&s="+s,true);
  xmlhttp.send();
  }
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

							 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> My Account <span style="color:red; font-weight:bold;"><?php echo $_GET['response']; ?></span></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
														<li class="active">
															<i class="fa fa-cogs"></i> My Account
													</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
              

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
					
              
  <?php
         $eq = "SELECT * FROM `users` WHERE `ID` = '" . $_SESSION['user_id'] . "'";
         $eg = mysqli_query($conn, $eq) or die($conn->error);
         $er = mysqli_fetch_array($eg);
              
							echo '<div class="row">
              
								<div class="col-lg-6">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Personal Information  <span style="color:red;">' . $_GET['response1'] . '</span>
											</h3>
										</div>
										<div class="panel-body">
                      <form action="settings/settings.php" method="post">
											<input type="hidden" name="id" value="' . $er['ID'] . '" />
											<input type="hidden" name="section" value="Personal" />
                        <div class="form-group">
                          <label for="fname">First Name:</label>
                          <input type="text" class="form-control" id="fname" name="fname" value="' . $er['fname'] . '" />
                        </div>
                        <div class="form-group">
                          <label for="lname">Last Name:</label>
                          <input type="text" class="form-control" id="lname" name="lname" value="' . $er['lname'] . '" />
                        </div>
                        <div class="form-group">
                          <label for="email">Email Address:</label>
                          <input type="email" class="form-control" id="email" name="email" value="' . $er['email'] . '" />
                        </div>
                        <div class="form-group">
                          <label for="phone">Phone#:</label>
                          <input type="tel" class="form-control" id="phone" name="phone" value="' . $er['phone'] . '" />
                        </div>
                        <div style="text-align:center;">
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                      </form>
										</div>
									</div>
								</div>
                
                
                <div class="col-lg-6">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												<i class="fa fa-lock"></i> Security Information <span style="color:red;">' . $_GET['response2'] . '</span>
											</h3>
										</div>
										<div class="panel-body">
                      <form action="settings/settings.php" method="post">
											<input type="hidden" name="id" value="' . $er['ID'] . '" />
											<input type="hidden" name="section" value="Security" />
                        <div class="form-group">
                          <label for="username">Username:</label>
                          <input type="text" class="form-control" id="username" name="username" value="' . $er['username'] . '" />
                        </div>
                        <div class="form-group">
                          <label for="password">Current Password:</label>
                          <input type="password" class="form-control" id="password" name="password" value="' . $er['password'] . '" disabled/>
                        </div>
                        <div class="form-group">
                          <label for="npassword">New Password:</label>
                          <input type="password" class="form-control" id="npassword" name="npassword" />
                        </div>
                        <div style="text-align:center;">
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                      </form>
										</div>
									</div>
								</div>
                
							</div><!--End Row-->';
							
							//States Assigned...
							$states = $er['myState'];
							if($er['myState2'] != ''){
								$states .= ', ' . $er['myState2'];
							}
							if($er['myState3'] != ''){
								$states .= ', ' . $er['myState3'];
							}
							if($er['myState4'] != ''){
								$states .= ', ' . $er['myState4'];
							}
							
              echo '<div class="row">
               
                <div class="col-lg-6">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Company Information
											</h3>
										</div>
										<div class="panel-body">
                      <form action="" method="post">
                        <div class="form-group">
                          <label for="position">Position:</label>
                          <input type="text" class="form-control" id="position" name="position" value="' . $er['position'] . '" disabled/>
                        </div>
                        <div class="form-group">
                          <label for="astate">Assigned State(s):</label>
                          <input type="text" class="form-control" id="astate" name="astate" value="' . $states . '" disabled/>
                        </div>
                        <!--<div style="text-align:center;">
                        <button type="submit" class="btn btn-primary" disabled>Save Changes</button>
                        </div>-->
                      </form>
										</div>
									</div>
								</div>';
								//Num of dealers
								$dcq = "SELECT * FROM `dealers` WHERE `inactive` != 'Yes' AND `state` = '" . $er['myState'] . "' OR `state` = '" . $er['myState2'] . "' OR `state` = '" . $er['myState3'] . "' OR `state` = '" . $er['myState4'] . "'";
								$dcg = mysqli_query($conn, $dcq) or die($conn->error);
								$dcCount = mysqli_num_rows($dcg);
								//Num of visits
								$vnq = "SELECT * FROM `visit_notes` WHERE `user` = '" . $er['ID'] . "'";
								$vng = mysqli_query($conn, $vnq) or die($conn->error);
								$vnCount = mysqli_num_rows($vng);
								//Num of surveys
								$srq = "SELECT * FROM `rep_survey` WHERE `rep_name` = '" . $er['fname'] . " " . $er['lname'] . "'";
								$srg = mysqli_query($conn, $srq) or die($conn->error);
								$srCount = mysqli_num_rows($srg);
							
							
              
					echo '<div class="col-lg-6">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Employee Statistics
											</h3>
										</div>
										<div class="panel-body">
                      <p><b>My Dealers:</b> ' . $dcCount . '</p>
											<p><b>Total # of Visits Made:</b> ' . $vnCount . '</p>
											<p><b>Total # of Surveys Returned</b> ' . $srCount . '</p>
										</div>
									</div>
								</div>
							</div><!--End Row-->';
              ?>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="http://marketforceapp.com/marketforce/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="http://marketforceapp.com/marketforce/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
	<!--
    <script src="http://marketforceapp.com/marketforce/js/plugins/morris/raphael.min.js"></script>
    <script src="http://marketforceapp.com/marketforce/js/plugins/morris/morris.min.js"></script>
    <script src="http://marketforceapp.com/marketforce/js/plugins/morris/morris-data.js"></script>
-->
 <?php include 'footer.html'; ?>
</body>

</html>