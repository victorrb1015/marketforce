$("#new_order_button").click(function() {
    $('#home').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#pending').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#invoiced').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#paid').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#out_of_time').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#search').removeClass("btn-outline-").addClass("btn-dark");
    $('#new_order_button').removeClass("btn-outline-success").addClass("btn-dark");
    $('#orders-table-section').hide();
    $('#new-order-section').show();
    $('#num_inv').text('Estimate #');
    $('#button_save').text('Save Order');
    $('#datepicker_install_date').datepicker({
        format: 'mmmm dd, yyyy',
        uiLibrary: 'bootstrap'
    });
    $('#customer').select2();
    $('#dealer').select2();
    setInvoiceNumber();
});

function home() {
    $('#home').removeClass("btn-dark").addClass("btn-outline-dark");
    $('#pending').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#invoiced').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#paid').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#out_of_time').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#search').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#new_order_button').removeClass("btn-success").addClass("btn-success");
    $('#new-order-section').hide();
    $('#orders-table-section').show();
    getHomeInfo();
}


$("#pending").click(function() {
    $('#home').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#pending').removeClass("btn-dark").addClass("btn-outline-dark");
    $('#invoiced').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#paid').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#out_of_time').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#search').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#new_order_button').removeClass("btn-success").addClass("btn-success");
    $('#new-order-section').hide();
    $('#orders-table-section').show();
});

$("#invoiced").click(function() {
    $('#home').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#pending').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#invoiced').removeClass("btn-dark").addClass("btn-outline-dark");
    $('#paid').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#out_of_time').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#search').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#new_order_button').removeClass("btn-success").addClass("btn-success");
    $('#new-order-section').hide();
    $('#orders-table-section').show();
});

$("#paid").click(function() {
    $('#home').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#pending').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#invoiced').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#paid').removeClass("btn-dark").addClass("btn-outline-dark");
    $('#out_of_time').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#search').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#new_order_button').removeClass("btn-success").addClass("btn-success");
    $('#new-order-section').hide();
    $('#orders-table-section').show();
});

$("#out_of_time").click(function() {
    $('#home').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#pending').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#invoiced').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#paid').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#out_of_time').removeClass("btn-dark").addClass("btn-outline-dark");
    $('#search').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#new_order_button').removeClass("btn-success").addClass("btn-success");
    $('#new-order-section').hide();
    $('#orders-table-section').show();
});

$("#search").click(function() {
    $('#home').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#pending').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#invoiced').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#paid').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#out_of_time').removeClass("btn-outline-dark").addClass("btn-dark");
    $('#search').removeClass("btn-dark").addClass("btn-outline-dark");
    $('#new_order_button').removeClass("btn-success").addClass("btn-success");
    $('#new-order-section').hide();
    $('#orders-table-section').show();
});

function NewOrder() {
    let color_top = $('#color_top').val();
    let color_sides = $('#color_sides').val();
    let color_ends = $('#color_ends').val();
    let color_trim = $('#color_trim').val();
    let customer_first_name = $('#customer_first_name').val();
    let customer_last_name = $('#customer_last_name').val();
    let customer_address = $('#customer_address').val();
    let customer_city = $('#customer_city').val();
    let customer_state = $('#customer_state').val();
    let customer_zip = $('#customer_zip').val();
    let customer_phone = $('#customer_phone').val();
    let dealer_id = $('#dealer').val();
    let description = $('#description').val();
    let width = $('#width').val();
    let roof = $('#roof_length').val();
    let frame = $('#frame_length').val();
    let leg = $('#leg_height').val();
    let carport_price = $('#carport_price').val();
    let land_level = $('input[name=surface_level]:checked').val();
    let installation = $('input[name=ready_istallation]:checked').val();
    let electricity = $('input[name=electricity_available]:checked').val();
    let surface = $('input[name=installation_surface]:checked').val();
    let payment = $('input[name=payment]:checked').val();

    if (customer_first_name == "") {
        Swal.fire({
            icon: 'warning',
            title: 'Name missing'
        })
        return;
    }
    if (customer_last_name == "") {
        Swal.fire({
            icon: 'warning',
            title: 'Last name missing'
        })
        return;
    }
    if (customer_address == "") {
        Swal.fire({
            icon: 'warning',
            title: 'Address missing'
        })
        return;
    }
    if (customer_city == "") {
        Swal.fire({
            icon: 'warning',
            title: 'City missing'
        })
        return;
    }
    if (customer_state == "") {
        Swal.fire({
            icon: 'warning',
            title: 'State missing'
        })
        return;
    }
    if (customer_zip == "") {
        Swal.fire({
            icon: 'warning',
            title: 'Zip missing'
        })
        return;
    }
    if (customer_phone == "") {
        Swal.fire({
            icon: 'warning',
            title: 'Phone missing'
        })
        return;
    }
    if (dealer_id == "") {
        Swal.fire({
            icon: 'warning',
            title: 'Dealer missing'
        })
        return;
    }
    if (description == "" || description == 0) {
        Swal.fire({
            icon: 'warning',
            title: 'Carport missing'
        })
        return;
    }
    if (width == "" || width == 0) {
        Swal.fire({
            icon: 'warning',
            title: 'Width missing'
        })
        return;
    }
    if (roof == "" || roof == 0) {
        Swal.fire({
            icon: 'warning',
            title: 'Roof missing'
        })
        return;
    }
    if (frame == "" || frame == 0) {
        Swal.fire({
            icon: 'warning',
            title: 'Frame missing'
        })
        return;
    }
    if (leg == "" || leg == 0) {
        Swal.fire({
            icon: 'warning',
            title: 'leg missing'
        })
        return;
    }
    if (carport_price == "" || carport_price == 0) {
        Swal.fire({
            icon: 'warning',
            title: 'Carport price missing'
        })
        return;
    }
    if(color_top == ""){
        Swal.fire({
            icon: 'warning',
            title: 'Select top color'
        })
        return;
    }
    if(color_sides == ""){
        Swal.fire({
            icon: 'warning',
            title: 'Select sides color'
        })
        return;
    }
    if(color_ends == ""){
        Swal.fire({
            icon: 'warning',
            title: 'Select ends color'
        })
        return;
    }
    if(color_trim == ""){
        Swal.fire({
            icon: 'warning',
            title: 'Select trim color'
        })
        return;
    }
    if (land_level == undefined) {
        Swal.fire({
            icon: 'warning',
            title: 'Land level missing'
        })
        return;
    }
    if (installation === undefined) {
        Swal.fire({
            icon: 'warning',
            title: 'Installation missing'
        })
        return;
    }
    if (electricity === undefined) {
        Swal.fire({
            icon: 'warning',
            title: 'Electricity missing'
        })
        return;
    }
    if (surface === undefined) {
        Swal.fire({
            icon: 'warning',
            title: 'Surface missing'
        })
        return;
    }
    if (payment === undefined) {
        Swal.fire({
            icon: 'warning',
            title: 'Payment missing'
        })
        return;
    }

    let data = {
        'number_invoice': $('#number_invoice').val(),
        'invoice_date': $('#inv_date').val(),
        'note': $('#note').val(),
        'customer_first_name': customer_first_name,
        'customer_last_name': customer_last_name,
        'customer_address': customer_address,
        'customer_city': customer_city,
        'customer_state': customer_state,
        'customer_zip': customer_zip,
        'customer_phone': customer_phone,
        'dealer_id': dealer_id,
        'description': description,
        'width': width,
        'roof_length': roof,
        'frame_length': frame,
        'leg_height': leg,
        'gauge': $('#gauge').val(),
        'carport_price': carport_price,
        'color_top': color_top,
        'color_sides': color_sides,
        'color_ends': color_ends,
        'color_trim': color_trim,
        'carport_type': $('#carport_type').val(),
        'carport_type_price': $('#carport_type_price').val(),
        'ad_leg_height': $('input:radio[name=ad_leg_height]:checked').val() ? $('input:radio[name=ad_leg_height]:checked').val() : "",
        'ad_leg_height_desc': $('#ad_leg_height_desc').val(),
        'ad_leg_height_price': $('#ad_leg_height_price').val(),
        'bothSidesOptions': $('input:radio[name=bothSidesOptions]:checked').val() ? $('input:radio[name=bothSidesOptions]:checked').val() : "" ,
        'side_quantity': $('#side_quantity').val(),
        'priceSide': $('#priceSide').val(),
        'EndOptions_both_end': $('input:radio[name=EndOptions]:checked').val() ? $('input:radio[name=EndOptions]:checked').val() : "",
        'end_quantity': $('#end_quantity').val(),
        'priceEnd': $('#priceEnd').val(),
        'gableOptions': $('input:radio[name=gableOptions]:checked').val() ? $('input:radio[name=gableOptions]:checked').val() : "",
        'gable_quantity': $('#gable_quantity').val(),
        'priceGable': $('#priceGable').val(),
        'rollUp': $('#rollUp').val(),
        'rollUpColor': $('#rollUpColor').val(),
        'sizeRoll': $('#sizeRoll').val(),
        'priceRoll': $('#priceRoll').val(),
        'wallDoor': $('#wallDoor').val(),
        'sizeWall': $('#sizeWall').val(),
        'priceWall': $('#priceWall').val(),
        'window': $('#window').val(),
        'sizeWindow': $('#sizeWindow').val(),
        'priceWindow': $('#priceWindow').val(),
        'anchor_system_decline': $('input[name=anchor_system_decline]:checked').val() ? $('input[name=anchor_system_decline]:checked').val() : "",
        'ancla': $('input[name=suelo]:checked').val() ? $('input[name=suelo]:checked').val() : "",
        'anchor_system_quantity': $('#anchor_system_quantity').val(),
        'anchor_system_price': $('#anchor_system_price').val(),
        'total_sale': $('#total_sale').val(),
        'tax': $('#tax').val(),
        'tax_exempt': $('#tax_exempt').val(),
        'total': $('#total').val(),
        'non_tax_exempt': $('#non_tax_exempt').val(),
        'deposit': $('#deposit').val(),
        'balance_due': $('#balance_due').val(),
        'special_instructions': $('#special_instructions').val(),
        'surface_level': land_level,
        'ready_istallation': installation,
        'electricity_available': electricity,
        'installation_surface': surface,
        'payment': payment,
        'other_payment': $('#payment_other').val(),
        'vProducts': vProducts.length > 0 ? vProducts : "",
        'action': 'saveOrder'
    };
    $.ajax({
        url: 'php/get-info.php',
        type: 'POST',
        data: data,
        success: function(data) {
            if (data.error) {
                Swal.fire({
                    icon: 'error',
                    title: data.msg
                })
            }else{
                Swal.fire({
                    icon: 'success',
                    title: data.msg
                })
                home();
            }
        }
    });
}

function getHomeInfo() {
    $.ajax({
        type: "POST",
        url: "php/get-info.php",
        data: {
            'action': 'getHomeInfo'
        },
        success: function(data) {
            $('#orders-table-section').not(':first').not(':last').remove();
            let html = '';
            for (let i = 0; i < data.length; i++) {
                html += '<tr>';
                html += '<th scope="row">' + data[i].id + '</th>';
                html += '<td>' + data[i].date + '</td>';
                html += '<td>' + data[i].customer + '</td>';
                html += '<td></td>';
                html += '<td>' + data[i].status + '<button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#statusModal">Show status</button></td>';
                html += '<td></td>';
                html += '<td></td>';
                html += '</tr>';
            }
            $('#body_orders_table').append(html);
        }
    });
}

function countAdditionalLeg(){
    let leg = $("#leg_height").val();

    if(leg > 6){

        $("#ad_leg_height_desc").val(leg-6);
    }else{
        if (leg <= 6 && leg > 0) {
            $("#ad_leg_height_desc").val(0);
        } 
    }
}

function addRow(tableID) {
    let cid = $("#customer").val();
    let invRows = $("#bodyItem tr").length;
    if (cid == "") {
        alert("Select Customer");
        return;
    }

    if (invRows <= 0) {
        invRows = 1;
    } else {
        invRows++;
    }

    // Obtiene una referencia a la tabla
    var tableRef = document.getElementById(tableID);

    // Inserta una fila en la tabla, en el índice 0
    var newRow = tableRef.insertRow(invRows-1);

    // Inserta una celda en la fila, en el índice 0
    var cellUno = newRow.insertCell(0);

    // Inserta una celda en la fila, en el índice 1
    var cellDos = newRow.insertCell(1);
    
    // Inserta una celda en la fila, en el índice 2
    var cellTres = newRow.insertCell(2);

    // Inserta una celda en la fila, en el índice 3
    var cellCuatro = newRow.insertCell(3);

    //Item...
    var item = document.createElement('input');
    item.setAttribute('style', 'width:100%;');
    item.setAttribute('id', 'item_' + invRows);
    item.setAttribute('name', 'item_' + invRows);
    item.setAttribute('data-rowid', invRows);
    item.setAttribute('onchange', 'change_product(' + invRows + ', "change_item");');
    
    
    // Añade un nodo de texto a la celda
    cellUno.appendChild(item);

    //Quantity
    var am = document.createElement('input');
    am.setAttribute('type', 'number');
    am.setAttribute('style', 'width:100%;');
    am.setAttribute('id', 'amount_' + invRows);
    am.setAttribute('name', 'amount_' + invRows);
    am.setAttribute('value', 0);
    am.setAttribute('data-rowid', invRows);
    am.setAttribute('onchange', 'change_product(' + invRows + ', "change_amount"); soloNumero(this);');

    cellDos.appendChild(am);

    //Price
    var pr = document.createElement('input');
    pr.setAttribute('type', 'number');
    pr.setAttribute('style', 'width:100%;');
    pr.setAttribute('id', 'price_' + invRows);
    pr.setAttribute('name', 'price_' + invRows);
    pr.setAttribute('value', 0);
    pr.setAttribute('data-rowid', invRows);
    pr.setAttribute('onchange', 'change_product(' + invRows + ', "change_price"); soloNumero(this);');

    cellTres.appendChild(pr);

    var de = document.createElement('i');
    de.setAttribute('class', 'fa-solid fa-trash-can');
    de.setAttribute('style', 'color: red;');
    de.setAttribute('onclick', 'deleteRow('+ invRows +');');

    cellCuatro.appendChild(de);

        
    
}

function deleteRow(row) {
    if (vProducts.length <= 0) {
        $("#item_"+row).parent().parent().remove();
    }else{
        for (const key in vProducts) {
            if (vProducts[key].row === row) {
                vProducts.splice(key, 1);
               
            }
        }
        $("#item_"+row).parent().parent().remove();
    }
    console.log(vProducts);
    console.log(vProducts[0]);
    calTotal();
    
}

function change_product(row, action) {

    let item = $("#item_" + row).val();
    let amount = $("#amount_" + row).val();
    let price = $("#price_" + row).val();

    switch (action) {
        case 'change_item':
            
            let itemObj = {};
            if (vProducts.length <= 0) {
                console.log("agrega primer item");
                itemObj.row = row;
                itemObj.item = item;
                itemObj.amount = parseInt(amount);
                itemObj.price = parseFloat(price);

                vProducts.push(itemObj);
            } else {
                if (vProducts.find(element => element.row === row)) {
                    console.log("actualiza item");
                    for (const key in vProducts) {
                        if (vProducts[key].row === row) {
                            vProducts[key].item = item;
                            vProducts[key].amount = parseInt(amount);
                            vProducts[key].price = parseFloat(price);
                        }
                    }
                } else {
                    console.log("agrega otro item");
                    itemObj.row = row;
                    itemObj.item = item;
                    itemObj.amount = parseInt(amount);
                    itemObj.price = parseFloat(price);

                    vProducts.push(itemObj);
                }
            }
            calTotal();
             
            break;
        case 'change_amount':
            
            for (const key in vProducts) {
                if (vProducts[key].row === row) {
                    console.log("cambia amount");
                    vProducts[key].amount = parseInt(amount);
                }
            }
            calTotal();
            break;
        case 'change_price':
            
            for (const key in vProducts) {
                if (vProducts[key].row === row) {
                    console.log("cambia price");
                    vProducts[key].price = parseFloat(price);
                }
            }
            calTotal();
            break;

        default:
            break;
    }

}

function validTax() {
    let taxExempt = $("#tax_exempt").val();

    if (taxExempt >= 0) {
        $("#tax").val(0);
    };
    calTotal();
}

function calTotal() {
    //PRECIOS
    let priceCarport = $("#carport_price").val() === undefined ? parseFloat(0) : ($("#carport_price").val() == "") ? parseFloat(0) : parseFloat($("#carport_price").val());
    let priceCarportType = $("#carport_type_price").val() === undefined ? parseFloat(0) : $("#carport_type_price").val() == "" ? parseFloat(0) : ($("#carport_type_price").val() == "") ? parseFloat(0) : parseFloat($("#carport_type_price").val());
    let priceLegHeight = $("#ad_leg_height_price").val() === undefined ? parseFloat(0) : $("#ad_leg_height_price").val() == "" ? parseFloat(0) : parseFloat($("#ad_leg_height_price").val());
    let priceSide = $("#priceSide").val() === undefined ? parseFloat(0) : $("#priceSide").val() == "" ? parseFloat(0) : parseFloat($("#priceSide").val());
    let priceEnd = $("#priceEnd").val() === undefined ? parseFloat(0) : $("#priceEnd").val() == "" ? parseFloat(0) : parseFloat($("#priceEnd").val());
    let priceGable = $("#priceGable").val() === undefined ? parseFloat(0) : $("#priceGable").val() == "" ? parseFloat(0) : parseFloat($("#priceGable").val());
    let priceRoll = $("#priceRoll").val() === undefined ? parseFloat(0) : $("#priceRoll").val() == "" ? parseFloat(0) : parseFloat($("#priceRoll").val());
    let priceWall = $("#priceWall").val() === undefined ? parseFloat(0) : $("#priceWall").val() == "" ? parseFloat(0) : parseFloat($("#priceWall").val());
    let priceWindow = $("#priceWindow").val() === undefined ? parseFloat(0) : $("#priceWindow").val() == "" ? parseFloat(0) : parseFloat($("#priceWindow").val());
    let priceAnchor = $("#anchor_system_price").val() === undefined ? parseFloat(0) : $("#anchor_system_price").val() == "" ? parseFloat(0) : parseFloat($("#anchor_system_price").val());

    //CANTIDADES
    let quantityLegHeight = $("#ad_leg_height_desc").val() === undefined ? parseFloat(0) : $("#ad_leg_height_desc").val() == "" ? parseFloat(0) : parseFloat($("#ad_leg_height_desc").val());
    let quantitySide = $("#side_quantity").val() === undefined ? parseFloat(0) : $("#side_quantity").val() == "" ? parseFloat(0) : parseFloat($("#side_quantity").val());
    let quantityEnd = $("#end_quantity").val() === undefined ? parseFloat(0) : $("#end_quantity").val() == "" ? parseFloat(0) : parseFloat($("#end_quantity").val());
    let quantityGable = $("#gable_quantity").val() === undefined ? parseFloat(0) : $("#gable_quantity").val() == "" ? parseFloat(0) : parseFloat($("#gable_quantity").val());
    let quantityRoll = $("#sizeRoll").val() === undefined ? parseFloat(0) : $("#sizeRoll").val() == "" ? parseFloat(0) : parseFloat($("#sizeRoll").val());
    let quantityWall = $("#sizeWall").val() === undefined ? parseFloat(0) : $("#sizeWall").val() == "" ? parseFloat(0) : parseFloat($("#sizeWall").val());
    let quantityWindow = $("#sizeWindow").val() === undefined ? parseFloat(0) : $("#sizeWindow").val() == "" ? parseFloat(0) : parseFloat($("#sizeWindow").val());
    let quantityAnchor = $("#anchor_system_quantity").val() === undefined ? parseFloat(0) : $("#anchor_system_quantity").val() == "" ? parseFloat(0) : parseFloat($("#anchor_system_quantity").val());

    let totalSale = 0;
    let totalLeg = 0;
    let totalSide = 0;
    let totalEnd = 0;
    let totalGable = 0;
    let totalRoll = 0;
    let totalWall = 0;
    let totalWindow = 0;
    let totalAnchor = 0;

    if(priceLegHeight > 0){
        
        totalLeg = priceLegHeight * quantityLegHeight;
        $("#ad_leg_height_price_total").val(totalLeg);
    }
    if(priceSide > 0){
        totalSide = priceSide * quantitySide;
        $("#priceSideTotal").val(totalSide);
    }

    if(priceEnd > 0){
        totalEnd = priceEnd * quantityEnd;
        $("#priceEndTotal").val(totalEnd);
    }
    if(priceGable > 0){
        totalGable = priceGable * quantityGable;
        $("#priceGableTotal").val(totalGable);
    }
    if(priceRoll > 0){
        totalRoll = priceRoll * quantityRoll;
        $("#priceRollTotal").val(totalRoll);
    }
    if(priceWall > 0){
        totalWall = priceWall * quantityWall;
        $("#priceWallTotal").val(totalWall);
    }
    if(priceWindow > 0){
        totalWindow = priceWindow * quantityWindow;
        $("#priceWindowTotal").val(totalWindow);
    }
    if(priceAnchor > 0){
        totalAnchor = priceAnchor * quantityAnchor;
        $("#anchor_system_price_total").val(totalAnchor);
    }

    if (vProducts.length <= 0) {
        console.log("calcula total sin items");

        totalSale = priceCarport + 
        priceCarportType + 
        (priceLegHeight*quantityLegHeight) + 
        (priceSide*quantitySide) + 
        (priceEnd*quantityEnd) + 
        (priceGable*quantityGable) + 
        (priceRoll*quantityRoll) + 
        (priceWall*quantityWall) + 
        (priceWindow*quantityWindow)+
        (priceAnchor*quantityAnchor);

        console.log(totalSale);
    } else {
        console.log("calcula total con items");

        totalSale = priceCarport + 
        priceCarportType + 
        (priceLegHeight*quantityLegHeight) + 
        (priceSide*quantitySide) + 
        (priceEnd*quantityEnd) + 
        (priceGable*quantityGable) + 
        (priceRoll*quantityRoll) + 
        (priceWall*quantityWall) + 
        (priceWindow*quantityWindow)+
        (priceAnchor*quantityAnchor);

        for (const key in vProducts) {
            console.log("price: " +parseFloat(vProducts[key].price) +" amount: "+parseFloat(vProducts[key].amount));
            totalSale = parseFloat(totalSale) + (parseFloat(vProducts[key].price) * parseFloat(vProducts[key].amount));
        }

    }

    let tax = $("#tax").val() == undefined ? parseFloat(0) : $("#tax").val() == "" ? parseFloat(0) : parseFloat($("#tax").val());
    let taxExempt = parseFloat($("#tax_exempt").val());

    $("#total_sale").val(totalSale);

    let total_tax = (totalSale * tax)/100;
    let total = totalSale + total_tax;

    $("#total").val(total);

    let taxContractor = $("#non_tax_exempt").val() == undefined ? parseFloat(0) : $("#non_tax_exempt").val() == "" ? parseFloat(0) : parseFloat($("#non_tax_exempt").val());
    let deposit = $("#deposit").val() == undefined ? parseFloat(0) : $("#deposit").val() == "" ? parseFloat(0) : parseFloat($("#deposit").val());
    let balanceDue = (total + taxContractor) - deposit;
    $("#balance_due").val(balanceDue);


}


function getAllInfo(customer_id) {
    console.log('Getting all informacion');
    $.ajax({
        url: 'php/get-info.php',
        type: 'POST',
        data: {
            'action': 'getAllInfo'
        },
        success: function(data) {
            //alert(data.msg);
            //window.location.reload();
        }
    });

}

function print(){
    console.log(window.frames);
    console.log("generated pdf");
    /*let pdf = document.getElementById('new-order-section');
    var file = new Blob([pdf], {type: 'application/pdf'});
    var fileURL = window.URL.createObjectURL(file);

    
    window.open(fileURL,"_blank");

    
    */
    var doc = new jsPDF('p', 'pt', 'letter');
    
    var elementHTML = document.getElementById("new-order-section");
    var specialElementHandlers = {
        '#elementH': function (element, renderer) {
            return true;
        }
    };

    doc.fromHTML(elementHTML, 15, 15, {
        'width': 170,
        'elementHandlers': specialElementHandlers
    });

    doc.save('sample-document.pdf');

    /*
    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };
    
    doc.fromHTML($('#new-order-section').html(), 15, 15, {
        'width': 700,
        'elementHandlers': specialElementHandlers
    });
    doc.save('sample_file.pdf');
    */
    
}

function getAddresCustomer(customer_id) {
    $.ajax({
        url: 'php/get-info.php',
        type: 'POST',
        data: {
            'action': 'get_customer_info',
            'customer_id': customer_id,
        },
        success: function(data) {
            $('#address_customer').empty();
            $('#address_customer').css('padding-top', '15px');
            $('#address_customer').css('padding-bottom', '15px');
            $('#address_customer').append('<h5>Phone: ' + data.company_phone + '</h5>')
            $('#address_customer').append('<h4>Ship To:</h4>');
            var select = $("<select></select>").attr("id", "shipping_address").attr("name", "shipping_address").attr('style', "width: 100%;");
            $.each(data.addresses, function(index, json) {
                var text_address = json.address + ' ' + json.city + ', ' + json.state + ' ' + json.zip;
                select.append($("<option></option>").attr("value", json.address_id).text(text_address));
            });
            $('#address_customer').append(select);
            $('#shipping_address').select2();
            $("#btnShipAddres").show();

            $("#description").empty("");
            
            let selectProducts = document.getElementById('description');

            var opt = document.createElement('option');
            opt.value = 0;
            opt.innerHTML = "Select";
            selectProducts.appendChild(opt1);
            //SE AGREGAN LOS PRODUCTOS
            console.log(data.products);
            for (const key in data.products) {
                
                var opt = document.createElement('option');
                opt.value = data.products[key].id;
                opt.innerHTML = data.products[key].name;
                selectProducts.appendChild(opt);
                
            }
            console.log(data);
        }
    });
}

function getProducts() {
    $.ajax({
        url: 'php/get-info.php',
        type: 'POST',
        data: {
            'action': 'get_products',
            
        },
        success: function(data) {
            $("#description").empty();
            let selectProducts = document.getElementById('description');
            var opt1 = document.createElement('option');
                opt1.value = 0;
                opt1.innerHTML = "Select";
                selectProducts.appendChild(opt1);
            //SE AGREGAN LOS PRODUCTOS
            console.log(data.products);
            for (const key in data.products) {
                
                var opt = document.createElement('option');
                opt.value = data.products[key].id;
                opt.innerHTML = data.products[key].name;
                selectProducts.appendChild(opt);
                
            }
            console.log(data);
        }
    });
}

function getInfoDealer(dealer_id) {
    $.ajax({
        url: 'php/get-info.php',
        type: 'POST',
        data: {
            'action': 'get_dealer_info',
            'dealer_id': dealer_id,
        },
        success: function(data) {
            $('#address_dealer').empty();
            $('#address_dealer').css('padding-top', '15px');
            $('#address_dealer').css('padding-bottom', '15px');
            $('#address_dealer').append('<h5>Phone: ' + data.phone + '</h5>');
            $('#address_dealer').append('<h5>Address: ' + data.address + '</h5>');
            console.log(data);
        }
    });
}

function setInvoiceNumber() {
    $.ajax({
        url: 'php/get-info.php',
        type: 'POST',
        data: {
            'action': 'get_inv_number',
        },
        success: function(data) {
            $('#number_invoice').val(data.invNum);
            console.log(data);
        }
    });
}

function saveInvoice() {

    data = {
        "number_invoice": $('#number_invoice').val(),
        "date_invoice": $('#inv_date').val(),
        "customer_id": $('#customer').val(),
        "dealer_id": $('#dealer').val(),


    }
}
function soloNumero(input) {
    console.log(Number(input.value));
    console.log(input);
    input.value = Number(input.value);
}