function load_ship_modal(){
    $("#ship_state").select2({
        dropdownParent: $('#addShipAddressModal')
    });
    let cid = $("#customer").val();
    //$("#addShipAddressModal").modal('show');
	document.getElementById('address_bubbles').innerHTML = '';
	document.getElementById('ship-address-customer').innerHTML = '';
	document.getElementById('ship_address_cid').value = '';
	document.getElementById('ship_address').value = '';
	document.getElementById('ship_city').value = '';
	document.getElementById('ship_state').value = '';
	document.getElementById('ship_zip').value = '';

    $.ajax({
        url: 'php/get-info.php',
        type: 'POST',
        data: {
            'cid': cid,
            'action': 'getShipAddress'
        },
        success: function(data) {
            if (data.error) {
                Swal.fire({
                    icon: 'error',
                    title: data.msg
                })
            }else{
                
                
                if(data.rows === true){
                    document.getElementById('ship-address-customer').innerHTML = data.cname;
                    document.getElementById('ship_address_cid').value = cid;
                    for(var i = 0; i < data.addresses.length; i++){
                        var a = data.addresses[i];

                        var md = document.getElementById('address_bubbles');
                        var d = document.createElement('div');
                        d.id = 'sa_'+a.address_id;
                        d.setAttribute('class','alert alert-info');
                        var btn = document.createElement('button');
                        btn.setAttribute('type','button');
                        btn.setAttribute('class','close');
                        //btn.setAttribute('data-dismiss','alert');
                        btn.setAttribute('onclick','remove_ship_address('+a.address_id+');');
                        btn.setAttribute('aria-hidden','true');
                        btn.setAttribute('style','opacity:1;');
                        btn.innerHTML = '&times;';
                        d.appendChild(btn);
                        var strong = document.createElement('strong');
                        strong.innerHTML = a.address+'<br>'+a.city+', '+a.state+' '+a.zip;
                        d.appendChild(strong);
                        md.appendChild(d);
                    }
                }else {
                    document.getElementById('ship-address-customer').innerHTML = data.cname;
                    document.getElementById('ship_address_cid').value = cid;
                }
            }
        }
    });
}


function add_ship_address(){
	var saddress = document.getElementById('ship_address').value;
	if(saddress === ''){
		document.getElementById('ship_address_error').innerHTML = 'Please Enter The Street Address';
		return;
	}
	var scity = document.getElementById('ship_city').value;
	if(scity === ''){
		document.getElementById('ship_address_error').innerHTML = 'Please Enter The City';
		return;
	}
	var sstate = document.getElementById('ship_state').value;
	if(sstate === ''){
		document.getElementById('ship_address_error').innerHTML = 'Please Enter The State';
		return;
	}
	var szip = document.getElementById('ship_zip').value;
	if(szip === ''){
		document.getElementById('ship_address_error').innerHTML = 'Please Enter The Zip Code';
		return;
	}
	var cid = document.getElementById('customer').value;

    let data = {
        'saddress': saddress,
        'scity': scity,
        'sstate': sstate,
        'szip': szip,
        'cid': cid,
        'action': 'addShipAddres'
    };

    $.ajax({
        url: 'php/get-info.php',
        type: 'POST',
        data: data,
        success: function(data) {
            if (data.error) {
                Swal.fire({
                    icon: 'error',
                    title: data.msg
                })
            }else{
                Swal.fire({
                    icon: 'success',
                    title: data.msg
                })
                var md = document.getElementById('address_bubbles');
                var d = document.createElement('div');
                d.id = 'sa_'+data.ship_address_id;
                d.setAttribute('class','alert alert-info');
                var btn = document.createElement('button');
                btn.setAttribute('type','button');
                btn.setAttribute('class','close');
                //btn.setAttribute('data-dismiss','alert');
                btn.setAttribute('onclick','remove_ship_address('+data.ship_address_id+');');
                btn.setAttribute('aria-hidden','true');
                btn.setAttribute('style','opacity:1;');
                btn.innerHTML = '&times;';
                d.appendChild(btn);
                var strong = document.createElement('strong');
                strong.innerHTML = saddress+'<br>'+scity+', '+sstate+' '+szip;
                d.appendChild(strong);
                md.appendChild(d);
                //Reset Form Fields...
                document.getElementById('ship_address').value = '';
                document.getElementById('ship_city').value = '';
                document.getElementById('ship_state').value = '';
                document.getElementById('ship_zip').value = '';
                getAddresCustomer(cid);
            }
        }
    });

}



function remove_ship_address(aid){
	if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
		console.log(this.responseText);
		var r = JSON.parse(this.responseText);
		if(r.response === 'GOOD'){
			document.getElementById('sa_'+aid).remove();
		}else{
			console.warn('An error occurred removing the information you requested');
		}
      
    }
  }
  xmlhttp.open("GET","orders/php/remove-ship-address.php?aid="+aid,true);
  xmlhttp.send();
}