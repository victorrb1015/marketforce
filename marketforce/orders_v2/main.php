<?php
ini_set('session.save_path', '/home/marketforce/mf_temp');
session_start();
$db_host = 'localhost';
$db_user = 'marketfo_mf';
$db_pass = '#NgTFJQ!z@t8';
$db_mf_db = 'marketfo_marketforce';
$db_db = $_SESSION['org_db_name'];
$mysql0 = new mysqli($db_host, $db_user, $db_pass, 'marketfo_marketforce');
$sql_terms = "SELECT terms from organizations where org_id like '" . $_SESSION['org_id'] . "'";
$result0 = $mysql0->query($sql_terms);
$res_array = $result0->fetch_array();

$mysqli = new mysqli($db_host, $db_user, $db_pass, $db_db);

include '../security/session/session-settings.php';
$sql_customer = "SELECT * FROM `order_customers` WHERE `inactive` != 'Yes' ORDER BY company_name";
$result_customer = $mysqli->query($sql_customer);
$sql_dealers = "SELECT ID, name, address, city, state, zip, phone  FROM `dealers` where inactive NOT LIKE 'Yes'";
$result_dealers = $mysqli->query($sql_dealers);

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/spinner.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
    <!-- SELECT2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    
    <title>Orders</title>
    <style>
        body {
            background-color: black;
        }

        .gj-datepicker {
            width: auto;
        }
    </style>
</head>

<body>

    <div class="row" style="width:auto;">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--Buttons Control-->
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-2">
                                <button type="button" class="btn btn-success btn-lg" id="new_order_button"><i class="fas fa-file-signature"></i> New Order
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-10">
                                <button type="button" class="btn btn-dark" id="home" onclick="home();"><i class="fas fa-file-invoice-dollar"></i> Orders
                                </button>
                                <button type="button" class="btn btn-dark" id="pending"><i class="fas fa-clock"></i>
                                    Pending Orders
                                </button>
                                <button type="button" class="btn btn-dark" id="invoiced"><i class="fas fa-file-invoice"></i> Invoiced Orders
                                </button>
                                <button type="button" class="btn btn-dark" id="paid"><i class="fas fa-hand-holding-usd"></i> Paid Orders
                                </button>
                                <button type="button" class="btn btn-dark" id="out_of_time"><i class="fas fa-ban"></i>
                                    Orders Out Of Time
                                </button>
                                <button type="button" class="btn btn-dark" id="search"><i class="fas fa-search"></i>
                                    Search Orders
                                </button>
                            </div>
                        </div>
                    </div> <!-- / Buttons Control-->
                    <hr>
                    <div id="orders-table-section">
                        <table class="table table-bordered table-striped" id="orders-table">
                            <thead>
                                <tr>
                                    <th scope="col">Date</th>
                                    <th scope="col">User</th>
                                    <th scope="col">Order Info</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Notes</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="body_orders_table">
                                
                            </tbody>
                        </table>
                    </div>
                    <div id="new-order-section">
                        <br>
                        <div class="card">
                            <div class="card-body">
                                <form>
                                    <div class="row" style="padding-top: 15px;">
                                        <!-- Logo, Address and Basic order Info -->
                                        <div class="col-4">
                                            <img alt="Logo" src="<?php echo $_SESSION['org_logo_url_alt']; ?>" style="width:50%;">
                                        </div>
                                        <div class="col-4">
                                            <p><?php echo $_SESSION['org_name']; ?></p>
                                            <p><?php echo $_SESSION['org_address']; ?>
                                                <br><?php echo $_SESSION['org_city'] . ', ' . $_SESSION['org_state'] . ' ' . $_SESSION['org_zip']; ?>
                                            </p>
                                            <p><?php echo $_SESSION['org_phone']; ?></p>
                                        </div>
                                        <div class="col-4">
                                            <div class="input-group mb-3">
                                                <span class="input-group-text"><?php echo $_SESSION['full_name']; ?></span>
                                                <textarea class="form-control order-background" name="note" id="note" cols="5" rows="5"></textarea>
                                            </div>
                                            <div class="input-group mb-3">
                                                <span class="input-group-text" id="num_inv"></span>
                                                <input type="text" class="form-control order-background" id="number_invoice" name="number_invoice" readonly>
                                            </div>
                                            <div class="input-group mb-3">
                                                <span class="input-group-text">Date</span>
                                                <input type="text" class="form-control" value="<? echo date("F d, Y"); ?>" name="inv_date" id="inv_date" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-top: 15px; padding-bottom: 15px;">
                                        <!-- customer Select  and Dealer Select -->
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text">First name</span>
                                                    <input type="text" class="form-control order-background" id="customer_first_name" name="customer_first_name" >
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text">Last name</span>
                                                    <input type="text" class="form-control order-background" id="customer_last_name" name="customer_last_name" >
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text">Address</span>
                                                    <input type="text" class="form-control order-background" id="customer_address" name="customer_address" >
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <div class="input-group mb-3">
                                                        <span class="input-group-text">City</span>
                                                        <input type="text" class="form-control order-background" id="customer_city" name="customer_city" >
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="input-group mb-3">
                                                        <span class="input-group-text">State</span>
                                                        <select class="form-select" name="customer_state" id="customer_state">
                                                            <option value="">Select</option>
                                                            <?php
                                                                $sq = "SELECT * FROM `states` ORDER BY `state` ASC";
                                                                $sg = $mysqli->query($sq);
                                                                while($sr = $sg->fetch_array()){
                                                                    echo '<option value="' . $sr['state'] . '">' . $sr['state'] . '</option>';
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="input-group mb-3">
                                                        <span class="input-group-text">Zip code</span>
                                                        <input type="text" class="form-control order-background" id="customer_zip" name="customer_zip">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <div class="input-group mb-3">
                                                        <span class="input-group-text">Phone</span>
                                                        <input type="text" class="form-control order-background" id="customer_phone" name="customer_phone">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--<select id="customer" name="customer" style="width: 100%;" onchange="getAddresCustomer(this.value);" style="width: 100%">
                                                <option value="">Select Customer</option>
                                                <?php
                                                /*
                                                while ($cr = $result_customer->fetch_array()) {
                                                    echo '<option value="' . $cr['ID'] . '">' . $cr['company_name'] . '</option>';
                                                } */
                                                ?>
                                            </select>
                                            <button class="btn btn-primary" style="display: none;" id="btnShipAddres" type="button" data-bs-toggle="modal" data-bs-target="#addShipAddressModal" onclick="load_ship_modal();">Add Address</button>
                                            <div id="address_customer"></div>
                                            -->
                                            
                                        </div>
                                        <div class="col-6">
                                            <select id="dealer" class="form-select" name="dealer" onchange="getInfoDealer(this.value); getProducts();">
                                                <option value="">Select Dealer</option>
                                                <?php
                                                while ($dr = $result_dealers->fetch_array()) {
                                                    echo '<option value="' . $dr['ID'] . '">' . $dr['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                            <div id="address_dealer"></div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-right: 10px;padding-left: 11px;">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text">Description</span>
                                                            <select name="description" class="form-select" id="description"></select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-sm-4">
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text">Width</span>
                                                            <input type="number" step="0.01" class="form-control order-background" id="width" name="width">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-sm-6">
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text">Roof Length</span>
                                                            <input type="number" step="0.01" class="form-control order-background" id="roof_length" name="roof_length" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-sm-6">
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text">Frame Length</span>
                                                            <input type="number" step="0.01" class="form-control order-background" id="frame_length" name="frame_length" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6">
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text">Leg Height</span>
                                                            <input type="number" class="form-control order-background" id="leg_height" name="leg_height" onchange="countAdditionalLeg();"> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2 col-sm-2">
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text">Gauge</span>
                                                            <select name="gauge" id="gauge" class="form-select">
                                                                <option value="">Select</option>
                                                                <option value="Standard">14(Standard)</option>
                                                                <option value="Optional">12(Optional)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 offset-md-3">
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text" style="background-color: deepskyblue;"> <b>Price</b> </span>
                                                            <input type="number" step="0.01" class="form-control order-background" style="background-color: deepskyblue;" id="carport_price" name="carport_price" min="0" onchange="calTotal();"> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <h5>Colors:</h5>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text">top</span>
                                                            <select name="color_top" id="color_top" class="form-select">
                                                                <option value="">Select color</option>
                                                                <option value="white">White</option>
                                                                <option value="forest_green">Forest Green</option>
                                                                <option value="hawaiian_blue">Hawaiian Blue</option>
                                                                <option value="zinc_gray">Zinc Gray</option>
                                                                <option value="black">Black</option>
                                                                <option value="barn_red">Barn Red</option>
                                                                <option value="light_stone">Light Stone</option>
                                                                <option value="pebble_beige">Pebble Beige</option>
                                                                <option value="pewter_gray">Pewter Gray</option>
                                                                <option value="brown">Brown</option>
                                                                <option value="mocha_tan">Mocha Tan</option>
                                                                <option value="taupe">Taupe</option>
                                                                <option value="burgundy">Burgundy</option>
                                                                <option value="cardinal_red">Cardinal Red</option>
                                                                <option value="galvalume">Galvalume</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text">Sides</span>
                                                            <select name="color_sides" id="color_sides" class="form-select">
                                                                <option value="">Select color</option>
                                                                <option value="white">White</option>
                                                                <option value="forest_green">Forest Green</option>
                                                                <option value="hawaiian_blue">Hawaiian Blue</option>
                                                                <option value="zinc_gray">Zinc Gray</option>
                                                                <option value="black">Black</option>
                                                                <option value="barn_red">Barn Red</option>
                                                                <option value="light_stone">Light Stone</option>
                                                                <option value="pebble_beige">Pebble Beige</option>
                                                                <option value="pewter_gray">Pewter Gray</option>
                                                                <option value="brown">Brown</option>
                                                                <option value="mocha_tan">Mocha Tan</option>
                                                                <option value="taupe">Taupe</option>
                                                                <option value="burgundy">Burgundy</option>
                                                                <option value="cardinal_red">Cardinal Red</option>
                                                                <option value="galvalume">Galvalume</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text">Ends</span>
                                                            <select name="color_ends" id="color_ends" class="form-select">
                                                                <option value="">Select color</option>
                                                                <option value="white">White</option>
                                                                <option value="forest_green">Forest Green</option>
                                                                <option value="hawaiian_blue">Hawaiian Blue</option>
                                                                <option value="zinc_gray">Zinc Gray</option>
                                                                <option value="black">Black</option>
                                                                <option value="barn_red">Barn Red</option>
                                                                <option value="light_stone">Light Stone</option>
                                                                <option value="pebble_beige">Pebble Beige</option>
                                                                <option value="pewter_gray">Pewter Gray</option>
                                                                <option value="brown">Brown</option>
                                                                <option value="mocha_tan">Mocha Tan</option>
                                                                <option value="taupe">Taupe</option>
                                                                <option value="burgundy">Burgundy</option>
                                                                <option value="cardinal_red">Cardinal Red</option>
                                                                <option value="galvalume">Galvalume</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text">Trim</span>
                                                            <select name="color_trim" id="color_trim" class="form-select">
                                                                <option value="">Select color</option>
                                                                <option value="white">White</option>
                                                                <option value="forest_green">Forest Green</option>
                                                                <option value="hawaiian_blue">Hawaiian Blue</option>
                                                                <option value="zinc_gray">Zinc Gray</option>
                                                                <option value="black">Black</option>
                                                                <option value="barn_red">Barn Red</option>
                                                                <option value="light_stone">Light Stone</option>
                                                                <option value="pebble_beige">Pebble Beige</option>
                                                                <option value="pewter_gray">Pewter Gray</option>
                                                                <option value="brown">Brown</option>
                                                                <option value="mocha_tan">Mocha Tan</option>
                                                                <option value="taupe">Taupe</option>
                                                                <option value="burgundy">Burgundy</option>
                                                                <option value="cardinal_red">Cardinal Red</option>
                                                                <option value="galvalume">Galvalume</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-top: 15px;">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="row" style="padding-top: 10px;padding-right: 10px;padding-left: 10px;">
                                                            <h5>Options:</h5>
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-8">
                                                                            <div class="input-group mb-3">
                                                                                <span class="input-group-text">Roof</span>
                                                                                <select name="carport_type" id="carport_type" class="form-select">
                                                                                    <option value="">Select</option>
                                                                                    <option value="regular_frame">Regular Frame</option>
                                                                                    <option value="a-frame">A-Frame</option>
                                                                                    <option value="vertical_roof">Vertical Roof</option>
                                                                                    <option value="all_vertical">All Vertical</option>
                                                                                </select>
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="carport_type_price" id="carport_type_price" min="0" onchange="calTotal();">
                                                                            </div>
                                                                            <label style="font-size: 12px;">each</label>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            
                                                                        </div><!-- Col -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!-- Row -->
                                                        <div class="row" style="padding-top: 10px;padding-right: 10px;padding-left: 10px;">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input" type="radio" id="ad_leg_height" name="ad_leg_height" value="leg_height">
                                                                                <label class="form-check-label" for="ad_leg_height">Additional Leg Height</label>
                                                                            </div>
                                                                            <br>
                                                                            <label style="font-size: 12px;">Standard six ft included at no additional cost.</label>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group mb-3">
                                                                                <input class="form-control order-background" type="number" id="ad_leg_height_desc" name="ad_leg_height_desc" min="0" readonly>
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="ad_leg_height_price" id="ad_leg_height_price" min="0" onchange="calTotal();" >
                                                                            </div>
                                                                            <label style="font-size: 12px;">each</label>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="ad_leg_height_price_total" id="ad_leg_height_price_total" min="0" readonly>
                                                                            </div>
                                                                            <label style="font-size: 12px;">total</label>
                                                                        </div><!-- Col -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!-- Row -->
                                                        <div class="row" style="padding-top: 10px;padding-right: 10px;padding-left: 10px;">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input" type="radio" id="bothSidesOptions_one_sides" name="bothSidesOptions" value="one_sides">
                                                                                <label class="form-check-label" for="bothSidesOptions_one_sides">One Side</label>
                                                                            </div>
                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input" type="radio" id="bothSidesOptions_both_sides" name="bothSidesOptions" value="both_sides">
                                                                                <label class="form-check-label" for="bothSidesOptions_both_sides">Both Sides</label>
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group mb-3">
                                                                                <input class="form-control order-background" type="number" name="side_quantity" id="side_quantity" min="0">
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group ">
                                                                                $ <input class="form-control order-background" type="number" name="priceSide" id="priceSide" min="0" onchange="calTotal();" >
                                                                            </div>
                                                                            <label style="font-size: 12px;">each</label>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group ">
                                                                                $ <input class="form-control order-background" type="number" name="priceSideTotal" id="priceSideTotal" min="0" readonly>
                                                                            </div>
                                                                            <label style="font-size: 12px;">total</label>
                                                                        </div><!-- Col -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!-- Row -->
                                                        <div class="row" style="padding-top: 10px;padding-right: 10px;padding-left: 10px;">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input" type="radio" id="EndOptions_one_end" name="EndOptions" value="one_end">
                                                                                <label class="form-check-label" for="EndOptions_one_end">One End</label>
                                                                            </div>
                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input" type="radio" id="EndOptions_both_end" name="EndOptions" value="both_end">
                                                                                <label class="form-check-label" for="EndOptions_both_end">Both Ends</label>
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group mb-3">
                                                                                <input class="form-control order-background" type="number" name="end_quantity" id="end_quantity" min="0">
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="priceEnd" id="priceEnd" min="0" onchange="calTotal();">
                                                                            </div>
                                                                            <label style="font-size: 12px;">each</label>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="priceEndTotal" id="priceEndTotal" min="0" readonly>
                                                                            </div>
                                                                            <label style="font-size: 12px;">total</label>
                                                                        </div><!-- Col -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!-- Row -->
                                                        <div class="row" style="padding-top: 10px;padding-right: 10px;padding-left: 10px;">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <div class="row">
                                                                                <label class="form-check-label" for="inlineCheckbox4" style="font-size: 18px;"><b>Gable Ends</b></label>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-6">
                                                                                    <div class="form-check form-check-inline">
                                                                                        <input class="form-check-input" type="radio" id="gable_option_single" name="gableOptions" value="single">
                                                                                        <label class="form-check-label">Single</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-6">
                                                                                    <div class="form-check form-check-inline">
                                                                                        <input class="form-check-input" type="radio" id="gable_option_double" name="gableOptions" value="double">
                                                                                        <label class="form-check-label">Double</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group mb-3">
                                                                                <input class="form-control order-background" type="number" name="gable_quantity" id="gable_quantity" min="0" >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="priceGable" id="priceGable" min="0" onchange="calTotal();" >
                                                                            </div>
                                                                            <label style="font-size: 12px;">each</label>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="priceGableTotal" id="priceGableTotal" min="0" >
                                                                            </div>
                                                                            <label style="font-size: 12px;">total</label>
                                                                        </div><!-- Col -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!-- Row -->
                                                        <div class="row" style="padding-top: 10px;padding-right: 10px;padding-left: 10px;">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-3">
                                                                            <div class="input-group mb-3">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-text">Roll-Up Door</span>
                                                                                    <select name="rollUp" id="rollUp" class="form-select">
                                                                                        <option value="">Select</option>
                                                                                        <option value="6x6">6'x6'</option>
                                                                                        <option value="8x7">8'x7'</option>
                                                                                        <option value="9x7">9'x7'</option>
                                                                                        <option value="10x8">10'x8'</option>
                                                                                        <option value="10x10">10'x10'</option>
                                                                                        <option value="10x12">10'x12'</option>
                                                                                        <option value="12x12">12'x12'</option>
                                                                                        <option value="12x14">12'x14'</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-3">
                                                                            <div class="input-group">
                                                                                <span class="input-group-text">Color</span>
                                                                                <select name="rollUpColor" id="rollUpColor" class="form-select">
                                                                                    <option value="">Select</option>
                                                                                    <option value="white">White</option>
                                                                                    <option value="forest_green">Forest Green</option>
                                                                                    <option value="hawaiian_blue">Hawaiian Blue</option>
                                                                                    <option value="zinc_gray">Zinc Gray</option>
                                                                                    <option value="black">Black</option>
                                                                                    <option value="barn_red">Barn Red</option>
                                                                                    <option value="light_stone">Light Stone</option>
                                                                                    <option value="pebble_beige">Pebble Beige</option>
                                                                                    <option value="pewter_gray">Pewter Gray</option>
                                                                                    <option value="brown">Brown</option>
                                                                                    <option value="mocha_tan">Mocha Tan</option>
                                                                                    <option value="taupe">Taupe</option>
                                                                                    <option value="burgundy">Burgundy</option>
                                                                                    <option value="cardinal_red">Cardinal Red</option>
                                                                                    <option value="galvalume">Galvalume</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-2">
                                                                            <div class="input-group mb-3">
                                                                                <input class="form-control order-background" type="number" name="sizeRoll" id="sizeRoll" min="0">
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="priceRoll" id="priceRoll" min="0" onchange="calTotal();" >
                                                                            </div>
                                                                            <label style="font-size: 12px;">each</label>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="priceRollTotal" id="priceRollTotal" min="0" >
                                                                            </div>
                                                                            <label style="font-size: 12px;">total</label>
                                                                        </div><!-- Col -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!-- Row -->
                                                        <div class="row" style="padding-top: 10px;padding-right: 10px;padding-left: 10px;">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="input-group">
                                                                                <span class="input-group-text">Wall-in Door</span>
                                                                                <select name="wallDoor" id="wallDoor" class="form-select">
                                                                                    <option value="">Select</option>
                                                                                    <option value="32x72">32" X 72"</option>
                                                                                    <option value="36x80">36" X 80"</option>
                                                                                </select>
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2 offset-md-2">
                                                                            <div class="input-group mb-3">
                                                                                <input class="form-control order-background" type="number" name="sizeWall" id="sizeWall">
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="priceWall" id="priceWall" min="0" onchange="calTotal();">
                                                                            </div>
                                                                            <label style="font-size: 12px;">each</label>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="priceWallTotal" id="priceWallTotal" min="0">
                                                                            </div>
                                                                            <label style="font-size: 12px;">total</label>
                                                                        </div><!-- Col -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!-- Row -->
                                                        <div class="row" style="padding-top: 10px;padding-right: 10px;padding-left: 10px;">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <div class="input-group">
                                                                                <span class="input-group-text">Window</span>
                                                                                <select name="window" id="window" class="form-select">
                                                                                    <option value="">Select</option>
                                                                                    <option value="24x36">24" X 36"</option>
                                                                                    <option value="30x36">30" X 36"</option>
                                                                                </select>
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2 offset-md-2">
                                                                            <div class="input-group mb-3">
                                                                                <input class="form-control order-background" type="number" name="sizeWindow" id="sizeWindow">
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="priceWindow" id="priceWindow" min="0" onchange="calTotal();">
                                                                            </div>
                                                                            <label style="font-size: 12px;">each</label>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="priceWindowTotal" id="priceWindowTotal" min="0" >
                                                                            </div>
                                                                            <label style="font-size: 12px;">total</label>
                                                                        </div><!-- Col -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!-- Row -->
                                                        <div class="row" style="padding-top: 10px;padding-right: 10px;padding-left: 10px;">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <div class="row">
                                                                                <label class="form-check-label" for="inlineCheckbox4"><b>Anchor System (Standard rebar included)</b></label>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-12">
                                                                                    <div class="form-check form-check-inline">
                                                                                        <input class="form-check-input" type="radio" id="option15" name="suelo" value="MHA">
                                                                                        <label class="form-check-label" for="inlineCheckbox4">Mobile Home</label>
                                                                                    </div>
                                                                                    <div class="form-check form-check-inline">
                                                                                        <input class="form-check-input" type="radio" id="option16" name="suelo" value="Asphalt">
                                                                                        <label class="form-check-label" for="inlineCheckbox4">Asphalt</label>
                                                                                    </div>
                                                                                    <div class="form-check form-check-inline">
                                                                                        <input class="form-check-input" type="radio" id="option17" name="suelo" value="Concrete">
                                                                                        <label class="form-check-label" for="inlineCheckbox4">Concrete</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-12">
                                                                                    <div class="form-check form-check-inline">
                                                                                        <input class="form-check-input" type="radio" id="anchor_system_decline" name="anchor_system" value="Decline">
                                                                                        <label class="form-check-label" >Decline</label>
                                                                                    </div>
                                                                                    <!-- <div class="form-check form-check-inline">
                                                                                        <input class="form-check-input" type="radio" id="anchor_system_accept" name="anchor_system" value="Accept">
                                                                                        <label class="form-check-label" >Accept</label>
                                                                                    </div> -->
                                                                                </div>
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group mb-3">
                                                                                <input class="form-control order-background" type="number" name="anchor_system_quantity" id="anchor_system_quantity" onchange="soloNumero(this);">
                                                                            </div>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="anchor_system_price" id="anchor_system_price" min="0" onchange="calTotal();">
                                                                            </div>
                                                                            <label style="font-size: 12px;">each</label>
                                                                        </div><!-- Col -->
                                                                        <div class="col-2">
                                                                            <div class="input-group">
                                                                                $ <input class="form-control order-background" type="number" name="anchor_system_price_total" id="anchor_system_price_total" min="0" >
                                                                            </div>
                                                                            <label style="font-size: 12px;">total</label>
                                                                        </div><!-- Col -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!-- Row -->
                                                    </div>
                                                </div>
                                            </div><!-- Col -->
                                            <div class="col-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h5>ALL ORDERS C.O.D.</h5>
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    <span class="input-group-text">Total Sale: </span>
                                                                </div>
                                                            </div><!-- Col -->
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    $<input type="number" class="form-control order-background" id="total_sale" name="total_sale" readonly>
                                                                </div>
                                                            </div><!-- Col -->
                                                        </div><!-- Row -->
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    <span class="input-group-text">Tax: </span>
                                                                </div>
                                                            </div><!-- Col -->
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    %<input type="number" class="form-control order-background" id="tax" name="tax" min="0" onchange="calTotal();">
                                                                </div>
                                                            </div><!-- Col -->
                                                        </div><!-- Row -->
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    <span class="input-group-text">Tax Exempt: </span>
                                                                </div>
                                                            </div><!-- Col -->
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    $<input type="number" class="form-control order-background" id="tax_exempt" name="tax_exempt" min="0" onchange="validTax(); ">
                                                                </div>
                                                            </div><!-- Col -->
                                                        </div><!-- Row -->
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    <span class="input-group-text">Total: </span>
                                                                </div>
                                                            </div><!-- Col -->
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    $<input type="number" class="form-control order-background" id="total" name="total" readonly>
                                                                </div>
                                                            </div><!-- Col -->
                                                        </div><!-- Row -->
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    <span class="input-group-text">Non-Tax</span>
                                                                    <br>
                                                                    <span class="input-group-text">Contractor Fees:</span>
                                                                </div>
                                                            </div><!-- Col -->
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    $<input type="number" class="form-control order-background" id="non_tax_exempt" name="non_tax_exempt" min="0" onchange="calTotal();" >
                                                                </div>
                                                            </div><!-- Col -->
                                                        </div><!-- Row -->
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    <span class="input-group-text">Deposit: </span>
                                                                </div>
                                                            </div><!-- Col -->
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    $<input type="number" class="form-control order-background" id="deposit" name="deposit" min="0" onchange="calTotal();" >
                                                                </div>
                                                            </div><!-- Col -->
                                                        </div><!-- Row -->
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    <span class="input-group-text">Balance Due: </span>
                                                                </div>
                                                            </div><!-- Col -->
                                                            <div class="col-6">
                                                                <div class="input-group mb-3">
                                                                    $<input type="number" class="form-control order-background" id="balance_due" name="balance_due" readonly>
                                                                </div>
                                                            </div><!-- Col -->
                                                        </div><!-- Row -->
                                                    </div><!-- Col -->
                                                </div><!-- Row -->
                                            </div><!-- Col -->
                                        </div><!-- Row -->
                                    </div><!-- Row -->
                                    </br>
                                    <div class="row" style="padding-top: 15px;padding-right: 10px;padding-left: 11px;">
                                        <button type="button" class="btn btn-secondary" onclick="addRow('bodyItem')"><b>Click here for aditional item</b></button>
                                        <div class="align-items-center" id="spinnerLoading" style="display: none;">
                                            <strong>Loading List...</strong>
                                            <div class="spinner-border ms-auto text-primary" role="status" aria-hidden="true"></div>
                                        </div>
                                        <table class="table table-striped table-bordered" style="margin-top: 10px">
                                            <thead>
                                                <tr>
                                                    <th>Item</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                </tr>
                                            </thead>
                                            <tbody id="bodyItem">

                                            </tbody>
                                        </table>
                                    </div><!-- Row -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <h5>Special</h5>
                                                            <h5>Instructions:</h5>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-floating">
                                                                <textarea class="form-control order-background" id="special_instructions" name="special_instructions" style="height: 108px"></textarea>
                                                                <label for="special_instructions"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- Row -->
                                    <div class="row" style="padding-top: 15px;">
                                        <div class="col-md-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <h6>Land level</h6>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" id="surface_level_y" name="surface_level" value="Yes">
                                                                        <label class="form-check-label" for="surface_level_y">Yes</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" id="surface_level_n" name="surface_level" value="No">
                                                                        <label class="form-check-label" for="surface_level_n">No</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" id="surface_level_not" name="surface_level" value="Not_sure">
                                                                        <label class="form-check-label" for="surface_level_n">Not Sure</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <h6>Ready For Installation?</h6>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" id="ready_istallation_y" name="ready_istallation" value="Yes">
                                                                        <label class="form-check-label" for="ready_istallation_y">Yes</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" id="ready_istallation_n" name="ready_istallation" value="No">
                                                                        <label class="form-check-label" for="ready_istallation_n">No</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <h6>Electricity Available Within 100ft of Site?</h6>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" id="electricity_available_y" name="electricity_available" value="Yes">
                                                                            <label class="form-check-label" for="electricity_available_y">Yes</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" id="electricity_available_n" name="electricity_available" value="No">
                                                                            <label class="form-check-label" for="electricity_available_n">No</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- Row -->
                                    <div class="row" style="padding-top: 15px;">
                                        <div class="col-md-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="col-md-12">
                                                        <h6>Installation Surface</h6>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" id="installation_surface_ground" name="installation_surface" value="ground">
                                                                <label class="form-check-label" for="installation_surface_ground">Ground</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" id="installation_surface_concrete" name="installation_surface" value="concrete">
                                                                <label class="form-check-label" for="installation_surface_g">Concrete</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" id="installation_surface_asphalt" name="installation_surface" value="asphalt">
                                                                <label class="form-check-label" for="installation_surface_g">Asphalt</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" id="installation_surface_gravel" name="installation_surface" value="gravel">
                                                                <label class="form-check-label" for="installation_surface_gravel">Gravel</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h6>I will make my final payment by:</h6>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" id="payment_cash" name="payment" value="cash">
                                                                <label class="form-check-label" for="payment_cash">Cash</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" id="payment_check" name="payment" value="check">
                                                                <label class="form-check-label" for="payment_check">Check</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" id="payment_cc" name="payment" value="cc">
                                                                <label class="form-check-label" for="payment_cc">CC</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" id="payment_po" name="payment" value="po">
                                                                <label class="form-check-label" for="payment_po">P.O.</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-check form-check-inline">
                                                                <label class="finput-group-text">Other: </label>
                                                                <input class="form-control" type="text" id="payment_other">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-top: 15px;padding-right: 10px;padding-left: 11px;">
                                        <div class="card">
                                            <div class="card-body">
                                                <p style="font-size: x-small;">Owr company <? echo $res_array[0]; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center" style="padding-top: 35px;padding-right: 10px;">
                                        <div class="col-md-6" style="padding-right: 1px;">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row" style="padding-top: 45px;">
                                                        <div class="col-md-6">
                                                            <hr>
                                                            <span class="input-group-text" >Buyer signature</span>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="input-group mb-3">
                                                                <span class="input-group-text">Date</span>
                                                                <input type="text" class="form-control order-background" id="buyer_date" name="buyer_date">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-md-center" style="padding-top: 15px;padding-right: 10px;">
                                        <div class="col-md-6" style="padding-right: 1px;">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="input-group">
                                                                <span class="input-group-text">Installer name</span>
                                                                <input type="text" class="form-control order-background">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="input-group mb-3">
                                                                <span class="input-group-text">Date</span>
                                                                <input type="text" class="form-control order-background">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row" style="padding-top: 15px;">
                                        <div class="col-md-3 offset-md-9">
                                            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                                <button type="button" class="btn btn-primary btn-lg" onclick="print();">Print</button>
                                            </div>
                                        </div>
                                        <div class="col-md-3 offset-md-9">
                                            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                                <button type="button" class="btn btn-success btn-lg" onclick="NewOrder()" id="button_save"></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="editor"></div>
                </div>
            </div>
        </div>
        
        <!-- MODAL STATUS -->
        <div class="modal fade" role="dialog" tabindex="-1" id="statusModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="text-center modal-title">Order status</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <!-- container div not required -->
                            <ul class="progress-nav progress-function">
                                <li class="active"><a href="#">Profile</a></li>
                                <li><a href="#">Background</a></li>
                                <li><a href="#2">Tech Scan</a></li>
                                <li><a href="#">Opportunities</a></li>
                                <li><a href="#">Opportunities</a></li>
                                <li><a href="#">Opportunities</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/d26731de46.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        let vProducts = [];
        $(document).ready(function() {
            $('#home').removeClass("btn-dark").addClass("btn-outline-dark");
            $('#new-order-section').hide();
            $("#spinnerLoading").hide();
        });
    </script>
    <script src="js/general_functions.js" type="text/javascript"></script>
    <script src="js/ship_address.js" type="text/javascript"></script>
    <script src="js/step_progress_bar.js" type="text/javascript"></script>
</body>

</html>