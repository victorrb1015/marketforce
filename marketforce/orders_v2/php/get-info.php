<?php
ini_set('session.save_path','/home/marketforce/mf_temp');
session_start();
error_reporting(E_ALL);
ini_set("display_errors", 1);

$db_host = 'localhost';
$db_user = 'root';
$db_pass = 'secret';
$db_mf_db = 'marketfo_marketforce';
$mysqli = new mysqli($db_host, $db_user, $db_pass, $_SESSION['org_db_name']);
/* verificar la conexión */
if (mysqli_connect_errno()) {
    printf("Falló la conexión failed: %s\n", $mysqli->connect_error);
    exit();
}
if(isset($_POST['cid'])){
    $cid = $_POST['cid'];
}

$array = [];
switch($_POST['action']){
    case 'getHomeInfo':
        $sql = "SELECT * FROM `orders_v2` WHERE status != 'out_time' ORDER BY ID DESC";
        $result = $mysqli->query($sql);
        
        while($row = $result->fetch_array()){
            $sql2 = "SELECT * FROM `order_customers` WHERE `ID` = '" . $row['customer_id'] . "'";
            $result2 = $mysqli->query($sql2);
            $row2 = $result2->fetch_array();
            
            $data = [
                'status' => $row['status'],
                'inv_num' => $row['invoice_number'],
                'date' => date("m/d/y",strtotime($row['invoce_date'])),
                'customer' => $row2['customer_first_name']." ".$row2['customer_last_name']
                
            ];
            $array['orders'][] = $data;
        }
        break;
    case 'getAllInfo':
        if("832122" == $_SESSION['org_id']){
            $sqlGetAll = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND m_lock = False ORDER BY `item_name` ASC ";
        }else{
            $sqlGetAll = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' ORDER BY `item_name` ASC ";
        }
        $resultGA = $mysqli->query($sqlGetAll);
        while($row = $resultGA->fetch_array()){
            $data_products = [
                'ID' => $row['ID'],
                'zone' => $row['zone'],
                'item_type' => $row['item_type'],
                'p_name' => $row['item_name'],
                'p_info' => $row[''],
            ];
        }
        
        break;
    case 'getItems':
        
        $products = [];
        if("832122" == $_SESSION['org_id']){
            $q = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND m_lock = False ORDER BY `item_name` ASC ";
        }else{
            $q = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' ORDER BY `item_name` ASC ";
        }
        $g = $mysqli->query($q);
        while($r = $g->fetch_array()){

            $lq = "SELECT * FROM `order_special_pricing` WHERE `inactive` != 'Yes' AND `cid` = '" . $cid . "' AND `pid` = '" . $r['ID'] . "' AND `lockout` = 'Yes'";
            $lg = $mysqli->query($lq);

            if($lg->num_rows <= 0 || $cid == '' || $cid == 'undefined'){
                $products = [
                    "ID" => $r['ID'],
                    "barcode" => $r['barcode'],
                    "pname" => $r['item_name'],
                    "pinfo" => $r['item_info'],
                    "pprice" => $r['item_price'],
                ];
                $array['products'][] = $products;
            }
            
            
        }
        break;
    case 'getItem':
        $item = $_POST['ID'];
        $product = [];
        if("832122" == $_SESSION['org_id']){
            $q = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND m_lock = False AND ID = ". $item ."  ORDER BY `item_name` ASC ";
        }else{
            $q = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' ORDER BY `item_name` ASC ";
        }
        $g = $mysqli->query($q);
        while($r = $g->fetch_array()){

            $lq = "SELECT * FROM `order_special_pricing` WHERE `inactive` != 'Yes' AND `cid` = '" . $cid . "' AND `pid` = '" . $r['ID'] . "' AND `lockout` = 'Yes'";
            $lg = $mysqli->query($lq);

            if($lg->num_rows <= 0 || $cid == '' || $cid == 'undefined'){
                $product += [
                    "ID" => $r['ID'],
                    "barcode" => $r['barcode'],
                    "pname" => $r['item_name'],
                    "pinfo" => $r['item_info'],
                    "pprice" => $r['item_price'],
                ];
                $array['product'][] = $product;
            }
            
        }
        break;
    case 'get_customer_info':
        $customer_id = $_POST['customer_id'];
        $sqlGetCompany = "SELECT * FROM `order_customers` WHERE `ID` = '" . $customer_id . "'";
        $resultGCI = $mysqli->query($sqlGetCompany);
        $res_array = $resultGCI->fetch_array();
        $array['company_name'] = $res_array['company_name'];
        $array['company_phone'] = $res_array['customer_phone'];
        $sqlAddres = "SELECT * FROM `order_ship_addresses` WHERE `inactive` != 'Yes' AND `cid` = '" . $customer_id . "'";
        $resultAddress = $mysqli->query($sqlAddres);
        while($sar = $resultAddress->fetch_array()){
            $dataA =[
                'address_id' => $sar['ID'],
                'address' => $sar['address'],
                'city' => $sar['city'],
                'state' => $sar['state'],
                'zip' => $sar['zip'],
                'main' => $sar['main'],
            ];
            $array['addresses'][] = $dataA;
        }

        //GET INFO PRODUCTS
        $sqlProducts = "SELECT * FROM products WHERE inactive!=1 ORDER BY name";
        $rProducts = $mysqli->query($sqlProducts);
        while($rArray = $rProducts->fetch_array()){
            $dataProduts = [
                'id' => $rArray['ID'],
                'name' => $rArray['name'],
            ];
            $array['products'][] = $dataProduts;
        }

        break;
    case 'get_products':
        
        //GET INFO PRODUCTS
        $sqlProducts = "SELECT * FROM products WHERE inactive!=1 ORDER BY name";
        $rProducts = $mysqli->query($sqlProducts);
        while($rArray = $rProducts->fetch_array()){
            $dataProduts = [
                'id' => $rArray['ID'],
                'name' => $rArray['name'],
            ];
            $array['products'][] = $dataProduts;
        }

        break;
    case 'get_dealer_info':
        $dealer_id = $_POST['dealer_id'];
        $sqlGetDealer = "SELECT * FROM `dealers` WHERE ID ='".$dealer_id. "'";
        $resultGD = $mysqli->query($sqlGetDealer);
        $res_array = $resultGD->fetch_array();
        $array['phone'] = $res_array['phone'];
        $array['address'] = $res_array['address'].' '.$res_array['city'].', '.$res_array['state'].' '.$res_array['zip'];
        break;
    case 'get_inv_number':
        $giq = "SELECT max(`invoice_number`) AS `invoice_number` FROM `orders_v2`";
        $gig = $mysqli->query($giq);
        $r = $gig->fetch_assoc();
        $inv_num_arr = array();
        
        if($r['invoice_number'] == NULL){
            
            $invNum = 100;
        }else{
            $invNum = $r['invoice_number'] + 1;
        }
        $array['invNum'] = $invNum;
        break;
    case 'saveOrder':
        
        
        $time = date("H:i:s");               
        $date = date("Y-m-d"); 

        $sqlCustomer = "INSERT INTO order_customers (
            date,
            time,
            customer_first_name,
            customer_last_name,
            customer_phone,
            customer_address,
            customer_city,
            customer_state,
            customer_zip)
            VALUES(
            '".$date."',
            '".$time."',
            '".$_POST['customer_first_name']."',
            '".$_POST['customer_last_name']."',
            '".$_POST['customer_phone']."',
            '".$_POST['customer_address']."',
            '".$_POST['customer_city']."',
            '".$_POST['customer_state']."',
            '".$_POST['customer_zip']."')";
        if(!$mysqli->query($sqlCustomer)){
            $array['error'] = true;
            $array['msg'] = "error saving customer data, ";
            $array['type'] = $mysqli->error . ": consulta: ".$sqlCustomer;
        }else{
            $customerId = $mysqli->insert_id;

            $sqlAddress = "INSERT INTO order_ship_addresses (
                date,
                time,
                cid,
                address, 
                city, 
                state, 
                zip, 
                rep_id, 
                rep_name,
                inactive
            )values(
                '".$date."',
                '".$time."',
                '".$customerId."',
                '".$_POST['customer_address']."',
                '".$_POST['customer_city']."',
                '".$_POST['customer_state']."',
                '".$_POST['customer_zip']."',
                '".$_SESSION['org_id']."',
                '".$_SESSION['full_name']."',
                'No')";
            if(!$mysqli->query($sqlAddress)){
                $array['error'] = true;
                $array['msg'] = "error saving ship address, ";
                $array['type'] = $mysqli->error . ": consulta: ".$sqlAddress;
            }else{
                $shipAddressId = $mysqli->insert_id;

                $sqlOrder = "INSERT INTO orders_v2 (
                    invoice_number,
                    invoice_date,
                    customer_id,
                    ship_addres_id,
                    dealer_id,
                    description,
                    width,
                    roof_length,
                    frame_length,
                    leg_height,
                    gauge,
                    carport_price,
                    color_top,
                    color_sides,
                    color_ends,
                    color_trim,
                    carport_type,
                    carport_type_price,
                    additional_leg_height,
                    additional_leg_height_desc,
                    additional_leg_height_price,
                    both_sides,
                    side_quantity,
                    both_sides_price,
                    ends,
                    end_quantity,
                    ends_price,
                    gable,
                    gable_quantity,
                    gable_price,
                    roll,
                    rollUpColor,
                    roll_desc,
                    roll_price,
                    wall,
                    wall_desc,
                    wall_price,
                    window,
                    window_desc,
                    window_price,
                    anchor_system_decline,
                    anchors,
                    anchor_system_quantity,
                    anchor_system_price,
                    total_sale,
                    tax,
                    tax_exempt,
                    total,
                    tax_contractor,
                    deposit,
                    balance_due,
                    special_instruction,
                    surface_level,
                    ready_installation,
                    electricity_available,
                    instalation_surface,
                    payment,
                    other_payment
                    
                ) 
                VALUES (
                    '".$_POST['number_invoice']."',
                    '".$_POST['invoice_date']."',
                    ".$customerId.",
                    ".$shipAddressId.",
                    ".$_POST['dealer_id'].",
                    '".$_POST['description']."',
                    ".$_POST['width'].",
                    ".$_POST['roof_length'].",
                    ".$_POST['frame_length'].",
                    ".$_POST['leg_height'].",
                    '".$_POST['gauge']."',
                    ".$_POST['carport_price'].",
                    '".$_POST['color_top']."',
                    '".$_POST['color_sides']."',
                    '".$_POST['color_ends']."',
                    '".$_POST['color_trim']."',
                    '".$_POST['carport_type']."',
                    ".$_POST['carport_type_price'].",
                    '".$_POST['ad_leg_height']."',
                    '".$_POST['ad_leg_height_desc']."',
                    ".$_POST['ad_leg_height_price'].",
                    '".$_POST['bothSidesOptions']."',
                    ".$_POST['side_quantity'].",
                    ".$_POST['priceSide'].",
                    '".$_POST['EndOptions_both_end']."',
                    ".$_POST['end_quantity'].",
                    ".$_POST['priceEnd'].",
                    '".$_POST['gableOptions']."',
                    ".$_POST['gable_quantity'].",
                    ".$_POST['priceGable'].",
                    '".$_POST['rollUp']."',
                    '".$_POST['rollUpColor']."',
                    ".$_POST['sizeRoll'].",
                    ".$_POST['priceRoll'].",
                    '".$_POST['wallDoor']."',
                    ".$_POST['sizeWall'].",
                    ".$_POST['priceWall'].",
                    '".$_POST['window']."',
                    ".$_POST['sizeWindow'].",
                    ".$_POST['priceWindow'].",
                    '".$_POST['anchor_system_decline']."',
                    '".$_POST['ancla']."',
                    ".$_POST['anchor_system_quantity'].",
                    ".$_POST['anchor_system_price'].",
                    ".$_POST['total_sale'].",
                    ".$_POST['tax'].",
                    ".$_POST['tax_exempt'].",
                    ".$_POST['total'].",
                    ".$_POST['non_tax_exempt'].",
                    ".$_POST['deposit'].",
                    ".$_POST['balance_due'].",
                    '".$_POST['special_instructions']."',
                    '".$_POST['surface_level']."',
                    '".$_POST['ready_istallation']."',
                    '".$_POST['electricity_available']."',
                    '".$_POST['installation_surface']."',
                    '".$_POST['payment']."',
                    '".$_POST['other_payment']."'
                    )";
                
                $rOrder = $mysqli->query($sqlOrder);
                $orderID = $mysqli->insert_id;
                if(!$rOrder){
                    $array['error'] = true;
                    $array['msg'] = "error saving order, ";
                    $array['type'] = $mysqli->error . ": consulta: ".$sqlOrder;
                    
                }else{
                    $sqlNote = "INSERT INTO notes_v2 (order_id, note) VALUES (".$orderID.",'".$_POST['note']."')";
                    $mysqli->query($sqlNote);

                    if(!$_POST['vProducts'] == ""){
                        $sqlItems = "INSERT INTO order_details_v2 (
                            id_order_v2,
                            item,
                            description,
                            amount,
                            price
                        )VALUES";
                        foreach ($_POST['vProducts'] as $key => $value) {
                            $sqlItems .= "(";
                            $sqlItems .= $orderID.","; 
                            $sqlItems .= "'".$_POST['vProducts'][$key]['item']."',";
                            $sqlItems .= "'".$_POST['vProducts'][$key]['description']."',";
                            $sqlItems .= $_POST['vProducts'][$key]['amount'].",";
                            $sqlItems .= $_POST['vProducts'][$key]['price'];
                            $sqlItems .="),";
                            
                        }
                        $sqlItems = substr($sqlItems, 0, -1);

                        
                        $rOrderItems = $mysqli->query($sqlItems);
                        if(!$rOrderItems){
                            $array['error'] = true;
                            $array['msg'] = "error saving items, ". $mysqli->error;
                            $array['type'] = $mysqli->error . ": consulta: ".$sqlItems;
                            
                        }
                    }
                    
                    
                }
                $sqlAddres = "SELECT * FROM `order_ship_addresses` WHERE ID =" . $shipAddressId;
                $resultAddress = $mysqli->query($sqlAddres);
                $sar = $resultAddress->fetch_array();
                $addressURL = $sar['address']. ' '.$sar['city'].', '.$sar['state'].' '.$sar['zip'];
                $Url = "https://maps.googleapis.com/maps/api/geocode/json?";
                $datos = [
                    "address" => $addressURL,
                    "key" => "AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg"
                ];
                $datos = http_build_query($datos);
                $Url .= $datos;
                $aContext = array("http" =>
                    array(
                        "method" => "GET",
                        "header" =>
                            'Accept: application/json' . "\r\n" .
                            'Content-Type: application/json' . "\r\n",
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    )
                );
                $cxContext = stream_context_create($aContext);
                $sFile = file_get_contents($Url, false, $cxContext);
                $jData = json_decode(json_encode($sFile), true);
                $jdata = json_decode($jData);
                if (strlen($jdata->status) > 3) {
                    $lat = "Error";
                    $lng = "Error";
                } else {
                    $lat = $jdata->results[0]->geometry->location->lat;
                    $lng = $jdata->results[0]->geometry->location->lng;
                }
                if (strlen($lat) > 0 && strlen($lng) > 0) {
                    $date = date("Y-m-d");
                    $insert = "INSERT INTO `portal_relationship` (`orderID`, `lat`, `lng`, `date`) VALUES ('" . $orderID . "', '" . $lat . "', '" . $lng . "', '" . $date . "')";
                    
                    if(!$mysqli->query($insert)){
                        $array['error'] = true;
                        $array['msg'] = "error saving portal_relationship, ". $mysqli->error;
                        $array['type'] = $mysqli->error . ": consulta: ".$insert;
                    }

                    
                }
                $insertSalesPortal = "INSERT INTO salesPortalLinked (
                    `orderID`,
                    `totalSale`,
                    `customer`,
                    `orderStatus`,
                    `buildingAddress`,
                    `buildingCity`,
                    `buildingState`,
                    `buildingZipCode`,
                    `invoiceDate`,
                    `archieved`,
                    `inactive`
                ) VALUES 
                (
                    '".$orderID."',
                    '".$_POST['balance_due']."',
                    '".$customerId."',
                    'In Shop',
                    '".$sar['address']."',
                    '".$sar['city']."',
                    '".$sar['state']."',
                    '".$sar['zip']."',
                    '".$_POST['invoice_date']."',
                    'No',
                    'No'
                )";

                $iq = "INSERT INTO `scheduler_sync` 
                (
                `date`,
                `time`,
                `total_orders`,
                `message`
                )
                VALUES
                (
                CURRENT_DATE,
                CURRENT_TIME,
                '1',
                'Scheduler Syncing Successfull'
                )";


                if(!$mysqli->query($iq)){
                    $array['error'] = true;
                    $array['msg'] = "error saving scheduler_sync, ";
                    $array['type'] = $mysqli->error . ": consulta: ".$iq;
                    
                }


                if(!$mysqli->query($insertSalesPortal)){
                    $array['error'] = true;
                    $array['msg'] = "error saving salesPortalLinked, ";
                    $array['type'] = $mysqli->error . ": consulta: ".$insertSalesPortal;
                    
                }

                if(!isset($array['error'])){
                    $array['success'] = true;
                    $array['msg'] = "Order Saved Successfully";
                }
            }
        }
        break;
    case 'addShipAddres':
        //Load Variables...
        $cid = $_POST['cid'];
        $address = $_POST['saddress'];
        $city = $_POST['scity'];
        $state = $_POST['sstate'];
        $zip = $_POST['szip'];


        //Add Address...
        $q = "INSERT INTO `order_ship_addresses`
                (
                `date`,
                `time`,
                `cid`,
                `address`,
                `city`,
                `state`,
                `zip`,
                `rep_id`,
                `rep_name`,
                `main`,
                `inactive`
                )
                VALUES
                (
                CURRENT_DATE,
                CURRENT_TIME,
                '" . $cid . "',
                '" . $address . "',
                '" . $city . "',
                '" . $state . "',
                '" . $zip . "',
                '" . $_SESSION['user_id'] . "',
                '" . $_SESSION['full_name'] . "',
                '',
                'No'
                )";
        
        if(!$mysqli->query($q)){
            $array['error'] = true;
            $array['msg'] = "error saving Ship Address, ";
            $array['type'] = $mysqli->error . ": consulta: ".$q;
            
        }

        $iq = "SELECT * FROM `order_ship_addresses` ORDER BY `ID` DESC LIMIT 1";
        if(!$ig = $mysqli->query($iq)){
            $array['error'] = true;
            $array['msg'] = "error Consulting Ship Address, ";
            $array['type'] = $mysqli->error . ": consulta: ".$iq;
        }else{
            $array['error'] = false;
            $ir = $ig->fetch_array();
            $array['ship_address_id'] = $ir['ID'];
            $array['msg'] = "Save Ship Address Successfuly ";
        }
        
        break;
    case 'getShipAddress':
        //Load Variables...
        $cid = $_POST['cid'];

        //Get Company Name...
        $cnq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $cid . "'";
        if(!$cng = $mysqli->query($cnq)){
            $array['error'] = true;
            $array['msg'] = "error Consulting Custome ";
            $array['type'] = $mysqli->error . ": consulta: ".$cnq;
        }else{
            $array['error'] = false;
            $cnr = $cng->fetch_array();
            $cname = $cnr['company_name'];
        }
        
        //Get Shipping Addresses...
        $saq = "SELECT * FROM `order_ship_addresses` WHERE `inactive` != 'Yes' AND `cid` = '" . $cid . "'";
        if(!$sag = $mysqli->query($saq)){
            $array['error'] = true;
            $array['msg'] = "error Consulting Ship Address ";
            $array['type'] = $mysqli->error . ": consulta: ".$saq;
        }else{
            $array['error'] = false;
            $i = 0;
            $a = [];
            if($sag->num_rows > 0){
                while($sar = $sag->fetch_array()){
                    $a[$i]['address_id'] = $sar['ID'];
                    $a[$i]['address'] = $sar['address'];
                    $a[$i]['city'] = $sar['city'];
                    $a[$i]['state'] = $sar['state'];
                    $a[$i]['zip'] = $sar['zip'];
                    $a[$i]['main'] = $sar['main'];
                    $i++;
                }
                $array['rows'] = true;
            }
            
        }
        
        $array['addresses'] = $a;
        $array['cname'] = $cname;
        
        break;
}

header('Content-type: application/json; charset=utf-8');
echo json_encode($array, JSON_FORCE_OBJECT);
exit();