<?php
include 'connection.php';

//Load Variables...
$wid = $_GET['wid'];
$rep_name = $_GET['rep_name'];
$rep_id = $_GET['rep_id'];
$wnote = mysqli_real_escape_string($conn, $_GET['wnote']);

//Get User Info
$wq = "SELECT * FROM `waivers` WHERE `ID` = '" . $wid . "'";
$wg = mysqli_query($conn, $wq) or die($conn->error);
$wr = mysqli_fetch_array($wg);


//Insert Note...
	$nq = "INSERT INTO `waiver_notes`
				(
				`date`,
				`time`,
				`rep_id`,
				`rep_name`,
				`cname`,
				`waiver_id`,
				`note`
				)
				VALUES
				(
				CURRENT_DATE,
				CURRENT_TIME,
				'" . $rep_id . "',
				'" . $rep_name . "',
				'" . $wr['cname'] . "',
				'" . $wid . "',
				'" . $wnote . "'
				)";
	mysqli_query($conn, $nq) or die($conn->error);

//Response...
echo 'Your note has been recorded!';

?>