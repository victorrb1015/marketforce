function send_closing(rid){
  var check = confirm('Are you sure you want to send the closing email?');
  if(check === false){
    //alert('Stopped');
    return;
  }
		if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","repairs/php/send-closing-email.php?rid="+rid,true);
  xmlhttp.send();
}