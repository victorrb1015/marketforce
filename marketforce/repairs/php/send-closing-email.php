<?php
include '../../php/connection.php';

//Load Variables...
$rid = $_GET['rid'];

//Get Repairs info...
$rq = "SELECT * FROM `repairs` WHERE `ID` = '" . $rid . "'";
$rg = mysqli_query($conn, $rq) or die($conn->error);
$rr = mysqli_fetch_array($rg);


//Send Email...
include '../../php/phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include '../../php/phpmailsettings.php';
$mail->CharSet = 'UTF-8';
switch ($_SESSION['org_id']) {
  case "162534":
    $mail->Subject = 'Northedge Steel Repair';
    $mail->setFrom('repairs@northedgesteel.us', 'Northedge Steel');
    $mail->addReplyTo('repairs@northedgesteel.us');
    break;
  default:
  $mail->Subject = 'All Steel Carports Repair';
  $mail->setFrom('repairs@allsteelcarports.com', 'All Steel Carports');
  $mail->addReplyTo('repairs@allsteelcarports.com');
}
$mail->addAddress($rr['cemail']);
$mail->addBCC('archive@ignition-innovations.com');
//$mail->addBCC('michael@allsteelcarports.com');
$CUSTOMER_NAME = $rr['cname'];
$INVOICE_NUMBER = $rr['inv'];
include '../emails/templates/closing-repair-email.php';
$mail->Body = $etemp;
if($rr['cemail'] != ''){
if($mail->send()){
  
  //Update Repairs DB...
$urq = "UPDATE `repairs` SET `closing_email_sent` = CURRENT_DATE WHERE `ID` = '" . $rid . "'";
mysqli_query($conn, $urq) or die($conn->error);

//Insert Note....
$nq = "INSERT INTO `repair_notes`
          (
          `date`,
          `time`,
          `rep_id`,
          `rep_name`,
          `cname`,
          `cid`,
          `note`,
          `inactive`
          )
          VALUES
          (
          CURRENT_DATE,
          CURRENT_TIME,
          '" . $_SESSION['user_id'] . "',
          '" . $_SESSION['full_name'] . "',
          '" . $rr['cname'] . "',
          '" . $rid . "',
          'Closing Email Sent to " . $rr['cemail'] . ".',
          'No'
          )";
mysqli_query($conn, $nq) or die($conn->error);

echo 'Closing Email Sent To: ' . $rr['cemail'];
  
}else{
  echo 'ERROR: ' . $mail->ErrorInfo;
}
  
}else{
  echo 'Unable To Send: No Email Address Provided.';
}



?>