<div class="modal fade" role="dialog" tabindex="-1" id="displayRepair">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Display Repair Order Form For: <span id="form_dname" style="color:blue;font-weight:bold;"></span></h4>
                </div>
                <div class="modal-body">
                  <form action="repairs/php/add-display-repair.php" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <p>Dealer Name</p><input type="text" id="drdname" style="width:100%;" name="drdname" class="form-control" disabled></div>
                        <div class="col-md-6">
                            <p>Today's Date</p>
                            <p><strong>7/27/2018</strong></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p>Address</p><input type="text" id="draddress" style="width:100%;" name="draddress" class="form-control" required></div>
                        <div class="col-md-6">
                            <p>City</p><input type="text" id="drcity" style="width:100%;" name="drcity" class="form-control" required></div>
                        <div class="col-md-6">
                            <p>State</p>
                            <select id="drstate" style="width:100%;" name="drstate" class="form-control" required>
                              <option value="">State</option>
                              <?
                              $stq = "SELECT * FROM `states` ORDER BY `state` ASC";
                              $stg = mysqli_query($conn, $stq) or die($conn->error);
                              while($str = mysqli_fetch_array($stg)){
                                echo '<option value="' . $str['state'] . '">' . $str['state'] . '</option>';
                              }
                              ?>
                            </select>
                        </div>
                        <div
                            class="col-md-6">
                            <p>Zip Code</p><input type="text" id="drzip" style="width:100%;" name="drzip" class="form-control" required></div>
                </div>
                <div class="row">
                    <div class="col-md-12"><br>
                      <p><strong>Please list the <mark>Building Dimensions</mark>, <mark>Roof Color</mark>, <mark>Trim Color</mark>, and the <mark>cause of the damage</mark>:</strong></p><textarea id="dealer_statement" style="width:100%;" name="dealer_statement" class="form-control" required></textarea></div>
                    <div class="col-md-12"><br>
                        <p><strong>If a police report was filed regarding the damage to this building, please indicate here and include the report number:</strong><br></p><textarea id="drdetails" style="width:100%;" name="drdetails" class="form-control"></textarea></div>
                </div>
                <div class="row">
                    <div class="col-md-12"><br></div>
                    <div class="col-md-6">
                        <p><strong>Add Files:&nbsp;</strong></p>
                        <p>Image 1:</p><input type="file" id="img_1" name="img_1" class="form-control"><br>
                        <p>Image 2:</p><input type="file" id="img_2" name="img_2" class="form-control"><br>
                        <p>Image 3:</p><input type="file" id="img_3" name="img_3" class="form-control"></div>
                    <div class="col-md-6">
                        <p style="color:rgb(251,250,250);">Add Files:&nbsp;</p>
                        <p>Image 4:</p><input type="file" id="img_4" name="img_4" class="form-control"><br>
                        <p>Image 5:</p><input type="file" id="img_5" name="img_5" class="form-control"></div>
                </div>
                    <input type="hidden" id="did" name="did" value="" />
                    <input type="hidden" id="dname" name="dname" value="" />
                    <input type="hidden" id="rep_id" name="rep_id" value="<? echo $_SESSION['user_id']; ?>" />
                    <input type="hidden" id="rep_name" name="rep_name" value="<? echo $_SESSION['full_name']; ?>" />
            </div>
            <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Close</button><button class="btn btn-success" type="submit">Save</button></div>
           </form>
        </div>
    </div>
 </div>