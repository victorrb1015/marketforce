<?php 
include 'php/connection.php';

//Load Variables 
$cid = $_GET['id'];
$cname = mysqli_real_escape_string($conn, $_GET['cname']);
$rid = $_GET['rid'];
$rname = mysqli_real_escape_string($conn, $_GET['rname']);
$note = mysqli_real_escape_string($conn, $_GET['note']);


//Add Note to the repair
$q = "INSERT INTO `repair_notes`
      (
      `date`,
      `time`,
      `rep_id`,
      `rep_name`,
      `cname`,
      `cid`,
      `note`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $rid . "',
      '" . $rname . "',
      '" . $cname . "',
      '" . $cid . "',
      '" . $note . "',
      'No'
      )";

mysqli_query($conn, $q) or die($conn->error);

echo 'Your note was added to the repair!';

?>