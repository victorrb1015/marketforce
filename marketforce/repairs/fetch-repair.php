<?php
include '../php/connection.php';

//Load Variables
$id = $_GET['id'];


$q = "SELECT * FROM `repairs` WHERE `ID` = '" . $id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
$r = str_replace('"', '&quot;', $r);
$r = str_replace("'", "&apos;", $r);

if($r['sale_date'] == '' || $r['sale_date'] == '1969-12-31'){
  $sale_date = '';
}else{
  $sale_date = date("m/d/Y", strtotime($r['sale_date']));
}
if($r['install_date'] == '' || $r['install_date'] == '1969-12-31'){
  $install_date = '';
}else{
  $install_date = date("m/d/Y", strtotime($r['install_date']));
}

$response = '{
              "ID" : "' . $r['ID'] . '",
              "cname" : "' . $r['cname'] . '",
              "today_date" : "' . date("m/d/Y", strtotime($r['today_date'])) . '",
              "inv" : "' . $r['inv'] . '",
              "dname" : "' . $r['dname'] . '",
              "sale_date" : "' . $sale_date . '",
              "contractor_name" : "' . $r['contractor_name'] . '",
              "install_date" : "' . $install_date . '",
              "address" : "' . $r['address'] . '",
              "city" : "' . $r['city'] . '",
              "state" : "' . $r['state'] . '",
              "zip" : "' . $r['zip'] . '",
              "building_size_type" : "' . $r['building_size_type'] . '",
              "cust_statement" : "' . str_replace("\r\n", " ", $r['cust_statement']) . '",
              "dealer_statement" : "' . str_replace("\r\n", " ", $r['dealer_statement']) . '",
              "contractor_statement" : "' . str_replace("\r\n", " ", $r['contractor_statement']) . '",
              "details" : "' . str_replace("\r\n", " ", $r['details']) . '",
              "cphone" : "' . $r['cphone'] . '",
              "cemail" : "' . $r['cemail'] . '",
              "task1" : "' . mysqli_real_escape_string($conn, $r['task1']) . '",
              "task2" : "' . mysqli_real_escape_string($conn, $r['task2']) . '",
              "task3" : "' . mysqli_real_escape_string($conn, $r['task3']) . '",
              "task4" : "' . mysqli_real_escape_string($conn, $r['task4']) . '",
              "task5" : "' . mysqli_real_escape_string($conn, $r['task5']) . '",
              "task6" : "' . mysqli_real_escape_string($conn, $r['task6']) . '",
              "allsteel_cost" : "' . $r['allsteel_cost'] . '",
              "dealer_cost" : "' . $r['dealer_cost'] . '",
              "contractor_cost" : "' . $r['contractor_cost'] . '",
              "customer_cost" : "' . $r['customer_cost'] . '",
              "customer_balance" : "' . $r['customer_balance'] . '",
              "total_cost" : "' . $r['total_cost'] . '",
              "contractor" : "' . $r['contractor'] . '",
              "did" : "' . $r['did'] . '",
              "img1" : "' . $r['img1'] . '",
              "img2" : "' . $r['img2'] . '",
              "img3" : "' . $r['img3'] . '",
              "img4" : "' . $r['img4'] . '",
              "img5" : "' . $r['img5'] . '",
              "urgent" : "' . $r['urgent'] . '",
              "submitted_by_name" : "' . $r['submitted_by_name'] . '"
              }';
              

echo $response;

?>            