<?php
session_start();
include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--These are th script links -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 		<!-- Burton Solution Scripts -->
		<script src="js/new/signup.js"></script>
  
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
		<!--JS Date Picker-->
		  <script>
  $( document ).ready( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>

<style>
  .sign-up{
    list-style-type: none;
  }
  .sign-up li{
    display: inline-block;
    float: left;
    padding: 5px;
  }
  #sign-up{
    text-align: center;
    width: 40%;
    margin: auto;
  }
  
  </style>
  
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="img/logo-allsteel.png" ></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="signup.php"><i class="fa fa-fw fa-key"></i> Forgot Password</a>
                    </li>
                    <li>
												<a href="https://burtonsolution.on.spiceworks.com/portal/tickets" target="_blank"><i class="fa fa-bug"></i> Report An Issue</a>
									</li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="width: 175px;" ><small> Forgot Password</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-key"></i> Forgot Password
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

    <!-- This is where the content for the page goes! -->
             
                <div id="sign-up">
                   <form action="php/forgot-pass.php" method="post">
                   <div class="col-lg-12">
                     <p>
                       <label for="org">Please Select Your Organization</label>
                       <select id="org" name="org" required>
                         <option value="">Select An Organization</option>
                         <?php
                          $oq = "SELECT * FROM `organizations` WHERE `inactive` != 'Yes' ORDER BY `org_name` DESC";
                          $og = mysqli_query($mf_conn, $oq) or die($conn->error);
                          while($or = mysqli_fetch_array($og)){
                            echo '<option value="' . $or['org_id'] . '">' . $or['org_name'] . '</option>';
                          }
                          ?>
                       </select>
                     </p>
                     <p>
                       <label for="email">Please Enter The Email Address For Your Account</label>
                       <input type="email" class="form-controls" id="email" name="email" placeholder="Email Address" autofocus required/>
                     </p>
										 <p style="color:red; text-align:center;"><?php echo $_GET['response']; ?></p>
										 <p style="text-align:center;">
										 <button type="submit">Submit</button>
										 </p>
                   </div>
                  </form>
                          
              </div>
               
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
	


 <?php include 'footer.html'; ?>
</body>

</html>








