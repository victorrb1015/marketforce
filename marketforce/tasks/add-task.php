<?php
include '../php/connection.php';

//Load Variables
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];
$task = mysqli_real_escape_string($conn, $_GET['task']);
$auser_id = $_GET['auser_id'];
  //Get AUser Name
  $nq = "SELECT * FROM `users` WHERE `ID` = '" . $auser_id . "'";
  $ng = mysqli_query($conn, $nq) or die($conn->error);
  $nr = mysqli_fetch_array($ng);
$auser_name = $nr['fname'] . ' ' . $nr['lname'];
$dl = date("Y-m-d", strtotime($_GET['dl']));


//INSERT Task...
$q = "INSERT INTO `tasks`
      (
      `date`,
      `from_rep_id`,
      `from_rep_name`,
      `to_rep_id`,
      `to_rep_name`,
      `task`,
      `deadline`,
      `status`,
      `inactive`
      )
      VALUES
      (
      NOW() + INTERVAL 1 HOUR,
      '" . $rep_id . "',
      '" . $rep_name . "',
      '" . $auser_id . "',
      '" . $auser_name . "',
      '" . $task . "',
      '" . $dl . "',
      'Pending',
      'No'
      )";
mysqli_query($conn, $q) or die($conn->error);

echo 'Your task has been submitted successfully!';


//From Rep Info
$frq = "SELECT * FROM `users` WHERE `ID` = '" . $rep_id . "'";
$frg = mysqli_query($conn, $frq) or die($conn->error);
$frr = mysqli_fetch_array($frg);

//To Rep Info
$trq = "SELECT * FROM `users` WHERE `ID` = '" . $auser_id . "'";
$trg = mysqli_query($conn, $trq) or die($conn->error);
$trr = mysqli_fetch_array($trg);

if($rep_id != $auser_id && $rep_id != '999'){
    //Email Parameter Setup...
    include '../php/phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    include '../php/phpmailsettings.php';
    $mail->setFrom('no-reply@allsteelcarports.com','Market Force');
    $mail->addReplyTo($frr['email']);
    $mail->addAddress($trr['email']);
    $mail->addBCC('michael@burtonsolution.tech');
    $mail->Subject = 'Task Alert!';
    $mail->Body = '<html>
                    <head>
                      <style>
                        .main{
                          text-align: center;
                        }
                        img{
                          width: 80%;
                        }
                      </style>
                    </head>
                    <body>
                    <div class="main">
                      <img src="http://marketforceapp.com/marketforce/img/Market%20Force%20Logo%202.png" />
                      <h2>Notification From Market Force</h2>
                      <p>
                      ' . $frr['fname'] . ' ' . $frr['lname'] . ' assigned you a new task in Market Force:
                      </p>
                      <p>
                      <b>' . $task . '</b>
                      </p>
                      <br><br>
                      <p>Thank You,<br><strong>Market Force Team<strong></p>
                    </div>
                    </body>
                  </html>';
$mail->send();

//SMS Alert
$mail->ClearAddresses();
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
	//Remove special characters from phone number
	$phone = str_replace("-","",$trr['phone']);
	$phone = str_replace("(","",$phone);
	$phone = str_replace(")","",$phone);
	$phone = str_replace(" ","",$phone);
$pcq = "SELECT * FROM `cell_providers` WHERE `inactive` != 'Yes'";
$pcg = mysqli_query($conn, $pcq) or die($conn->error);
while($pcr = mysqli_fetch_array($pcg)){
  $mail->addAddress($phone . '@' . $pcr['extension']);
}
$mail->addBCC('michael@burtonsolution.tech');
$mail->Subject = 'Task Alert!';
$mail->Body = $frr['fname'] . ' ' . $frr['lname'] . ' assigned you a new task in Market Force!';
if($phone != ''){
$mail->send();
	}
  
}

?>