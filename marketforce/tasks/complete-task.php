<?php
include '../php/connection.php';

//Load Variables
$tid = $_GET['tid'];
$rep_name = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];


//Mark Task As Complete...
$q = "UPDATE `tasks` SET `status` = 'Completed', `completed_date` = NOW() + INTERVAL 1 HOUR WHERE `ID` = '" . $tid . "'";
mysqli_query($conn, $q) or die($conn->error);

echo 'Your task has been marked as completed!';

//Send Notification of message...
$mq = "SELECT * FROM `tasks` WHERE `ID` = '" . $tid . "'";
$mg = mysqli_query($conn, $mq) or die($conn->error);
$mr = mysqli_fetch_array($mg);

//From Rep Info
$frq = "SELECT * FROM `users` WHERE `ID` = '" . $mr['from_rep_id'] . "'";
$frg = mysqli_query($conn, $frq) or die($conn->error);
$frr = mysqli_fetch_array($frg);

//To Rep Info
$trq = "SELECT * FROM `users` WHERE `ID` = '" . $mr['to_rep_id'] . "'";
$trg = mysqli_query($conn, $trq) or die($conn->error);
$trr = mysqli_fetch_array($trg);

if($mr['from_rep_id'] != $mr['to_rep_id'] && $mr['from_rep_id'] != '999'){
    //Email Parameter Setup...
    include '../php/phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    include '../php/phpmailsettings.php';
    $mail->setFrom('no-reply@allsteelcarports.com','Market Force');
    $mail->addReplyTo($trr['email']);
    $mail->addAddress($frr['email']);
    $mail->addBCC('michael@burtonsolution.tech');
    $mail->Subject = 'Task Alert!';
    $mail->Body = '<html>
                    <head>
                      <style>
                        .main{
                          text-align: center;
                        }
                        img{
                          width: 80%;
                        }
                      </style>
                    </head>
                    <body>
                    <div class="main">
                      <img src="http://marketforceapp.com/marketforce/img/Market%20Force%20Logo%202.png" />
                      <h2>Notification From Market Force</h2>
                      <p>
                      ' . $trr['fname'] . ' ' . $trr['lname'] . ' has completed the following task in Market Force:
                      </p>
                      <p>
                      <b>' . mysqli_real_escape_string($conn, $mr['task']) . '</b>
                      </p>
                      <br><br>
                      <p>Thank You,<br><strong>Market Force Team<strong></p>
                    </div>
                    </body>
                  </html>';
$mail->send();

//SMS Alert
$mail->ClearAddresses();
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
	//Remove special characters from phone number
	$phone = str_replace("-","",$frr['phone']);
	$phone = str_replace("(","",$phone);
	$phone = str_replace(")","",$phone);
	$phone = str_replace(" ","",$phone);
$pcq = "SELECT * FROM `cell_providers` WHERE `inactive` != 'Yes'";
$pcg = mysqli_query($conn, $pcq) or die($conn->error);
while($pcr = mysqli_fetch_array($pcg)){
  $mail->addAddress($phone . '@' . $pcr['extension']);
}
$mail->addBCC('michael@burtonsolution.tech');
$mail->Subject = 'Task Alert!';
$mail->Body = $trr['fname'] . ' ' . $trr['lname'] . ' has completed a task in Market Force!';
	if($phone != ''){
$mail->send();
	}
  
}

?>