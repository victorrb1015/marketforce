<?php
include 'connection.php';

//Load Variables
$id = $_GET['id'];
$mode = $_GET['mode'];
$note = mysqli_real_escape_string($conn, $_GET['note']);

$q = "SELECT * FROM `check_requests` WHERE `pid` = '" . $id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
$cid = $r['cust_id'];
$cat = $r['category'];
$amount = $r['amount'];

$nq = "SELECT * FROM `collections` WHERE `ID` = '" . $cid . "'";
$ng = mysqli_query($conn, $nq) or die($conn->error);
$nr = mysqli_fetch_array($ng);
$cname = $nr['name'];

if($mode == 'approve'){
  $uq = "UPDATE `check_requests` SET `status` = 'Completed' WHERE `pid` = '" . $id . "'";
  mysqli_query($conn, $uq) or die ($conn->error);
  echo 'Thank You For Confirming This Payment!';
}


if($mode == 'decline'){
  //Make the payment record inactive...
  $ccq = "UPDATE `collections_calls` SET `inactive` = 'Yes' WHERE `ID` = '" . $id . "'";
  //mysqli_query($conn, $ccq) or die($conn->error);
  
  //Add the payment back to the debt amount in the database and ensure the customer is listed as collections...
  $cuq = "UPDATE `collections` 
          SET `" . $cat . "` = `" . $cat . "` + '" . $amount . "', 
          `paid` = 'No', 
          `collection` = 'Yes'
          WHERE 
          `ID` = '" . $cid . "'";
  mysqli_query($conn, $cuq) or die($conn->error);
  
  
  $niq = "INSERT INTO `collections_calls`
        (`date`,`customer_id`,`name`,`rep_id`,`rep_name`,`type`,`note`,`inactive`)
        VALUES
        (
        NOW() + INTERVAL 1 HOUR,
        '" . $cid . "',
        '" . $cname . "',
        '999',
        'Accounting',
        'Note',
        'The Electronic Payment from " . $cname . " in the amount of $" . $amount . " was marked as <b>NOT RECEIVED</b> by accounting.<br>Notes: " . $note . "',
        'No'
        )";
  
  mysqli_query($conn, $niq) or die($conn->error);
  
  $crq = "UPDATE `check_requests` SET `status` = 'Completed' WHERE `pid` = '" . $id . "'";
  mysqli_query($conn, $crq) or die($conn->error);
  
  echo 'This payment has been reversed and the customer is in collections!';
}



?>