<?php
include 'php/connection.php';

//Load Variables
$letter = $_GET['letter'];//Signifies which letter to be sent
$id = $_GET['id'];//Collection ID
$view = $_GET['view'];
$rep_id = $_GET['rep_id'];
$rep_name = mysqli_real_escape_string($conn, $_GET['rep_name']);

//Get collections information...
$iq = "SELECT * FROM `collections` WHERE `ID` = '" . $id . "'";
$ig = mysqli_query($conn, $iq) or die($conn->error);
$ir = mysqli_fetch_array($ig);

//Get Dealer Information
$diq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $ir['did'] . "'";
$dig = mysqli_query($conn, $diq) or die($conn->error);
$dir = mysqli_fetch_array($dig);
$demail = $dir['email'];
//Remove special characters from phone number
	$dealer_cell = str_replace("-","",$dir['dcphone']);
	$dealer_cell = str_replace("(","",$dealer_cell);
	$dealer_cell = str_replace(")","",$dealer_cell);
	$dealer_cell = str_replace(" ","",$dealer_cell);
//Setup Email Template Variables
$dname = $ir['dname'];
$cname = $ir['name'];
$inv = $ir['inv#'];
$crate = $ir['c_rate'];
if($ir['p_date'] == '' || $ir['p_date'] == '1969-12-31' || $ir['p_date'] == '0000-00-00'){
	$sdate = '';
}else{
	$sdate = date("m/d/Y",strtotime($ir['p_date']));
}
$sale_price = number_format($ir['sale_price'],2);
$tamount = number_format($ir['tamount'],2);
$subtotal = number_format(($ir['sale_price'] + $ir['tamount']),2);
$uf_dealer_owes = ($ir['sale_price'] + $ir['tamount']) - $ir['sale_price'] * ($crate / 100);
$owed_amount = number_format($ir['sale_collected_amount'] - ($ir['sale_price'] * ($crate / 100)),2);
$customer_owes = number_format(($ir['sale_price'] + $ir['tamount']) - ($ir['sale_price'] * ($crate / 100)) - $uf_dealer_owes,2);
$camount = number_format($ir['sale_price'] * ($crate / 100),2);
$collected_amount = number_format($ir['sale_collected_amount'],2);

$lost = ($ir['sale_collected_amount'] - $ir['sale_price'] * ($crate / 100)) * 1000;
$lost = number_format($lost,2);
//$owed_amount = number_format($cowed_amount,2);
//Setup Email Parameters
include '../php/phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer();
include '../php/phpmailsettings.php';
$mail->setFrom('info@allsteelcarports.com', 'All Steel Carports');
//$mail->addAddress('michael@burtonsolution.tech');
//$mail->addAddress('michael@allsteelcarports.com');
//Parse Multiple Emails
$memails = explode(",", $demail);
$anum = count($memails) - 1;
$memi = 0;
	while($memi <= $anum){
		$xm = $memails[$memi];
		$mail->addAddress($xm);
		$memi++;
	}
//$mail->addAddress($demail);
//$mail->addBCC('michael@burtonsolution.tech');
$mail->Subject = 'Payment Notice!';

//Include the correct email template...
if($letter == 'dce1'){
  include 'dealer-letters/com-error-letter1.php';
  $note = 'An Over Collect Commission email was sent to ' . $demail . ' by ' . $rep_name . '.';
  $uq = "UPDATE `collections` SET `dce_sent_date` = CURRENT_DATE, `dce_sent_by` = '" . $rep_name . "' WHERE `ID` = '" . $id . "'";
  mysqli_query($conn, $uq) or die($conn->error);
}elseif($letter == 'dop1'){
  include 'dealer-letters/over-payment-letter1.php';
  $note = 'An Over Collect Payment email was sent to ' . $demail . ' by ' . $rep_name . '.';
  $uq = "UPDATE `collections` SET `dop_sent_date` = CURRENT_DATE, `dop_sent_by` = '" . $rep_name . "' WHERE `ID` = '" . $id . "'";
  mysqli_query($conn, $uq) or die($conn->error);
}elseif($letter == '2'){
  include 'dealer-letters/letter2.php';
  $note = 'An additional Over Collect email was sent to ' . $demail . ' by ' . $rep_name . '.';
  $uq = "UPDATE `collections` SET `letter2_sent_date` = CURRENT_DATE, `letter2_sent_by` = '" . $rep_name . "', `letter2_sent_num` = (`letter2_sent_num` + 1) WHERE `ID` = '" . $id . "'";
  mysqli_query($conn, $uq) or die($conn->error);
}else{
  echo 'ERROR!!';
  break;
}

$mail->Body = $etemp;

if($view == 'yes'){
  echo $mail->Body;
}else{
  $mail->send();
	
	//Setup SMS Text
	$mail->ClearAddresses();
	$pq = "SELECT * FROM `cell_providers` WHERE `inactive` != 'Yes'";
	$pg = mysqli_query($conn, $pq) or die($conn->error);
	while($pr = mysqli_fetch_array($pg)){
		$mail->addAddress($dealer_cell . '@' . $pr['extension']);
	}
	//$mail->addAddress('7657169694@txt.att.net');
  //$mail->addBCC('michael@burtonsolution.tech');
	$mail->Body = 'An email was sent to ' . $demail . ' regarding customer "' . $cname . '".';
	$mail->send();
	
	
  $inq = "INSERT INTO `collections_calls` 
          (
          `date`,
          `customer_id`,
          `name`,
          `rep_id`,
          `rep_name`,
          `type`,
          `note`,
          `inactive`
          )
          VALUES
          (
          NOW() + INTERVAL 1 HOUR,
          '" . $id . "',
          '" . $cname . "',
          '" . $rep_id . "',
          '" . $rep_name . "',
          'Email',
          '" . $note . "',
          'No'
          )";
  
  mysqli_query($conn, $inq) or die($conn->error);
  
  echo 'Your email has been sent!';
  
  echo '<script>
        window.location = "../collections.php";
        </script>';
}

?>

