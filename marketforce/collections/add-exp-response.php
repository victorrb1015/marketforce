<?php
include '../php/connection.php';

//Set Organization...
$org_id = $_GET['org_id'];
$_SESSION['org_id'] = $org_id;
include '../php/connection.php';
$iq = "SELECT * FROM `organizations` WHERE `org_id` = '" . $org_id . "'";
$ig = mysqli_query($mf_conn, $iq) or die($mf_conn->error);
$ir = mysqli_fetch_array($ig);
$org_img = $ir['org_logo_url_alt'];

//Get Query String...
$qs = $_SERVER['QUERY_STRING'];

?>
<html>
    <head>
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      
	  	<script>
	  	<?php echo 'var qs = "' . $qs . '";'; ?>

	  	function submit_cn(){
	  		var exp = document.getElementById('explaination').value;
	  		if(exp === ''){
	  			document.getElementById('error_msg').innerHTML = 'Please Enter An Explaination of the Mistake';
	  			return;
	  		}
	  		window.location = "http://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/collections/record-email-response.php?"+qs+"&exp="+exp;
	  	}

      function count_char(){
        var cc = document.getElementById('explaination').value;
        var cct = document.getElementById('char_count');
        var count = 250 - cc.length;
        cct.innerHTML = count;
      }

      function encodeURL(url){
        url = url.replace(/&/g, '%26'); 
        url = url.replace(/#/g, '%23');
        return url;
      }
	  	</script>
    </head>
    <body>
  <div class="jumbotron text-xs-center" style="text-align:center;height:100%;">
  <img src="<?php echo $org_img; ?>" style="width:20%;" />
  <h1 class="display-3">Please Explain The Mistake:</h1>
  <p class="lead">
  	<textarea onkeyup="count_char();" maxlength="250" id="explaination" name="explaination" class="form-control" placeholder="Explaination Here..." style="width:25%;height:20vh;margin:auto;"></textarea>
    <p>Characters Left: <span id="char_count" style="color:red;">250</span>
  </p>
  <hr>
  <p>
    Questions? <a href="mailto:collections@allsteelcarports.com" style="color:red;">Contact us</a>
  </p>
  <p class="lead">
    <a class="btn btn-success btn-lg" role="button" onclick="submit_cn();">Submit</a>
  </p>
</div>
</body>
</html>


