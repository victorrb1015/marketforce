<?php
include 'php/connection.php';

//Load Time Frame Variables
$year = $_GET['year'];
$quarter = $_GET['quarter'];
$month = $_GET['month'];
$day = $_GET['day'];
$week = $_GET['week'];

$q = "SELECT * FROM `collections_calls` WHERE `inactive` != 'Yes'";

$c = 0;
if($year != ''){
  $q .= " AND YEAR(`date`) = '" . $year."'";
  $c++;
}
if($quarter != ''){
  $q .= " AND QUARTER(`date`) = '" . $quarter."'";
  $c++;
}
if($month != ''){
  $q .= " AND MONTH(`date`) = '" . $month."'";
  $c++;
}
if($week != ''){
  $q .= " AND WEEK(`date`) = WEEK('" . $week . "',0)";
  $c++;
}
if($day != ''){
  $q .= " AND DATE(`date`) = DATE('" . $day . "')";
  $c++;
}
if($c == 0){
  //Today's Date
  //$q .= " AND DATE(`date`) = CURRENT_DATE";
  //This Month
  $q .= " AND MONTH(`date`) = MONTH(CURRENT_DATE)";
}


$g = mysqli_query($conn, $q) or die($conn->error);

//Setup total variables
$calls = 0;
$notes = 0;
$emails = 0;
$texts = 0;
$payments = 0;

while($r = mysqli_fetch_array($g)){
  if($r['type'] == 'Phone Call'){
    $calls++;
  }
  if($r['type'] == 'Note'){
    $notes++;
  }
  if($r['type'] == 'Email'){
    $emails++;
  }
  if($r['type'] == 'Text'){
    $texts++;
  }
  if($r['type'] == 'Payment'){
    $payments++;
  }
}

$total_comms = $calls + $emails + $texts;

?>