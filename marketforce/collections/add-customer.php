<?php
include 'php/connection.php';
//Load Variables
/*
$nfn = mysqli_real_escape_string($conn,$_GET['nfn']);
$nphone = mysqli_real_escape_string($conn,$_GET['nphone']);
$nemail = mysqli_real_escape_string($conn,$_GET['nemail']);
$nstate = $_GET['nstate'];
$ninvoice = mysqli_real_escape_string($conn,$_GET['ninvoice']);
$namount = mysqli_real_escape_string($conn,$_GET['namount']);
$ncat = $_GET['ncat'];
$nrep = $_GET['nrep'];
$note = mysqli_real_escape_string($conn, $_GET['note']);
*/
$mode = $_POST['mode'];// "Edit" or ""
$nfn = mysqli_real_escape_string($conn,$_POST['nfull_name']);
$nphone = mysqli_real_escape_string($conn,$_POST['nphone']);
$nemail = mysqli_real_escape_string($conn,$_POST['nemail']);
$nstate = $_POST['nstate'];
$ninvoice = mysqli_real_escape_string($conn,$_POST['ninvoice']);
$namount = mysqli_real_escape_string($conn,$_POST['namount']);
$ncat = $_POST['ncat'];
$nrep = $_POST['nrep'];
$note = mysqli_real_escape_string($conn, $_POST['add_note']);
$af = $_POST['auto_frequency'];
$ndebt = mysqli_real_escape_string($conn, $_POST['ndebt']);
$eid = $_POST['edit_id'];
$ocat = $_POST['ocat'];
$did = $_POST['dname'];
//Get dealer name from id...
$dnq = "SELECT * FROM `dealers` WHERE `ID` = '" . $did . "'";
$dng = mysqli_query($conn, $dnq) or die($conn->error);
$dnr = mysqli_fetch_array($dng);
$dname = mysqli_real_escape_string($conn,$dnr['name']);

$namount = str_replace(',','',$namount);
$ndebt = str_replace(',','',$ndebt);

//Dealer_pmt additional fields
$pdate = date("Y-m-d",strtotime($_POST['pdate']));
$sprice = $_POST['sprice'];
$tamount = $_POST['tamount'];
$crate = $_POST['crate'];
$acoll = $_POST['acoll'];

//In Pre-Collections?
$precb = $_POST['precb'];

//Remove commas from money amounts
$sprice = str_replace(',','',$sprice);
$tamount = str_replace(',','',$tamount);
$acoll = str_replace(',','',$acoll);
              

//Update Dealer Cell Phone#
$dcphone = mysqli_real_escape_string($conn, $_POST['dcell']);
if($dcphone != ''){
$udcq = "UPDATE `new_dealer_form` SET `dcphone` = '" . $dcphone . "' WHERE `ID` = '" . $did . "'";
mysqli_query($conn, $udcq) or die($conn->error);
}


if($_FILES['fileToUpload']['size'] != 0) {
//$post_body = file_get_contents('php://input');//Set the RAW data from POST Variables to be able to view them.
$id = rand(100000,1000000);

//Document Parsing
$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/../img-storage/doc_imgs/";
$target_file2 = $target_dir . basename($_FILES["fileToUpload"]["name"]);

$uploadOk = 1;
$imageFileType = pathinfo($target_file2,PATHINFO_EXTENSION);
$target_file = $target_dir . $id . "_docImg." . $imageFileType;
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
      //  echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "pdf" ) {
    echo "<br>ERROR!- only JPG, JPEG, PDF, PNG & GIF files are allowed.";
    //echo "<br><script>alert('" . $imageFileType . "');</script>";
    $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "<br>ERROR!- your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      
      //INSERT FILE PATH INTO DATABASE
      $file_name_1 = 'http://burtonsolution.tech/img-storage/doc_imgs/' . $id . "_docImg." . $imageFileType;
      //echo '<br>File was moved to the right location!';
    }
}
}else{
  //Set URL to empty if no file was selected to upload...
  $file_name_1 = '';
  $uploadOk = 0;
}//End if file was uploaded

if($mode != 'Edit'){
//Get Rep's Name
$rnq = "SELECT * FROM `users` WHERE `ID` = '" . $nrep . "'";
$rng = mysqli_query($conn, $rnq) or die($conn->error);
$rnr = mysqli_fetch_array($rng);
$rep_name = $rnr['fname'] . ' ' . $rnr['lname'];


$iq = "INSERT INTO `collections`
      (
      `date`,
      `assigned_rep_id`,
      `assigned_rep_name`,
      `dname`,
      `did`,
      `name`,
      `state`,
      `phone`,
      `email`,
      `inv#`,
      `p_date`,
      `sale_price`,
      `tamount`,
      `sale_collected_amount`,
      `initial_debt`,
      `" . $ncat . "`,
      `notes`,
      `collection`,
      `paid`,
      `precb`,
      `file_url`,
       `inactive`
      )
      VALUES
      (
      NOW() + INTERVAL 1 HOUR,
      '" . $nrep . "',
      '" . $rep_name . "',
      '" . $dname . "',
      '" . $did . "',
      '" . $nfn . "',
      '" . $nstate . "',
      '" . $nphone . "',
      '" . $nemail . "',
      '" . $ninvoice . "',
      '" . $pdate . "',
      '" . $sprice . "',
      '" . $tamount . "',
      '" . $acoll . "',
      '" . $namount . "',
      '" . $namount . "',
      '" . $note . "',
      'Yes',
      'No',
      '" . $precb . "',
      '" . $file_name_1 . "',
      'No'
      )";

mysqli_query($conn, $iq) or die($conn->error);

echo '<br>' . $nfn . ' was successfully added into collections!';
  
}else{
  
  //Get Rep's Name
  $rnq = "SELECT * FROM `users` WHERE `ID` = '" . $nrep . "'";
  $rng = mysqli_query($conn, $rnq) or die($conn->error);
  $rnr = mysqli_fetch_array($rng);
  $rep_name = $rnr['fname'] . ' ' . $rnr['lname'];
  
  //Clear Old Category Balance
  $cq = "UPDATE `collections`
          SET
          `" . $ocat . "` = ''
          WHERE `ID` = '" . $eid . "'";
  mysqli_query($conn,$cq) or die($conn->error);
  
  //Update the customer's information...
  if($uploadOk == 1){
    $uq = "UPDATE `collections`
          SET
          `assigned_rep_id` = '" . $nrep . "',
          `assigned_rep_name` = '" . $rep_name . "',
          `dname` = '" . $dname . "',
          `did` = '" . $did . "',
          `name` = '" . $nfn . "',
          `state` = '" . $nstate . "',
          `phone` = '" . $nphone . "',
          `email` = '" . $nemail . "',
          `inv#` = '" . $ninvoice . "',
          `p_date` = '" . $pdate . "',
          `sale_price` = '" . $sprice . "',
          `tamount` = '" . $tamount . "',
          `c_rate` = " . $crate . ",
          `sale_collected_amount` = '" . $acoll . "',
          `initial_debt` = '" . $namount . "',
          `" . $ncat . "` = '" . $ndebt . "',
          `notes` = '" . $note . "',
          `precb` = '" . $precb . "',
          `auto_frequency` = '" . $af . "',
          `file_url` = '" . $file_name_1 . "'
          WHERE
          `ID` = '" . $eid . "'";
  }else{
  $uq = "UPDATE `collections`
          SET
          `assigned_rep_id` = '" . $nrep . "',
          `assigned_rep_name` = '" . $rep_name . "',
          `dname` = '" . $dname . "',
          `did` = '" . $did . "',
          `name` = '" . $nfn . "',
          `state` = '" . $nstate . "',
          `phone` = '" . $nphone . "',
          `email` = '" . $nemail . "',
          `inv#` = '" . $ninvoice . "',
          `p_date` = '" . $pdate . "',
          `sale_price` = '" . $sprice . "',
          `tamount` = '" . $tamount . "',
          `c_rate` = " . $crate . ",
          `sale_collected_amount` = '" . $acoll . "',
          `initial_debt` = '" . $namount . "',
          `" . $ncat . "` = '" . $ndebt . "',
          `notes` = '" . $note . "',
          `precb` = '" . $precb . "',
          `auto_frequency` = '" . $af . "'
          WHERE
          `ID` = '" . $eid . "'";
  }
  mysqli_query($conn,$uq) or die($conn->error);
  
  echo '<br>' . $nfn . ' has been successfully updated!';
  
}



echo '<script>
      window.close();
      </script>';
?>