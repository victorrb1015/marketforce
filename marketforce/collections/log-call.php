<?php
include 'php/connection.php';

//Load Variables
$cid = $_GET['cid'];
$type = 'Phone Call';
$tphone = mysqli_real_escape_string($conn, $_GET['tphone']);
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];
$note = mysqli_real_escape_string($conn, $_GET['note']);
$cnote = mysqli_real_escape_string($conn, $_GET['call_note']);

//Generate DB Note for Call
if($cnote != ''){
  $fnote = $note . ' -> ' . $cnote;
}else{
  $fnote = $note;
}

//Get customer name
$cnq = "SELECT * FROM `collections` WHERE `ID` = '" . $cid . "'";
$cng = mysqli_query($conn, $cnq) or die($conn->error);
$cnr = mysqli_fetch_array($cng);
$cname = mysqli_real_escape_string($conn, $cnr['name']);


//Log Call
$mq = "INSERT INTO `collections_calls`
      (
      `date`,
      `customer_id`,
      `name`,
      `rep_id`,
      `rep_name`,
      `type`,
      `phone_tried`,
      `note`,
      `inactive`
      )
      VALUES
      (
      NOW() + INTERVAL 1 HOUR,
      '" . $cid . "',
      '" . $cname . "',
      '" . $rep_id . "',
      '" . $rep_name . "',
      '" . $type . "',
      '" . $tphone . "',
      '" . $fnote . "',
      'No'
      )";
$mg = mysqli_query($conn, $mq) or die($conn->error);
  
//Response...
echo 'Your call to ' . $cname . ' has been recorded!';
?>
  
  