<?php
include 'php/connection.php';

//Load Variables
$cid = $_GET['cid'];
$type = 'Payment';
$amount = mysqli_real_escape_string($conn,$_GET['amount']);
$method = $_GET['method'];
$check_num = mysqli_real_escape_string($conn,$_GET['check_num']);
$cc_conf = mysqli_real_escape_string($conn,$_GET['cc_conf']);
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];
$note = mysqli_real_escape_string($conn, $_GET['note']);
$edn = mysqli_real_escape_string($conn, $_GET['edn']);

//Get customer name
$cnq = "SELECT * FROM `collections` WHERE `ID` = '" . $cid . "'";
$cng = mysqli_query($conn, $cnq) or die($conn->error);
$cnr = mysqli_fetch_array($cng);
$cname = mysqli_real_escape_string($conn, $cnr['name']);

                  $i = 0;
                  if($cnr['ck_ca_cc'] != ''){
                    $cat = 'ck_ca_cc';
                    $i++;
                  }
                  if($cnr['po'] != ''){
                    $cat = 'po';
                    $i++;
                  }
                  if($cnr['nsf_stop'] != ''){
                    $cat = 'nsf_stop';
                    $i++;
                  }
                  if($cnr['repair'] != ''){
                    $cat = 'repair';
                    $i++;
                  }
                  if($cnr['dealer_pmt'] != ''){
                    $cat = 'dealer_pmt';
                    $i++;
                  }
                  if($cnr['dealer_fin'] != ''){
                    $cat = 'dealer_fin';
                    $i++;
                  }
                  if($cnr['legal'] != ''){
                    $cat = 'legal';
                    $i++;
                  }
                  if($cnr['bli'] != ''){
                    $cat = 'bli';
                    $i++;
                  }
                  if($cnr['ezpay'] != ''){
                    $cat = 'ezpay';
                    $i++;
                  }
                  if($cnr['scoggin'] != ''){
                    $cat = 'scoggin';
                    $i++;
                  }
                  if($cnr['double_d'] != ''){
                    $cat = 'double_d';
                    $i++;
                  }
                  if($cnr['dallen_legacy'] != ''){
                    $cat = 'dallen_legacy';
                    $i++;
                  }
									if($cnr['rto_national'] != ''){
										$cat = 'rto_national';
										$i++;
									}



//Log Call
$mq = "INSERT INTO `collections_calls`
      (
      `date`,
      `customer_id`,
      `name`,
      `rep_id`,
      `rep_name`,
      `type`,
      `amount`,
      `method`,
      `check_num`,
      `cc_conf`,
			`edn`,
      `note`,
      `inactive`
      )
      VALUES
      (
      NOW() + INTERVAL 1 HOUR,
      '" . $cid . "',
      '" . $cname . "',
      '" . $rep_id . "',
      '" . $rep_name . "',
      '" . $type . "',
      '" . $amount . "',
      '" . $method . "',
      '" . $check_num . "',
      '" . $cc_conf . "',
			'" . $edn . "',
      '" . $note . "',
      'No'
      )";
$mg = mysqli_query($conn, $mq) or die($conn->error);
  
$pcq = "SELECT * FROM `collections` WHERE `ID` = '" . $cid . "'";
$pcg = mysqli_query($conn, $pcq) or die($conn->error);
$pcr = mysqli_fetch_array($pcg);
if($pcr[$cat] <= $amount){
  $uq = "UPDATE `collections` SET `" . $cat . "` = `" . $cat . "` - " . $amount . ", `paid` = 'Yes', `collection` = 'No' WHERE `ID` = '" . $cid . "'";
  $ug = mysqli_query($conn, $uq) or die($conn->error);
}else{
  $uq = "UPDATE `collections` SET `" . $cat . "` = `" . $cat . "` - " . $amount . " WHERE `ID` = '" . $cid . "'";
  $ug = mysqli_query($conn, $uq) or die($conn->error);
}


if($method == 'Electronic Payment'){
	
$pidq = "SELECT * FROM `collections_calls` ORDER BY `ID` DESC LIMIT 1";
$pidg = mysqli_query($conn, $pidq) or die($conn->error);
$pidr = mysqli_fetch_array($pidg);
	
	$dcq = "INSERT INTO `check_requests`
					(
					`date`,
					`time`,
					`type`,
					`user`,
					`user_id`,
					`amount`,
					`to`,
					`category`,
					`cust_id`,
					`pid`,
					`status`,
					`note`
					)
					VALUES
					(
					CURRENT_DATE,
					CURRENT_TIME,
					'Collections Payment',
					'" . $rep_name . "',
					'" . $rep_id . "',
					'" . $amount . "',
					'" . $cname . " -> $" . $amount . "',
					'" . $cat . "',
					'" . $cid . "',
					'" . $pidr['ID'] . "',
					'Pending',
					'" . $edn . "'
					)";
	$dcg = mysqli_query($conn, $dcq) or die($conn->error);
	
	//Email Accounting
	include '../php/phpmailer/PHPMailerAutoload.php';
	$mail = new PHPMailer();
	include '../php/phpmailsettings.php';
	$mail->setFrom('collections@allsteelcarports.com','Market Force');
	//$mail->addAddress('scott@allsteelcarports.com');
	$mail->addAddress('melanie@allsteelcarports.com');
	//$mail->addCC('travis@allsteelcarports.com');
	//$mail->addBCC('michael@burtonsolution.tech');
	$mail->Subject = 'New Electronic Payment Confirmation Request';
	
	$mail->Body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Collection Report</title>


<style type="text/css">
img {
max-width: 100%;
}
body {
-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em;
}
body {
background-color: #f6f6f6;
}
@media only screen and (max-width: 640px) {
  body {
    padding: 0 !important;
  }
  h1 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h2 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h3 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h4 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h1 {
    font-size: 22px !important;
  }
  h2 {
    font-size: 18px !important;
  }
  h3 {
    font-size: 16px !important;
  }
  .container {
    padding: 0 !important; width: 100% !important;
  }
  .content {
    padding: 0 !important;
  }
  .content-wrap {
    padding: 10px !important;
  }
  .invoice {
    width: 100% !important;
  }
}

/* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
* {
  margin: 0;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  box-sizing: border-box;
  font-size: 14px;
}

img {
  max-width: 100%;
}

body {
  -webkit-font-smoothing: antialiased;
  -webkit-text-size-adjust: none;
  width: 100% !important;
  height: 100%;
  line-height: 1.6em;
  /* 1.6em * 14px = 22.4px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 22px;*/
}

/* Lets make sure all tables have defaults */
table td {
  vertical-align: top;
}

/* -------------------------------------
    BODY & CONTAINER
------------------------------------- */
body {
  background-color: #f6f6f6;
}

.body-wrap {
  background-color: #f6f6f6;
  width: 100%;
}

.container {
  display: block !important;
  max-width: 600px !important;
  margin: 0 auto !important;
  /* makes it centered */
  clear: both !important;
}

.content {
  max-width: 600px;
  margin: 0 auto;
  display: block;
  padding: 20px;
}

/* -------------------------------------
    HEADER, FOOTER, MAIN
------------------------------------- */
.main {
  background-color: #fff;
  border: 1px solid #e9e9e9;
  border-radius: 3px;
}

.content-wrap {
  padding: 20px;
}

.content-block {
  padding: 0 0 20px;
}

.header {
  width: 100%;
  margin-bottom: 20px;
}

.footer {
  width: 100%;
  clear: both;
  color: #999;
  padding: 20px;
}
.footer p, .footer a, .footer td {
  color: #999;
  font-size: 12px;
}

/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1, h2, h3 {
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  color: #000;
  margin: 40px 0 0;
  line-height: 1.2em;
  font-weight: 400;
}

h1 {
  font-size: 32px;
  font-weight: 500;
  /* 1.2em * 32px = 38.4px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 38px;*/
}

h2 {
  font-size: 24px;
  /* 1.2em * 24px = 28.8px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 29px;*/
}

h3 {
  font-size: 18px;
  /* 1.2em * 18px = 21.6px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 22px;*/
}

h4 {
  font-size: 14px;
  font-weight: 600;
}

p, ul, ol {
  margin-bottom: 10px;
  font-weight: normal;
}
p li, ul li, ol li {
  margin-left: 5px;
  list-style-position: inside;
}

/* -------------------------------------
    LINKS & BUTTONS
------------------------------------- */
a {
  color: #348eda;
  text-decoration: underline;
}

.btn-primary {
  text-decoration: none;
  color: #FFF;
  background-color: #348eda;
  border: solid #348eda;
  border-width: 10px 20px;
  line-height: 2em;
  /* 2em * 14px = 28px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 28px;*/
  font-weight: bold;
  text-align: center;
  cursor: pointer;
  display: inline-block;
  border-radius: 5px;
  text-transform: capitalize;
}

/* -------------------------------------
    OTHER STYLES THAT MIGHT BE USEFUL
------------------------------------- */
.last {
  margin-bottom: 0;
}

.first {
  margin-top: 0;
}

.aligncenter {
  text-align: center;
}

.alignright {
  text-align: right;
}

.alignleft {
  text-align: left;
}

.clear {
  clear: both;
}

/* -------------------------------------
    ALERTS
    Change the class depending on warning email, good email or bad email
------------------------------------- */
.alert {
  font-size: 16px;
  color: #fff;
  font-weight: 500;
  padding: 20px;
  text-align: center;
  border-radius: 3px 3px 0 0;
}
.alert a {
  color: #fff;
  text-decoration: none;
  font-weight: 500;
  font-size: 16px;
}
.alert.alert-warning {
  //background-color: #FF9F00;
  background-color:#F60032;
}
.alert.alert-bad {
  background-color: #D0021B;
}
.alert.alert-good {
  background-color: #68B90F;
}

/* -------------------------------------
    INVOICE
    Styles for the billing table
------------------------------------- */
.invoice {
  margin: 40px auto;
  text-align: left;
  width: 80%;
}
.invoice td {
  padding: 5px 0;
}
.invoice .invoice-items {
  width: 100%;
}
.invoice .invoice-items td {
  border-top: #eee 1px solid;
}
.invoice .invoice-items .total td {
  border-top: 2px solid #333;
  border-bottom: 2px solid #333;
  font-weight: 700;
}

/* -------------------------------------
    RESPONSIVE AND MOBILE FRIENDLY STYLES
------------------------------------- */
@media only screen and (max-width: 640px) {
  body {
    padding: 0 !important;
  }

  h1, h2, h3, h4 {
    font-weight: 800 !important;
    margin: 20px 0 5px !important;
  }

  h1 {
    font-size: 22px !important;
  }

  h2 {
    font-size: 18px !important;
  }

  h3 {
    font-size: 16px !important;
  }

  .container {
    padding: 0 !important;
    width: 100% !important;
  }

  .content {
    padding: 0 !important;
  }

  .content-wrap {
    padding: 10px !important;
  }

  .invoice {
    width: 100% !important;
  }
}

/*# sourceMappingURL=styles.css.map */
</style>
</head>

<body itemscope itemtype="http://schema.org/EmailMessage" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">

<div style="text-align:center;">
<img src="';
switch ($_SESSION['org_id']) {
  case "832122":
    $mail->Body .= 'http://marketforceapp.com/assets/img/brands/acero-news.png';
    break;
  case "738004":
    $mail->Body .= 'http://marketforceapp.com/assets/img/brands/acero-news.png';
    break;
  case "654321":
    $mail->Body .= 'http://marketforceapp.com/assets/img/brands/acero-news.png';
    break;
  case "162534":
    $mail->Body .= 'http://marketforceapp.com/assets/img/brands/NorthEdgeSteel-01.png';
    break;
  case "615243":
    $mail->Body .= 'http://marketforceapp.com/assets/img/brands/legacyMFlogo.png';
    break;
  default:
  $mail->Body .= 'http://allsteelcarports.com/marketforce/marketforce/img/logo-allsteel-full.png';
}
$mail->Body .= '" width="500" />
</div>

<table class="body-wrap" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6"><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
		<td class="container" width="600" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
			<div class="content" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
				<table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff"><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert alert-warning" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #F60032; margin: 0; padding: 20px;" align="center" bgcolor="#F60032" valign="top">
							Electronic Payment Confirmation Request
						</td>
					</tr><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-wrap" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
							<table width="100%" cellpadding="0" cellspacing="0" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
										 
                    A new Electronic Payment Confirmation Request has been submitted in Market Force by <strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">' . $rep_name . '</strong>.
										<!--<strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">' . $cname . '</strong>.-->
										<br>
									</td>
								</tr><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
										<!--This report is based on collections information entered into Market Force on-->
									</td>
								</tr>
                <!--<tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; text-align: center;" valign="top">
										<a href="http://ignition.church/admin" class="btn-primary" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;">View Registrant Information</a>
									</td>
								</tr>-->
								<!--<tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
										Thanks for choosing Acme Inc.
									</td>
								</tr>-->
								</table></td>
					</tr></table><div class="footer" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">
					<table width="100%" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="aligncenter content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">Questions? Email: repairs@allsteelcarports.com</td>
						</tr></table></div></div>
		</td>
		<td style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
	</tr></table></body>
</html>';
	
	$mail->send();
}

//Response...
echo 'Your payment from ' . $cnr['name'] . ' has been recorded!';
?>
  
  