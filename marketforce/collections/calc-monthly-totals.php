<?php
include 'php/connection.php';

//Setup Variables
$jan = 0;
$feb = 0;
$mar = 0;
$apr = 0;
$may = 0;
$jun = 0;
$jul = 0;
$aug = 0;
$sep = 0;
$oct = 0;
$nov = 0;
$dec = 0;
//Financed
$jan_f = 0;
$feb_f = 0;
$mar_f = 0;
$apr_f = 0;
$may_f = 0;
$jun_f = 0;
$jul_f = 0;
$aug_f = 0;
$sep_f = 0;
$oct_f = 0;
$nov_f = 0;
$dec_f = 0;
//Unfinanced
$jan_u = 0;
$feb_u = 0;
$mar_u = 0;
$apr_u = 0;
$may_u = 0;
$jun_u = 0;
$jul_u = 0;
$aug_u = 0;
$sep_u = 0;
$oct_u = 0;
$nov_u = 0;
$dec_u = 0;

//January
$q = "SELECT * FROM `collections` WHERE MONTH(`date`) = 1 AND `collection` = 'Yes' AND `precb` != 'Yes' AND `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
    $jan_u = $jan_u + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
    $jan_f = $jan_f + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
}
$jan = $jan_u + $jan_f;
//February
$q = "SELECT * FROM `collections` WHERE MONTH(`date`) = 2 AND `collection` = 'Yes' AND `precb` != 'Yes' AND `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
    $feb_u = $feb_u + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
    $feb_f = $feb_f + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
}
$feb = $feb_u + $feb_f;
//March
$q = "SELECT * FROM `collections` WHERE MONTH(`date`) = 3 AND `collection` = 'Yes' AND `precb` != 'Yes' AND `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
    $mar_u = $mar_u + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
    $mar_f = $mar_f + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
}
$mar = $mar_u + $mar_f;
//April
$q = "SELECT * FROM `collections` WHERE MONTH(`date`) = 4 AND `collection` = 'Yes' AND `precb` != 'Yes' AND `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
    $apr_u = $apr_u + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
    $apr_f = $apr_f + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
}
$apr = $apr_u + $apr_f;
//May
$q = "SELECT * FROM `collections` WHERE MONTH(`date`) = 5 AND `collection` = 'Yes' AND `precb` != 'Yes' AND `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
    $may_u = $may_u + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
    $may_f = $may_f + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
}
$may = $may_u + $may_f;
//June
$q = "SELECT * FROM `collections` WHERE MONTH(`date`) = 6 AND `collection` = 'Yes' AND `precb` != 'Yes' AND `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
    $jun_u = $jun_u + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
    $jun_f = $jun_f + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
}
$jun = $jun_u + $jun_f;
//July
$q = "SELECT * FROM `collections` WHERE MONTH(`date`) = 7 AND `collection` = 'Yes' AND `precb` != 'Yes' AND `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
    $jul_u = $jul_u + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
    $jul_f = $jul_f + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
}
$jul = $jul_u + $jul_f;
//August
$q = "SELECT * FROM `collections` WHERE MONTH(`date`) = 8 AND `collection` = 'Yes' AND `precb` != 'Yes' AND `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
    $aug_u = $aug_u + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
    $aug_f = $aug_f + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
}
$aug = $aug_u + $aug_f;
//September
$q = "SELECT * FROM `collections` WHERE MONTH(`date`) = 9 AND `collection` = 'Yes' AND `precb` != 'Yes' AND `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
    $nov_u = $nov_u + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
    $nov_f = $nov_f + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
}
$nov = $nov_u + $nov_f;
//October
$q = "SELECT * FROM `collections` WHERE MONTH(`date`) = 10 AND `collection` = 'Yes' AND `precb` != 'Yes' AND `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
    $oct_u = $oct_u + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
    $oct_f = $oct_f + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
}
$oct = $oct_u + $oct_f;
//November
$q = "SELECT * FROM `collections` WHERE MONTH(`date`) = 11 AND `collection` = 'Yes' AND `precb` != 'Yes' AND `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
    $nov_u = $nov_u + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
    $nov_f = $nov_f + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
}
$nov = $nov_u + $nov_f;
//December
$q = "SELECT * FROM `collections` WHERE MONTH(`date`) = 12 AND `collection` = 'Yes' AND `precb` != 'Yes' AND `inactive` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
    $dec_u = $dec_u + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
    $dec_f = $dec_f + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
}
$dec = $dec_u + $dec_f;

?>










