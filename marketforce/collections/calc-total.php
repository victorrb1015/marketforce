<?php
include 'php/connection.php';

/*------------------------------------------
Enter Change Log Notes Here...
------------------------------------------*/

//Load Time Frame Variables
$year = $_GET['year'];
$quarter = $_GET['quarter'];
$month = $_GET['month'];
$day = $_GET['day'];
$week = $_GET['week'];

$payments_today = 0;

$cdq = "SELECT * FROM `collections` WHERE `inactive` != 'Yes' AND `precb` != 'Yes' AND `agency` = ''";


$ptdpq = "SELECT * FROM `collections_calls` WHERE `type` = 'Payment'";


$c = 0;
if($year != ''){
  $cdq .= " AND YEAR(`date`) = '" . $year."'";
  $ptdpq .= " AND YEAR(`date`) = '" . $year."'";
  $c++;
}
if($quarter != ''){
  $cdq .= " AND QUARTER(`date`) = '" . $quarter."'";
  $ptdpq .= " AND QUARTER(`date`) = '" . $quarter."'";
  $c++;
}
if($month != ''){
  $cdq .= " AND MONTH(`date`) = '" . $month."'";
  $ptdpq .= " AND MONTH(`date`) = '" . $month."'";
  $c++;
}
if($week != ''){
  $cdq .= " AND WEEK(`date`) = WEEK('" . $week . "',0)";
  $ptdpq .= " AND WEEK(`date`) = WEEK('" . $week . "',0)";
  $c++;
}
if($day != ''){
  $cdq .= " AND DATE(`date`) = '" . $day . "'";
  $ptdpq .= " AND DATE(`date`) = '" . $day . "'";
  $c++;
}
if($c == 0){
  if($_GET['bypass'] == 'yes'){
  //Today's Date
  $cdq .= " AND DATE(`date`) = CURRENT_DATE";
  $ptdpq .= " AND DATE(`date`) = CURRENT_DATE";
  }elseif($_GET['all'] != 'all'){
  //This Month
  $cdq .= " AND MONTH(`date`) = MONTH(CURRENT_DATE)";
  $ptdpq .= " AND MONTH(`date`) = MONTH(CURRENT_DATE)";
  }
}


$ptdpg = mysqli_query($conn, $ptdpq) or die($conn->error);
    
//echo $ptdpq;

    while($ptdpr = mysqli_fetch_array($ptdpg)){
      $payments_today = $payments_today + floatval($ptdpr['amount']);
    }


$cdg = mysqli_query($conn, $cdq) or die($conn->error);


//Variable Setup
//individual
$coll_ck_ca_cc = 0;
$coll_po = 0;
$coll_nsf_stop = 0;
$coll_repair = 0;
$coll_dealer_pmt = 0;
$coll_dealer_fin = 0;
$coll_legal = 0;
$coll_bli = 0;
$coll_ezpay = 0;
$coll_scoggin = 0;
$coll_double_d = 0;
$coll_dallen_legacy = 0;
$coll_rto_national = 0;
//totals
$coll_non_fin_total = 0;
$coll_fin_total = 0;
$coll_legal = 0;
$coll_dealer = 0;
$coll_total = 0;

//individual
$paid_ck_ca_cc = 0;
$paid_po = 0;
$paid_nsf_stop = 0;
$paid_repair = 0;
$paid_dealer_pmt = 0;
$paid_dealer_fin = 0;
$paid_legal = 0;
$paid_bli = 0;
$paid_ezpay = 0;
$paid_scoggin = 0;
$paid_double_d = 0;
$paid_dallen_legacy = 0;
$paid_rto_national = 0;
//totals
$paid_non_fin_total = 0;
$paid_fin_total = 0;
$paid_legal = 0;
$paid_dealer = 0;
$paid_total_by_cat = 0;
$paid_total = 0;

$gtotal = 0;
$coll_payments = 0;
$collected_today = 0;

while($r = mysqli_fetch_array($cdg)){
  if($r['collection'] == 'Yes' && $r['paid'] == 'No'){
    
  //Individual Calcs...
  $coll_ck_ca_cc = $coll_ck_ca_cc + floatval($r['ck_ca_cc']);
  $coll_nsf_stop = $coll_nsf_stop + floatval($r['nsf_stop']);
  $coll_repair = $coll_repair + floatval($r['repair']);
  
 
    
  //Individual Calcs....
  $coll_po = $coll_po + floatval($r['po']);
  $coll_dealer_fin = $coll_dealer_fin + floatval($r['dealer_fin']);
  $coll_bli = $coll_bli + floatval($r['bli']);
  $coll_ezpay = $coll_ezpay + floatval($r['ezpay']);
  $coll_scoggin = $coll_scoggin + floatval($r['scoggin']);
  $coll_double_d = $coll_double_d + floatval($r['double_d']);
  $coll_dallen_legacy = $coll_dallen_legacy + floatval($r['dallen_legacy']);
  $coll_rto_national = $coll_rto_national + floatval($r['rto_national']);
  
  
  $coll_legal = $coll_legal + floatval($r['legal']);
  
  $coll_dealer = $coll_dealer + floatval($r['dealer_pmt']);
    
    
  $dpq = "SELECT * FROM `collections_calls` WHERE `customer_id` = '" . $r['ID'] . "' AND `type` = 'Payment'";
  $dpg = mysqli_query($conn, $dpq) or die($conn->error);
    
    while($dpr = mysqli_fetch_array($dpg)){
      $coll_payments = $coll_payments + floatval($dpr['amount']);
      
    }

    
  }
  
  if($r['collection'] == 'No' && $r['paid'] == 'Yes'){
    
   $paid_total = $paid_total + floatval($r['initial_debt']);
    
  //Individual Calcs...
  if($r['ck_ca_cc'] != ''){
  $paid_ck_ca_cc = $paid_ck_ca_cc + floatval($r['initial_debt']);
  }
  if($r['nsf_stop'] != ''){
  $paid_nsf_stop = $paid_nsf_stop + floatval($r['initial_debt']);
  }
  if($r['repair'] != ''){
  $paid_repair = $paid_repair + floatval($r['initial_debt']);
  }
  
    
  //Individual Calcs....
  if($r['po'] != ''){
  $paid_po = $paid_po + floatval($r['initial_debt']);
  }
  if($r['dealer_fin'] != ''){
  $paid_dealer_fin = $paid_dealer_fin + floatval($r['initial_debt']);
  }
  if($r['bli'] != ''){
  $paid_bli = $paid_bli + floatval($r['initial_debt']);
  }
  if($r['ezpay'] != ''){
  $paid_ezpay = $paid_ezpay + floatval($r['initial_debt']);
  }
  if($r['scoggin'] != ''){
  $paid_scoggin = $paid_scoggin + floatval($r['initial_debt']);
  }
  if($r['double_d'] != ''){
  $paid_double_d = $paid_double_d + floatval($r['initial_debt']);
  }
  if($r['dallen_legacy'] != ''){
  $paid_dallen_legacy = $paid_dallen_legacy + floatval($r['initial_debt']);
  }
  if($r['rtp_national'] != ''){
  $paid_rto_national = $paid_rto_national + floatval($r['initial_debt']);
  }
    
  
  if($r['legal'] != ''){
  $paid_legal = $paid_legal + floatval($r['initial_debt']);
  }
  if($r['dealer_pmt'] != ''){
  $paid_dealer = $paid_dealer + floatval($r['initial_debt']);
  }
    
   
  /*
   //Individual Calcs...
  $paid_ck_ca_cc = $paid_ck_ca_cc + floatval($r['ck_ca_cc']);
  $paid_nsf_stop = $paid_nsf_stop + floatval($r['nsf_stop']);
  $paid_repair = $paid_repair + floatval($r['repair']);
  $paid_non_fin_total = $paid_non_fin_total + floatval($r['ck_ca_cc']) + floatval($r['nsf_stop']) + floatval($r['repair']);
  
    
  //Individual Calcs....
  $paid_po = $paid_po + floatval($r['po']);
  $paid_dealer_fin = $paid_dealer_fin + floatval($r['dealer_fin']);
  $paid_bli = $paid_bli + floatval($r['bli']);
  $paid_ezpay = $paid_ezpay + floatval($r['ezpay']);
  $paid_scoggin = $paid_scoggin + floatval($r['scoggin']);
  $paid_double_d = $paid_double_d + floatval($r['double_d']);
  $paid_dallen_legacy = $paid_dallen_legacy + floatval($r['dallen_legacy']);
  $paid_rto_national = $paid_rto_national + floatval($r['rto_national']);
    
  $paid_fin_total = $paid_fin_total + floatval($r['rto_national']) + floatval($r['po']) + floatval($r['dealer_fin']) + floatval($r['bli']) + floatval($r['ezpay']) + floatval($r['scoggin']) + floatval($r['double_d']) + floatval($r['dallen_legacy']);
  
  $paid_legal = $paid_legal + floatval($r['legal']);
  
  $paid_dealer = $paid_dealer + floatval($r['dealer_pmt']);
  */
  
 
  }
  
  //echo $r['name'] . '<br>';
}


//COLLECTIONS TOTALS
 $coll_non_fin_total = $coll_non_fin_total + $coll_ck_ca_cc + $coll_nsf_stop + $coll_repair + $coll_dealer;
 $coll_fin_total = $coll_fin_total + $coll_rto_national + $coll_po + $coll_dealer_fin + $coll_bli + $coll_ezpay + $coll_scoggin + $coll_double_d + $coll_dallen_legacy;

//PAID TOTALS
 $paid_non_fin_total = $paid_non_fin_total + $paid_ck_ca_cc + $paid_nsf_stop + $paid_repair + $paid_dealer;
 $paid_fin_total = $paid_fin_total + $paid_rto_national + $paid_po + $paid_dealer_fin + $paid_bli + $paid_ezpay + $paid_scoggin + $paid_double_d + $paid_dallen_legacy;



$coll_total = $coll_total + $coll_fin_total + $coll_non_fin_total;
$paid_total_by_cat = $paid_total_by_cat + $paid_fin_total + $paid_non_fin_total + $coll_payments;
$paid_total = $paid_total + $payments_today;

$gtotal = $coll_total + $paid_total_by_cat + $paid_total;


//Calculate ALL Collections Amount
$overall_coll_amount = 0;

$ocaq = "SELECT * FROM `collections` WHERE `precb` != 'Yes' AND `inactive` != 'Yes' AND `collection` = 'Yes'";
$ocag = mysqli_query($conn, $ocaq) or die($conn->error);
while($ocar = mysqli_fetch_array($ocag)){
  if($ocar['legal'] == ''){
  $pq = "SELECT * FROM `collections_calls` WHERE `inactive` != 'Yes' AND `customer_id` = '" . $ocar['ID'] . "' AND `type` = 'Payment'";
  $pg = mysqli_query($conn, $pq) or die($conn->error);
  $pmts = 0;
  while($pr = mysqli_fetch_array($pg)){
    $pmts = $pmts + $pr['amount'];
  }
  $amt = $ocar['initial_debt'] - $pmts;
  $overall_coll_amount = $overall_coll_amount + $amt;
  }
}

/*echo '<h3>Collections:</h3>';
echo '<b>Non-Financed:</b> ' . $coll_non_fin_total . '<br>';
echo '<b>Finanaced:</b> ' . $coll_fin_total . '<br>';
echo '<b>Dealer:</b> ' . $coll_dealer . '<br>';
echo '<b>Total:</b> ' . $coll_total;
echo '<br><b style="color:red;">Legal:</b> ' . $coll_legal;
  
echo '<h3>Paid:</h3>';
echo $paid_ck_ca_cc . "<br>";
echo $paid_po . "<br>";
echo $paid_nsf_stop . "<br>";
echo $paid_repair . "<br>";
echo $paid_dealer . "<br>";
echo $paid_dealer_fin . "<br>";
echo $paid_legal . "<br>";
echo $paid_bli . "<br>";
echo $paid_ezpay . "<br>";
echo $paid_scoggin . "<br>";
echo $paid_double_d . "<br>";
echo $paid_dallen_legacy . "<br>";
echo '<b>Non-Financed:</b> ' . $paid_non_fin_total . '<br>';
echo '<b>Finanaced:</b> ' . $paid_fin_total . '<br>';
echo '<b>Dealer:</b> ' . $paid_dealer . '<br>';
echo '<b>Total:</b> ' . $paid_total;
echo '<br><b style="color:red;">Legal:</b> ' . $paid_legal;
  
echo '<h3>Grand Total:</h3>';
echo '<b>Grand Total:</b> ' . $gtotal;*/

/*
UPDATE 
`collections` 
SET 
`ck_ca_cc` = replace( replace(`ck_ca_cc`, ',', ''), '"', '' ),
`po` = replace( replace(`po`, ',', ''), '"', '' ),
`nsf_stop` = replace( replace(`nsf_stop`, ',', ''), '"', '' ),
`repair` = replace( replace(`repair`, ',', ''), '"', '' ),
`dealer_pmt` = replace( replace(`dealer_pmt`, ',', ''), '"', '' ),
`dealer_fin` = replace( replace(`dealer_fin`, ',', ''), '"', '' ),
`legal` = replace( replace(`legal`, ',', ''), '"', '' ),
`bli` = replace( replace(`bli`, ',', ''), '"', '' ),
`ezpay` = replace( replace(`ezpay`, ',', ''), '"', '' ),
`scoggin` = replace( replace(`scoggin`, ',', ''), '"', '' ),
`double_d` = replace( replace(`double_d`, ',', ''), '"', '' ),
`dallen_legacy` = replace( replace(`dallen_legacy`, ',', ''), '"', '' );
*/

?>