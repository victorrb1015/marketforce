<?php
include '../php/connection.php';

//Set Organization...
$org_id = $_GET['org_id'];
$_SESSION['org_id'] = $org_id;
include '../php/connection.php';
$iq = "SELECT * FROM `organizations` WHERE `org_id` = '" . $org_id . "'";
$ig = mysqli_query($mf_conn, $iq) or die($mf_conn->error);
$ir = mysqli_fetch_array($ig);
$org_img = $ir['org_logo_url_alt'];
$org_phone = $ir['phone'];

//Load Variables...
$cid = $_GET['cid'];
$letter = $_GET['letter'];
$res = $_GET['res'];
$cn = mysqli_real_escape_string($conn,$_GET['cn']);
$exp = mysqli_real_escape_string($conn,$_GET['exp']);

//Get Customer Name...
$cq = "SELECT * FROM `collections` WHERE `ID` = '" . $cid . "'";
$cg = mysqli_query($conn, $cq) or die($conn->error);
$cr = mysqli_fetch_array($cg);
$cname = $cr['name'];
$inv_num = $cr['inv#'];
$dealer_name = $cr['dname'];

//Set Letter Template...
switch ($letter){
	case 'opl1':
		$lc = 'Dealer Over Collect Payment';
		break;
	case 'ocl1':
		$lc = 'Dealer Over Collect Commission';
		break;
	default:
		$lc = '';
}

//Set Response Text...
switch ($res){
	case '1':
		$response = 'Dealer Response to Over Collect Email: I believe this was an All Steel Mistake. [Explaination: ' . $exp . ']';
		break;
	case '2':
		$response = 'Dealer Response to Over Collect Email: I will mail a check to All Steel Carports [Check#: ' . $cn . ']';
		break;
	case '3':
		$response = 'Dealer Response to Over Collect Email: I will call All Steel Carports and pay with a Credit/Debit Card';
		break;
	case '4':
		$response = 'Dealer Response to Over Collect Email: I want All Steel Carports to call me so I can pay with a Credit/Debit Card';
		break;
	default:
		$response = '';
}

//Log the response as a note on the account...
$nq = "INSERT INTO `collections_calls`
		(
		`date`,
		`customer_id`,
		`name`,
		`rep_id`,
		`rep_name`,
		`type`,
		`note`,
		`inactive`
		)
		VALUES(
		NOW() + INTERVAL 1 HOUR,
		'" . $cid . "',
		'" . $cname . "',
		'999',
		'Market Force',
		'Email',
		'" . $response . "',
		'No'
		)";
mysqli_query($conn, $nq) or die($conn->error);


//Display Thank You Page...
$ty = '<html>
    <head>
      <link rel="stylesheet" href="../css/bootstrap.min.css">
    </head>
    <body>
  <div class="jumbotron text-xs-center" style="text-align:center;height:100%;">
  <img src="' . $org_img . '" style="width:20%;" />
  <h1 class="display-3">Thank You!</h1>
  <p class="lead">Your response has been recorded and sent to All Steel Carports!</p>
  <hr>
  <p>
    Having trouble? <a href="mailto:collections@allsteelcarports.com" style="color:red;">Contact us</a>
  </p>
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="http://www.allsteelcarports.com" role="button">Continue</a>
  </p>
</div>
</body>
</html>';
echo $ty;



//Send an email to All Steel with the response...
include '../php/phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer();
include '../php/phpmailsettings.php';
$mail->setFrom('info@allsteelcarports.com', 'All Steel Carports');
$mail->addAddress('michael@ignition-innovations.com');
$mail->addBCC('michael@allsteelcarports.com');
$mail->Subject = 'Dealer Over Collection Notification!';
include 'dealer-letters/notification-email.php';
$mail->Body = $ntemp;
$mail->send();
//echo $mail->Body;

?>