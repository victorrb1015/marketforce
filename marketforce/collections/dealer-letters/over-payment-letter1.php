<?php
$etemp = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width">
  <!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
  <title></title>
  <!--[if !mso]><!-->
  <!--<![endif]-->
  <style type="text/css">
    body {
      margin: 0;
      padding: 0;
    }

    table,
    td,
    tr {
      vertical-align: top;
      border-collapse: collapse;
    }

    * {
      line-height: inherit;
    }

    a[x-apple-data-detectors=true] {
      color: inherit !important;
      text-decoration: none !important;
    }

    .ie-browser table {
      table-layout: fixed;
    }

    [owa] .img-container div,
    [owa] .img-container button {
      display: block !important;
    }

    [owa] .fullwidth button {
      width: 100% !important;
    }

    [owa] .block-grid .col {
      display: table-cell;
      float: none !important;
      vertical-align: top;
    }

    .ie-browser .block-grid,
    .ie-browser .num12,
    [owa] .num12,
    [owa] .block-grid {
      width: 500px !important;
    }

    .ie-browser .mixed-two-up .num4,
    [owa] .mixed-two-up .num4 {
      width: 164px !important;
    }

    .ie-browser .mixed-two-up .num8,
    [owa] .mixed-two-up .num8 {
      width: 328px !important;
    }

    .ie-browser .block-grid.two-up .col,
    [owa] .block-grid.two-up .col {
      width: 246px !important;
    }

    .ie-browser .block-grid.three-up .col,
    [owa] .block-grid.three-up .col {
      width: 246px !important;
    }

    .ie-browser .block-grid.four-up .col [owa] .block-grid.four-up .col {
      width: 123px !important;
    }

    .ie-browser .block-grid.five-up .col [owa] .block-grid.five-up .col {
      width: 100px !important;
    }

    .ie-browser .block-grid.six-up .col,
    [owa] .block-grid.six-up .col {
      width: 83px !important;
    }

    .ie-browser .block-grid.seven-up .col,
    [owa] .block-grid.seven-up .col {
      width: 71px !important;
    }

    .ie-browser .block-grid.eight-up .col,
    [owa] .block-grid.eight-up .col {
      width: 62px !important;
    }

    .ie-browser .block-grid.nine-up .col,
    [owa] .block-grid.nine-up .col {
      width: 55px !important;
    }

    .ie-browser .block-grid.ten-up .col,
    [owa] .block-grid.ten-up .col {
      width: 60px !important;
    }

    .ie-browser .block-grid.eleven-up .col,
    [owa] .block-grid.eleven-up .col {
      width: 54px !important;
    }

    .ie-browser .block-grid.twelve-up .col,
    [owa] .block-grid.twelve-up .col {
      width: 50px !important;
    }
  </style>
  <style type="text/css" id="media-query">
    @media only screen and (min-width: 520px) {
      .block-grid {
        width: 500px !important;
      }

      .block-grid .col {
        vertical-align: top;
      }

      .block-grid .col.num12 {
        width: 500px !important;
      }

      .block-grid.mixed-two-up .col.num3 {
        width: 123px !important;
      }

      .block-grid.mixed-two-up .col.num4 {
        width: 164px !important;
      }

      .block-grid.mixed-two-up .col.num8 {
        width: 328px !important;
      }

      .block-grid.mixed-two-up .col.num9 {
        width: 369px !important;
      }

      .block-grid.two-up .col {
        width: 250px !important;
      }

      .block-grid.three-up .col {
        width: 166px !important;
      }

      .block-grid.four-up .col {
        width: 125px !important;
      }

      .block-grid.five-up .col {
        width: 100px !important;
      }

      .block-grid.six-up .col {
        width: 83px !important;
      }

      .block-grid.seven-up .col {
        width: 71px !important;
      }

      .block-grid.eight-up .col {
        width: 62px !important;
      }

      .block-grid.nine-up .col {
        width: 55px !important;
      }

      .block-grid.ten-up .col {
        width: 50px !important;
      }

      .block-grid.eleven-up .col {
        width: 45px !important;
      }

      .block-grid.twelve-up .col {
        width: 41px !important;
      }
    }

    @media (max-width: 520px) {

      .block-grid,
      .col {
        min-width: 320px !important;
        max-width: 100% !important;
        display: block !important;
      }

      .block-grid {
        width: 100% !important;
      }

      .col {
        width: 100% !important;
      }

      .col>div {
        margin: 0 auto;
      }

      img.fullwidth,
      img.fullwidthOnMobile {
        max-width: 100% !important;
      }

      .no-stack .col {
        min-width: 0 !important;
        display: table-cell !important;
      }

      .no-stack.two-up .col {
        width: 50% !important;
      }

      .no-stack .col.num4 {
        width: 33% !important;
      }

      .no-stack .col.num8 {
        width: 66% !important;
      }

      .no-stack .col.num4 {
        width: 33% !important;
      }

      .no-stack .col.num3 {
        width: 25% !important;
      }

      .no-stack .col.num6 {
        width: 50% !important;
      }

      .no-stack .col.num9 {
        width: 75% !important;
      }

      .video-block {
        max-width: none !important;
      }

      .mobile_hide {
        min-height: 0px;
        max-height: 0px;
        max-width: 0px;
        display: none;
        overflow: hidden;
        font-size: 0px;
      }

      .desktop_hide {
        display: block !important;
        max-height: none !important;
      }
    }
  </style>
</head>

<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #C5C5C5;">
<style type="text/css" id="media-query-bodytag">
@media (max-width: 520px) {
  .block-grid {
    min-width: 320px!important;
    max-width: 100%!important;
    width: 100%!important;
    display: block!important;
  }
  .col {
    min-width: 320px!important;
    max-width: 100%!important;
    width: 100%!important;
    display: block!important;
  }
  .col > div {
    margin: 0 auto;
  }
  img.fullwidth {
    max-width: 100%!important;
    height: auto!important;
  }
  img.fullwidthOnMobile {
    max-width: 100%!important;
    height: auto!important;
  }
  .no-stack .col {
    min-width: 0!important;
    display: table-cell!important;
  }
  .no-stack.two-up .col {
    width: 50%!important;
  }
  .no-stack.mixed-two-up .col.num4 {
    width: 33%!important;
  }
  .no-stack.mixed-two-up .col.num8 {
    width: 66%!important;
  }
  .no-stack.three-up .col.num4 {
    width: 33%!important
  }
  .no-stack.four-up .col.num3 {
    width: 25%!important
  }
}
</style>
  <!--[if IE]><div class="ie-browser"><![endif]-->
  <table class="nl-container" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #C5C5C5; width: 100%;" cellpadding="0" cellspacing="0" role="presentation" width="100%" bgcolor="#C5C5C5" valign="top">
    <tbody>
      <tr style="vertical-align: top;" valign="top">
        <td style="word-break: break-word; vertical-align: top; border-collapse: collapse;" valign="top">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color:#C5C5C5"><![endif]-->
          <div style="background-color:transparent;">
            <div class="block-grid " style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #D31212;;">
              <div style="border-collapse: collapse;display: table;width: 100%;background-color:#D31212;">
                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:500px"><tr class="layout-full-width" style="background-color:#D31212"><![endif]-->
                <!--[if (mso)|(IE)]><td align="center" width="500" style="background-color:#D31212;width:500px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:15px; padding-bottom:15px;"><![endif]-->
                <div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top;;">
                  <div style="width:100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:15px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">
                      <!--<![endif]-->
                      <div class="img-container center  autowidth " align="center" style="padding-right: 0px;padding-left: 0px;">
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><![endif]--><img class="center  autowidth " align="center" border="0" src="';
                        switch ($_SESSION['org_id']) {
                          case "832122":
                            $etemp .= 'http://marketforceapp.com/assets/img/brands/acero-news.png';
                            break;
                          case "738004":
                            $etemp .= 'http://marketforceapp.com/assets/img/brands/acero-news.png';
                            break;
                          case "654321":
                            $etemp .= 'http://marketforceapp.com/assets/img/brands/acero-news.png';
                            break;
                          case "162534":
                            $etemp .= 'http://marketforceapp.com/assets/img/brands/NorthEdgeSteel-01.png';
                            break;
                          case "615243":
                            $etemp .= 'http://marketforceapp.com/assets/img/brands/legacyMFlogo.png';
                            break;
                          default:
                            $etemp .= 'http://allsteelcarports.com/marketforce/marketforce/img/logo-allsteel-full.png';
                        }
                        $etemp .= '" alt="Logo" title="Logo" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; border: 0; height: auto; float: none; width: 100%; max-width: 171px; display: block;" width="171">
                        <!--[if mso]></td></tr></table><![endif]-->
                      </div>
                      <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
                      <div style="color:#555555;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                        <div style="font-size: 12px; line-height: 14px; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; color: #555555;">
                          <p style="font-size: 14px; line-height: 16px; text-align: center; margin: 0;"><span style="color: #000000; font-size: 14px; line-height: 16px;"><strong><span style="font-size: 22px; line-height: 26px;">Affordable Buildings, Exceptional Quality!</span></strong></span><br></p>
                        </div>
                      </div>
                      <!--[if mso]></td></tr></table><![endif]-->
                      <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 40px; padding-left: 40px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
                      <div style="color:#555555;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;line-height:120%;padding-top:10px;padding-right:40px;padding-bottom:10px;padding-left:40px;">
                        <div style="font-size: 12px; line-height: 14px; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; color: #555555;">
                          <p style="font-size: 14px; line-height: 16px; text-align: center; margin: 0;"><span style="color: #ffffff; font-size: 14px; line-height: 16px;">';
                          switch ($_SESSION['org_id']) {
                            case "162534":
                              $etemp .= 'Northedge Steel';
                              break;
                            case "615243":
                              $etemp .= 'Legacy buildings';
                              break;
                            default:
                              $etemp .= 'All Steel Carports';
                          }
                          $etemp .= ' continues to lead the industry in creating affordable, high quality options for the consumer.</span></p>
                        </div>
                      </div>
                      <!--[if mso]></td></tr></table><![endif]-->
                      <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                  </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
              </div>
            </div>
          </div>
          <div style="background-color:transparent;">
            <div class="block-grid " style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FEFEFE;;">
              <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FEFEFE;">
                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:500px"><tr class="layout-full-width" style="background-color:#FEFEFE"><![endif]-->
                <!--[if (mso)|(IE)]><td align="center" width="500" style="background-color:#FEFEFE;width:500px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
                <div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top;;">
                  <div style="width:100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                      <!--<![endif]-->
                      <table class="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" role="presentation" valign="top">
                        <tbody>
                          <tr style="vertical-align: top;" valign="top">
                            <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; border-collapse: collapse;" valign="top">
                              <table class="divider_content" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 1px solid #BBBBBB;" align="center" role="presentation" valign="top">
                                <tbody>
                                  <tr style="vertical-align: top;" valign="top">
                                    <td style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse;" valign="top"><span></span></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                  </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
              </div>
            </div>
          </div>
          <div style="background-color:transparent;">
            <div class="block-grid " style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FEFEFE;;">
              <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FEFEFE;">
                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:500px"><tr class="layout-full-width" style="background-color:#FEFEFE"><![endif]-->
                <!--[if (mso)|(IE)]><td align="center" width="500" style="background-color:#FEFEFE;width:500px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
                <div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top;;">
                  <div style="width:100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                      <!--<![endif]-->
                      <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
<div style="color:#555555;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;line-height:150%; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">  
  <div style="font-size:12px;line-height:18px;color:#555555;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;text-align:left;">
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">Dear ' . $dname . ',</span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">&#160;</span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">
  We are contacting you about the following customer:
  </span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">&#160;</span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">
  <strong>Customer Name:</strong>&#160;' . $cname . '
  </span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">
  <strong>Invoice#:</strong>&#160;' . $inv . '
  </span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">
  <strong>Purchase Date:</strong> ' . $sdate . '
  </span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">&#160;</span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">
  It appears that the customer has <strong>OVER PAID</strong> the amount owed to you for their purchase.
  </span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">&#160;</span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">
  <strong>Sale Price:</strong>&#160;$' . $sale_price . '
  </span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">
  <strong>Tax Amount:</strong>&#160;$' . $tamount . '
  </span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">
  <strong>Total Sale:</strong>&#160;$' . $subtotal . '
  </span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">&#160;</span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">
  <strong>Amount Paid to you:</strong>&#160;$' . $collected_amount . '
  </span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">
  <strong>Correct Commission:&#160;<u>(' . $crate . '%) $' . $camount . '</u></strong>
  </span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">&#160;</span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">
  <span style="color:red;font-size:16px;"><strong>Balance owed to All Steel from Dealer:&#160;<u>$' . $owed_amount . '</u></strong></span>
  </span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">&#160;</span></p>
  <p style="margin: 0;font-size: 12px;line-height: 18px"><span style="font-size: 14px; line-height: 21px;">
  <strong>CUSTOMER owes All Steel:</strong>&#160;$' . $customer_owes . '
  </span></p>
                          <p style="font-size: 12px; line-height: 21px; margin: 0;"><span style="font-size: 14px;"> </span></p>
                          <p style="font-size: 12px; line-height: 21px; margin: 0;"><span style="font-size: 14px;">Please be advised that we CANNOT PLACE THE CUSTOMER on a schedule UNTIL the total amount owed to All Steel has been received in our office.  You may pay this amount by credit or debit card today or send a check into our office as soon as possible so that we can proceed with the customer\'s order.</span></p>
                        </div>
                      </div>
                      <!--[if mso]></td></tr></table><![endif]-->
                      <table class="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" role="presentation" valign="top">
                        <tbody>
                          <tr style="vertical-align: top;" valign="top">
                            <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; border-collapse: collapse;" valign="top">
                              <table class="divider_content" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 1px dotted #BBBBBB; height: 0px;" align="center" role="presentation" height="0" valign="top">
                                <tbody>
                                  <tr style="vertical-align: top;" valign="top">
                                    <td style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse;" height="0" valign="top"><span></span></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
                      <div style="color:#555555;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                        <div style="font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 12px; line-height: 14px; color: #555555;">
                          <p style="font-size: 14px; line-height: 16px; margin: 0;"><strong>Please select a response option:</strong></p>
                        </div>
                      </div>
                      <!--[if mso]></td></tr></table><![endif]-->
                      <div class="button-container" align="left" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:31.5pt; width:159.75pt; v-text-anchor:middle;" arcsize="10%" stroke="false" fillcolor="#D31212"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#ffffff; font-family:Arial, sans-serif; font-size:14px"><![endif]-->
                        <a href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/collections/add-exp-response.php?cid=' . $id . '&org_id=' . $_SESSION['org_id'] . '&letter=opl1&res=1">
                        <div style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#D31212;border-radius:4px;-webkit-border-radius:4px;-moz-border-radius:4px;width:auto; width:auto;;border-top:1px solid #D31212;border-right:1px solid #D31212;border-bottom:1px solid #D31212;border-left:1px solid #D31212;padding-top:5px;padding-bottom:5px;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;"><span style="padding-left:20px;padding-right:20px;font-size:14px;display:inline-block;">
                            <span style="font-size: 16px; line-height: 32px;"><span style="font-size: 14px;">I believe this is an All Steel mistake</span></span>
                          </span></div></a>
                        <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                      </div>
                      <div class="button-container" align="left" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:31.5pt; width:212.25pt; v-text-anchor:middle;" arcsize="10%" stroke="false" fillcolor="#D31212"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#ffffff; font-family:Arial, sans-serif; font-size:14px"><![endif]-->
                        <a href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/collections/add-cn-response.php?cid=' . $id . '&org_id=' . $_SESSION['org_id'] . '&letter=opl1&res=2">
                        <div style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#D31212;border-radius:4px;-webkit-border-radius:4px;-moz-border-radius:4px;width:auto; width:auto;;border-top:1px solid #D31212;border-right:1px solid #D31212;border-bottom:1px solid #D31212;border-left:1px solid #D31212;padding-top:5px;padding-bottom:5px;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;"><span style="padding-left:20px;padding-right:20px;font-size:14px;display:inline-block;">
                            <span style="font-size: 16px; line-height: 32px;"><span style="font-size: 14px;">I will mail a check to All Steel Carports</span></span>
                          </span></div></a>
                        <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                      </div>
                      <div class="button-container" align="left" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:31.5pt; width:288.75pt; v-text-anchor:middle;" arcsize="10%" stroke="false" fillcolor="#D31212"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#ffffff; font-family:Arial, sans-serif; font-size:14px"><![endif]-->
                        <a href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/collections/record-email-response.php?cid=' . $id . '&org_id=' . $_SESSION['org_id'] . '&letter=opl1&res=3">
                        <div style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#D31212;border-radius:4px;-webkit-border-radius:4px;-moz-border-radius:4px;width:auto; width:auto;;border-top:1px solid #D31212;border-right:1px solid #D31212;border-bottom:1px solid #D31212;border-left:1px solid #D31212;padding-top:5px;padding-bottom:5px;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;"><span style="padding-left:20px;padding-right:20px;font-size:14px;display:inline-block;">
                            <span style="font-size: 16px; line-height: 32px;"><span style="font-size: 14px; line-height: 24px;">I will call All Steel and pay with a credit/debit card</span></span>
                          </span></div></a>
                        <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                      </div>
                      <div class="button-container" align="left" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:31.5pt; width:319.5pt; v-text-anchor:middle;" arcsize="10%" stroke="false" fillcolor="#D31212"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#ffffff; font-family:Arial, sans-serif; font-size:14px"><![endif]-->
                        <!--<a href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/collections/record-email-response.php?cid=' . $id . '&org_id=' . $_SESSION['org_id'] . '&letter=opl1&res=2">
                        <div style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#D31212;border-radius:4px;-webkit-border-radius:4px;-moz-border-radius:4px;width:auto; width:auto;;border-top:1px solid #D31212;border-right:1px solid #D31212;border-bottom:1px solid #D31212;border-left:1px solid #D31212;padding-top:5px;padding-bottom:5px;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;"><span style="padding-left:20px;padding-right:20px;font-size:14px;display:inline-block;">
                            <span style="font-size: 16px; line-height: 32px;"><span style="font-size: 14px; line-height: 28px;">I want All Steel to call me so I can pay with a debit/credit card</span></span>
                          </span></div></a>-->
                        <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                      </div>
                      <table class="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" role="presentation" valign="top">
                        <tbody>
                          <tr style="vertical-align: top;" valign="top">
                            <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; border-collapse: collapse;" valign="top">
                              <table class="divider_content" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 1px dotted #BBBBBB; height: 0px;" align="center" role="presentation" height="0" valign="top">
                                <tbody>
                                  <tr style="vertical-align: top;" valign="top">
                                    <td style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse;" height="0" valign="top"><span></span></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
                      <div style="color:#555555;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                        <div style="font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 12px; line-height: 14px; color: #555555;">
                          <p style="font-size: 12px; line-height: 14px; margin: 0;">Thank you!</p>
                          <p style="font-size: 12px; line-height: 14px; margin: 0;">Collections Department</p>
                          <p style="font-size: 12px; line-height: 14px; margin: 0;">
                          ';
switch ($_SESSION['org_id']) {
  case "162534":
    $etemp .= 'Northedge Steel';
    break;
  case "615243":
    $etemp .= 'Legacy buildings';
    break;
  default:
    $etemp .= 'All Steel Carports';
}
$etemp .= '</p>
                          <p style="font-size: 12px; line-height: 14px; margin: 0;">';
                          switch ($_SESSION['org_id']) {
                            case "162534":
                              $etemp .= '765 591 8080 ';
                              break;
                            case "615243":
                              $etemp .= '918 910 8010';
                              break;
                            default:
                              $etemp .= '765 284 0694 x 104';
                          }
                          $etemp .= '</p>
                        </div>
                      </div>
                      <!--[if mso]></td></tr></table><![endif]-->
                      <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                  </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
              </div>
            </div>
          </div>
          <div style="background-color:transparent;">
            <div class="block-grid " style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #D31212;;">
              <div style="border-collapse: collapse;display: table;width: 100%;background-color:#D31212;">
                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:500px"><tr class="layout-full-width" style="background-color:#D31212"><![endif]-->
                <!--[if (mso)|(IE)]><td align="center" width="500" style="background-color:#D31212;width:500px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:15px; padding-bottom:15px;"><![endif]-->
                <div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top;;">
                  <div style="width:100% !important;">
                    <!--[if (!mso)&(!IE)]><!-->
                    <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:15px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">
                      <!--<![endif]-->
                      <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
                      <div style="color:#FFFFFF;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                        <div style="font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 12px; line-height: 14px; color: #FFFFFF;">
                          <p style="font-size: 12px; line-height: 14px; margin: 0;"><br></p>
                        </div>
                      </div>
                      <!--[if mso]></td></tr></table><![endif]-->
                      <!--[if (!mso)&(!IE)]><!-->
                    </div>
                    <!--<![endif]-->
                  </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
              </div>
            </div>
          </div>
          <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
        </td>
      </tr>
    </tbody>
  </table>
  <!--[if (IE)]></div><![endif]-->
</body></html>';
if($_GET['view_email'] == 'YES'){
  echo $etemp;
}
?>