<?php
include '../php/connection.php';

if($_SESSION['collections'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include '../php/connection.php';

//Load Variable
$CID = $_GET['cid'];


$ciq = "SELECT * FROM `collections` WHERE `ID` = '" . $CID . "'";
$cig = mysqli_query($conn, $ciq) or die($conn->error);
$cir = mysqli_fetch_array($cig);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="../js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	<link href="http://burtonsolution.tech/plugins/print-friendly.css" rel="stylesheet" type="text/css">
  
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
		@media print {
        footer {
					page-break-after: always;
				}
				#report{
					margin-top: 50px;
				}
      }
    .badge:hover{
      cursor: pointer;
    }
		th, td{
			padding: 10px;
		}
		#dl, dc{
			height: 430px;
		}
		.panel-body{
			height: 382px;
		}
		#scroll{
			overflow: scroll;
		}
		::-webkit-scrollbar {
    width: 0px;  /* remove scrollbar space */
    background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
		
		
		.packet-table td, th{
			border: 1px solid black;
		}
		.packet-table{
			margin:auto;
		}
    
    #hoverGreen{
      color:blue;
    }
    #hoverGreen:hover{
      color:green;
    }
    
    .page-header>img{
      height:auto !important;
      width:auto !important;
    }
  </style>
  <script>
    var rep_id = '<?php echo $_SESSION['user_id']; ?>';
    var rep_name = '<?php echo $_SESSION['full_name']; ?>';
    
  function clr_res(id,s){
    //alert("test");
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			//alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://library.burtonsolution.tech/php/reserve.php?id="+id+"&s="+s,true);
  xmlhttp.send();
  }
		

		
  </script>
</head>

<body>

    <div id="wrapper" style="padding-left:0px;">

        <!-- Navigation -->
       <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
											<?php
											if($_GET['remote'] == 'yes'){
											 	echo '<button type="button" class="btn btn-success" onclick="window.close();"><i class="fa fa-arrow-left"></i> Go Back</p></button>';
                       }else{
                      echo '<a href="../collections.php" class="no-print"><button type="button" class="btn btn-success"><i class="fa fa-arrow-left"></i> Go Back</p></button></a>';
											 }
											?>
											   <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="../img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> <?php echo $cir['name']; ?>'s Collection Report</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
														<li>
															<i class="fa fa-bar-chart"></i> <?php echo $cir['name']; ?>'s Collection Report
													</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							
							<div class="row">
								
                <div class="col-lg-4">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												<?php echo $cir['name']; ?>'s Carport (Feature in Beta)
											</h3>
										</div>
										<div class="panel-body">
											<img src="carp-img.jpg" style="width:100%;height:100%;" />
										</div>
									</div>
								</div>
                
								<!--Graphical Report-->
								<div class="col-lg-4">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Collection Attempts
											</h3>
										</div>
										<div class="panel-body" id="pvup">
											
										</div>
									</div>
								</div>
								<!--END Graphical Report-->
								
								
								<!--Graphical Report-->
								<div class="col-lg-4">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Debt Breakdown
											</h3>
										</div>
										<div class="panel-body" id="p-np">
											
										</div>
									</div>
								</div>
								<!--END Graphical Report-->
								
								
							</div><!--END ROW-->
							<footer></footer>
							
							<div class="row" id="report">
								<div class="col-lg-12">
									<div class="panel panel-red">
										<div class="panel-heading">
											<h3 class="panel-title">
											 <?php echo $cir['name']; ?>'s Collection History:
											</h3>
										</div>
										<div class="panel-body" style="height:100%;">
											<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
                        <th>Date</th>
                        <th>Name</th>
                        <th>State</th>
                        <th>Inv#</th>
                        <th>Category</th>
                        <th>Amount</th>
                        <th>Assigned Rep</th>
                        <th>Notes</th>
                      </thead>
							        <tbody>
                        
                <?php
                  
                
                        
                  $i = 0;
                  if($cir['ck_ca_cc'] != ''){
                    $cat = 'ck_ca_cc';
                    $i++;
                  }
                  if($cir['po'] != ''){
                    $cat = 'po';
                    $i++;
                  }
                  if($cir['nsf_stop'] != ''){
                    $cat = 'nsf_stop';
                    $i++;
                  }
                  if($cir['repair'] != ''){
                    $cat = 'repair';
                    $i++;
                  }
                  if($cir['dealer_pmt'] != ''){
                    $cat = 'dealer_pmt';
                    $i++;
                  }
                  if($cir['dealer_fin'] != ''){
                    $cat = 'dealer_fin';
                    $i++;
                  }
                  if($cir['legal'] != ''){
                    $cat = 'legal';
                    $i++;
                  }
                  if($cir['bli'] != ''){
                    $cat = 'bli';
                    $i++;
                  }
                  if($cir['ezpay'] != ''){
                    $cat = 'ezpay';
                    $i++;
                  }
                  if($cir['scoggin'] != ''){
                    $cat = 'scoggin';
                    $i++;
                  }
                  if($cir['double_d'] != ''){
                    $cat = 'double_d';
                    $i++;
                  }
                  if($cir['dallen_legacy'] != ''){
                    $cat = 'dallen_legacy';
                    $i++;
                  }
									if($cir['rto_national'] != ''){
										$cat = 'rto_national';
										$i++;
									}
                  
                  
                  echo '<tr>
                        <td>' . date("m/d/y", strtotime($cir['date'])) . '</td>
                        <td>' . $cir['name'] . '</td>
                        <td>' . $cir['state'] . '</td>
                        <td>' . $cir['inv#'] . '</td>
                        <td>' . $cat . '</td>
                        <td>' . number_format($cir[$cat],2) . '</td>
                        <td>' . $cir['assigned_rep_name'] . '</td>
                        <td>';
												
									
									
									$collnq = "SELECT * FROM `collections_calls`
														WHERE
														`inactive` != 'Yes' AND `customer_id` = '" . $cir['ID'] . "'
														ORDER BY `date` DESC";
									
									$collng = mysqli_query($conn, $collnq) or die($conn->error);
									
                    $calls = 0;
                    $notes = 0;
                    $emails = 0;
                    $texts = 0;
										$payments = 0;
									while($collnr = mysqli_fetch_array($collng)){
										
										if($collnr['type'] == 'Note'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														' . $collnr['rep_name'] . ' ' . date("m/d/y h:m A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . mysqli_real_escape_string($conn,$collnr['note']) . '
													</div>';
                      $notes++;
										}
										if($collnr['type'] == 'Phone Call'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														' . $collnr['rep_name'] . ' ' . date("m/d/y h:m A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . $collnr['rep_name'] . ' attempted calling ' . $collnr['name'] . ' @ ' . $collnr['phone_tried'] . '. ' . $collnr['note'] . '
													</div>';
                      $calls++;
										}
                    if($collnr['type'] == 'Email'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														' . $collnr['rep_name'] . ' ' . date("m/d/y h:m A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . $collnr['rep_name'] . ' Automatically Emailed ' . $collnr['name'] . '.
													</div>';
                      $emails++;
										}
                    if($collnr['type'] == 'Text'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														' . $collnr['rep_name'] . ' ' . date("m/d/y h:m A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . $collnr['rep_name'] . ' Automatically SMS Texted ' . $collnr['name'] . '.
													</div>';
                      $texts++;
										}
										if($collnr['type'] == 'Payment'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-usd"></i> ' . $collnr['rep_name'] . ' ' . date("m/d/y h:m A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . $collnr['rep_name'] . ' recorded a ' . $collnr['method'] . ' payment for ' . $collnr['name'] . ' in the amount of $' . $collnr['amount'] . '.';
											if($collnr['method'] == 'Check'){
												echo '<br>Check#: ' . $collnr['check_num'];
											}
											if($collnr['method'] == 'Credit Card'){
												echo '<br>Confirmation#: ' . $collnr['cc_conf'];
											}
													echo '</div>';
                      $payments++;
										}
									}
									if($cir['notes'] != ''){
										echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														Initial Note 
													</span></h4>
													<div class="well well-sm">
														' . $cir['notes'] . '
													</div><br>';
									}
									
									echo '</td>
                        </tr>';
                  
                
                        
                        
                        
                ?>
                        
							        </tbody>
										</table>
												<?php 
												if($cir['file_url'] != ''){
												echo '<br><br>
												<h1 style="text-align:center;"><u>Scanned File</u></h1>
												<iframe src="' . $cir['file_url'] . '" style="width:100%;height:1200px;"></iframe>';
												}
												?>
										</div>
										</div>
									</div>
								</div>
							</div><!--End Row-->
							
							
							
							</div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


    <!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../js/plugins/morris/raphael.min.js"></script>
    <script src="../js/plugins/morris/morris.min.js"></script>

  <?php 
$pay_made = ($cir['initial_debt'] - $cir[$cat]);
  include 'calc-total.php'; 
  
  	echo '<script>
				//alert("' . $dain . '");
				</script>';
	
									echo'<script>
												// Bar Chart
  										  Morris.Bar({
  										      element: "pvup",
  										      data: [{
                                device: "Calls Made",
                                geekbench: ' . $calls . '
                            }, {
                                device: "Notes Added",
                                geekbench: ' . $notes . '
                            }, {
                                device: "Emails Sent",
                                geekbench: ' . $emails . '
                            }, {
                                device: "Texts Sent",
                                geekbench: ' . $texts . '
                            }, {
                                device: "Payment Made",
                                geekbench: ' . $payments . '
                            }],
                            xkey: "device",
                            ykeys: ["geekbench"],
                            labels: ["Grade"],
                            barRatio: 0.4,
                            xLabelAngle: 35,
                            hideHover: "auto",
                            barColors: ["blue"],
  										      resize: true
  										  });
                        
												
												
												// Donut Chart
  										  Morris.Donut({
  										      element: "p-np",
  										      data: [{
  										          label: "Outstanding Debt",
  										          value: ' . $cir[$cat] . ',
                                color: "red"
  										      }, {
  										          label: "Payments Made",
  										          value: ' . $pay_made . ',
                                color: "green"
  										      }],
  										      resize: true
  										  });
												</script>';


  
  ?>

	
 <?php include '../footer.html'; ?>
<footer></footer>
</body>

</html>