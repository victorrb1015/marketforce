<?php
include '../php/connection.php';

//Set Organization...
$org_id = $_GET['org_id'];
$_SESSION['org_id'] = $org_id;
include '../php/connection.php';
$iq = "SELECT * FROM `organizations` WHERE `org_id` = '" . $org_id . "'";
$ig = mysqli_query($mf_conn, $iq) or die($mf_conn->error);
$ir = mysqli_fetch_array($ig);
$org_img = $ir['org_logo_url_alt'];

//Get Query String...
$qs = $_SERVER['QUERY_STRING'];

?>
<html>
    <head>
      <link rel="stylesheet" href="../css/bootstrap.min.css">
      
	  	<script>
	  	<?php echo 'var qs = "' . $qs . '";'; ?>

	  	function submit_cn(){
	  		var cn = document.getElementById('check_number').value;
	  		if(cn === ''){
	  			document.getElementById('error_msg').innerHTML = 'Please Enter The Check Number';
	  			return;
	  		}
	  		window.location = "http://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/collections/record-email-response.php?"+qs+"&cn="+cn;
	  	}
	  	</script>
    </head>
    <body>
  <div class="jumbotron text-xs-center" style="text-align:center;height:100%;">
  <img src="<?php echo $org_img; ?>" style="width:20%;" />
  <h1 class="display-3">Please Enter The Check Number:</h1>
  <p class="lead">
  	Our Main Office Address: <strong><?php echo $ir['org_address'] . ', ' . $ir['org_city'] . ', ' . $ir['org_state'] . ' ' . $ir['org_zip']; ?></strong>
  	<br><br>
  	<input type="text" id="check_number" name="check_number" class="form-control" placeholder="Check Number Here..." style="width:25%;margin:auto;"/></p>
  <hr>
  <p>
    Having trouble? <a href="mailto:collections@allsteelcarports.com" style="color:red;">Contact us</a>
  </p>
  <p class="lead">
    <a class="btn btn-success btn-lg" role="button" onclick="submit_cn();">Submit</a>
  </p>
</div>
</body>
</html>


