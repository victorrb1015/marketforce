  <script>
    
		var rep_id = '<?php echo $_SESSION['user_id']; ?>';
		var rep_name = '<?php echo $_SESSION['full_name']; ?>';
		
		function new_packet(id,x){
			if(x === 1){
				var stat = 'Completed';
				var note = prompt("Enter any notes or comments");
			}
			if(x === 2){
				var stat = 'Need Info';
				var note = prompt("What additional information do you need?");
			}
			
			function nprompt(){
				var note = prompt("What additional information do you need?");
			}
			
			if(x === 2 && note === null){
				alert('You must specify what information you need!');
				return;
			}
			
			if(x === 2 && note === ''){
				alert('You must specify what information you need!');
				return;
			}
			
			if(x === 1 && note === null){
				return;
			}
			
			if(x === 3){
				var stat = 'Needs Printing';
				var note = '';
			}
			
			
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			alert(this.responseText);
      //window.location.reload();
      
    }
  }
  xmlhttp.open("GET","php/new-packet-handler.php?id="+id+"&stat="+stat+"&note="+note,true);
  xmlhttp.send();
		}
		
		
		
		
		function check_request(id,x){
			var cnum = '';
			var camount = '';
			var mdate = '';
			
			if(x === 1){
				var id = document.getElementById('completed_id').value;
				var stat = 'Completed';
				var cnum = document.getElementById('cnum').value;
				if(cnum === '' || cnum === null){
					document.getElementById('completed_error_msg').innerHTML = "You must enter a check number to complete this request!";
					return;
				}
				cnum = encodeURL(cnum);
				
				var camount = document.getElementById('camount').value;
				if(camount === '' || camount === null){
					document.getElementById('completed_error_msg').innerHTML = "You must enter the check amount to complete this request!";
					return;
				}
				camount = encodeURL(camount);
				
				var mdate = document.getElementById('mdate').value;
				if(mdate === '' || mdate === null){
					document.getElementById('completed_error_msg').innerHTML = "You must enter the date the check was mailed to complete this request!";
					return;
				}
				mdate = encodeURL(mdate);
				var note = document.getElementById('completed_notes').value;
				note = encodeURL(note);
			}
			$("#completeCheck").modal("hide");
			
			if(x === 2){
				var id = document.getElementById('info_id').value;
				var stat = 'Need Info';
				var note = document.getElementById('add_info').value;
				if(note === ''){
					document.getElementById('info_error_msg').innerHTML = 'You must specify what additional information you require!';
					return;
				}
				note = encodeURL(note);
			}
			$("#checkNeedInfo").modal("hide");
			
			
			if(x === 1 && note === null){
				return;
			}
			
			
			if(x === 3){
				var stat = 'Cancelled';
				var note = 'This Request was cancelled by <?php echo $_SESSION['full_name']; ?>';
			}
			
			var note = note.replace('&','%26');
			var note = note.replace('#','%23');
			
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","php/check-request-handler.php?"+
							 "id="+id+
							 "&stat="+stat+
							 "&note="+note+
							 "&cnum="+cnum+
							 "&camount="+camount+
							 "&mdate="+mdate,true);
  xmlhttp.send();
		}
		
		
		function load_modal(id,name,mode){
			if(mode === 'Completed'){
				document.getElementById('completed_id').value = id;
				document.getElementById('ccrName').innerHTML = name;
			}
			if(mode === 'Need Info'){
				document.getElementById('info_id').value = id;
				document.getElementById('info_name').innerHTML = name;
			}
			if(mode === 'collection payment'){
				document.getElementById('approve_id').value = id;
			}
			if(mode === 'Task Note'){
				document.getElementById('taskNote_id').value = id;
			}
		}
		
		
		function encodeURL(url){
		url = url.replace(/&/g, '%26'); 
		url = url.replace(/#/g, '%23');
		return url;
	}
		
		
		function seen(alert,vid){
			var id = '<?php echo $_SESSION['user_id']; ?>';
			
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      $("#alert1").modal("hide");
      $("#alert2").modal("hide");
      $("#alert3").modal("hide");
			if(vid === 'Yes'){
			pause_vid();
			}
      window.location.reload();
    }
  }
  xmlhttp.open("GET","php/seen.php?id="+id+"&note="+alert,true);
  xmlhttp.send();
		}
    
    
    function submit_nc_form(){
  var user = '<?php echo $_SESSION['full_name']; ?>';
  var user_id = '<?php echo $_SESSION['user_id']; ?>';
  var amount = document.getElementById('ncamount').value;
  var to = document.getElementById('payto').value;
  var address = document.getElementById('ncaddress').value;
  var apt = document.getElementById('ncapt').value;
  var city = document.getElementById('nccity').value;
  var state = document.getElementById('ncstate').value;
  var zip = document.getElementById('nczip').value;
  var phone = document.getElementById('ncphone').value;
	var inv = document.getElementById('invnum').value;
  var ta1 = document.getElementById('ncta1').value;
  var ta2 = document.getElementById('ncta2').value;
  var ta3 = document.getElementById('ncta3').value;
  var ta4 = document.getElementById('ncta4').value;
	
	//Required Fields...
	if(user.value === '' || user_id.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'An error has occured processing this request! Please contact your system administrator!';
		return;
	}
	if(amount.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please enter the amount requested!';
		return;
	}
   amount = encodeURL(amount);
	if(to.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please enter the name of the payee!';
		return;
	}
    to = encodeURL(to);
	if(address.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please enter the payee address!';
		return;
	}
    address = encodeURL(address);
	if(city.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please enter the city!';
		return;
	}
   city = encodeURL(city);
	if(zip.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please enter the zip code!';
		return;
	}
   zip = encodeURL(zip);
	if(phone.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please enter the payee phone number!';
		return;
	}
   phone = encodeURL(phone);
	if(ta1.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please ensure all questions are answered!';
		return;
	}
   ta1 = encodeURL(ta1);
	if(ta2.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please ensure all questions are answered!';
		return;
	}
   ta2 = encodeURL(ta2);
	if(ta3.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please ensure all questions are answered!';
		return;
	}
   ta3 = encodeURL(ta3);
	if(ta4.value === ''){
		document.getElementById('nc_error_msg').innerHTML = 'Please ensure all questions are answered!';
		return;
	}
   ta4 = encodeURL(ta4);
  
      $("#newCheckRequest").modal("hide");
      

	
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","accounting/php/submit-check-request-form.php?"+
               "user="+user+
               "&user_id="+user_id+
               "&amount="+amount+
               "&to="+to+
               "&address="+address+
               "&apt="+apt+
               "&city="+city+
               "&state="+state+
               "&zip="+zip+
               "&phone="+phone+
							 "&inv="+inv+
               "&ta1="+ta1+
               "&ta2="+ta2+
               //"&test=True"+//Comment This line out when not in test mode (turns off email notifications only)
               "&ta3="+ta3+
               "&ta4="+ta4,true);
  xmlhttp.send();
  }
		
		
		
	function c_payment(mode,id){
		
		if(mode === 'approve'){
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","collections/php/c-approval.php?mode="+mode+"&id="+id,true);
  xmlhttp.send();
		}
		
		
		if(mode === 'decline'){
			var id = document.getElementById('approve_id').value;
			var note = document.getElementById('approve_note').value;
			if(note === ''){
				document.getElementById('approve_error_msg').innerHTML = 'Please Enter A Note About The Missing Payment!';
			}
			note = encodeURL(note);
			$("#notReceived").modal("hide");
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","collections/php/c-approval.php?mode="+mode+"&note="+note+"&id="+id,true);
  xmlhttp.send();
		}
	}
		
		
	function pause_vid(){
		var vid = document.getElementById("alert-vid"); 
    vid.pause(); 
	}
	function play_vid(){
		var vid = document.getElementById("alert-vid"); 
		vid.play();
	}
		
		
		

		
		
	
	function taskNote(){
		var tid = document.getElementById('taskNote_id').value;
		var note = document.getElementById('task_note').value;
		if(note === ''){
			document.getElementById('task_note_error').innerHTML = 'Please Enter Your Message!';
			return;
		}
		note = encodeURL(note);
		var rep_id = '<?php echo $_SESSION['user_id']; ?>';
		var rep_name = '<?php echo $_SESSION['full_name']; ?>';
		if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			//alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","tasks/add-task-note.php?rep_id="+rep_id+"&rep_name="+rep_name+"&note="+note+"&tid="+tid,true);
  xmlhttp.send();
	}
		
		
		
	function taskDone(id){
		if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","tasks/complete-task.php?rep_id="+rep_id+"&rep_name="+rep_name+"&tid="+id,true);
  xmlhttp.send();
	}
		
		
	function mark_installed(id){
		var rep_id = '<?php echo $_SESSION['user_id']; ?>';
		var rep_name = '<?php echo $_SESSION['full_name']; ?>';
		if (window.XMLHttpRequest) {
  		  // code for IE7+, Firefox, Chrome, Opera, Safari
  		  xmlhttp=new XMLHttpRequest();
  		} else {  // code for IE6, IE5
  		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  		}
  		xmlhttp.onreadystatechange=function() {
  		  if (this.readyState==4 && this.status==200) {
					alert(this.responseText);
  		    window.location.reload();
  		  }
  		}
  		xmlhttp.open("GET","php/mark-installed.php?rep_id="+rep_id+"&rep_name="+rep_name+"&id="+id,true);
  		xmlhttp.send();
  	}
</script>