function fetch_checklist(id){
  document.getElementById('addon_comps').style.display = 'inline';
  document.getElementById('new_dealer_comps').style.display = 'inline';
  document.getElementById('myCLForm').reset();
  document.getElementById('checklist_error').innerHTML = '';
  document.getElementById('cl_id').value = id;
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById('cl_form_type').value = r.form_type;
        if(r.form_type === 'New Dealer Packet'){
          document.getElementById('addon_comps').style.display = 'none';
          if(r.email_cb === 'on'){
            document.getElementById('email_cb').checked = true;
          }
          if(r.ein_cb === 'on'){
            document.getElementById('ein_cb').checked = true;
          }
          if(r.contract_form_cb === 'on'){
            document.getElementById('contract_form_cb').checked = true;
          }
          if(r.consent_form_cb === 'on'){
            document.getElementById('consent_form_cb').checked = true;
          }
          if(r.site_plan_cb === 'on'){
            document.getElementById('site_plan_cb').checked = true;
          }
          if(r.roofs_cb === 'on'){
            document.getElementById('roofs_cb').checked = true;
          }
          if(r.display_order_cb === 'on'){
            document.getElementById('display_order_cb').checked = true;
          }
          if(r.congrats_letter_cb === 'on'){
            document.getElementById('congrats_letter_cb').checked = true;
          }
          
        }else{
          
          document.getElementById('new_dealer_comps').style.display = 'none';
          document.getElementById('cl_start_date').value = r.start_date;
          document.getElementById('cl_total_sales').value = r.total_sales;
          document.getElementById('cl_current_sales').value = r.current_sales;
          if(r.consent_verify === 'on'){
            document.getElementById('consent_verify_cb').checked = true;
          }
          if(r.contract_verify === 'on'){
            document.getElementById('contract_verify_cb').checked = true;
          }
          
        }
        document.getElementById('notes_box').value = r.notes_box;
        console.log('Fetch Successful...');
        
      }else{
        console.warn("Fetch Error: "+r.message);
      }
    }
  }
  xmlhttp.open("GET","dashboard/php/fetch-checklist.php?id="+id,true);
  xmlhttp.send();
}


function save_checklist(mode){
  var email_cb = '';
  var ein_cb = '';
  var contract_form_cb = '';
  var consent_form_cb = '';
  var site_plan_cb = '';
  var roofs_cb = '';
  var display_order_cb = '';
  var congrats_letter_cb = '';
  var notes_box = '';
  var trip = false;
  var start_date = '';
  var total_sales = '';
  var current_sales = '';
  var consent_verify_cb = '';
  var contract_verify_cb = '';
  var ver_trip = false;
  
  var form_type = document.getElementById('cl_form_type').value;
  
  if(form_type === 'New Dealer Packet'){
  if(document.getElementById('email_cb').checked === true){
    email_cb = 'on';
  }else{
    trip = true;
  }
  if(document.getElementById('ein_cb').checked === true){
    ein_cb = 'on';
  }else{
    trip = true;
  }
  if(document.getElementById('contract_form_cb').checked === true){
    contract_form_cb = 'on';
    //alert('Test! - '+contract_form_cb);
    //return;
  }else{
    trip = true;
  }
  if(document.getElementById('consent_form_cb').checked === true){
    consent_form_cb = 'on';
  }else{
    trip = true;
  }
  if(document.getElementById('site_plan_cb').checked === true){
    site_plan_cb = 'on';
  }else{
    trip = true;
  }
  if(document.getElementById('roofs_cb').checked === true){
    roofs_cb = 'on';
  }else{
    trip = true;
  }
  if(document.getElementById('display_order_cb').checked === true){
    display_order_cb = 'on';
  }else{
    trip = true;
  }
  if(document.getElementById('congrats_letter_cb').checked === true){
    congrats_letter_cb = 'on';
  }else{
    trip = true;
  }
  
  if(mode === 'submit' && trip === true){
    document.getElementById('checklist_error').innerHTML = 'All Items must be checked in order to submit!';
    return;
  }
  
  }else{
    
    start_date = document.getElementById('cl_start_date').value;
    if(mode === 'submit' && start_date === ''){
      document.getElementById('checklist_error').innerHTML = 'Please Enter A Start Date!';
      return;
    }
    total_sales = document.getElementById('cl_total_sales').value;
    if(mode === 'submit' && total_sales === ''){
      document.getElementById('checklist_error').innerHTML = 'Please Enter Total Sales!';
      return;
    }
    current_sales = document.getElementById('cl_current_sales').value;
    if(mode === 'submit' && current_sales === ''){
      document.getElementById('checklist_error').innerHTML = 'Please Enter Current Sales!';
      return;
    }
    total_sales = document.getElementById('cl_total_sales').value;
    if(mode === 'submit' && total_sales === ''){
      document.getElementById('checklist_error').innerHTML = 'Please Enter Total Sales!';
      return;
    }
    if(document.getElementById('consent_verify_cb').checked === true){
      consent_verify_cb = 'on';
    }else{
      ver_trip = true;
    }
    if(document.getElementById('contract_verify_cb').checked === true){
      contract_verify_cb = 'on';
    }else{
      ver_trip = true;
    }
    
    if(mode === 'submit' && ver_trip === true){
      document.getElementById('checklist_error').innerHTML = 'All Items must be checked in order to submit!';
      return;
    }
    
  }
  notes_box = document.getElementById('notes_box').value;
  var id = document.getElementById('cl_id').value;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      alert(this.responseText);
      window.location.reload();
    }
  }
  xmlhttp.open("GET","dashboard/php/save-checklist.php?id="+id+
               "&mode="+mode+
               "&email_cb="+email_cb+
               "&ein_cb="+ein_cb+
               "&contract_form_cb="+contract_form_cb+
               "&consent_form_cb="+consent_form_cb+
               "&site_plan_cb="+site_plan_cb+
               "&roofs_cb="+roofs_cb+
               "&display_order_cb="+display_order_cb+
               "&congrats_letter_cb="+congrats_letter_cb+
               "&start_date="+start_date+
               "&total_sales="+total_sales+
               "&current_sales="+current_sales+
               "&consent_verify_cb="+consent_verify_cb+
               "&contract_verify_cb="+contract_verify_cb+
               "&notes_box="+notes_box,true);
  xmlhttp.send();
}