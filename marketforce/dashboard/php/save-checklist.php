<?php
include '../../php/connection.php';

//Load variables...
$mode = $_GET['mode'];
$id = $_GET['id'];
$email = $_GET['email_cb'];
$ein = $_GET['ein_cb'];
$contract = $_GET['contract_form_cb'];
$consent = $_GET['consent_form_cb'];
$site_plan = $_GET['site_plan_cb'];
$roofs = $_GET['roofs_cb'];
$display_order = $_GET['display_order_cb'];
$congrats = $_GET['congrats_letter_cb'];
$start_date = date("Y-m-d",strtotime($_GET['start_date']));
$total_sales = mysqli_real_escape_string($conn,$_GET['total_sales']);
$current_sales = mysqli_real_escape_string($conn,$_GET['current_sales']);
$consent_verify_cb = $_GET['consent_verify_cb'];
$contract_verify_cb = $_GET['contract_verify_cb'];
$notes = mysqli_real_escape_string($conn, $_GET['notes_box']);

//Process Form...
$uq = "UPDATE `new_packets` SET 
        `cl_email` = '" . $email . "',
        `cl_ein` = '" . $ein . "',
        `cl_contract` = '" . $contract . "',
        `cl_consent` = '" . $consent . "',
        `cl_site_plan` = '" . $site_plan . "',
        `cl_roofs` = '" . $roofs . "',
        `cl_display_order` = '" . $display_order . "',
        `cl_congrats` = '" . $congrats . "',
        `cl_start_date` = '" . $start_date . "',
        `cl_total_sales` = '" . $total_sales . "',
        `cl_current_sales` = '" . $current_sales . "',
        `cl_consent_verify` = '" . $consent_verify_cb . "',
        `cl_contract_verify` = '" . $contract_verify_cb . "',
        `cl_notes` = '" . $notes . "'";
if($mode == 'submit'){
      $uq .= ", 
        `cl_submitted_by_id` = '" . $_SESSION['user_id'] . "',
        `cl_submitted_date` = CURRENT_DATE";
}
    $uq .= " WHERE `ID` = '" . $id . "'";
mysqli_query($conn, $uq) or die($conn->error);


$q = "SELECT * FROM `new_packets` WHERE `ID` = '" . $id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
$dealer_form_type = $r['form_type'];
  
$dq = "SELECT * FROM `dealers` WHERE `ID` = '" . $r['dealer_id'] . "'";
$dg = mysqli_query($conn, $dq) or die($conn->error);
$dr = mysqli_fetch_array($dg);
$dealer_name = $dr['name'];





include '../../php/phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include '../../php/phpmailsettings.php';
$mail->setFrom('no-reply@marketforceapp.com','MarketForce');
$mail->addAddress('jeffrey@allsteelcarports.com');
$mail->addBCC('archive@ignition-innovations.com');
$mail->Subject = 'New Dealer Form Request!';
include '../email/templates/new-packet-email.php';
$mail->Body = $etemp;

if($mode == 'submit'){
  if($mail->send()){
    echo 'Checklist Has Been Submitted!';
  }else{
    echo 'ERROR: ' . $mail->ErrorInfo;
  }
}else{
  echo 'Checklist Info Has Been Saved!';
}

?>