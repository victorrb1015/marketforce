<?php
include '../../php/connection.php';

//Load variables...
$id = $_GET['id'];


//Fetch Checklist Data...
$q = "SELECT * FROM `new_packets` WHERE `ID` = '" . $id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);

$x->response = 'GOOD';
$x->message = 'Fetch Successful!';
$x->form_type = $r['form_type'];
$x->email_cb = $r['cl_email'];
$x->ein_cb = $r['cl_ein'];
$x->contract_form_cb = $r['cl_contract'];
$x->consent_form_cb = $r['cl_consent'];
$x->site_plan_cb = $r['cl_site_plan'];
$x->roofs_cb = $r['cl_roofs'];
$x->display_order_cb = $r['cl_display_order'];
$x->congrats_letter_cb = $r['cl_congrats'];
$x->start_date = date("m/d/Y",strtotime($r['cl_start_date']));
$x->total_sales = $r['cl_total_sales'];
$x->current_sales = $r['cl_current_sales'];
$x->consent_verify = $r['cl_consent_verify'];
$x->contract_verify = $r['cl_contract_verify'];
$x->notes_box = $r['cl_notes'];

$response = json_encode($x);
echo $response;

?>