    <div class="modal fade" role="dialog" tabindex="-1" id="packetChecklist">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Checklist</h4>
                </div>
                <div class="modal-body">
                  <form id="myCLForm">
                    <div id="new_dealer_comps">
                    <h3>New Dealer Checklist:</h3>
                  <table>
                    <tr>
                      <td>Email Verified</td>
                      <td><input type="checkbox" style="width:20px;height:20px;" id="email_cb" name="email_cb" /></td>
                    </tr>
                    <tr>
                      <td>EIN#</td>
                      <td><input type="checkbox" style="width:20px;height:20px;" id="ein_cb" name="ein_cb" /></td>
                    </tr>
                    <tr>
                      <td>Contract Signed</td>
                      <td><input type="checkbox" style="width:20px;height:20px;" id="contract_form_cb" name="contract_signed_cb" /></td>
                    </tr>
                    <tr>
                      <td>Consent Form Signed (Expiration Date)</td>
                      <td><input type="checkbox" style="width:20px;height:20px;" id="consent_form_cb" name="consent_form_cb" /></td>
                    </tr>
                    <tr>
                      <td>Site Plan</td>
                      <td><input type="checkbox" style="width:20px;height:20px;" id="site_plan_cb" name="site_plan_cb" /></td>
                    </tr>
                    <tr>
                      <td>A & R Roofs</td>
                      <td><input type="checkbox" style="width:20px;height:20px;" id="roofs_cb" name="roofs_cb" /></td>
                    </tr>
                    <tr>
                      <td>Display Order</td>
                      <td><input type="checkbox" style="width:20px;height:20px;" id="display_order_cb" name="display_order_cb" /></td>
                    </tr>
                    <tr>
                      <td>Congrats Letter / SN</td>
                      <td><input type="checkbox" style="width:20px;height:20px;" id="congrats_letter_cb" name="congrats_letter_cb" /></td>
                    </tr>
                  </table>
                  <hr>
                    </div>
                    <div id="addon_comps">
                    <h3>Addon / Relocate / Take Down</h3>
                    <table>
                      <tr>
                        <td>Start Date</td>
                        <td><input type="text" class="form-control date" id="cl_start_date" name="cl_start_date" autocomplete="off"/></td>
                      </tr>
                      <tr>
                        <td>Total Sales</td>
                        <td><input type="text" class="form-control usd" id="cl_total_sales" name="cl_total_sales" /></td>
                      </tr>
                      <tr>
                        <td>Current 12 Month Sales</td>
                        <td><input type="text" class="form-control usd" id="cl_current_sales" name="cl_current_sales" /></td>
                      </tr>
                    <tr>
                      <td>Lanlord Consent Form Verified?</td>
                      <td><input type="checkbox" style="width:20px;height:20px;" id="consent_verify_cb" name="consent_verify_cb" /></td>
                    </tr>
                    <tr>
                      <td>Updated Dealer Contract?</td>
                      <td><input type="checkbox" style="width:20px;height:20px;" id="contract_verify_cb" name="contract_verify_cb" /></td>
                    </tr>
                    </table>
                      <hr>
                    </div><!--Addon Comps-->
                  <input type="hidden" id="cl_form_type" name="cl_form_type" />
                  <input type="hidden" id="cl_id" name="cl_id" />
                  <h4>Notes:</h4>
                  <textarea id="notes_box" name="notes_box" class="form-control" style="height:200px;"></textarea>
                  </form>
                </div>
                <div class="modal-footer">
                  <p style="color:red;font-weight:bold;" id="checklist_error"></p>
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" type="button" onclick="save_checklist('save');">Save</button>
                  <button class="btn btn-success" type="button" onclick="save_checklist('submit');">Submit</button>
                </div>
            </div>
        </div>
    </div>