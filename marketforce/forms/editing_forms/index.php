<?php
include '../php/connection.php';

//ENTER SESSION SECURITY HERE!!--------

//Load Variables
$id = $_GET['id'];

//Load Info...
$mq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $id . "'";
$mg = mysqli_query($conn, $mq);
if(!$mg || mysqli_num_rows($mg) == 0){
  echo 'There was an error loading this dealers information!';
  break;
}
$mr = mysqli_fetch_array($mg);

?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Edit Dealer Information</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Web Form" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!-- js -->
<script src="js/jquery-2.1.3.min.js" type="text/javascript"></script>
<!-- //js -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href='//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,500,400italic,500italic,700,700italic' rel='stylesheet' type='text/css'>

<!--BS CODE LINKS-->
	<script src="js/bs/bs.js"></script>
	<script src="https://www.w3schools.com/lib/w3data.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Rock+Salt" rel="stylesheet">
	<style>
		.white{
			color:white;
		}
	</style>
	<script>
		function copyCell(){
			if(document.getElementById('same_cell').checked){
				document.getElementById('ownerphone').value = document.getElementById('dcphone').value;
			}else{
				document.getElementById('ownerphone').value = '';
			}
		}
	</script>
</head>
<body>
	<div class="main">
		<h1>Edit Dealer Information<br><span style="color:red;"><small>Dealer ID: <?php echo $mr['ID']; ?></small></span></h1>
		<div class="agileinfo_main">
			<form id="testform" action="php/form-handler.php" method="post" novalidate>
				<fieldset style="border: 1px solid #999">
					<legend>Dealer Details :</legend>
					<input type="hidden" id="user" name="user" value="<?php echo $_SESSION['full_name'] ?>" />
					<input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />
					<p for="bizname" class="w3_field1">Name Of Business</p>
					<input type="text" id="bizname" name="bizname" value="<?php echo $mr['bizname']; ?>" required data-error-msg="We need to know the business name"/>
					
					<p for="address">Address<br><small style="font-size:10px; color:red;">Please do not abbreviate address information<br> (dr, st, ave, etc)</small></p>
					<input id="address" name="address" type="text" value="<?php echo $mr['address']; ?>" required data-error-msg="We need to know the address" />
					
					<p for="city">City</p>
					<input id="city" name="city" type="text" value="<?php echo $mr['city']; ?>" required data-error-msg="We need to know the city" />
					
					<p for="state">State</p>
					<select id="state" name="state" required data-error-msg="We need to know the state">
						<option value="">Select A State...</option>
						<?php
							$ssq = "SELECT * FROM `states`";
							$ssg = mysqli_query($conn, $ssq) or die($conn->error);
							while($ssr = mysqli_fetch_array($ssg)){
								$selected = '';
								if($mr['state'] === $ssr['state']){
									$selected = 'selected';
								}
								echo '<option value="' . $ssr['state'] . '" ' . $selected . '>' . $ssr['state'] . '</option>';
							}
						?>
					</select>
					<!--<input id="state" name="state" type="text" required data-error-msg="We need to know the state" />-->
					
					<p for="zip">Zip</p>
					<input id="zip" name="zip" type="text" value="<?php echo $mr['zip']; ?>" required data-error-msg="We need to know the zip code" />
					
					<p for="phone">Cell Phone Number (10 Digit)<br><span style="color:red;font-size:12px;">(Used for SMS Text notifications)</span></p>
					<input id="dcphone" name="dcphone" type="text" maxlength="10" value="<?php echo $mr['dcphone']; ?>" required data-error-msg="We need to know the dealer cell phone number" />
					
					<p for="phone">Phone Number (10 Digit)</p>
					<input id="phone" name="phone" type="text" value="<?php echo $mr['phone']; ?>" required data-error-msg="We need to know the phone number" />
					
					<p for="fax">Fax Number (10 Digit)</p>
					<input id="fax" name="fax" type="text" value="<?php echo $mr['fax']; ?>" />
			  
				</fieldset>
				
				
				
				<fieldset style="border: 1px solid #999; background:rgba(229,0,20,0.42);">
					<legend>Mailing Address Info :</legend>
					
					<input type="checkbox" id="sameAddress" />&nbsp; <span style="color:white;">Same as Physical Address</span><br>
					
					<p for="bizname" class="w3_field1">Mailing Address</p>
					<input type="text" id="maddress" name="maddress" value="<?php echo $mr['maddress']; ?>" required data-error-msg="We need to know the business mailing address"/>
					
					<p for="city">City</p>
					<input id="mcity" name="mcity" type="text" value="<?php echo $mr['mcity']; ?>" required data-error-msg="We need to know the mailing city" />
					
					<p for="state">State</p>
					<select id="mstate" name="mstate" required data-error-msg="We need to know the mailing state">
						<option value="">Select A State...</option>
						<?php
							$ssq = "SELECT * FROM `states`";
							$ssg = mysqli_query($conn, $ssq) or die($conn->error);
							while($ssr = mysqli_fetch_array($ssg)){
								$selected = '';
								if($mr['mstate'] === $ssr['state']){
									$selected = 'selected';
								}
								echo '<option value="' . $ssr['state'] . '" ' . $selected . '>' . $ssr['state'] . '</option>';
							}
						?>
					</select>
					<!--<input id="state" name="state" type="text" required data-error-msg="We need to know the state" />-->
					
					<p for="zip">Zip</p>
					<input id="mzip" name="mzip" type="text" value="<?php echo $mr['mzip']; ?>" required data-error-msg="We need to know the mailing zip code" />
					
				</fieldset>
				
				
				
				<fieldset class="w3agile_field" style="border: 1px solid #999">
					<legend>About The Business :</legend>
					
					<p for="property" class="w3_field1">This Business's Property is</p>
					<input type="radio" id="owned" name="property" value="OWNED" required data-error-msg="Is the property owned/leased/rented?" <?php if($mr['property'] == 'OWNED'){echo 'checked';} ?>/> <span style="color: white;">OWNED</span>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" id="leased" name="property" value="LEASED-RENTED" <?php if($mr['property'] == 'LEASED/RENTED'){echo 'checked';} ?>/> <span style="color:white;">LEASED/RENTED</span> 
					
					<span id="expiration">
					<p for="expire">Lease Expiration Date</p>
					<input class="date" id="datepicker" name="expiration" type="text" value="<?php echo $mr['expiration']; ?>" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '2/08/2013';}" > 
					<br><br>
					<span style="color:red;">(Note: If you lease or rent your lot, <?php echo $_SESSION['org_full_name']; ?> will require a Landowner's Consent Form)</span>
					</span>
					
					<p for="email">Email</p>
					<input id="email" name="email" type="email" value="<?php echo $mr['email']; ?>" />
					
					<p for="website">Website</p>
					<input id="website" name="website" type="text" value="<?php echo $mr['website']; ?>" />
					
					<p for="years">Years In Business</p>
					<input type="text" id="years" name="years" value="<?php echo $mr['years']; ?>" required data-error-msg="Number of years in business?" />
					
					<p for="numbers">Number Of Locations</p>
					<input type="text" id="numbers" name="numbers" value="<?php echo $mr['numbers']; ?>" required data-error-msg="Number of locations?" />
					
					<p for="EIN">EIN #</p>
					<input type="text" id="EIN" name="EIN" value="<?php echo $mr['EIN']; ?>" />
			  
				</fieldset>
				
				<fieldset style="border: 1px solid #999">
					<legend>Salesman Info :</legend>
					
					<p for="name1" class="w3_field1">Salesman 1 Name</p>
					<input id="name1" name="name1" type="text" value="<?php echo $mr['name1']; ?>" required data-error-msg="We need to know who they are" />
			  
					<p for="email1">Salesman 1 Email</p>
					<input id="email1" name="email1" type="email" value="<?php echo $mr['email1']; ?>" />
				
					<p for="name2">Salesman 2 Name</p>
					<input id="name2" name="name2" type="text" value="<?php echo $mr['name2']; ?>" />
			  
					<p for="email2">Salesman 2 Email</p>
					<input id="email2" name="email2" type="email" value="<?php echo $mr['email2']; ?>" />
					
					<!--
					<p for="products">Products / Services Sold by Business</p>
					<input id="products" name="products" type="text" required data-error-msg="We need to know what they sell" />
					-->
					
					<p for="fulltime">Number of Full Time Employees</p>
					<input type="text" id="fulltime" name="fulltime" value="<?php echo $mr['fulltime']; ?>" required data-error-msg="We need to know the number of full time employees" />
					
					<p for="parttime">Number of Part Time Employees</p>
					<input type="text" id="parttime" name="parttime" value="<?php echo $mr['parttime']; ?>" required data-error-msg="We need to know the number of part time employees" />
					
					<p for="ownername">Business Owner&apos;s Name</p>
					<input id="ownername" name="ownername" type="text" value="<?php echo $mr['ownername']; ?>" required data-error-msg="We need to know who the owner is">
				
					<p for="owneraddress">Owners Home Address</p>
					<input id="owneraddress" name="owneraddress" type="text" value="<?php echo $mr['owneraddress']; ?>" required data-error-msg="We need to know the owner's address" />
					
					<p for="ownercity">City</p>
					<input id="ownercity" name="ownercity" type="text" value="<?php echo $mr['ownercity']; ?>" required data-error-msg="We need to know the owner's city" />
					
					<p for="ownerstate">State</p>
					<select id="ownerstate" name="ownerstate"  required data-error-msg="We need to know the state">
						<option value="">Select A State...</option>
						<?php
							$ssq = "SELECT * FROM `states`";
							$ssg = mysqli_query($conn, $ssq) or die($conn->error);
							while($ssr = mysqli_fetch_array($ssg)){
								$selected = '';
								if($mr['ownerstate'] === $ssr['state']){
									$selected = 'selected';
								}
								echo '<option value="' . $ssr['state'] . '" ' . $selected . '>' . $ssr['state'] . '</option>';
							}
						?>
					</select>
					<!--<input id="ownerstate" name="ownerstate" type="text" required data-error-msg="We need to know the owner's state" />-->
					
					<p for="ownerzip">Zip Code</p>
					<input id="ownerzip" name="ownerzip" type="text" value="<?php echo $mr['ownerzip']; ?>" required data-error-msg="We need to know the owner's zip code" />
					
					<p for="ownerphone">Owner's Cell Number (10 Digit)
						<br>
						<input type="checkbox" id="same_cell" onchange="copyCell();" /> &nbsp; <span style="color:red;font-size:12px;">Same as Dealer Cell Number above</span>
					</p>
					<input id="ownerphone" name="ownerphone" type="text" maxlength="10" value="<?php echo $mr['ownerphone']; ?>" required data-error-msg="We need to know the owner's phone number" />
					
					<p for="altphone">Owner's Alt. Phone Number (10 Digit)</p>
					<input id="altphone" name="altphone" type="text" value="<?php echo $mr['altphone']; ?>" />
					
					<!--<p for="pickaddress" class="w3_textarea">Pick Up Address</p>
					<textarea id="pickaddress" name="pickaddress" placeholder=" " required data-error-msg="Please enter valid address"></textarea>
					-->
				</fieldset>
				
				<fieldset style="border: 1px solid #999">
					<legend>Office Info :</legend>
					
					<p for="mainroad" class="w3_field1">Dealer located on Main Road?</p>
					<input type="radio" id="main-yes" name="mainroad" value="Yes" required data-error-msg="Is the dealer located on a main road?" <?php if($mr['mainroad'] == 'Yes'){echo 'checked';} ?>/> <span style="color:white;">Yes</span>
					&nbsp;
					<input type="radio" id="main-no" name="mainroad" value="No" <?php if($mr['mainroad'] == 'No'){echo 'checked';} ?>/> <span style="color:white;">No</span>		
					<br>
					
					<p for="visibility">Visibility from the main road is</p>
					<input type="radio" id="poor" name="visibility" value="Poor" required data-error-msg="Visibility from main road?" <?php if($mr['visibility'] == 'Poor'){echo 'checked';} ?>/> <span class="white">Poor</span>
					&nbsp;
					<input type="radio" id="good" name="visibility" value="Good" <?php if($mr['visibility'] == 'Good'){echo 'checked';} ?>/> <span class="white">Good</span>
					&nbsp;
					<input type="radio" id="great" name="visibility" value="Great" <?php if($mr['visibility'] == 'Great'){echo 'checked';} ?>/> <span class="white">Great</span>
					&nbsp;
					<input type="radio" id="excellent" name="visibility" value="Excellent" <?php if($mr['visibility'] == 'Excellent'){echo 'checked';} ?>/> <span class="white">Excellent</span>
					<br>
					
					<p for="displayspace">Display Space Available</p>
					<input type="radio" id="large" name="displayspace" value="Large" required data-error-msg="Display space available?" <?php if($mr['displayspace'] == 'Large'){echo 'checked';} ?>/> <span class="white">Large</span>
					&nbsp;
					<input type="radio" id="medium" name="displayspace" value="Medium" <?php if($mr['displayspace'] == 'Medium'){echo 'checked';} ?>/> <span class="white">Medium</span>
					&nbsp;
					<input type="radio" id="small" name="displayspace" value="Small" <?php if($mr['displayspace'] == 'Small'){echo 'checked';} ?>/> <span class="white">Small</span>
					<br>
					
					<p for="currentcompany">Is The Dealer Currently Selling For Another Company</p>
					<input type="radio" id="current-no" name="currentcompany" value="No" required data-error-msg="Is the dealer currently selling for another company?" <?php if($mr['currentcompany'] == 'No'){echo 'checked';} ?>/> <span class="white">No</span>
					&nbsp;
					<input type="radio" id="current-yes" name="currentcompany" value="Yes" <?php if($mr['currentcompany'] == 'Yes'){echo 'checked';} ?>/> <span class="white">Yes</span>
					<br>
					
					<p for="who">If Yes, Who?</p>
					<input type="text" id="who" name="who" value="<?php echo $mr['who']; ?>"/>
					
					<p for="salesarea">Is there a sales area dedicated to the sales of buildings?</p>
					<input type="radio" id="salesarea-yes" name="salesarea" value="Yes" <?php if($mr['salesarea'] == 'Yes'){echo 'checked';} ?> required data-error-msg="Is there a sales area dedicated to the sales of buildings?"/> <span class="white">Yes</span>
					&nbsp;
					<input type="radio" id="salesarea-no" name="salesarea" value="No" <?php if($mr['salesarea'] == 'No'){echo 'checked';} ?>/> <span class="white">No</span>
					<br>
					
					<p for="where">If Yes, Where?</p>
					<input type="text" id="where" name="where" value="<?php echo $mr['where']; ?>" />
					
					<p for="otherprod">What other related products does this location sell?</p>
					<input type="text" id="otherprod" name="otherprod" value="<?php echo $mr['otherprod']; ?>" data-error-msg="Related products sold?"/>
					
					<p for="closedealers">What dealers are within 15 miles of this location?</p>
					<input type="text" id="closedealers" name="closedealers" value="<?php echo $mr['closedealers']; ?>" required data-error-msg="Dealers Close By?"/>
					
					<p for="rto">Recommend RTO Programs?</p>
					<select id="rto" name="rto" required data-error-msg="We need to know RTO status">
						<option value="default">Select One...</option>
						<option value="No" <?php if($mr['rto'] == 'No'){echo 'selected';} ?>>No</option>
						<option value="Yes, have sold over 20,000 in carports" <?php if($mr['rto'] == 'Yes, have sold over 20,000 in carports'){echo 'selected';} ?>>Yes, have sold over 20,000 in carports</option>
						<option value="Yes, existing RTO dealer with a competitor" <?php if($mr['rto'] == 'Yes, existing RTO dealer with a competitor'){echo 'selected';} ?>>Yes, exsisting RTO dealer with a competitor</option>
					</select>
					
					<p for="bbb">BBB Rating</p>
					<input type="text" id="bbb" name="bbb" value="<?php echo $mr['bbb']; ?>" />
					
					<p for="mantra">Mantra</p>
					<input type="text" id="mantra" name="mantra" value="<?php echo $mr['mantra']; ?>" />
					
					
					
				</fieldset>

				<!--<button id="expandBtn" type="button" class="expand-trigger" aria-expanded="false" aria-controls="collapsedSection">Ask any question?</button>
				<div id="collapsedSection" aria-hidden="true">
					<p for="hiddenInput">This field may or may not be visible</p>
					<input id="hiddenInput" name="hiddenInput" type="text" required data-error-msg="You wanted an extra question"></input>
				</div>-->

				<input type="submit" id="sub" value="Submit">
				<input type="reset" id="res" value="Reset" />
				
			</form>
		</div>
		<!-- Calendar -->
				<link rel="stylesheet" href="css/jquery-ui.css" />
				<script src="js/jquery-ui.js"></script>
				  <script>
						  $(function() {
							$( "#datepicker" ).datepicker();
						  });
				  </script>
			<!-- //Calendar -->
		<!--<script src="js/attrvalidate.jquery.js" type="text/javascript"></script>
		THIS FILE REGULATES EVERY FIELD ON THE FORM-->
		<script type="text/javascript">
		  $(document).ready(function(){
			$('#testform').attrvalidate();
			$('#resetBtn').click(function(){ $('#testform').attrvalidate('reset'); });
			$('#expandBtn').click(function(){
			  var collapsible = $('#' + $(this).attr('aria-controls'));
			  $(collapsible).attr('aria-hidden', ($(collapsible).attr('aria-hidden') === 'false'));
			  $(this).attr('aria-expanded', ($(this).attr('aria-expanded') === 'false'));
			});
		  });
		</script>
		  <script>
				w3IncludeHTML();
			</script>
		<div class="agileits_copyright">
			<p>© 2017 <a href="http://allsteelcarports.com">All Steel Carports</a> | Implemented by <a href="http://burtonsolution.tech" style="font-family: Rock Salt, cursive;">Burton Solution</a></p>
		</div>
	</div>
</body>
</html>