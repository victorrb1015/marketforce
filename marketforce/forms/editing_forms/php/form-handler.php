<?php
include '../../../php/connection.php';

//Load Variables From Form
$id = $_POST['id'];
$user = mysqli_real_escape_string($conn, $_POST['user']);
$bizname = mysqli_real_escape_string($conn, $_POST['bizname']);
$address = mysqli_real_escape_string($conn, $_POST['address']);
$city = mysqli_real_escape_string($conn, $_POST['city']);
$state = mysqli_real_escape_string($conn, $_POST['state']);
$zip = mysqli_real_escape_string($conn, $_POST['zip']);
$dcphone = mysqli_real_escape_string($conn, $_POST['dcphone']);
$dcphone = preg_replace("/[^0-9,.]/", "", $dcphone);
$dcphone = "(" . substr($dcphone,0,3) . ") " . substr($dcphone,3,3) . "-" . substr($dcphone,6);
$phone = mysqli_real_escape_string($conn, $_POST['phone']);
$phone = preg_replace("/[^0-9,.]/", "", $phone);
$phone = "(" . substr($phone,0,3) . ") " . substr($phone,3,3) . "-" . substr($phone,6);
$fax = mysqli_real_escape_string($conn, $_POST['fax']);
if($fax == ''){
	$fax = 'none';
}else{
$fax = preg_replace("/[^0-9,.]/", "", $fax);
$fax = "(" . substr($fax,0,3) . ") " . substr($fax,3,3) . "-" . substr($fax,6);
}


//Mailing Address Info
$maddress = mysqli_real_escape_string($conn, $_POST['maddress']);
$mcity = mysqli_real_escape_string($conn, $_POST['mcity']);
$mstate = mysqli_real_escape_string($conn, $_POST['mstate']);
$mzip = mysqli_real_escape_string($conn, $_POST['mzip']);



$property = mysqli_real_escape_string($conn, $_POST['property']);
$expiration = mysqli_real_escape_string($conn, $_POST['expiration']);
if($expiration == 'mm/dd/yyyy'){
	$expiration = 'none';
}
$email = mysqli_real_escape_string($conn, $_POST['email']);
if($email == ''){
	$email = 'none';
}
$website = mysqli_real_escape_string($conn, $_POST['website']);
if($website == ''){
	$website = 'none';
}
$years = mysqli_real_escape_string($conn, $_POST['years']);
$numbers = mysqli_real_escape_string($conn, $_POST['numbers']);
$EIN = mysqli_real_escape_string($conn, $_POST['EIN']);
if($EIN == ''){
	$EIN = 'none';
}
$name1 = mysqli_real_escape_string($conn, $_POST['name1']);
$email1 = mysqli_real_escape_string($conn, $_POST['email1']);
if($email1 == ''){
	$email1 = 'none';
}
$name2 = mysqli_real_escape_string($conn, $_POST['name2']);
if($name2 == ''){
	$name2 = 'none';
}
$email2 = mysqli_real_escape_string($conn, $_POST['email2']);
if($email2 == ''){
	$email2 = 'none';
}
//$products = mysqli_real_escape_string($conn, $_POST['products']);
$fulltime = mysqli_real_escape_string($conn, $_POST['fulltime']);
$parttime = mysqli_real_escape_string($conn, $_POST['parttime']);
$ownername = mysqli_real_escape_string($conn, $_POST['ownername']);
$owneraddress = mysqli_real_escape_string($conn, $_POST['owneraddress']);
$ownercity = mysqli_real_escape_string($conn, $_POST['ownercity']);
$ownerstate = mysqli_real_escape_string($conn, $_POST['ownerstate']);
$ownerzip = mysqli_real_escape_string($conn, $_POST['ownerzip']);
$ownerphone = mysqli_real_escape_string($conn, $_POST['ownerphone']);
$ownerphone = preg_replace("/[^0-9,.]/", "", $ownerphone);
$ownerphone = "(" . substr($ownerphone,0,3) . ") " . substr($ownerphone,3,3) . "-" . substr($ownerphone,6);
$altphone = mysqli_real_escape_string($conn, $_POST['altphone']);
if($altphone == ''){
	$altphone = 'none';
}else{
$altphone = preg_replace("/[^0-9,.]/", "", $altphone);
$altphone = "(" . substr($altphone,0,3) . ") " . substr($altphone,3,3) . "-" . substr($altphone,6);
}
$mainroad = mysqli_real_escape_string($conn, $_POST['mainroad']);
$visibility = mysqli_real_escape_string($conn, $_POST['visibility']);
$displayspace = mysqli_real_escape_string($conn, $_POST['displayspace']);
$currentcompany = mysqli_real_escape_string($conn, $_POST['currentcompany']);
$who = mysqli_real_escape_string($conn, $_POST['who']);
if($who == ''){
	$who = 'none';
}
$salesarea = mysqli_real_escape_string($conn, $_POST['salesarea']);
$where = mysqli_real_escape_string($conn, $_POST['where']);
if($where == ''){
	$where = 'none';
}
$otherprod = mysqli_real_escape_string($conn, $_POST['otherprod']);
$closedealers = mysqli_real_escape_string($conn, $_POST['closedealers']);
$rto = mysqli_real_escape_string($conn, $_POST['rto']);
$bbb = mysqli_real_escape_string($conn, $_POST['bbb']);
if($bbb == ''){
	$bbb = 'none';
}
$mantra = mysqli_real_escape_string($conn, $_POST['mantra']);
if($mantra == ''){
	$mantra = 'none';
}

//Insert into the main Dealer list for note additions
$xxq = "UPDATE `dealers` SET 
				`name` = '" . $bizname . "',
				`address` = '" . $address . "',
				`city` = '" . $city . "',
				`state` = '" . $state . "',
				`zip` = '" . $zip . "',
				`phone` = '" . $phone . "',
				`fax` = '" . $fax . "',
				`email` = '" . $email . "'
				WHERE `ID` = '" . $id . "'";
				
$xxg = mysqli_query($conn, $xxq) or die($conn->error);


//INSERT DATA INTO DATABASE
$i = "UPDATE
      `new_dealer_form`
			SET
			`bizname` = '" . $bizname . "',
			`address` = '" . $address . "',
			`city` = '" . $city . "',
			`state` = '" . $state . "',
			`zip` = '" . $zip . "',
			`dcphone` = '" . $dcphone . "',
			`phone` = '" . $phone . "',
			`fax` = '" . $fax . "',
			`maddress` = '" . $maddress . "',
			`mcity` = '" . $mcity . "',
			`mstate` = '" . $mstate . "',
			`mzip` = '" . $mzip . "',
			`property` = '" . $property . "',
       `expiration` = '" . $expiration . "',
			 `email` = '" . $email . "',
			 `website` = '" . $website . "',
			 `years` = '" . $years . "',
			 `numbers` = '" . $numbers . "',
			 `EIN` = '" . $EIN . "',
			 `name1` = '" . $name1 . "',
			 `email1` = '" . $email1 . "',
			 `name2` = '" . $name2 . "',
			 `email2` = '" . $email2 . "',
       `fulltime` = '" . $fulltime . "',
			 `parttime` = '" . $parttime . "',
			 `ownername` = '" . $ownername . "',
			 `owneraddress` = '" . $owneraddress . "',
			 `ownercity` = '" . $ownercity . "',
			 `ownerstate` = '" . $ownerstate . "',
			 `ownerzip` = '" . $ownerzip . "',
			 `ownerphone` = '" . $ownerphone . "',
			 `altphone` = '" . $altphone . "',
			 `mainroad` = '" . $mainroad . "',
			 `visibility` = '" . $visibility . "',
			 `displayspace` = '" . $displayspace . "',
       `currentcompany` = '" . $currentcompany . "',
			 `who` = '" . $who . "',
			 `salesarea` = '" . $salesarea . "',
			 `where` = '" . $where . "',
			 `products` = '" . $otherprod . "',
			 `closedealers` = '" . $closedealers . "',
			 `rto` = '" . $rto . "',
			 `bbb` = '" . $bbb . "',
			 `mantra` = '" . $mantra . "',
			 `edited_by` = '" . $user . "',
			 `edit_timestamp` = (DATE_ADD(now() , INTERVAL 1 HOUR))
			 WHERE
			 `ID` = '" . $id . "'";
     

$ig = mysqli_query($conn, $i) or die($conn->error);




//Setup Google Geolocation in database
echo '<html>
			<head>
			<script>';
//echo 'alert("Hello");';
$q = "SELECT * FROM `dealers` WHERE `ID` = '" . $id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  
echo 'var address = "' . $r['address'] . ',' . $r['city'] . ',' . $r['state'] . ',' . $r['zip'] . '";
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
    var jd = JSON.parse(xmlhttp.responseText);
if(jd.status === "ZERO_RESULTS"){
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp12=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp12=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp12.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
			//alert("one");
      //document.getElementById("response").innerHTML = xmlhttp12.responseText;
    }
  }
  xmlhttp12.open("GET","http://' . $_SERVER['HTTP_HOST'] . '/marketforce/map/cron.php?error=true&name=' . $r['name'] . '&id=' . $r['ID'] . '&address=' . $r['address'] . '&city=' . $r['city'] . '&state=' . $r['state'] . '&zip=' . $r['zip'] . '",true);
  xmlhttp12.send();
    }
    var lat = jd.results[0].geometry.location.lat;
    var lng = jd.results[0].geometry.location.lng;
    
    var id = "' . $r['ID'] . '";
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp2=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp2.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("main").innerHTML += xmlhttp2.responseText + "<br>";
			//alert(xmlhttp2.responseText);//This will display the success of failed text for Geolocation!
      //document.getElementById("response2").innerHTML = xmlhttp2.responseText;
    }
  }
  xmlhttp2.open("GET","http://' . $_SERVER['HTTP_HOST'] . '/marketforce/map/insertlatlng.php?id="+id+"&lat="+lat+"&lng="+lng,true);
  xmlhttp2.send();
    }
  }
  xmlhttp.open("GET","https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg",true);
  xmlhttp.send();
  ';
  
}
echo '</script>';


//Display Response
echo '</head>
			<body>
			<h1 style="text-align:center;">Thank You!</h1>';
echo '<h2 style="text-align:center;">This dealer has been updated successfully!</h2>';
echo '<br><br>';
echo '<span id="response"></span><br><br>';
echo '<span id="response2"></span><br><br>';
echo '<div style="text-align:center;margin:auto;">
			<button type="button" onclick="window.close();" style="background:blue;color:white;">Close Window</button>
			</div>
			</body>
			</html>';
?>