<?php
include '../../security/session/get-org-info.php';
include '../../php/connection.php';

//Load Variables
$h = $_GET['h'];//Hours
$m = $_GET['m'];//Minutes
$q1 = $_GET['q1'];
$q2 = $_GET['q2'];
$q3 = $_GET['q3'];
$q4 = $_GET['q4'];
$q5 = $_GET['q5'];
$q6 = $_GET['q6'];
$q7 = $_GET['q7'];
$q8 = $_GET['q8'];
$q9 = $_GET['q9'];
$q10 = $_GET['q10'];
$ta1 = mysqli_real_escape_string($conn,$_GET['ta1']);
$ta2 = mysqli_real_escape_string($conn,$_GET['ta2']);
$d = $_GET['d'];//Dealer ID
$vd = $_GET['vd'];//Visit Date
$r = $_GET['rep'];//Rep's Name

//Get the Dealers name from the ID
$dnq = "SELECT * FROM `dealers` WHERE `ID` = '" . $d . "'";
$dng = mysqli_query($conn, $dnq);
$dnr = mysqli_fetch_array($dng);
$dealer_name = $dnr['name'];
$dealer_email = $dnr['email'];

//Check for Duplicate
$doubleq = "SELECT * FROM `rep_survey` WHERE `dealer_name` = '" . $d . "' AND `rep_name` = '" . $r . "' AND `visit_date` = '" . $vd . "'";
$doubleg = mysqli_query($conn, $doubleq) or die($conn->error);
if(mysqli_num_rows($doubleg) == 0){

//Insert into database
$iq = "INSERT 
        INTO 
        `rep_survey` (`hrs`,`mins`,`q1`,`q2`,`q3`,`q4`,`q5`,`q6`,`q7`,`q8`,`q9`,`q10`,`ta1`,`ta2`,`dealer_name`,`sub_date`,`sub_time`,`rep_name`,`visit_date`)
        VALUES
        ('" . $h . "',
         '" . $m . "',
         '" . $q1 . "',
         '" . $q2 . "',
         '" . $q3 . "',
         '" . $q4 . "',
         '" . $q5 . "',
         '" . $q6 . "',
         '" . $q7 . "',
         '" . $q8 . "',
         '" . $q9 . "',
         '" . $q10 . "',
         '" . $ta1 . "',
         '" . $ta2 . "',
         '" . $d . "',
         CURRENT_DATE,
         CURRENT_TIME,
         '" . $r . "',
         '" . $vd . "')";

$ig = mysqli_query($conn, $iq) or die($conn->error);
echo 'Your survey was submitted! ';
$numsa = array($q1,$q2,$q3,$q4,$q5,$q6,$q7,$q8,$q9,$q10);
  $i = 0;
 while($i <= 9){
switch ($numsa[$i]) {
    case "1":
        $numsa[$i] = "F";
        break;
    case "2":
        $numsa[$i] = "D";
        break;
    case "3":
        $numsa[$i] = "C";
        break;
    case "4":
        $numsa[$i] = "B";
        break;
    case "5":
        $numsa[$i] = "A";
        break;
    default:
        echo "ERROR!";
}
   $i++;
 }
  
  $hh = $h . ' HRS and';
  if($h == ''){
    $hh = '';
  }
  if($h == '0'){
    $hh = '';
  }
  if($h == 0){
    $hh = '';
  }
  
//STMP EMAIL PARAMETERS
include '../../php/phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include '../../php/phpmailsettings.php';

//Email Parameters
//$admin = 'michael@burtonsolution.tech';
$admin = 'michael@allsteelcarports.com';
$mail->addAddress($admin);
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
$mail->addReplyTo($dealer_email);
$mail->addCC('teresa@allsteelcarports.com');
$mail->addBCC('archive@ignition-innovations.com');
$mail->Subject = 'New Survey Info!';
  
$mail->Body = '<html>
          <head>
          <style>
          .header{
            text-align: center;
          }
          </style>
          </head>
          <body>
          <div class="main">
          <div class="header">
          <img src="http://marketforceapp.com/marketforce/img/logo-allsteel.png" style="text-align: center;"><br>
          <img src="http://marketforceapp.com/marketforce/img/confidential.png" style="transform: rotate(20deg); width: 40%; text-align: center;">
          <h1 style="text-align: center;">New Survey Information!</h1>
          </div>
          <br>
          <h3>One of your dealers,<span style="color:red;"> ' . $dealer_name . '</span>, has filled out the survey we sent them on your representative, <span style="color:red;">' . $r . '</span>.
          Below are the survey results:</h3>
          <br><br>
          <p>Visit Date: ' . $vd . '</p>
          <p>Visit Duration: ' . $hh . ' ' . $m . ' MINS</p>
          <p>Q1: My Outside Sales Representative Made the visit well worth my time.</p>
          <p><strong>Grade: ' . $numsa[0] . '</strong></p>
          <p>Q2: My Outside Sales Representative Was able to answer the questions I had with their knowledge of the product and sales procedures.</p>
          <p><strong>Grade: ' . $numsa[1] . '</strong></p>
          <p>Q3: My Outside Sales Representative Took the time to make sure we understood and could work with the information.</p>
          <p><strong>Grade: ' . $numsa[2] . '</strong></p>
          <p>Q4: My Outside Sales Representative Replaced and/or provided me with brochures and sales materials including order forms that we needed.</p>
          <p><strong>Grade: ' . $numsa[3] . '</strong></p>
          <p>Q5: My Outside Sales Representative Invited me to call or email with any questions or assistance I needed for the future.</p>
          <p><strong>Grade: ' . $numsa[4] . '</strong></p>
          <p>Q6: My Outside Sales Representative Would help me any way possible if needed.</p>
          <p><strong>Grade: ' . $numsa[5] . '</strong></p>
          <p>Q7: My Outside Sales Representative Seems genuinely interested in our success in sales for All Steel Carports.</p>
          <p><strong>Grade: ' . $numsa[6] . '</strong></p>
          <p>Q8: My Outside Sales Representative Provides me with accurate and timely responses when I call, text or email.</p>
          <p><strong>Grade: ' . $numsa[7] . '</strong></p>
          <p>Q9: My Outside Sales Representative Responds to my Texts, Calls and Emails within 24 hours.</p>
          <p><strong>Grade: ' . $numsa[8] . '</strong></p>
          <p>Q10: My Outside Sales Representative Would be a top choice if I were hiring for my company.</p>
          <p><strong>Grade: ' . $numsa[9] . '</strong></p>
          <br>
          <p><strong>Share any additional comments about this visit: <span style="color:red;">(CONFIDENTIAL)</span></strong></p>
          <p><strong>Customer: </strong>' . $ta1 . '</p>
          <br>
          <p><strong>Please let me know how we can better assist you and your sales team:</strong></p>
          <p><strong>Customer: </strong>' . $ta2 . '</p>
          <br>
          <h3>Thanks,<br><strong>Market Force</strong></h3>
         
          </div>
          </body>
          </html>';

if(!$mail->send()){
  echo 'There was an error sending your email! ';
  echo $mail->ErrorInfo;
}else{
echo 'Thank You! Your input significantly helps us in our training process to ensure we give you the best help available.';
}
  
}else{
  echo 'A Survey has already been submitted for this Rep on this date!';
}
?>
  
  