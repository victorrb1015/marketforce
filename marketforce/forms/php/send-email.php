<?php
include 'connection.php';
include '../../../phpmailer/PHPMailerAutoload.php';

//var_dump($_POST);
//break;

//Load Variables
$q = $_POST['q'];
$m = mysqli_real_escape_string($conn, str_replace('-xxx-', '&', $_POST['m']));
$m = str_replace('\n', '<br>', $m);
//echo $m . '----';
$t = mysqli_real_escape_string($conn, str_replace('-xxx-', '&', $_POST['t']));
$un = $_POST['un'];
$user = $_POST['user'];


//Get Query Type ('ou' [OSR Update] or 'du' [Dealer Update] )

//If Query OSR Update
if ($q == 'ou') {
    //INSERT INTO DB
    $i = "INSERT INTO `oss_updates` 
        (`update_id`,`topic`,`mess`,`user`,`date`,`time`)
        VALUES
        ('" . date('mdY') . "','" . $t . "','" . $m . "','" . $user . "',CURRENT_DATE,CURRENT_TIME)";
    mysqli_query($conn, $i) or die($conn->error);

    //Email Parameters
    $mail = new PHPMailer;

    include '../../../phpmailsettings.php';
    $mail->setFrom('no-reply@allsteelcarports.com', 'All Steel Carports');
    $mail->addReplyTo('michael@allsteelcarports.com', 'Michael Burton'); //For When DB is used for emailing.
    $eq = "SELECT * FROM `users` WHERE `position` = 'Outside Sales Rep' AND `inactive` != 'Yes'";
    $eg = mysqli_query($conn, $eq) or die($conn->error);
    while ($er = mysqli_fetch_array($eg)) {
        $mail->addAddress($er['email']);
        //echo $er['email'] . '
        //';
    }
    $mail->addCC('michael@allsteelcarports.com');
    $mail->addCC('chavez@allsteelcarports.com');
    $mail->addCC('teresa@allsteelcarports.com');
    //$mail->addAddress('michael@ignition-innovations.com');
    $mail->addBCC('archive@ignition-innovations.com');
    //$mail->addCC('victor@grupocomunicado.com');
    $mail->Subject = 'OSTR Update!';


    $mail->Body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    
    <!-- Textarea Formatter Code -->
    <script src="http://cloud.tinymce.com/stable/tinymce.min.js?apiKey=wh7y9ve9aotjjcywsf2ittpjdbhlk1q7ur241kh8e9dnwlpe"></script>
    <script>tinymce.init({ selector:"textarea" });</script>
    
    
    <style>
      .m_span p, h1, h2, h3, h4, h5, h6{
        color: black;
      }
    </style>
    
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no" /> <!-- disable auto telephone linking in iOS -->
		<title>OSTR UPDATE</title>
		<style type="text/css">
			/* RESET STYLES */
			html { background-color:#E1E1E1; margin:0; padding:0; }
			body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, "Lucida Grande", sans-serif;}
			table{border-collapse:collapse;}
			table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#7A7A7A;font-weight:normal;}
			img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
			a {text-decoration:none !important;border-bottom: 1px solid;}
			h1, h2, h3, h4, h5, h6{/*color:#5F5F5F;*/ font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}
			/* CLIENT-SPECIFIC STYLES */
			.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */
			table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */
			#outlook a{padding:0;} /* Force Outlook 2007 and up to provide a "view in browser" message. */
			img{-ms-interpolation-mode: bicubic;display:block;outline:none; text-decoration:none;} /* Force IE to smoothly render resized images. */
			body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; font-weight:normal!important;} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
			.ExternalClass td[class="ecxflexibleContainerBox"] h3 {padding-top: 10px !important;} /* Force hotmail to push 2-grid sub headers down */
			/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */
			/* ========== Page Styles ========== */
			h1{display:block;font-size:26px;font-style:normal;font-weight:normal;line-height:100%;}
			h2{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:120%;}
			h3{display:block;font-size:17px;font-style:normal;font-weight:normal;line-height:110%;}
			h4{display:block;font-size:18px;font-style:italic;font-weight:normal;line-height:100%;}
			.flexibleImage{height:auto;}
			.linkRemoveBorder{border-bottom:0 !important;}
			table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}
			body, #bodyTable{background-color:#E1E1E1;}
			#emailHeader{background-color:#E1E1E1;}
			#emailBody{background-color:#FFFFFF;}
			#emailFooter{background-color:#E1E1E1;}
			.nestedContainer{background-color:#F8F8F8; border:1px solid #CCCCCC;}
			.emailButton{background-color:#205478; border-collapse:separate;}
			.buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
			.buttonContent a{color:#FFFFFF; display:block; text-decoration:none!important; border:0!important;}
			.emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}
			.emailCalendarMonth{background-color:#205478; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}
			.emailCalendarDay{color:#205478; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}
			.imageContentText {margin-top: 10px;line-height:0;}
			.imageContentText a {line-height:0;}
			#invisibleIntroduction {display:none !important;} /* Removing the introduction text from the view */
			/*FRAMEWORK HACKS & OVERRIDES */
			span[class=ios-color-hack] a {color:#275100!important;text-decoration:none!important;} /* Remove all link colors in IOS (below are duplicates based on the color preference) */
			span[class=ios-color-hack2] a {color:#205478!important;text-decoration:none!important;}
			span[class=ios-color-hack3] a {color:#8B8B8B!important;text-decoration:none!important;}
			/* A nice and clean way to target phone numbers you want clickable and avoid a mobile phone from linking other numbers that look like, but are not phone numbers.  Use these two blocks of code to "unstyle" any numbers that may be linked.  The second block gives you a class to apply with a span tag to the numbers you would like linked and styled.
			Inspired by Campaign Monitor&apos;s article on using phone numbers in email: http://www.campaignmonitor.com/blog/post/3571/using-phone-numbers-in-html-email/.
			*/
			.a[href^="tel"], a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:none!important;cursor:default!important;}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:auto!important;cursor:default!important;}
			/* MOBILE STYLES */
			@media only screen and (max-width: 480px){
				/*////// CLIENT-SPECIFIC STYLES //////*/
				body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */
				/* FRAMEWORK STYLES */
				/*
				CSS selectors are written in attribute
				selector format to prevent Yahoo Mail
				from rendering media query styles on
				desktop.
				*/
				/*td[class="textContent"], td[class="flexibleContainerCell"] { width: 100%; padding-left: 10px !important; padding-right: 10px !important; }*/
				table[id="emailHeader"],
				table[id="emailBody"],
				table[id="emailFooter"],
				table[class="flexibleContainer"],
				td[class="flexibleContainerCell"] {width:100% !important;}
				td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {display: block;width: 100%;text-align: left;}
				/*
				The following style rule makes any
				image classed with "flexibleImage"
				fluid when the query activates.
				Make sure you add an inline max-width
				to those images to prevent them
				from blowing out.
				*/
				td[class="imageContent"] img {height:auto !important; width:100% !important; max-width:100% !important; }
				img[class="flexibleImage"]{height:auto !important; width:100% !important;max-width:100% !important;}
				img[class="flexibleImageSmall"]{height:auto !important; width:auto !important;}
				/*
				Create top space for every second element in a block
				*/
				table[class="flexibleContainerBoxNext"]{padding-top: 10px !important;}
				/*
				Make buttons in the email span the
				full width of their container, allowing
				for left- or right-handed ease of use.
				*/
				table[class="emailButton"]{width:100% !important;}
				td[class="buttonContent"]{padding:0 !important;}
				td[class="buttonContent"] a{padding:15px !important;}
			}
			/*  CONDITIONS FOR ANDROID DEVICES ONLY
			*   http://developer.android.com/guide/webapps/targeting.html
			*   http://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/ ;
			=====================================================*/
			@media only screen and (-webkit-device-pixel-ratio:.75){
				/* Put CSS for low density (ldpi) Android layouts in here */
			}
			@media only screen and (-webkit-device-pixel-ratio:1){
				/* Put CSS for medium density (mdpi) Android layouts in here */
			}
			@media only screen and (-webkit-device-pixel-ratio:1.5){
				/* Put CSS for high density (hdpi) Android layouts in here */
			}
			/* end Android targeting */
			/* CONDITIONS FOR IOS DEVICES ONLY
			=====================================================*/
			@media only screen and (min-device-width : 320px) and (max-device-width:568px) {
			}
			/* end IOS targeting */
		</style>
		<!--
			Outlook Conditional CSS
			These two style blocks target Outlook 2007 & 2010 specifically, forcing
			columns into a single vertical stack as on mobile clients. This is
			primarily done to avoid the "page break bug" and is optional.
			More information here:
			http://templates.mailchimp.com/development/css/outlook-conditional-css
		-->
		<!--[if mso 12]>
			<style type="text/css">
				.flexibleContainer{display:block !important; width:100% !important;}
			</style>
		<![endif]-->
		<!--[if mso 14]>
			<style type="text/css">
				.flexibleContainer{display:block !important; width:100% !important;}
			</style>
		<![endif]-->
	</head>
	<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

		<!-- CENTER THE EMAIL // -->
		<!--
		1.  The center tag should normally put all the
			content in the middle of the email page.
			I added "table-layout: fixed;" style to force
			yahoomail which by default put the content left.
		2.  For hotmail and yahoomail, the contents of
			the email starts from this center, so we try to
			apply necessary styling e.g. background-color.
		-->
		<center style="background-color:#E1E1E1;">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
				<tr>
					<td align="center" valign="top" id="bodyCell">

						<!-- EMAIL HEADER // -->
						<!--
							The table "emailBody" is the email&apos;s container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

							<!-- HEADER ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<!--
																		The "invisibleIntroduction" is the text used for short preview
																		of the email before the user opens it (50 characters max). Sometimes,
																		you do not want to show this message depending on your design but this
																		text is highly recommended.
																		You do not have to worry if it is hidden, the next <td> will automatically
																		center and apply to the width 100% and also shrink to 50% if the first <td>
																		is visible.
																	-->
																	<td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																						An update from All Steel Carports
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td align="right" valign="middle" class="flexibleContainerBox">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					<!-- CONTENT // -->
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																						<!--If you can&apos;t see this message, <a href="#" target="_blank" style="text-decoration:none;border-bottom:1px solid #828282;color:#828282;"><span style="color:#828282;">view&nbsp;it&nbsp;in&nbsp;your&nbsp;browser</span></a>.-->
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // END -->

						</table>
						<!-- // END -->

						<!-- EMAIL BODY // -->
						<!--
							The table "emailBody" is the email&apos;s container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">

							<!-- MODULE ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<!--
										The centering table keeps the content
										tables centered in the emailBody table,
										in case its width is set to 100%.
									-->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#7AC142">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<!--
													The flexible container has a set width
													that gets overridden by the media query.
													Most content tables within can then be
													given 100% widths.
												-->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<!--
															The content table is the first element
																that&apos;s entirely separate from the structural
																framework of the email.
															-->
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>';
    switch ($_SESSION['org_id']) {
        case "738004":
        case "654321":
        case "832122":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="http://dev.marketforceapp.com/assets/img/brands/acero-news.png" style="max-width: 1000px;"><br>';
            break;
        case "162534":
            $mail->Body .= '<td align="center" valign="top" class="textContent" style="Background-color: #A0A2A6">
                                                          <img src="http://dev.marketforceapp.com/assets/img/brands/NorthEdgeSteel-01.png" style="max-width: 350px;"><br>';
            break;
        case "615243":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="http://dev.marketforceapp.com/assets/img/brands/legacyMFlogo.png" style="max-width: 1000px;"><br>';
            break;

        case "329698":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="https://marketforceapp.com/assets/img/brands/integrity-logo2.png" style="max-width: 1000px;"><br>';
            break;

        case "532748":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="https://marketforceapp.com/assets/img/brands/JMLogo.png" style="max-width: 600px;"><br>';
            break;
        case "841963":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                                            <img src="https://marketforceapp.com/assets/img/brands/logo_star_buildings.png" style="max-width: 600px;"><br>';
            break;
        default:
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="'.$_SESSION['org_logo_url_alt'].'" style="max-width: 1000px;"><br>';
    }
    $mail->Body .= '<h1 style="color:#FFFFFF;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:35px;font-weight:normal;margin-bottom:5px;text-align:center;">';
    switch ($_SESSION['org_id']) {
        case "162534":
            $mail->Body .= '765.791.8080';
            break;
    }
    $mail->Body .= '</h1>
																		<h2 style="text-align:center;font-weight:normal;font-family:Helvetica,Arial,sans-serif;font-size:23px;margin-bottom:10px;color:#205478;line-height:135%;">';
    switch ($_SESSION['org_id']) {
        case "329698":
            $mail->Body .= 'We Specialize in Satisfied Customers';
            break;
        case "162534":
            $mail->Body .= 'Building Beyond Measure';
            break;
        default:
            $mail->Body .= 'Affordable Buildings, Exceptional Quality!';
    }
    $mail->Body .= '</h2>
																		<div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:25px;margin-bottom:0;color:#FFFFFF;line-height:135%;">OSTR Update ' . date('mdY') . '</div>
																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


						<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="90%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					<h3 mc:edit="header" style="color:black;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">';
    switch ($_SESSION['org_id']) {
        case "162534":
            $mail->Body .= 'Northedge Steel Dealers';
            break;
        default:
            $mail->Body .= 'Email To';
    }
    $mail->Body .= ': &nbsp;&nbsp;Outside Sales & Training &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Representatives<br>Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . date("l, m/d/Y") . '<br>Topic: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $t . '</h3>
																					<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:17px;margin-bottom:0;color:black;line-height:135%;">
                                          <br><br>
                                            <span class="m_span">
                                          ' . $m . '
                                            </span>
                                          <!--<br><br>
                                          Thank you very much!
                                          <br><br>
                                          <strong>All Steel Carports</strong>-->
                                         </div>
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->



							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					<!--<h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Dear valued customer,</h3>-->
																					<!--<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">Thank you so much for your input in our training department. Together, what we can easily see is only a small percentage of what is possible!</div>-->
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->




              <!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#7AC142">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					
																					<div mc:edit="body" style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:20px;margin-bottom:0;color:#000000;line-height:135%;">
                                          <strong>Important Email Addresses
                                          <br><br>NEW ORDERS: &nbsp; &nbsp; <a href="#" style="color: black; list-style-type: none;">';
    switch ($_SESSION['org_id']) {
        case "162534":
            $mail->Body .= 'sales@northedgesteel.us';
            break;
        case "329698":
            $mail->Body .= 'orders@integritycarports.com';
            break;
        case "532748":
            $mail->Body .= 'orders@renovationsjm.com';
            break;
        case "841963":
            $mail->Body .= 'orders@starbuildingsandcarports.com';
            break;
        default:
            $mail->Body .= 'orders@allsteelcarports.com';
    }
    $mail->Body .= '</a>
                                          <br><br>REPAIRS: &nbsp; &nbsp; <a href="#" style="color: black; list-style-type: none;">';
    switch ($_SESSION['org_id']) {
        case "329698":
            $mail->Body .= 'repairs@integritycarports.com';
            break;
        case "162534":
            $mail->Body .= 'repairs@northedgesteel.us';
            break;
        case "532748":
            $mail->Body .= 'repairs@renovationsjm.com';
            break;
        case "841963":
            $mail->Body .= 'repairs@starbuildingsandcarports.com';
            break;
        default:
            $mail->Body .= 'repairs@allsteelcarports.com';
    }
    $mail->Body .= '</a>
                                          <br>
                                          </strong>
                                         </div>
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->
							


							<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">
							<!-- FOOTER ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td valign="top" bgcolor="#E1E1E1">

																		<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																			<div>Copyright &#169; 2017 <a href="http://www.burtonsolution.tech" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">Market Force</span></a>. All&nbsp;rights&nbsp;reserved.</div>
																			<!--<div>If you do not want to recieve emails from us, you can <a href="#" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">unsubscribe</span></a>.</div>-->
																		</div>

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>

						</table>
						<!-- // END -->

					</td>
				</tr>
			</table>
		</center>
	</body>
	</html>';

    //If in BETA server, only send as a test to these addresses...
    if ($_SERVER['HTTP_HOST'] == 'beta.marketforceapp.com') {
        $mail->clearAllRecipients();
        $mail->addAddress('mburton3969@gmail.com');
        echo 'Test Mode - ';
    }

    //Send Email
    if (!$mail->send()) {
        echo 'Message could not be sent.<br>';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'Message has been sent. ';
    }
    //mail($admin,$sub,$mess,$header);
    //echo 'Thank You!';
} //End OSS Update Query


//-------------------------------------------------------------------------------------------------------//

//If Query Dealer Update
if ($q == 'du') {
    $xx->emails = 'First Email Blank';


    $mail = new PHPMailer;
    include '../../../phpmailsettings.php';

    //Email Parameters

    switch ($_SESSION['org_id']) {
        case "615243":
            $mail->setFrom('info@legacybuildings.us', 'Legacy Buildings');
            break;
        default:
            $mail->setFrom('info@allsteelcarports.com', 'All Steel Carports');
    }

    $mail->addReplyTo('info@allsteelcarports.com', 'All Steel Carports'); //For When DB is used for emailing.

    $aq = "SELECT * FROM `email_attachments` WHERE `email_id` = '" . $un . "' AND `inactive` != 'Yes'";
    $ag = mysqli_query($conn, $aq) or die($conn->error);
    while ($ar = mysqli_fetch_array($ag)) {
        $mail->addAttachment($ar['url'], $ar['file_name']);
        /* echo '<script>
            alert("' . $ar['url'] . '");
          </script>';*/
    }

    $mail->addAddress('info@allsteelcarports.com', 'All Steel Dealers');
    //$mail->addAddress('info@ignition-innovations.com', 'All Steel Dealer Group');
    //Setup Email Query

    $eq = "SELECT * FROM `new_dealer_form` WHERE (`inactive` != 'Yes')";
    if ($_POST['ALL'] != 'Yes') {
        $sq = "SELECT DISTINCT `state` FROM `dealers` WHERE `inactive` != 'Yes'";
        $sg = mysqli_query($conn, $sq) or die($conn->error);
        $ii = 0;
        while ($sr = mysqli_fetch_array($sg)) {
            $ss = str_replace(" ", "_", $sr['state']);
            if ($_POST[$ss] != '') {

                $ii++;
                if ($ii > 1) {
                    $eq .= " OR ";
                } else {
                    $eq .= " AND ";
                }

                if ($_POST[$ss] == 'Arkansas') {
                    if ($_POST['arkansas_remote'] != '') {
                        $eq .= "`state` = '" . $sr['state'] . "'";
                    } else {
                        $eq .= "(`state` = '" . $sr['state'] . "' AND `remote` != 'Yes')";
                    }
                } elseif ($_POST[$ss] == 'Missouri') {
                    if ($_POST['missouri_remote'] != '') {
                        $eq .= "`state` = '" . $sr['state'] . "'";
                    } else {
                        $eq .= "(`state` = '" . $sr['state'] . "' AND `remote` != 'Yes')";
                    }
                } elseif ($_POST[$ss] == 'Kentucky') {
                    if ($_POST['kentucky_remote'] != '') {
                        $eq .= "`state` = '" . $sr['state'] . "'";
                    } else {
                        $eq .= "(`state` = '" . $sr['state'] . "' AND `remote` != 'Yes')";
                    }
                } elseif ($_POST[$ss] == 'Tennessee') {
                    if ($_POST['tennessee_remote'] != '') {
                        $eq .= "`state` = '" . $sr['state'] . "'";
                    } else {
                        $eq .= "(`state` = '" . $sr['state'] . "' AND `remote` != 'Yes')";
                    }
                } else {
                    $eq .= "`state` = '" . $sr['state'] . "'";
                }
            } elseif ($_POST['arkansas_remote'] != '') {
                $eq .= " AND (`state` = 'Arkansas' AND `remote` = 'Yes')";
            } elseif ($_POST['missouri_remote'] != '') {
                $eq .= " AND (`state` = 'Missouri' AND `remote` = 'Yes')";
            } elseif ($_POST['kentucky_remote'] != '') {
                $eq .= " AND (`state` = 'Kentucky' AND `remote` = 'Yes')";
            } elseif ($_POST['tennessee_remote'] != '') {
                $eq .= " AND (`state` = 'Tennessee' AND `remote` = 'Yes')";
            }
        }
    }

    //print_r($_POST);
    //echo $eq;
    //break;
    echo 'Email Sent To: 
	';
    $eg = mysqli_query($conn, $eq) or die($conn->error);
    while ($er = mysqli_fetch_array($eg)) {
        if ($er['email'] != '' && $er['email'] != 'none' && $er['inactive'] != 'Yes') {
            $mail->addBCC($er['email']);
            echo $er['bizname'] . '->' . $er['email'] . '<br> 
			';
            $xx->emails .= ',' . mysqli_real_escape_string($conn, $er['bizname']) . '--' . mysqli_real_escape_string($conn, $er['email']);
        }
        if ($er['email1'] != '' && $er['email1'] != 'none' && $er['inactive'] != 'Yes') {
            $mail->addBCC($er['email1']);
            echo $er['bizname'] . '->' . $er['email1'] . '<br> 
			';
            $xx->emails .= ',' . mysqli_real_escape_string($conn, $er['bizname']) . '--' . mysqli_real_escape_string($conn, $er['email1']);
        }
        if ($er['email2'] != '' && $er['email2'] != 'none' && $er['inactive'] != 'Yes') {
            $mail->addBCC($er['email2']);
            echo $er['bizname'] . '->' . $er['email2'] . '<br>
			';
            $xx->emails .= ',' . mysqli_real_escape_string($conn, $er['bizname']) . '--' . mysqli_real_escape_string($conn, $er['email2']);
        }
    }

    $ejson = json_encode($xx);
    //INSERT INTO DB
    $i = "INSERT INTO `dealer_updates` 
        (`ID`,`topic`,`mess`,`user`,`date`,`time`,`emails`)
        VALUES
        ('" . $un . "','" . $t . "','" . $m . "','" . $user . "',CURRENT_DATE,CURRENT_TIME,'" . mysqli_real_escape_string($conn, $ejson) . "')";
    mysqli_query($conn, $i) or die($conn->error);
    switch ($_SESSION['org_id']) {
        case '162534':
            $mail->addBCC('jtrue@northedgesteel.us');
            $mail->addBCC('mcooper@northedgesteel.us');
            $mail->addBCC('tcarr@northedgesteel.us');
            $mail->addBCC('tnally@northedgesteel.us');
            break;
        case '615243':
            $mail->addBCC('jeffrey@allsteelcarports.com');
            $mail->addBCC('achavez@allsteelcarports.com');
            break;
    }

    $mail->addBCC('michael@allsteelcarports.com');
    $mail->addBCC('teresa@allsteelcarports.com');
    $mail->addBCC('archive@ignition-innovations.com');
    $mail->Subject = 'Dealer Update ' . $un;

    $mail->Body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    
    <!-- Textarea Formatter Code -->
    <script src="http://cloud.tinymce.com/stable/tinymce.min.js?apiKey=wh7y9ve9aotjjcywsf2ittpjdbhlk1q7ur241kh8e9dnwlpe"></script>
    <script>tinymce.init({ selector:"textarea" });</script>
    
    
    <style>
      .m_span p, h1, h2, h3, h4, h5, h6{
        color: black;
      }
    </style>
    
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no" /> <!-- disable auto telephone linking in iOS -->
		<title>Respmail is a response HTML email designed to work on all major email platforms and smartphones</title>
		<style type="text/css">
			/* RESET STYLES */
			html { background-color:#E1E1E1; margin:0; padding:0; }
			body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, "Lucida Grande", sans-serif;}
			table{border-collapse:collapse;}
			table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#7A7A7A;font-weight:normal;}
			img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
			a {text-decoration:none !important;border-bottom: 1px solid;}
			h1, h2, h3, h4, h5, h6{/*color:#5F5F5F;*/ font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}
			/* CLIENT-SPECIFIC STYLES */
			.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */
			table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */
			#outlook a{padding:0;} /* Force Outlook 2007 and up to provide a "view in browser" message. */
			img{-ms-interpolation-mode: bicubic;display:block;outline:none; text-decoration:none;} /* Force IE to smoothly render resized images. */
			body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; font-weight:normal!important;} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
			.ExternalClass td[class="ecxflexibleContainerBox"] h3 {padding-top: 10px !important;} /* Force hotmail to push 2-grid sub headers down */
			/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */
			/* ========== Page Styles ========== */
			h1{display:block;font-size:26px;font-style:normal;font-weight:normal;line-height:100%;}
			h2{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:120%;}
			h3{display:block;font-size:17px;font-style:normal;font-weight:normal;line-height:110%;}
			h4{display:block;font-size:18px;font-style:italic;font-weight:normal;line-height:100%;}
			.flexibleImage{height:auto;}
			.linkRemoveBorder{border-bottom:0 !important;}
			table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}
			body, #bodyTable{background-color:#E1E1E1;}
			#emailHeader{background-color:#E1E1E1;}
			#emailBody{background-color:#FFFFFF;}
			#emailFooter{background-color:#E1E1E1;}
			.nestedContainer{background-color:#F8F8F8; border:1px solid #CCCCCC;}
			.emailButton{background-color:#205478; border-collapse:separate;}
			.buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
			.buttonContent a{color:#FFFFFF; display:block; text-decoration:none!important; border:0!important;}
			.emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}
			.emailCalendarMonth{background-color:#205478; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}
			.emailCalendarDay{color:#205478; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}
			.imageContentText {margin-top: 10px;line-height:0;}
			.imageContentText a {line-height:0;}
			#invisibleIntroduction {display:none !important;} /* Removing the introduction text from the view */
			/*FRAMEWORK HACKS & OVERRIDES */
			span[class=ios-color-hack] a {color:#275100!important;text-decoration:none!important;} /* Remove all link colors in IOS (below are duplicates based on the color preference) */
			span[class=ios-color-hack2] a {color:#205478!important;text-decoration:none!important;}
			span[class=ios-color-hack3] a {color:#8B8B8B!important;text-decoration:none!important;}
			/* A nice and clean way to target phone numbers you want clickable and avoid a mobile phone from linking other numbers that look like, but are not phone numbers.  Use these two blocks of code to "unstyle" any numbers that may be linked.  The second block gives you a class to apply with a span tag to the numbers you would like linked and styled.
			Inspired by Campaign Monitor&apos;s article on using phone numbers in email: http://www.campaignmonitor.com/blog/post/3571/using-phone-numbers-in-html-email/.
			*/
			.a[href^="tel"], a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:none!important;cursor:default!important;}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:auto!important;cursor:default!important;}
			/* MOBILE STYLES */
			@media only screen and (max-width: 480px){
				/*////// CLIENT-SPECIFIC STYLES //////*/
				body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */
				/* FRAMEWORK STYLES */
				/*
				CSS selectors are written in attribute
				selector format to prevent Yahoo Mail
				from rendering media query styles on
				desktop.
				*/
				/*td[class="textContent"], td[class="flexibleContainerCell"] { width: 100%; padding-left: 10px !important; padding-right: 10px !important; }*/
				table[id="emailHeader"],
				table[id="emailBody"],
				table[id="emailFooter"],
				table[class="flexibleContainer"],
				td[class="flexibleContainerCell"] {width:100% !important;}
				td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {display: block;width: 100%;text-align: left;}
				/*
				The following style rule makes any
				image classed with "flexibleImage"
				fluid when the query activates.
				Make sure you add an inline max-width
				to those images to prevent them
				from blowing out.
				*/
				td[class="imageContent"] img {height:auto !important; width:100% !important; max-width:100% !important; }
				img[class="flexibleImage"]{height:auto !important; width:100% !important;max-width:100% !important;}
				img[class="flexibleImageSmall"]{height:auto !important; width:auto !important;}
				/*
				Create top space for every second element in a block
				*/
				table[class="flexibleContainerBoxNext"]{padding-top: 10px !important;}
				/*
				Make buttons in the email span the
				full width of their container, allowing
				for left- or right-handed ease of use.
				*/
				table[class="emailButton"]{width:100% !important;}
				td[class="buttonContent"]{padding:0 !important;}
				td[class="buttonContent"] a{padding:15px !important;}
			}
			/*  CONDITIONS FOR ANDROID DEVICES ONLY
			*   http://developer.android.com/guide/webapps/targeting.html
			*   http://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/ ;
			=====================================================*/
			@media only screen and (-webkit-device-pixel-ratio:.75){
				/* Put CSS for low density (ldpi) Android layouts in here */
			}
			@media only screen and (-webkit-device-pixel-ratio:1){
				/* Put CSS for medium density (mdpi) Android layouts in here */
			}
			@media only screen and (-webkit-device-pixel-ratio:1.5){
				/* Put CSS for high density (hdpi) Android layouts in here */
			}
			/* end Android targeting */
			/* CONDITIONS FOR IOS DEVICES ONLY
			=====================================================*/
			@media only screen and (min-device-width : 320px) and (max-device-width:568px) {
			}
			/* end IOS targeting */
		</style>
		<!--
			Outlook Conditional CSS
			These two style blocks target Outlook 2007 & 2010 specifically, forcing
			columns into a single vertical stack as on mobile clients. This is
			primarily done to avoid the "page break bug" and is optional.
			More information here:
			http://templates.mailchimp.com/development/css/outlook-conditional-css
		-->
		<!--[if mso 12]>
			<style type="text/css">
				.flexibleContainer{display:block !important; width:100% !important;}
			</style>
		<![endif]-->
		<!--[if mso 14]>
			<style type="text/css">
				.flexibleContainer{display:block !important; width:100% !important;}
			</style>
		<![endif]-->
	</head>
	<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

		<!-- CENTER THE EMAIL // -->
		<!--
		1.  The center tag should normally put all the
			content in the middle of the email page.
			I added "table-layout: fixed;" style to force
			yahoomail which by default put the content left.
		2.  For hotmail and yahoomail, the contents of
			the email starts from this center, so we try to
			apply necessary styling e.g. background-color.
		-->
		<center style="background-color:#E1E1E1;">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
				<tr>
					<td align="center" valign="top" id="bodyCell">

						<!-- EMAIL HEADER // -->
						<!--
							The table "emailBody" is the email&apos;s container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

							<!-- HEADER ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<!--
																		The "invisibleIntroduction" is the text used for short preview
																		of the email before the user opens it (50 characters max). Sometimes,
																		you do not want to show this message depending on your design but this
																		text is highly recommended.
																		You do not have to worry if it is hidden, the next <td> will automatically
																		center and apply to the width 100% and also shrink to 50% if the first <td>
																		is visible.
																	-->
																	<td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																						An update from All Steel Carports
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td align="right" valign="middle" class="flexibleContainerBox">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					<!-- CONTENT // -->
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																						<!--If you can&apos;t see this message, <a href="#" target="_blank" style="text-decoration:none;border-bottom:1px solid #828282;color:#828282;"><span style="color:#828282;">view&nbsp;it&nbsp;in&nbsp;your&nbsp;browser</span></a>.-->
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // END -->

						</table>
						<!-- // END -->

						<!-- EMAIL BODY // -->
						<!--
							The table "emailBody" is the email&apos;s container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">

							<!-- MODULE ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<!--
										The centering table keeps the content
										tables centered in the emailBody table,
										in case its width is set to 100%.
									-->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#3498db">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<!--
													The flexible container has a set width
													that gets overridden by the media query.
													Most content tables within can then be
													given 100% widths.
												-->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<!--
															The content table is the first element
																that&apos;s entirely separate from the structural
																framework of the email.
															-->
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	
                                    <!--<img src="http://burtonsolution.tech/allsteel/marketforce/img/logo2.png"><br>-->';
    switch ($_SESSION['org_id']) {
        case "738004":
        case "654321":
        case "832122":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="https://marketforceapp.com/assets/img/brands/acero-news.png" style="max-width: 1000px;"><br>';
            break;
        case "162534":
            $mail->Body .= '<td align="center" valign="top" class="textContent" style="Background-color: #A0A2A6">
                                                          <img src="https://marketforceapp.com/assets/img/brands/NorthEdgeSteel-01.png" style="max-width: 350px;"><br>';
            break;
        case "615243":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="https://marketforceapp.com/assets/img/brands/legacyMFlogo.png" style="max-width: 1000px;"><br>';
            break;

        case "329698":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="https://marketforceapp.com/assets/img/brands/integrity-logo2.png" style="max-width: 1000px;"><br>';
            break;

        case "532748":
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="https://marketforceapp.com/assets/img/brands/JMLogo_2.png" style="max-width: 600px;"><br>';
            break;
        default:
            $mail->Body .= '<td align="center" valign="top" class="textContent">
                                                             <img src="'.$_SESSION['org_logo_url_alt'].'" style="max-width: 1000px;"><br>';
    }
    $mail->Body .= '<!--<img src="http://beta.marketforceapp.com/assets/img/brands/Logo-mailing-allsteel.png"><br>-->
																		<!--<h1 style="color:#FFFFFF;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:35px;font-weight:normal;margin-bottom:5px;text-align:center;">Introduction header</h1>-->
																		<h2 style="text-align:center;font-weight:normal;font-family:Helvetica,Arial,sans-serif;font-size:23px;margin-bottom:10px;color:#205478;line-height:135%;">';
    switch ($_SESSION['org_id']) {
        case "162534":
            $mail->Body .= 'Building Beyond Measure';
            break;
        case "329698":
            $mail->Body .= 'We Specialize in Satisfied Customers';
            break;
        default:
            $mail->Body .= 'Affordable Buildings, Exceptional Quality!';
    }
    $mail->Body .= '</h2>
																		<div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:25px;margin-bottom:0;color:#FFFFFF;line-height:135%;">';
    switch ($_SESSION['org_id']) {
        case "832122":
            $mail->Body .= 'Acero TX News ';
            break;
        case "738004":
            $mail->Body .= 'Acero CA News ';
            break;
        case "654321":
            $mail->Body .= 'Acero MEX News ';
            break;
        case "162534":
            $mail->Body .= '';
            break;
        default:
            $mail->Body .= 'Dealer ';
    }
    $mail->Body .= ' Update ' . $un . '</div>

																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


						<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="90%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					<h3 mc:edit="header" style="color:black;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
                                          Email To: &nbsp;&nbsp;';
    switch ($_SESSION['org_id']) {
        case "832122":
            $mail->Body .= 'Acero TX Costumer';
            break;
        case "738004":
            $mail->Body .= 'Acero CA Dealers';
            break;
        case "654321":
            $mail->Body .= 'Acero MEX Dealers';
            break;
        case "162534":
            $mail->Body .= 'Dealer Communication';
            break;
        case "615243":
            $mail->Body .= 'Legacy Buildings Dealers';
            break;
        default:
            $mail->Body .= 'All  Carport Dealers';
    }
    $mail->Body .= '<br>Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . date("l, m/d/Y") . '<br>Topic: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $t . '</h3>

																					<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:17px;margin-bottom:0;color:black;line-height:135%;">
                                          <br><br>
                                            <span class="m_span">
                                          ' . $m . '
                                            </span>
                                          <!--<br><br>
                                          Thank you very much!
                                          <br><br>
                                          <strong>All Steel Carports</strong>-->
                                         </div>
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->
                      
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->



							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					<!--<h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Dear valued customer,</h3>-->
																					<!--<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">Thank you so much for your input in our training department. Together, what we can easily see is only a small percentage of what is possible!</div>-->
																				</td>
																			</tr>
																		</table><!-- If user is in "united metals" then show flyer, else show the other one -->';

    if ($_SESSION['org_id'] == '558558') {
        //$mail->Body .= '<img src="http://marketforceapp.com/assets/img/dealer-flyers/may-ums.jpg" style="max-width: 1000px;"/>';
    } else {
        // $mail->Body .= '<img src="http://marketforceapp.com/assets/img/dealer-flyers/may-asc.jpg" style="max-width: 1000px;"/>';
    }

    $mail->Body .= ' <!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->




              <!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#3498db">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%"';

    switch ($_SESSION['org_id']) {
        case "162534":
            $mail->Body .= 'style="Background-color: #A0A2A6"';
            break;
    }
    $mail->Body .= '>
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					
																					<div mc:edit="body" style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:20px;margin-bottom:0;color:#000000;line-height:135%;">
                                          <p style="color: #FFF; font-size: 40px;">';
    switch ($_SESSION['org_id']) {
        case "832122":
            $mail->Body .= '469.344.3587 ';
            break;
        case "738004":
            $mail->Body .= 'Acero CA ';
            break;
        case "654321":
            $mail->Body .= 'Acero MEX ';
            break;
        case "162534":
            $mail->Body .= '765.591.8080';
            break;
        case "329698":
            $mail->Body .= '(903) 255-7175';
            break;
        default:
            $mail->Body .= '800.730.7908 ';
    }
    $mail->Body .= '</p>
                                          <strong>Contact
                                          <br><br>&nbsp; &nbsp; <a href="#" style="color: black; list-style-type: none;">';
    switch ($_SESSION['org_id']) {
        case "832122":
            $mail->Body .= 'Acero TX ';
            break;
        case "738004":
            $mail->Body .= 'Acero CA ';
            break;
        case "654321":
            $mail->Body .= 'Acero MEX ';
            break;
        case "162534":
            $mail->Body .= 'info@northedgesteel.us';
            break;
        case "615243":
            $mail->Body .= 'orders@legacybuildings.us';
            break;
        case "532748":
            $mail->Body .= 'orders@renovationsjm.com';
            break;
        case "329698":
            $mail->Body .= 'integritycarports@gmail.com';
            break;
        case "841963":
            $mail->Body .= 'orders@starbuildingsandcarports.com';
            break;
        default:
            $mail->Body .= 'orders@allsteelcarports.com ';
    }
    $mail->Body .= '</a>
                                          <br>
                                          </strong>
                                         </div>
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->
							


							<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">
							<!-- FOOTER ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td valign="top" bgcolor="#E1E1E1">

																		<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																			<div>Copyright &#169; 2017 <a href="http://www.burtonsolution.tech" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">Market Force</span></a>. All&nbsp;rights&nbsp;reserved.</div>
																			<!--<div>If you do not want to recieve emails from us, you can <a href="#" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">unsubscribe</span></a>.</div>-->
																		</div>

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>

						</table>
						<!-- // END -->

					</td>
				</tr>
			</table>
		</center>
	</body>
	</html>';


    //If in BETA server, only send as a test to these addresses...
    if ($_SERVER['HTTP_HOST'] == 'beta.marketforceapp.com') {
        $mail->clearAllRecipients();
        $mail->addAddress('mburton3969@gmail.com');
        echo 'Test Mode - ';
    }


    //Send Email
    if (!$mail->send()) {
        echo 'Message could not be sent.<br>';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {

        echo 'Message has been sent';
    }
    //echo 'Email would be sent!';
    //echo 'Thank You!';
}//End Dealer Update Query
