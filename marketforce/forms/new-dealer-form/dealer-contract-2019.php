<?php
include_once('php/connection.php');

//Signature Box Plugin
echo'<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/south-street/jquery-ui.css" rel="stylesheet">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<!--[if IE]>
<script type="text/javascript" src="http://burtonsolution.tech/plugins/js/excanvas.js"></script>
<![endif]-->

<script type="text/javascript" src="http://burtonsolution.tech/plugins/jquery-signature-pack/jquery.ui.touch-punch.min.js"></script>
<link type="text/css" href="http://burtonsolution.tech/plugins/jquery-signature-pack/jquery.signature.css" rel="stylesheet">
<script type="text/javascript" src="http://burtonsolution.tech/plugins/jquery-signature-pack/jquery.signature.js"></script>
<style>
  body > iframe { display: none; }
  @media print {
        footer {page-break-after: always;}
      }
</style>';

$gsq = "SELECT * FROM `signatures` WHERE `dealer_id` = '" . $_GET['id'] . "'";
$gsg = mysqli_query($conn, $gsq) or die($conn->error);
$gsr = mysqli_fetch_array($gsg);

echo'<script>
  $(document).ready(function(){
$("#dsign").signature();
$("#rsign").signature();
  $("#dsign").signature("draw", ' . $gsr['dsign'] . ');
  $("#rsign").signature("draw", ' . $gsr['rsign'] . ');
  $("#dbutton_toggle_retry").toggle();
  $("#rbutton_toggle_retry").toggle();
  });

  function clear_sign(id){
  if(id == "rsign"){
  $("#rsign").signature("clear");
  }
  if(id == "dsign"){
  $("#dsign").signature("clear");
  }
  }

  function retry_sign(idx){
  if(idx == "rsign"){
  $("#rsign").signature("enable");
  $("#rbutton_toggle_retry").toggle();
  $("#rbutton_toggle_save").toggle();
  $("#rsign").signature("clear");
  }
  if(idx == "dsign"){
  $("#dsign").signature("enable");
  $("#dbutton_toggle_retry").toggle();
  $("#dbutton_toggle_save").toggle();
  $("#dsign").signature("clear");
  }
  }

  function save_sign(id){
  var did = "' . $_GET['id'] . '";
  var doc = "Dealer Contract";
  var dname = document.getElementById("dname").value;
  var dstate = document.getElementById("dstate").value;
  var ddl = document.getElementById("ddl").value;
  var ddob = document.getElementById("ddob").value;
  
  if(id == "rsign"){
  var rs = $("#rsign").signature("toJSON");
  $("#rsign").signature("disable");
  $("#rbutton_toggle_save").toggle();
  $("#rbutton_toggle_retry").toggle();
  }
  if(id == "dsign"){
  if(dname == ""){
    alert("Please Enter The Dealers Name");
    return;
  }
  if(ddl == ""){
    alert("Please Enter the Dealers Driver License Number");
    return;
  }
  if(dstate == ""){
    alert("Please Enter The State of The Drivers License");
    return;
  }
  if(ddob == ""){
    alert("Please enter the Date of Birth");
    return;
  }
  
  var ds = $("#dsign").signature("toJSON");
  $("#dsign").signature("disable");
  $("#dbutton_toggle_save").toggle();
  $("#dbutton_toggle_retry").toggle();
  }

  if (window.XMLHttpRequest) {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
  if (this.readyState==4 && this.status==200) {
  //document.getElementById("livesearch").innerHTML=this.responseText;
alert(this.responseText);

  if(id == "rsign"){
  $("#rsign").signature("disable");
  }
  if(id == "dsign"){
  $("#dsign").signature("disable");
  }

  }
  }
  xmlhttp.open("GET","php/save-signature.php"+
  "?did="+did+"&doc="+doc+"&rs="+rs+"&ds="+ds+"&type="+id+"&dname="+dname+"&dstate="+dstate+"&ddl="+ddl+"&ddob="+ddob,true);
  xmlhttp.send();
  }
  </script>';

//Load Variables
$id1 = $_GET['id'];

//Info Query
$q1 = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $id1 . "'";
$g1 = mysqli_query($conn, $q1) or die($conn->error);
$r1 = mysqli_fetch_array($g1);

if($gsr['ddate'] == '0000-00-00'){
  $day = date("d");
  $monthYear = date("F, Y");
}else{
  $day = date("d", strtotime($gsr['ddate']));
  $monthYear = date("F, Y", strtotime($gsr['ddate']));
}

echo '<html>
  <head>
  <title>Dealer Contract</title>
  <style>
  .main{
  width: 90%;
  margin: auto;
  }
  .header{
  //text-align: center;
  }
  .header img{
  pading-right: 10%;
  width: 30%;
  }
  .header-info{
  margin-left: 35%;
  }
  .header .info{
  position: absolute;
  right: 6%;
  top: 2%;
  }
  h1{
  text-align: center;
  }
  span{
  color: red;
  text-decoration: underline;
  font-size: 20px;
  }
  label{
  font-weight: bold;
  }
  .info{
  font-size: 15px;
  }
  #rsign, #dsign{
  width: 100%;
  height: 120px;
  overflow: hidden;
  }
  #smaller{
  font-size: 17px;
  }
  </style>
</head>
  <body>
  <div class="main">
  <div class="header">
  <!--<img src="images/all-steel-red.jpg" />-->
        <img src="' . $_SESSION['org_logo_url'] . '" style="width=100%;">
        <!--<img class="header-info" src="images/header-info.jpeg" />-->
        <div class="info">
        <label>' . $_SESSION['org_address'] . '<br>
        ' . $_SESSION['org_city'] . ', ' . $_SESSION['org_state'] . ' ' . $_SESSION['org_zip'] . '</label><br>
        Phone: ' . $_SESSION['org_phone'] . '<br>
        Email: ' . $_SESSION['org_email'] . '
        </div>
      </div>
  <h1>Dealer Services Contract</h1>
  <p id="smaller">
  This Agreement entered into this <strong><span id="smaller">' . $day . '</span></strong> day of <strong><span id="smaller">' . $monthYear . '</span></strong>,
  between ' . $_SESSION['org_full_name'] . ' (' . $_SESSION['org_abrev'] . ') of ' . $_SESSION['org_city'] . ', ' . $_SESSION['org_state'] . ' and <strong><span id="smaller">' . $r1['bizname'] . '</span></strong> (herein after "Dealer") for
  Dealer Services for ' . $_SESSION['org_abrev'] . ' products, for and in consideration of the mutual obligations and other good and valuable
  consideration, ' . $_SESSION['org_full_name'] . ' and Dealers hereby agrees as follows:
  <br>
  ' . $_SESSION['org_abrev'] . ' agrees to provide products, including, but not limited to steel carports or steel garages or storage structures to Dealer, ' . $_SESSION['org_abrev'] . ' will provide, at its sole discretion, display products which shall be used for display purposes only, for display at Dealer’s place of business.  ' . $_SESSION['org_abrev'] . ' will further provide information, such as brochures, catalogs and other information ' . $_SESSION['org_abrev'] . ' deems appropriate regarding products and pricing.  Prices on ' . $_SESSION['org_abrev'] . ' products will be established solely by ' . $_SESSION['org_abrev'] . ' and products will be sold in accordance with and under conditions set forth in the information provided.  It is agreed that the display products shall remain the property of ' . $_SESSION['org_abrev'] . ' and shall not be sold.  Dealer shall provide exclusive Dealer Services for the sale of ' . $_SESSION['org_abrev'] . ' products.  Dealer specifically agrees and warrants that Dealer will not sell any products, including, but not limited to steel carports, steel garages or steel storage structures for any other person or any other business entity during the term of this contract. Products not supplied by ' . $_SESSION['org_abrev'] . ', such as wooden sheds & play sets do not apply to this agreement.    
  <b>Commission:</b> ' . $_SESSION['org_abrev'] . ' agrees to pay 10% commission for any ' . $_SESSION['org_abrev'] . ' product sold by Dealer.  The commission is based on the actual sales price of the product <u>excluding any applicable tax.</u>  It is preferred that Dealer collects their commission as a down payment on the product <i>knowing over collections will delay installation of building.</i> In the event that the commission is not collected as a down payment, Dealer will forward the entire purchase price to ' . $_SESSION['org_abrev'] . ' and ' . $_SESSION['org_abrev'] . ' will pay the commission to the Dealer <i>upon installation and payment of the building..</i>  <b>For custom size orders, Dealer must collect 50% as down payment.  Dealer may keep their commission and forward the remainder of the down payment to ' . $_SESSION['org_abrev'] . '.</b>  ' . $_SESSION['org_abrev'] . ' agrees to be responsible for collection of any delinquent payment owed by the buyer.  
  <br>
  In the event ' . $_SESSION['org_abrev'] . ' is requested by Dealer to accept a credit or debit card for a down payment on any product, ' . $_SESSION['org_abrev'] . ' will be entitled to deduct 3% of Dealer’s commission to reimburse ' . $_SESSION['org_abrev'] . ' for costs and handling of the credit card transaction. 
  <br>
  ' . $_SESSION['org_full_name'] . ' will restrict placement of other ' . $_SESSION['org_abrev'] . ' dealers in close proximity to your whenever possible!  Exceptions arise as we work with our corporate partners who may locate a sales lot within your area for which we are required to supply our buildings to the corporate partner upon demand.  Your sales rep will notify you of any issue should this situation arise.  
  <br>
  This agreement may be terminated with 30 days <u style= "font-style: italic;">written notice</u> by either party. Dealer consent is granted for removal of displays. ' . $_SESSION['org_full_name'] . ' will make every arrangement to remove displays from the property within 30-60 days of <u style= "font-style: italic;">written</u> notification and Dealer will be responsible to remove any personal items stored in displays.. Dealer agrees that all sales derived within the 30 days of notification belong to ' . $_SESSION['org_full_name'] . ', unless displays are removed prior to the 30 days (whichever occurs first).  It is expressly agreed that any dispute, suit, claim or legal proceeding of any nature arising from the obligations under this agreement, shall be filed in a court of competent jurisdiction in ' . $_SESSION['org_county'] . ' County, ' . $_SESSION['org_state'] . ' and be controlled by the laws of the State of ' . $_SESSION['org_state'] . '.
  </p>
<!--<footer></footer>-->
  <p>
  <!--<strong>Authorized Dealer Representative:</strong><br><br>(Print)_________________________________________/(Signature)_________________________________________-->
  <strong>Authorized Dealer Representative Signature:</strong><br><br>
  <strong>Dealer Name:</strong><input type="text" id="dname" value="' . $gsr['dname'] . '" /> 
  <strong>DL#:</strong><input type="text" id="ddl" value="' . $gsr['ddl#'] . '" /> 
  <strong>DL State:</strong><input type="text" id="dstate" value="' . $gsr['dstate'] . '" maxlength="2" style="width:30px;" />
  <br>
  <strong>Date of Birth:</strong><input type="text" id="ddob" value="' . $gsr['ddob'] . '" />
  <br>
  <div id="dsign"></div><br>
  <button type="button" onclick="clear_sign(&apos;dsign&apos;);">Clear</button>
  <span id="dbutton_toggle_save"><button type="button" onclick="save_sign(&apos;dsign&apos;);">Save</button></span>
  <span id="dbutton_toggle_retry"><button type="button" onclick="retry_sign(&apos;dsign&apos;);">Retry Signature</button></span>

  </p>
  <p id="smaller">
  <strong>Phone#:</strong> ' . $r1['phone'] . ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Address:</strong> ' . $r1['address'] . ', ' . $r1['city'] . ', ' . $r1['state'] . ' ' . $r1['zip'] . '
  </p>
  <p>
  <!--<strong>Authorized ' . $_SESSION['org_full_name'] . ' Representative:</strong><br><br>(Print)_________________________________________/(Signature)_________________________________________-->
  <strong>Authorized ' . $_SESSION['org_full_name'] . ' Representative Signature:</strong><div id="rsign"></div><br>
  <button type="button" onclick="clear_sign(&apos;rsign&apos;);">Clear</button>
  <span id="rbutton_toggle_save"><button type="button" onclick="save_sign(&apos;rsign&apos;);">Save</button></span>
  <span id="rbutton_toggle_retry"><button type="button" onclick="retry_sign(&apos;rsign&apos;);">Retry Signature</button></span>
  <br><br><br>
  <footer></footer>
  </div>
  </body>
</html>';

echo '<script>
  //window.print();
  </script>';

?>