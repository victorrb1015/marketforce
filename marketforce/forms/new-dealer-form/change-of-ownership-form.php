<?php
include_once('php/connection.php');

//Signature Box Plugin
echo'<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/south-street/jquery-ui.css" rel="stylesheet">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>';


$lgsq = "SELECT * FROM `ownership_changes` WHERE `did` = '" . $_GET['id'] . "'";
$lgsg = mysqli_query($conn, $lgsq) or die($conn->error);
$lgsr = mysqli_fetch_array($lgsg);


//Load Variables
$id4 = $_GET['id'];

//Info Query
$q4 = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $id4 . "'";
$g4 = mysqli_query($conn, $q4) or die($conn->error);
$r4 = mysqli_fetch_array($g4);

echo '<html>
  <head>
  <title>Change of Ownership Form</title>
  <style>
  .main{
  width: 90%;
  margin: auto;
  }
  .header{
  //text-align: center;
  }
  .header img{
  pading-right: 10%;
  width: 30%;
  }
  .header-info{
  margin-left: 35%;
  }
  .header .info{
  position: absolute;
  right: 6%;
  top: 2%;
  }
  h1{
  text-align: center;
  }
  label{
  font-weight: bold;
  }
  .info{
  font-size: 15px;
  }
  #lsign{
  width: 80%;
  height: 80px;
  overflow: hidden;
  }
  .one{
    background: #D9EDF7;
    padding: 10px;
  }
  .two{
    background: #DFF0D8;
    padding: 10px;
  }
  .main_container{
    width:75%;
    margin: auto;
  }
  </style>
  <script src="js/ownership-change-handler.js"></script>
  </head>
  <body>
  <div class="main">
  <div class="header">
  <!--<img src="images/all-steel-red.jpg" />-->
        <img src="' . $_SESSION['org_logo_url'] . '" style="width=100%;">
        <!--<img class="header-info" src="images/header-info.jpeg" />-->
        <div class="info">
        <label>' . $_SESSION['org_address'] . '<br>
        ' . $_SESSION['org_city'] . ', ' . $_SESSION['org_state'] . ' ' . $_SESSION['org_zip'] . '</label><br>
        Phone: ' . $_SESSION['org_phone'] . '<br>
        Email: ' . $_SESSION['org_email'] . '
        </div>
      </div>
  <div class="main_container">
  <h1>Dealer Change of Ownership Form</h1>
  <p style="text-align:right; width: 80%; margin: auto;">
      <a onclick="printable();">
      <button type="button" style="background-color:blue; color:white; height: 5%; width: 15%;">
      <strong>Print This Form</strong>
      </button>
      </a>
      </p>
  <div style="font-size:22px;">
  <br>
  <section class="one">
  <h3><u>Current Dealer Name:</u></h3>
  <strong>Business Name:</strong> <span id="currentName">' . $r4['bizname'] . '</span>
  <br>
  <strong>Street Address:</strong> <span id="currentAddress">' . $r4['address'] . '</span>
  <br>
  <strong>City:</strong> <span id="currentCity">' . $r4['city'] . '</span> &nbsp; 
  <strong>State:</strong> <span id="currentState">' . $r4['state'] . '</span> &nbsp; 
  <strong>Zip:</strong> <span id="currentZip">' . $r4['zip'] . '</span>
  <br>';

if($r4['ownername'] == ''){
  echo '<strong>Owner Name:</strong> <span id="currentOwner">' . $r4['ownername'] . '</span>';
}else{
  echo '<strong>Owner Name:</strong> <span id="currentOwner">' . $r4['ownername'] . '</span>';
  }

echo '<br>
      <strong>Phone#:</strong> <span id="currentPhone">' . $r4['phone'] . '</span>
      <br>
      <strong>Email:</strong> <span id="currentEmail">' . $r4['email'] . '</span>
</section>
  <br><br>
  <section class="two">
  <h3><u>New Dealer Name:</u></h3>
  <input type="checkbox" id="sameAddress" /> Same address as above
  <br><br>
  <strong>New Business Name:</strong><input type="text" id="newName" value="' . $lgsr['newName'] . '" style="width:40%; height:30px;" />
  <br><br>
  <strong>New Address:</strong><input type="text" id="newAddress" value="' . $lgsr['newAddress'] . '" style="width:40%; height:30px;" />
  <br><br>
  <strong>City:</strong><input type="text" id="newCity" value="' . $lgsr['newCity'] . '" style="width:20%; height:30px;" />
  <strong>State:</strong><input type="text" id="newState" value="' . $lgsr['newState'] . '" style="width:20%; height:30px;" />
  <strong>Zip:</strong><input type="text" id="newZip" value="' . $lgsr['newZip'] . '" style="width:20%; height:30px;" />
  <br><br>
  <strong>Owner&apos;s Name:</strong><input type="text" id="newOwner" value="' . $lgsr['newOwner'] . '" style="width:40%; height:30px;" />
  <br><br>
  <strong>Business Phone:</strong><input type="text" id="newPhone" value="' . $lgsr['newPhone'] . '" style="width:40%; height:30px;" /> 
  <br><br>
  <strong>Email:</strong><input type="text" id="newEmail" value="' . $lgsr['newEmail'] . '" style="width:50%; height:30px;" />
  <input type="hidden" id="did" value="' . $id4 . '" />
  <input type="hidden" id="uid" value="' . $_SESSION['user_id'] . '" />
  </section>
  
  <p>
  <strong>Date:</strong> ' . date("l, F d, Y") . '
  </p>
  <div class="buttons" style="text-align:center;">
  <button type="button" onclick="save();" id="save_btn">Save Form</button>
  <button type="button" onclick="send_form();" id="submit_btn">Submit Form</button>
  </div>
  </div>
  </div>
  <footer></footer>
  </div>
  </body>
</html>';

echo '<script>
  //window.print();
  </script>';
?>