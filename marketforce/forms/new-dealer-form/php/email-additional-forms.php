<?php
include 'connection.php';

//Error Variable
$error = true;

//Load Variables
$form = $_GET['form'];
$id = $_GET['id'];
$rep = $_GET['rep'];

//Load Dealer Information
$q = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
$bName = mysqli_real_escape_string($conn, $r['bizname']);
$location = $r['city'] . ', ' . $r['state'];

//Load Rep information
$q2 = "SELECT * FROM `users` WHERE `ID` = '" . $rep . "'";
$g2 = mysqli_query($conn, $q2) or die($conn->error);
$r2 = mysqli_fetch_array($g2);
$remail = $r2['email'];
$rname = $r2['fname'] . ' ' . $r2['lname'];

include 'phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include 'phpmailsettings.php';
$mail->addAddress('michael@allsteelcarports.com');
//$mail->addAddress('info@burtonsolution.tech');
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
$mail->addCC('teresa@allsteelcarports.com');
$mail->addCC('jeffrey@allsteelcarports.com');
$mail->addBCC('archive@ignition-innovations.com');
$mail->addAddress($remail);

if($form == 'addon'){
  //Insert into Admin Que...
$deq = "SELECT * FROM `new_packets` WHERE `dealer_id` = '" . $id . "' AND `form_type` = 'Display Addon Form' AND `status` != 'Completed' AND `status` != 'Cancelled'";
$deg = mysqli_query($conn, $deq) or die($conn->error);
if(mysqli_num_rows($deg) == 0){
  $iq = "INSERT INTO `new_packets` 
        (`dealer_id`,`form_type`,`date`,`time`,`rep`,`name`,`status`)
        VALUES
        ('" . $id . "','Display Addon Form',CURRENT_DATE,CURRENT_TIME,'" . $rep . "','" . $bName . "','Pending')";
  mysqli_query($conn, $iq) or die($conn->error);
}else{
  //This will do nothing because it is already in the Que...
}
  
  $mail->Subject = 'Display Addon Form -- ' . $r['bizname'] . ' of ' . $r['city'] . ', ' . $r['state'];
  $mail->Body = '<html>
                  <head>
                  <style>
                    .main{
                      text-align:center;
                    }
                    .title{
                      font-size: 20px;
                    }
                    img{
                      width: 40%;
                    }
                    .blue{
                      color: blue;
                    }
                    a{
                      text-decoration: none;
                      color: black;
                    }
                    .btn{
                      background: #E64C3D;
                      padding: 10px;
                      border-radius: 10px;
                    }
                  </style>
                  <body>
                    <div class="main">
                    
                      <img src="http://marketforceapp.com/marketforce/img/Market%20Force%20Logo%202.png" />
                      <p class="title"><b>' . $r2['fname'] . ' ' . $r2['lname'] . '</b> filled out a new <b class="blue">Display Addon Form</b> 
                      for <b>' . $r['bizname'] . '</b> of ' . $r['city'] . ', ' . $r2r['state'] . '</p>
                      <br>
                      <u><p><b>Click the button below to open the form!</b></p></u>
                      <br>
                      <p class="link">
                      <a href="http://marketforceapp.com/marketforce/forms/new-dealer-form/printable-display-addon-form.php?id=' . $id . '">
                      <span class="btn">
                      <b>Open Document</b>
                      </span>
                      </a>
                      </p>
                      
                    </div>
                  </body>
                  </html>';
    
    //echo $mail->Body;
    $mail->send();
  $error = false;
  echo 'Your form was submitted! Make sure you have saved this form with the SAVE button at the bottom!';
}

  if($form == 'relocation'){
    //Insert into Admin Que...
$deq = "SELECT * FROM `new_packets` WHERE `dealer_id` = '" . $id . "' AND `form_type` = 'Display Relocation Form' AND `status` != 'Completed' AND `status` != 'Cancelled'";
$deg = mysqli_query($conn, $deq) or die($conn->error);
if(mysqli_num_rows($deg) == 0){
    $iq = "INSERT INTO `new_packets` 
        (`dealer_id`,`form_type`,`date`,`time`,`rep`,`name`,`status`)
        VALUES
        ('" . $id . "','Display Relocation Form',CURRENT_DATE,CURRENT_TIME,'" . $rep . "','" . $bName . "','Pending')";
  mysqli_query($conn, $iq) or die($conn->error);
}else{
  //Do nothing
}
    
    $mail->Subject = 'Display Relocation Form -- ' . $r['bizname'] . ' of ' . $r['city'] . ', ' . $r['state'];
  $mail->Body = '<html>
                  <head>
                  <style>
                    .main{
                      text-align:center;
                    }
                    .title{
                      font-size: 20px;
                    }
                    img{
                      width: 40%;
                    }
                    .blue{
                      color: blue;
                    }
                    a{
                      text-decoration: none;
                      color: black;
                    }
                    .btn{
                      background: #E64C3D;
                      padding: 10px;
                      border-radius: 10px;
                    }
                  </style>
                  <body>
                    <div class="main">
                    
                      <img src="http://marketforceapp.com/marketforce/img/Market%20Force%20Logo%202.png" />
                      <p class="title"><b>' . $r2['fname'] . ' ' . $r2['lname'] . '</b> filled out a new <b class="blue">Display Relocation Form</b> 
                      for <b>' . $r['bizname'] . '</b> of ' . $r['city'] . ', ' . $r['state'] . '</p>
                      <br>
                      <u><p><b>Click the button below to open the form!</b></p></u>
                      <br>
                      <p class="link">
                      <a href="http://marketforceapp.com/marketforce/forms/new-dealer-form/printable-display-relocation-form.php?id=' . $id . '">
                      <span class="btn">
                      <b>Open Document</b>
                      </span>
                      </a>
                      </p>
                      
                    </div>
                  </body>
                  </html>';
    
    //echo $mail->Body;
    $mail->send();
  $error = false;
    echo 'Your form was submitted! Make sure you have saved this form with the SAVE button at the bottom!';
  }


if($form == 'removal'){
  //Insert into Admin Que...
$deq = "SELECT * FROM `new_packets` WHERE `dealer_id` = '" . $id . "' AND `form_type` = 'Display Removal Form' AND `status` != 'Completed' AND `status` != 'Cancelled'";
$deg = mysqli_query($conn, $deq) or die($conn->error);
if(mysqli_num_rows($deg) == 0){
  $iq = "INSERT INTO `new_packets` 
        (`dealer_id`,`form_type`,`date`,`time`,`rep`,`name`,`status`)
        VALUES
        ('" . $id . "','Display Removal Form',CURRENT_DATE,CURRENT_TIME,'" . $rep . "','" . $bName . "','Pending')";
  mysqli_query($conn, $iq) or die($conn->error);
}else{
  
}
  
   $mail->Subject = 'Display Removal Form -- ' . $r['bizname'] . ' of ' . $r['city'] . ', ' . $r['state'];
  $mail->Body = '<html>
                  <head>
                  <style>
                    .main{
                      text-align:center;
                    }
                    .title{
                      font-size: 20px;
                    }
                    img{
                      width: 40%;
                    }
                    .blue{
                      color: blue;
                    }
                    a{
                      text-decoration: none;
                      color: black;
                    }
                    .btn{
                      background: #E64C3D;
                      padding: 10px;
                      border-radius: 10px;
                    }
                  </style>
                  <body>
                    <div class="main">
                    
                      <img src="http://marketforceapp.com/marketforce/img/Market%20Force%20Logo%202.png" />
                      <p class="title"><b>' . $r2['fname'] . ' ' . $r2['lname'] . '</b> filled out a new <b class="blue">Display Removal Form</b> 
                      for <b>' . $r['bizname'] . '</b> of ' . $r['city'] . ', ' . $r['state'] . '</p>
                      <br>
                      <u><p><b>Click the button below to open the form!</b></p></u>
                      <br>
                      <p class="link">
                      <a href="http://marketforceapp.com/marketforce/forms/new-dealer-form/printable-display-removal-form.php?id=' . $id . '">
                      <span class="btn">
                      <b>Open Document</b>
                      </span>
                      </a>
                      </p>
                      
                    </div>
                  </body>
                  </html>';
    
    //echo $mail->Body;
    $mail->send();
  $error = false;
  echo 'Your form was submitted! Make sure you have saved this form with the SAVE button at the bottom!';
}


if($form == 'closing'){
  //Insert into Admin Que...
$deq = "SELECT * FROM `new_packets` WHERE `dealer_id` = '" . $id . "' AND `form_type` = 'Dealer Closing Form' AND `status` != 'Completed' AND `status` != 'Cancelled'";
$deg = mysqli_query($conn, $deq) or die($conn->error);
if(mysqli_num_rows($deg) == 0){
  $iq = "INSERT INTO `new_packets` 
        (`dealer_id`,`form_type`,`date`,`time`,`rep`,`name`,`status`)
        VALUES
        ('" . $id . "','Dealer Closing Form',CURRENT_DATE,CURRENT_TIME,'" . $rep . "','" . $bName . "','Pending')";
  mysqli_query($conn, $iq) or die($conn->error);
}else{
  
}
  
  $mail->Subject = 'Dealer Closing Form -- ' . $bName. ' of ' . $location;
  $mail->Body = '<html>
                  <head>
                  <style>
                    .main{
                      text-align:center;
                    }
                    .title{
                      font-size: 20px;
                    }
                    img{
                      width: 40%;
                    }
                    .blue{
                      color: blue;
                    }
                    a{
                      text-decoration: none;
                      color: black;
                    }
                    .btn{
                      background: #E64C3D;
                      padding: 10px;
                      border-radius: 10px;
                    }
                  </style>
                  <body>
                    <div class="main">
                    
                      <img src="http://marketforceapp.com/marketforce/img/Market%20Force%20Logo%202.png" />
                      <p class="title"><b>' . $rname . '</b> filled out a new <b class="blue">Dealer Closing Form</b> 
                      for <b>' . $bName . '</b> of ' . $location . '</p>
                      <br>
                      <u><p><b>Click the button below to open the form!</b></p></u>
                      <br>
                      <p class="link">
                      <a href="http://marketforceapp.com/marketforce/forms/new-dealer-form/printable-dealer-closing-form.php?id=' . $id . '">
                      <span class="btn">
                      <b>Open Document</b>
                      </span>
                      </a>
                      </p>
                      
                    </div>
                  </body>
                  </html>';
    
    //echo $mail->Body;
    $mail->send();
  $error = false;
  echo 'Your form was submitted! Make sure you have saved this form with the SAVE button at the bottom!';
}




if($error == true){
  echo 'There was an error submitting this form!';
}

?>