<?php
include 'connection.php';
//Load Variables
$did = $_GET['did'];
$uid = $_GET['uid'];

$oldName = mysqli_real_escape_string($conn, $_GET['cname']);
$oldAddress = mysqli_real_escape_string($conn, $_GET['caddress']);
$oldCity = mysqli_real_escape_string($conn, $_GET['ccity']);
$oldState = mysqli_real_escape_string($conn, $_GET['cstate']);
$oldZip = mysqli_real_escape_string($conn, $_GET['czip']);
$oldOwner = mysqli_real_escape_string($conn, $_GET['cowner']);
$oldPhone = mysqli_real_escape_string($conn, $_GET['cphone']);
$oldEmail = mysqli_real_escape_string($conn, $_GET['cemail']);

$newName = mysqli_real_escape_string($conn, $_GET['nname']);
$newAddress = mysqli_real_escape_string($conn, $_GET['naddress']);
$newCity = mysqli_real_escape_string($conn, $_GET['ncity']);
$newState = mysqli_real_escape_string($conn, $_GET['nstate']);
$newZip = mysqli_real_escape_string($conn, $_GET['nzip']);
$newOwner = mysqli_real_escape_string($conn, $_GET['nowner']);
$newPhone = mysqli_real_escape_string($conn, $_GET['nphone']);
$newEmail = mysqli_real_escape_string($conn, $_GET['nemail']);


$cq = "SELECT * FROM `ownership_changes` WHERE `did` = '" . $did . "'";
$cg = mysqli_query($conn,$cq) or die($conn->error);
if(mysqli_num_rows($cg) == 0){
  $iq = "INSERT INTO `ownership_changes` (`did`) VALUES ('" . $did . "')";
  $ig = mysqli_query($conn, $iq) or die($conn->error);
}

$uq = "UPDATE
       `ownership_changes`
       SET
       `did` = '" . $did . "',
       `uid` = '" . $uid . "',
       `oldName` = '" . $oldName . "',
       `oldAddress` = '" . $oldAddress . "',
       `oldCity` = '" . $oldCity . "',
       `oldState` = '" . $oldState . "',
       `oldZip` = '" . $oldZip . "',
       `oldOwner` = '" . $oldOwner . "',
       `oldPhone` = '" . $oldPhone . "',
       `oldEmail` = '" . $oldEmail . "',
       `newName` = '" . $newName . "',
       `newAddress` = '" . $newAddress . "',
       `newCity` = '" . $newCity . "',
       `newState` = '" . $newState . "',
       `newZip` = '" . $newZip . "',
       `newOwner` = '" . $newOwner . "',
       `newPhone` = '" . $newPhone . "',
       `newEmail` = '" . $newEmail . "'
       WHERE
       `did` = '" . $did . "'";

$ug = mysqli_query($conn, $uq) or die($conn->error);

echo 'Your form has been saved!';


if($_GET['request'] == 'send'){
  
  $unq = "SELECT * FROM `users` WHERE `ID` = '" . $uid . "'";
  $ung = mysqli_query($conn, $unq) or die($conn->error);
  $unr = mysqli_fetch_array($ung);
  
  //Insert into Admin Que...
$deq = "SELECT * FROM `new_packets` WHERE `dealer_id` = '" . $idr['ID'] . "' AND `form_type` = 'Ownership Change Form'";
$deg = mysqli_query($conn, $deq) or die($conn->error);
if(mysqli_num_rows($deg) == 0){
  $iiq = "INSERT INTO `new_packets` 
        (`dealer_id`,`form_type`,`date`,`time`,`rep`,`name`,`status`)
        VALUES
        ('" . $did . "','Ownership Change Form',CURRENT_DATE,CURRENT_TIME,'" . $uid . "','" . $oldName . "','Pending')";
  mysqli_query($conn, $iiq) or die($conn->error);
}else{
  //Do Nothing...
}
  
  include 'phpmailer/PHPMailerAutoload.php';
  $mail = new PHPMailer;
  include 'phpmailsettings.php';
  
  //$mail->addAddress('michael@allsteelcarports.com');
$mail->addAddress('info@burtonsolution.tech');
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
$mail->addBCC('michael@burtonsolution.tech');
  
   $mail->Subject = 'Dealer Ownership Change Form';
  $mail->Body = '<html>
                  <head>
                  <style>
                    .main{
                      text-align:center;
                    }
                    .title{
                      font-size: 20px;
                    }
                    img{
                      width: 40%;
                    }
                    .blue{
                      color: blue;
                    }
                    a{
                      text-decoration: none;
                      color: black;
                    }
                    .btn{
                      background: #E64C3D;
                      padding: 10px;
                      border-radius: 10px;
                    }
                  </style>
                  <body>
                    <div class="main">
                    
                      <img src="http://marketforceapp.com/marketforce/img/Market%20Force%20Logo%202.png" />
                      <p class="title"><b>' . $unr['fname'] . ' ' . $unr['lname'] . '</b> filled out a new <b class="blue">Dealer Ownership Change Form</b> 
                      for <b>' . $oldName . '</b> of ' . $oldCity . ', ' . $oldState . '</p>
                      <br>
                      <u><p><b>Click the button below to open the form!</b></p></u>
                      <br>
                      <p class="link">
                      <a href="http://marketforceapp.com/marketforce/forms/new-dealer-form/printable-ownership-change-form.php?id=' . $did . '">
                      <span class="btn">
                      <b>Open Document</b>
                      </span>
                      </a>
                      </p>
                      
                    </div>
                  </body>
                  </html>';
    
    //echo $mail->Body;
    $mail->send();
  $error = false;
  echo ' Your form was submitted!';
  
}

?>




