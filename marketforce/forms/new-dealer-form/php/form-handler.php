<?php
include '../../../php/connection.php';

//Load Variables From Form
$user = mysqli_real_escape_string($conn, $_POST['user']);
$bizname = mysqli_real_escape_string($conn, $_POST['bizname']);
$address = mysqli_real_escape_string($conn, $_POST['address']);
$city = mysqli_real_escape_string($conn, $_POST['city']);
$state = mysqli_real_escape_string($conn, $_POST['state']);
$zip = mysqli_real_escape_string($conn, $_POST['zip']);
$dcphone = mysqli_real_escape_string($conn, $_POST['dcphone']);
$dcphone = preg_replace("/[^0-9,.]/", "", $dcphone);
$dcphone = "(" . substr($dcphone,0,3) . ") " . substr($dcphone,3,3) . "-" . substr($dcphone,6);
$phone = mysqli_real_escape_string($conn, $_POST['phone']);
$phone = preg_replace("/[^0-9,.]/", "", $phone);
$phone = "(" . substr($phone,0,3) . ") " . substr($phone,3,3) . "-" . substr($phone,6);
$fax = mysqli_real_escape_string($conn, $_POST['fax']);
if($fax == ''){
	$fax = 'none';
}else{
$fax = preg_replace("/[^0-9,.]/", "", $fax);
$fax = "(" . substr($fax,0,3) . ") " . substr($fax,3,3) . "-" . substr($fax,6);
}

//Mailing Address Info
$maddress = mysqli_real_escape_string($conn, $_POST['maddress']);
$mcity = mysqli_real_escape_string($conn, $_POST['mcity']);
$mstate = mysqli_real_escape_string($conn, $_POST['mstate']);
$mzip = mysqli_real_escape_string($conn, $_POST['mzip']);


$property = mysqli_real_escape_string($conn, $_POST['property']);
$expiration = mysqli_real_escape_string($conn, $_POST['expiration']);
if($expiration == 'mm/dd/yyyy'){
	$expiration = 'none';
}
$email = mysqli_real_escape_string($conn, $_POST['email']);
if($email == ''){
	$email = 'none';
}
$website = mysqli_real_escape_string($conn, $_POST['website']);
if($website == ''){
	$website = 'none';
}
$years = mysqli_real_escape_string($conn, $_POST['years']);
$numbers = mysqli_real_escape_string($conn, $_POST['numbers']);
$EIN = mysqli_real_escape_string($conn, $_POST['EIN']);
if($EIN == ''){
	$EIN = 'none';
}
$name1 = mysqli_real_escape_string($conn, $_POST['name1']);
$email1 = mysqli_real_escape_string($conn, $_POST['email1']);
if($email1 == ''){
	$email1 = 'none';
}
$name2 = mysqli_real_escape_string($conn, $_POST['name2']);
if($name2 == ''){
	$name2 = 'none';
}
$email2 = mysqli_real_escape_string($conn, $_POST['email2']);
if($email2 == ''){
	$email2 = 'none';
}
//$products = mysqli_real_escape_string($conn, $_POST['products']);
$fulltime = mysqli_real_escape_string($conn, $_POST['fulltime']);
$parttime = mysqli_real_escape_string($conn, $_POST['parttime']);
$ownername = mysqli_real_escape_string($conn, $_POST['ownername']);
$owneraddress = mysqli_real_escape_string($conn, $_POST['owneraddress']);
$ownercity = mysqli_real_escape_string($conn, $_POST['ownercity']);
$ownerstate = mysqli_real_escape_string($conn, $_POST['ownerstate']);
$ownerzip = mysqli_real_escape_string($conn, $_POST['ownerzip']);
$ownerphone = mysqli_real_escape_string($conn, $_POST['ownerphone']);
$ownerphone = preg_replace("/[^0-9,.]/", "", $ownerphone);
$ownerphone = "(" . substr($ownerphone,0,3) . ") " . substr($ownerphone,3,3) . "-" . substr($ownerphone,6);
$altphone = mysqli_real_escape_string($conn, $_POST['altphone']);
if($altphone == ''){
	$altphone = 'none';
}else{
$altphone = preg_replace("/[^0-9,.]/", "", $altphone);
$altphone = "(" . substr($altphone,0,3) . ") " . substr($altphone,3,3) . "-" . substr($altphone,6);
}
$mainroad = mysqli_real_escape_string($conn, $_POST['mainroad']);
$visibility = mysqli_real_escape_string($conn, $_POST['visibility']);
$displayspace = mysqli_real_escape_string($conn, $_POST['displayspace']);
$currentcompany = mysqli_real_escape_string($conn, $_POST['currentcompany']);
$who = mysqli_real_escape_string($conn, $_POST['who']);
if($who == ''){
	$who = 'none';
}
$salesarea = mysqli_real_escape_string($conn, $_POST['salesarea']);
$where = mysqli_real_escape_string($conn, $_POST['where']);
if($where == ''){
	$where = 'none';
}
$otherprod = mysqli_real_escape_string($conn, $_POST['otherprod']);
$closedealers = mysqli_real_escape_string($conn, $_POST['closedealers']);
$rto = mysqli_real_escape_string($conn, $_POST['rto']);
$bbb = mysqli_real_escape_string($conn, $_POST['bbb']);
if($bbb == ''){
	$bbb = 'none';
}
$mantra = mysqli_real_escape_string($conn, $_POST['mantra']);
if($mantra == ''){
	$mantra = 'none';
}

//Insert into the main Dealer list for note additions
$xxq = "INSERT INTO `dealers` (`name`,`address`,`city`,`state`,`zip`,`phone`,`fax`,`email`,`status`,`commision`)
				VALUES
				('" . $bizname . "',
				 '" . $address . "',
				 '" . $city . "',
				 '" . $state . "',
				 '" . $zip . "',
				 '" . $phone . "',
				 '" . $fax . "',
				 '" . $email . "',
				 'New',
				 '10%')";
$xxg = mysqli_query($conn, $xxq) or die($conn->error);

//Get Submission ID
$idq = "SELECT * FROM `dealers` ORDER BY `ID` DESC LIMIT 1";
$idg = mysqli_query($conn, $idq) or die($conn->error);
$idr = mysqli_fetch_array($idg);

//INSERT DATA INTO DATABASE
$i = "INSERT
      INTO
      `new_dealer_form`
      (`ID`,`user`,`date`,`time`,`bizname`,`address`,`city`,`state`,`zip`,`dcphone`,`phone`,`fax`,`maddress`,`mcity`,`mstate`,`mzip`,`property`,
       `expiration`,`email`,`website`,`years`,`numbers`,`EIN`,`name1`,`email1`,`name2`,`email2`,
       `fulltime`,`parttime`,`ownername`,`owneraddress`,`ownercity`,`ownerstate`,`ownerzip`,`ownerphone`,`altphone`,`mainroad`,`visibility`,`displayspace`,
       `currentcompany`,`who`,`salesarea`,`where`,`products`,`closedealers`,`rto`,`bbb`,`mantra`)
       VALUES
       ('" . $idr['ID'] . "',
			 '" . $user . "',
        CURRENT_DATE,
        CURRENT_TIME,
        '" . $bizname . "',
        '" . $address . "',
        '" . $city . "',
        '" . $state . "',
        '" . $zip . "',
				'" . $dcphone . "',
        '" . $phone . "',
        '" . $fax . "',
				'" . $maddress . "',
				'" . $mcity . "',
				'" . $mstate . "',
				'" . $mzip . "',
        '" . $property . "',
        '" . $expiration . "',
        '" . $email . "',
        '" . $website . "',
        '" . $years . "',
        '" . $numbers . "',
        '" . $EIN . "',
        '" . $name1 . "',
        '" . $email1 . "',
        '" . $name2 . "',
        '" . $email2 . "',
        '" . $fulltime . "',
        '" . $parttime . "',
        '" . $ownername . "',
        '" . $owneraddress . "',
				'" . $ownercity . "',
				'" . $ownerstate . "',
				'" . $ownerzip . "',
        '" . $ownerphone . "',
        '" . $altphone . "',
        '" . $mainroad . "',
        '" . $visibility . "',
        '" . $displayspace . "',
        '" . $currentcompany . "',
        '" . $who . "',
        '" . $salesarea . "',
        '" . $where . "',
        '" . $otherprod . "',
        '" . $closedealers . "',
        '" . $rto . "',
        '" . $bbb . "',
        '" . $mantra . "')";

$ig = mysqli_query($conn, $i) or die($conn->error);



//Insert into the Display Orders database
$dofq = "INSERT INTO `display_orders`
			 (`ID`,
			 `tcg`,
       `tcg_v1`,
       `tcg_v2`,
       `tcg_v3`,
       `tcg_v4`,
       `tcg_v5`,
       `tcg_v6`,
       `tcg_price`,
			 `ip`,
			 `ip_price`,
       `g`,
       `g_v1`,
       `g_v2`,
       `g_v3`,
       `g_v4`,
       `g_v5`,
       `g_price`,
       `cua`,
       `cua_v1`,
       `cua_v2`,
       `cua_v3`,
       `cua_v4`,
       `cua_price`,
       `cur`,
       `cur_v1`,
       `cur_v2`,
       `cur_v3`,
       `cur_v4`,
       `cur_price`,
       `rvp`,
       `rvp_v1`,
       `rvp_v2`,
       `rvp_v3`,
       `rvp_v4`,
       `rvp_v5`,
       `rvp_price`,
       `cst`,
       `cst_v1`,
       `cst_v2`,
       `cst_v3`,
       `cst_price`,
       `csp`,
       `csp_v1`,
       `csp_v2`,
       `csp_v3`,
       `csp_v4`,
       `csp_v5`,
       `csp_price`,
       `vb`,
       `vb_v1`,
       `vb_v2`,
       `vb_v3`,
       `vb_v4`,
       `vb_v5`,
       `vb_price`,
       `u`,
       `u_v1`,
       `u_v2`,
       `u_v3`,
       `u_v4`,
       `u_v5`,
       `u_price`,
       `au`,
       `au_v1`, 
       `au_v2`, 
       `au_v3`, 
       `au_v4`,
       `au_price`, 
       `us`,
       `us_v1`, 
       `us_v2`, 
       `us_v3`, 
       `us_v4`,
       `us_price`, 
       `vu`,
       `vu_v1`, 
       `vu_v2`, 
       `vu_v3`,
       `vu_v4`,
       `vu_price`)
			 VALUES
			 ('" . $idr['ID'] . "',
			 'false','22','26','8','2','1','1','5800.00',
			 'false','',
			 	'false','18','26','8','1','1','4400.00',
				'false','18','31','7','1','3250.00',
				'false','18','31','6','1','3100.00',
				'false','12','31','12','2','6','2400.00',
				'false','18','21','6','800.00',
				'false','24','21','7','1','1','2000.00',
				'false','20','21','9','1','1','5265.00',
				'false','12','20','7','1','1','4000.00',
				'false','10','12','7','1','2095.00',
				'false','8','12','6','1','1695.00',
				'false','10','12','7','1','2595.00')";
//$dofg = mysqli_query($conn, $dofq) or die($conn->error);

//Enter into Dealer_Closing table
$dcq = "INSERT INTO `dealer_closing`
			 (`ID`,
			 `tcg`,
       `tcg_v1`,
       `tcg_v2`,
       `tcg_v3`,
       `tcg_v4`,
       `tcg_v5`,
       `tcg_v6`,
       `tcg_price`,
       `g`,
       `g_v1`,
       `g_v2`,
       `g_v3`,
       `g_v4`,
       `g_v5`,
       `g_price`,
       `cua`,
       `cua_v1`,
       `cua_v2`,
       `cua_v3`,
       `cua_v4`,
       `cua_price`,
       `cur`,
       `cur_v1`,
       `cur_v2`,
       `cur_v3`,
       `cur_v4`,
       `cur_price`,
       `rvp`,
       `rvp_v1`,
       `rvp_v2`,
       `rvp_v3`,
       `rvp_v4`,
       `rvp_v5`,
       `rvp_price`,
       `cst`,
       `cst_v1`,
       `cst_v2`,
       `cst_v3`,
       `cst_price`,
       `csp`,
       `csp_v1`,
       `csp_v2`,
       `csp_v3`,
       `csp_v4`,
       `csp_v5`,
       `csp_price`,
       `vb`,
       `vb_v1`,
       `vb_v2`,
       `vb_v3`,
       `vb_v4`,
       `vb_v5`,
       `vb_price`,
       `u`,
       `u_v1`,
       `u_v2`,
       `u_v3`,
       `u_v4`,
       `u_v5`,
       `u_price`,
       `au`,
       `au_v1`, 
       `au_v2`, 
       `au_v3`, 
       `au_v4`,
       `au_price`, 
       `us`,
       `us_v1`, 
       `us_v2`, 
       `us_v3`, 
       `us_v4`,
       `us_price`, 
       `vu`,
       `vu_v1`, 
       `vu_v2`, 
       `vu_v3`,
       `vu_v4`,
       `vu_price`)
			 VALUES
			 ('" . $idr['ID'] . "',
			 'false','22','26','8','2','1','1','5800.00',
			 	'false','18','26','8','1','1','4400.00',
				'false','18','31','7','1','3250.00',
				'false','18','31','6','1','3100.00',
				'false','12','31','12','2','6','2400.00',
				'false','18','21','6','800.00',
				'false','24','21','7','1','1','2000.00',
				'false','20','21','9','1','1','5265.00',
				'false','12','20','7','1','1','4000.00',
				'false','10','12','7','1','2095.00',
				'false','8','12','6','1','1695.00',
				'false','10','12','7','1','2595.00')";
  //mysqli_query($conn, $dcq) or die($conn->error);

//Insert into Display_addons table
$q = "INSERT INTO `display_addons`
			 (`ID`,
			 `tcg`,
       `tcg_v1`,
       `tcg_v2`,
       `tcg_v3`,
       `tcg_v4`,
       `tcg_v5`,
       `tcg_v6`,
       `tcg_price`,
			 `ip`,
			 `ip_price`,
       `g`,
       `g_v1`,
       `g_v2`,
       `g_v3`,
       `g_v4`,
       `g_v5`,
       `g_price`,
       `cua`,
       `cua_v1`,
       `cua_v2`,
       `cua_v3`,
       `cua_v4`,
       `cua_price`,
       `cur`,
       `cur_v1`,
       `cur_v2`,
       `cur_v3`,
       `cur_v4`,
       `cur_price`,
       `rvp`,
       `rvp_v1`,
       `rvp_v2`,
       `rvp_v3`,
       `rvp_v4`,
       `rvp_v5`,
       `rvp_price`,
       `cst`,
       `cst_v1`,
       `cst_v2`,
       `cst_v3`,
       `cst_price`,
       `csp`,
       `csp_v1`,
       `csp_v2`,
       `csp_v3`,
       `csp_v4`,
       `csp_v5`,
       `csp_price`,
       `vb`,
       `vb_v1`,
       `vb_v2`,
       `vb_v3`,
       `vb_v4`,
       `vb_v5`,
       `vb_price`,
       `u`,
       `u_v1`,
       `u_v2`,
       `u_v3`,
       `u_v4`,
       `u_v5`,
       `u_price`,
       `au`,
       `au_v1`, 
       `au_v2`, 
       `au_v3`, 
       `au_v4`,
       `au_price`, 
       `us`,
       `us_v1`, 
       `us_v2`, 
       `us_v3`, 
       `us_v4`,
       `us_price`, 
       `vu`,
       `vu_v1`, 
       `vu_v2`, 
       `vu_v3`,
       `vu_v4`,
       `vu_price`)
			 VALUES
			 ('" . $idr['ID'] . "',
			 'false','22','26','8','2','1','1','5800.00',
			 'false','',
			 	'false','18','26','8','1','1','4400.00',
				'false','18','31','7','1','3250.00',
				'false','18','31','6','1','3100.00',
				'false','12','31','12','2','6','2400.00',
				'false','18','21','6','800.00',
				'false','24','21','7','1','1','2000.00',
				'false','20','21','9','1','1','5265.00',
				'false','12','20','7','1','1','4000.00',
				'false','10','12','7','1','2095.00',
				'false','8','12','6','1','1695.00',
				'false','10','12','7','1','2595.00')";
  //mysqli_query($conn, $q) or die($conn->error);

//Create Slot for E-Signatures
$bit = '{"lines":[]}';
$esq = "INSERT INTO `signatures` (`ID`,`dealer_id`,`rsign`,`dsign`,`lsign`) VALUES ('" . $idr['ID'] . "','" . $idr['ID'] . "','" . $bit . "','" . $bit . "','" . $bit . "')";
$esg = mysqli_query($conn, $esq) or die($conn->error);

//Email Submission
include_once('phpmailer/PHPMailerAutoload.php');
$mail = new PHPMailer;
include_once('phpmailsettings.php');

$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
$mail->addAddress('archive@ignition-innovations.com');
//$mail->addAddress('michael@allsteelcarports.com');
//$mail->addBCC('michael@burtonsolution.tech');
$mail->Subject = $bizname . ' of ' . $city . ', ' . $state . ' - New Dealer Packet';
$mail->Body = '
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no" /> <!-- disable auto telephone linking in iOS -->
		<title>New Dealer Notification</title>
		<style type="text/css">
			/* RESET STYLES */
			html { background-color:#E1E1E1; margin:0; padding:0; }
			body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, "Lucida Grande", sans-serif;}
			table{border-collapse:collapse;}
			table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#7A7A7A;font-weight:normal;}
			img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
			a {text-decoration:none !important;border-bottom: 1px solid;}
			h1, h2, h3, h4, h5, h6{color:#5F5F5F; font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}
			/* CLIENT-SPECIFIC STYLES */
			.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */
			table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */
			#outlook a{padding:0;} /* Force Outlook 2007 and up to provide a "view in browser" message. */
			img{-ms-interpolation-mode: bicubic;display:block;outline:none; text-decoration:none;} /* Force IE to smoothly render resized images. */
			body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; font-weight:normal!important;} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
			.ExternalClass td[class="ecxflexibleContainerBox"] h3 {padding-top: 10px !important;} /* Force hotmail to push 2-grid sub headers down */
			/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */
			/* ========== Page Styles ========== */
			h1{display:block;font-size:26px;font-style:normal;font-weight:normal;line-height:100%;}
			h2{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:120%;}
			h3{display:block;font-size:17px;font-style:normal;font-weight:normal;line-height:110%;}
			h4{display:block;font-size:18px;font-style:italic;font-weight:normal;line-height:100%;}
			.flexibleImage{height:auto;}
			.linkRemoveBorder{border-bottom:0 !important;}
			table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}
			body, #bodyTable{background-color:#E1E1E1;}
			#emailHeader{background-color:#E1E1E1;}
			#emailBody{background-color:#FFFFFF;}
			#emailFooter{background-color:#E1E1E1;}
			.nestedContainer{background-color:#F8F8F8; border:1px solid #CCCCCC;}
			.emailButton{background-color:#205478; border-collapse:separate;}
			.buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
			.buttonContent a{color:#FFFFFF; display:block; text-decoration:none!important; border:0!important;}
			.emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}
			.emailCalendarMonth{background-color:#205478; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}
			.emailCalendarDay{color:#205478; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}
			.imageContentText {margin-top: 10px;line-height:0;}
			.imageContentText a {line-height:0;}
			#invisibleIntroduction {display:none !important;} /* Removing the introduction text from the view */
			/*FRAMEWORK HACKS & OVERRIDES */
			span[class=ios-color-hack] a {color:#275100!important;text-decoration:none!important;} /* Remove all link colors in IOS (below are duplicates based on the color preference) */
			span[class=ios-color-hack2] a {color:#205478!important;text-decoration:none!important;}
			span[class=ios-color-hack3] a {color:#8B8B8B!important;text-decoration:none!important;}
			/* A nice and clean way to target phone numbers you want clickable and avoid a mobile phone from linking other numbers that look like, but are not phone numbers.  Use these two blocks of code to "unstyle" any numbers that may be linked.  The second block gives you a class to apply with a span tag to the numbers you would like linked and styled.
			Inspired by Campaign Monitor&apos;s article on using phone numbers in email: http://www.campaignmonitor.com/blog/post/3571/using-phone-numbers-in-html-email/.
			*/
			.a[href^="tel"], a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:none!important;cursor:default!important;}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:auto!important;cursor:default!important;}
			/* MOBILE STYLES */
			@media only screen and (max-width: 480px){
				/*////// CLIENT-SPECIFIC STYLES //////*/
				body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */
				/* FRAMEWORK STYLES */
				/*
				CSS selectors are written in attribute
				selector format to prevent Yahoo Mail
				from rendering media query styles on
				desktop.
				*/
				/*td[class="textContent"], td[class="flexibleContainerCell"] { width: 100%; padding-left: 10px !important; padding-right: 10px !important; }*/
				table[id="emailHeader"],
				table[id="emailBody"],
				table[id="emailFooter"],
				table[class="flexibleContainer"],
				td[class="flexibleContainerCell"] {width:100% !important;}
				td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {display: block;width: 100%;text-align: left;}
				/*
				The following style rule makes any
				image classed with "flexibleImage"
				fluid when the query activates.
				Make sure you add an inline max-width
				to those images to prevent them
				from blowing out.
				*/
				td[class="imageContent"] img {height:auto !important; width:100% !important; max-width:100% !important; }
				img[class="flexibleImage"]{height:auto !important; width:100% !important;max-width:100% !important;}
				img[class="flexibleImageSmall"]{height:auto !important; width:auto !important;}
				/*
				Create top space for every second element in a block
				*/
				table[class="flexibleContainerBoxNext"]{padding-top: 10px !important;}
				/*
				Make buttons in the email span the
				full width of their container, allowing
				for left- or right-handed ease of use.
				*/
				table[class="emailButton"]{width:100% !important;}
				td[class="buttonContent"]{padding:0 !important;}
				td[class="buttonContent"] a{padding:15px !important;}
			}
			/*  CONDITIONS FOR ANDROID DEVICES ONLY
			*   http://developer.android.com/guide/webapps/targeting.html
			*   http://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/ ;
			=====================================================*/
			@media only screen and (-webkit-device-pixel-ratio:.75){
				/* Put CSS for low density (ldpi) Android layouts in here */
			}
			@media only screen and (-webkit-device-pixel-ratio:1){
				/* Put CSS for medium density (mdpi) Android layouts in here */
			}
			@media only screen and (-webkit-device-pixel-ratio:1.5){
				/* Put CSS for high density (hdpi) Android layouts in here */
			}
			/* end Android targeting */
			/* CONDITIONS FOR IOS DEVICES ONLY
			=====================================================*/
			@media only screen and (min-device-width : 320px) and (max-device-width:568px) {
			}
			/* end IOS targeting */
		</style>
		<!--
			Outlook Conditional CSS
			These two style blocks target Outlook 2007 & 2010 specifically, forcing
			columns into a single vertical stack as on mobile clients. This is
			primarily done to avoid the "page break bug" and is optional.
			More information here:
			http://templates.mailchimp.com/development/css/outlook-conditional-css
		-->
		<!--[if mso 12]>
			<style type="text/css">
				.flexibleContainer{display:block !important; width:100% !important;}
			</style>
		<![endif]-->
		<!--[if mso 14]>
			<style type="text/css">
				.flexibleContainer{display:block !important; width:100% !important;}
			</style>
		<![endif]-->
	</head>
	<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

		<!-- CENTER THE EMAIL // -->
		<!--
		1.  The center tag should normally put all the
			content in the middle of the email page.
			I added "table-layout: fixed;" style to force
			yahoomail which by default put the content left.
		2.  For hotmail and yahoomail, the contents of
			the email starts from this center, so we try to
			apply necessary styling e.g. background-color.
		-->
		<center style="background-color:#E1E1E1;">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
				<tr>
					<td align="center" valign="top" id="bodyCell">

						<!-- EMAIL HEADER // -->
						<!--
							The table "emailBody" is the email&apos;s container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

							<!-- HEADER ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<!--
																		The "invisibleIntroduction" is the text used for short preview
																		of the email before the user opens it (50 characters max). Sometimes,
																		you do not want to show this message depending on your design but this
																		text is highly recommended.
																		You do not have to worry if it is hidden, the next <td> will automatically
																		center and apply to the width 100% and also shrink to 50% if the first <td>
																		is visible.
																	-->
																	<td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																						A new Dealer has been added in Market Force! Click to print it out!
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td align="right" valign="middle" class="flexibleContainerBox">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					<!-- CONTENT // -->
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																						If you can&apos;t see this message, <a href="#" target="_blank" style="text-decoration:none;border-bottom:1px solid #828282;color:#828282;"><span style="color:#828282;">view&nbsp;it&nbsp;in&nbsp;your&nbsp;browser</span></a>.
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // END -->

						</table>
						<!-- // END -->

						<!-- EMAIL BODY // -->
						<!--
							The table "emailBody" is the email&apos;s container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">

							<!-- MODULE ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<!--
										The centering table keeps the content
										tables centered in the emailBody table,
										in case its width is set to 100%.
									-->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF; background-image: linear-gradient(50deg, #5B5B5B 50%, #000000 50%);" >
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<!--
													The flexible container has a set width
													that gets overridden by the media query.
													Most content tables within can then be
													given 100% widths.
												-->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<!--
															The content table is the first element
																that&apos;s entirely separate from the structural
																framework of the email.
															-->
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top" class="textContent">
                                    <img src="http://marketforceapp.com/marketforce/img/Market%20Force%20Logo%202.png" style="width:80%;"><br>
																		<!--<h1 style="color:#FFFFFF;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:35px;font-weight:normal;margin-bottom:5px;text-align:center;">Introduction header</h1>-->
																		<h2 style="text-align:center;font-weight:normal;font-family:Helvetica,Arial,sans-serif;font-size:23px;margin-bottom:10px;color:#E64C3D;line-height:135%;">Collaboration & Integration</h2>
																		<div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#FFFFFF;line-height:135%;">Market Force strives to create custom innovative collaboration software that allows businesses to thrive!</div>
																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


						<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					<h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Dear Mr. Burton,</h3>
																					<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;"><strong style="color:red;">' . $user . '</strong> has added a <strong>New Dealer</strong>, <strong style="color:red;">' . $bizname . '</strong>, into the system. For your convenience, we have emailed you the entire packet so you may view or print it. Click the button below to view or print the packet!</div>
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr style="padding-top:0;">
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td style="padding-top:0;" align="center" valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table border="0" cellpadding="0" cellspacing="0" width="50%" class="emailButton" style="background-color: #000000;">
																<tr>
																	<td align="center" valign="middle" class="buttonContent" style="padding-top:15px;padding-bottom:15px;padding-right:15px;padding-left:15px;">
																		<a style="color:#FFFFFF;text-decoration:none;font-family:Helvetica,Arial,sans-serif;font-size:20px;line-height:135%;" href="http://marketforceapp.com/marketforce/forms/new-dealer-form/print-packet.php?id=' . $idr['ID'] . '" target="_blank">Print Dealer Packet</a>
																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


							


							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					<!--<h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Dear valued customer,</h3>-->
																					<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">Thank you,<br><strong>Market Force Team</div>
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


							<!-- MODULE ROW // -->
							<tr style="background-image: linear-gradient(50deg, #5B5B5B 50%, #000000 50%);">
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td valign="top" class="imageContent" style="height:75px;">
																		<!--<img src="http://www.charlesmudy.com/respmail/respmail-full.jpg" width="500" class="flexibleImage" style="max-width:500px;width:100%;display:block;" alt="Text" title="Text" />-->
																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


							<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">
							<!-- FOOTER ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td valign="top" bgcolor="#E1E1E1">

																		<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																			<div>Copyright &#169; 2017 <a href="http://www.burtonsolution.tech" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">Market Force</span></a>. All&nbsp;rights&nbsp;reserved.</div>
																			<!--<div>If you do not want to recieve emails from us, you can <a href="#" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">unsubscribe</span></a>.</div>-->
																		</div>

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>

						</table>
						<!-- // END -->

					</td>
				</tr>
			</table>
		</center>
	</body>
</html>';

$mail->send();

//Add Support Ticket to add to Market Force Map
$mail->ClearAddresses();
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
$mail->addAddress('help@burtonsolution.on.spiceworks.com');
$mail->Subject = 'New Dealer!';
$mail->Body = 'Please Add the following dealer to the map:<br>
							<br>
							' . $bizname . '<br>
							' . $address . '<br>
							' . $city . ', ' . $state . ' ' . $zip . '<br>
							Phone#: ' . $phone . '<br>
							Fax#: ' . $fax . '<br>
							Email: ' . $email;

//$mail->send();//Turn back on to send the support ticket emails

//echo $mail->Body . '<br>';

//Setup Google Geolocation in database
echo '<script>';

$q = "SELECT * FROM `dealers` WHERE `lat` = '' LIMIT 1";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  
echo 'var address = "' . $r['address'] . ',' . $r['city'] . ',' . $r['state'] . ',' . $r['zip'] . '";
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
    var jd = JSON.parse(xmlhttp.responseText);
if(jd.status === "ZERO_RESULTS"){
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp12=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp12=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp12.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
    }
  }
  xmlhttp12.open("GET","http://' . $_SERVER['HTTP_HOST'] . '/marketforce/map/cron.php?error=true&name=' . $r['name'] . '&id=' . $r['ID'] . '&address=' . $r['address'] . '&city=' . $r['city'] . '&state=' . $r['state'] . '&zip=' . $r['zip'] . '",true);
  xmlhttp12.send();
    }
    var lat = jd.results[0].geometry.location.lat;
    var lng = jd.results[0].geometry.location.lng;
    
    var id = "' . $r['ID'] . '";
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp2=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp2.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("main").innerHTML += xmlhttp2.responseText + "<br>";
      window.location.reload();
    }
  }
  xmlhttp2.open("GET","http://' . $_SERVER['HTTP_HOST'] . '/marketforce/map/insertlatlng.php?id="+id+"&lat="+lat+"&lng="+lng,true);
  xmlhttp2.send();
    }
  }
  xmlhttp.open("GET","https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg",true);
  xmlhttp.send();
  ';
  
}
echo '</script>';


//Display Response
echo 'Thank You!';
echo '<br><br>';

//echo $i;
?>