<?php
include 'connection.php';

//Load Variables
$did = $_GET['did'];
$doc = $_GET['doc'];
$rs = $_GET['rs'];
$ds = $_GET['ds'];
$dname = $_GET['dname'];
$ddl = $_GET['ddl'];
$ddob = $_GET['ddob'];
$dstate = $_GET['dstate'];
$ls = $_GET['ls'];
$lname = $_GET['lname'];
$ldl = $_GET['ldl'];
$ldob = $_GET['ldob'];
$lstate = $_GET['lstate'];
$type = $_GET['type'];


//Check Query Type
if($type === 'dsign'){
$isq = "UPDATE `signatures` SET `dsign` = '" . $ds . "',`dname` = '" . $dname . "',`ddl#` = '" . $ddl . "',`ddob` = '" . $ddob . "',`dstate` = '" . $dstate . "',`document` = '" . $doc . "', `ddate` = CURRENT_DATE, `dtime` = CURRENT_TIME WHERE `dealer_id` = '" . $did . "'";
$isg = mysqli_query($conn, $isq) or die($conn->error);
  echo 'Dealer Signature has been recorded!';
}

if($type === 'rsign'){
$isq = "UPDATE `signatures` SET `rsign` = '" . $rs . "', `document` = '" . $doc . "', `rdate` = CURRENT_DATE, `rtime` = CURRENT_TIME WHERE `dealer_id` = '" . $did . "'";
$isg = mysqli_query($conn, $isq) or die($conn->error);
  echo 'OSR Signature has been recorded!';
}

if($type === 'lsign'){
  //load variables
  $llname = mysqli_real_escape_string($conn, $_GET['llname']);
  $llbizname = mysqli_real_escape_string($conn, $_GET['llbizname']);
  $lladdress = mysqli_real_escape_string($conn, $_GET['lladdress']);
  $llcity = mysqli_real_escape_string($conn, $_GET['llcity']);
  $llstate = mysqli_real_escape_string($conn, $_GET['llstate']);
  $llzip = mysqli_real_escape_string($conn, $_GET['llzip']);
  $llphone = mysqli_real_escape_string($conn, $_GET['llphone']);
  $llfax = mysqli_real_escape_string($conn, $_GET['llfax']);
  $llemail = mysqli_real_escape_string($conn, $_GET['llemail']);
  //IP Info & Email...
  $ipAddress = mysqli_real_escape_string($conn, $_GET['ip']);
  $vemail = mysqli_real_escape_string($conn, $_GET['vemail']);
  
  
$isq = "UPDATE `signatures` 
        SET `lsign` = '" . $ls . "',
        `lname` = '" . $lname . "', 
        `ldl#` = '" . $ldl . "',
        `ldob` = '" . $ldob . "', 
        `lstate` = '" . $lstate . "', 
        `document` = '" . $doc . "', 
        `ldate` = CURRENT_DATE, 
        `ltime` = NOW() + 1,
        `ip_address` = '" . $ipAddress . "',
        `visitor_email` = '" . $vemail . "' 
        WHERE 
        `dealer_id` = '" . $did . "'";
$isg = mysqli_query($conn, $isq) or die($conn->error);
  
$infoq = "UPDATE `new_dealer_form` 
          SET 
          `llname` = '" . $llname . "', 
          `llbizname` = '" . $llbizname . "', 
          `lladdress` = '" . $lladdress . "',
          `llcity` = '" . $llcity . "',
          `llstate` = '" . $llstate . "',
          `llzip` = '" . $llzip. "',
          `llphone` = '" . $llphone . "',
          `llfax` = '" . $llfax . "',
          `llemail` = '" . $llemail . "'
          WHERE `ID` = '" . $did . "'";
$infoq = mysqli_query($conn, $infoq) or die($conn->error);
  echo 'Landlord Signature and information has been recorded!';
}


?>