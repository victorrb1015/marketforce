<?php
include '../../../php/connection.php';

$id = $_POST['id'];
//echo $_FILES["fileToUpload"]["name"];//This is the name of the file being uploaded
//echo $_FILES["fileToUpload"]["tmp_name"];//This is the location and temporary name of the file when form submitted.
$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/../doc-files/contract_forms/";
$target_file2 = $target_dir . basename($_FILES["fileToUpload"]["name"]);

$uploadOk = 1;
$imageFileType = pathinfo($target_file2,PATHINFO_EXTENSION);
$target_file = $target_dir . $id . "_contractForm." . $imageFileType;
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
      //  echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
/*if (file_exists($target_file)) {
    echo "<br>ERROR!- file already exists.";
    $uploadOk = 0;
}*/
// Check file size (turned off for now)
/*if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "<br>Sorry, your file is too large.";
    $uploadOk = 0;
}*/
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "JPG") {
    echo "<br>ERROR!- only JPG, JPEG, PNG & GIF files are allowed.";
    //echo "<br><script>alert('" . $imageFileType . "');</script>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "<br>ERROR!- your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      
      //INSERT FILE PATH INTO DATABASE
      $file_name_1 = 'http://docs.marketforceapp.com/contract_forms/' . $id . "_contractForm." . $imageFileType;
      $fuq = "UPDATE `new_dealer_form` SET `contract_form` = '" . $file_name_1 . "' WHERE `ID` = '" . $id . "'";
      mysqli_query($conn, $fuq) or die($conn->error);
      
        echo "<br>The file '". basename( $_FILES["fileToUpload"]["name"]). "' has been uploaded.";
        echo "<script>
                setInterval(function(){
                window.location = 'http://marketforceapp.com/marketforce/print-forms.php?ider=" . $id . "';
                },1500);
              </script>";
    }
}

?>
