function disp_rel(){
	var tcg = document.querySelector('input[name="tcg"]:checked').value;
  
	var g = document.querySelector("input[name='g']:checked").value;
	
	var cua = document.querySelector("input[name='cua']:checked").value;
  
	var cur = document.querySelector("input[name='cur']:checked").value;
  
	var rvp = document.querySelector("input[name='rvp']:checked").value;
  
	var cst = document.querySelector("input[name='cst']:checked").value;
  
	var csp = document.querySelector("input[name='csp']:checked").value;
  
	var vb = document.querySelector("input[name='vb']:checked").value;
  
	var u = document.querySelector("input[name='u']:checked").value;
  
	var au = document.querySelector("input[name='au']:checked").value;
  
	var us = document.querySelector("input[name='us']:checked").value;
  
	var vu = document.querySelector("input[name='vu']:checked").value;
  
  var notes = document.getElementById('notes').value;
  var id = document.getElementById('doid').value;
  var user = document.getElementById('douser').value;
	var mbd = document.getElementById('ndmoveby').value;
 
	if(mbd === ''){
		alert('Please enter the MOVE BY date for this relocation!');
		return;
	}
	
	//New Dealer Info
	var ndn = document.getElementById('ndn').value;
	if(ndn === ''){
		document.getElementById('ndn_star').innerHTML = '*';
		alert('Please enter the Dealer Name for the new location!');
		return;
	}
	var ndaddress = document.getElementById('ndaddress').value;
	if(ndaddress === ''){
		document.getElementById('ndaddress_star').innerHTML = '*';
		alert('Please enter the address of the new location!');
		return;
	}
	var ndcity = document.getElementById('ndcity').value;
	if(ndcity === ''){
		document.getElementById('ndcity_star').innerHTML = '*';
		alert('Please enter the city of the new location!');
		return;
	}
	var ndstate = document.getElementById('ndstate').value;
	if(ndstate === ''){
		document.getElementById('ndstate_star').innerHTML = '*';
		alert('Please enter the state of the new location');
		return;
	}
	var ndzip = document.getElementById('ndzip').value;
	if(ndzip === ''){
		document.getElementById('ndzip_star').innerHTML = '*';
		alert('Please enter the zip code of the new location!');
		return;
	}

  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			alert(this.responseText);
      //window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/forms/new-dealer-form/php/save-relocation-form.php"+
               "?id="+id+
               "&user="+user+
               "&tcg="+tcg+
							 "&ndn="+ndn+
							 "&nda="+ndaddress+
							 "&ndc="+ndcity+
							 "&nds="+ndstate+
							 "&ndz="+ndzip+
               "&g="+g+
               "&cua="+cua+
               "&cur="+cur+
               "&rvp="+rvp+
               "&cst="+cst+
               "&csp="+csp+
               "&vb="+vb+
               "&u="+u+
               "&au="+au+
               "&us="+us+
               "&vu="+vu+
               "&notes="+notes+
							 "&mbd="+mbd,true);
  xmlhttp.send();
}



function sub_form(){
	var idx = document.getElementById('doid').value;//Dealer ID for link
	var uid = document.getElementById('douser').value;//Rep ID for email
	
	var mbd = document.getElementById('ndmoveby').value;
 
	if(mbd === ''){
		alert('Please enter the MOVE BY date for this relocation!');
		return;
	}
	
	if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			alert(this.responseText);
      //window.location.reload();
      disp_rel();
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/forms/new-dealer-form/php/email-additional-forms.php?"+
							 				"form=relocation&id="+idx+"&rep="+uid,true);
	xmlhttp.send();
}

