/*====================================================
  This script is to allow the toggling of form fields
  when needed
====================================================*/
$(document).ready(function(){
$('#sameAddress').click(function(){
  
  //Current Variables
  var currentAddress = document.getElementById('address');
  var currentCity = document.getElementById('city');
  var currentState = document.getElementById('state');
  var currentZip = document.getElementById('zip');
  
  //New Variables
  var newAddress = document.getElementById('maddress');
  var newCity = document.getElementById('mcity');
  var newState = document.getElementById('mstate');
  var newZip = document.getElementById('mzip');
  
  var boxx = document.getElementById('sameAddress');
  if(boxx.checked){
    newAddress.value = currentAddress.value;
    newCity.value = currentCity.value;
    newState.value = currentState.value;
    newZip.value = currentZip.value;
  }else{
    newAddress.value = '';
    newCity.value = '';
    newState.value = '';
    newZip.value = '';
 }
  
})
});