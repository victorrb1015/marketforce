<?php
include_once('php/connection.php');

//Load Variables
$id13 = $_GET['id'];


//Info Query
$q13 = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $id13 . "'";
$g13 = mysqli_query($conn, $q13) or die($conn->error);
$r13 = mysqli_fetch_array($g13);
$dealer_state = $r13['state'];
$region = '';
//Michigan Region...
if($dealer_state == 'Michigan'){
  $region = 'Michigan';
}
//Indiana Region...
if($dealer_state == 'Indiana' || $dealer_state == 'Illinois' || $dealer_state == 'Ohio'){
  $region = 'Indiana';
}
//Indiana Region...
if($dealer_state == 'Oklahoma' || $dealer_state == 'Missouri' || $dealer_state == 'Arkansas' || $dealer_state == 'Kentucky' || $dealer_state == 'Texas' || $dealer_state == 'Kansas'){
  $region = 'Oklahoma';
}

//Check Display Orders...
$cdoq2 = "SELECT * FROM `display_orders` WHERE `inactive` != 'Yes' AND `ID` = '" . $id13 . "'";
$cdog2 = mysqli_query($conn, $cdoq2) or die($conn->error);
if(mysqli_num_rows($cdog2) > 0){
  $doir2 = mysqli_fetch_array($cdog2);
  //Get Defaults...
  $defq2 = "SELECT * FROM `display_form_defaults` WHERE `inactive` != 'Yes' AND `region` = '" . $region . "'";
  $defg2 = mysqli_query($conn, $defq2) or die($conn->error);
  $defr2 = mysqli_fetch_array($defg2);
}else{
  $doiq2 = "SELECT * FROM `display_form_defaults` WHERE `inactive` != 'Yes' AND `region` = '" . $region . "'";
  $doig2 = mysqli_query($conn, $doiq2) or die($conn->error);
  $doir2 = mysqli_fetch_array($doig2);
}

//Order query
/*$doiq2 = "SELECT * FROM `display_orders` WHERE `ID` = '" . $id13 . "'";
$doig2 = mysqli_query($conn, $doiq2) or die($conn->error);
$doir2 = mysqli_fetch_array($doig2);*/

echo '<html>
  <head>
    <title>Display Order Form</title>
    <style>
      html{
        -webkit-print-color-adjust:exact;
      }
      .main{
        width: 90%;
        margin: auto;
      }
      .header{
        //text-align: center;
      }
      .header img{
        pading-right: 10%;
        width: 30%;
      }
      .header-info{
        margin-left: 35%;
      }
      .header .info{
        position: absolute;
        right: 6%;
        top: 2%;
      }
      h1,h2{
        text-align: center;
      }
      span{
        color: red;
        text-decoration: underline;
        font-size: 20px;
      }
      label{
        font-weight: bold;
      }
      .info{
        font-size: 15px;
      }
      td{
        padding-bottom: 15px;
        font-size: 25px;
      }
      p{
        font-size: 20px;
      }
      table{
        width: 100%;
      }
      input[type=checkbox] {
        transform: scale(2);
      }
    </style>
    <script src="js/display-order-handler.js"></script>
  </head>
  <body>
    <div class="main">
      <div class="header">
        <!--<img src="images/all-steel-red.jpg" />-->
        <img src="' . $_SESSION['org_logo_url'] . '" style="width=100%;">';
switch($_SESSION['org_id']){
    case '329698':
        echo '<img class="header-info" src="images/integrity_dir.png" alt="direction"/>';
        break;
    default:
        echo '<img class="header-info" src="images/header-info.jpeg" alt="direction" />';
}
echo '</div>
      <input type="hidden" id="doid" value="' . $id3 . '" />
      <input type="hidden" id="douser" value="' . $_SESSION['user_id'] . '" />
      <h2>Dealer Display Order Form</h2>
      <p>
      <strong>Dealer Name:</strong> ' . $r13['bizname'] . ' &nbsp; <strong>OSR:</strong> ' . $r13['user'] . '
      </p>
      <p>
      <strong>Street Address:</strong> ' . $r13['address'] . '
      </p>
      <p>
      <strong>City:</strong> ' . $r13['city'] . ' &nbsp; <strong>State:</strong> ' . $r13['state'] . ' &nbsp; <strong>Zip:</strong> ' . $r13['zip'] . '
      </p>
      <br>
      <p>
      <table>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="tcg"'; if($doir2['tcg'] == 'true'){echo 'checked';} echo '/> &nbsp;&nbsp; 1. Two Car Garage: 
      </td>
      <td style="width: 75%;">';
  if($doir2['tcg_v1'] != $defr2['tcg_v1']){
    echo '<mark>' . $doir2['tcg_v1'] . '</mark>';
  }else{
    echo $doir2['tcg_v1'];
  }
      echo 'x';
  if($doir2['tcg_v2'] != $defr2['tcg_v2']){
    echo '<mark>' . $doir2['tcg_v2'] . '</mark>';
  }else{
    echo $doir2['tcg_v2'];
  }
      echo 'x';
  if($doir2['tcg_v3'] != $defr2['tcg_v3']){
    echo '<mark>' . $doir2['tcg_v3'] . '</mark>';
  }else{
    echo $doir2['tcg_v3'];
  }
      echo ' A-Frame / ';
  if($doir2['tcg_v4'] != $defr2['tcg_v4']){
    echo '<mark>' . $doir2['tcg_v4'] . '</mark>';
  }else{
    echo $doir2['tcg_v4'];
  }
      echo ' - 9x7 Doors / 
      Two-Tone VERT ROOF / ';
  if($doir2['tcg_v5'] != $defr2['tcg_v5']){
    echo '<mark>' . $doir2['tcg_v5'] . '</mark>';
  }else{
    echo $doir2['tcg_v5'];
  }
      echo ' - 36x80 Walk-In Door / ';
  if($doir2['tcg_v6'] != $defr2['tcg_v6']){
    echo '<mark>' . $doir2['tcg_v6'] . '</mark>';
  }else{
    echo $doir2['tcg_v6'];
  }
      echo ' Window / 
      $' . $doir2['tcg_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="ip"'; if($doir2['ip'] == 'true'){echo 'checked';} echo ' /> &nbsp;&nbsp; Insulation Pkg: 
      </td>
      <td style="width: 75%;">
      Black Foam Left Corner /  
      Condenstop Left Roof /  
      Insulation Right Roof / 
      $' . $doir['ip_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="g"'; if($doir2['g'] == 'true'){echo 'checked';} echo ' /> &nbsp;&nbsp; 2. Garage: 
      </td>
      <td style="width: 75%;">';
  if($doir2['g_v1'] != $defr2['g_v1']){
    echo '<mark>' . $doir2['g_v1'] . '</mark>';
  }else{
    echo $doir2['g_v1'];
  }
      
   echo 'x';
  if($doir2['g_v2'] != $defr2['g_v2']){
    echo '<mark>' . $doir2['g_v2'] . '</mark>';
  }else{
    echo $doir2['g_v2'];
  }
      echo 'x';
  if($doir2['g_v3'] != $defr2['g_v3']){
    echo '<mark>' . $doir2['g_v3'] . '</mark>';
  }else{
    echo $doir2['g_v3'];
  }
       echo 'A-Frame /  
      Vertical Roof / ';
  if($doir2['g_v4'] != $defr2['g_v4']){
    echo '<mark>' . $doir2['g_v4'] . '</mark>';
  }else{
    echo $doir2['g_v4'];
  }
      echo ' - 9x7 Door / ';
  if($doir2['g_v5'] != $defr2['g_v5']){
    echo '<mark>' . $doir2['g_v5'] . '</mark>';
  }else{
    echo $doir2['g_v5'];
  }
      echo ' - 32x72 Walk-In Door /  
      $' . $doir2['g_price'] . '
      </td>
      </tr>
      </table>
      ***********************************************************************************************************************
      <table>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="cua"'; if($doir2['cua'] == 'true'){echo 'checked';} echo ' /> &nbsp;&nbsp; 3. Combo Unit A-Frame:  
      </td>
      <td style="width: 75%;">';
  if($doir2['cua_v1'] != $defr2['cua_v1']){
    echo '<mark>' . $doir2['cua_v1'] . '</mark>';
  }else{
    echo $doir2['cua_v1'];
  }
      echo 'x';
  if($doir2['cua_v2'] != $defr2['cua_v2']){
    echo '<mark>' . $doir2['cua_v2'] . '</mark>';
  }else{
    echo $doir2['cua_v2'];
  }
      echo 'x';
  if($doir2['cua_v3'] != $defr2['cua_v3']){
    echo '<mark>' . $doir2['cua_v3'] . '</mark>';
  }else{
    echo $doir2['cua_v3'];
  }
      echo '/ 
      10ft Enclosed /  
      A-Frame / ';
  if($doir2['cua_v4'] != $defr2['cua_v4']){
    echo '<mark>' . $doir2['cua_v4'] . '</mark>';
  }else{
    echo $doir2['cua_v4'];
  }
      echo ' - 8x7 Door / 
      $' . $doir2['cua_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="cur"'; if($doir2['cur'] == 'true'){echo 'checked';} echo ' /> &nbsp;&nbsp; 4. RV Port:  
      </td>
      <td style="width: 75%;">';
  if($doir2['cur_v1'] != $defr2['cur_v1']){
    echo '<mark>' . $doir2['cur_v1'] . '</mark>';
  }else{
    echo $doir2['cur_v1'];
  }
      echo 'x';
  if($doir2['cur_v2'] != $defr2['cur_v2']){
    echo '<mark>' . $doir2['cur_v2'] . '</mark>';
  }else{
    echo $doir2['cur_v2'];
  }
      echo 'x';
  if($doir2['cur_v3'] != $defr2['cur_v3']){
    echo '<mark>' . $doir2['cur_v3'] . '</mark>';
  }else{
    echo $doir2['cur_v3'];
  }
      echo ' / 
      Regular Roof / ';
  if($doir2['cur_v4'] != $defr2['cur_v4']){
    echo '<mark>' . $doir2['cur_v4'] . '</mark>';
  }else{
    echo $doir2['cur_v4'];
  }
  echo ' Extra Sheets / ';
  if($doir2['cur_v5'] != $defr2['cur_v5']){
    echo '<mark>' . $doir2['cur_v5'] . '</mark>';
  }else{
    echo $doir2['cur_v5'];
  }
      echo ' Supports / 
      $' . $doir2['cur_price'] . '
      </td>
      </tr>
      </table>
      <!--***********************************************************************************************************************-->
      <table>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="rvp"'; if($doir2['rvp'] == 'true'){echo 'checked';} echo ' /> &nbsp;&nbsp; 5. RV Port:  
      </td>
      <td style="width: 75%;">';
  if($doir2['rvp_v1'] != $defr2['rvp_v1']){
    echo '<mark>' . $doir2['rvp_v1'] . '</mark>';
  }else{
    echo $doir2['rvp_v1'];
  }
      echo 'x';
  if($doir2['rvp_v2'] != $defr2['rvp_v2']){
    echo '<mark>' . $doir2['rvp_v2'] . '</mark>';
  }else{
    echo $doir2['rvp_v2'];
  }
      echo 'x';
  if($doir2['rvp_v3'] != $defr2['rvp_v3']){
    echo '<mark>' . $doir2['rvp_v3'] . '</mark>';
  }else{
    echo $doir2['rvp_v3'];
  }
      echo '/ 
      Regular Roof / ';
  if($doir2['rvp_v4'] != $defr2['rvp_v4']){
    echo '<mark>' . $doir2['rvp_v4'] . '</mark>';
  }else{
    echo $doir2['rvp_v4'];
  }
      echo ' Extra Sheets / ';
  if($doir2['rvp_v5'] != $defr2['rvp_v5']){
    echo '<mark>' . $doir2['rvp_v5'] . '</mark>';
  }else{
    echo $doir2['rvp_v5'];
  }
      echo ' Supports / 
      $' . $doir2['rvp_price'] . '
      </td>
      </tr>
      </table>
      ***********************************************************************************************************************
      <table>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="cst"'; if($doir2['cst'] == 'true'){echo 'checked';} echo ' /> &nbsp;&nbsp; 6. Carport Standard:  
      </td>
      <td style="width: 75%;">';
  if($doir2['cst_v1'] != $defr2['cst_v1']){
    echo '<mark>' . $doir2['cst_v1'] . '</mark>';
  }else{
    echo $doir2['cst_v1'];
  }
      echo 'x';
  if($doir2['cst_v2'] != $defr2['cst_v2']){
    echo '<mark>' . $doir2['cst_v2'] . '</mark>';
  }else{
    echo $doir2['cst_v2'];
  }
      echo 'x';
  if($doir2['cst_v3'] != $defr2['cst_v3']){
    echo '<mark>' . $doir2['cst_v3'] . '</mark>';
  }else{
    echo $doir2['cst_v3'];
  }
      echo '/ 
      Regular Roof / 
      $' . $doir2['cst_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="csp"'; if($doir2['csp'] == 'true'){echo 'checked';} echo ' /> &nbsp;&nbsp; 7. Carport Special:  
      </td>
      <td style="width: 75%;">';
  if($doir2['csp_v1'] != $defr2['csp_v1']){
    echo '<mark>' . $doir2['csp_v1'] . '</mark>';
  }else{
    echo $doir2['csp_v1'];
  }
      echo 'x';
  if($doir2['csp_v2'] != $defr2['csp_v2']){
    echo '<mark>' . $doir2['csp_v2'] . '</mark>';
  }else{
    echo $doir2['csp_v2'];
  }
      echo 'x';
  if($doir2['csp_v3'] != $defr2['csp_v3']){
    echo '<mark>' . $doir2['csp_v3'] . '</mark>';
  }else{
    echo $doir2['csp_v3'];
  }
      echo '/ 
      A-Frame / ';
  if($doir2['csp_v4'] != $defr2['csp_v4']){
    echo '<mark>' . $doir2['csp_v4'] . '</mark>';
  }else{
    echo $doir2['csp_v4'];
  }
      echo ' Gable End / ';
  if($doir2['csp_v5'] != $defr2['csp_v5']){
    echo '<mark>' . $doir2['csp_v5'] . '</mark>';
  }else{
    echo $doir2['csp_v5'];
  }
      echo ' Sheet On Each Side / 
      $' . $doir2['csp_price'] . '
      </td>
      </tr>
      </table>
      ***********************************************************************************************************************
      <table>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="vb"'; if($doir2['vb'] == 'true'){echo 'checked';} echo ' /> &nbsp;&nbsp; 8. Vertical Building:  
      </td>
      <td style="width: 75%;">';
  if($doir2['vb_v1'] != $defr2['vb_v1']){
    echo '<mark>' . $doir2['vb_v1'] . '</mark>';
  }else{
    echo $doir2['vb_v1'];
  }
      echo 'x';
  if($doir2['vb_v2'] != $defr2['vb_v2']){
    echo '<mark>' . $doir2['vb_v2'] . '</mark>';
  }else{
    echo $doir2['vb_v2'];
  }
      echo 'x';
  if($doir2['vb_v3'] != $defr2['vb_v3']){
    echo '<mark>' . $doir2['vb_v3'] . '</mark>';
  }else{
    echo $doir2['vb_v3'];
  }
      echo '/ 
      ALL Vertical / Fully Enclosed / ';
  if($doir2['vb_v4'] != $defr2['vb_v4']){
    echo '<mark>' . $doir2['vb_v4'] . '</mark>';
  }else{
    echo $doir2['vb_v4'];
  }
      echo ' - 10x8 Roll Up Door / ';
  if($doir2['vb_v5'] != $defr2['vb_v5']){
    echo '<mark>' . $doir2['vb_v5'] . '</mark>';
  }else{
    echo $doir2['vb_v5'];
  }
      echo ' - 34x76 Walk-In Door / 
      $' . $doir2['vb_price'] . '
      </td>
      </tr>
      </table>
      ***********************************************************************************************************************
      <table>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="u"'; if($doir2['u'] == 'true'){echo 'checked';} echo ' /> &nbsp;&nbsp; 9. Utility - Front Railing:  
      </td>
      <td style="width: 75%;">';
  if($doir2['u_v1'] != $defr2['u_v1']){
    echo '<mark>' . $doir2['u_v1'] . '</mark>';
  }else{
    echo $doir2['u_v1'];
  }
      echo 'x';
  if($doir2['u_v2'] != $defr2['u_v2']){
    echo '<mark>' . $doir2['u_v2'] . '</mark>';
  }else{
    echo $doir2['u_v2'];
  }
      echo 'x';
  if($doir2['u_v3'] != $defr2['u_v3']){
    echo '<mark>' . $doir2['u_v3'] . '</mark>';
  }else{
    echo $doir2['u_v3'];
  }
      echo ' With FLOOR 
      / 
      A-Frame / No Vertical Roof / With RAILING On Front / ';
  if($doir2['u_v4'] != $defr2['u_v4']){
    echo '<mark>' . $doir2['u_v4'] . '</mark>';
  }else{
    echo $doir2['u_v4'];
  }
      echo ' - 32x71 Walk-In Door / ';
  if($doir2['u_v5'] != $defr2['u_v5']){
    echo '<mark>' . $doir2['u_v5'] . '</mark>';
  }else{
    echo $doir2['u_v5'];
  }
      echo ' - 6x6 Door / 
      $' . $doir2['u_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="au"'; if($doir2['au'] == 'true'){echo 'checked';} echo ' /> &nbsp;&nbsp; 10. A-Frame Utility:  
      </td>
      <td style="width: 75%;">';
  if($doir2['au_v1'] != $defr2['au_v1']){
    echo '<mark>' . $doir2['au_v1'] . '</mark>';
  }else{
    echo $doir2['au_v1'];
  }
      echo 'x';
  if($doir2['au_v2'] != $defr2['au_v2']){
    echo '<mark>' . $doir2['au_v2'] . '</mark>';
  }else{
    echo $doir2['au_v2'];
  }
      echo 'x';
  if($doir2['au_v3'] != $defr2['au_v3']){
    echo '<mark>' . $doir2['au_v3'] . '</mark>';
  }else{
    echo $doir2['au_v3'];
  }
      echo ' With FLOOR 
      / 
      A-Frame / ';
  if($doir2['au_v4'] != $defr2['au_v4']){
    echo '<mark>' . $doir2['au_v4'] . '</mark>';
  }else{
    echo $doir2['au_v4'];
  }
      echo ' - 6x6 Door / 
      $' . $doir2['au_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="us"'; if($doir2['us'] == 'true'){echo 'checked';} echo ' /> &nbsp;&nbsp; 11. Utility Standard:  
      </td>
      <td style="width: 75%;">';
  if($doir2['us_v1'] != $defr2['us_v1']){
    echo '<mark>' . $doir2['us_v1'] . '</mark>';
  }else{
    echo $doir2['us_v1'];
  }
      echo 'x';
  if($doir2['us_v2'] != $defr2['us_v2']){
    echo '<mark>' . $doir2['us_v2'] . '</mark>';
  }else{
    echo $doir2['us_v2'];
  }
      echo 'x';
  if($doir2['us_v3'] != $defr2['us_v3']){
    echo '<mark>' . $doir2['us_v3'] . '</mark>';
  }else{
    echo $doir2['us_v3'];
  }
      echo ' With FLOOR / ';
  if($doir2['us_v4'] != $defr2['us_v4']){
    echo '<mark>' . $doir2['us_v4'] . '</mark>';
  }else{
    echo $doir2['us_v4'];
  }
      echo ' - 6x6 Door / 
      $' . $doir2['us_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="vu"'; if($doir2['vu'] == 'true'){echo 'checked';} echo ' /> &nbsp;&nbsp; 12. Vertical Utility:  
      </td>
      <td style="width: 75%;">';
  if($doir2['vu_v1'] != $defr2['vu_v1']){
    echo '<mark>' . $doir2['vu_v1'] . '</mark>';
  }else{
    echo $doir2['vu_v1'];
  }
      echo 'x';
  if($doir2['vu_v2'] != $defr2['vu_v2']){
    echo '<mark>' . $doir2['vu_v2'] . '</mark>';
  }else{
    echo $doir2['vu_v2'];
  }
      echo 'x';
  if($doir2['vu_v3'] != $defr2['vu_v3']){
    echo '<mark>' . $doir2['vu_v3'] . '</mark>';
  }else{
    echo $doir2['vu_v3'];
  }
      echo '/ 
      ALL VERTICAL / ';
  if($doir2['vu_v4'] != $defr2['vu_v4']){
    echo '<mark>' . $doir2['vu_v4'] . '</mark>';
  }else{
    echo $doir2['vu_v4'];
  }
      echo ' - 6x6 Door / 
      $' . $doir2['vu_price'] . '
      </td>
      </tr>
      </table>
      </p>
      <p style="text-align:center;">
      <strong>Notes:</strong> 
      <mark>' . $doir2['notes'] . '</mark>
      </p>
      </div>
      <footer></footer>
  </body>
</html>';

echo '<script>
      //window.print();
      </script>';
?>