<?php
include_once('php/connection.php');

//Signature Box Plugin
echo'<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/south-street/jquery-ui.css" rel="stylesheet">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>';


$lgsq = "SELECT * FROM `ownership_changes` WHERE `did` = '" . $_GET['id'] . "'";
$lgsg = mysqli_query($conn, $lgsq) or die($conn->error);
$lgsr = mysqli_fetch_array($lgsg);


//Load Variables
$id4 = $_GET['id'];

//Info Query
$q4 = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $id4 . "'";
$g4 = mysqli_query($conn, $q4) or die($conn->error);
$r4 = mysqli_fetch_array($g4);

echo '<html>
  <head>
  <title>Change of Ownership Form</title>
  <style>
  .main{
  width: 90%;
  margin: auto;
  }
  .header{
  //text-align: center;
  }
  .header img{
  pading-right: 10%;
  width: 30%;
  }
  .header-info{
  margin-left: 35%;
  }
  .header .info{
  position: absolute;
  right: 6%;
  top: 2%;
  }
  h1{
  text-align: center;
  }
  label{
  font-weight: bold;
  }
  .info{
  font-size: 15px;
  }
  #lsign{
  width: 80%;
  height: 80px;
  overflow: hidden;
  }
  .one{
    background: #D9EDF7;
    padding: 10px;
  }
  .two{
    background: #DFF0D8;
    padding: 10px;
  }
  .main_container{
    width:75%;
    margin: auto;
  }
  </style>
  <script src="js/ownership-change-handler.js"></script>
  </head>
  <body>
  <div class="main">
  <div class="header">
  <!--<img src="images/all-steel-red.jpg" />-->
  <img src="' . $_SESSION['org_logo_url'] . '" style="width=100%;">
  <img class="header-info" src="images/header-info.jpeg" />
  <!--<div class="info">
  <label>2200 North Granville Avenue<br>
  Muncie, Indiana 47303</label><br>
  Phone: (765) 284-0694<br>
  Fax: (765) 284-2689<br>
  Email: Michael@allsteelcarports.com
  </div>-->
  </div>
  <div class="main_container">
  <h1>Dealer Change of Ownership Form</h1>
  <div style="font-size:22px;">
  <br>
  <section class="one">
  <h3><u>Current Dealer Name:</u></h3>
  <strong>Business Name:</strong> <span id="currentName">' . $lgsr['oldName'] . '</span>
  <br>
  <strong>Street Address:</strong> <span id="currentAddress">' . $lgsr['oldAddress'] . '</span>
  <br>
  <strong>City:</strong> <span id="currentCity">' . $lgsr['oldCity'] . '</span> &nbsp; 
  <strong>State:</strong> <span id="currentState">' . $lgsr['oldState'] . '</span> &nbsp; 
  <strong>Zip:</strong> <span id="currentZip">' . $lgsr['oldZip'] . '</span>
  <br>';

if($r4['ownername'] == ''){
  echo '<strong>Owner Name:</strong> <span id="currentOwner">Unknown</span>';
}else{
  echo '<strong>Owner Name:</strong> <span id="currentOwner">' . $lgsr['oldOwner'] . '</span>';
  }

echo '<br>
      <strong>Phone#:</strong> <span id="currentPhone">' . $lgsr['oldPhone'] . '</span>
      <br>
      <strong>Email:</strong> <span id="currentEmail">' . $lgsr['oldEmail'] . '</span>
</section>
  <br>
  <section class="two">
  <h3><u>New Dealer Name:</u></h3>
  <strong>New Business Name:</strong> ' . $lgsr['newName'] . '
  <br><br>
  <strong>New Address:</strong> ' . $lgsr['newAddress'] . '
  <br><br>
  <strong>City:</strong> ' . $lgsr['newCity'] . '
  <strong>State:</strong> ' . $lgsr['newState'] . '
  <strong>Zip:</strong> ' . $lgsr['newZip'] . '
  <br><br>
  <strong>Owner&apos;s Name:</strong> ' . $lgsr['newOwner'] . '
  <br><br>
  <strong>Business Phone:</strong> ' . $lgsr['newPhone'] . '
  <br><br>
  <strong>Email:</strong> ' . $lgsr['newEmail'] . '
  <input type="hidden" id="did" value="' . $id4 . '" />
  <input type="hidden" id="uid" value="' . $_SESSION['user_id'] . '" />
  </section>
  
  <p>
  <strong>Date:</strong> ' . date("l, F d, Y") . '
  </p>
  </div>
  </div>
  <footer></footer>
  </div>
  </body>
</html>';

echo '<script>
  //window.print();
  </script>';
?>