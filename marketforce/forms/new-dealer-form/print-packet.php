<?php
include_once('php/connection.php');
//Load Variables
$idx = $_GET['id'];

//Info Query
$qx = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $idx . "'";
$gx = mysqli_query($conn, $qx) or die($conn->error);
$rx = mysqli_fetch_array($gx);

$giq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $idx . "'";
$gig = mysqli_query($conn, $giq) or die($conn->error);
$gir = mysqli_fetch_array($gig);

echo '<title>' . $rx['bizname'] . ' - New Dealer Packet</title>
      <style>
      @media print {
        footer {page-break-after: always;}
      }
      .rotate90{
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
      }
      .icontainer{
        height: 100%;
        width:100%;
        background-image: url("' . $gir['site_plan'] . '");
        background-size: 100% 100%;
      }
      .elemento {
            margin-right: auto;
            color: #fff;
            background-color: #28719d;
            border-color: #0d6efd;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
            vertical-align: middle;
            padding: .375rem .35rem;
            border-radius: .25rem;
        }
        .row_element{
            text-align: center;
        }
        .header .info{
            padding-top: 80px;
        }
      </style>';
echo '<div class="row_element">
      <div class="col-md-12">
        <button onClick="window.print()" class="elemento">Print this page</button>
</div>
</div>';

//Dealer Info Form
include 'dealer-info-form.php';
if($_GET['ps'] == '1'){
  include 'dealer-info-form.php';
  include 'dealer-info-form.php';
}

//Dealer Contract Form
if($gir['contract_form'] != ''){
  echo '<u><h1 style="text-align:center;">' . $gir['bizname'] . ' Dealer Contract</h1></u>';
  echo '<br>';
  echo '<img src="' . $gir['contract_form'] . '?' . rand(0,10000) . '" style="width:100%;" />';
}else{
  //Get Contract Form Name...
  $cfq = "SELECT * FROM `signatures` WHERE `ID` = '" . $idx . "'";
  $cfg = mysqli_query($conn, $cfq) or die($conn->error);
  $cfr = mysqli_fetch_array($cfg);
  $noDate = strtotime("0000-00-00");
  $ddate = strtotime($cfr['ddate']);
  $cdate = strtotime("30 May 2019");
  if($cdate > $ddate && $ddate != $noDate){
    include 'dealer-contract.php';
  }else{
    include 'dealer-contract-2019.php';
  }
  //include 'dealer-contract.php';
}
echo '<footer></footer>';

//Landlord Consent Form
if($gir['consent_form'] != ""){
  echo '<u><h1 style="text-align:center;">' . $gir['bizname'] . '&apos;s Owner/Landlord Consent Form</h1></u>';
  echo '<br>';
  echo '<img src="' . $gir['consent_form'] . '?' . rand(0,10000) . '" style="width:100%;" />';
}else{
include 'landlord-consent-form.php';
}
echo '<footer></footer>';

//Display Order Form
include 'printable-display-order-form.php';
if($_GET['ps'] == '1'){
  include 'printable-display-order-form.php';
  include 'printable-display-order-form.php';
}


echo '<footer></footer>';

//Site Plan
if($gir['site_plan'] != ""){
  if($_GET['ps'] == '1'){
      echo '<u><h1 style="text-align:center;">' . $gir['bizname'] . '&apos;s Site Plan</h1></u>';
      echo '<br>';
      echo '<img src="' . $gir['site_plan'] . '?' . rand(0,10000) . '" style="width: 100%;" />';
      echo '<footer></footer>';
  }
    
  //echo '<div class="icontainer"></div>';
  echo '<u><h1 style="text-align:center;">' . $gir['bizname'] . '&apos;s Site Plan</h1></u>';
  echo '<br>';
  echo '<img src="' . $gir['site_plan'] . '?' . rand(0,10000) . '" style="width: 100%;" />';
}



?>