<?php
session_start();
include_once('php/connection.php');

//Load Variables
$id3 = $_GET['id'];


//Info Query
$q3 = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $id3 . "'";
$g3 = mysqli_query($conn, $q3) or die($conn->error);
$r3 = mysqli_fetch_array($g3);

//Order query
$doiq = "SELECT * FROM `display_orders` WHERE `ID` = '" . $id3 . "'";
$doig = mysqli_query($conn, $doiq) or die($conn->error);
$doir = mysqli_fetch_array($doig);

echo '<html>
  <head>
    <title>Display Order Form</title>
    <style>
      html{
        -webkit-print-color-adjust:exact;
      }
      .main{
        width: 90%;
        margin: auto;
      }
      .header{
        //text-align: center;
      }
      .header img{
        pading-right: 10%;
        width: 30%;
      }
      .header-info{
        margin-left: 35%;
      }
      .header .info{
        position: absolute;
        right: 6%;
        top: 2%;
      }
      h1{
        text-align: center;
      }
      span{
        color: red;
        text-decoration: underline;
        font-size: 20px;
      }
      label{
        font-weight: bold;
      }
      .info{
        font-size: 15px;
      }
      td{
        padding-bottom: 15px;
      }
    </style>
    <script src="js/display-order-handler.js"></script>
  </head>
  <body>
    <div class="main">
      <div class="header">
        <img src="images/all-steel-red.jpg" />
        <img class="header-info" src="images/header-info.jpeg" />
        <!--<div class="info">
        <label>2200 North Granville Avenue<br>
        Muncie, Indiana 47303</label><br>
        Phone: (765) 284-0694<br>
        Fax: (765) 284-2689<br>
        Email: Michael@allsteelcarports.com
        </div>-->
      </div>
      <input type="hidden" id="doid" value="' . $id3 . '" />
      <input type="hidden" id="douser" value="' . $_SESSION['user_id'] . '" />
      <h1>Dealer Display Order Form</h1>
      <br>
      <p style="text-align:right; width: 80%; margin: auto;">
      <a href="printable-display-order-form.php?id=' . $id3 . '" target="_blank">
      <button type="button" style="background-color:blue; color:white; height: 5%; width: 15%;">
      <strong>Print This Form</strong>
      </button>
      </a>
      </p>
      <p>
      <strong>Dealer Name:</strong> ' . $r3['bizname'] . ' &nbsp; <strong>OSR:</strong> ' . $r3['user'] . '
      </p>
      <p>
      <strong>Street Address:</strong> ' . $r3['address'] . '
      </p>
      <p>
      <strong>City:</strong> ' . $r3['city'] . ' &nbsp; <strong>State:</strong> ' . $r3['state'] . ' &nbsp; <strong>Zip:</strong> ' . $r3['zip'] . '
      </p>
      <p>
      <table>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="tcg"'; if($doir['tcg'] == 'true'){echo 'checked';} echo '/> Two Car Garage: 
      </td>
      <td style="width: 75%;">
      <input type="text" id="tcg_v1" value="' . $doir['tcg_v1'] . '" style="width:30px;" />X
      <input type="text" id="tcg_v2" value="' . $doir['tcg_v2'] . '" style="width:30px;" />X
      <input type="text" id="tcg_v3" value="' . $doir['tcg_v3'] . '" style="width:30px;" /> A-Frame / 
      <input type="text" id="tcg_v4" value="' . $doir['tcg_v4'] . '" style="width:30px;" /> - 9x7 Doors / 
      Two-Tone VERT ROOF / 
      <input type="text" id="tcg_v5" value="' . $doir['tcg_v5'] . '" style="width:30px;" /> - 34x76 Walk-In Door /  
      <input type="text" id="tcg_v6" value="' . $doir['tcg_v6'] . '" style="width:30px;" /> Window /
      <br>
      $<input type="text" id="tcg_price" value="' . $doir['tcg_price'] . '" />
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="g"'; if($doir['g'] == 'true'){echo 'checked';} echo ' /> Garage: 
      </td>
      <td style="width: 75%;">
      <input type="text" id="g_v1" value="' . $doir['g_v1'] . '" style="width:30px;" />X
      <input type="text" id="g_v2" value="' . $doir['g_v2'] . '" style="width:30px;" />X
      <input type="text" id="g_v3" value="' . $doir['g_v3'] . '" style="width:30px;" /> A-Frame /  
      Vertical Roof /  
      <input type="text" id="g_v4" value="' . $doir['g_v4'] . '" style="width:30px;" /> - 9x7 Door / 
      <input type="text" id="g_v5" value="' . $doir['g_v5'] . '" style="width:30px;" /> - 32x76 Walk-In Door /  
      <br>
      $<input type="text" id="g_price" value="' . $doir['g_price'] . '" />
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="cua"'; if($doir['cua'] == 'true'){echo 'checked';} echo ' /> Combo Unit A-Frame:  
      </td>
      <td style="width: 75%;">
      <input type="text" id="cua_v1" value="' . $doir['cua_v1'] . '" style="width:30px;" />X
      <input type="text" id="cua_v2" value="' . $doir['cua_v2'] . '" style="width:30px;" />X
      <input type="text" id="cua_v3" value="' . $doir['cua_v3'] . '" style="width:30px;" /> / 
      10ft Enclosed /  
      A-Frame /  
      <input type="text" id="cua_v4" value="' . $doir['cua_v4'] . '" style="width:30px;" /> - 8x7 Door / 
      <br>
      $<input type="text" id="cua_price" value="' . $doir['cua_price'] . '" />
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="cur"'; if($doir['cur'] == 'true'){echo 'checked';} echo ' /> Combo Unit Regular:  
      </td>
      <td style="width: 75%;">
      <input type="text" id="cur_v1" value="' . $doir['cur_v1'] . '" style="width:30px;" />X
      <input type="text" id="cur_v2" value="' . $doir['cur_v2'] . '" style="width:30px;" />X
      <input type="text" id="cur_v3" value="' . $doir['cur_v3'] . '" style="width:30px;" /> / 
      10ft Enclosed /  
      Regular Roof /  
      <input type="text" id="cur_v4" value="' . $doir['cur_v4'] . '" style="width:30px;" /> - 8x7 Door / 
      <br>
      $<input type="text" id="cur_price" value="' . $doir['cur_price'] . '" />
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="rvp"'; if($doir['rvp'] == 'true'){echo 'checked';} echo ' /> RV Port:  
      </td>
      <td style="width: 75%;">
      <input type="text" id="rvp_v1" value="' . $doir['rvp_v1'] . '" style="width:30px;" />X
      <input type="text" id="rvp_v2" value="' . $doir['rvp_v2'] . '" style="width:30px;" />X
      <input type="text" id="rvp_v3" value="' . $doir['rvp_v3'] . '" style="width:30px;" /> / 
      Regular Roof / 
      <input type="text" id="rvp_v4" value="' . $doir['rvp_v4'] . '" style="width:30px;" /> Extra Sheets / 
      <input type="text" id="rvp_v5" value="' . $doir['rvp_v5'] . '" style="width:30px;" /> Supports / 
      <br>
      $<input type="text" id="rvp_price" value="' . $doir['rvp_price'] . '" />
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="cst"'; if($doir['cst'] == 'true'){echo 'checked';} echo ' /> Carport Standard:  
      </td>
      <td style="width: 75%;">
      <input type="text" id="cst_v1" value="' . $doir['cst_v1'] . '" style="width:30px;" />X
      <input type="text" id="cst_v2" value="' . $doir['cst_v2'] . '" style="width:30px;" />X
      <input type="text" id="cst_v3" value="' . $doir['cst_v3'] . '" style="width:30px;" /> / 
      Regular Roof / 
      <br>
      $<input type="text" id="cst_price" value="' . $doir['cst_price'] . '" />
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="csp"'; if($doir['csp'] == 'true'){echo 'checked';} echo ' /> Carport Special:  
      </td>
      <td style="width: 75%;">
      <input type="text" id="csp_v1" value="' . $doir['csp_v1'] . '" style="width:30px;" />X
      <input type="text" id="csp_v2" value="' . $doir['csp_v2'] . '" style="width:30px;" />X
      <input type="text" id="csp_v3" value="' . $doir['csp_v3'] . '" style="width:30px;" /> / 
      A-Frame / 
      <input type="text" id="csp_v4" value="' . $doir['csp_v4'] . '" style="width:30px;" /> Gable End / 
      <input type="text" id="csp_v5" value="' . $doir['csp_v5'] . '" style="width:30px;" /> Sheet On Each Side / 
      <br>
      $<input type="text" id="csp_price" value="' . $doir['csp_price'] . '" />
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="vb"'; if($doir['vb'] == 'true'){echo 'checked';} echo ' /> Vertical Building:  
      </td>
      <td style="width: 75%;">
      <input type="text" id="vb_v1" value="' . $doir['vb_v1'] . '" style="width:30px;" />X
      <input type="text" id="vb_v2" value="' . $doir['vb_v2'] . '" style="width:30px;" />X
      <input type="text" id="vb_v3" value="' . $doir['vb_v3'] . '" style="width:30px;" /> / 
      ALL Vertical / Fully Enclosed / 
      <input type="text" id="vb_v4" value="' . $doir['vb_v4'] . '" style="width:30px;" /> - 10x8 Roll Up Door / 
      <input type="text" id="vb_v5" value="' . $doir['vb_v5'] . '" style="width:30px;" /> - 34x76 Walk-In Door / 
      <br>
      $<input type="text" id="vb_price" value="' . $doir['vb_price'] . '" />
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="u"'; if($doir['u'] == 'true'){echo 'checked';} echo ' /> Utility - Front Railing:  
      </td>
      <td style="width: 75%;">
      <input type="text" id="u_v1" value="' . $doir['u_v1'] . '" style="width:30px;" />X
      <input type="text" id="u_v2" value="' . $doir['u_v2'] . '" style="width:30px;" />X
      <input type="text" id="u_v3" value="' . $doir['u_v3'] . '" style="width:30px;" /> With FLOOR / 
      A-Frame / No Vertical Roof / With RAILING On Front / 
      <input type="text" id="u_v4" value="' . $doir['u_v4'] . '" style="width:30px;" /> - 32x71 Walk-In Door / 
      <input type="text" id="u_v5" value="' . $doir['u_v5'] . '" style="width:30px;" /> - 6x6 Door / 
      $<input type="text" id="u_price" value="' . $doir['u_price'] . '" />
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="au"'; if($doir['au'] == 'true'){echo 'checked';} echo ' /> A-Frame Utility:  
      </td>
      <td style="width: 75%;">
      <input type="text" id="au_v1" value="' . $doir['au_v1'] . '" style="width:30px;" />X
      <input type="text" id="au_v2" value="' . $doir['au_v2'] . '" style="width:30px;" />X
      <input type="text" id="au_v3" value="' . $doir['au_v3'] . '" style="width:30px;" /> With FLOOR / 
      A-Frame / 
      <input type="text" id="au_v4" value="' . $doir['au_v4'] . '" style="width:30px;" /> - 6x6 Door / 
      <br>
      $<input type="text" id="au_price" value="' . $doir['au_price'] . '" />
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="us"'; if($doir['us'] == 'true'){echo 'checked';} echo ' /> Utility Standard:  
      </td>
      <td style="width: 75%;">
      <input type="text" id="us_v1" value="' . $doir['us_v1'] . '" style="width:30px;" />X
      <input type="text" id="us_v2" value="' . $doir['us_v2'] . '" style="width:30px;" />X
      <input type="text" id="us_v3" value="' . $doir['us_v3'] . '" style="width:30px;" /> With FLOOR / 
      <input type="text" id="us_v4" value="' . $doir['us_v4'] . '" style="width:30px;" /> - 6x6 Door / 
      <br>
      $<input type="text" id="us_price" value="' . $doir['us_price'] . '" />
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">
      <input type="checkbox" id="vu"'; if($doir['vu'] == 'true'){echo 'checked';} echo ' /> Vertical Utility:  
      </td>
      <td style="width: 75%;">
      <input type="text" id="vu_v1" value="' . $doir['vu_v1'] . '" style="width:30px;" />X
      <input type="text" id="vu_v2" value="' . $doir['vu_v2'] . '" style="width:30px;" />X
      <input type="text" id="vu_v3" value="' . $doir['vu_v3'] . '" style="width:30px;" /> / 
      ALL VERTICAL / 
      <input type="text" id="vu_v4" value="' . $doir['vu_v4'] . '"style="width:30px;" /> - 6x6 Door / 
      <br>
      $<input type="text" id="vu_price" value="' . $doir['vu_price'] . '" />
      </td>
      </tr>
      </table>
      </p>
      <p style="text-align:center;">
      <strong>Notes:</strong> <textarea id="notes" style="width: 70%; height:150px;">' . $doir['notes'] . '</textarea><br>
      <button type="button" onclick="disp_save();">Save</button> <!--<button type="button" onclick="">Submit</button>-->
      </p>
      </div>
      <footer></footer>
  </body>
</html>';

echo '<script>
      //window.print();
      </script>';
?>