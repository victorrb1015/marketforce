<script>
    $(document).ready(function(){
			$("#dsign").signature();
			$("#rsign").signature();
      $("#dsign").signature("draw", ' . $gsr['dsign'] . ');
      $("#rsign").signature("draw", ' . $gsr['rsign'] . ');
      $("#dbutton_toggle_retry").toggle();
      $("#rbutton_toggle_retry").toggle();
    });
    
    function clear_sign(id){
    if(id == "rsign"){
      $("#rsign").signature("clear");
      }
    if(id == "dsign"){
      $("#dsign").signature("clear");
      }
    }
    
    
    function retry_sign(idx){
      if(idx == "rsign"){
      $("#rsign").signature("enable");
      $("#rbutton_toggle_retry").toggle();
      $("#rbutton_toggle_save").toggle();
      $("#rsign").signature("clear");
      }
    if(idx == "dsign"){
      $("#dsign").signature("enable");
      $("#dbutton_toggle_retry").toggle();
      $("#dbutton_toggle_save").toggle();
      $("#dsign").signature("clear");
      }
    }
    
    
    function save_sign(id){
    var did = "' . $_GET['id'] . '";
    var doc = "Dealer Contract";
		
    if(id == "rsign"){
      var rs = $("#rsign").signature("toJSON");
      $("#rsign").signature("disable");
      $("#rbutton_toggle_save").toggle();
      $("#rbutton_toggle_retry").toggle();
      }
    if(id == "dsign"){
      var ds = $("#dsign").signature("toJSON");
      $("#dsign").signature("disable");
      $("#dbutton_toggle_save").toggle();
      $("#dbutton_toggle_retry").toggle();
      }
      
      if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			alert(this.responseText);
      
      if(id == "rsign"){
      $("#rsign").signature("disable");
      }
    if(id == "dsign"){
      $("#dsign").signature("disable");
      }
      
    }
  }
  xmlhttp.open("GET","http://marketforceapp.com/marketforce/forms/new-dealer-form/php/save-signature.php"+
                      "?did="+did+"&doc="+doc+"&rs="+rs+"&ds="+ds+"&type="+id,true);
  xmlhttp.send();
    }
   </script>