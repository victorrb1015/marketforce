<?php
include_once('php/connection.php');

//Load Variables
$id13 = $_GET['id'];


//Info Query
$q13 = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $id13 . "'";
$g13 = mysqli_query($conn, $q13) or die($conn->error);
$r13 = mysqli_fetch_array($g13);

//Order query
$doiq2 = "SELECT * FROM `display_orders` WHERE `ID` = '" . $id13 . "'";
$doig2 = mysqli_query($conn, $doiq2) or die($conn->error);
$doir2 = mysqli_fetch_array($doig2);

//Relocation query
$relq = "SELECT * FROM `display_relocation` WHERE `ID` = '" . $id13 . "'";
$relg = mysqli_query($conn, $relq) or die($conn->error);
$relr = mysqli_fetch_array($relg);

echo '<html>
  <head>
    <title>Display Relocation Form</title>
    <style>
      html{
        -webkit-print-color-adjust:exact;
      }
      .main{
        width: 90%;
        margin: auto;
      }
      .header{
        //text-align: center;
      }
      .header img{
        pading-right: 10%;
        width: 30%;
      }
      .header-info{
        margin-left: 35%;
      }
      .header .info{
        position: absolute;
        right: 6%;
        top: 2%;
      }
      h1{
        text-align: center;
      }
      span{
        color: red;
        text-decoration: underline;
        font-size: 20px;
      }
      label{
        font-weight: bold;
      }
      .info{
        font-size: 15px;
      }
      td{
        padding-bottom: 15px;
      }
      table{
        width: 100%;
      }
    </style>
    <script src="js/display-relocation-handler.js"></script>
  </head>
  <body>
    <div class="main">
      <div class="header">
        <!--<img src="images/all-steel-red.jpg" />-->
        <img src="' . $_SESSION['org_logo_url'] . '" style="width=100%;">
        <img class="header-info" src="images/header-info.jpeg" />
        <!--<div class="info">
        <label>2200 North Granville Avenue<br>
        Muncie, Indiana 47303</label><br>
        Phone: (765) 284-0694<br>
        Fax: (765) 284-2689<br>
        Email: Michael@allsteelcarports.com
        </div>-->
      </div>
      <input type="hidden" id="doid" value="' . $id13 . '" />
      <input type="hidden" id="douser" value="' . $_SESSION['user_id'] . '" />
      <h1>Dealer Display Relocation Form</h1>
      <br>
      <p>
      <strong>Dealer Name:</strong> ' . $r13['bizname'] . ' &nbsp; <strong>OSR:</strong> ' . $r13['user'] . '
      </p>
      <p>
      <strong>Move From Street Address:</strong> ' . $r13['address'] . '
      </p>
      <p>
      <strong>City:</strong> ' . $r13['city'] . ' &nbsp; <strong>State:</strong> ' . $r13['state'] . ' &nbsp; <strong>Zip:</strong> ' . $r13['zip'] . '
      </p>
      ************************************************************************************************************
      <p>
      <strong>Dealer Name:</strong> ' . $relr['new_dealer_name'] . ' &nbsp; <strong>OSR:</strong> ' . $r13['user'] . '
      </p>
      <p>
      <strong>Move To Street Address:</strong> ' . $relr['new_dealer_address'] . '
      </p>
      <p>
      <strong>City:</strong> ' . $relr['new_dealer_city'] . ' &nbsp; <strong>State:</strong> ' . $relr['new_dealer_state'] . ' &nbsp; <strong>Zip:</strong> ' . $relr['new_dealer_zip'] . '
      </p>
      <p>
      <mark><strong>Move By Date:</strong> ' . $relr['move_by_date'] . '</mark>
      </p>
      ***************** <b style="color:red;">RELOCATE (CHECK MARK) / REMOVE & RETURN TO FACTORY (X MARK)</b> *****************
      <br>
      <p>
      <table>
      <tr>
      <td style="width: 25%;">';
if($relr['tcg'] == 'check'){
      echo'<img src="images/green-check-box.png" style="height:20px; width:20px;" />';
}
if($relr['tcg'] == 'x'){
      echo '<img src="images/square-red-x.png" style="height:20px; width:20px;" />';
}
if($relr['tcg'] == 'na'){
      echo '<img src="images/na-icon.svg" style="height:30px; width:30px;" />';
}
 echo ' &nbsp;1. Two Car Garage: 
      </td>
      <td style="width: 75%;">';
  if($doir2['tcg_v1'] != '22'){
    echo '<mark>' . $doir2['tcg_v1'] . '</mark>';
  }else{
    echo $doir2['tcg_v1'];
  }
      echo 'x';
  if($doir2['tcg_v2'] != '26'){
    echo '<mark>' . $doir2['tcg_v2'] . '</mark>';
  }else{
    echo $doir2['tcg_v2'];
  }
      echo 'x';
  if($doir2['tcg_v3'] != '8'){
    echo '<mark>' . $doir2['tcg_v3'] . '</mark>';
  }else{
    echo $doir2['tcg_v3'];
  }
      echo ' A-Frame / ';
  if($doir2['tcg_v4'] != '2'){
    echo '<mark>' . $doir2['tcg_v4'] . '</mark>';
  }else{
    echo $doir2['tcg_v4'];
  }
      echo ' - 9x7 Doors / 
      Two-Tone VERT ROOF / ';
  if($doir2['tcg_v5'] != '1'){
    echo '<mark>' . $doir2['tcg_v5'] . '</mark>';
  }else{
    echo $doir2['tcg_v5'];
  }
      echo ' - 34x76 Walk-In Door / ';
  if($doir2['tcg_v6'] != '1'){
    echo '<mark>' . $doir2['tcg_v6'] . '</mark>';
  }else{
    echo $doir2['tcg_v6'];
  }
      echo ' Window / 
      $' . $doir2['tcg_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">';
if($relr['g'] == 'check'){
      echo'<img src="images/green-check-box.png" style="height:20px; width:20px;" />';
}
if($relr['g'] == 'x'){
      echo '<img src="images/square-red-x.png" style="height:20px; width:20px;" />';
}
if($relr['g'] == 'na'){
      echo '<img src="images/na-icon.svg" style="height:30px; width:30px;" />';
}
  echo ' &nbsp;2. Garage: 
      </td>
      <td style="width: 75%;">';
  if($doir2['g_v1'] != '18'){
    echo '<mark>' . $doir2['g_v1'] . '</mark>';
  }else{
    echo $doir2['g_v1'];
  }
      
   echo 'x';
  if($doir2['g_v2'] != '26'){
    echo '<mark>' . $doir2['g_v2'] . '</mark>';
  }else{
    echo $doir2['g_v2'];
  }
      echo 'x';
  if($doir2['g_v3'] != '8'){
    echo '<mark>' . $doir2['g_v3'] . '</mark>';
  }else{
    echo $doir2['g_v3'];
  }
       echo 'A-Frame /  
      Vertical Roof / ';
  if($doir2['g_v4'] != '1'){
    echo '<mark>' . $doir2['g_v4'] . '</mark>';
  }else{
    echo $doir2['g_v4'];
  }
      echo ' - 9x7 Door / ';
  if($doir2['g_v5'] != '1'){
    echo '<mark>' . $doir2['g_v5'] . '</mark>';
  }else{
    echo $doir2['g_v5'];
  }
      echo ' - 32x76 Walk-In Door /  
      $' . $doir2['g_price'] . '
      </td>
      </tr>
      </table>
      ***********************************************************************************************************************
      <table>
      <tr>
      <td style="width: 25%;">';
if($relr['cua'] == 'check'){
      echo'<img src="images/green-check-box.png" style="height:20px; width:20px;" />';
}
if($relr['cua'] == 'x'){
      echo '<img src="images/square-red-x.png" style="height:20px; width:20px;" />';
}
if($relr['cua'] == 'na'){
      echo '<img src="images/na-icon.svg" style="height:30px; width:30px;" />';
}
echo ' &nbsp;3. Combo Unit A-Frame:  
      </td>
      <td style="width: 75%;">';
  if($doir2['cua_v1'] != '18'){
    echo '<mark>' . $doir2['cua_v1'] . '</mark>';
  }else{
    echo $doir2['cua_v1'];
  }
      echo 'x';
  if($doir2['cua_v2'] != '31'){
    echo '<mark>' . $doir2['cua_v2'] . '</mark>';
  }else{
    echo $doir2['cua_v2'];
  }
      echo 'x';
  if($doir2['cua_v3'] != '7'){
    echo '<mark>' . $doir2['cua_v3'] . '</mark>';
  }else{
    echo $doir2['cua_v3'];
  }
      echo '/ 
      10ft Enclosed /  
      A-Frame / ';
  if($doir2['cua_v4'] != '1'){
    echo '<mark>' . $doir2['cua_v4'] . '</mark>';
  }else{
    echo $doir2['cua_v4'];
  }
      echo ' - 8x7 Door / 
      $' . $doir2['cua_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">';
if($relr['cur'] == 'check'){
      echo'<img src="images/green-check-box.png" style="height:20px; width:20px;" />';
}
if($relr['cur'] == 'x'){
      echo '<img src="images/square-red-x.png" style="height:20px; width:20px;" />';
}
if($relr['cur'] == 'na'){
      echo '<img src="images/na-icon.svg" style="height:30px; width:30px;" />';
}
echo ' &nbsp;4. Combo Unit Regular:  
      </td>
      <td style="width: 75%;">';
  if($doir2['cur_v1'] != '18'){
    echo '<mark>' . $doir2['cur_v1'] . '</mark>';
  }else{
    echo $doir2['cur_v1'];
  }
      echo 'x';
  if($doir2['cur_v2'] != '31'){
    echo '<mark>' . $doir2['cur_v2'] . '</mark>';
  }else{
    echo $doir2['cur_v2'];
  }
      echo 'x';
  if($doir2['cur_v3'] != '6'){
    echo '<mark>' . $doir2['cur_v3'] . '</mark>';
  }else{
    echo $doir2['cur_v3'];
  }
      echo '/ 
      10ft Enclosed /  
      Regular Roof / ';
  if($doir2['cur_v4'] != '1'){
    echo '<mark>' . $doir2['cur_v4'] . '</mark>';
  }else{
    echo $doir2['cur_v4'];
  }
      echo ' - 8x7 Door / 
      $' . $doir2['cur_price'] . '
      </td>
      </tr>
      </table>
      ***********************************************************************************************************************
      <table>
      <tr>
      <td style="width: 25%;">';
if($relr['rvp'] == 'check'){
      echo'<img src="images/green-check-box.png" style="height:20px; width:20px;" />';
}
if($relr['rvp'] == 'x'){
      echo '<img src="images/square-red-x.png" style="height:20px; width:20px;" />';
}
if($relr['rvp'] == 'na'){
      echo '<img src="images/na-icon.svg" style="height:30px; width:30px;" />';
}
echo ' &nbsp;5. RV Port:  
      </td>
      <td style="width: 75%;">';
  if($doir2['rvp_v1'] != '12'){
    echo '<mark>' . $doir2['rvp_v1'] . '</mark>';
  }else{
    echo $doir2['rvp_v1'];
  }
      echo 'x';
  if($doir2['rvp_v2'] != '31'){
    echo '<mark>' . $doir2['rvp_v2'] . '</mark>';
  }else{
    echo $doir2['rvp_v2'];
  }
      echo 'x';
  if($doir2['rvp_v3'] != '12'){
    echo '<mark>' . $doir2['rvp_v3'] . '</mark>';
  }else{
    echo $doir2['rvp_v3'];
  }
      echo '/ 
      Regular Roof / ';
  if($doir2['rvp_v4'] != '2'){
    echo '<mark>' . $doir2['rvp_v4'] . '</mark>';
  }else{
    echo $doir2['rvp_v4'];
  }
      echo ' Extra Sheets / ';
  if($doir2['rvp_v5'] != '6'){
    echo '<mark>' . $doir2['rvp_v5'] . '</mark>';
  }else{
    echo $doir2['rvp_v5'];
  }
      echo ' Supports / 
      $' . $doir2['rvp_price'] . '
      </td>
      </tr>
      </table>
      ***********************************************************************************************************************
      <table>
      <tr>
      <td style="width: 25%;">';
if($relr['cst'] == 'check'){
      echo'<img src="images/green-check-box.png" style="height:20px; width:20px;" />';
}
if($relr['cst'] == 'x'){
      echo '<img src="images/square-red-x.png" style="height:20px; width:20px;" />';
}
if($relr['cst'] == 'na'){
      echo '<img src="images/na-icon.svg" style="height:30px; width:30px;" />';
}
echo ' &nbsp;6. Carport Standard:  
      </td>
      <td style="width: 75%;">';
  if($doir2['cst_v1'] != '18'){
    echo '<mark>' . $doir2['cst_v1'] . '</mark>';
  }else{
    echo $doir2['cst_v1'];
  }
      echo 'x';
  if($doir2['cst_v2'] != '21'){
    echo '<mark>' . $doir2['cst_v2'] . '</mark>';
  }else{
    echo $doir2['cst_v2'];
  }
      echo 'x';
  if($doir2['cst_v3'] != '6'){
    echo '<mark>' . $doir2['cst_v3'] . '</mark>';
  }else{
    echo $doir2['cst_v3'];
  }
      echo '/ 
      Regular Roof / 
      $' . $doir2['cst_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">';
if($relr['csp'] == 'check'){
      echo'<img src="images/green-check-box.png" style="height:20px; width:20px;" />';
}
if($relr['csp'] == 'x'){
      echo '<img src="images/square-red-x.png" style="height:20px; width:20px;" />';
}
if($relr['csp'] == 'na'){
      echo '<img src="images/na-icon.svg" style="height:30px; width:30px;" />';
}
echo ' &nbsp;7. Carport Special:  
      </td>
      <td style="width: 75%;">';
  if($doir2['csp_v1'] != '24'){
    echo '<mark>' . $doir2['csp_v1'] . '</mark>';
  }else{
    echo $doir2['csp_v1'];
  }
      echo 'x';
  if($doir2['csp_v2'] != '21'){
    echo '<mark>' . $doir2['csp_v2'] . '</mark>';
  }else{
    echo $doir2['csp_v2'];
  }
      echo 'x';
  if($doir2['csp_v3'] != '7'){
    echo '<mark>' . $doir2['csp_v3'] . '</mark>';
  }else{
    echo $doir2['csp_v3'];
  }
      echo '/ 
      A-Frame / ';
  if($doir2['csp_v4'] != '1'){
    echo '<mark>' . $doir2['csp_v4'] . '</mark>';
  }else{
    echo $doir2['csp_v4'];
  }
      echo ' Gable End / ';
  if($doir2['csp_v5'] != '1'){
    echo '<mark>' . $doir2['csp_v5'] . '</mark>';
  }else{
    echo $doir2['csp_v5'];
  }
      echo ' Sheet On Each Side / 
      $' . $doir2['csp_price'] . '
      </td>
      </tr>
      </table>
      ***********************************************************************************************************************
      <table>
      <tr>
      <td style="width: 25%;">';
if($relr['vb'] == 'check'){
      echo'<img src="images/green-check-box.png" style="height:20px; width:20px;" />';
}
if($relr['vb'] == 'x'){
      echo '<img src="images/square-red-x.png" style="height:20px; width:20px;" />';
}
if($relr['vb'] == 'na'){
      echo '<img src="images/na-icon.svg" style="height:30px; width:30px;" />';
}
echo ' &nbsp;8. Vertical Building:  
      </td>
      <td style="width: 75%;">';
  if($doir2['vb_v1'] != '20'){
    echo '<mark>' . $doir2['vb_v1'] . '</mark>';
  }else{
    echo $doir2['vb_v1'];
  }
      echo 'x';
  if($doir2['vb_v2'] != '21'){
    echo '<mark>' . $doir2['vb_v2'] . '</mark>';
  }else{
    echo $doir2['vb_v2'];
  }
      echo 'x';
  if($doir2['vb_v3'] != '9'){
    echo '<mark>' . $doir2['vb_v3'] . '</mark>';
  }else{
    echo $doir2['vb_v3'];
  }
      echo '/ 
      ALL Vertical / Fully Enclosed / ';
  if($doir2['vb_v4'] != '1'){
    echo '<mark>' . $doir2['vb_v4'] . '</mark>';
  }else{
    echo $doir2['vb_v4'];
  }
      echo ' - 10x8 Roll Up Door / ';
  if($doir2['vb_v5'] != '1'){
    echo '<mark>' . $doir2['vb_v5'] . '</mark>';
  }else{
    echo $doir2['vb_v5'];
  }
      echo ' - 34x76 Walk-In Door / 
      $' . $doir2['vb_price'] . '
      </td>
      </tr>
      </table>
      ***********************************************************************************************************************
      <table>
      <tr>
      <td style="width: 25%;">';
if($relr['u'] == 'check'){
      echo'<img src="images/green-check-box.png" style="height:20px; width:20px;" />';
}
if($relr['u'] == 'x'){
      echo '<img src="images/square-red-x.png" style="height:20px; width:20px;" />';
}
if($relr['u'] == 'na'){
      echo '<img src="images/na-icon.svg" style="height:30px; width:30px;" />';
}
echo ' &nbsp;9. Utility - Front Railing:  
      </td>
      <td style="width: 75%;">';
  if($doir2['u_v1'] != '12'){
    echo '<mark>' . $doir2['u_v1'] . '</mark>';
  }else{
    echo $doir2['u_v1'];
  }
      echo 'x';
  if($doir2['u_v2'] != '20'){
    echo '<mark>' . $doir2['u_v2'] . '</mark>';
  }else{
    echo $doir2['u_v2'];
  }
      echo 'x';
  if($doir2['u_v3'] != '7'){
    echo '<mark>' . $doir2['u_v3'] . '</mark>';
  }else{
    echo $doir2['u_v3'];
  }
      echo ' With FLOOR 
      / 
      A-Frame / No Vertical Roof / With RAILING On Front / ';
  if($doir2['u_v4'] != '1'){
    echo '<mark>' . $doir2['u_v4'] . '</mark>';
  }else{
    echo $doir2['u_v4'];
  }
      echo ' - 32x71 Walk-In Door / ';
  if($doir2['u_v5'] != '1'){
    echo '<mark>' . $doir2['u_v5'] . '</mark>';
  }else{
    echo $doir2['u_v5'];
  }
      echo ' - 6x6 Door / 
      $' . $doir2['u_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">';
if($relr['au'] == 'check'){
      echo'<img src="images/green-check-box.png" style="height:20px; width:20px;" />';
}
if($relr['au'] == 'x'){
      echo '<img src="images/square-red-x.png" style="height:20px; width:20px;" />';
}
if($relr['au'] == 'na'){
      echo '<img src="images/na-icon.svg" style="height:30px; width:30px;" />';
}
echo ' &nbsp;10. A-Frame Utility:  
      </td>
      <td style="width: 75%;">';
  if($doir2['au_v1'] != '10'){
    echo '<mark>' . $doir2['au_v1'] . '</mark>';
  }else{
    echo $doir2['au_v1'];
  }
      echo 'x';
  if($doir2['au_v2'] != '12'){
    echo '<mark>' . $doir2['au_v2'] . '</mark>';
  }else{
    echo $doir2['au_v2'];
  }
      echo 'x';
  if($doir2['au_v3'] != '7'){
    echo '<mark>' . $doir2['au_v3'] . '</mark>';
  }else{
    echo $doir2['au_v3'];
  }
      echo ' With FLOOR 
      / 
      A-Frame / ';
  if($doir2['au_v4'] != '1'){
    echo '<mark>' . $doir2['au_v4'] . '</mark>';
  }else{
    echo $doir2['au_v4'];
  }
      echo ' - 6x6 Door / 
      $' . $doir2['au_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">';
if($relr['us'] == 'check'){
      echo'<img src="images/green-check-box.png" style="height:20px; width:20px;" />';
}
if($relr['us'] == 'x'){
      echo '<img src="images/square-red-x.png" style="height:20px; width:20px;" />';
}
if($relr['us'] == 'na'){
      echo '<img src="images/na-icon.svg" style="height:30px; width:30px;" />';
}
echo ' &nbsp;11. Utility Standard:  
      </td>
      <td style="width: 75%;">';
  if($doir2['us_v1'] != '8'){
    echo '<mark>' . $doir2['us_v1'] . '</mark>';
  }else{
    echo $doir2['us_v1'];
  }
      echo 'x';
  if($doir2['us_v2'] != '12'){
    echo '<mark>' . $doir2['us_v2'] . '</mark>';
  }else{
    echo $doir2['us_v2'];
  }
      echo 'x';
  if($doir2['us_v3'] != '6'){
    echo '<mark>' . $doir2['us_v3'] . '</mark>';
  }else{
    echo $doir2['us_v3'];
  }
      echo ' With FLOOR / ';
  if($doir2['us_v4'] != '1'){
    echo '<mark>' . $doir2['us_v4'] . '</mark>';
  }else{
    echo $doir2['us_v4'];
  }
      echo ' - 6x6 Door / 
      $' . $doir2['us_price'] . '
      </td>
      </tr>
      <tr>
      <td style="width: 25%;">';
if($relr['vu'] == 'check'){
      echo'<img src="images/green-check-box.png" style="height:20px; width:20px;" />';
}
if($relr['vu'] == 'x'){
      echo '<img src="images/square-red-x.png" style="height:20px; width:20px;" />';
}
if($relr['vu'] == 'na'){
      echo '<img src="images/na-icon.svg" style="height:30px; width:30px;" />';
}
 echo ' &nbsp;12. Vertical Utility:  
      </td>
      <td style="width: 75%;">';
  if($doir2['vu_v1'] != '10'){
    echo '<mark>' . $doir2['vu_v1'] . '</mark>';
  }else{
    echo $doir2['vu_v1'];
  }
      echo 'x';
  if($doir2['vu_v2'] != '12'){
    echo '<mark>' . $doir2['vu_v2'] . '</mark>';
  }else{
    echo $doir2['vu_v2'];
  }
      echo 'x';
  if($doir2['vu_v3'] != '7'){
    echo '<mark>' . $doir2['vu_v3'] . '</mark>';
  }else{
    echo $doir2['vu_v3'];
  }
      echo '/ 
      ALL VERTICAL / ';
  if($doir2['vu_v4'] != '1'){
    echo '<mark>' . $doir2['vu_v4'] . '</mark>';
  }else{
    echo $doir2['vu_v4'];
  }
      echo ' - 6x6 Door / 
      $' . $doir2['vu_price'] . '
      </td>
      </tr>
      </table>
      </p>
      <p style="text-align:center;">
      <strong>Notes:</strong> 
      ' . $relr['notes'] . '<br>
      </p>
      </div>
      <footer></footer>
  </body>
</html>';

echo '<script>
      //window.print();
      </script>';
?>