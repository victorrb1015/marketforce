<?php
include_once('php/connection.php');

//Signature Box Plugin
echo'<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/south-street/jquery-ui.css" rel="stylesheet">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<!--[if IE]>
<script type="text/javascript" src="http://burtonsolution.tech/plugins/js/excanvas.js"></script>
<![endif]-->

<script type="text/javascript" src="http://burtonsolution.tech/plugins/jquery-signature-pack/jquery.ui.touch-punch.min.js"></script>
<link type="text/css" href="http://burtonsolution.tech/plugins/jquery-signature-pack/jquery.signature.css" rel="stylesheet">
<script type="text/javascript" src="http://burtonsolution.tech/plugins/jquery-signature-pack/jquery.signature.js"></script>
<style>
  body > iframe { display: none; }
  @media print {
        footer {page-break-after: always;}
      }
</style>';

$lgsq = "SELECT * FROM `signatures` WHERE `dealer_id` = '" . $_GET['id'] . "'";
$lgsg = mysqli_query($conn, $lgsq) or die($conn->error);
$lgsr = mysqli_fetch_array($lgsg);

echo'<script>
  $(document).ready(function(){
$("#lsign").signature();
  $("#lbutton_toggle_retry").toggle();
  $("#lsign").signature("draw", ' . $lgsr['lsign'] . ');

  });

  function lclear_sign(id){
  if(id == "lsign"){
  $("#lsign").signature("clear");
  }
  }

  function lretry_sign(idx){
  if(idx == "lsign"){
  $("#lsign").signature("enable");
  $("#lbutton_toggle_retry").toggle();
  $("#lbutton_toggle_save").toggle();
  $("#lsign").signature("clear");
  }
  }

  function lsave_sign(id){
  var ldid = "' . $_GET['id'] . '";
  var ldoc = "Landlord Consent Form";
  var lname = document.getElementById("lname").value;
  var lstate = document.getElementById("lstate").value;
  var ldl = document.getElementById("ldl").value;
  var ldob = document.getElementById("ldob").value;
  
  if(id == "lsign"){
    if(lname == ""){
    alert("Please Enter The Landlords Name");
    return;
  }
  if(ldl == ""){
    alert("Please Enter the Landlords Driver License Number");
    return;
  }
  if(lstate == ""){
    alert("Please Enter The State of The Drivers License");
    return;
  }
  if(ldob == ""){
    alert("Please enter the Date of Birth!");
    return;
  }
  
  var ls = $("#lsign").signature("toJSON");
  $("#lsign").signature("disable");
  $("#lbutton_toggle_save").toggle();
  $("#lbutton_toggle_retry").toggle();
  }

  if (window.XMLHttpRequest) {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
  if (this.readyState==4 && this.status==200) {
  //document.getElementById("livesearch").innerHTML=this.responseText;
alert(this.responseText);

  $("#lsign").signature("disable");

  }
  }
  xmlhttp.open("GET","http://allsteelcarports.com/marketforce/marketforce/forms/new-dealer-form/php/save-signature.php"+
  "?did="+ldid+"&doc="+ldoc+"&ls="+ls+"&type="+id+"&lname="+lname+"&lstate="+lstate+"&ldl="+ldl+"&ldob="+ldob,true);
  xmlhttp.send();
  }
  </script>';

//Load Variables
$id4 = $_GET['id'];

//Info Query
$q4 = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $id4 . "'";
$g4 = mysqli_query($conn, $q4) or die($conn->error);
$r4 = mysqli_fetch_array($g4);

echo '<html>
  <head>
  <title>Landlord Consent Form</title>
  <style>
  .main{
  width: 90%;
  margin: auto;
  }
  .header{
  //text-align: center;
  }
  .header img{
  pading-right: 10%;
  width: 30%;
  }
  .header-info{
  margin-left: 35%;
  }
  .header .info{
  position: absolute;
  right: 6%;
  top: 2%;
  }
  h1{
  text-align: center;
  }
  span{
  color: red;
  text-decoration: underline;
  font-size: 20px;
  }
  label{
  font-weight: bold;
  }
  .info{
  font-size: 15px;
  }
  #lsign{
  width: 80%;
  height: 80px;
  overflow: hidden;
  }
  </style>
  </head>
  <body>
  <div class="main">
  <div class="header">
  <img src="images/all-steel-red.jpg" />
  <img class="header-info" src="images/header-info.jpeg" />
  <!--<div class="info">
  <label>2200 North Granville Avenue<br>
  Muncie, Indiana 47303</label><br>
  Phone: (765) 284-0694<br>
  Fax: (765) 284-2689<br>
  Email: Michael@allsteelcarports.com
  </div>-->
  </div>
  <h1>Landlord Consent Form</h1>
  
  I,_________________________________________ the landlord or owner of the location stated below, give ASC and their
  representatives express permission to place various building display units at the following location being rented or
  leased by the following individual or company listed below:
  <br><br>
  <strong>Business Name:</strong> ' . $r4['bizname'] . '
  <br>
  <strong>Street Address:</strong> ' . $r4['address'] . '
  <br>
  <strong>City:</strong> ' . $r4['city'] . ' &nbsp; <strong>State:</strong> ' . $r4['state'] . ' &nbsp; <strong>Zip:</strong> ' . $r4['zip'] . '
  <br><br>
  I understand that ASC is placing these units on my property for the purpose of promoting sales with the business
  renting/leasing from me. The lessee does not own these buildings but has requested that ASC place the buildings on
  their rented/leased property for the purpose of creating building sales for their business.
  <br>
  I further give permission to ASC and their representative(s) to come onto the property located at the above stated address
  to do maintenance, repairs or to remove or repossess the display units as they deem necessary. I understand that NO ATTACHMENT
  will be made by me to these units as a result of any financial obligations or disputes that may arise from the individual or
  business renting/leasing from me as listed above.
  <br>
  Should the individual or business listed above cease to lease or rent from me, I give my consent for ASC to enter the property
  to remove the displays placed on my property. I also understand that I may contact ASC to request that the buildings be removed
  from the property in the event that the business should cease operations on my property. ASC will make every attempt to remove
  the buildings within 45-60 days of notification or earlier.
  <br><br>
  <strong>Landlord Business Name:</strong>______________________________________________________________
  <br>
  <strong>Landlord&apos;s Address:</strong>_____________________________________________________________
  <br>
  <strong>City:</strong>_____________________________<strong>State:</strong>_____________________________<strong>Zip:</strong>____________
  <br>
  <strong>Business Phone:</strong> (____)_________________________ <strong>Fax:</strong> (____)_________________________
  <br>
  <strong>Email:</strong>_______________________________________________________________________________
  <br>
  <strong>Lease at this property expires:</strong> <span>' . $r4['expiration'] . '</span>
  
  <!--<strong>Landlord Signature:</strong><br><br>(Print)_________________________________________/(Signature)_________________________________________-->
  <strong>Authorized All Steel Carports Representative Signature:</strong><br><br>
  <strong>Landlord Name:</strong><input type="text" id="lname" value="' . $lgsr['lname'] . '" /> 
  <strong>DL#:</strong><input type="text" id="ldl" value="' . $lgsr['ldl#'] . '" /> 
  <strong>DL State:</strong><input type="text" id="lstate" value="' . $lgsr['lstate'] . '" maxlength="2" style="width:30px;" />
  <br>
  <strong>Date of Birth:</strong><input type="text" id="ldob" value="' . $lgsr['ldob'] . '" />
  <br>
  <div id="lsign"></div><br>
  <button type="button" onclick="lclear_sign(&apos;lsign&apos;);">Clear</button>
  <span id="lbutton_toggle_save"><button type="button" onclick="lsave_sign(&apos;lsign&apos;);">Save</button></span>
  <span id="lbutton_toggle_retry"><button type="button" onclick="lretry_sign(&apos;lsign&apos;);">Retry Signature</button></span>
  <br><br><br>
  
  <p>
  <strong>Date:</strong> ' . date("l, F d, Y") . '
  </p>
  <footer></footer>
  </div>
  </body>
</html>';

echo '<script>
  //window.print();
  </script>';
?>