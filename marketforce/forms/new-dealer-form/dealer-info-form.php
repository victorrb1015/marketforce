<?php
include_once('php/connection.php');

//Get Organization Data...
/*
$oq = "SELECT * FROM `organizations` WHERE `inactive` != 'Yes'";
$og = mysqli_query($conn, $oq) or die($conn->error);
$or = mysqli_fetch_array($og);
$org_logo = $or['logo_url'];
*/
//var_dump($_SESSION);

//Load Variables
$id2 = $_GET['id'];


//Info Query
$q2 = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $id2 . "'";
$g2 = mysqli_query($conn, $q2) or die($conn->error);
$r2 = mysqli_fetch_array($g2);


echo '<html>
  <head>
    <title>Dealer Information Form</title>
    <style>
      .main{
        width: 90%;
        margin: auto;
      }
      .header{
        //text-align: center;
      }
      .header img{
        pading-right: 10%;
        width: 30%;
      }
      .header-info{
        margin-left: 35%;
      }
      .header .info{
        position: absolute;
        right: 6%;
        top: 2%;
      }
      h1{
        text-align: center;
      }
      h2, h3, h5{
        text-align: center;
        color: red;
      }
      span{
        color: red;
        text-decoration: underline;
        font-size: 20px;
      }
      label{
        font-weight: bold;
      }
      .info{
        font-size: 15px;
      }
      
    </style>
  </head>
  <body>
    <div class="main">
      <div class="header">
        <!--<img src="images/all-steel-red.jpg" />-->
        <img src="' . $_SESSION['org_logo_url'] . '" style="width=100%;">
        <!--<img class="header-info" src="images/header-info.jpeg" />-->
        <div class="info">
        <label>' . $_SESSION['org_address'] . '<br>
        ' . $_SESSION['org_city'] . ', ' . $_SESSION['org_state'] . ' ' . $_SESSION['org_zip'] . '</label><br>
        Phone: ' . $_SESSION['org_phone'] . '<br>
        Email: ' . $_SESSION['org_email'] . '
        </div>
      </div>
      <h1>'.$_SESSION['org_full_name'].' Dealer Information Form</h1>
      
      <h2 class="bname">' . $r2['bizname'] . '</h2>
      <br>
      <div style="font-size: 20px;">
      <strong>Address:</strong> ' . $r2['address'] . '
      <br>
      <strong>City:</strong> ' . $r2['city'] . ' &nbsp; <strong>State:</strong> ' . $r2['state'] . ' &nbsp; <strong>Zip:</strong> ' . $r2['zip'] . '
      <br>
      <strong>Phone:</strong> ' . $r2['phone'] . ' &nbsp; <strong>Fax:</strong> ' . $r2['fax'] . '
      <br>
      <strong>Mail Address:</strong>' . $r2['address'] . '
      <br>
      <strong>City:</strong> ' . $r2['city'] . ' &nbsp; <strong>State:</strong> ' . $r2['state'] . ' &nbsp; <strong>Zip:</strong> ' . $r2['zip'] . '
      <br>
      <strong>The property this business operates on is:</strong> ' . $r2['property'];

      if($r2['property'] != 'OWNED'){
      echo '<br>
            <strong>The Lease or Rental Agreement expires:</strong> ' . $r2['expiration'];
      }
      
      echo '<br>
      <strong>Email:</strong> ' . $r2['email'] . ' &nbsp; <strong>Website:</strong> ' . $r2['website'] . '
      <br>
      <strong>Years in Business:</strong> ' . $r2['years'] . ' &nbsp; <strong>Number of Locations:</strong> ' . $r2['numbers'] . '
      <br>
      <strong>EIN#:</strong> ' . $r2['EIN'] . '
      
      <h5>Who will be responsible for selling buildings on this lot?</h5>
      
      <strong>Name:</strong> ' . $r2['name1'] . ' &nbsp; <strong>Email:</strong> ' . $r2['email1'] . '
      <br>
      <strong>Name:</strong> ' . $r2['name2'] . ' &nbsp; <strong>Email:</strong> ' . $r2['email2'] . '
      <br>
      <strong>Products or Services sold by the business:</strong> ' . $r2['products'] . '
      <br>
      <strong>Number of Employees Full Time:</strong> ' . $r2['fulltime'] . ' &nbsp; 
      <strong>Number of Employees Part Time:</strong> ' . $r2['parttime'] . ' &nbsp;
      <br>
      <strong>Business Owner&apos;s Name</strong> ' . $r2['ownername'] . '
      <br>
      <strong>Owner&apos;s Address</strong> ' . $r2['owneraddress'] . '
      <br>
      <strong>Owner&apos;s Phone#:</strong> ' . $r2['ownerphone'] . '
      <br>
      <strong>Alternate Phone#:</strong> ' . $r2['altphone'] . '
      
      <h5>Traffic Information</h5>
      
      <strong>Dealer located on a main road?</strong> ' . $r2['mainroad'] . '
      <br>
      <strong>Visibility from the main road is:</strong> ' . $r2['visibility'] . '
      <br>
      <strong>Size Of Display Space Available:</strong> ' . $r2['displayspace'] . '
      <br>
      <strong>Is the dealer currently selling for another carport company?</strong> ' . $r2['currentcompany'];

if($r2['currentcompany'] == 'Yes'){
  echo '<br>
        <strong>If so, who with?</strong> ' . $r2['who'];
}

    echo '<br>
      <strong>Is there a sales area where a customer can be seated that is dedicated to the sale 
      of buildings located within the facility?</strong> ' . $r2['salesarea'];

if($r2['salesarea'] == 'Yes'){
  echo '<br>
        <strong>If so, where?</strong> ' . $r2['where'];
}

      echo'<br>
      <strong>What other products does this location sell that associates with our buildings?</strong>
      <br>
      <strong>Who are the dealers within 15 miles of this location?</strong> ' . $r2['closedealers'] . '
      <br>
      <strong>Recommended RTO Programs?</strong> ' . $r2['rto'] . '
      <br>
      <strong>BBB Rating:</strong> ' . $r2['bbb'] . '
      <br>
      <strong>Mantra:</strong> ' . $r2['mantra'] . '
      <br><br>
      <!--<strong>This information was submitted by <span>' . $r2['user'] . '</span></strong><br>-->
      <strong>This Dealer is assigned to <span>' . $r2['user'] . '</span></strong><br>
      <strong>Date:</strong> ' . date("l, F d, Y", strtotime($r2['date'])) . '
     </div>
     <footer></footer>
    </div>
  </body>
</html>';

echo '<script>
      //window.print();
      </script>';
?>