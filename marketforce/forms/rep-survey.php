<?php
error_reporting(0);
include '../php/connection.php';
include '../security/session/get-org-info.php';
//Load Variables
$dealer = $_GET['d'];
$rep = $_GET['r'];
$vdate = $_GET['vd'];
?>
<html>
  <head>
    <title>Performance Survey | All Steel Carports</title>
    
     <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="../js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <!--Survey Handler-->
    <script src="js/rep-survey-handler.js"></script>
 
    <style>
      .form-group label{
        font-size: 25px;
        vertical-align: middle;
      }
    </style>
  </head>
  <body>
    <div class="header" style="margin: auto; text-align: center;">
      <!--<img src="../img/logo-allsteel.png">-->
      
    </div>
    <br>
    <div id="main">
      <div id="page-wrapper" style="width: 80%; margin: auto;">
        
     
      <div class="row">
        
        <div class="col-lg-12" style="margin: auto; text-align: center;">
        <h1 class="page-header">
          Outside Sales Representative Training Survey
        </h1>
         
          <h4 style="width: 80%; margin: auto;">
            Thank you so much for making time available for your All Steel Carports’ Sales
            Representative to meet with you and/or your sales team for training.  Your 
            feedback on the visit can insure our team is always doing the best we can to meet your 
            needs on the sales front!<br><br>

            This quick survey will greatly assist in making improvements and learning from your experience.  <br><br>
            The information is <span style="color: red;">CONFIDENTIAL</span> and used for training purposes only.
            <br><br>
            This <span style="color: red;"><i>CONFIDENTIAL</i></span> survey is being submitted to: <br><br><b>Michael Burton
            <br>Director of Sales & Marketing<br>
            All Steel Carports</b>
          </h4>
          
          <h1 class="page-header"></h1>
        </div>
      </div><!--//Row-->
        
    <div class="row">
      <div class="col-lg-6">
          <h3>
            Your Sales Representative: <b style="color: blue;"><?php echo $rep; ?></b>
          </h3>
          <h3>
            Date Of Visit: <b style="color: blue;"><?php echo $vdate; ?></b>
          </h3>
        
      </div>
      
      
        <div class="col-lg-6" style="text-align: center;">
          <h3 style="color:blue;">
            <b><span id="starr" style="color:red;"></span></b>
            How Long Did Your Rep Visit With You?
          </h3>
          <div class="q" style="width: 50%; margin: auto;">
       
        <div class="form-group input-group">
           <input type="text" class="form-control" id="hrs">
           <span class="input-group-addon" style="color:blue;">Hrs</span>
           <input type="text" class="form-control" id="mins">
           <span class="input-group-addon" style="color:blue;">Mins</span>
        </div>
          </div>
        </div><!--//Container-->
        
      
      </div><!--//Row-->
      
      <h1 class="page-header"></h1>
      <div class="row">
        <div class="col-lg-12">
          
        <h3 class="page-header" style="text-align:center;">
          <strong> Please Grade The Following Statements:</strong>
        </h3>
          <h2>
           &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; My Outside Sales Representative...
          </h2>
          <br>
     
       
         <div class="alert alert-info" style="color: black;">   
          <h3>
            <b><span id="q1_star" style="color:red;"></span></b>1. Made the visit well worth my time.
          </h3>
          
      <div class="form-group">
         <!--<label>Inline Radio Buttons</label>-->
         <label class="radio-inline">
             <input type="radio" id="" name="q1" value="5">A
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q1" value="4">B
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q1" value="3">C
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q1" value="2">D
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q1" value="1">F
         </label>
        
     </div>
          </div>
          <br>
          
         <div class="alert alert-info" style="color: black;">   
          <h3>
          <b><span id="q2_star" style="color:red;"></span></b>2. Was able to answer the questions I had with their knowledge of the product and sales procedures.
          </h3>
          
      <div class="form-group">
         <!--<label>Inline Radio Buttons</label>-->
        <label class="radio-inline">
             <input type="radio" id="" name="q2" value="5">A
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q2" value="4">B
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q2" value="3">C
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q2" value="2">D
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q2" value="1">F
         </label>
     </div>
          </div>
          <br>
          
         <div class="alert alert-info" style="color: black;">   
          <h3>
          <b><span id="q3_star" style="color:red;"></span></b>3.	Took the time to make sure we understood and could work with the information.
          </h3>
          
      <div class="form-group">
         <!--<label>Inline Radio Buttons</label>-->
         <label class="radio-inline">
             <input type="radio" id="" name="q3" value="5">A
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q3" value="4">B
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q3" value="3">C
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q3" value="2">D
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q3" value="1">F
         </label>
     </div>
          </div>
          <br>
          
         <div class="alert alert-info" style="color: black;">   
          <h3>
          <b><span id="q4_star" style="color:red;"></span></b>4.	Replaced and/or provided me with brochures and sales materials including order forms that we needed.
          </h3>
          
      <div class="form-group">
         <!--<label>Inline Radio Buttons</label>-->
         <label class="radio-inline">
             <input type="radio" id="" name="q4" value="5">A
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q4" value="4">B
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q4" value="3">C
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q4" value="2">D
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q4" value="1">F
         </label>
     </div>
          </div>
          <br>
          
          
          
         <div class="alert alert-info" style="color: black;">   
          <h3>
          <b><span id="q5_star" style="color:red;"></span></b>5.	Invited me to call or email them personally with any questions or assistance I needed for the future.
          </h3>
          
      <div class="form-group">
         <!--<label>Inline Radio Buttons</label>-->
         <label class="radio-inline">
             <input type="radio" id="" name="q5" value="5">A
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q5" value="4">B
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q5" value="3">C
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q5" value="2">D
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q5" value="1">F
         </label>
     </div>
          </div>
          <br>
          
          
         <div class="alert alert-info" style="color: black;">   
           <h3>
          <b><span id="q6_star" style="color:red;"></span></b>6.	Would help me any way possible if needed.
          </h3>
          
      <div class="form-group">
         <!--<label>Inline Radio Buttons</label>-->
         <label class="radio-inline">
             <input type="radio" id="" name="q6" value="5">A
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q6" value="4">B
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q6" value="3">C
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q6" value="2">D
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q6" value="1">F
         </label>
     </div>
          </div>
          <br>
          
          
          
         <div class="alert alert-info" style="color: black;">   
           <h3>
          <b><span id="q7_star" style="color:red;"></span></b>7.	Seems genuinely interested in our success in sales for All Steel Carports.
          </h3>
          
      <div class="form-group">
         <!--<label>Inline Radio Buttons</label>-->
         <label class="radio-inline">
             <input type="radio" id="" name="q7" value="5">A
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q7" value="4">B
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q7" value="3">C
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q7" value="2">D
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q7" value="1">F
         </label>
     </div>
          </div>
          <br>
          
          
         <div class="alert alert-info" style="color: black;">   
           <h3>
          <b><span id="q8_star" style="color:red;"></span></b>8.	Provides me with accurate and timely responses when I call, text or email.
          </h3>
          
      <div class="form-group">
         <!--<label>Inline Radio Buttons</label>-->
         <label class="radio-inline">
             <input type="radio" id="" name="q8" value="5">A
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q8" value="4">B
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q8" value="3">C
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q8" value="2">D
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q8" value="1">F
         </label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

        <label class="radio-inline">
             <input type="radio" id="" name="q8" value="5">N/A
         </label>
     </div>
          </div>
          <br>
          
          
          
          
         <div class="alert alert-info" style="color: black;">   
           <h3>
          <b><span id="q9_star" style="color:red;"></span></b>9.	Responds to my Texts, Calls and Emails within 24 hours.
          </h3>
          
      <div class="form-group">
         <!--<label>Inline Radio Buttons</label>-->
         <label class="radio-inline">
             <input type="radio" id="" name="q9" value="5">A
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q9" value="4">B
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q9" value="3">C
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q9" value="2">D
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q9" value="1">F
         </label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q9" value="5">N/A
         </label>
     </div>
          </div>
          <br>
          
          
          
          
         <div class="alert alert-info" style="color: black;">   
           <h3>
          <b><span id="q10_star" style="color:red;"></span></b>10.	Would be a top choice if I were hiring for my company.
          </h3>
          
      <div class="form-group">
         <!--<label>Inline Radio Buttons</label>-->
         <label class="radio-inline">
             <input type="radio" id="" name="q10" value="5">A
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q10" value="4">B
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
         <label class="radio-inline">
             <input type="radio" id="" name="q10" value="3">C
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q10" value="2">D
         </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="radio-inline">
             <input type="radio" id="" name="q10" value="1">F
         </label>
     </div>
          </div>
          
          <h1 class="page-header"></h1>
          
        <div class="alert alert-info" style="color: black;">
          <h3>
            Share any additional comments about this visit: (<span style="color: red;">CONFIDENTIAL</span>)
          </h3>
          <div class="form-group">
             <!--<label>Text area</label>-->
             <textarea class="form-control" rows="3" id="ta1"></textarea>
          </div>
          <br>
          <h3>
            Please let me know how we can better assist you and your sales team:
          </h3>
          <div class="form-group">
             <!--<label>Text area</label>-->
             <textarea class="form-control" rows="3" id="ta2"></textarea>
          </div>
          <br>
           
          <div style="text-align: center;">
            <input type="hidden" id="dealer_name" value="<?php echo $dealer; ?>" />
            <input type="hidden" id="visit_date" value="<?php echo $vdate; ?>" />
            <input type="hidden" id="rep" value="<?php echo $rep; ?>" />
           <button type="button" class="btn btn-default" onclick="sub_form();">Submit Survey</button>
          </div>
          </div>
          
        </div>
        
      </div><!--//Row-->
      
      
      </div>
    </div><!--//Wrapper-->
    <?php include '../footer.html'; ?>
  </body>
</html>