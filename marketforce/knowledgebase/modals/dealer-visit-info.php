		 <!-- Modal -->
  <div class="modal fade" id="dealerVisitInfo" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="pause_vid();" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Learn About The New Dealer Visit Information Dashboard:</h3>
        </div>
        <div class="modal-body">
					<video id="auto-plan" height="100%" width="100%" controls>
						<source src="http://burtonsolution.tech/img-storage/knowledgebase/videos/dealer-visit-info.mov" type="video/mp4">
						Your browser does not support the video tag.
					</video>
          <h4>
						The new Dealer Visit Information Dashboard allows you to track your Prospects (Previously called "Cold Calls") and Dealers by state in a very organized fashion!
					</h4>
					
        </div>
        <div class="modal-footer">
					<!--<button type="button" class="btn btn-success" onclick="">Submit</button>-->
          <button type="button" class="btn btn-default" onclick="pause_vid();" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->