<?php
session_start();

include '../../php/connection.php';

$_SESSION['org_id'] = $_GET['org_id'];
$dbq = "SELECT * FROM `organizations` WHERE `org_id` = '" . $_GET['org_id'] . "'";
$dbg = mysqli_query($mf_conn, $dbq) or die($mf_conn->error);
$dbr = mysqli_fetch_array($dbg);
$_SESSION['org_db_name'] = $dbr['db_name'];

include '../../php/connection.php';
$cache_buster = uniqid();
?>
<html>
<head>
	<title>Shop Orders Display</title>
	<link rel="stylesheet" href="css/style.css?cb=<?php echo $cache_buster; ?>" />

	<!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

	<div class="title">
		<h1>Pending Shop Orders</h1>
	</div>

<div id="main">


		
	<?php
	/*$oq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `in_shop` = 'Yes' AND `shop_done` != 'Yes'";
	$og = mysqli_query($conn, $oq) or die($conn->error);
	$ii = 1;
	while($or = mysqli_fetch_array($og)){
		 if($ii % 2 == 0){ 
    	      $ri = "even";  
    	  } 
    	  else{ 
    	      $ri = "odd"; 
    	  } 
		
		echo '<div class="row order-row ' . $ri . '">
			  <div class="col-lg-4 order-cell">' . date("m/d/y",strtotime($or['date'])) . '</div>
			  <div class="col-lg-4 order-cell">' . $or['cname'] . '</div>
			  <div class="col-lg-4 order-cell">Inv# ' . $or['inv'] . '</div>
			  </div>';
		$ii++;
		}*/
	?>

</div>

</body>
<!-- jQuery -->
<script src="http://marketforceapp.com/marketforce/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="http://marketforceapp.com/marketforce/js/bootstrap.min.js"></script>
	
	<script>
		(function refresh_orders(){
			var org_id = '<?php echo $_GET['org_id']; ?>';
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			console.log(this.responseText);
      //window.location.reload();
			if(JSON.parse(this.response)){
				var r = JSON.parse(this.response);
				for(var i = 0; i < r.order.length; i++){
					var rr = r.order[i];
					var m = document.getElementById('main');
					var row = document.createElement('div');
					row.setAttribute('class','row order-row '+rr.row_color);
					var c1 = document.createElement('div');
					c1.setAttribute('class','col-lg-4 order-cell');
					c1.innerHTML = rr.date;
					var c2 = document.createElement('div');
					c2.setAttribute('class','col-lg-4 order-cell');
					c2.innerHTML = rr.name;
					var c3 = document.createElement('div');
					c3.setAttribute('class','col-lg-4 order-cell');
					c3.innerHTML = rr.inv_num;
					row.appendChild(c1);
					row.appendChild(c2);
					row.appendChild(c3);
					m.appendChild(row);
				}
			}else{
				alert('ERROR!');
			}
      
    }
  }
  xmlhttp.open("GET","php/fetch-orders.php?org_id="+org_id,true);
  xmlhttp.send();
		})();
		
		setInterval(function refresh_orders(){
			var org_id = '<?php echo $_GET['org_id']; ?>';
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			console.log(this.responseText);
      //window.location.reload();
			if(JSON.parse(this.response)){
				document.getElementById('main').innerHTML = '';
				var r = JSON.parse(this.response);
				for(var i = 0; i < r.order.length; i++){
					var rr = r.order[i];
					var m = document.getElementById('main');
					var row = document.createElement('div');
					row.setAttribute('class','row order-row '+rr.row_color);
					var c1 = document.createElement('div');
					c1.setAttribute('class','col-lg-4 order-cell');
					c1.innerHTML = rr.date;
					var c2 = document.createElement('div');
					c2.setAttribute('class','col-lg-4 order-cell');
					c2.innerHTML = rr.name;
					var c3 = document.createElement('div');
					c3.setAttribute('class','col-lg-4 order-cell');
					c3.innerHTML = rr.inv_num;
					row.appendChild(c1);
					row.appendChild(c2);
					row.appendChild(c3);
					m.appendChild(row);
				}
			}else{
				alert('ERROR!');
			}
      
    }
  }
  xmlhttp.open("GET","php/fetch-orders.php?org_id="+org_id,true);
  xmlhttp.send();
		},10000);
	</script>
</html>