function new_vendor(){
  document.getElementById('vendor_form').reset();
  document.getElementById('vendor_id').value = '';
  document.getElementById('vendor_mode').value = 'New';
}


function save_vendor(){
  
  var validity = document.getElementById('vendor_form').reportValidity();
  if(validity === true){
    var url = 'orders/php/add-edit-vendor.php';
    var params = $('#vendor_form').serialize();
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {//Call a function when the state changes.
        if(xhr.readyState == 4 && xhr.status == 200) {
          //Do Something...
          var r = JSON.parse(this.responseText);
          if(r.response === 'GOOD'){
            toast_alert('Success!',r.message,'bottom-right','success');
            $('#newVendorModal').modal('toggle');
            //add_vendor_row(r.data);
            window.location.reload();
          }else{
            toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
          }
        }
    }
    xhr.open('POST', url, true);
    //Send the proper header information along with the request
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send(params); 
  }
}


function edit_vendor(vendor_id){
  document.getElementById('vendor_mode').value = 'Edit';
  document.getElementById('vendor_id').value = vendor_id;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var q = JSON.parse(this.responseText);
      document.getElementById('vendor_name').value = q.vendor_name;
      document.getElementById('contact_name').value = q.contact_name;
      document.getElementById('vendor_email').value = q.vendor_email;
      document.getElementById('vendor_phone').value = q.vendor_phone;
      document.getElementById('vendor_notes').value = q.vendor_notes;
      
    }
  }
  xmlhttp.open("GET","orders/php/get-vendor-details.php?vendor_id="+vendor_id,true);
  xmlhttp.send();
}


function remove_vendor(vendor_id){
  var prompt = confirm("Are you sure you want to remove this Vendor?");
  if(prompt === false){
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById('vendor_row_'+vendor_id).remove();
        toast_alert('Success!',r.message,'bottom-right','success');
      }else if(r.response === 'Error'){
        toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
      }else{
        toast_alert('Something Went Wrong...','An error ocurred while processing your request.','bottom-right','error');
      }
      
    }
  }
  xmlhttp.open("GET","orders/php/delete-vendor.php?vendor_id="+vendor_id,true);
  xmlhttp.send();
}


function add_vendor_row(v){
  console.log(v);
  var tbody = document.getElementById('vendors_tbody');
  //Create Row
  var tr = document.createElement('tr');
  tr.id = 'vendor_row_'+v.vendor_id;
  //Named
  var td = document.createElement('td');
  td.innerHTML = v.vendor_name;
  tr.appendChild(td);
  //Contact
  td = document.createElement('td');
  td.innerHTML = v.contact_name;
  tr.appendChild(td);
  //Phone #
  td = document.createElement('td');
  td.innerHTML = v.vendor_phone;
  tr.appendChild(td);
  //Email
  td = document.createElement('td');
  td.innerHTML = v.vendor_email;
  tr.appendChild(td);
  //Notes
  td = document.createElement('td');
  td.innerHTML = v.vendor_notes;
  tr.appendChild(td);
  //Category
  /*td = document.createElement('td');
  td.innerHTML = v.category;
  tr.appendChild(td);*/
  //Actions
  td = document.createElement('td');
    //Edit Button
    var btn = document.createElement('button');
    btn.setAttribute('class','btn btn-default btn-sm');
    btn.setAttribute('type','button');
    btn.setAttribute('data-toggle','modal');
    btn.setAttribute('data-target','#newVendorModal');
    btn.setAttribute('onclick','edit_vendor('+v.vendor_id+');');
    btn.innerHTML = 'Edit';
    td.appendChild(btn);
    //Deactivate Button
    btn = document.createElement('button');
    btn.setAttribute('class','btn btn-danger btn-sm');
    btn.setAttribute('type','button');
    btn.setAttribute('onclick','remove_vendor('+v.vendor_id+');');
    btn.innerHTML = 'Deactivate';
    td.appendChild(btn);
    //Email Vendor Button
    btn = document.createElement('button');
    btn.setAttribute('class','btn btn-warning btn-sm');
    btn.setAttribute('type','button');
    var a = document.createElement('a');
    a.href = 'mailto:'+v.vendor_email;
    a.innerHTML = 'Email Vendor';
    btn.appendChild(a);
    td.appendChild(btn);
  tr.appendChild(td);
  
  //Destroy DataTable
  var dt = $("#vendors_table").dataTable();
  if(v.vendor_mode === 'Edit'){
    document.getElementById('vendor_row_'+v.vendor_id).remove();
    console.log('Removing Row: '+v.vendor_id);
    dt.fnClearTable();
  }
  dt.fnDestroy();
  
  //Add Row To Table
  tbody.appendChild(tr);
  
  //Run DataTables Activation
  $("#vendors_table").dataTable({
    "paging": false,
  });
  
  
}






















































