function edit_special_pricing(cid){
  document.getElementById('sp_form').reset();
  document.getElementById('sp_cid').value = cid;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      console.log(this.responseText);
      var q = JSON.parse(this.responseText);
      
			Object.keys(q).forEach(function(key) {
        console.log('Key : ' + key + ', Value : ' + q[key]);
        if(key.includes('_lockout')){
          if(q[key] === 'Yes'){
            if(document.getElementById(key)){
              document.getElementById(key).checked = true;
            }
          }else{
            if(document.getElementById(key)){
              document.getElementById(key).checked = false;
            }
          }
        }else{
          if(document.getElementById(key+'_sprice')){
            document.getElementById(key+'_sprice').value = q[key];
          }
        }
      });
      
    }
  }
  xmlhttp.open("GET","orders/php/get-special-pricing.php?cid="+cid+"&ponly=y",true);
  xmlhttp.send();
}


function remove_special_price(pid){
  var cid = document.getElementById('sp_cid').value;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      alert(this.responseText);
      document.getElementById(pid+'_sprice').value = '';
      
    }
  }
  xmlhttp.open("GET","orders/php/remove-special-pricing.php?cid="+cid+"&pid="+pid,true);
  xmlhttp.send();
}