function add_color(list){
  var mode = 'Add';
  var color = document.getElementById(list+'_color').value;
  if(color === ''){
    document.getElementById(list+'_response').innerHTML = '<b style="color:red;font-weight:bold;">*You must enter a color...</b>';
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var r = JSON.parse(this.responseText);
      console.log(r);
      if(r.status === 'GOOD'){
        document.getElementById(list+'_response').innerHTML = r.response;
        document.getElementById(list+'_color').value = '';
        document.getElementById(list+'_color').focus();
        var tr = document.createElement('tr');
        tr.id = list+'_color_'+r.colorID;
        var td = document.createElement('td');
        td.innerHTML = color;
        tr.appendChild(td);
        var td = document.createElement('td');
        var btn = document.createElement('button');
        btn.setAttribute('type','button');
        btn.setAttribute('class','btn btn-danger btn-xs');
        btn.setAttribute('onclick','remove_color(\''+list+'\','+r.colorID+');');
        var icon = document.createElement('i');
        icon.setAttribute('class','fa fa-times');
        btn.appendChild(icon);
        td.appendChild(btn);
        tr.appendChild(td);
        document.getElementById(list+'_tableBody').appendChild(tr);
        sortTable(list);
      }else{
        document.getElementById(list+'_response').innerHTML = r.response;
      }
      
    }
  }
  xmlhttp.open("GET","orders/php/add-remove-colors.php?mode="+mode+"&list="+list+"&color="+color+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
}



function remove_color(list, id){
  var mode = 'Remove';
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var r = JSON.parse(this.responseText);
      console.log(r);
      if(r.status === 'GOOD'){
        document.getElementById(list+'_response').innerHTML = r.response;
        document.getElementById(list+'_color').value = '';
        document.getElementById(list+'_color_'+id).remove();
      }else{
        document.getElementById(list+'_response').innerHTML = r.response;
      }
      
    }
  }
  xmlhttp.open("GET","orders/php/add-remove-colors.php?mode="+mode+"&list="+list+"&id="+id+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
}


function sortTable(list) {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById(list+"_table");
  switching = true;
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[0];
      y = rows[i + 1].getElementsByTagName("TD")[0];
      //check if the two rows should switch place:
      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
        //if so, mark as a switch and break the loop:
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}