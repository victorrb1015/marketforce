function new_cl(){
  document.getElementById('cl_mode').value = 'New';
  document.getElementById('cid').value = '';
  document.getElementById('cid').disabled = false;
  document.getElementById('credit_amount').value = '';
  document.getElementById('cl_notes').value = '';
}


function save_cl(){
  var cid = document.getElementById('cid').value;
  if(cid === ''){
    alert('Please Select A Customer!');
    return;
  }
  cid = urlEncode(cid);
  var camount = document.getElementById('credit_amount').value;
  if(camount === ''){
    alert('Please Enter The Credit Amount!');
    return;
  }
  camount = urlEncode(camount);
  var cl_notes = document.getElementById('cl_notes').value;
  cl_notes = urlEncode(cl_notes);
  
  
  var mode = document.getElementById('cl_mode').value;
  var clid = document.getElementById('cl_id').value;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'orders.php?tab=financing';
      
    }
  }
  xmlhttp.open("GET","orders/php/add-edit-credit-line.php?cid="+cid+"&camount="+camount+"&cl_notes="+cl_notes+"&mode="+mode+"&clid="+clid,true);
  xmlhttp.send();
}


function edit_cl(id){
  document.getElementById('cl_mode').value = 'Edit';
  document.getElementById('cl_id').value = id;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var q = JSON.parse(this.responseText);
      document.getElementById('cid').value = q.cid;
      document.getElementById('cid').disabled = true;
      document.getElementById('credit_amount').value = q.camount;
      document.getElementById('cl_notes').value = q.cl_notes;
      
    }
  }
  xmlhttp.open("GET","orders/php/get-credit-line-details.php?clid="+id,true);
  xmlhttp.send();
}


function remove_cl(clid){
  var conf = confirm("Are you sure you want to deactivate this customer's credit line?");
  if(conf === false || conf === '' || conf === null){
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location = 'orders.php?tab=financing';
      
    }
  }
  xmlhttp.open("GET","orders/php/delete-credit-line.php?clid="+clid,true);
  xmlhttp.send();
}