function export_inv(){
  var conf = confirm('Are you sure you want to export all pending invoices? Click "OK" to continue. Click "Cancel" to cancel the export.');
  if(conf === false){
    return;
  }else{
    window.open("orders/export/qb-export_v2.php?rep_id="+rep_id+"&rep_name="+rep_name, "_blank");
  }
}