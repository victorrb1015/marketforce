    <div class="modal fade" role="dialog" tabindex="-1" id="specialPricing">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="modal-title text-center">Add/Edit Special Pricing</h3>
                </div>
                <form id="sp_form" action="orders/php/add-edit-special-pricing.php" method="post">
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                  <th style="">Current Products</th>
                                  <th style="">Barcode</th>
                                  <th style="">Cost</th>
                                  <th style="">Norm. Price</th>
                                  <th style="">Spec. Price</th>
                                  <th>Lockout?</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                              $plq = '';
                              if($_SESSION['org_id'] == '832122'){
                                  $plq = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND m_lock = False ORDER BY item_name, barcode";
                              }else{
                                  $plq = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' ORDER BY item_name, barcode";
                              }
                              $plg = mysqli_query($conn, $plq) or die($conn->error);
                              while($plr = mysqli_fetch_array($plg)){
                                echo '<tr>
                                    <td style="color: rgb(48,115,173);"><strong>' . $plr['item_name'] . '</strong></td>
                                    <td style="color: rgb(48,115,173);"><strong>' . $plr['barcode'] . '</strong></td>
                                    <td style="color: rgb(255,255,255);">' . $plr['item_cost'] . '</td>
                                    <td style="color: rgb(255,255,255);">' . $plr['item_price'] . '</td>
                                    <td><input class="form-control" type="text" name="' . $plr['ID'] . '_sprice" id="' . $plr['ID'] . '_sprice" style="color: rgb(255,255,255);"></td>
                                    <td><input type="checkbox" class="form-control" style="height:20px;" id="' . $plr['ID'] . '_lockout" name="' . $plr['ID'] . '_lockout" /></td>
                                    <td><button type="button" class="close" onclick="remove_special_price(' . $plr['ID'] . ');"><span aria-hidden="true" style="color:red !Important;">×</span></button>
                                </tr>';
                              }
                              ?>
                              <input type="hidden" id="sp_cname" name="sp_cname" value="" />
                              <input type="hidden" id="sp_cid" name="sp_cid" value="" />
                              <input type="hidden" id="sp_rep_name" name="sp_rep_name" value="<?php echo $_SESSION['full_name']; ?>" />
                              <input type="hidden" id="sp_rep_id" name="sp_rep_id" value="<?php echo $_SESSION['user_id']; ?>" />
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" type="submit">Save</button>
                </div>
                      </form>
            </div>
        </div>
    </div>