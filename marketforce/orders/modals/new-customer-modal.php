<div class="modal fade" role="dialog" tabindex="-1" id="newCustomerModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="modal-title text-center">Add/Edit Customer</h3>
                </div>
              <form action="orders/php/add-edit-customer.php" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p><strong>Company Name:</strong></p>
                            <input class="form-control" type="text" name="company_name" placeholder="Company Name" id="company_name">
                            <p>&nbsp;</p>
                            <p><strong>Account Owner:</strong></p>
                            <input class="form-control" type="text" name="customer_first_name" placeholder="First Name" id="customer_first_name">
                            <input class="form-control" type="text" name="customer_last_name" placeholder="Last Name" id="customer_last_name">
                            <p>&nbsp;</p>
                            <p><strong>Email:</strong></p><input class="form-control" type="email" name="customer_email" placeholder="Email" id="customer_email" style="width: 100%;">
                            <p>&nbsp;</p>
                            <p><strong>Phone Number:</strong></p><input class="form-control phone" type="tel" name="customer_phone" placeholder="Phone Number" id="customer_phone" style="width: 100%;">
                            <p>&nbsp;</p>
                            <a id="link_docs" target="_blank" class="btn btn-primary">Documents</a>
                          <input type="hidden" id="customer_mode" name="customer_mode" value="New" />
                          <input type="hidden" id="customer_id" name="customer_id" value="" />
                        </div>
                        <div class="col-md-6">
                          <p><strong>Address:</strong></p>
                            <input class="form-control" type="text" name="customer_address" placeholder="Address" id="customer_address" style="width: 100%;">
                            <br>
                            <input class="form-control" type="text" name="customer_city" placeholder="City" id="customer_city" style="width: 100%;">
                            <br>
                            <select class="form-control" name="customer_state" id="customer_state" style="width: 100%;">
                              <option value="">State</option>
                              <?php
                              $sq = "SELECT * FROM `states` ORDER BY `state` ASC";
                              $sg = mysqli_query($conn, $sq) or die($conn->error);
                              while($sr = mysqli_fetch_array($sg)){
                                echo '<option value="' . $sr['state'] . '">' . $sr['state'] . '</option>';
                              }
                              ?>
                            </select>
                            <br>
                            <input class="form-control" name="customer_zip" placeholder="Zip Code" id="customer_zip" style="width: 100%;">
                            <p>&nbsp;</p>
                            <p><strong>Authorized Users:</strong></p>
                            <textarea class="form-control" id="authUsers" name="authUsers"></textarea>
                            <p>&nbsp;</p>
                            <p><strong>Tax Exemption Form:</strong></p>
                            <div id="taxForm_div">
                              <input type="file" class="form-control" id="taxForm" name="taxForm" />
                            </div>
                            <p>&nbsp;</p>
                         </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-md-12">
                          <p><strong>Notes: <span style="color:red;">(Not seen by customer)</span></strong></p><textarea class="form-control" name="customer_notes" placeholder="Enter Notes Here..." id="customer_notes" style="width: 100%;height: 197px;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <!--<button class="btn btn-primary" type="button" onclick="save_customer();">Save</button>-->
                  <button class="btn btn-primary" type="submit">Save</button>
                </div>
              </form>
            </div>
        </div>
    </div>