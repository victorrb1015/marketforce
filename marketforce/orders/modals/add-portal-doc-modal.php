<div class="modal fade" role="dialog" tabindex="-1" id="newPortalDocModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="text-center modal-title">Add Portal Document</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body" style="text-align:center;">
        <form action="orders/php/upload-doc.php" method="post" enctype="multipart/form-data">
          <input type="text" id="doc_name" name="doc_name" class="form-control" placeholder="Document Name" />
          <br><br>
          <textarea id="doc_desc" name="doc_desc" class="form-control" placeholder="Document Description" style="height:"></textarea>
          <input type="file" id="portalDocFile" name="portalDocFile" style="visibility:hidden;" onchange="this.form.submit();" />
        </form>
        <button type="button" class="btn btn-success btn-sm" onclick="upload_portalDoc();">
          <i class="fa fa-upload"> Choose Document To Upload</i> 
        </button>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>