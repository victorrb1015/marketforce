<div class="modal fade" role="dialog" tabindex="-1" id="colorsModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h3 class="text-center modal-title"><i class="fa fa-list-ul"></i> Manage Colors</h3>
      </div>
      <div class="modal-body">
        <div class="container" style="width:100%;">
          <div class="row">
           
     <?php
      $lq = "SELECT * FROM `order_color_lists` WHERE `inactive` != 'Yes'";
      $lg = mysqli_query($conn, $lq) or die($conn->error);
      while($lr = mysqli_fetch_array($lg)){
       echo '<div class="col-12">
              <h3 class="text-center">' . $lr['list_name'] . ' Color List</h3>
              <button type="button" class="btn btn-success btn-sm" style="float:right;" onclick="add_color(\'' . $lr['list'] . '\');"><i class="fa fa-plus"></i> Add</button>
              <div style="overflow: hidden; padding-right: .1em;padding-bottom:.5em;">
                 <input type="text" id="' . $lr['list'] . '_color" placeholder="Enter New Color Here..." class="form-control" style="width:100%;" />
              </div>
              <table id="' . $lr['list'] . '_table" class="table table-bordered table-hover table-striped">
                <thead class="bg-info">
                  <tr>
                    <th>Colors &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><span id="' . $lr['list'] . '_response" style="float:right;"></span></small></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="' . $lr['list'] . '_tableBody">';
         
         $lnq = "SELECT * FROM `order_product_colors` WHERE `list` = '" . $lr['list'] . "' AND `inactive` != 'Yes' ORDER BY `color_name` ASC";
         $lng = mysqli_query($conn, $lnq) or die($conn->error);
         while($lnr = mysqli_fetch_array($lng)){
            echo '<tr id="' . $lr['list'] . '_color_' . $lnr['ID'] . '">
                    <td>' . $lnr['color_name'] . '</td>
                    <td style="width:20px;"><button type="button" class="btn btn-danger btn-xs" onclick="remove_color(\'' . $lr['list'] . '\',' . $lnr['ID'] . ');"><i class="fa fa-times"></i></button></td>
                  </tr>';
         }
         
          echo '</tbody>
              </table>
            </div>
            
            <hr>';
      }
      ?> 
            
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>