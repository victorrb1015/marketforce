<div class="modal fade" role="dialog" tabindex="-1" id="addNoteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                  <h3 class="text-center modal-title">Add Note</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body"><textarea id="add_note" style="width: 100%;height: 100px;" name="add_note"></textarea></div>
                <input type="hidden" id="noteID" name="noteID" />
                <div class="modal-footer">
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" type="button" onclick="submit_note();">Add Note</button>
              </div>
            </div>
        </div>
    </div>