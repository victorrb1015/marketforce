<div class="modal fade" role="dialog" tabindex="-1" id="newOrderModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">New Order</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                  <form id="newOrderForm" action="orders/php/add-order.php" method="post" enctype="multipart/form-data">
                    <div class="col-md-12" style="margin-bottom:20px;">
                        <p class="">Customer Name:</p>
                        <input type="text" class="form-control" id="cname" style="width: 100%;" name="cname" required></div>
                    <div class="col-md-6">
                        <p class="" style="">Invoice Number:</p>
                        <input type="text" class="form-control" id="invoice" style="width: 100%;" name="invoice">
                      <small>(Not Required Until Invoiced)</small>
                    </div>
                    <div class="col-md-6">
                        <p class="">Due Date:</p>
                        <input type="text" class="form-control date" id="due_date" style="width: 100%;" name="due_date" autocomplete="off" required></div>
                    <div class="col-md-12" style="margin-top:20px;">
                        <p class="">Order Notes:</p>
                        <textarea id="notes" class="form-control" style="width: 100%;" name="notes"></textarea></div>
                  
                    <div class="col-md-12" style="margin-top:20px;">
                      <select class="form-control" id="priority" name="priority" required>
                        <option value="">Set Priority</option>
                        <option value="Low">Low</option>
                        <option value="Medium">Medium</option>
                        <option value="High">High</option>
                      </select>
                    </div>
                    
                    <div class="col-md-12" style="margin-top:20px;" id="fileDiv">
                      <h4>Work Order Attachment</h4>
                      <input type="file" id="fileBox" name="fileBox" class="form-control"/>
                      <small>(Supports: JPG, PNG, PDF, Excel)</small>
                    </div>
                    
                    <div class="col-md-12" style="margin-top:20px;" id="inv_fileDiv">
                      <h4>Invoice Attachment</h4>
                      <input type="file" id="invfileBox" name="invfileBox" class="form-control"/>
                      <small>(Supports: JPG, PNG, PDF, Excel)</small>
                    </div>
                    
                    <div class="col-md-12" style="margin-top:20px;" id="bol_fileDiv">
                      <h4>Bill of Lading Attachment</h4>
                      <input type="file" id="bolfileBox" name="bolfileBox" class="form-control"/>
                      <small>(Supports: JPG, PNG, PDF, Excel)</small>
                    </div>
                    
                  <input type="hidden" name="rep_id" id="rep_id" value="<?php echo $_SESSION['user_id']; ?>" />
                  <input type="hidden" name="rep_name" id="rep_name" value="<?php echo $_SESSION['full_name']; ?>" />
                  <input type="hidden" name="mode" id="mode" value="New" />
                  <input type="hidden" name="oid" id="oid" value="" />
                </div>
                <div class="modal-footer">
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" id="newSubBTN" type="submit">Add</button>
                  </form>
              </div>
            </div>
        </div>
    </div>