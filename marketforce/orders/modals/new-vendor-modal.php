<div class="modal fade" role="dialog" tabindex="-1" id="newVendorModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="modal-title text-center">Add/Edit Vendor</h3>
                </div>
              <form id="vendor_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p><strong>Vendor Name:</strong></p>
                            <input class="form-control" type="text" name="vendor_name" placeholder="Vendor Name" id="vendor_name" required>
                            <p>&nbsp;</p>
                            <p><strong>Account Contact:</strong></p>
                            <input class="form-control" type="text" name="contact_name" placeholder="Contact Name" id="contact_name" required>
                            <p>&nbsp;</p>
                            <p><strong>Email:</strong></p><input class="form-control" type="email" name="vendor_email" placeholder="Email" id="vendor_email" style="width: 100%;" required>
                            <p>&nbsp;</p>
                            <p><strong>Phone Number:</strong></p><input class="form-control phone" type="tel" name="vendor_phone" placeholder="Phone Number" id="vendor_phone" style="width: 100%;" required>
                            <p>&nbsp;</p>
                          <input type="hidden" id="vendor_mode" name="vendor_mode" value="New" />
                          <input type="hidden" id="vendor_id" name="vendor_id" value="" />
                        </div>
                        <div class="col-md-6">
                          <!--<p><strong>Address:</strong></p>
                            <input class="form-control" type="text" name="customer_address" placeholder="Address" id="customer_address" style="width: 100%;">
                            <br>
                            <input class="form-control" type="text" name="customer_city" placeholder="City" id="customer_city" style="width: 100%;">
                            <br>
                            <select class="form-control" name="customer_state" id="customer_state" style="width: 100%;">
                              <option value="">State</option>
                              <?php
                              $sq = "SELECT * FROM `states` ORDER BY `state` ASC";
                              $sg = mysqli_query($conn, $sq) or die($conn->error);
                              while($sr = mysqli_fetch_array($sg)){
                                echo '<option value="' . $sr['state'] . '">' . $sr['state'] . '</option>';
                              }
                              ?>
                            </select>
                            <br>
                            <input class="form-control" name="customer_zip" placeholder="Zip Code" id="customer_zip" style="width: 100%;">
                            <p>&nbsp;</p>
                            <p><strong>Authorized Users:</strong></p>
                            <textarea class="form-control" id="authUsers" name="authUsers"></textarea>
                            <p>&nbsp;</p>
                            <p><strong>Tax Exemption Form:</strong></p>
                            <div id="taxForm_div">
                              <input type="file" class="form-control" id="taxForm" name="taxForm" />
                            </div>
                            <p>&nbsp;</p>-->
                         </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-md-12">
                          <p><strong>Notes: <span style="color:red;">(Not seen by vendor)</span></strong></p><textarea class="form-control" maxlength="1000" name="vendor_notes" placeholder="Enter Notes Here..." id="vendor_notes" style="width: 100%;height: 197px;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" type="button" onclick="save_vendor();">Save</button>
                </div>
              </form>
            </div>
        </div>
    </div>