			 <!-- Modal -->
  <div class="modal fade" id="newPortalUserModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Portal User Form</h4>
        </div>
        <div class="modal-body">
          
          <table class="access_table">
            <tr>
              <td style="padding-right:10px;"><h4>First Name</h4></td>
              <td><input type="text" class="form-control" id="pu_fname" name="pu_fname" placeholder="First Name" /></td>
            </tr>
            <tr>
              <td style="padding-right:10px;"><h4>Last Name</h4></td>
              <td><input type="text" class="form-control" id="pu_lname" name="pu_lname" placeholder="Last Name" /></td>
            </tr>
            <tr>
              <td style="padding-right:10px;"><h4>Email</h4></td>
              <td><input type="text" class="form-control" id="pu_email" name="pu_email" placeholder="Email" /></td>
            </tr>
            <tr>
              <td style="padding-right:10px;"><h4>Company</h4></td>
              <td>
                <select class="form-control" id="pu_company" name="pu_company">
                  <option value="">Select Company</option>
                  <?php
                  $pucq = "SELECT * FROM `order_customers` WHERE `inactive` != 'Yes'";
                  $pucg = mysqli_query($conn, $pucq) or die($conn->error);
                  while($pucr = mysqli_fetch_array($pucg)){
                    echo '<option value="' . $pucr['ID'] . '">' . $pucr['company_name'] . '</option>';
                  }
                  ?>
                </select>
              </td>
            </tr>
            <tr>
              <td style="padding-right:10px;"><h4>User Type:</h4></td>
              <td>
                <select class="form-control" id="pu_affiliate" name="pu_affiliate">
                  <option value="">Select One</option>
                  <option value="Yes">Affiliate</option>
                  <option value="No">Regular</option>
                </select>
              </td>
            </tr>
          </table>
          <input type="hidden" id="pu_mode" name="pu_mode"/>
          <input type="hidden" id="pu_uid" name="pu_uid"/>
          <p id="ep" style="text-align:center;color:red;font-weight:bold;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success" id="save_portal_user_btn" onclick="save_portal_user();">Save</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->