<?php
include '../../php/connection.php';

//Load Variables...
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];

//Set Variables...
$csvContent = '';

//Set CSV Header...
$header = 'Invoice Number,Date,Customer Name,Invoice Amount';
$csvContent .= $header . "\r\n";

//Get Details...
$q = "SELECT * FROM `order_invoices` WHERE `inactive` != 'Yes' AND `exported` != 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){

  $freight = $r['freight'];

  
//Check Order Status...
  $oq = "SELECT * FROM `orders` WHERE `inv` = '" . $r['inv_num'] . "'";
  $og = mysqli_query($conn, $oq) or die($conn->error);
  $or = mysqli_fetch_array($og);
  if($or['invoiced'] != 'Yes'){
    continue;
  }else{
    //Set Invoices as EXPORTED...
  $uq = "UPDATE `order_invoices` SET 
          `exported` = 'Yes',
          `export_date` = CURRENT_DATE,
          `export_time` = CURRENT_TIME,
          `export_rep_id` = '" . $rep_id . "',
          `export_rep_name` = '" . $rep_name . "'
         WHERE `ID` = '" . $or['ID'] . "'";
  mysqli_query($conn, $uq) or die($conn->error);
  
  $uuq = "UPDATE `order_invoice_items` SET 
          `exported` = 'Yes',
          `export_date` = CURRENT_DATE,
          `export_time` = CURRENT_TIME,
          `export_rep_id` = '" . $rep_id . "',
          `export_rep_name` = '" . $rep_name . "'
          WHERE `inactive` != 'Yes' AND `inv_num` = '" . $or['inv'] . "'";
  mysqli_query($conn, $uuq) or die($conn->error);
    
    
   //Get Customer Info...
$cq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $r['cid'] . "'";
$cg = mysqli_query($conn, $cq) or die($conn->error);
$cr = mysqli_fetch_array($cg);


//Get Invoice Items
$iq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $r['inv_num'] . "'";
$ig = mysqli_query($conn, $iq) or die($conn->error);
  $invAmount = 0;
while($ir = mysqli_fetch_array($ig)){
  
  //Calc Invoice Total...
  $invAmount = $invAmount + $ir['item_price'];
  
}
  
  $invTotal = $invAmount + $freight;
  $invN = 'MF-' . $r['inv_num'];
  $invD = $r['date'];
  $invC = $cr['company_name'];
  $cRow = $invN . ',' . $invD . ',' . $invC . ',' . $invTotal;
  $csvContent .= $cRow . "\r\n";
}
  
  
  
  }




//$response = json_encode($x);

//echo $csvContent;
$timestamp = date("mdYHis");
//$time = date("His");

$fname = 'export-' . $timestamp . '.csv';
$fp = fopen($fname,'wb');
fwrite($fp,$csvContent);
fclose($fp);

//header('Content-Type: text/csv; charset=utf-8');
//header('Content-Disposition: attachment; filename='.$fname);

header('Content-type: application/csv');
header("Content-Disposition: inline; filename=".$fname);
readfile($fname);





?>
