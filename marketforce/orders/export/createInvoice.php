<?php
include '../../php/connection.php';
if(!isset($_SESSION['org_id'])){
  echo 'Login Required...';
  echo '<script>
          window.location = "' . $_SERVER['HTTP_HOST'] . '";
        </script>';
}
error_reporting(E_ALL);
require "vendor/autoload.php";
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Invoice;

//print_r($_SESSION['sessionAccessToken']);
//echo '<br><br>';
//echo $_SESSION['sessionAccessToken'];

function makeAPICall()
{

    // Create SDK instance
    $config = include('config.php');
    $dataService = DataService::Configure(array(
        'auth_mode' => 'oauth2',
        'ClientID' => $config['client_id'],
        'ClientSecret' =>  $config['client_secret'],
        'RedirectURI' => $config['oauth_redirect_uri'],
        'scope' => $config['oauth_scope'],
        'baseUrl' => "development"
    ));

    /*
     * Retrieve the accessToken value from session variable
     */
    $accessToken = $_SESSION['sessionAccessToken'];
    var_dump($accessToken);
    /*
     * Update the OAuth2Token of the dataService object
     */
    $dataService->updateOAuth2Token($accessToken);
    //$companyInfo = $dataService->getCompanyInfo();

    //print_r($companyInfo);
    //return $companyInfo;
}

$result = makeAPICall();

?>
