<?php
error_reporting(1);
error_reporting(E_ALL);
/*
* This Exporting script is set to export the invoice as a single line item with the TOTAL
* invoice amount as the total. There are no invoice details exported in this script.
*
*/
include '../../php/connection.php';

//Load Variables...
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];
$list = array();


//Set Variables...
$csvContent = '';

//Set CSV Header...
$header = 'Invoice Number,Date,Customer Name,Item Code,Description,Quantity,Price Each,Amount';
$csvContent .= $header . "\r\n";

//Get Details...
$q = "SELECT * FROM `order_invoices` WHERE `inactive` != 'Yes' AND `exported` != 'Yes'";

$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){

  $freight = $r['freight'];
  
  
  //Check Order Status...
  $oq = "SELECT * FROM `orders` WHERE `inv` = '" . $r['inv_num'] . "'";
  $og = mysqli_query($conn, $oq) or die($conn->error);
  $or = mysqli_fetch_array($og);
  
  if(strlen($or['invoiced']) != 0){
    //echo "truena";
    continue;
  }else{
    //Set Invoices as EXPORTED...
    $uq = "UPDATE `order_invoices` SET 
            `exported` = 'Yes',
            `export_date` = CURRENT_DATE,
            `export_time` = CURRENT_TIME,
            `export_rep_id` = '" . $rep_id . "',
            `export_rep_name` = '" . $rep_name . "'
          WHERE `inv_num` = '" . $or['inv'] . "'";
    //mysqli_query($conn, $uq) or die($conn->error);
    
    $uuq = "UPDATE `order_invoice_items` SET 
            `exported` = 'Yes',
            `export_date` = CURRENT_DATE,
            `export_time` = CURRENT_TIME,
            `export_rep_id` = '" . $rep_id . "',
            `export_rep_name` = '" . $rep_name . "'
            WHERE `inactive` != 'Yes' AND `inv_num` = '" . $or['inv'] . "'";
    //mysqli_query($conn, $uuq) or die($conn->error);
      
      
    //Get Customer Info...
    $cq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $r['cid'] . "'";
    $cg = mysqli_query($conn, $cq) or die($conn->error);
    $cr = mysqli_fetch_array($cg);


    //Get Invoice Items
    $iq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $r['inv_num'] . "'";
    $ig = mysqli_query($conn, $iq) or die($conn->error);
    $invAmount = 0;
    
    while($ir = mysqli_fetch_array($ig)){
      $record = array();
      //Set Record to Invoice Info...
      array_push($record,
        $r['inv_num'],
        date("m/d/Y",strtotime($r['date'])),
        str_replace(',',' ',$r['cname']),
        str_replace(',',' ',$ir['item_name']),
        str_replace(',',' ',$ir['item_desc']),
      );
      if($ir['item_length'] > 0){
        array_push($record, ($ir['item_qty'] * $ir['item_length']));
        
      }else{
        array_push($record, $ir['item_qty']);
      }

      array_push($record, $ir['item_rate']);
      array_push($record, $ir['item_price']);

      //SE AGREGA A LA LISTA GENERAL
      array_push($list, $record);

      
      
      //Add Record to CSV Data set...
      $csvContent .= $record . "\r\n";
      
      //Calc Invoice Total...
      $invAmount = $invAmount + $ir['item_price'];
      
    }
    

      
    /*$invTotal = $invAmount + $freight;
    $invN = 'MF-' . $r['inv_num'];
    $invD = $r['date'];
    $invC = $cr['company_name'];
    $cRow = $invN . ',' . $invD . ',' . $invC . ',' . $invTotal;
    $csvContent .= $cRow . "\r\n";*/
  }
  
  if($freight > 0){
    $record = array();
    //Set Record to Invoice Info...
    $record($record,
      $r['inv_num'],;
      date("m/d/Y",strtotime($r['date'])),
      str_replace(',',' ',$r['cname']),
      'Freight',
      'Cost of Shipping',
      '1',
      $freight,
      $freight
    );

    //SE AGREGA A LA LISTA GENERAL
    array_push($list, $record);
    
    //Add Record to CSV Data set...
    $csvContent .= $record . "\r\n";
  }
  
  
}


//$response = json_encode($x);

//echo $csvContent;
$timestamp = date("mdYHis");
//$time = date("His");

$fname = 'export-' . $timestamp . '-' . $_SESSION['org_id'] . '.csv';
$file_path = 'export_files/' . $fname;
$fp = fopen($file_path,'w');

foreach ($list as $fields) {
  fputcsv($fp, $fields);
}

fclose($fp);

//header('Content-Type: text/csv; charset=utf-8');
//header('Content-Disposition: attachment; filename='.$fname);

header('Content-type: application/csv');
header("Content-Disposition: inline; filename=".$fname);
readfile($file_path);


?>