<?php 
include '../../php/connection.php';
error_reporting(E_ALL);

require "vendor/autoload.php";
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Invoice;

//Load Variables...
$token = $_GET['code'];
$realmID = $_GET['realmId'];

if(isset($_SESSION['org_id'])){
  
  function processCode()
{
    // Create SDK instance
    $config = include('config.php');
    $dataService = DataService::Configure(array(
        'auth_mode' => 'oauth2',
        'ClientID' => $config['client_id'],
        'ClientSecret' =>  $config['client_secret'],
        'RedirectURI' => $config['oauth_redirect_uri'],
        'scope' => $config['oauth_scope'],
        'baseUrl' => "development"
    ));
    $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
    $parseUrl = parseAuthRedirectUrl($_SERVER['QUERY_STRING']);
    //print_r($parseUrl);
    /*
     * Update the OAuth2Token
     */
    $accessToken = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken($parseUrl['code'], $parseUrl['realmId']);
    //var_dump($accessToken);
    //echo '<br><br>';
    //echo $accessToken->accessTokenKey->{'accessTokenKey:QuickBooksOnline\API\Core\OAuth\OAuth2\OAuth2AccessToken:private'};
    //$dataService->updateOAuth2Token($accessToken);
    /*
     * Setting the accessToken for session variable
     */
    $_SESSION['sessionAccessToken'] = $accessToken;
    //echo $accessToken;
}
function parseAuthRedirectUrl($url)
{
    parse_str($url,$qsArray);
    return array(
        'code' => $qsArray['code'],
        'realmId' => $qsArray['realmId']
    );
}
$result = processCode();

  
//UPDATE DB...
$q = "UPDATE `organizations` SET `qb_token` = '" . $token . "', `qb_realmID` = '" . $realmID . "', `qb_last_updated` = CURRENT_DATE 
      WHERE 
      `org_id` = '" . $_SESSION['org_id'] . "'";
mysqli_query($mf_conn, $q) or die($mf_conn->error);

echo 'Database Updated...';
//echo '<br>';
//print_r($_GET);
echo '<script>
        //window.location = "../../orders.php";
      </script>';
}else{
echo 'Login Required...';
echo '<script>
        setTimeout(function(){
          window.location = "' . $_SERVER['HTTP_HOST'] . '";
          },2000);
      </script>';
}
?>