REQUEST URI FOR SEQUENCE ID 00002
==================================
https://sandbox-quickbooks.api.intuit.com/v3/company/193514611891254/invoice?minorversion=36

REQUEST HEADERS
================
Authorization: Bearer Q011560329594pSOtxqNhgnT4wffQkTKhNr3xkKW9vM36Tt0TZ
host: sandbox-quickbooks.api.intuit.com
user-agent: V3PHPSDK6.0.0
accept: application/xml
connection: close
content-type: application/xml
content-length: 648

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:Line>
    <ns0:Amount>100</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Services">1</ns0:ItemRef>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>1</ns0:CustomerRef>
  <ns0:BillEmail>
    <ns0:Address>Familiystore@intuit.com</ns0:Address>
  </ns0:BillEmail>
  <ns0:BillEmailCc>
    <ns0:Address>a@intuit.com</ns0:Address>
  </ns0:BillEmailCc>
  <ns0:BillEmailBcc>
    <ns0:Address>v@intuit.com</ns0:Address>
  </ns0:BillEmailBcc>
</ns0:Invoice>


