<?php
ini_set('session.save_path','/home/marketforce/mf_temp');
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$db_db = $_SESSION['org_db_name'];
$mysqli = new mysqli('localhost', 'marketfo_mf', '#NgTFJQ!z@t8', $db_db);
if (mysqli_connect_errno()) {
    printf("Falló la conexión failed: %s\n", $mysqli->connect_error);
    exit();
}
//Load Variables...
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];
$excel = [
    ['Invoice Number', 'Date','Customer Name','Item Code', 'Description','Quantity','Price Each','Amount'],
];
$report = array();
$sel = "SELECT * FROM `order_invoices` WHERE `inactive` != 'Yes' AND (`exported` != 'Yes' OR `exported` IS NULL)";
$g = $mysqli->query($sel);
while ($r = $g->fetch_array()) {
    $freight = $r['freight'];
    $oq = "SELECT * FROM `orders` WHERE `inv` = '" . $r['inv_num'] . "'";
    
    $x = $mysqli->query($oq);
    $row = $x->fetch_array();
    
    if ('Yes' == $row['invoiced']){
        
        $uq = "UPDATE `order_invoices` SET 
          `exported` = 'Yes',
          `export_date` = CURRENT_DATE,
          `export_time` = CURRENT_TIME,
          `export_rep_id` = '" . $rep_id . "',
          `export_rep_name` = '" . $rep_name . "'
         WHERE `inv_num` = '" . $row['inv'] . "'";
        $mysqli->query($uq);
        $uuq = "UPDATE `order_invoice_items` SET 
          `exported` = 'Yes',
          `export_date` = CURRENT_DATE,
          `export_time` = CURRENT_TIME,
          `export_rep_id` = '" . $rep_id . "',
          `export_rep_name` = '" . $rep_name . "'
          WHERE `inactive` != 'Yes' AND `inv_num` = '" . $row['inv'] . "'";
        $mysqli->query($uuq);
        //Get Customer Info...
        $cq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $r['cid'] . "'";
        $y = $mysqli->query($oq);
        $costumerRow = $y->fetch_array();
        //Get Invoice Items
        $iq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $r['inv_num'] . "'";
        
        $z = $mysqli->query($iq);
        $invAmount = 0;
        while($ir = $z->fetch_array()){
            
            if ($ir['item_length'] > 0) {
                $record = ($ir['item_qty'] * $ir['item_length']);
            } else {
                $record = $ir['item_qty'];
            }
            $excel[] = [
                $r['inv_num'],
                date("m/d/Y", strtotime($r['date'])),
                str_replace(',', ' ', $r['cname']),
                str_replace(',', ' ', $ir['item_name']),
                str_replace(',', ' ', $ir['item_desc']),
                $record,
                $ir['item_rate'],
                $ir['item_price'],
            ];
            $invAmount += $ir['item_price'];
        }
    }
    if ($freight > 0){
        $excel[] = [
            $r['inv_num'],
            date("m/d/Y", strtotime($r['date'])),
            str_replace(',', ' ', $r['cname']),
            'Freight',
            "Cost of Shipping",
            1,
            $freight,
            $freight,
        ];
    }
}
//echo $csvContent;
$timestamp = date("mdYHis");
//$time = date("His");
$fname = 'export-' . $timestamp . '-' . $_SESSION['org_id'] . '.csv';
$file_path = 'export_files/' . $fname;

$fp = fopen($file_path, 'wb');
foreach ($excel as $fields) {
    fputcsv($fp, $fields);
}
fclose($fp);


header('Content-type: application/csv');
header("Content-Disposition: inline; filename=" . $fname);
readfile($file_path);
