<?php
include '../../php/connection.php';

//Load Variables...
$mode = $_GET['mode'];
$uid = $_GET['uid'];
$fname = $_GET['fname'];
$lname = $_GET['lname'];
$email = $_GET['email'];
$company = $_GET['company'];
$affiliate = $_GET['affiliate'];

$username = substr($fname,0,1) . $lname;
$password = uniqid();

$x->response = 'ERROR';
$x->message = 'Error: Mode not recognized. User not saved.';

if($mode == 'New'){
  $q = "INSERT INTO `users`
        (
        `date`,
        `time`,
        `fname`,
        `lname`,
        `email`,
        `user`,
        `pass`,
        `cid`,
        `org_id`,
        `affiliate`,
        `inactive`
        )
        VALUES
        (
        CURRENT_DATE,
        NOW() + 1,
        '" . $fname . "',
        '" . $lname . "',
        '" . $email . "',
        '" . $username . "',
        '" . $password . "',
        '" . $company . "',
        '" . $_SESSION['org_id'] . "',
        '" . $affiliate . "',
        'No'
        )";
  mysqli_query($pconn, $q) or die($pconn->error);
  $x->response = 'GOOD';
  $x->message = 'User was added successfully!';
  
  //Send Email to User that was added...
  include '../../php/phpmailer/PHPMailerAutoload.php';
  $mail = new PHPMailer;
  include '../../php/phpmailsettings.php';
  $mail->CharSet = "UTF-8";
  $mail->setFrom('no-reply@marketforceapp.com','MarketForce');
  $mail->addAddress($email);
  $mail->addBCC('archive@ignition-innovations.com');
  $mail->Subject = 'Customer Portal Account';
  include '../email/new-portal-account-email.php';
  $mail->Body = $npaTemp;
  
  if($mail->send()){
    $x->email_sent = 'Success';
  }else{
    $x->email_sent = 'Error: ' . $mail->ErrorInfo;
  }
  
}

if($mode == 'Edit'){
   $q = "UPDATE `users` SET
          `fname` = '" . $fname . "',
          `lname` = '" . $lname . "',
          `email` = '" . $email . "',
          `cid` = '" . $company . "'
          WHERE `ID` = '" . $uid . "'";
  mysqli_query($pconn, $q) or die($pconn->error);
  $x->response = 'GOOD';
  $x->message = 'Changes were saved successfully!';
}

$response = json_encode($x);
echo $response;

?>