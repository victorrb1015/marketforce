<?php
include '../../php/connection.php';
//error_reporting(E_ALL);

$cid = $_REQUEST['cid'];

$x->response = 'GOOD';
$x->products = array();
//Get Details...

if("832122" == $_SESSION['org_id']){
    $q = "SELECT * FROM `inventory_items` WHERE item_type='Product' AND  m_lock = False ORDER BY `item_name` ASC ";
}else{
    $q = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' ORDER BY `item_name` ASC ";
}
$g = mysqli_query($conn, $q) or die($conn->error);
$i = 0;
while($r = mysqli_fetch_array($g)){
  
  //Check if locked out...
  $lq = "SELECT * FROM `order_special_pricing` WHERE `inactive` != 'Yes' AND `cid` = '" . $cid . "' AND `pid` = '" . $r['ID'] . "' AND `lockout` = 'Yes'";
  $lg = mysqli_query($conn, $lq) or die($conn->error);
  if(mysqli_num_rows($lg) <= 0 || $cid == '' || $cid == 'undefined'){
    $x->products[$i]->ID = $r['ID'];
    $x->products[$i]->zone = $r['zone'];
    $x->products[$i]->item_type = $r['item_type'];
    $x->products[$i]->barcode = $r['barcode'];
    $x->products[$i]->pname = $r['item_name'];
    $x->products[$i]->pinfo = $r['item_info'];
    $x->products[$i]->punits = $r['item_units'];
    $x->products[$i]->pcost = $r['item_cost'];
    $x->products[$i]->pprice = $r['item_price'];
    $x->products[$i]->pweight = $r['item_weight'];
    $x->products[$i]->length_required = $r['length_required'];
    $x->products[$i]->color_required = $r['color_required'];
    $x->products[$i]->color_list = $r['color_list'];
    $x->products[$i]->inactive = $r['inactive'];
    $i++;
  }
}

$response = json_encode($x);

echo $response;

?>