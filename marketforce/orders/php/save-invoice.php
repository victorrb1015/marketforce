<?php
include '../../php/connection.php';
//var_dump($_POST);

//Load Variables...
$jsondata = array();
$mode = $_POST['mode'];
$rep_id = $_POST['rep_id'];
$rep_name = $_POST['rep_name'];
$org_id = $_POST['org_id'];
$cid = $_POST['cid'];
$invNum = mysqli_real_escape_string($conn, $_POST['invNum']);
$invDate = $_POST['invDate'];
$freight = $_POST['freight'];
$taxRate = mysqli_real_escape_string($conn, $_POST['taxRate']);
$notes = mysqli_real_escape_string($conn, $_POST['notes']);
$items = json_decode($_POST['items'], true);
$sa = $_POST['sa'];
$customer_mode = $_POST['customer_mode'];
$affiliate_mode = $_POST['affiliate_mode'];
if ($affiliate_mode == 'true') {
  $affiliate_mode = 'Yes';
} else {
  $affiliate_mode = 'No';
}
$cp_mode = $_POST['cpMode'];
if ($cp_mode == 'Quote') {
  $cpMode = 'Quote';
} else {
  $cpMode = 'Estimate';
}

//Get Customer Name...
$cnq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $cid . "'";
$cng = mysqli_query($conn, $cnq) or die($conn->error);
$cnr = mysqli_fetch_array($cng);
$cname = $cnr['customer_first_name'] . ' ' . $cnr['customer_last_name'];
$company_name = $cnr['company_name'];

//Check if Invoice Number Exists...
$ciq = "SELECT * FROM `order_invoices` WHERE `inv_num` = '" . $invNum . "'";
$cig = mysqli_query($conn, $ciq) or die($conn->error);
$cinr = mysqli_num_rows($cig);
$cir = mysqli_fetch_array($cig);
$istatus = $cir['status'];
//If Invoice Exists...
if ($cinr > 0 && $mode != 'CNew' && $mode != 'ANew' ) {
  //Make the Quote an Estimate
  if (($mode == 'CEdit' || $mode == 'AEdit') && $cpMode != 'Quote') {
    $ouq = "UPDATE `orders` SET `status` = '" . $cpMode . "' WHERE `inv` = '" . $invNum . "'";
    mysqli_query($conn, $ouq) or die($conn->error);
  }

  //Update Invoice Info...
  $uq = "UPDATE `order_invoices` SET 
          `cname` = '" . mysqli_real_escape_string($conn, $company_name) . "',
          `ship_address` = '" . $sa . "',
          `freight` = '" . $freight . "',
          `tax_rate` = '" . $taxRate . "',
          `notes` = '" . $notes . "',
          `last_edit` = CURRENT_DATE,
          `last_edit_time` = CURRENT_TIME,
          `last_edit_by` = '" . $rep_name . "'
          WHERE `inv_num` = '" . $invNum . "'";
  mysqli_query($conn, $uq) or die($conn->error);

  //Make current invoice Items inactive...
  $dq = "UPDATE `order_invoice_items` SET `inactive` = 'Yes' WHERE `inv_num` = '" . $invNum . "'";
  mysqli_query($conn, $dq) or die($conn->error);

  //Update Invoice Items...
  for ($i = 0, $iMax = count($items); $i < $iMax; $i++) {
    $pid = $items[$i]["itemID"];
    $item_name = mysqli_real_escape_string($conn, $items[$i]["name"]);
    $item_desc = mysqli_real_escape_string($conn, $items[$i]["desc"]);
    $item_color = mysqli_real_escape_string($conn, $items[$i]["color"]);
    $item_length = $items[$i]["length"];
    $item_rate = $items[$i]["rate"];
    $item_arate = $items[$i]["arate"];
    $item_qty = $items[$i]["qty"];
    $item_price = $items[$i]["price"];

    //Get Item Weight...
    //$wq = "SELECT `product_weight` AS `weight` FROM `order_products` WHERE `ID` = '" . $pid . "'";
    $wq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $pid . "'";
    $wg = mysqli_query($conn, $wq) or die($conn->error);
    $wr = mysqli_fetch_array($wg);
    $weight = $wr['item_weight'];

    //Add new invoice items...
    $iiq = "INSERT INTO `order_invoice_items` 
          (
          `date`,
          `time`,
          `rep_id`,
          `rep_name`,
          `inv_num`,
          `pid`,
          `item_name`,
          `item_desc`,
          `item_color`,
          `item_length`,
          `item_rate`,
          `item_arate`,
          `item_qty`,
          `item_price`,
          `item_weight`,
          `inactive`
          )
          VALUES
          (
          CURRENT_DATE,
          CURRENT_TIME,
          '" . $rep_id . "',
          '" . $rep_name . "',
          '" . $invNum . "',
          '" . $pid . "',
          '" . $item_name . "',
          '" . $item_desc . "',
          '" . $item_color . "',
          '" . $item_length . "',
          '" . $item_rate . "',
          '" . $item_arate . "',
          '" . $item_qty . "',
          '" . $item_price . "',
          '" . $weight . "',
          'No'
          )";
    mysqli_query($conn, $iiq) or die($conn->error);
  }
  if ($istatus == 'Estimate') {
    $rtype = 'Estimate';
    $raction = 'Saved';
  } else {
    $rtype = 'Invoice';
    $raction = 'Saved';
  }
  if (($mode == 'CEdit' || $mode == 'AEdit') && $cpMode != 'Quote') {
    $rtype = 'Order';
    $raction = 'Submitted';
  }
  echo 'Your ' . $rtype . ' Has Been ' . $raction . '!';
} else {
  //echo "entro 2";
  if ($cinr > 0) {
    $invNum++;
  }
  //Insert General Invoice Info...
  $iq = "INSERT INTO `order_invoices`
          (
          `est_date`,
          `est_time`,
          `date`,
          `time`,
          `rep_id`,
          `rep_name`,
          `cid`,
          `cname`,
          `affiliate_mode`,
          `inv_num`,
          `ship_address`,
          `freight`,
          `tax_rate`,
          `notes`,
          `last_edit`,
          `last_edit_by`,
          `status`,
          `inactive`
          )
          VALUES
          (
          CURRENT_DATE,
          CURRENT_TIME,
          CURRENT_DATE,
          CURRENT_TIME,
          '" . $rep_id . "',
          '" . $rep_name . "',
          '" . $cid . "',
          '" . mysqli_real_escape_string($conn, $company_name) . "',
          '" . $affiliate_mode . "',
          '" . $invNum . "',
          '" . $sa . "',
          '" . $freight . "',
          '" . $taxRate . "',
          '" . $notes . "',
          CURRENT_DATE,
          '" . $rep_name . "',
          '" . $cpMode . "',
          'No'
          )";
  mysqli_query($conn, $iq) or die($conn->error);

  //Insert Invoice Items...
  for ($i = 0; $i < count($items); $i++) {
    $pid = $items[$i]["itemID"];
    $item_name = $items[$i]["name"];
    $item_desc = $items[$i]["desc"];
    $item_color = $items[$i]["color"];
    $item_length = $items[$i]["length"];
    $item_rate = $items[$i]["rate"];
    $item_arate = $items[$i]["arate"];
    $item_qty = $items[$i]["qty"];
    $item_price = $items[$i]["price"];


    //Get Item Weight...
    $wq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $pid . "'";
    $wg = mysqli_query($conn, $wq) or die($conn->error);
    $wr = mysqli_fetch_array($wg);
    $weight = $wr['item_weight'];


    $iiq = "INSERT INTO `order_invoice_items` 
          (
          `date`,
          `time`,
          `rep_id`,
          `rep_name`,
          `inv_num`,
          `pid`,
          `item_name`,
          `item_desc`,
          `item_color`,
          `item_length`,
          `item_rate`,
          `item_arate`,
          `item_qty`,
          `item_price`,
          `item_weight`,
          `inactive`
          )
          VALUES
          (
          CURRENT_DATE,
          CURRENT_TIME,
          '" . $rep_id . "',
          '" . $rep_name . "',
          '" . $invNum . "',
          '" . $pid . "',
          '" . $item_name . "',
          '" . $item_desc . "',
          '" . $item_color . "',
          '" . $item_length . "',
          '" . $item_rate . "',
          '" . $item_arate . "',
          '" . $item_qty . "',
          '" . $item_price . "',
          '" . $weight . "',
          'No'
          )";
    mysqli_query($conn, $iiq) or die($conn->error);
  }


  //Add invoice to the order tracker DB...
  $aq = "INSERT INTO `orders` 
          (
          `date`,
          `time`,
          `rep_id`,
          `rep_name`,
          `cname`,
          `inv`,
          `file_url`,
          `inv_file_url`,
          `bol_file_url`,
          `status`,
          `inactive`
          )
          VALUES
          (
          CURRENT_DATE,
          CURRENT_TIME,
          '" . $rep_id . "',
          '" . $rep_name . "',
          '" . mysqli_real_escape_string($conn, $company_name) . "',
          '" . $invNum . "',
          'http://marketforceapp.com/marketforce/orders/invoice/work-order.php?org_id=" . $org_id . "&inv=" . $invNum . "',
          'http://marketforceapp.com/marketforce/orders/invoice/invoice.php?org_id=" . $org_id . "&mode=Print&inv=" . $invNum . "',
          'http://marketforceapp.com/marketforce/orders/invoice/bol.php?org_id=" . $org_id . "&inv=" . $invNum . "',
          '" . $cpMode . "',
          'No'
          )";
  mysqli_query($conn, $aq) or die($conn->error);

  //echo $aq;



  if ($customer_mode == 'true' && $cpMode != 'Quote') {

    //INSERT Task...
    $tq = "INSERT INTO `tasks`
      (
      `date`,
      `from_rep_id`,
      `from_rep_name`,
      `to_rep_id`,
      `to_rep_name`,
      `task`,
      `deadline`,
      `status`,
      `inactive`
      )
      VALUES
      (
      NOW() + INTERVAL 1 HOUR,
      '999',
      'MarketForce',
      '" . $_SESSION['org_orders_rep_id'] . "',
      '" . $_SESSION['org_orders_rep_name'] . "',
      'Confirm Portal Order From " . mysqli_real_escape_string($conn, $company_name) . ". Invoice Number: " . $invNum . "',
      '0000-00-00 00:00:00',
      'Pending',
      'No'
      )";
    mysqli_query($conn, $tq) or die($conn->error);

    //Send Email Notification to Employees...
  include '../../php/phpmailer/PHPMailerAutoload.php';
  $mail = new PHPMailer;
  include '../../php/phpmailsettings.php';
  $mail->CharSet = 'UTF-8';
  $mail->setFrom('no-reply@marketforceapp.com','MarketForce');
  //$mail->addAddress('michael@ignition-innovations.com');
  $mail->addAddress($_SESSION['org_orders_rep_email']);
  //$mail->addCC('cabrera@allsteelcarports.com');
  $mail->addBCC('archive@ignition-innovations.com');
  $mail->Subject = 'New Portal Order!';
  //Set Email Template Variables...
  $et_companyName = $company_name;
  $et_invNum = $invNum;
  $et_orgID = $_SESSION['op_org_id'];
  include '../email/new-portal-order-email.php';
  $mail->Body = $newPortalOrderTemp;
  
  if($mail->send()){
      $jsondata["message"] =  'Your Order Has Been Submitted!';
  }else{
      $jsondata["message"] =  'Error: ' . $mail->ErrorInfo;
  }
  } else {
    if ($cpMode == 'Quote') {
        $jsondata["message"] = 'Your Quote Has Been Saved!';
    } else {
        $jsondata["message"] =  'Your Estimate Has Been Saved!';
    }
  }
}


$jsondata["success"] = true;
header('Content-type: application/json; charset=utf-8');
echo json_encode($jsondata, JSON_FORCE_OBJECT);
exit();

/*for($i = 0; $i < count($items); $i++){
  echo $items[$i]["desc"];
  echo '<br>';
}*/
