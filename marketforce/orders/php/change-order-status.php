<?php
include '../../php/connection.php';

//Load Variables...
$oid = $_GET['oid'];
$mode = $_GET['mode'];
$rep_name = $_GET['rep_name'];
$rep_id = $_GET['rep_id'];
$shopNum = mysqli_real_escape_string($conn, $_GET['shopNum']);
$trackingNum = mysqli_real_escape_string($conn, $_GET['trackingNum']);
$invoiceNum = mysqli_real_escape_string($conn, $_GET['invoiceNum']);
$payDetail = mysqli_real_escape_string($conn, $_GET['payDetail']);
$bypass = $_GET['bypass'];
$dir = $_GET['dir'];

//Get Current Inv Num...
$inq = "SELECT * FROM `orders` WHERE `ID` = '" . $oid . "'";
$ing = mysqli_query($conn, $inq) or die($conn->error);
$inr = mysqli_fetch_array($ing);
$invNum = $inr['inv'];


switch ($mode) {
  case 'Estimate':
    $sq = "UPDATE `orders` SET `status` = 'Estimate' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'rec_order':
    $sq = "UPDATE `orders` SET `rec_order` = 'Yes', `rec_order_date` = CURRENT_DATE, `rec_order_time` = CURRENT_TIME, `rec_order_rep` = '" . $rep_id . "', `status` = 'rec_order' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    $uiq = "UPDATE `order_invoices` SET `date` = CURRENT_DATE, `time` = CURRENT_TIME, `status` = 'Pending' WHERE `inv_num` = '" . $invNum . "'";
    mysqli_query($conn, $uiq) or die($conn->error);
    //Get invoice item details...
    $iiq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $invNum . "'";
    $iig = mysqli_query($conn, $iiq) or die($conn->error);
    while($iir = mysqli_fetch_array($iig)){
      $pid = $iir['pid'];
      if($iir['item_length'] == 'N/A'){
        $qty = $iir['item_qty'];
      }else{
        $length = $iir['item_length'];
        $qty = ($iir['item_qty'] * $length);
      }
      //Get Item Zone...
      $izq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $pid . "'";
      $izg = mysqli_query($conn, $izq) or die($conn->error);
      $izr = mysqli_fetch_array($izg);
      $iZone = strtolower($izr['zone']);
      $cqty = $izr['qty'];
      $nqty = $cqty - $qty;
      //Update Inventory_Items DB inv level...
      $uilq = "UPDATE `inventory_items` SET `qty` = (`qty` - '" . $qty . "'), `qty_" . $iZone . "` = (`qty_" . $iZone . "` - '" . $qty . "') WHERE `ID` = '" . $pid . "'";
      mysqli_query($conn, $uilq) or die($conn->error);
      //Log Adjustment...
      $aiq = "INSERT INTO `inventory_adjustments` 
              (
              `date`,
              `time`,
              `pid`,
              `zone`,
              `current_qty`,
              `new_qty`,
              `adjustment`,
              `type`,
              `invoice_number`,
              `user_id`,
              `user_name`,
              `status`,
              `approval`,
              `inactive`
              )
              VALUES
              (
              CURRENT_DATE,
              CURRENT_TIME,
              '" . $pid . "',
              '" . $izr['zone'] . "',
              '" . $cqty . "',
              '" . $nqty . "',
              '" . $qty . "',
              'Invoice',
              '" . $invNum . "',
              '" . $_SESSION['user_id'] . "',
              '" . $_SESSION['full_name'] . "',
              'Pending',
              '',
              'No'
              )";
      mysqli_query($conn, $aiq) or die($conn->error);
    }
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'on_hold':
    $sq = "UPDATE `orders` SET `on_hold` = 'Yes', `on_hold_date` = CURRENT_DATE, `on_hold_time` = CURRENT_TIME, `on_hold_rep` = '" . $rep_id . "' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'in_process':
    $sq = "UPDATE `orders` SET `on_hold` = 'No', `on_hold_date` = CURRENT_DATE, `on_hold_time` = CURRENT_TIME, `on_hold_rep` = '" . $rep_id . "' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'in_shop':
    $sq = "UPDATE `orders` SET `in_shop` = 'Yes', `in_shop_date` = CURRENT_DATE, `in_shop_time` = CURRENT_TIME, `in_shop_rep` = '" . $rep_id . "', `shopNum` = '" . $shopNum . "', `status` = 'in_shop' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'shop_done':
    $sq = "UPDATE `orders` SET  `shop_done` = 'Yes', `shop_done_date` = CURRENT_DATE, `shop_done_time` = CURRENT_TIME, `shop_done_rep` = '" . $rep_id . "', `status` = 'shop_done' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'shipped':
    $sq = "UPDATE `orders` SET  `shipped` = 'Yes', `trackingNum` = '" . $trackingNum . "', `shipped_date` = CURRENT_DATE, `shipped_time` = CURRENT_TIME, `shipped_rep` = '" . $rep_id . "', `status` = 'shipped' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'invoiced':
    if($bypass == 'yes'){
      $sq = "UPDATE `orders` SET `invoiced` = 'Yes', `invoiced_date` = CURRENT_DATE, `invoiced_time` = CURRENT_TIME, `invoiced_rep` = '" . $rep_id . "', `status` = 'invoiced' WHERE `ID` = '" . $oid . "'";
    }else{
      //$sq = "UPDATE `orders` SET  `inv` = '" . $invoiceNum . "', `invoiced` = 'Yes', `invoiceNum` = '" . $invoiceNum . "', `invoiced_date` = CURRENT_DATE, `invoiced_time` = CURRENT_TIME, `invoiced_rep` = '" . $rep_id . "', `status` = 'invoiced' WHERE `ID` = '" . $oid . "'";
      if($dir == 'back'){
        $sq = "UPDATE `orders` SET `invoiced` = 'Yes', `paid` = '', `paid_date` = '0000-00-00', `paid_time` = '00:00:00', `paid_rep` = '', `status` = 'invoiced' WHERE `ID` = '" . $oid . "'";
      }else{
        $sq = "UPDATE `orders` SET `invoiced` = 'Yes', `invoiceNum` = '" . $invoiceNum . "', `invoiced_date` = CURRENT_DATE, `invoiced_time` = CURRENT_TIME, `invoiced_rep` = '" . $rep_id . "', `status` = 'invoiced' WHERE `ID` = '" . $oid . "'";
      }
    }
      
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  case 'paid':
    $sq = "UPDATE `orders` SET  `paid` = 'Yes', `payDetail` = '" . $payDetail . "', `paid_date` = CURRENT_DATE, `paid_time` = CURRENT_TIME, `paid_rep` = '" . $rep_id . "', `status` = 'paid' WHERE `ID` = '" . $oid . "'";
    mysqli_query($conn, $sq) or die($conn->error);
    echo 'Your order status has been updated to ' . $mode;
    break;
  default:
    echo 'Order Status Error!';
    break;
}

?>