<?php
include '../../php/connection.php';

//Load Variables...
$doc_name = mysqli_real_escape_string($conn, $_REQUEST['doc_name']);
$doc_desc = mysqli_real_escape_string($conn, $_REQUEST['doc_desc']);

//Tax Form Uploader...
if($_FILES['portalDocFile']['size'] != 0) {
$uid = uniqid();
$target_dir = "../../../oportal/documents/docs/";
$target_file2 = $target_dir . basename($_FILES["portalDocFile"]["name"]);

$uploadOk = 1;
$imageFileType = pathinfo($target_file2,PATHINFO_EXTENSION);
$target_file = $target_dir . $uid . "_portalDoc_" . $idate . "." . $imageFileType;

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
   && $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "pdf" 
   && $imageFileType != "PDF" ) {
    echo "<br>ERROR!- only JPG, JPEG, PNG & GIF files are allowed.";
    //echo "<br><script>alert('" . $imageFileType . "');</script>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "<br>ERROR!- your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["portalDocFile"]["tmp_name"], $target_file)) {
      
      //INSERT FILE PATH INTO DATABASE
      $file_name_1 = 'http://' . $_SERVER['HTTP_HOST'] . '/oportal/documents/docs/' . $uid . "_portalDoc_" . $idate . "." . $imageFileType;
      
        echo "<br>The file '". basename( $_FILES["portalDocFile"]["name"]). "' has been uploaded.";
        
    }else{
      echo '<br>There was an error moving your file from the temporary location to the permanent location.';
    }
}

$iq = "INSERT INTO `portal_documents`
      (
      `date`,
      `time`,
      `org_id`,
      `doc_type`,
      `cid`,
      `doc_name`,
      `doc_description`,
      `doc_url`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $_SESSION['org_id'] . "',
      'Marketing',
      'ALL',
      '" . $doc_name . "',
      '" . $doc_desc . "',
      '" . $file_name_1 . "',
      'No'
      )";
mysqli_query($pconn, $iq) or die($pconn->error);
  
echo '<script>
        window.location = "../../orders.php?tab=portal_docs";
      </script>';
  
}else{//End File Size Check to see if file exists...
  
  echo '<br><br>No File Found...';
  
}

?>