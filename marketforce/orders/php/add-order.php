<?php
include '../../php/connection.php';

//Load Variables
$rep_id = $_POST['rep_id'];
$rep_name = $_POST['rep_name'];
$cname = mysqli_real_escape_string($conn, $_POST['cname']);
$inv = mysqli_real_escape_string($conn, $_POST['invoice']);
$due_date = date("Y-m-d",strtotime($_POST['due_date']));
$notes = mysqli_real_escape_string($conn, $_POST['notes']);
$priority = $_POST['priority'];
$mode = $_POST['mode'];
$oid = $_POST['oid'];


//Work Order Attachment....................................................................................................................................................................................
if($_FILES['fileBox']['size'] != 0) {
//$post_body = file_get_contents('php://input');//Set the RAW data from POST Variables to be able to view them.
$id = rand(100000,1000000);

//Document Parsing
$target_dir = "../file_uploads/";
$target_file2 = $target_dir . basename($_FILES["fileBox"]["name"]);

$uploadOk1 = 1;
$imageFileType = pathinfo($target_file2,PATHINFO_EXTENSION);
$target_file = $target_dir . $id . "_docImg." . $imageFileType;
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileBox"]["tmp_name"]);
    if($check !== false) {
      //  echo "File is an image - " . $check["mime"] . ".";
        $uploadOk1 = 1;
    } else {
        echo "File is not an image.";
        $uploadOk1 = 0;
    }
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "pdf" && $imageFileType != "xlsx" && $imageFileType != "xlsm" ) {
    echo "<br>ERROR!- only JPG, JPEG, PDF, PNG & GIF files are allowed.";
    //echo "<br><script>alert('" . $imageFileType . "');</script>";
    $uploadOk1 = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk1 == 0) {
    echo "<br>ERROR!- your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileBox"]["tmp_name"], $target_file)) {
      
      //INSERT FILE PATH INTO DATABASE
      $file_name_1 = 'http://marketforceapp.com/marketforce/orders/file_uploads/' . $id . "_docImg." . $imageFileType;
      //echo '<br>File was moved to the right location!';
    }
}
}else{
  //Set URL to empty if no file was selected to upload...
  $file_name_1 = '';
  $uploadOk1 = 0;
}//End if file was uploaded


//Invoice Attachment....................................................................................................................................................................................
if($_FILES['invfileBox']['size'] != 0) {
//$post_body = file_get_contents('php://input');//Set the RAW data from POST Variables to be able to view them.
$id = rand(100000,1000000);

//Document Parsing
$target_dir = "../file_uploads/";
$target_file2 = $target_dir . basename($_FILES["invfileBox"]["name"]);

$uploadOk2 = 1;
$imageFileType = pathinfo($target_file2,PATHINFO_EXTENSION);
$target_file = $target_dir . $id . "_docImg." . $imageFileType;
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["invfileBox"]["tmp_name"]);
    if($check !== false) {
      //  echo "File is an image - " . $check["mime"] . ".";
        $uploadOk2 = 1;
    } else {
        echo "File is not an image.";
        $uploadOk2 = 0;
    }
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "pdf" && $imageFileType != "xlsx" && $imageFileType != "xlsm" ) {
    echo "<br>ERROR!- only JPG, JPEG, PDF, PNG & GIF files are allowed.";
    //echo "<br><script>alert('" . $imageFileType . "');</script>";
    $uploadOk2 = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk2 == 0) {
    echo "<br>ERROR!- your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["invfileBox"]["tmp_name"], $target_file)) {
      
      //INSERT FILE PATH INTO DATABASE
      $file_name_2 = 'http://marketforceapp.com/marketforce/orders/file_uploads/' . $id . "_docImg." . $imageFileType;
      //echo '<br>File was moved to the right location!';
    }
}
}else{
  //Set URL to empty if no file was selected to upload...
  $file_name_2 = '';
  $uploadOk2 = 0;
}//End if file was uploaded



//Bill of Lading Attachment....................................................................................................................................................................................
if($_FILES['bolfileBox']['size'] != 0) {
//$post_body = file_get_contents('php://input');//Set the RAW data from POST Variables to be able to view them.
$id = rand(100000,1000000);

//Document Parsing
$target_dir = "../file_uploads/";
$target_file2 = $target_dir . basename($_FILES["bolfileBox"]["name"]);

$uploadOk3 = 1;
$imageFileType = pathinfo($target_file2,PATHINFO_EXTENSION);
$target_file = $target_dir . $id . "_docImg." . $imageFileType;
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["bolfileBox"]["tmp_name"]);
    if($check !== false) {
      //  echo "File is an image - " . $check["mime"] . ".";
        $uploadOk3 = 1;
    } else {
        echo "File is not an image.";
        $uploadOk3 = 0;
    }
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "pdf" && $imageFileType != "xlsx" && $imageFileType != "xlsm" ) {
    echo "<br>ERROR!- only JPG, JPEG, PDF, PNG & GIF files are allowed.";
    //echo "<br><script>alert('" . $imageFileType . "');</script>";
    $uploadOk3 = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk3 == 0) {
    echo "<br>ERROR!- your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["bolfileBox"]["tmp_name"], $target_file)) {
      
      //INSERT FILE PATH INTO DATABASE
      $file_name_3 = 'http://marketforceapp.com/marketforce/orders/file_uploads/' . $id . "_docImg." . $imageFileType;
      //echo '<br>File was moved to the right location!';
    }
}
}else{
  //Set URL to empty if no file was selected to upload...
  $file_name_3 = '';
  $uploadOk3 = 0;
}//End if file was uploaded


if($mode == 'New'){
//Add Order to the database
$iq = "INSERT INTO `orders`
      (
      `date`,
      `time`,
      `rep_id`,
      `rep_name`,
      `cname`,
      `inv`,
      `due_date`,
      `notes`,
      `priority`,
      `rec_order`,
      `rec_order_date`,
      `rec_order_time`,
      `file_url`,
      `inv_file_url`,
      `bol_file_url`,
      `status`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $rep_id . "',
      '" . $rep_name . "',
      '" . $cname . "',
      '" . $inv . "',
      '" . $due_date . "',
      '" . $notes . "',
      '" . $priority . "',
      'Yes',
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $file_name_1 . "',
      '" . $file_name_2 . "',
      '" . $file_name_3 . "',
      'rec_order',
      'No'
      )";
mysqli_query($conn, $iq) or die($conn->error);
  
  
  //Setup Email Parameters...
  include '../../php/phpmailer/PHPMailerAutoload.php';
  $mail = new PHPMailer();
  include '../../php/phpmailsettings.php';
  $mail->setFrom('no-reply@allsteelcarports.com','Market Force');
  $mail->addAddress('cabrera@allsteelcarports.com');
  //$mail->addAddress('michael@burtonsolution.com');
  $mail->addBCC('archive@ignition-innovations.com');
  $mail->Subject = 'New Order!';
  
  //Setup Email Template Variable...
  $email->cname = $cname;
  $email->inv = $inv;
  $email->due_date = $due_date;
  $email->order_notes = $notes;
  $email->priority = $priority;
  $email->file_url = $file_name_1;
  $email->inv_file_url = $file_name_2;
  $email->bol_file_url = $file_name_3;
  include '../email/new-order-email.php';
  
  //Set the Email Body...
  $mail->Body = $newOrderEmail;
  
  //Send the Email...
  $mail->send();
  
  
}

if($mode == 'Update'){
  $uq = "UPDATE `orders` SET
        `cname` = '" . $cname . "',
        `inv` = '" . $inv . "',
        `due_date` = '" . $due_date . "',
        `notes` = '" . $priority . "'";
  if($uploadOk1 != 0){
    $uq .= ", `file_url` = '" . $file_name_1 . "'";
  }
  if($uploadOk2 != 0){
    $uq .= ", `inv_file_url` = '" . $file_name_2 . "'";
  }
  if($uploadOk3 != 0){
    $uq .= ", `bol_file_url` = '" . $file_name_3 . "'";
  }
        
  $uq .= " WHERE `ID` = '" . $oid . "'";
  
  mysqli_query($conn, $uq) or die($conn->error);
}

echo '<script>
        window.location = "../../orders.php";
        </script>';


?>