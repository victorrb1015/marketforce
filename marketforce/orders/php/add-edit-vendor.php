<?php
header('Content-Type: application/json');
error_reporting(0);
include '../../php/connection.php';

//Load Variables...
$vendor_id = $_REQUEST['vendor_id'];
$vendor_name = mysqli_real_escape_string($conn, $_REQUEST['vendor_name']);
$contact_name = mysqli_real_escape_string($conn, $_REQUEST['contact_name']);
$email = mysqli_real_escape_string($conn, $_REQUEST['vendor_email']);
$phone = mysqli_real_escape_string($conn, $_REQUEST['vendor_phone']);
//$address = mysqli_real_escape_string($conn, $_REQUEST['customer_address']);
//$city = mysqli_real_escape_string($conn, $_REQUEST['customer_city']);
//$state = mysqli_real_escape_string($conn, $_REQUEST['customer_state']);
//$zip = mysqli_real_escape_string($conn, $_REQUEST['customer_zip']);
//$authUsers = mysqli_real_escape_string($conn, $_REQUEST['authUsers']);
$notes = mysqli_real_escape_string($conn, $_REQUEST['vendor_notes']);
$category = mysqli_real_escape_string($conn, $_REQUEST['vendor_category']);
$mode = $_REQUEST['vendor_mode'];



#Main Functions...

//Add Customer...
if($mode == 'New'){
	
	$aq = "INSERT INTO `vendors`
			(
			`date`,
			`time`,
      `vendor_name`,
			`contact_name`,
			`vendor_email`,
			`vendor_phone`,
			`notes`,
			`category`,
			`inactive`
			)
			VALUES
			(
			CURRENT_DATE,
			CURRENT_TIME,
      '" . $vendor_name . "',
			'" . $contact_name . "',
			'" . $email . "',
			'" . $phone . "',
			'" . $notes . "',
			'" . $category . "',
			'No'
			)";

	mysqli_query($conn, $aq) or die($conn->error);
  
  $x->response = 'GOOD';
	$x->message = 'Customer Successully Added!';
  $_REQUEST['vendor_id'] = $conn->insert_id;

}

if($mode == 'Edit'){

	$uq = "UPDATE `vendors` SET
      `vendor_name` = '" . $vendor_name . "',
			`contact_name` = '" . $contact_name . "',
			`vendor_email` = '" . $email . "',
			`vendor_phone` = '" . $phone . "',
      `notes` = '" . $notes . "',
      `category` = '" . $category . "'
			WHERE `ID` = '" . $vendor_id . "'";

	mysqli_query($conn, $uq) or die($conn->error);
  
  $x->response = 'GOOD';
	$x->message = 'Customer Updated Successfully!';

}

$x->data = $_REQUEST;

//Setup Response Output...
$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;