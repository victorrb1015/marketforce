<?php
include '../../php/connection.php';

//Get Color Lists...
$lq = "SELECT DISTINCT `list` FROM `order_color_lists` WHERE `inactive` != 'Yes'";
$lg = mysqli_query($conn, $lq) or die($conn->error);

//Form JSON Data for each list...
while($lr = mysqli_fetch_array($lg)){
  $list = $lr['list'];
  //Get Colors on list...
  $q = "SELECT * FROM `order_product_colors` WHERE `list` = '" . $list . "' AND `inactive` != 'Yes' ORDER BY `color_name` ASC";
  $g = mysqli_query($conn, $q) or die($conn->error);
  $x->$list->colors = [];
  while($r = mysqli_fetch_array($g)){
    array_push($x->$list->colors, $r['color_name']);
  }
}

$response = json_encode($x);

echo $response;

?>