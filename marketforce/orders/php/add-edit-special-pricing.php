<?php
include '../../php/connection.php';
//echo var_dump($_POST);
//Load variables...
$cname = $_POST['sp_cname'];
$cid = $_POST['sp_cid'];
$rep_name = $_POST['sp_rep_name'];
$rep_id = $_POST['sp_rep_id'];
$items = array();
//Get product list...
$pq = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes'";
$pg = mysqli_query($conn, $pq) or die($conn->error);
while($pr = mysqli_fetch_array($pg)){
  //Check if special price was entered for product...
  $cpid = $pr['ID'];
  $prod_price = mysqli_real_escape_string($conn, $_POST[$cpid . '_sprice']);
  $int = (float)$prod_price;
    if($int > 0){
        //echo $pr['item_cost'];
        $lockout = $_POST[$cpid . '_lockout'];
        if($lockout == 'on'){
            $lockout = 'Yes';
        }else{
            $lockout = 'No';
        }
        //check if product has special price for this customer...
        $pcq = "SELECT * FROM `order_special_pricing` WHERE `pid` = '" . $cpid . "' AND `cid` = '" . $cid . "' AND `inactive` != 'Yes'";
        $pcg = mysqli_query($conn, $pcq) or die($conn->error);
        if(mysqli_num_rows($pcg) > 0){
            $res_s = mysqli_fetch_array($pcg);
            $item_cost = (float)$res_s['special_price'];
            if($int !== $item_cost){
                $uq = "UPDATE `order_special_pricing` SET `special_price` = '" . $int . "', `lockout` = '" . $lockout . "',`date` = CURRENT_DATE,`time` = CURRENT_TIME WHERE `pid` = '" . $cpid . "' AND `cid` = '" . $cid . "'";
                mysqli_query($conn, $uq) or die($conn->error);
                $array = [
                    'item_name' => $pr['item_name'],
                    'item_cost' => $pr['item_cost'],
                    'special_price' => $int
                ];
                $items[] = $array;
            }
            //Update the current record...
        }else{
            $array = [
                'item_name' => $pr['item_name'],
                'item_cost' => $pr['item_cost'],
                'special_price' => $int
            ];
            $items[] = $array;
            //Insert the new special price record...
            $iq = "INSERT INTO `order_special_pricing` 
              (
              `date`,
              `time`,
              `cid`,
              `pid`,
              `special_price`,
              `lockout`,
              `inactive`
              )
              VALUES
              (
              CURRENT_DATE,
              CURRENT_TIME,
              '" . $cid . "',
              '" . $cpid . "',
              '" . $int . "',
              '" . $lockout . "',
              'No'
              )";
            mysqli_query($conn, $iq) or die($conn->error);
        }
    }
}

echo '<br/>Special Pricing has been updated successfully!';
echo '<script>
        window.location = "../../orders.php?tab=customers";
      </script>';