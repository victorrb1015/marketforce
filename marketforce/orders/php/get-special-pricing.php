<?php
include '../../php/connection.php';

//Load variables...
$cid = mysqli_real_escape_string($conn, $_GET['cid']);
$rep_name = mysqli_real_escape_string($conn, $_GET['rep_name']);
$rep_id = mysqli_real_escape_string($conn, $_GET['rep_id']);
$ponly = mysqli_real_escape_string($conn, $_GET['ponly']);
$mode = mysqli_real_escape_string($conn, $_REQUEST['mode']);

//Get Special Pricing Details...
$q = "SELECT * FROM `order_special_pricing` WHERE `inactive` != 'Yes' AND `cid` = '" . $cid . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  $x->$r['pid'] = $r['special_price'];
  $lo_key = $r['pid'] . '_lockout';
  $x->$lo_key = $r['lockout'];
}

if($ponly != 'y'){
$nq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $cid . "'";
$ng = mysqli_query($conn, $nq) or die($conn->error);
$nr = mysqli_fetch_array($ng);
$x->name = $nr['customer_first_name'] . ' ' . $nr['customer_last_name'];
$x->email = $nr['customer_email'];
$x->phone = $nr['customer_phone'];
if($nr['taxform_url'] != ''){
  $x->taxExempt = 'Yes';
  $x->taxExemptForm = $nr['taxform_url'];
  $x->taxRate = $nr['tax_rate'];
}else{
  $x->taxExempt = 'No';
  $x->taxExemptForm = '';
  $x->taxRate = $nr['tax_rate'];
}
}

$response = json_encode($x);

echo $response;
  
?>