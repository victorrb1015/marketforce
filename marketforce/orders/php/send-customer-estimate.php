<?php
include '../../php/connection.php';

//Load Variables...
$invNum = $_GET['inv'];
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];

//Get ID...
$iq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `inv` = '" . $invNum . "'";
$ig = mysqli_query($conn, $iq) or die($conn->error);
$ir = mysqli_fetch_array($ig);
$ID = $ir['ID'];

//Get Customer...
$q = "SELECT * FROM `order_invoices` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $invNum . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
$cid = $r['cid'];
$cname = $r['cname'];

//Get email...
$eq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $cid . "'";
$eg = mysqli_query($conn, $eq) or die($conn->error);
$er = mysqli_fetch_array($eg);
$cemail = $er['customer_email'];

$invLink = 'http://marketforceapp.com/marketforce/orders/invoice/invoice.php?org_id=' . $_SESSION['org_id'] . '&mode=Print&inv=' . $invNum;


include '../../php/phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include '../../php/phpmailsettings.php';


switch ($_SESSION['org_id']) {
  case "123456":
    $mail->setFrom('orders@allsteelcarports.com', 'All American');
    break;
  default:
  $mail->setFrom('orders@allsteelcarports.com', 'Acero Building Components');
}
$mail->addAddress($cemail);
$mail->addBCC('archive@ignition-innovations.com');
$mail->Subject = 'Your Order Estimate!';

include '../email/send-customer-estimate-email.php';
$mail->Body = $sendEstimateEmail;

if($mail->send()){
  $x->status = 'GOOD';
  $x->ID = $ID;
  $x->response = 'Estimate ' . $invNum . ' was sent to ' . $cname;
  $x->error = 'Email Error: ' . $mail->ErrorInfo;
  //Add Note to DB...
  $nq = "INSERT INTO `order_notes`
        (
        `date`,
        `time`,
        `order_id`,
        `rep_id`,
        `rep_name`,
        `cname`,
        `note`,
        `inactive`
        )
        VALUES
        (
        CURRENT_DATE,
        CURRENT_TIME,
        '" . $ID . "',
        '" . $rep_id . "',
        '" . $rep_name . "',
        '" . $cname . "',
        'Estimate was emailed to the customer at " . $cemail . ".',
        'No'
        )";
  mysqli_query($conn, $nq) or die($conn->error);
}else{
  $x->status = 'BAD';
  $x->ID = $ID;
  $x->response = 'AN ERROR OCCURED!';
  $x->error = 'Email Error: ' . $mail->ErrorInfo;
}
$response = json_encode($x);
echo $response;

?>