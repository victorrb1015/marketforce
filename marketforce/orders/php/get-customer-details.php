<?php
include '../../php/connection.php';

//Load Variables...
$cid = $_GET['cid'];

//Get Details...
$q = "SELECT * FROM `order_customers` WHERE `ID` = '" . $cid . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);

$x->response = 'GOOD';
$x->ID = $r['ID'];
$x->company = $r['company_name'];
$x->fname = $r['customer_first_name'];
$x->lname = $r['customer_last_name'];
$x->email = $r['customer_email'];
$x->phone = $r['customer_phone'];
$x->address = $r['customer_address'];
$x->city = $r['customer_city'];
$x->state = $r['customer_state'];
$x->zip = $r['customer_zip'];
$x->authUsers = $r['auth_users'];
$x->taxFormURL = $r['taxform_url'];
$x->taxRate = $r['tax_rate'];
$x->notes = $r['customer_notes'];
$x->status = $r['status'];
$x->inactive = $r['inactive'];

$response = json_encode($x);

echo $response;

?>