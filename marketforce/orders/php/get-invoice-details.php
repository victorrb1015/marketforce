<?php
include '../../php/connection.php';

//Load Variables...
$inv = $_GET['inv'];
$jsondata = array();

//Get Details...
$q = "SELECT * FROM `order_invoices` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $inv . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);

$jsondata['invNum'] = $r['inv_num'];
$jsondata['invDate'] = date("F d, Y", strtotime($r['date']));
$jsondata['rep_id'] = $r['rep_id'];
$jsondata['rep_name'] = $r['rep_name'];
$jsondata['cid'] = $r['cid'];
$jsondata['cname'] = $r['cname'];
$jsondata['affiliate_mode'] = $r['affiliate_mode'];
$jsondata['freight'] = $r['freight'];
$jsondata['taxRate'] = $r['tax_rate'];
$jsondata['notes'] = $r['notes'];
$jsondata['status'] = $r['status'];
$jsondata['sa'] = $r['sa'];

//Get Customer Info...
$cq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $r['cid'] . "'";
$cg = mysqli_query($conn, $cq) or die($conn->error);
$cr = mysqli_fetch_array($cg);
$cemail = $cr['customer_email'];
$cphone = $cr['customer_phone'];
$jsondata['cemail'] = $cemail;
$jsondata['cphone'] = $cphone;
if($cr['taxform_url'] != ''){
    $jsondata['taxExempt'] = 'Yes';
    $jsondata['taxExemptForm'] = $cr['taxform_url'];
}else{
    $jsondata['taxExempt'] = 'No';
    $jsondata['taxExemptForm'] = '';
}

//Get Invoice Items
$iq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $inv . "'";
$ig = mysqli_query($conn, $iq) or die($conn->error);
$i = 0;
$json = array();
while($ir = mysqli_fetch_array($ig)){
    $array = [
        'pid' => $ir['pid'],
        'item_name' => $ir['item_name'],
        'item_desc' => $ir['item_desc'],
        'item_color' => $ir['item_color'],
        'item_length' => $ir['item_length'],
        'item_rate' => $ir['item_rate'],
        'item_arate' => $ir['item_arate'],
        'item_qty' => $ir['item_qty'],
        'item_price' => $ir['item_price'],
        'item_weight' => $ir['item_weight'],
    ];
    $jsondata['items'][$i] = $array;
  $i++;
}
header('Content-type: application/json; charset=utf-8');
echo json_encode($jsondata, JSON_FORCE_OBJECT);
exit();
