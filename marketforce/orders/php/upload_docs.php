<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
include '../../php/connection.php';

$array = [];
switch ($_POST['action']) {
    case 'upload_file':
        if(count($_FILES) != 0) {
            $id = $_GET['id'];
            $uid = uniqid();

            $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/docs-files/docs-customer/".$id."/";
            //$target_dir = "/home/marketforce/doc-files/docs-customer/".$id."/";
            //$target_dir = "/".$id."/";
            foreach ($_FILES as $file) {
                $target_file2 = $target_dir . basename($file["name"]);

                if (!file_exists($target_dir)) {
                    mkdir($target_dir, 0755, true);
                }
                $imageFileType = pathinfo($target_file2,PATHINFO_EXTENSION);
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "pdf"
                    && $imageFileType != "PDF" ) {

                    $array=['msg'=>'ERROR!- only JPG, JPEG, PNG & GIF files are allowed.', 'error'=>true];
                    echo json_encode($array);

                }else{
                    if(file_exists($target_file2)){
                        $array=['msg'=>'The file '. basename( $file["name"]). ' already exists.', 'error'=>true, 'data'=>""];
                        echo json_encode($array);
                    }else{
                        if (move_uploaded_file($file["tmp_name"], $target_file2)) {

                            $name = $file["name"];
                            //INSERT FILE PATH INTO DATABASE
                            $ruta = "/docs-files/docs-customer/".$id."/".$name;
                            $sql = "INSERT INTO docs_customer (id_customer, rute, name) values(".$id.", '".$ruta."', '".$name."')";
                            $res = mysqli_query($conn, $sql) or die($conn->error);

                            $sdocs = "SELECT * FROM docs_customer WHERE id_customer=".$id." AND inactive!=1 ORDER BY id ASC";
                            $qdocs = mysqli_query($conn, $sdocs) or die($conn->error);

                            $string = "";
                            while ($rdocs = mysqli_fetch_array($qdocs)){
                                $string .= '<tr>';
                                $string .= '<td>';
                                $string .='<a href="'. $rdocs['rute'] . '" target="_blank">' . $rdocs['name'] . '</a>';
                                $string .= '</td>';
                                $string .= '<td>';
                                $string .='<button class="btn btn-danger" onclick="deleteFile('.$rdocs['id'].')">Delete</button>';
                                $string .= '</td>';
                                $string .= '</tr>';
                            }

                            $array=['msg'=>'The file '. basename( $file["name"]). ' has been uploaded.', 'error'=>false, 'data'=>$string];
                            echo json_encode($array);
                        }else{

                            $array=['msg'=>'There was an error moving your file from the temporary location to the permanent location.', 'error'=>true];
                            echo json_encode($array);
                        }
                    }

                }
            }
        }else{//End File Size Check to see if file exists...
            $array=['msg'=>'No File Found', 'error'=>true];
            echo json_encode($array);
        }
        break;
    case 'delete_file':
        $id = $_POST['id'];
        $sql = "SELECT * FROM docs_customer WHERE id=".$id;
        $q = mysqli_query($conn, $sql) or die($conn->error);
        $r = mysqli_fetch_array($q);

        $ruta = $_SERVER['DOCUMENT_ROOT'].$r['rute'];
        if (file_exists($ruta)) {

            if (unlink($ruta)) {
                // file was successfully deleted
                $sqlDelete = "DELETE FROM `docs_customer` WHERE id=".$id;
                $q = mysqli_query($conn, $sqlDelete) or die($conn->error);

                $sdocs = "SELECT * FROM docs_customer WHERE id_customer=".$r['id_customer']." AND inactive!=1 ORDER BY id ASC";
                $qdocs = mysqli_query($conn, $sdocs) or die($conn->error);

                $string = "";
                while ($rdocs = mysqli_fetch_array($qdocs)){
                    $string .= '<tr>';
                    $string .= '<td>';
                    $string .='<a href="'. $rdocs['rute'] . '" target="_blank">' . $rdocs['name'] . '</a>';
                    $string .= '</td>';
                    $string .= '<td>';
                    $string .='<button class="btn btn-danger" onclick="deleteFile('.$rdocs['id'].')">Delete</button>';
                    $string .= '</td>';
                    $string .= '</tr>';
                }
                $array=['msg'=>'file was successfully deleted.', 'error'=>false, 'data'=>$string];
                echo json_encode($array);
            } else {
                // there was a problem deleting the file
                $array=['msg'=>'there was a problem deleting the file.', 'error'=>true];
                echo json_encode($array);
            }
        }

        break;

    default:
        break;
}
