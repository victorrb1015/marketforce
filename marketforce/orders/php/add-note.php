<?php
include '../../php/connection.php';

//Load Variables...
$oid = $_GET['oid'];
$note = mysqli_real_escape_string($conn, $_GET['note']);
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];

$cnq = "SELECT * FROM `orders` WHERE `ID` = '" . $oid . "'";
$cng = mysqli_query($conn, $cnq) or die($conn->error);
$cnr = mysqli_fetch_array($cng);
$cname = mysqli_real_escape_string($conn, $cnr['cname']);

//Add Note to database...
$q = "INSERT INTO `order_notes` 
      (
      `date`,
      `time`,
      `order_id`,
      `rep_id`,
      `rep_name`,
      `cname`,
      `note`,
      `inactive`
      )
      VALUES
      (
      CURRENT_DATE,
      CURRENT_TIME,
      '" . $oid . "',
      '" . $rep_id . "',
      '" . $rep_name . "',
      '" . $cname . "',
      '" . $note . "',
      'No'
      )";
mysqli_query($conn, $q) or die($conn->error);

echo 'Your note was added to the system!';

?>