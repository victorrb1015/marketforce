<?php
include '../../php/connection.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Documents</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <strong>Documents:</strong>
            <div class="col-4">

                <input type="file" name="document[]" id="document" multiple="multiple">
            </div>
            <div class="col-2">
                <input type="button" class="btn btn-primary" value="Upload" onclick="uploadDocumentCustomer(<? echo $_GET['id']; ?>)">
            </div>

        </div>
        <br>
        <div class="row">
            <table class="table table-striped table-bordered">
                <thead >
                <tr>
                    <th>Document</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="body_docs">
                <?php
                    $id = $_GET['id'];
                    $string = '';
                    $sq = "SELECT * FROM `docs_customer` where id_customer=".$id." and inactive!=1 ORDER BY `id` ASC";
                    $sg = mysqli_query($conn, $sq) or die($conn->error);
                    while($sr = mysqli_fetch_array($sg)){
                        $string = '<tr>';
                        $string .= '<td>';
                        $string .='<a href="'. $sr['rute'] . '" target="_blank">' . $sr['name'] . '</a>';
                        $string .= '</td>';
                        $string .= '<td>';
                        $string .='<button class="btn btn-danger" onclick="deleteFile('.$sr['id'].')">Delete</button>';
                        $string .= '</td>';
                        $string .= '</tr>';
                        echo $string;
                    }
                ?>

                </tbody>
            </table>
        </div>

    </div>

</body>

<script src="../../jquery/external/jquery/jquery.js"></script>
<script src="../../jquery/jquery-ui.js"></script>

<!--JS Code for ALL pages to access-->
<script src="../../js/mask/jquery.mask.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script>
    function uploadDocumentCustomer(id) {

        var idFiles=document.getElementById("document");
        var archivos=idFiles.files;
        var data=new FormData();

        for(var i=0;i<archivos.length;i++)
        {
            // Al objeto data, le pasamos clave,valor
            data.append("archivo"+i,archivos[i]);
            data.append("action",'upload_file');
        }
        $.ajax({
            url: "../php/upload_docs.php?id="+id,
            type: "POST",
            dataType: "json",
            data: data,
            cache: false,
            contentType: false,
            processData: false
        }).done(function(res){
                console.log(res);
                if (res.error === true){
                    alert(res.msg);
                }else{
                    var b = document.getElementById('body_docs');
                    var c = document.getElementById('document');
                    c.value = "";
                    b.innerHTML = "";
                    b.innerHTML = res.data;
                }
        });
    }

    function deleteFile(id) {
        var action = "delete_file";
        $.ajax({
            url: "../php/upload_docs.php",
            type: "POST",
            dataType: "json",
            data: {"action":action, "id":id},
        }).done(function(res){
            console.log(res);
            if (res.error === true){
                alert(res.msg);
            }else{
                var b = document.getElementById('body_docs');
                b.innerHTML = "";
                b.innerHTML = res.data;
            }
        });
    }
</script>
</html>
