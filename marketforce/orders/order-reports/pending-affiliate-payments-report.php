<?php
include '../../php/connection.php';

echo '
<html>
<head>
<title>Sales by Unit Report</title>
<link href="../../jquery/jquery-ui.css" rel="stylesheet" />
<style>
/* page */

html { font: 16px/1 "Open Sans", sans-serif; overflow: auto; padding: 0.5in; }
html { background: #999; cursor: default; }

body { box-sizing: border-box; min-height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }

th, td{
  border:1px solid black;
  padding:5px;
}

@media print {
	* { -webkit-print-color-adjust: exact; }
	html { background: none; padding: 0; }
	body { box-shadow: none; margin: 0; }
	span:empty { display: none; }
	.add, .cut { display: none; }
}

@page { margin: 0; }
</style>
</head>
<body>
<h1 style="text-align:center;">Pending Affiliate Payments Report</h1>
';

echo '<table class="table table-bordered table-hover table-striped packet-table" id="sales_table">
            <thead>
              <tr>
                <th>Date</th>
                <th>Inv#</th>
                <th>Customer</th>
                <th>Internal Amount</th>
                <th>External Amount</th>
                <th>Payout Amount</th>
              </tr>
            </thead>
            <tbody>';

              $oq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `status` = 'paid'";
              $og = mysqli_query($conn, $oq) or die($conn->error);
              while($or = mysqli_fetch_array($og)){
                $iq = "SELECT * FROM `order_invoices` WHERE `inactive` != 'Yes' AND `affiliate_mode` = 'Yes' AND `inv_num` = '" . $or['inv'] . "'";
                $ig = mysqli_query($conn, $iq) or die($conn->error);
                $ir = mysqli_fetch_array($ig);
                if(mysqli_num_rows($ig) > 0){
                
                  $apq = "SELECT * FROM `order_invoice_affiliate_payments` WHERE `inv_num` = '" . $ir['inv_num'] . "' AND `cid` = '" . $ir['cid'] . "'";
                  $apg = mysqli_query($conn, $apq) or die($conn->error);
                  if(mysqli_num_rows($apg) <= 0){
                    $nTotal = 0;
                    $aTotal = 0;
                    //Get Invoice Total...
                    $iiq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $or['inv'] . "'";
                    $iig = mysqli_query($conn, $iiq) or die($conn->error);
                    while($iir = mysqli_fetch_array($iig)){

                      //Check if item is a coil...
                      $ccq = "SELECT * FROM `inventory_items` WHERE `id` = '" . $iir['pid'] . "'";
                      $ccg = mysqli_query($conn, $ccq) or die($conn->error);
                      $ccr = mysqli_fetch_array($ccg);

                      $nTotal = $nTotal + (number_format($iir['item_rate'],2) * $iir['item_qty']);
                      $aTotal = $aTotal + (number_format($iir['item_arate'],2) * $iir['item_qty']);

                    }

                    echo '<tr>
                            <td>' . date("m/d/y",strtotime($ir['date'])) . '</td>
                            <td>' . $ir['inv_num'] . '</td>
                            <td>' . $ir['cname'] . '</td>
                            <td>$' . number_format($nTotal,2) . '</td>
                            <td>$' . number_format($aTotal,2) . '</td>
                            <td style="background:red;">$' . number_format(($aTotal - $nTotal),2) . '</td>
                          </tr>';
                  }
                }
              }
            
echo '</tbody>
			    </table>';

echo '</body>
<!--JQuery Files-->
        <script src="../../jquery/external/jquery/jquery.js"></script>
        <script src="../../jquery/jquery-ui.js"></script>
        <script>
          $(document).ready(function(){
            $( ".date" ).datepicker();
          });
        </script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
  $("#sales_table").dataTable({
    "paging": false,
    "order": [[ 1, "asc" ]]
  });
</script>
</html>';

?>