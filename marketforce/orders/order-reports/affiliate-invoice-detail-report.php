<?php
include '../../php/connection.php';

//Load Variables...
$cid = $_REQUEST['cid'];

echo '
<html>
<head>
<title>Affiliate Sales by Product Report</title>
<link href="../../jquery/jquery-ui.css" rel="stylesheet" />
<style>
/* page */

html { font: 16px/1 "Open Sans", sans-serif; overflow: auto; padding: 0.5in; }
html { background: #999; cursor: default; }

body { box-sizing: border-box; min-height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }

th, td{
  border:1px solid black;
  padding:5px;
}

@media print {
	* { -webkit-print-color-adjust: exact; }
	html { background: none; padding: 0; }
	body { box-shadow: none; margin: 0; }
	span:empty { display: none; }
	.add, .cut { display: none; }
}

@page { margin: 0; }
</style>
</head>
<body>';
echo '<h1 style="text-align:center;">Affiliate Sales by Product Report</h1>';

$cq = "SELECT * FROM `order_customers` WHERE `inactive` != 'Yes' AND `ID` = '" . $cid . "'";
$cg = mysqli_query($conn, $cq) or die($conn->error);
$cr = mysqli_fetch_array($cg);
echo '<h3 style="text-align:center;">' . $cr['company_name'] . '</h3>';

echo '<table class="table table-bordered table-hover table-striped packet-table" id="sales_table">
            <thead>
              <tr>
                <th>Barcode</th>
                <th>Item Type</th>
                <th>Item Name</th>
                <th>Internal Sales</th>
                <th>External Sales</th>
              </tr>
            </thead>
            <tbody>';
            $inTotal = 0;
            $iaTotal = 0;
            $iq = "SELECT * FROM `order_invoices` WHERE `inactive` != 'Yes' AND `affiliate_mode` = 'Yes' AND `cid` = '" . $cr['ID'] . "'";
            $ig = mysqli_query($conn, $iq) or die($conn->error);
            while($ir = mysqli_fetch_array($ig)){
              $oq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `inv` = '" . $ir['inv_num'] . "' AND `status` = 'paid'";
              $og = mysqli_query($conn, $oq) or die($conn->error);
              $or = mysqli_fetch_array($og);
              if(mysqli_num_rows($og) > 0){
                $nTotal = 0;
                $aTotal = 0;
                //Get Invoice Total...
                $iiq = "SELECT DISTINCT `pid` FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $or['inv'] . "'";
                $iig = mysqli_query($conn, $iiq) or die($conn->error);
                while($iir = mysqli_fetch_array($iig)){
                  
                  //Get Item Info...
                  $ccq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $iir['pid'] . "'";
                  $ccg = mysqli_query($conn, $ccq) or die($conn->error);
                  $ccr = mysqli_fetch_array($ccg);
                  if($ccr['barcode'] == ''){
                    $ccr['barcode'] = 'Unknown';
                  }
                  if($ccr['item_type'] == ''){
                    $ccr['item_type'] = 'Unknown';
                  }
                  echo '<tr>
                          <td>
                            <a href="product-sale-report.php?pid=' . $ccr['ID'] . '" target="_blank">
                              ' . $ccr['barcode'] . '
                            </a>
                          </td>
                          <td>' . $ccr['item_type'] . '</td>
                          <td>' . $ccr['item_name'] . '</td>';
                  
                  //Get total sales for item...
                  $nTotal = 0;
                  $aTotal = 0;
                  $q = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $or['inv'] . "' AND `pid` = '" . $iir['pid'] . "'";
                  $g = mysqli_query($conn, $q) or die($conn->error);
                  while($r = mysqli_fetch_array($g)){
                   $nTotal = $nTotal + number_format($r['item_rate'],2);
                   $aTotal = $aTotal + number_format($r['item_arate'],2);
                  }

                  echo '<td>
                          $' . number_format($nTotal,2) . '
                        </td>
                        <td>
                          $' . number_format($aTotal,2) . '
                        </td>
                      </tr>';
                  }

                }
              }
            
            
echo '</tbody>
			    </table>';

echo '</body>
<!--JQuery Files-->
        <script src="../../jquery/external/jquery/jquery.js"></script>
        <script src="../../jquery/jquery-ui.js"></script>
        <script>
          $(document).ready(function(){
            $( ".date" ).datepicker();
          });
        </script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
  $("#sales_table").dataTable({
    "paging": false,
    "order": [[ 0, "asc" ]]
  });
</script>
</html>';

?>