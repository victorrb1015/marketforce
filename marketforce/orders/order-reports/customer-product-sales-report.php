<?php
include '../../php/connection.php';

//Load Variables...
$cid = mysqli_real_escape_string($conn, $_GET['cid']);
$sdate = mysqli_real_escape_string($conn,$_GET['sdate']);
$edate = mysqli_real_escape_string($conn,$_GET['edate']);
$all = mysqli_real_escape_string($conn,$_GET['all']);

//Get Company Name...
$cnq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $cid . "'";
$cng = mysqli_query($conn, $cnq) or die($conn->error);
$cnr = mysqli_fetch_array($cng);
$cname = $cnr['company_name'];

echo '
<html>
<head>
<title>Customer Product Sales Report</title>
<link href="../../jquery/jquery-ui.css" rel="stylesheet" />
<style>
/* page */

html { font: 16px/1 "Open Sans", sans-serif; overflow: auto; padding: 0.5in; }
html { background: #999; cursor: default; }

body { box-sizing: border-box; min-height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }

th, td{
  border:1px solid black;
  padding:5px;
}

@media print {
	* { -webkit-print-color-adjust: exact; }
	html { background: none; padding: 0; }
	body { box-shadow: none; margin: 0; }
	span:empty { display: none; }
	.add, .cut { display: none; }
}

@page { margin: 0; }
</style>
</head>
<body>
<h1 style="text-align:center;">Customer Product Sales Report</h1>
';


  //Load Variables...
  if($all != ''){
    echo '<b>Sales From: ALL Dates</b> 
          <br><br>';
  }else{
    echo '<b>Sales From: ' . $sdate . ' to ' . $edate . ' for ' . $cname . '</b> 
          <br><br>';
  }
  
 echo '<table id="sales_table">
        <thead>
         <tr style="background:lightgray;">
         <th>Product Name</th>
         <th>Product ID</th>
         <th>Sold By</th>
         <th>Units Sold</th>
         <th>Avg Price/Unit</th>
         <th>Total Sales</th>
         </tr>
         </thead>
         <tbody>';

//Get Product List...
$pq = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' ORDER BY `item_name` ASC";
$pg = mysqli_query($conn, $pq) or die($conn->error);
while($pr = mysqli_fetch_array($pg)){
  
  //Get Sales for this product...
  $pqty = 0;
  $plength = 0;
  $pprice = 0;
  $pprice_avg = 0;
  $prate = 0;
  //$npunits = 0;
  $punits = 0;
  $spq = "SELECT * FROM `order_invoice_items` WHERE `pid` = '" . $pr['ID'] . "' AND `inactive` != 'Yes'";
  $spg = mysqli_query($conn, $spq) or die($conn->error);
  $icount = mysqli_num_rows($spg);
  if($icount > 0){
    $ii = 0;
    while($spr = mysqli_fetch_array($spg)){
      $npunits = 0;
      if($all != ''){
      $ivq = "SELECT * FROM `order_invoices` WHERE `status` != 'Quote' AND `inv_num` = '" . $spr['inv_num'] . "' AND `inactive` != 'Yes' AND `cid` = '" . $cid . "'";
      }else{
        $ivq = "SELECT * FROM `order_invoices` 
                WHERE 
                `status` != 'Quote'
                AND
                `inv_num` = '" . $spr['inv_num'] . "' 
                AND 
                `inactive` != 'Yes'
                AND 
                `cid` = '" . $cid . "'
                AND 
                `date` >= '" . $sdate . "' 
                AND 
                `date` <= '" . $edate . "'";
      }
      $ivg = mysqli_query($conn, $ivq) or die($conn->error);
      $rcount = mysqli_num_rows($ivg);
      if($rcount > 0){
        $ii++;
        $pqty = $pqty + $spr['item_qty'];
        if($spr['item_length'] == 'N/A'){
          $plength_add = 0;
        }else{
          $plength_add = $spr['item_length'];
        }
        $plength = $plength + $plength_add;
        if($plength <= 0){
          $npunits = $spr['item_qty'] * 1;
        }else{
          $npunits = $spr['item_qty'] * $plength;
        }
        $punits = $punits + $npunits;
        $pprice = $pprice + $spr['item_price'];
        $prate = $prate + $spr['item_rate'];
      
      }//End if valid invoice...
   }
}
  
if($plength <= 0){
  $plength = 1;
} 
$pprice_avg = ($prate / $ii);
 
  
  echo '<tr>
          <td>
            <!--<a href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/orders/order-reports/product-sales-report.php?pid=' . $pr['ID'] . '&sdate=' . $sdate . '&edate=' . $edate . '&all=' . $_POST['submit_all'] . '" target="_blank">-->
            ' . $pr['item_name'] . '
            <!--</a>-->
          </td>
          <td>' . $pr['ID'] . '</td>
          <td>' . $pr['item_units'] . '</td>
          <td>' . number_format($punits,2) . '</td>
          <td>$' . number_format($pprice_avg,2) . '</td>
          <td>$' . number_format($pprice,2) . '</td>
        </tr>';
  
}
echo '</tbody>
      </table>';

echo '</body>
<!--JQuery Files-->
        <script src="../../jquery/external/jquery/jquery.js"></script>
        <script src="../../jquery/jquery-ui.js"></script>
        <script>
          $(document).ready(function(){
            $( ".date" ).datepicker();
          });
        </script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
  $("#sales_table").dataTable({
    "paging": false,
    "order": [[ 5, "desc" ]]
  });
</script>
</html>';

?>