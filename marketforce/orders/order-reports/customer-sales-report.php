<?php
include '../../php/connection.php';

echo '
<html>
<head>
<title>Sales by Customer Report</title>
<link href="../../jquery/jquery-ui.css" rel="stylesheet" />
<style>
/* page */

html { font: 16px/1 "Open Sans", sans-serif; overflow: auto; padding: 0.5in; }
html { background: #999; cursor: default; }

body { box-sizing: border-box; min-height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }

th, td{
  border:1px solid black;
  padding:5px;
}

@media print {
	* { -webkit-print-color-adjust: exact; }
	html { background: none; padding: 0; }
	body { box-shadow: none; margin: 0; }
	span:empty { display: none; }
	.add, .cut { display: none; }
}

@page { margin: 0; }
</style>
</head>
<body>
<h1 style="text-align:center;">Sales by Customer Report</h1>
';

if(!$_POST['submit'] && !$_POST['submit_all']){
  
  echo '<div style="margin:auto;text-align:center;">
          <h2><u>Select Date Range for Report</u></h2>
          <form action="customer-sales-report.php" method="post" />
          <input type="text" class="date" name="sdate" placeholder="Start Date" autocomplete="off" />
          <input type="text" class="date" name="edate" placeholder="End Date" autocomplete="off" />
          <input type="submit" name="submit" value="Submit" />
          <input type="submit" name="submit_all" value="View All Dates" />
        </div>';
  
  
  
}else{
  //Load Variables...
  if($_POST['submit_all']){
    echo '<b>Sales From: ALL Dates<b> 
          <a href="customer-sales-report.php" style="color:blue;">(Change Date Range)</a>
          <br><br>';
  }else{
    $sdate = date("Y-m-d",strtotime($_POST['sdate']));
    $edate = date("Y-m-d",strtotime($_POST['edate']));
    echo '<b>Sales From: ' . $_POST['sdate'] . ' to ' . $_POST['edate'] . '<b> 
          <a href="customer-sales-report.php" style="color:blue;">(Change Date Range)</a>
          <br><br>';
  }
  
 echo '<table id="sales_table">
        <thead>
         <tr style="background:lightgray;">
         <th>Customer Name</th>
         <th>Total Sales</th>
         </tr>
         </thead>
         <tbody>';

//Get Product List...
$pq = "SELECT * FROM `order_customers` WHERE `inactive` != 'Yes' ORDER BY `company_name` ASC";
$pg = mysqli_query($conn, $pq) or die($conn->error);
while($pr = mysqli_fetch_array($pg)){
  
  //Get Sales for this Customer...
  $grand_total = 0;
  if($_POST['submit_all']){
    $iq = "SELECT * FROM `order_invoices` WHERE `status` != 'Quote' AND `inactive` != 'Yes' AND `cid` = '" . $pr['ID'] . "'";
  }else{
    $iq = "SELECT * FROM `order_invoices` WHERE `status` != 'Quote' AND `inactive` != 'Yes' AND `cid` = '" . $pr['ID'] . "' AND `date` >= '" . $sdate . "' AND `date` <= '" . $edate . "'";
  }
  $ig = mysqli_query($conn, $iq) or die($conn->error);
  while($ir = mysqli_fetch_array($ig)){
    $inv_total = 0;
    $iiq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $ir['inv_num'] . "'";
    $iig = mysqli_query($conn, $iiq) or die($conn->error);
    while($iir = mysqli_fetch_array($iig)){
      $inv_total = $inv_total + $iir['item_price'];
      $grand_total = $grand_total + $iir['item_price'];
    }
  }
  
  
 
  
  echo '<tr>
          <td>
            <a href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/orders/order-reports/customer-product-sales-report.php?cid=' . $pr['ID'] . '&sdate=' . $sdate . '&edate=' . $edate . '&all=' . $_POST['submit_all'] . '" target="_blank">
            ' . $pr['company_name'] . '
            </a>
          </td>
          <td>$' . number_format($grand_total,2) . '</td>
        </tr>';
  
}
echo '</tbody>
      </table>';

echo '';

}

echo '</body>
<!--JQuery Files-->
        <script src="../../jquery/external/jquery/jquery.js"></script>
        <script src="../../jquery/jquery-ui.js"></script>
        <script>
          $(document).ready(function(){
            $( ".date" ).datepicker();
          });
        </script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
  $("#sales_table").dataTable({
    "paging": false,
    "order": [[ 1, "desc" ]]
  });
</script>
</html>';

?>