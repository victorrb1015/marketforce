<?php
include '../../php/connection.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Unpaid Orders</title>
</head>
<body>
<div class="container">
    <div class="row">
        <h1>Unpaid Orders:</h1>

    </div>
    <br>
    <div class="row">
        <table class="table table-striped table-bordered">
            <thead >
            <tr>
                <th>Date</th>
                <th>Order</th>
                <th>Customer</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody id="body_orders">
            <?php

            $string = '';
            $sq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `status` = 'invoiced' ORDER BY `date` DESC";
            $sg = mysqli_query($conn, $sq) or die($conn->error);
            while($sr = mysqli_fetch_array($sg)){
                $string = '<tr>';
                $string .= '<td>';
                $string .= $sr['date'];
                $string .= '</td>';
                $string .= '<td>';
                $string .= $sr['inv'];
                $string .= '</td>';
                $string .= '<td>';
                $string .= $sr['cname'];
                $string .= '</td>';
                $string .= '<td>';
                $string .= $sr['status'];
                $string .= '</td>';
                $string .= '</tr>';
                echo $string;
            }
            ?>

            </tbody>
        </table>
    </div>

</div>

</body>

<script src="../../jquery/external/jquery/jquery.js"></script>
<script src="../../jquery/jquery-ui.js"></script>

<!--JS Code for ALL pages to access-->
<script src="../../js/mask/jquery.mask.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</html>
