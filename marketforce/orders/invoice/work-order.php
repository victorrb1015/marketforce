<?php
session_start();
include '../../php/connection.php';
$oq = "SELECT * FROM `organizations` WHERE `org_id` = '" . $_GET['org_id'] . "'";
$og = mysqli_query($mf_conn, $oq) or die($conn->error);
if(mysqli_num_rows($og) > 0){
  $or = mysqli_fetch_array($og);
  $_SESSION['org_db_name'] = $or['db_name'];
}else{
	echo 'There was an error opening this Work Order.';
	break;
  //$_SESSION['org_db_name'] = 'mburton9_mf_allsteelcarports';
}
include '../../php/connection.php';

//Load Variables...
$invNum = $_GET['inv'];

$q = "SELECT * FROM `order_invoices` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $invNum . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);

$cid = $r['cid'];
$cname = $r['cname'];
$date = date("F d, Y", strtotime($r['date']));
?>
<html>
	<head>
		<meta charset="utf-8">
		<title>Work Order <? echo $invNum; ?></title>
		<link rel="stylesheet" href="css/style.css">
		<link rel="license" href="https://www.opensource.org/licenses/mit-license/">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	</head>
	<body>
		<header>
			<h1 style="background:red;color:black;">Work Order</h1>
			<!--<address>
				<p>All Steel Carports</p>
				<p>2200 N Granville Ave<br>Muncie, IN 47303</p>
				<p>(765) 284-0694</p>
			</address>
			<span>
				<img alt="" src="http://marketforceapp.com/marketforce/img/all-steel-red.jpg" style="width:50%;">
			</span>-->
		</header>
		<article>
			<h1>Recipient</h1>
			<address>
        <p>Customer: <? echo $cname; ?></p>
			</address>
			<table class="meta">
				<tr>
					<th><span>Invoice #</span></th>
					<td><span id="invNum"><? echo $invNum; ?></span></td>
				</tr>
				<tr>
					<th><span>Date</span></th>
					<td><span id="invDate"><? echo $date; ?></span></td>
				</tr>
				<!--<tr>
					<th><span>Amount Due</span></th>
					<td><span id="prefix">$</span><span id="amt_due"><? echo $amtDue; ?></span></td>
				</tr>-->
			</table>
			<table class="inventory">
				<thead>
					<tr>
						<th style="width:180px;"><span>Item</span></th>
						<th><span>Description</span></th>
						<th><span>Color</span></th>
						<th style="width:60px;"><span>Quantity</span></th>
						<th style="width:60px;"><span>Length</span></th>
                        <th style="width:120px;"><span>Weight</span></th>
						<!--<th><span>Price</span></th>-->
					</tr>
				</thead>
				<tbody id="inv_table">
          <?
          $iq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $invNum . "'";
          $ig = mysqli_query($conn, $iq) or die($conn->error);
          while($ir = mysqli_fetch_array($ig)){
              if($ir['item_length'] != '' && $ir['item_length'] != 0){
                  $item_weight = ($ir['item_weight'] * $ir['item_qty'] * $ir['item_length']);
              }else{
                  $item_weight = ($ir['item_weight'] * $ir['item_qty']);
              }
            echo '<tr>
                    <td>
                      ' . $ir['item_name'] . '
                    </td>
                    <td>
                      ' . $ir['item_desc'] . '
                    </td>
                    <td>
                      ' . $ir['item_color'] . '
                    </td>
                    <td>
                      ' . $ir['item_qty'] . '
                    </td>
                    <td>
                      ' . $ir['item_length'] . '
                    </td>
                    <td>
                      ' . $item_weight . ' lbs
                    </td>
                  </tr>';
          }
					?>
				</tbody>
			</table>
			<!--<a class="add">+</a>
			<table class="balance">
				<tr>
					<th><span>Total</span></th>
					<td><span data-prefix>$</span><span id="total">0.00</span></td>
				</tr>
				<tr>
					<th><span>Amount Paid</span></th>
					<td><span data-prefix>$</span><span>0.00</span></td>
				</tr>
				<tr>
					<th><span>Balance Due</span></th>
					<td><span data-prefix>$</span><span id="bal_due">0.00</span></td>
				</tr>
			</table>-->
		</article>
		<!--<aside>
			<h1><span>Additional Notes For Shop</span></h1>
			<div>
        <textarea id="notes" name="notes" style="width:100%;min-height:32px;">No Notes.</textarea>
			</div>
		</aside>-->
    <input type="hidden" id="rep_id" value="<? echo $_SESSION['user_id']; ?>" />
    <input type="hidden" id="rep_name" value="<? echo $_SESSION['full_name']; ?>" />
    <div style="width:100%;">
      <!--<button type="button" 
              style="cursor:pointer;float:right;background:green;padding:8px;border-radius:10px;color:white;margin:10px;" 
              onclick="save_invoice('<? echo $_GET['mode']; ?>');">
        Save
      </button>-->
      <button type="button" 
              style="cursor:pointer;float:right;background:green;padding:8px;border-radius:10px;color:white;margin:10px;" 
              id="printPageButton"
              onclick="window.print();">
        Print
      </button>
    </div>
	</body>
  <script src="../../js/mask/jquery.mask.js"></script>
  <script>
    $(document).ready(function(){
      //$('.date').mask('00/00/0000');
      $('.number').mask('000,000');
      //$('.time').mask('00:00:00');
      //$('.phone').mask('(000) 000-0000');
      //$('.phone_us').mask('(000) 000-0000');
      $('.usd').mask('000,000,000,000,000.00', {reverse: true});
      //$( ".date" ).datepicker();
    });
  </script>
</html>