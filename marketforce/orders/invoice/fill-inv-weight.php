<?php
include '../../php/connection.php';
error_reporting(E_ALL);
$inv_num = '1801199';

//Get Invoice Items...
$iiq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes'";
//$iiq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $inv_num . "'";
$iig = mysqli_query($conn, $iiq) or die($conn->error);
while($iir = mysqli_fetch_array($iig)){
  $pid = $iir['pid'];
  $inv_num = $iir['inv_num'];
  
  //Get Item Weight...
  $wq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $pid . "'";
  $wg = mysqli_query($conn, $wq) or die($conn->error);
  $wr = mysqli_fetch_array($wg);
  $item_weight = $wr['item_weight'];
  
  //Update Item Weight to the invoice item database...
  $uq = "UPDATE `order_invoice_items` SET `item_weight` = '" . $item_weight . "' WHERE `ID` = '" . $iir['ID'] . "'";
  mysqli_query($conn, $uq) or die($conn->error);
  echo 'Updated Invoice#: ' . $inv_num . ' - Item#: ' . $pid . ' [' . $wr['item_name'] . '] weight to: ' . $item_weight . '<br>';
}