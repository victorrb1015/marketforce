<?php
session_start();
include '../../php/connection.php';
$oq = "SELECT * FROM `organizations` WHERE `org_id` = '" . $_GET['org_id'] . "'";
$og = mysqli_query($mf_conn, $oq) or die($conn->error);
if(mysqli_num_rows($og) > 0){
  $or = mysqli_fetch_array($og);
  $_SESSION['org_db_name'] = $or['db_name'];
  $_SESSION['org_logo_url_alt'] = $or['org_logo_url_alt'];
  $_SESSION['org_phone'] = $or['phone'];
  $_SESSION['org_name'] = $or['org_name'];
  $_SESSION['org_address'] = $or['org_address'];
  $_SESSION['org_city'] = $or['org_city'];
  $_SESSION['org_state'] = $or['org_state'];
  $_SESSION['org_zip'] = $or['org_zip'];
}else{
  echo 'There was an error opening this Bill of Lading.';
  break;
  //$_SESSION['org_db_name'] = 'mburton9_mf_allsteelcarports';
}
include '../../php/connection.php';

//Load Variables...
$invNum = $_GET['inv'];

$q = "SELECT * FROM `order_invoices` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $invNum . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
$sa = $r['ship_address'];

//Get Shipping Address...
$saq = "SELECT * FROM `order_ship_addresses` WHERE `ID` = '" . $sa . "'";
$sag = mysqli_query($conn, $saq) or die($conn->error);
$sar = mysqli_fetch_array($sag);
$saddress = $sar['address'];
$scity = $sar['city'];
$sstate = $sar['state'];
$szip = $sar['zip'];


$cid = $r['cid'];
$cname = $r['cname'];
$date = date("F d, Y", strtotime($r['date']));

//Get Customer Info...
$cq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $cid . "'";
$cg = mysqli_query($conn, $cq) or die($conn->error);
$cr = mysqli_fetch_array($cg);
if($sa == ''){
$saddress = $cr['customer_address'];
$scity = $cr['customer_city'];
$sstate = $cr['customer_state'];
$szip = $cr['customer_zip'];
}

$total_weight = 0;
$item_weight = 0;
$total_items = 0;
?>
<html>
	<head>
		<meta charset="utf-8">
		<title>Bill Of Lading <? echo $invNum; ?></title>
		<link rel="stylesheet" href="css/style.css">
		<link rel="license" href="https://www.opensource.org/licenses/mit-license/">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	</head>
	<body>
		<header>
			<h1 style="background:green;">Bill Of Lading</h1>
			<address>
        <p><?php echo $_SESSION['org_name']; ?></p>
        <p><?php echo $_SESSION['org_address']; ?><br><?php echo $_SESSION['org_city'] . ', ' . $_SESSION['org_state'] . ' ' . $_SESSION['org_zip']; ?></p>
        <p><?php echo $_SESSION['org_phone']; ?></p>
      </address>
      <span>
        <img alt="Logo" src="<?php echo $_SESSION['org_logo_url_alt']; ?>" style="width:50%;">
        <!--<input type="file" accept="image/*">-->
      </span>
    </header>
		<article>
			<h1>Recipient</h1>
			<address>
        <p>
          <span style="text-decoration:underline;">SHIP TO:</span> 
          <br><br>
          <? echo $cname; ?>
          <br>
          <? echo $saddress; ?>
          <br>
          <? echo $scity . ', ' . $sstate . ' ' . $szip; ?>
        </p>
			</address>
			<table class="meta">
				<tr>
					<th><span>Invoice #</span></th>
					<td><span id="invNum"><? echo $invNum; ?></span></td>
				</tr>
				<tr>
					<th><span>Date</span></th>
					<td><span id="invDate"><? echo $date; ?></span></td>
				</tr>
				<!--<tr>
					<th><span>Amount Due</span></th>
					<td><span id="prefix">$</span><span id="amt_due"><? echo $amtDue; ?></span></td>
				</tr>-->
			</table>
			<table class="inventory">
				<thead>
					<tr>
						<th><span>Item</span></th>
						<th><span>Description</span></th>
						<th><span>Color</span></th>
                        <th style="width:60px;"><span>Quantity</span></th>
                        <th style="width:60px;"><span>Length</span></p>
						<th style="width:120px;"><span>Weight</span></th>
					</tr>
				</thead>
				<tbody id="inv_table">
          <?
          $iq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $invNum . "'";
          $ig = mysqli_query($conn, $iq) or die($conn->error);
          while($ir = mysqli_fetch_array($ig)){
            if($ir['item_length'] != '' && $ir['item_length'] != 0){
              $item_weight = ($ir['item_weight'] * $ir['item_qty'] * $ir['item_length']);
            }else{
              $item_weight = ($ir['item_weight'] * $ir['item_qty']);
            }
            $total_weight = $total_weight + $item_weight;
            $total_items = $total_items + $ir['item_qty'];
            echo '<tr>
                    <td>
                      ' . $ir['item_name'] . '
                    </td>
                    <td>
                      ' . $ir['item_desc'] . '
                    </td>
                    <td>
                      ' . $ir['item_color'] . '
                    </td>
                    <td>
                    ' . $ir['item_qty'] . '
                    </td>
                    <td>
                      ' . $ir['item_length'] . '
                    </td>
                    <td>
                      ' . $item_weight . ' lbs
                    </td>
                  </tr>';
          }
					?>
				</tbody>
			</table>
			<!--<a class="add">+</a>-->
			<table class="balance">
				<tr>
					<th><span>Total # Of Items</span></th>
          <td><span><? echo $total_items; ?></span></td>
				</tr>
				<tr>
					<th><span>Total Weight</span></th>
					<td><span><? echo $total_weight; ?> lbs</td>
				</tr>
				<!--<tr>
					<th><span>Balance Due</span></th>
					<td><span data-prefix>$</span><span id="bal_due">0.00</span></td>
				</tr>-->
			</table>
		</article>
		<aside>
			<h1><span>Shipping Information</span></h1>
			<div>
        <p>Driver Signature:___________________________ &nbsp; Date/Time:_____________________</p>
        <br><br>
        <p>Received By:___________________________ &nbsp; Date/Time:_____________________</p>
			</div>
		</aside>
    <input type="hidden" id="rep_id" value="<? echo $_SESSION['user_id']; ?>" />
    <input type="hidden" id="rep_name" value="<? echo $_SESSION['full_name']; ?>" />
    <div style="width:100%;">
      <!--<button type="button" 
              style="cursor:pointer;float:right;background:green;padding:8px;border-radius:10px;color:white;margin:10px;" 
              onclick="save_invoice('<? echo $_GET['mode']; ?>');">
        Save
      </button>-->
      <button type="button" 
              style="cursor:pointer;float:right;background:green;padding:8px;border-radius:10px;color:white;margin:10px;" 
              id="printPageButton"
              onclick="window.print();">
        Print
      </button>
    </div>
	</body>
  <script src="../../js/mask/jquery.mask.js"></script>
  <script>
    $(document).ready(function(){
      //$('.date').mask('00/00/0000');
      $('.number').mask('000,000');
      //$('.time').mask('00:00:00');
      //$('.phone').mask('(000) 000-0000');
      //$('.phone_us').mask('(000) 000-0000');
      $('.usd').mask('000,000,000,000,000.00', {reverse: true});
      //$( ".date" ).datepicker();
    });
  </script>
</html>