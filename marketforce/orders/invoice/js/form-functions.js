function save_invoice(mode, cpMode) {
    var params = 'mode=' + mode;
    var items = {};
    if (customer_mode === false) {
        var rep_id = document.getElementById('rep_id').value;
        var rep_name = document.getElementById('rep_name').value;
        var org_id = document.getElementById('org_id').value;
    } else {
        var rep_id = document.getElementById('op_rep_id').value;
        var rep_name = document.getElementById('op_rep_name').value;
        var org_id = document.getElementById('op_org_id').value;
    }
    params += '&rep_id=' + rep_id + '&rep_name=' + rep_name + '&org_id=' + org_id;

    //Invoice Info...
    var invNumber = document.getElementById('invNum').innerHTML;
    if (invNumber === '') {
        alert('Please Enter An Invoice Number!');
        return;
    }
    if (invNumUse === true) {
        alert('Invoice Number Already In Use. Please Enter A Different Invoice Number!');
        return;
    }
    invNumber = encodeURL(invNumber);
    params += '&invNum=' + invNumber;
    var invDate = document.getElementById('invDate').innerHTML;
    params += '&invDate=' + invDate;

    if (weightOver === true) {
        alert('Your Invoice Exceeds the Weight Limit of 45,500lbs per truck!');
        if (customer_mode === true) {
            return;
        }
    }

    var cid = document.getElementById('customer').value;
    if (cid === '') {
        alert('Please Select A Customer For This Invoice!');
        return;
    }
    params += '&cid=' + cid;

    //Shipping Address...
    var sa = document.getElementById('shipping_address').value;
    if (sa === '') {
        alert('Please Select A Shipping Address For This Invoice!');
        return;
    }
    params += '&sa=' + sa;

    var taxRate = document.getElementById('taxPercent').value;
    params += '&taxRate=' + taxRate;

    if (exempt_status === false && taxRate <= 0) {
        alert('This customer is not tax exempt. You must enter a tax rate to calculate taxes on this invoice!');
        return;
    }

    var notes = document.getElementById('notes').value;
    notes = encodeURL(notes);
    params += '&notes=' + notes;


    //Invoice Items...
    for (var i = 0; i < rowNums.length; i++) {
        var rnum = rowNums[i];
        items[i] = {};
        var selector = document.getElementById('item_' + rnum);
        items[i].itemID = selector.value;
        items[i].name = selector[selector.selectedIndex].text;
        items[i].desc = document.getElementById('desc_' + rnum).value;
        items[i].color = document.getElementById('color_' + rnum).value;
        if (items[i].color === '' && document.getElementById('color_' + rnum).disabled === false) {
            alert('Please Select A Color for item# ' + (i + 1));
            return;
        }
        items[i].length = document.getElementById('length_' + rnum).value;
        if (items[i].length < 1 && items[i].length !== 'N/A') {
            alert('Please Enter A Length for item# ' + (i + 1));
            return;
        }
        items[i].rate = document.getElementById('rate_' + rnum).value;
        if (affiliate_mode === true) {
            items[i].arate = document.getElementById('arate_' + rnum).value;
            if (items[i].arate === '') {
                alert('Please Enter A Customer Rate (Bottom Price Field) for item# ' + (i + 1));
                return;
            }
        }
        items[i].rate = document.getElementById('rate_' + rnum).value;
        items[i].qty = document.getElementById('qty_' + rnum).value;
        if (items[i].qty < 1 && items[i].qty !== 'N/A') {
            alert('Please Enter A Quantity for item# ' + (i + 1));
            return;
        }
        items[i].price = document.getElementById('price_' + rnum).value;
    }
    items = JSON.stringify(items);
    //items = encodeURL(items);
    params += '&items=' + items;
    console.log(items);

    var freight = document.getElementById('freight').value;
    if ((freight <= 0 || freight === '') && customer_mode === false) {
        var conf = confirm('Is freight needed? Click "Cancel" to add frieght. Click "OK" for no freight.');
        if (conf === true) {
            //Do nothing...
        } else {
            return;
        }
    }
    params += '&freight=' + freight;
    params += '&customer_mode=' + customer_mode;
    params += '&affiliate_mode=' + affiliate_mode;
    params += '&cpMode=' + cpMode;
    //params = encodeURL(params);

    //Check if Invoice is over available credit...
    if (invTotal > credit_line && customer_mode === true && cpMode !== 'Quote') {
        if (affiliate_mode !== true) {
            alert("The total of this invoice exceeds your available credit of $" + credit_line + ". In order to submit this order, you must call and make a payment on your account!");
            return;
        }
    }
    console.log(items);
    $.ajax({
        type: 'POST',
        url: 'php/save-invoice.php',
        data: {
            "mode": mode,
            "rep_id": rep_id,
            "rep_name": rep_name,
            "org_id": org_id,
            "invNum": invNumber,
            "invDate": invDate,
            "cid": cid,
            "sa": sa,
            "taxRate": taxRate,
            "notes": notes,
            "items": items,
            "freight": freight,
            "customer_mode": customer_mode,
            "affiliate_mode": affiliate_mode,
            "cpMode": cpMode
        },
        success: function (data) {
            console.log(data);
            if (data['success'] === true) {
                alert(data['message']);
            } else {
                alert(data['message']);
            }
            if (customer_mode === false) {
                window.location = '../../orders.php';
            } else {
                window.location = '../../../oportal/orders.php';
            }
        }
    });
    /*
    if (window.XMLHttpRequest) {
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
    } else {  // code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
      if (this.readyState==4 && this.status==200) {

              alert(this.responseText);
        //window.close();

      }
    }
    xmlhttp.open("POST","../php/save-invoice.php",true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send(params);*/
}


function get_customer_balance(cid) {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {  // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {

            var r = JSON.parse(this.responseText);
            if (r.response === 'GOOD') {
                if (r.has_credit === 'Yes') {
                    credit_line = r.credit_available;
                } else {
                    credit_line = 0;
                }
                console.log('Credit Line Retrieved...Available Credit: $' + r.credit_available);
            } else {
                console.warn('ERROR: Customer balance was not retrieved...');
            }

        }
    }
    xmlhttp.open("GET", "../php/get-customer-balance.php?cid=" + cid, true);
    xmlhttp.send();
}


