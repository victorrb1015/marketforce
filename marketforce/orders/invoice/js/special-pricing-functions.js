function get_special_pricing(){
  var cid = document.getElementById('customer').value;
  //Get Special Pricing List...
console.log('Getting Special Pricing...');
if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var r = JSON.parse(this.responseText);
      specPricing = r;
      document.getElementById('cemail').innerHTML = specPricing.email;
      document.getElementById('cphone').innerHTML = specPricing.phone;
      delete specPricing.name;
      delete specPricing.email;
      delete specPricing.phone;
      console.log('Special Pricing Loaded.');
      console.log(specPricing);
      
      if(r.taxExempt === 'Yes'){
        exempt_status = true;
        var a = document.createElement('a');
        a.setAttribute('href',r.taxExemptForm);
        a.setAttribute('target','_blank');
        var t = document.createElement('b');
        t.innerHTML = 'EXEMPT -- ';
        a.appendChild(t);
        document.getElementById('exempt').appendChild(a);
        document.getElementById('taxPercent').value = 0;
        document.getElementById('taxPercent').setAttribute('readonly','readonly');
      }else{
        exempt_status = false;
        document.getElementById('taxPercent').removeAttribute('readonly');
        document.getElementById('taxPercent').value = r.taxRate;
        document.getElementById('exempt').innerHTML = '';
      }
      
      verify_pricing();
      
    }
  }
  xmlhttp.open("GET","../php/get-special-pricing.php?cid="+cid,true);
  xmlhttp.send();
}