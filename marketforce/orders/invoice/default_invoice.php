<head>
		<meta charset="utf-8">
    <title>Estimate <?php echo $invNum; ?></title>
		<link rel="stylesheet" href="css/style.css">
		<link rel="license" href="https://www.opensource.org/licenses/mit-license/">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!--<script src="script.js"></script>-->
    <script src="js/globals.js?cb=<?php echo $cache_buster; ?>"></script>
    <script src="js/table-functions.js?cb=<?php echo $cache_buster; ?>"></script>
    <script src="js/item-functions.js?cb=<?php echo $cache_buster; ?>"></script>
    <script src="js/calc-functions.js?cb=<?php echo $cache_buster; ?>"></script>
    <script src="js/special-pricing-functions.js?cb=<?php echo $cache_buster; ?>"></script>
    <script src="js/form-functions.js?cb=<?php echo $cache_buster; ?>"></script>
    <script src="js/edit-functions.js?cb=<?php echo $cache_buster; ?>"></script>
    <script src="js/ship-address-functions.js?cb=<?php echo $cache_buster; ?>"></script>
    
    <?php
    if($_GET['mode'] == 'Edit' || $_GET['mode'] == 'CEdit' || $_GET['mode'] == 'AEdit' || $_GET['mode'] == 'Print' || $_GET['mode'] == 'APrint'){
      /*echo '<script>
             setTimeout(function(){
              edit_invoice("' . $invNum . '","' . $_GET['mode'] . '");
             },6000);
            </script>';*/
      echo '<script>
             function begin_edit(){
              console.warn("Begin Editing");
              edit_invoice("' . $invNum . '","' . $_GET['mode'] . '");
             }
            </script>';
    }else{
      echo '<script>
             function begin_edit(){
              console.log("Not in edit mode...");
             }
            </script>';
    }
    ?>
    <style>
      .preloader-it{
        background: #fff;
        position: fixed;
        top:0;
        left:0;
        z-index: 10001;
        height: 100%;
        width: 100%;
        overflow: hidden;
      }
    </style>
	</head>
	<body style="width: 100%">
    <!-- Preloader -->
    <div id="preloader-it" class="preloader-it" style="background: url('../../global/imgs/loading-gif-block.gif'), rgba(0,0,0,0.7); background-size:contain 50%;background-repeat:no-repeat;background-position:50% 50%;">
      <div class="la-anim-1"></div>
      <img src="../../../../assets/img/white-full-mf-logo.png" style="position:absolute;left:43%;top:5%;width:15%;" />
      <p style="position:absolute;left:45%;top:80%;font-size:50px;font-weight:bold;color:#FFF;">
        Loading...
      </p>
    </div>
    <!-- END Preloader -->
		<header>
			<h1 id="docTitle" style="background:pink;color:#000;">Estimate</h1>
			<address>
				<p><?php echo $_SESSION['org_name']; ?></p>
				<p><?php echo $_SESSION['org_address']; ?><br><?php echo $_SESSION['org_city'] . ', ' . $_SESSION['org_state'] . ' ' . $_SESSION['org_zip']; ?></p>
				<p><?php echo $_SESSION['org_phone']; ?></p>
                <?php
                if($_SESSION['org_id'] == '738004'){
                    echo '<p>Phone: 530 458 5800</p>';
                }?>
			</address>
			<span>
				<img alt="Logo" src="<?php echo $_SESSION['org_logo_url_alt']; ?>" style="width:50%;">
				<!--<input type="file" accept="image/*">-->
			</span>
		</header>
		<article>
			<h1>Recipient</h1>
			<address>
        <select id="customer" name="customer" onchange="get_all_products(this.value);get_special_pricing();get_addresses(this.value);get_customer_balance(this.value);">
          <option value="">Select Customer</option>
          <?php
          $cq = "SELECT * FROM `order_customers` WHERE `inactive` != 'Yes' ORDER BY company_name";
          $cg = mysqli_query($conn, $cq) or die($conn->error);
          while($cr = mysqli_fetch_array($cg)){
            echo '<option value="' . $cr['ID'] . '">' . $cr['company_name'] . '</option>';
          }
          ?>
        </select>
        <br><br>
        <p>
          <span id="cemail"></span>
          <br>
          <span id="cphone"></span>
          <br><br>
          <div id="ship_address_select" style="width:100px;"></div>
        </p>
			</address>
			<table class="meta">
				<tr>
					<th><span id="invNum-title">Estimate # </span></th>
					<td><span id="invNum" <?php echo $ob; ?> <?php echo $ce; ?>><?php echo $invNum; ?></span></td>
				</tr>
				<tr>
					<th><span>Date</span></th>
					<td><span id="invDate"><?php echo $date ?></span></td>
				</tr>
				<tr>
					<th><span>Amount Due</span></th>
					<td><span id="prefix">$</span><span id="amt_due"><?php echo $amtDue; ?></span></td>
				</tr>
			</table>
			<table id="invTable" class="inventory">
				<thead>
					<tr>
						<th style="width:15px;"><span>#</span></th>
						<th><span>Item</span></th>
						<th><span>Description</span></th>
            <th style="width:70px;"><span>Color</span></th>
						<th style="width:35px;"><span>Qty</span></th>
						<th style="width:70px;"><span>Rate</span></th>
            <th style="width:60px;"><span>Length(FT)</span></th>
						<th style="width:70px;"><span>Price</span></th>
						<th style="width:35px;"><span>Wt.</span></th>
					</tr>
				</thead>
				<tbody id="inv_table">
					<!--<tr id="row_1">
						<td>
							<a class="cut" data-rownum="1">-</a>
							<select id="item_1" name="item_1">
								<option value="">Select A Product</option>
							</select>
						</td>
						<td>
							<textarea id="desc_1" name="desc_1"></textarea>
						</td>
						<td>
              <span>$</span>
							<input type="text" class="usd" id="rate_1" name="rate_1" />
						</td>
						<td>
							<input type="text" class="number" id="qty_1" name="qty_1" />
						</td>
						<td>
              <span>$</span>
							<input type="text" class="usd" id="price_1" name="price_1" />
						</td>
					</tr>-->
				</tbody>
			</table>
			<a class="add">+</a>
			<table class="balance">
        <tr>
          <th><span>Weight <span id="weight_warning"></span></span></th>
					<td><span><input type="text" id="total_weight" style="width:50%;text-align:right;" value="" onchange="calc_total();" readonly="readonly"/></span><span data-prefix> lbs</span></td>
				</tr>
        <tr>
					<th><span>Freight</span></th>
					<td><span data-prefix>$</span><span><input type="text" id="freight" style="width:50%;text-align:right;" value="" onchange="calc_total();"/></span></td>
				</tr>
        <tr>
					<th><span>SubTotal</span></th>
					<td><span data-prefix>$</span><span id="subtotal">0.00</span></td>
				</tr>
        <tr>
					<th><span>Tax <input type="text" class="tax" id="taxPercent" style="float:right;width:50%;" placeholder="0.0" onchange="calc_total();"/>%</span></th>
          <td><span id="exempt"></span><span data-prefix>$</span><span id="tax">0.00</span></td>
				</tr>
				<tr>
					<th><span>Total</span></th>
					<td><span data-prefix>$</span><span id="total">0.00</span></td>
				</tr>
				<tr>
					<th><span>Amount Paid</span></th>
					<td><span data-prefix>$</span><span id="amt_paid">0.00</span></td>
				</tr>
				<tr>
					<th><span>Balance Due</span></th>
					<td><span data-prefix>$</span><span id="bal_due">0.00</span></td>
				</tr>
			</table>
		</article>
		<aside>
			<h1><span>Additional Notes</span></h1>
			<div>
				<!--<p>A late fee of 1.5% will be made on unpaid balances after 30 days.</p>-->
                <textarea id="notes" name="notes" placeholder="Click here to add notes..." style="width:100%;min-height:32px;">A late fee of 1.5% will be made on unpaid balances after 30 days.</textarea>
			</div>
		</aside>
    <input type="hidden" id="rep_id" value="<?php echo $_SESSION['user_id']; ?>" />
    <input type="hidden" id="rep_name" value="<?php echo $_SESSION['full_name']; ?>" />
    <input type="hidden" id="org_id" value="<?php echo $_SESSION['org_id']; ?>" />
    <input type="hidden" id="op_rep_id" value="<?php echo $_SESSION['op_user_id']; ?>" />
    <input type="hidden" id="op_rep_name" value="<?php echo $_SESSION['op_full_name']; ?>" />
    <input type="hidden" id="op_org_id" value="<?php echo $_SESSION['op_org_id']; ?>" />
    <div style="width:100%;">
      <button type="button" 
              style="cursor:pointer;float:right;background:green;padding:8px;border-radius:10px;color:white;margin:10px;border:1px solid black;"
              onclick="save_invoice('<?php echo $_GET['mode']; ?>');">
        Submit
      </button>
      <?php
        if($_GET['mode'] == 'CNew' || $_GET['mode'] == 'CEdit' || $_GET['mode'] == 'ANew' || $_GET['mode'] == 'AEdit'){
          echo '<button type="button" 
                        style="cursor:pointer;float:right;background:#E26C70;padding:8px;border-radius:10px;color:white;margin:10px;border:1px solid black;" 
                        title="MarketForce will save this information until you are ready to submit the order."
                        onclick="save_invoice(\'' . $_GET['mode'] . '\',\'Quote\');">
                  Save As Quote
                </button>';
        }
      ?>
    </div>
	</body>