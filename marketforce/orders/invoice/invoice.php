<?php
if($_GET['mode'] == 'CNew' || $_GET['mode'] == 'CEdit'){
  ini_set('session.save_path','/home/marketforce/mf_temp');
}else{
  ini_set('session.save_path','/home/marketforce/mf_temp');
}
session_start();
if($_SERVER['HTTP_HOST'] == 'sandbox.marketforceapp.com'){
	error_reporting(E_ALL);
	$db_host = 'localhost';
	$db_user = 'root';
	$db_pass = 'root';
	$db_mf_db = 'marketfo_marketforce';
	$db_db = $_SESSION['org_db_name'];
}else{
	error_reporting(0);
	$db_host = 'localhost';
	$db_user = 'marketfo_mf';
	$db_pass = '#NgTFJQ!z@t8';
	$db_mf_db = 'marketfo_marketforce';
	$db_db = $_SESSION['org_db_name'];
}

$mf_conn = mysqli_connect($db_host,$db_user,$db_pass,$db_mf_db) or die($mf_conn->error);
$portal_db_name = 'marketfo_mf_oportal';
$pconn = mysqli_connect($db_host,$db_user,$db_pass,$portal_db_name);
if($_SESSION['org_db_name']){
  $conn = mysqli_connect($db_host,$db_user,$db_pass,$db_db) or die($conn->error);
}
if($_GET['mode'] == 'CNew' || $_GET['mode'] == 'ANew'){
   if($_SESSION['op_in'] != 'Yes'){
      echo 'Not Authorized!';
      echo '<script>
            //window.location = "../../../oportal/";
            </script>';
   }
}
$oq = "SELECT * FROM `organizations` WHERE `org_id` = '" . $_GET['org_id'] . "'";
$og = mysqli_query($mf_conn, $oq) or die($conn->error);
if(mysqli_num_rows($og) > 0){
  $or = mysqli_fetch_array($og);
  $_SESSION['org_db_name'] = $or['db_name'];
  $_SESSION['org_logo_url_alt'] = $or['org_logo_url_alt'];
  $_SESSION['org_phone'] = $or['phone'];
  $_SESSION['org_name'] = $or['org_name'];
  $_SESSION['org_address'] = $or['org_address'];
  $_SESSION['org_city'] = $or['org_city'];
  $_SESSION['org_state'] = $or['org_state'];
  $_SESSION['org_zip'] = $or['org_zip'];
  $_SESSION['inv_num_start'] = $or['inv_num_start'];
}else{
	echo 'There was an error opening this invoice/estimate.';
	//break;
}
include '../../php/connection.php';

$invNum = $_GET['inv'];

//Set Affiliate Mode...
if($_GET['mode'] == 'ANew' || $_GET['mode'] == 'AEdit' || $_GET['mode'] == 'APrint'){
  echo '<script>
          var affiliate_mode = true;
        </script>';
}else{
  echo '<script>
          var affiliate_mode = false;
        </script>';
}

if($_GET['mode'] == 'New' || $_GET['mode'] == 'CNew' || $_GET['mode'] == 'ANew'){
  //Get New Invoice Number
  $giq = "SELECT MAX(`inv_num`) AS `new_inv` FROM `order_invoices` WHERE `old_inv_nums` != 'Yes'";
  $gig = mysqli_query($conn, $giq) or die($conn->error);
  $gir = mysqli_fetch_array($gig);
  $inv_num_arr = array();
  if(mysqli_num_rows($gig) > 0){
      $sel = "SELECT inv_num FROM `order_invoices` WHERE `old_inv_nums` != 'Yes' ORDER BY inv_num DESC LIMIT 25";
      $gog = mysqli_query($conn, $sel) or die($conn->error);
      while($gor = mysqli_fetch_array($gog)){
          if (is_numeric($gor['inv_num'])) {
              $inv_num_arr[] = $gor['inv_num'];
          }
      }
      $invNum = max($inv_num_arr) + 1;
    //$invNum = $_SESSION['inv_num_start'];
  }else{
    $invNum = $_SESSION['inv_num_start'];
  }
  //$invNum = rand(100000,999999);

  echo '<script>
          check_inv(' . $invNum . ');
        </script>';
  		
	//$invNum = '190003';
  $date = date("F d, Y");
	$cname = 'Cash Customer';
	$cemail = 'Email:';
	$cphone = 'Phone:';
  $amtDue = 0.00;
}

if($_GET['mode'] == 'Edit' || $_GET['mode'] == 'CEdit' || $_GET['mode'] == 'AEdit'){
      if(!$_GET['inv']){
        echo '<script>
              alert("There was an error loading the invoice!");
              window.location = "../../orders.php";
              </script>';
        //break;
      }
}

if($_GET['mode'] == 'New'){
  $ce = 'contenteditable';
  //$ce = '';
  $ob = '';
}else{
  $ce = '';
  $ob = '';
}
$cache_buster = uniqid();
?>
<html>
  <?
  if($_SESSION['org_id'] == '123456'){
    include 'all_american_invoice.php';
  }else {
    include 'default_invoice.php';
	}?>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script src="../../js/mask/jquery.mask.js"></script>
  <script>
    $(document).ready(function(){
      //$('.date').mask('00/00/0000');
      $('.number').mask('000,000', {reverse:true});
      //$('.tax').mask('00.00');
      //$('.time').mask('00:00:00');
      //$('.phone').mask('(000) 000-0000');
      //$('.phone_us').mask('(000) 000-0000');
      $('.usd').mask('000,000,000,000,000.00', {reverse: true});
      //$( ".date" ).datepicker();
    });
  </script>
  <script src="js/inv-checker.js"></script>
  <?php
  if(($_SESSION['in'] != 'Yes' && $_SESSION['op_in'] != 'Yes') || $_GET['mode'] == 'Print' || $_GET['mode'] == 'APrint'){
  echo '<script src="js/printable.js"></script>';
  }
  ?>
  <script>
    <?php
      if($_GET['mode'] == 'CNew' || $_GET['mode'] == 'ANew'){
        echo 'customer_mode = true;
        document.getElementById("customer").value = ' . $_GET['cid'] . ';
        get_special_pricing();
        get_addresses(' . $_GET['cid'] . ');
        get_customer_balance(' . $_GET['cid'] . ');
        get_all_products(' . $_GET['cid'] . ');
        console.log("Info Loaded...");';
      }
    if($_GET['mode'] == 'CEdit' || $_GET['mode'] == 'AEdit'){
        echo 'customer_mode = true;
        get_customer_balance(' . $_GET['cid'] . ');
        get_all_products(' . $_GET['cid'] . ');';
      }
    ?>
    </script>
<?php
if($_GET['mode'] == 'CNew' || $_GET['mode'] == 'CEdit' || $_GET['mode'] == 'ANew' || $_GET['mode'] == 'AEdit'){
  echo '<script src="js/customer-mode.js?cb=' . $cache_buster . '"></script>';
}
?>
</html>