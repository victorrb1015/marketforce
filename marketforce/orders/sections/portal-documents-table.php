<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary card-view">
      <div class="panel-heading">
        <h1 class="panel-title">Portal Documents <small>(Available to All Affiliate Portal Users)</small> &nbsp; 
          <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#newPortalDocModal" onclick="pu_load_doc_modal('New');"><i class="fa fa-plus"></i> Add Portal Doc</button>
        </h1>
      </div>
      <div class="panel-body table-responsive" style="height:auto;">
        <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr class="info">
              <th>Date</th>
              <th>Document Name</th>
              <th>Document Description</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
        <?php
          $plq = "SELECT * FROM `portal_documents` WHERE `inactive` != 'Yes' AND `cid` = 'ALL'";
          $plg = mysqli_query($pconn, $plq) or die($pconn->error);
          while($plr = mysqli_fetch_array($plg)){
            echo '<tr>
              <td>' . date("m/d/y", strtotime($plr['date'])) . '</td>
              <td><strong>' . $plr['doc_name'] . '</strong></td>
              <td>' . $plr['doc_description'] . '</td>
              <td>
                <div class="btn-group" role="group">
                  <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#newPortalUserModal" onclick="window.open(\'' . $plr['doc_url'] . '\',\'_blank\');">View</button>
                  <button class="btn btn-danger" type="button" onclick="remove_portal_user(' . $plr['ID'] . ');">Remove</button>
                </div>
              </td>
            </tr>';
          }
       ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>