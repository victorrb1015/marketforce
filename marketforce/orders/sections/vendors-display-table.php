<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary card-view">
      <div class="panel-heading">
        <h1 class="panel-title">Vendors &nbsp; 
          <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#newVendorModal" onclick="new_vendor();"><i class="fa fa-plus"></i> Add Vendor</button>
        </h1>
      </div>
      <div class="panel-body table-responsive" style="height:auto;">
        <table id="vendors_table" class="table table-bordered table-hover table-striped">
          <thead>
            <tr class="info">
              <th style="font-size:18px;">Vendor Name</th>
              <th style="font-size:18px;">Contact</th>
              <th style="font-size:18px;">Phone #</th>
              <th style="font-size:18px;">Email</th>
              <th style="font-size:18px;">Notes</th>
              <!--<th style="font-size:18px;">Category</th>-->
              <th style="font-size: 18px;width: 191px;">Actions</th>
            </tr>
          </thead>
          <tbody id="vendors_tbody">
        <?php
          $vq = "SELECT * FROM `vendors` WHERE `inactive` != 'Yes'";
          $vg = mysqli_query($conn, $vq) or die($conn->error);
          while($vr = mysqli_fetch_array($vg)){
            echo '<tr id="vendor_row_' . $vr['ID'] . '">
                    <td style="color:white;">' . $vr['vendor_name'] . '</td>
                    <td style="color:white;">' . $vr['contact_name'] . '</td>
                    <td style="color:white;">' . $vr['vendor_phone'] . '</td>
                    <td style="color:white;">' . $vr['vendor_email'] . '</td>
                    <td style="color:white;">' . $vr['notes'] . '</td>
                    <!--<td style="color:white;"><strong>' . $vr['category'] . '</strong></td>-->
                    <td>
                      <div class="btn-group" role="group">
                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#newVendorModal" onclick="edit_vendor(' . $vr['ID'] . ');">Edit</button>
                        <button class="btn btn-danger" type="button" onclick="remove_vendor(' . $vr['ID'] . ');">Deactivate</button>
                        <button class="btn btn-warning" type="button"><a href="mailto:' . $vr['vendor_email'] . '">Email Vendor</a></button>
                      </div>
                    </td>
                  </tr>';
          }
       ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>