

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary card-view">
            <div class="panel-heading">
                <h1 class="panel-title">Orders Out of Time &nbsp;
                </h1>
            </div>
            <div class="panel-body table-responsive" style="height:auto;">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr class="info">
                        <th style="font-size:18px;">Date</th>
                        <th style="font-size:18px;">User</th>
                        <th style="font-size:18px;">Customer</th>
                        <th style="font-size: 18px;width: 175px;">Order Info</th>
                        <th style="font-size: 18px;">Status</th>
                        <th style="font-size:18px;">Notes</th>
                        <th style="font-size: 18px;width: 216px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $oq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `status` = 'out_time' ORDER BY `date` DESC";
                    $og = mysqli_query($conn, $oq) or die($conn->error);
                    while($or = mysqli_fetch_array($og)){
                        $oecq = "SELECT * FROM `order_invoices` WHERE `inv_num` = '" . $or['inv'] . "'";
                        $oecg = mysqli_query($conn, $oecq) or die($conn->error);
                        $oecr = mysqli_fetch_array($oecg);



                            if($or['on_hold'] == 'Yes'){
                                $style = 'style="background:orange;"';
                            }else{
                                if($oecr['exported'] == 'Yes'){
                                    $style = 'style="background:lightgreen;"';
                                }else{
                                    $style = '';
                                }
                            }


                        echo '<tr ' . $style . '>
                                    <td>
																			' . date("m/d/y",strtotime($oecr['date'])) . '
																			<br>
																			<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#newOrderDateModal" onclick="document.getElementById(\'new_order_date_id\').value = \'' . $or['inv'] . '\';">
																				<i class="fa fa-calendar"></i> Change Date
																			</button>
																		</td>
                                    <td>' . $or['rep_name'] . '</td>
                                    <td>' . $or['cname'] . '</td>
                                    <td>';
                        if($or['inv'] != ''){
                            echo '<p>
																			Inv#: ' . $or['inv'] . '&nbsp;&nbsp;&nbsp;
																			<br>
																			<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#newInvNumberModal" onclick="document.getElementById(\'new_inv_number_id\').value = \'' . $or['inv'] . '\';">
																				<i class="fas fa-hashtag"></i> Change Inv Number
																			</button>
																		</p>';
                        }

                        //Get Invoice Total...
                        $itq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $or['inv'] . "'";
                        $itg = mysqli_query($conn, $itq) or die($conn->error);
                        $itotal = 0;
                        $trq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $iecr['cid'] . "'";
                        $trg = mysqli_query($conn, $trq) or die($conn->error);
                        $trr = mysqli_fetch_array($trg);
                        $tr = ($trr['tax_rate'] / 100);
                        while($itr = mysqli_fetch_array($itg)){
                            $itotal = $itotal + $itr['item_price'];
                        }
                        if($trr['taxform_url'] == ''){
                            $itotal = $itotal * (1 + $tr);
                        }
                        //Add Freight...
                        $itotal = $itotal + $iecr['freight'];

                        echo '<p>Amount: $' . number_format($itotal,2) . '</p>';

                        if($or['file_url'] != '' && $or['status'] != 'Estimate'){
                            echo '<br><br>
                                      <a href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/orders/invoice/work-order.php?org_id=' . $_SESSION['org_id'] . '&inv=' . $or['inv'] . '" target="_blank">
                                        <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View Work Order</button>
                                      </a>';
                        }
                        if($or['inv_file_url'] != '' && $or['status'] == 'Estimate'){
                            echo '<br><br>
                                      <a href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/orders/invoice/invoice.php?org_id=' . $_SESSION['org_id'] . '&mode=Print&inv=' . $or['inv'] . '" target="_blank">
                                        <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View Estimate</button>
                                      </a>';
                        }else if($or['inv_file_url'] != '' && $or['status'] != 'Estimate'){
                            echo '<br><br>
                                      <a href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/orders/invoice/invoice.php?org_id=' . $_SESSION['org_id'] . '&mode=Print&inv=' . $or['inv'] . '" target="_blank">
                                        <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View Invoice</button>
                                      </a>';
                        }
                        if($or['bol_file_url'] != '' && $or['status'] != 'Estimate'){
                            echo '<br><br>
                                      <a href="http://' . $_SERVER['HTTP_HOST'] . '/marketforce/orders/invoice/bol.php?org_id=' . $_SESSION['org_id'] . '&inv=' . $or['inv'] . '" target="_blank">
                                        <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View Bill of Lading</button>
                                      </a>';
                        }

                        echo '</td>
                                    <td>
                                      <div class="note-box" style="max-height:200px;width:500px;margin:auto;">
                                        <div class="row bs-wizard" style="border-bottom:0;text-align:center;">';

                        if($or['status'] == 'Estimate'){
                            echo '<div class="col-12 alert alert-warning" style="height:100px;background:pink;color:#000;font-weight:bold;font-size:4rem;">
                            ESTIMATE
                          </div>';
                        }else{
                            if($or['rec_order'] == 'Yes'){
                                $rec_order = 'complete';
                                $rec_order_date = date("m/d/y", strtotime($or['rec_order_date']));
                            }else{
                                $rec_order = 'disabled';
                                $rec_order_date = '';
                            }

                            echo '<div class="col-xs-2 bs-wizard-step ' . $rec_order . '">
                                            <div class="text-center bs-wizard-stepnum">Rec. Order</div>
                                            <div class="progress"><div class="progress-bar"></div></div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            <div class="bs-wizard-info text-center">' . $rec_order_date . '</div>
                                          </div>';

                            if($or['in_shop'] == 'Yes'){
                                $in_shop = 'complete';
                                $in_shop_date = date("m/d/y", strtotime($or['in_shop_date']));
                            }else{
                                $in_shop = 'disabled';
                                $in_shop_date = '';
                            }

                            echo '<div class="col-xs-2 bs-wizard-step ' . $in_shop . '"><!-- complete -->
                                            <div class="text-center bs-wizard-stepnum">In-Shop</div>
                                            <div class="progress"><div class="progress-bar"></div></div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            <div class="bs-wizard-info text-center">' . $in_shop_date . '</div>
                                          </div>';

                            if($or['shop_done'] == 'Yes'){
                                $shop_done = 'complete';
                                $shop_done_date = date("m/d/y", strtotime($or['shop_done_date']));
                            }else{
                                $shop_done = 'disabled';
                                $shop_done_date = '';
                            }

                            echo '<div class="col-xs-2 bs-wizard-step ' . $shop_done . '"><!-- complete -->
                                            <div class="text-center bs-wizard-stepnum">Shop-Done</div>
                                            <div class="progress"><div class="progress-bar"></div></div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            <div class="bs-wizard-info text-center">' . $shop_done_date . '</div>
                                          </div>';

                            if($or['shipped'] == 'Yes'){
                                $shipped = 'complete';
                                $shipped_date = date("m/d/y", strtotime($or['shipped_date']));
                            }else{
                                $shipped = 'disabled';
                                $shipped_date = '';
                            }

                            echo '<div class="col-xs-2 bs-wizard-step ' . $shipped . '"><!-- active -->
                                            <div class="text-center bs-wizard-stepnum">Shipped</div>
                                            <div class="progress"><div class="progress-bar"></div></div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            <div class="bs-wizard-info text-center">' . $shipped_date . '</div>
                                          </div>';

                            if($or['invoiced'] == 'Yes'){
                                $invoiced = 'complete';
                                $invoiced_date = date("m/d/y", strtotime($or['invoiced_date']));
                            }else{
                                $invoiced = 'disabled';
                                $invoiced_date = '';
                            }

                            echo '<div class="col-xs-2 bs-wizard-step ' . $invoiced . '"><!-- active -->
                                            <div class="text-center bs-wizard-stepnum">Invoiced</div>
                                            <div class="progress"><div class="progress-bar"></div></div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            <div class="bs-wizard-info text-center">' . $invoiced_date . '</div>
                                          </div>';

                            if($or['paid'] == 'Yes'){
                                $paid = 'complete';
                                $paid_date = date("m/d/y", strtotime($or['paid_date']));
                                $paid_time = date("h:i A", strtotime($or['paid_time']));
                            }else{
                                $paid = 'disabled';
                                $paid_date = '';
                                $paid_time = '';
                            }

                            echo '<div class="col-xs-2 bs-wizard-step ' . $paid . '"><!-- active -->
                                            <div class="text-center bs-wizard-stepnum">Paid</div>
                                            <div class="progress"><div class="progress-bar"></div></div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            <div class="bs-wizard-info text-center"data-toggle="tooltip" data-placement="bottom" title="' . $paid_time . '">' . $paid_date . '</div>
                                          </div>';

                        }//END If Estimate...

                        echo '</div>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="note-box" style="max-height:200px;width:400px;margin:auto;overflow:scroll;">';
                        if($or['notes'] != ''){
                            echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
                                                <i class="far fa-comments"></i> Initial Notes ' . date("m/d/y",strtotime($or['date'])) . ' ' . date("h:i A",strtotime($or['time'])) . '
                                              </span></h4>
                                                <div class="well well-sm">
                                                ' . $or['notes'] . '
                                              </div>';
                        }
                        $onq = "SELECT * FROM `order_notes` WHERE `inactive` != 'Yes' AND `order_id` = '" . $or['ID'] . "' ORDER BY `ID` DESC";
                        $ong = mysqli_query($conn, $onq) or die($conn->error);
                        while($onr = mysqli_fetch_array($ong)){
                            echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="far fa-comments"></i> ' . $onr['rep_name'] . ' ' . date("m/d/y",strtotime($onr['date'])) . ' ' . date("h:i A",strtotime($onr['time'])) . '
													</span></h4>
														<div class="well well-sm">
														' . $onr['note'] . '
													</div>';
                        }
                        echo '</div>
                                    </td>
                                    <td style="color:rgb(213,19,7);">
                                      <button class="btn btn-primary btn-sm" type="button" style="margin:2px;" data-toggle="modal" data-target="#addNoteModal" onclick="add_note(' . $or['ID'] . ');"><i class="far fa-comments"></i> Add Note</button>
                                      <!--<button class="btn btn-default btn-sm" type="button" style="margin:2px;" data-toggle="modal" data-target="#newOrderModal" onclick="get_order_details(' . $or['ID'] . ');"><i class="fas fa-edit"></i> Edit</button>-->
                                      <a href="orders/invoice/invoice.php?org_id=' . $_SESSION['org_id'] . '&mode=Edit&inv=' . $or['inv'] . '"><button class="btn btn-default btn-sm" type="button" style="margin:2px;color:black !important;"><i class="fas fa-edit"></i> Edit</button></a>';


                        if($or['on_hold'] == 'Yes'){
                            echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px; background-color:#dd4910" onclick="change_status(' . $or['ID'] . ',\'in_process\');"><i class="fa fa-check-square-o"></i> Send To Process</button>';
                        }else{
                            echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px; background-color:#dd4910" onclick="change_status(' . $or['ID'] . ',\'on_hold\');"><i class="fa fa-check-square-o"></i> On Hold</button>';
                            
                            if($or['status'] == 'out_time'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'Estimate\');"><i class="fa fa-file"></i> Estimate</button>';
                            }
                            if($or['status'] == 'Estimate'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'rec_order\');"><i class="fa fa-file"></i> Make Invoice</button>';
                            }
                            if($or['status'] == 'rec_order'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'in_shop\');"><i class="fa fa-wrench"></i> On Shop</button>';
                            }
                            if($or['status'] == 'in_shop'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'shop_done\');"><i class="fa fa-wrench"></i> <i class="fa fa-check"></i> Shop Done</button>';
                            }
                            if($or['status'] == 'shop_done'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'shipped\');"><i class="fa fa-truck"></i> Ship Order</button>';
                            }
                            if($or['status'] == 'shipped'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'invoiced\');"><i class="fa fa-usd"></i> Invoice</button>';
                            }
                            if($or['status'] == 'invoiced'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'paid\');"><i class="fa fa-check-square-o"></i> Mark Paid</button>';
                            }

                        }



                        if($or['status'] != 'Estimate'){
                            echo '<button class="btn btn-warning btn-sm" type="button" style="margin:2px;" data-toggle="modal" data-target="#infoBox" onclick="view_order_info(' . $or['ID'] . ');"><i class="fa fa-info-circle"></i> Info</button>';
                        }

                        if($or['status'] == 'Estimate'){
                            echo '<button class="btn btn-warning btn-sm" type="button" style="margin:2px;color:#000;background:pink;" onclick="send_customer_estimate(' . $or['inv'] . ');"><i class="fa fa-envelope"></i> Send EST</button>';
                        }else{
                            echo '<button class="btn btn-warning btn-sm" type="button" style="margin:2px;color:white;background:black;" onclick="send_customer_invoice(' . $or['inv'] . ');"><i class="fa fa-envelope"></i> Send INV</button>';
                        }

                        echo '<button class="btn btn-danger btn-sm" type="button" style="margin:2px;" onclick="delete_order(' . $or['ID'] . ');"><i class="fa fa-trash"></i> Delete</button>
                                    </td>
                                </tr>';
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>