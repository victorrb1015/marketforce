<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary card-view">
      <div class="panel-heading">
        <h1 class="panel-title">Customer Display Table &nbsp; 
          <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#newCustomerModal" onclick="new_customer();"><i class="fa fa-plus"></i> New Customer</button>
        </h1>
      </div>
      <div class="panel-body table-responsive" style="height:auto;">
        <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr class="info">
              <th style="font-size:18px;">Date</th>
              <th style="font-size:18px;">Company</th>
              <th style="font-size:18px;">Owner</th>
              <th style="font-size:18px;">Email</th>
              <th style="font-size:18px;">Phone Number</th>
              <th style="font-size:18px;">Auth. Users</th>
              <th style="font-size:18px;">Notes</th>
              <th style="font-size: 18px;width: 144px;">Actions</th>
            </tr>
          </thead>
          <tbody>
         <?php
         $cq = "SELECT * FROM `order_customers` WHERE `inactive` != 'Yes' ORDER BY company_name";
         $cg = mysqli_query($conn, $cq) or die($conn->error);
         while($cr = mysqli_fetch_array($cg)){
            echo '<tr>
              <td>' . date("m/d/y", strtotime($cr['date'])) . '</td>
              <td style="color: rgb(47,112,169);"><strong>' . $cr['company_name'] . '</strong></td>
              <td>' . $cr['customer_first_name'] . ' ' . $cr['customer_last_name'] . '</td>
              <td>' . $cr['customer_email'] . '</td>
              <td>' . $cr['customer_phone'] . '</td>
              <td>' . $cr['auth_users'] . '</td>
              <td>' . $cr['customer_notes'] . '</td>
              <td>
                <div class="btn-group" role="group">
                  <button class="btn btn-default" type="button" data-toggle="modal" data-target="#newCustomerModal" onclick="edit_customer(' . $cr['ID'] . ');">Edit</button>
                  <button class="btn btn-success" type="button" data-toggle="modal" data-target="#specialPricing" onclick="edit_special_pricing(' . $cr['ID'] . ');">Special Pricing</button>
                  <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#addShipAddressModal" onclick="load_ship_modal(' . $cr['ID'] . ');">Ship Address</button>
                  <button class="btn btn-danger" type="button" onclick="remove_customer(' . $cr['ID'] . ');">Delete</button>
                </div>
              </td>
            </tr>';
         }
         ?>
            <tr></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>