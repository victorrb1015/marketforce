<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary card-view">
      <div class="panel-heading">
        <h1 class="panel-title">Reports</h1>
      </div>
      <div class="panel-body table-responsive" style="height:auto;">
        <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr class="info">
              <th>Report Name</th>
							<th>Report Description</th>
							<th>View</th>
            </tr>
          </thead>
          <tbody>
            
            <tr>
              <td><b>Special Pricing Report</b></td>
							<td>This report displays all products with special prices set</td>
							<td>
                <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/orders/order-reports/special-pricing-report.php" target="_blank">
                <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
                </a>
							</td>
            </tr>
						
						<tr>
              <td><b>Sales By Customer Report</b></td>
							<td>This report displays the sales for all customers</td>
							<td>
                <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/orders/order-reports/customer-sales-report.php" target="_blank">
                <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
                </a>
							</td>
            </tr>
            
            <tr>
              <td><b>Sales By Unit Report</b></td>
							<td>This report displays the units sold for all products</td>
							<td>
                <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/orders/order-reports/sales-report.php" target="_blank">
                <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
                </a>
							</td>
            </tr>
            
            <tr>
              <td><b>Pending Affiliate Payments Report</b></td>
							<td>This report displays all Pending Payments for Affiliate Customers</td>
							<td>
                <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/orders/order-reports/pending-affiliate-payments-report.php" target="_blank">
                <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
                </a>
							</td>
            </tr>
            
            <tr>
              <td><b>Affiliate Sales Report</b></td>
							<td>This report displays all Sales for Affiliate Customers</td>
							<td>
                <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/orders/order-reports/affiliate-sales-report.php" target="_blank">
                <span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
                </a>
							</td>
            </tr>
            
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>