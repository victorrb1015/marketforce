<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary card-view">
      <div class="panel-heading">
      <h1 class="panel-title">Search Orders</h1>
    </div>
    <br />
    <div class="container">
      <div class="row">
        <div class="col-8">
          <div class="card">
            <div class="card-body">
              <div class="container">
                <form action="orders/php/search-order.php" method="post">
                  <div class="row">
                    <div class="col">
                      <input type="text" class="form-control" name="inv_number" placeholder="invoice numbers separated by commas (123, 123, ...)">
                    </div>
                    <br />
                    <button class="btn btn-primary" type="submit">Search</button>
                    <br />
                  </div>
                </form> 
              </div>
            </div>                    
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>