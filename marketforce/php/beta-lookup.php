<?php
include 'connection.php';

//Load Variables
$str = mysqli_real_escape_string($conn, $_GET['str']);
$user = $_GET['user'];

echo '<table class="result_table">
      <tr>
      <th>Name</th>
      <th>State</th>
      <th>View Notes</th>
      <th>Add Internal Note</td>
      <th>View Images</th>
      <th>View Forms</th>
      <th>Edit Dealer</th>
      <th>Update Signs</th>
      <th id="hider-th">Deactivate</th>
      <th>OSTR</th>
      </tr>';
$q = "SELECT * FROM `dealers` WHERE `name` LIKE '%" . $str . "%' ORDER BY `name` ASC";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  if($r['inactive'] != 'Yes'){
  echo '<tr>
  <td>' . $r['name'] . '</td>
  <td>' . $r['city'] . ', ' . $r['state'] . '</td>
  <td style="text-align:center;"><h3><a href="dealer-lookup.php?id=' . $r['ID'] . '&lookup=yes"><i class="fa fa-eye"></i></a></h3></td>
  <td style="text-align:center;"><a onclick="load_modal(' . $r['ID'] . ',\'' . $r['name'] . '\',\'int\');" data-toggle="modal" data-target="#intNote"><h3 style="color:blue;"><i class="fa fa-comments-o"></i></h3></a></td>';
$iq = "SELECT `ID` FROM `dealer_imgs` WHERE `dealer_id` = '" . $r['ID'] . "'";
$ig = mysqli_query($conn, $iq) or die($conn->error);
if($rn = mysqli_num_rows($ig) != 0){
  echo '<td style="text-align:center;"><h3><a href="php/get-images.php?id=' . $r['ID'] . '" target="_blank"><i class=" fa fa-picture-o"></i></a></h3></td>';
}else{
  echo '<td style="text-align:center;"><h3 style="color:gray;"><i class=" fa fa-picture-o"></i></h3></td>';

}
  echo '<td style="text-align:center;"><h3><a href="print-forms.php?ider=' . $r['ID'] . '"><i class="fa fa-list-alt"></i></a></h3></td>
  <td style="text-align:center;"><h3><a href="http://marketforceapp.com/marketforce/forms/editing_forms/index.php?id=' . $r['ID'] . '&user=' . $user . '" target="_blank"><i class="fa fa-pencil"></i></a></h3></td>';
    if($r['new_signs'] == 'Done'){
      echo '<td style="text-align:center;color:black;"><button type="button" class="btn btn-success btn-sm" onclick="complete_signs(' . $r['ID'] . ',\'Not Done\');"><i class="fa fa-thumbs-up fa-2x"></i></button></td>';
    }else{
      echo '<td style="text-align:center;color:black;"><button type="button" class="btn btn-danger btn-sm" onclick="complete_signs(' . $r['ID'] . ',\'Done\');"><i class="fa fa-thumbs-down fa-2x"></i></button></td>';
    }
  echo '<td id="hider-td" style="text-align:center;"><h3><a href="http://marketforceapp.com/marketforce/reports/deactivate_dealer.php?id=' . $r['ID'] . '" target="_blank"><i class="fa fa-ban"></i></a></h3></td>
  </tr>';
    
  }else{//if inactive
  
  echo '<tr>
  <td style="color:red;">' . $r['name'] . '<br>{INACTIVE DEALER}</td>
  <td style="color:red;">' . $r['city'] . ', ' . $r['state'] . '</td>
  <td style="text-align:center;"><h3><a href="dealer-lookup.php?id=' . $r['ID'] . '&lookup=yes"><i class="fa fa-eye"></i></a></h3></td>
  <td style="text-align:center;"><a onclick="load_modal(' . $r['ID'] . ',\'' . $r['name'] . '\',\'int\');" data-toggle="modal" data-target="#intNote"><h3 style="color:blue;"><i class="fa fa-comments-o"></i></h3></a></td>';
$iq = "SELECT `ID` FROM `dealer_imgs` WHERE `dealer_id` = '" . $r['ID'] . "'";
$ig = mysqli_query($conn, $iq) or die($conn->error);
if($rn = mysqli_num_rows($ig) != 0){
  echo '<td style="text-align:center;"><h3><a href="php/get-images.php?id=' . $r['ID'] . '" target="_blank"><i class=" fa fa-picture-o"></i></a></h3></td>';
}else{
  echo '<td style="text-align:center;"><h3 style="color:gray;"><i class=" fa fa-picture-o"></i></h3></td>';

}
  echo '<td style="text-align:center;"><h3><a href="print-forms.php?ider=' . $r['ID'] . '"><i class="fa fa-list-alt"></i></a></h3></td>
  <td style="text-align:center;"><h3><a href="http://marketforceapp.com/marketforce/forms/editing_forms/index.php?id=' . $r['ID'] . '&user=' . $user . '" target="_blank"><i class="fa fa-pencil"></i></a></h3></td>';
  echo '<td style="text-align:center;"><h3 style="color:gray;"><i class="fa fa-ban"></i></h3></td>';
  echo '<td id="hider-td" style="text-align:center;"><h3><a href="http://marketforceapp.com/marketforce/reports/deactivate_dealer.php?id=' . $r['ID'] . '" target="_blank"><i class="fa fa-ban"></i></a></h3></td>';
    
  //The Rep Assignment for Dealer Map...
    $rnq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $r['ID'] . "'";
    $rng = mysqli_query($conn, $rnq) or die($conn->error);
    $rnr = mysqli_fetch_array($rng);
    
  echo '<td>' . $rnr['user'] . '</td>';
    
  echo '</tr>';
  //Old Inactive Lookup (All not functional except view forms).
    /* echo '<tr>
  <td style="color:red;">' . $r['name'] . '<br>{INACTIVE DEALER}</td>
  <td style="color:red;">' . $r['city'] . ', ' . $r['state'] . '</td>
  <td style="text-align:center;"><h3 style="color:gray;"><i class="fa fa-eye"></i></h3></td>
  <td style="text-align:center;"><h3 style="color:gray;"><i class="fa fa-comments-o"></i></h3></td>
  <td style="text-align:center;"><h3 style="color:gray;"><i class=" fa fa-picture-o"></i></h3></td>
  <!--<td style="text-align:center;"><h3 style="color:gray;"><i class="fa fa-list-alt"></i></h3></td>-->
  <td style="text-align:center;"><h3><a href="print-forms.php?ider=' . $r['ID'] . '"><i class="fa fa-list-alt"></i></a></h3></td>
  <td style="text-align:center;"><h3 style="color:gray;"><i class="fa fa-pencil"></i></h3></td>
  <td style="text-align:center;"><h3 style="color:gray;"><i class="fa fa-ban"></i></h3></td>
  <td id="hider-td" style="text-align:center;"><h3 style="color:gray;"><i class="fa fa-ban"></i></h3></td>
  </tr>';*/
  }//End if inactive...
}
echo '</table>';

?>