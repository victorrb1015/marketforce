<?php

//System Automation for Dealer Ownership Changes
  if($ir['form_type'] == 'Ownership Change Form'){
    
    //Get dealer information from new_dealer_form database
    $diq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $pid . "'";
    $dig = mysqli_query($conn, $diq) or die($conn->error);
    $dir = mysqli_fetch_array($dig);
    
    //Get Ownership Change information from database
    $ocq = "SELECT * FROM `ownership_changes` WHERE `did` = '" . $pid . "'";
    $ocg = mysqli_query($conn, $ocq) or die($conn->error);
    $ocr = mysqli_fetch_array($ocg);
    
    
    //Convert UID to Rep Name
      $repnameq = "SELECT * FROM `users` WHERE `ID` = '" . $ocr['uid'] . "'";
      $repnameg = mysqli_query($conn, $repnameq) or die($conn->error);
      $repnamer = mysqli_fetch_array($repnameg);
        $full_name = $repnamer['fname'] . ' ' . $repnamer['lname'];
    
    //Make old Dealer INACTIVE
    $rdq = "UPDATE `dealers` SET `inactive` = 'Yes' WHERE `ID` = '" . $ir['dealer_id'] . "'";
    mysqli_query($conn, $rdq) or die($conn->error);
    $rdq2 = "UPDATE `new_dealer_form` SET `inactive` = 'Yes' WHERE `ID` = '" . $ir['dealer_id'] . "'";
    mysqli_query($conn, $rdq2) or die($conn->error);
    
    
    //Insert into the main Dealer list for note additions
$xxq = "INSERT INTO `dealers` (`name`,`address`,`city`,`state`,`zip`,`phone`,`fax`,`email`,`status`)
				VALUES
				('" . $ocr['newName'] . "',
				 '" . $ocr['newAddress'] . "',
				 '" . $ocr['newCity'] . "',
				 '" . $ocr['newState'] . "',
				 '" . $ocr['newZip'] . "',
				 '" . $ocr['newPhone'] . "',
				 '" . $ocr['newFax'] . "',
				 '" . $ocr['newEmail'] . "',
				 'New')";
$xxg = mysqli_query($conn, $xxq) or die($conn->error);

//Get Submission ID
$idq = "SELECT * FROM `dealers` ORDER BY `ID` DESC LIMIT 1";
$idg = mysqli_query($conn, $idq) or die($conn->error);
$idr = mysqli_fetch_array($idg);
    

//INSERT DATA INTO DATABASE
$i = "INSERT
      INTO
      `new_dealer_form`
      (`ID`,`user`,`date`,`time`,`bizname`,`address`,`city`,`state`,`zip`,`phone`,`fax`,`maddress`,`mcity`,`mstate`,`mzip`,`property`,
       `expiration`,`email`,`website`,`years`,`numbers`,`EIN`,`name1`,`email1`,`name2`,`email2`,
       `fulltime`,`parttime`,`ownername`,`owneraddress`,`ownercity`,`ownerstate`,`ownerzip`,`ownerphone`,`altphone`,`mainroad`,`visibility`,`displayspace`,
       `currentcompany`,`who`,`salesarea`,`where`,`otherprod`,`closedealers`,`rto`,`bbb`,`mantra`)
       VALUES
       ('" . $idr['ID'] . "',
			 '" . $full_name . "',
        CURRENT_DATE,
        CURRENT_TIME,
        '" . $ocr['newName'] . "',
				'" . $ocr['newAddress'] . "',
				'" . $ocr['newCity'] . "',
				'" . $ocr['newState'] . "',
				'" . $ocr['newZip'] . "',
				'" . $ocr['newPhone'] . "',
				'" . $ocr['newFax'] . "',
				'',
				'',
				'',
				'',
        '" . $dir['property'] . "',
        '" . $dir['expiration'] . "',
				'" . $ocr['newEmail'] . "',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '" . $ocr['newOwner'] . "',
        '',
				'',
				'',
				'',
        '',
        '',
        '" . $dir['mainroad'] . "',
        '" . $dir['visibility'] . "',
        '" . $dir['displayspace'] . "',
        '',
        '',
        '" . $dir['salesarea'] . "',
        '" . $dir['where'] . "',
        '" . $dir['otherprod'] . "',
        '" . $dir['closedealers'] . "',
        '',
        '',
        '')";

$ig = mysqli_query($conn, $i) or die($conn->error);



//Insert into the Display Orders database
$dofq = "INSERT INTO `display_orders`
			 (`ID`,
			 `tcg`,
       `tcg_v1`,
       `tcg_v2`,
       `tcg_v3`,
       `tcg_v4`,
       `tcg_v5`,
       `tcg_v6`,
       `tcg_price`,
       `g`,
       `g_v1`,
       `g_v2`,
       `g_v3`,
       `g_v4`,
       `g_v5`,
       `g_price`,
       `cua`,
       `cua_v1`,
       `cua_v2`,
       `cua_v3`,
       `cua_v4`,
       `cua_price`,
       `cur`,
       `cur_v1`,
       `cur_v2`,
       `cur_v3`,
       `cur_v4`,
       `cur_price`,
       `rvp`,
       `rvp_v1`,
       `rvp_v2`,
       `rvp_v3`,
       `rvp_v4`,
       `rvp_v5`,
       `rvp_price`,
       `cst`,
       `cst_v1`,
       `cst_v2`,
       `cst_v3`,
       `cst_price`,
       `csp`,
       `csp_v1`,
       `csp_v2`,
       `csp_v3`,
       `csp_v4`,
       `csp_v5`,
       `csp_price`,
       `vb`,
       `vb_v1`,
       `vb_v2`,
       `vb_v3`,
       `vb_v4`,
       `vb_v5`,
       `vb_price`,
       `u`,
       `u_v1`,
       `u_v2`,
       `u_v3`,
       `u_v4`,
       `u_v5`,
       `u_price`,
       `au`,
       `au_v1`, 
       `au_v2`, 
       `au_v3`, 
       `au_v4`,
       `au_price`, 
       `us`,
       `us_v1`, 
       `us_v2`, 
       `us_v3`, 
       `us_v4`,
       `us_price`, 
       `vu`,
       `vu_v1`, 
       `vu_v2`, 
       `vu_v3`,
       `vu_v4`,
       `vu_price`)
			 VALUES
			 ('" . $idr['ID'] . "',
			 'false','22','26','8','2','1','1','5800.00',
			 	'false','18','26','8','1','1','4400.00',
				'false','18','31','7','1','3250.00',
				'false','18','31','6','1','3100.00',
				'false','12','31','12','2','6','2400.00',
				'false','18','21','6','800.00',
				'false','24','21','7','1','1','2000.00',
				'false','20','21','9','1','1','5265.00',
				'false','12','20','7','1','1','4000.00',
				'false','10','12','7','1','2095.00',
				'false','8','12','6','1','1695.00',
				'false','10','12','7','1','2595.00')";
$dofg = mysqli_query($conn, $dofq) or die($conn->error);

//Create Slot for E-Signatures
$bit = '{"lines":[]}';
$esq = "INSERT INTO `signatures` (`ID`,`dealer_id`,`rsign`,`dsign`,`lsign`) VALUES ('" . $idr['ID'] . "','" . $idr['ID'] . "','" . $bit . "','" . $bit . "','" . $bit . "')";
$esg = mysqli_query($conn, $esq) or die($conn->error);
    
  }

?>