<?php
include 'connection.php';

//Load Variables
$bc = $_GET['bc'];

//Check if this is an update query
if(isset($_GET['s'])){
//Load Variables
  $ID = $_GET['ID'];
  $barcode = $_GET['barcode'];
  $title = $_GET['title'];
  $afn = $_GET['afn'];
  $aln = $_GET['aln'];
  $genre = $_GET['genre'];
  $age = $_GET['age'];
  $loc = $_GET['location'];
  $shelf = $_GET['shelf'];
  $mat = $_GET['material'];
  $cost = $_GET['cost'];
  $status = $_GET['status'];
  $cond = $_GET['condition'];
  
//Perform Update to the database
  $q = "UPDATE `inventory`
        SET
        `barcode` = '" . $barcode . "',
        `title` = '" . $title . "',
        `authorfname` = '" . $afn . "',
        `authorlname` = '" . $aln . "',
        `genre` = '" . $genre . "',
        `age` = '" . $age . "',
        `location` = '" . $loc . "',
        `shelf` = '" . $shelf . "',
        `material` = '" . $mat . "',
        `cost` = '" . $cost . "',
        `status` = '" . $status . "',
        `condition` = '" . $cond . "' 
        WHERE
        `ID` = '" . $ID . "'";
  mysqli_query($conn, $q) or die($conn->error);
  
  echo '<div style="text-align: center;">Your changes have been saved!</div>';
}else{
//Load Variables
$bc = $_GET['bc'];
  
//Check if book is valid
$vq = "SELECT * FROM `inventory` WHERE `barcode` = '" . $bc . "'";
$vget = mysqli_query($conn, $vq) or die ($conn->error);
$vr = mysqli_fetch_array($vget);

if(mysqli_num_rows($vget) == 0){
  //Not found error message
  echo '*The book you scanned is not found in the database...';
}else{
  //Display Details in table format
  echo'<br><br><table class="tabler" style="margin: auto; color: black;">
        <tr><td>Barcode: </td><td><input type="text" id="barcode" value="' . $vr['barcode'] . '" /></td></tr>
        <tr><td>Title: </td><td><input type="text" id="title" value="' . $vr['title'] . '" /></td></tr>
        <tr><td>Autdors First Name: </td><td><input type="text" id="authorfname" value="' . $vr['authorfname'] . '" /></td></tr>
        <tr><td>Autdors Last Name: </td><td><input type="text" id="authorlname" value="' . $vr['authorlname'] . '" /></td></tr>
        <tr><td>Genre: </td><td><input type="text" id="genre" value="' . $vr['genre'] . '" /></td></tr>
        <tr><td>Age Group: </td><td><input type="text" id="age" value="' . $vr['age'] . '" /></td></tr>
        <tr><td>Location: </td><td><input type="text" id="location" value="' . $vr['location'] . '" /></td></tr>
        <tr><td>Shelf#: </td><td><input type="text" id="shelf" value="' . $vr['shelf'] . '" /></td></tr>
        <tr><td>Material: </td><td><input type="text" id="material" value="' . $vr['material'] . '" /></td></tr>
        <tr><td>Cost: </td><td><input type="text" id="cost" value="' . $vr['cost'] . '" /></td></tr>
        <tr><td>Status: </td><td><input type="text" id="status" value="' . $vr['status'] . '" /></td></tr>
        <tr><td>Condition: </td><td><input type="text" id="condition" value="' . $vr['condition'] . '" /></td></tr>
        <tr><td><input type="hidden" id="ID" value="' . $vr['ID'] . '"></td></tr>
       </table>
       <br><br>
       <button type="button" onclick="update_det();" style="color: black; margin: auto;">Save Changes</button>';
}
}
?>