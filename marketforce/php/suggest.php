<?php
include 'connection.php';

$mess = mysqli_real_escape_string($conn, $_GET['mess']);
$rep_name = $_GET['rep_name'];
$rep_id = $_GET['rep_id'];

//Turn off specific notification
$oq = "INSERT INTO `suggestions`
       (
       `date`,
       `rep_id`,
       `rep_name`,
       `suggestion`
       )
       VALUES
       (
       NOW() + INTERVAL 1 HOUR,
       '" . $rep_id . "',
       '" . $rep_name . "',
       '" . $mess . "'
       )";
mysqli_query($conn, $oq) or die($conn->error);

echo 'Your suggestion has been submitted successfully!';

    include 'phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    include 'phpmailsettings.php';
    $mail->setFrom('no-reply@allsteelcarports.com','Market Force');
    $mail->addAddress('michael@burtonsolution.tech');
    $mail->addBCC('michael@burtonsolution.tech');
    $mail->Subject = 'Notification From Market Force!';
    $mail->Body = '<html>
                    <head>
                      <style>
                        .main{
                          text-align: center;
                        }
                        img{
                          width: 80%;
                        }
                      </style>
                    </head>
                    <body>
                    <div class="main">
                      <img src="http://marketforceapp.com/marketforce/img/Market%20Force%20Logo%202.png" />
                      <h2>Notification From Market Force</h2>
                      <h3>New Suggestion!</h3>
                      <p>' . $_GET['mess'] . '</p>
                      <br>
                      <p><b>From:</b> ' . $rep_name . '</p>
                      <br><br>
                      <p>Thank You,<br><strong>Market Force Team<strong></p>
                    </div>
                    </body>
                  </html>';

$mail->send();


?>