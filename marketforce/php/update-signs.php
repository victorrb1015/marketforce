<?php
include 'connection.php';

//Load Variables
$id = $_GET['id'];
$mode = $_GET['mode'];
$rep_name = $_GET['rep_name'];
$rep_id = $_GET['rep_id'];

//Get Dealer Name
$dnq = "SELECT * FROM `dealers` WHERE `ID` = '" . $id . "'";
$dng = mysqli_query($conn, $dnq) or die($conn->error);
$dnr = mysqli_fetch_array($dng);
$dname = mysqli_real_escape_string($conn, $dnr['name']);

//UPDATE database
if($mode == 'Done'){
$q = "UPDATE `dealers` SET `new_signs` = 'Done' WHERE `ID` = '" . $id . "'";
//Log the Activity
$log = fopen("update-signs-activity-log.txt","a");
fwrite($log, date("m/d/y h:iA") . " --> " . $rep_name . "\n");
fwrite($log, "Marked Sign Changes as DONE for " . $dname . " in " . $dnr['city'] . ", " . $dnr['state'] . ".\n");
fwrite($log, "---------------------------------------------------------------------------------------------\n");
fclose($log);
  
}else{
$q = "UPDATE `dealers` SET `new_signs` = '' WHERE `ID` = '" . $id . "'";

$log = fopen("update-signs-activity-log.txt","a");
fwrite($log, date("m/d/y h:iA") . " --> " . $rep_name . "\n");
fwrite($log, "Marked Sign Changes as NOT DONE for " . $dname . " in " . $dnr['city'] . ", " . $dnr['state'] . ".\n");
fwrite($log, "---------------------------------------------------------------------------------------------\n");
fclose($log);

}
mysqli_query($conn, $q) or die($conn->error);

echo 'The dealers file has been updated!';


?>