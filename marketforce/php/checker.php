<?php
session_start();

//Include Connection to DB
include 'connection.php';

//Load Variables
$q = $_GET['q'];
$bc = $_GET['bc'];
$user = $_GET['user'];
$cond = $_GET['cond'];

//Make sure book is valid
$vq = "SELECT * FROM `inventory` WHERE `Barcode` = '" . $bc . "'";
$vget = mysqli_query($conn, $vq);
$vr = mysqli_fetch_array($vget);
if(mysqli_num_rows($vget) === 0){
  echo "The book you scanned is invalid...";
}else{
//If this is an IN Query, Do this...
if($q == "in"){
  $iq = "UPDATE `inventory` SET `status` = 'IN-STOCK', `condition` = '" . $cond . "', `date_out` = NULL WHERE `barcode` = '" . $bc . "'";
  $iget = mysqli_query($conn, $iq) or die($conn->error);
  echo 'Please return "' . $vr['title'] . '" to shelf#: ' . $vr['shelf'];
}


//If this is an OUT Query, Do this...
if($q == "out"){
//Get User Info
 $userq = "SELECT * FROM `users` WHERE `ID` = '" . $user . "'";
  $userget = mysqli_query($conn, $userq);
  $userr = mysqli_fetch_array($userget);
//If book is not already checked out, Do this...
  if($vr['status'] == "IN-STOCK"){
$oq = "UPDATE `inventory` SET `status` = '" . $user . "', `date_out` = CURRENT_DATE WHERE `Barcode` = '" . $bc . "'";
$oget = mysqli_query($conn, $oq) or die($conn->error);
  
  echo '"' . $vr['title'] . '" has been checked out to ' . $userr['fname'] . ' ' . $userr['lname'] . '.';
  }else{echo '"' . $vr['title'] . '" is already checked out to someone else...';}
}
  
}//End of the main else statement
?>