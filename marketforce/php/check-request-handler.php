<?php
include 'connection.php';

$oid = $_GET['orgID'];
$oq = "SELECT * FROM `organizations` WHERE `org_id` = '" . $oid . "'";
$og = mysqli_query($mf_conn, $oq) or die($mf_conn->error);
$or = mysqli_fetch_array($og);
$orgDB = $or['db_name'];
$aconn = mysqli_connect('localhost','marketfo_mf','#NgTFJQ!z@t8',$orgDB) or die($aconn->error);

//Load Variables
$id = $_GET['id'];
$stat = $_GET['stat'];
$note = mysqli_real_escape_string($aconn, $_GET['note']);
$cnum = mysqli_real_escape_string($aconn,$_GET['cnum']);
$camount = mysqli_real_escape_string($aconn,$_GET['camount']);
$mdate = mysqli_real_escape_string($aconn,$_GET['mdate']);
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];

//Setup Email System
include 'phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include 'phpmailsettings.php';
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
//$mail->addAddress('michael@burtonsolution.tech');//Test Mode Only...
//$mail->addAddress('scott@allsteelcarports.com');
$mail->addAddress('ap@allsteelcarports.com');
$mail->addBCC('michael@allsteelcarports.com');
$mail->addBCC('archive@ignition-innovations.com');

//Get Info for ID
$iq = "SELECT * FROM `check_requests` WHERE `ID` = '" . $id . "'";
$ig = mysqli_query($aconn, $iq) or die($aconn->error);
$ir = mysqli_fetch_array($ig);
$dname = mysqli_real_escape_string($aconn, $ir['to']);

//Get Rep Info
$rnq = "SELECT * FROM `users` WHERE `ID` = '" . $ir['user_id'] . "'";
$rng = mysqli_query($aconn, $rnq) or die($aconn->error);
$rnr = mysqli_fetch_array($rng);
$remail = $rnr['email'];


//If Stat is COMPLETED....................................................................
if($stat == 'Completed'){
  $q = "UPDATE `check_requests` SET 
  `status` = 'Completed', 
  `last_action_date` = CURRENT_DATE,
  `last_action_time` = CURRENT_TIME,
  `last_action_id` = '" . $rep_id . "',
  `last_action_name` = '" . $rep_name. "',
  `note` = '" . $note . "',
  `cnum` = '" . $cnum . "',
  `camount` = '" . $camount . "',
  `mdate` = '" . $mdate . "'
  WHERE `ID` = '" . $id . "'";
  mysqli_query($aconn, $q) or die($aconn->error);
  
  
  //Setup email parameters...
  $mail->addAddress($remail);
  $mail->Subject = 'Your Check Request Was Completed!';
  $mail->Body = '<html>
                  <head>
                  <style>
                  .main{
                    text-align: center;
                    width:90%;
                  }
                  img{
                    width: 30%;
                  }
                  </style>
                  </head>
                  <body>
                    <div class="main">
                      <img src="http://marketforceapp.com/marketforce/img/Market Force Logo 2.png" />
                      <h1>Notification:</h1>
                      <p>Your Check Request for <b>' . $dname . '</b> was completed!
                      <br><br>
                      <b>Notes:</b> ' . $note . '</p>
                      <br>
                      <h3><u>Check Details:</u></h3>
                      <p><b>Check#:</b> ' . $cnum . '</p>
                      <p><b>Check Amount:</b> ' . $camount . '</p>
                      <p><b>Date Check Was Mailed:</b> ' . $mdate . '</p>
                    </div>
                  </body>
                  </html>';
  
  $mail->send();
  echo 'Check Request for ' . $dname . ' has been updated to COMPLETED';//Confirmation response
}


//If Status is MORE INFO............................................................................
if($stat == 'Need Info'){
  $q = "UPDATE `check_requests` SET 
        `status` = 'Need Info', 
        `last_action_date` = CURRENT_DATE,
        `last_action_time` = CURRENT_TIME,
        `last_action_id` = '" . $rep_id . "',
        `last_action_name` = '" . $rep_name. "',
        `note` = '" . $note . "' 
        WHERE `ID` = '" . $id . "'";
  mysqli_query($aconn, $q) or die($aconn->error);
  
  
  //Setup email parameters...
  $mail->addAddress($remail);
  $mail->Subject = 'We Need More Info!';
  $mail->Body = '<html>
                  <head>
                  <style>
                  .main{
                    text-align: center;
                    width:90%;
                  }
                  img{
                    width: 30%;
                  }
                  </style>
                  </head>
                  <body>
                    <div class="main">
                      <img src="http://marketforceapp.com/marketforce/img/Market Force Logo 2.png" />
                      <h1>Notification:</h1>
                      <p>More information has been requested about your Check Request for <b>' . $dname . '</b>.
                      <br><br>
                      <b>Notes:</b> ' . $note . '</p>
                      <br>
                      <!--<p>
                      Please Email Scott @ <a href="mailto:scott@allsteelcarports.com">scott@allsteelcarports.com</a> 
                      with the requested information ASAP!
                      </p>-->
                    </div>
                  </body>
                  </html>';
  
  $mail->send();
  echo 'Check Request for ' . $dname . ' has been updated to Need Info';//Confirmation response
}




//If Status is CANCELLED..............................................................................................
if($stat == 'Cancelled'){
  $q = "UPDATE `check_requests` SET 
        `status` = 'Cancelled', 
        `last_action_date` = CURRENT_DATE,
        `last_action_time` = CURRENT_TIME,
        `last_action_id` = '" . $rep_id . "',
        `last_action_name` = '" . $rep_name. "',
        `note` = '" . $note . "'
        WHERE `ID` = '" . $id . "'";
  mysqli_query($aconn, $q) or die($aconn->error);
  
  
  //Setup email parameters...
  $mail->addAddress($remail);
  $mail->Subject = 'Cancelled Check Request!';
  $mail->Body = '<html>
                  <head>
                  <style>
                  .main{
                    text-align: center;
                    width:90%;
                  }
                  img{
                    width: 30%;
                  }
                  </style>
                  </head>
                  <body>
                    <div class="main">
                      <img src="http://marketforceapp.com/marketforce/img/Market Force Logo 2.png" />
                      <h1>Notification:</h1>
                      <p>Your Check Request for <b>' . $dname . '</b> has been cancelled.
                      <br><br>
                      <b>Notes:</b> ' . $note . '</p>
                    </div>
                  </body>
                  </html>';
  
  $mail->send();
  echo 'Check Request for ' . $dname . ' has been cancelled';//Confirmation response
}

?>