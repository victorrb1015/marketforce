<?php
include 'connection.php';

//Load Variables
$id = $_GET['id'];
$stat = $_GET['stat'];
$note = mysqli_real_escape_string($conn, $_GET['note']);

//Setup Email System
include 'phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include 'phpmailsettings.php';
$mail->CharSet = "UTF-8";
$mail->setFrom('no-reply@allsteelcarports.com','MarketForce');
//$mail->addAddress('michael@burtonsolution.tech');
$mail->addAddress('michael@allsteelcarports.com');
$mail->addCC('teresa@allsteelcarports.com');
$mail->addBCC('archive@ignition-innovations.com');

//Get Info for ID
$iq = "SELECT * FROM `new_packets` WHERE `ID` = '" . $id . "'";
$ig = mysqli_query($conn, $iq) or die($conn->error);
$ir = mysqli_fetch_array($ig);
  $pid = $ir['dealer_id'];

//Get Rep Info
$rnq = "SELECT * FROM `users` WHERE `ID` = '" . $ir['rep'] . "'";
$rng = mysqli_query($conn, $rnq) or die($conn->error);
$rnr = mysqli_fetch_array($rng);
$remail = $rnr['email'];


//If Stat is COMPLETED....................................................................
if($stat == 'Completed'){
  if($ir['form_type'] == 'New Dealer Packet' || $ir['form_type'] == 'Display Addon Form'){
    $q = "UPDATE `new_packets` SET `status` = 'Pending Installation', `note` = '" . $note . "', `cl_approved` = 'Yes' WHERE `ID` = '" . $id . "'";
  }else{
    $q = "UPDATE `new_packets` SET `status` = 'Completed', `note` = '" . $note . "', `cl_approved` = 'Yes' WHERE `ID` = '" . $id . "'";
  }
  mysqli_query($conn, $q) or die($conn->error);
  
  //System Automation of setting the dealer to INACTIVE
  if($ir['form_type'] == 'Dealer Closing Form'){
    $rdq = "UPDATE `dealers` SET `inactive` = 'Yes' WHERE `ID` = '" . $ir['dealer_id'] . "'";
    mysqli_query($conn, $rdq) or die($conn->error);
    $rdq2 = "UPDATE `new_dealer_form` SET `inactive` = 'Yes' WHERE `ID` = '" . $ir['dealer_id'] . "'";
    mysqli_query($conn, $rdq2) or die($conn->error);
  }
  
  //This script handles any ownership change form details...
  include 'ownership-change-action.php';
  
  
  //Setup email parameters...
  $mail->addAddress($remail);
  $mail->Subject = 'Your Document Was Accepted!';
  $mail->Body = '<html>
                  <head>
                  <style>
                  .main{
                    text-align: center;
                    width:90%;
                  }
                  img{
                    width: 30%;
                  }
                  </style>
                  </head>
                  <body>
                    <div class="main">
                      <img src="http://marketforceapp.com/marketforce/img/Market Force Logo 2.png" />
                      <h1>Notification:</h1>
                      <p>Your ' . $ir['form_type'] . ' for <b>' . $ir['name'] . '</b> was accepted!
                      <br><br>
                      <b>Notes:</b> ' . $note . '</p>
                    </div>
                  </body>
                  </html>';
  
  $mail->send();
  echo $ir['name'] . ' has been updated to COMPLETED';//Confirmation response
}


//If Status is MORE INFO............................................................................
if($stat == 'Need Info'){
  $q = "UPDATE `new_packets` SET `status` = 'Need Info', `note` = '" . $note . "' WHERE `ID` = '" . $id . "'";
  mysqli_query($conn, $q) or die($conn->error);
  
  
  //Setup email parameters...
  $mail->addAddress($remail);
  $mail->Subject = 'We Need More Info!';
  $mail->Body = '<html>
                  <head>
                  <style>
                  .main{
                    text-align: center;
                    width:90%;
                  }
                  img{
                    width: 30%;
                  }
                  </style>
                  </head>
                  <body>
                    <div class="main">
                      <img src="http://marketforceapp.com/marketforce/img/Market Force Logo 2.png" />
                      <h1>Notification:</h1>
                      <p>More information has been requested about your ' . $ir['form_type'] . ' for <b>' . $ir['name'] . '</b>.
                      <br><br>
                      <b>Notes:</b> ' . $note . '</p>
                    </div>
                  </body>
                  </html>';
  
  $mail->send();
  echo $ir['name'] . ' has been updated to Need Info';//Confirmation response
}



if($stat == 'Needs Printing'){
  $q = "UPDATE `new_packets` SET `cl_approved` = 'Printed' WHERE `ID` = '" . $id . "'";
  mysqli_query($conn, $q) or die($conn->error);
  echo $ir['name'] . ' has been marked Printed.';
}

?>