<?php
include 'connection.php';

//Load Variables
$m = mysqli_real_escape_string($conn, $_GET['m']);
$id = $_GET['id'];
$user = $_GET['user'];
$name = mysqli_real_escape_string($conn, $_GET['n']);
$cb1 = $_GET['cb1'];
$cb2 = $_GET['cb2'];
$cb3 = $_GET['cb3'];
$cb4 = $_GET['cb4'];
$cb5 = $_GET['cb5'];
$cb6 = $_GET['cb6'];
$cb7 = $_GET['cb7'];
$cb8 = $_GET['cb8'];
$cb9 = $_GET['cb9'];
$cb10 = $_GET['cb10'];
$cb11 = $_GET['cb11'];
$cb12 = $_GET['cb12'];
$cb13 = $_GET['cb13'];
$cb14 = $_GET['cb14'];
$cb15 = $_GET['cb15'];
$cb16 = $_GET['cb16'];
$cb17 = $_GET['cb17'];
$cb18 = $_GET['cb18'];
$cb19 = $_GET['cb19'];
$cb20 = $_GET['cb20'];
$cb21 = $_GET['cb21'];
$cb22 = $_GET['cb22'];
$cb23 = $_GET['cb23'];
$cb24 = $_GET['cb24'];
$cb25 = $_GET['cb25'];
$vtype = $_GET['vtype'];
$biz = mysqli_real_escape_string($conn, $_GET['biz']);
$address = mysqli_real_escape_string($conn, $_GET['address']);
$city = mysqli_real_escape_string($conn, $_GET['city']);
$state = mysqli_real_escape_string($conn, $_GET['state']);
$de = mysqli_real_escape_string($conn, $_GET['de']);
$df = mysqli_real_escape_string($conn, $_GET['df']);
$dc = mysqli_real_escape_string($conn, $_GET['dc']);

//Price Box Variables
$pb1 = $_GET['pb1'];
$pb2 = $_GET['pb2'];
$pb3 = $_GET['pb3'];
$pb4 = $_GET['pb4'];
$pb5 = $_GET['pb5'];
$pb6 = $_GET['pb6'];
$pb7 = $_GET['pb7'];
$pb8 = $_GET['pb8'];
$pb9 = $_GET['pb9'];


//Daily Hours Info
$sun_open = mysqli_real_escape_string($conn, $_GET['sun_open']);
$sun_close = mysqli_real_escape_string($conn, $_GET['sun_close']);
$sun_status = mysqli_real_escape_string($conn, $_GET['sun_status']);
$mon_open = mysqli_real_escape_string($conn, $_GET['mon_open']);
$mon_close = mysqli_real_escape_string($conn, $_GET['mon_close']);
$mon_status = mysqli_real_escape_string($conn, $_GET['mon_status']);
$tue_open = mysqli_real_escape_string($conn, $_GET['tue_open']);
$tue_close = mysqli_real_escape_string($conn, $_GET['tue_close']);
$tue_status = mysqli_real_escape_string($conn, $_GET['tue_status']);
$wed_open = mysqli_real_escape_string($conn, $_GET['wed_open']);
$wed_close = mysqli_real_escape_string($conn, $_GET['wed_close']);
$wed_status = mysqli_real_escape_string($conn, $_GET['wed_status']);
$thu_open = mysqli_real_escape_string($conn, $_GET['thu_open']);
$thu_close = mysqli_real_escape_string($conn, $_GET['thu_close']);
$thu_status = mysqli_real_escape_string($conn, $_GET['thu_status']);
$fri_open = mysqli_real_escape_string($conn, $_GET['fri_open']);
$fri_close = mysqli_real_escape_string($conn, $_GET['fri_close']);
$fri_status = mysqli_real_escape_string($conn, $_GET['fri_status']);
$sat_open = mysqli_real_escape_string($conn, $_GET['sat_open']);
$sat_close = mysqli_real_escape_string($conn, $_GET['sat_close']);
$sat_status = mysqli_real_escape_string($conn, $_GET['sat_status']);

//Unmanned Lot?
$unman = $_GET['unman'];

//New Signs
$signs = $_GET['signs'];

//Check if note exists for dealer from user already 
if($id == 'cold'){
	/*$c = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $id . "' AND `user` = '" . $user . "' AND `date` = CURRENT_DATE";
$cg = mysqli_query($conn, $c) or die($conn->error);
//$cr = mysqli_fetch_array($cg);
if(mysqli_num_rows($cg) == 0){*/

//Insert note query
$iq = "INSERT 
       INTO 
       `visit_notes` 
       (`dealer_id`,
			 `biz_name`,
			 `address`,
			 `city`,
			 `state`,
			 `notes`,
			 `user`,
			 `date`,
			 `time`,
			 `name`,
			 `cb1`,
			 `cb2`,
			 `cb3`,
			 `cb4`,
			 `cb5`,
			 `cb6`,
			 `cb7`,
			 `cb8`,
			 `cb9`,
			 `cb10`,
			 `cb11`,
			 `cb12`,
			 `cb13`,
			 `cb14`,
			 `cb15`,
			 `cb16`,
			 `cb17`,
			 `cb18`,
			 `cb19`,
			 `cb20`,
			 `cb21`,
			 `cb22`,
			 `cb23`,
			 `cb24`,
			 `cb25`,
			 `pb1`,
			 `pb2`,
			 `pb3`,
			 `pb4`,
			 `pb5`,
			 `pb6`,
			 `pb7`,
			 `pb8`,
			 `pb9`,
			 `vtype`,
			 `email`) 
       VALUES
       ('" . $id . "','" . $biz . "','" . $address . "','" . $city . "','" . $state . "','" . $m . "','" . $user . "',CURRENT_DATE,DATE_ADD(now(), INTERVAL 1 HOUR),
       '" . $name . "',
       '" . $cb1 . "',
       '" . $cb2 . "',
       '" . $cb3 . "',
       '" . $cb4 . "',
       '" . $cb5 . "',
       '" . $cb6 . "',
       '" . $cb7 . "',
       '" . $cb8 . "',
       '" . $cb9 . "',
       '" . $cb10 . "',
       '" . $cb11 . "',
       '" . $cb12 . "',
       '" . $cb13 . "',
       '" . $cb14 . "',
       '" . $cb15 . "',
       '" . $cb16 . "',
       '" . $cb17 . "',
       '" . $cb18 . "',
       '" . $cb19 . "',
       '" . $cb20 . "',
       '" . $cb21 . "',
       '" . $cb22 . "',
       '" . $cb23 . "',
			 '" . $cb24 . "',
			 '" . $cb25 . "',
			 '" . $pb1 . "',
			 '" . $pb2 . "',
			 '" . $pb3 . "',
			 '" . $pb4 . "',
			 '" . $pb5 . "',
			 '" . $pb6 . "',
			 '" . $pb7 . "',
			 '" . $pb8 . "',
			 '" . $pb9 . "',
			 'Cold Call',
			 '" . $de . "')";

$ig = mysqli_query($conn, $iq) or die($conn->error);
	echo 'Your Cold Call has been recorded. Thank You!';
	
	//I removed an email update function here ... did not look like it was doing anything
	//Update main list
	/*$xq = "UPDATE `dealers` SET 
				`email` = '" . $de . "'
				WHERE `ID` = '" . $id . "'";
	$xg = mysqli_query($conn, $xq) or die($conn->error);*/
	
}else{
	
$c = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $id . "' AND `user` = '" . $user . "' AND `date` = CURRENT_DATE AND `vtype` != 'Internal Note'";
$cg = mysqli_query($conn, $c) or die($conn->error);
//$cr = mysqli_fetch_array($cg);
if(mysqli_num_rows($cg) == 0 || $vtype == 'Internal Note'){

//Insert note query
$iq = "INSERT 
       INTO 
       `visit_notes` 
       (`dealer_id`,
			 `notes`,
			 `user`,
			 `date`,
			 `time`,
			 `name`,
			 `cb1`,
			 `cb2`,
			 `cb3`,
			 `cb4`,
			 `cb5`,
			 `cb6`,
			 `cb7`,
			 `cb8`,
			 `cb9`,
			 `cb10`,
			 `cb11`,
			 `cb12`,
			 `cb13`,
			 `cb14`,
			 `cb15`,
			 `cb16`,
			 `cb17`,
			 `cb18`,
			 `cb19`,
			 `cb20`,
			 `cb21`,
			 `cb22`,
			 `cb23`,
			 `cb24`,
			 `cb25`,
			 `pb1`,
			 `pb2`,
			 `pb3`,
			 `pb4`,
			 `pb5`,
			 `pb6`,
			 `pb7`,
			 `pb8`,
			 `pb9`,
			 `vtype`,
			 `email`,
			 `fax`) 
       VALUES
       ('" . $id . "','" . $m . "','" . $user . "',CURRENT_DATE,DATE_ADD(now(), INTERVAL 1 HOUR),
       '" . $name . "',
       '" . $cb1 . "',
       '" . $cb2 . "',
       '" . $cb3 . "',
       '" . $cb4 . "',
       '" . $cb5 . "',
       '" . $cb6 . "',
       '" . $cb7 . "',
       '" . $cb8 . "',
       '" . $cb9 . "',
       '" . $cb10 . "',
       '" . $cb11 . "',
       '" . $cb12 . "',
       '" . $cb13 . "',
       '" . $cb14 . "',
       '" . $cb15 . "',
       '" . $cb16 . "',
       '" . $cb17 . "',
       '" . $cb18 . "',
       '" . $cb19 . "',
       '" . $cb20 . "',
       '" . $cb21 . "',
       '" . $cb22 . "',
       '" . $cb23 . "',
			 '" . $cb24 . "',
			 '" . $cb25 . "',
			 '" . $pb1 . "',
			 '" . $pb2 . "',
			 '" . $pb3 . "',
			 '" . $pb4 . "',
			 '" . $pb5 . "',
			 '" . $pb6 . "',
			 '" . $pb7 . "',
			 '" . $pb8 . "',
			 '" . $pb9 . "',
			 '" . $vtype . "',
			 '" . $de . "',
			 '" . $df . "')";

$ig = mysqli_query($conn, $iq);

	//Update Hours Open data
	$xq = "UPDATE `dealers` SET 
				`sun_open` = '" . $sun_open . "',
				`sun_close` = '" . $sun_close . "',
				`sun_status` = '" . $sun_status . "',
				`mon_open` = '" . $mon_open . "',
				`mon_close` = '" . $mon_close . "',
				`mon_status` = '" . $mon_status . "',
				`tue_open` = '" . $tue_open . "',
				`tue_close` = '" . $tue_close . "',
				`tue_status` = '" . $tue_status . "',
				`wed_open` = '" . $wed_open . "',
				`wed_close` = '" . $wed_close . "',
				`wed_status` = '" . $wed_status . "',
				`thu_open` = '" . $thu_open . "',
				`thu_close` = '" . $thu_close . "',
				`thu_status` = '" . $thu_status . "',
				`fri_open` = '" . $fri_open . "',
				`fri_close` = '" . $fri_close . "',
				`fri_status` = '" . $fri_status . "',
				`sat_open` = '" . $sat_open . "',
				`sat_close` = '" . $sat_close . "',
				`sat_status` = '" . $sat_status . "',
				`new_signs` = '" . $signs . "'
				WHERE `ID` = '" . $id . "'";
	$xg = mysqli_query($conn, $xq) or die($conn->error);
	
		//SMTP EMAIL SETUP
		include 'phpmailer/PHPMailerAutoload.php';
		$mail = new PHPMailer;
		include 'phpmailsettings.php';
	
	//Check to see if note was successfully entered...
	$ncq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $id . "' AND `date` = CURRENT_DATE";
	$ncg = mysqli_query($conn, $ncq) or die($conn->error);
	if(mysqli_num_rows($ncg) == 0 || !$ig){
		$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
		$mail->addAddress('michael@burtonsolution.tech');
		$mail->addAddress('3362513969@txt.att.net');
		$mail->Subject = 'Note Failure!!!!';
		$mail->Body = '<html>
										<body>
											<div id="main">
												<h1>Note Failure:</h1>
												<p><b>Name:</b> ' . $name . '</p>
												<p><b>ID:</b> ' . $id . '</p>
												<p><b>User:</b> ' . $user . '</p>
												<p><b>Notes:</b> ' . $m . '</p>
												<p><b>cb1:</b> ' . $cb1 . '</p>
												<p><b>cb2:</b> ' . $cb2 . '</p>
												<p><b>cb3:</b> ' . $cb3 . '</p>
												<p><b>cb4:</b> ' . $cb4 . '</p>
												<p><b>cb5:</b> ' . $cb5 . '</p>
												<p><b>cb6:</b> ' . $cb6 . '</p>
												<p><b>cb7:</b> ' . $cb7 . '</p>
												<p><b>cb8:</b> ' . $cb8 . '</p>
												<p><b>cb9:</b> ' . $cb9 . '</p>
												<p><b>cb10:</b> ' . $cb10 . '</p>
												<p><b>cb11:</b> ' . $cb11 . '</p>
												<p><b>cb12:</b> ' . $cb12 . '</p>
												<p><b>cb13:</b> ' . $cb13 . '</p>
												<p><b>cb14:</b> ' . $cb14 . '</p>
												<p><b>cb15:</b> ' . $cb15 . '</p>
												<p><b>cb16:</b> ' . $cb16 . '</p>
												<p><b>cb17:</b> ' . $cb17 . '</p>
												<p><b>cb18:</b> ' . $cb18 . '</p>
												<p><b>cb19:</b> ' . $cb19 . '</p>
												<p><b>cb20:</b> ' . $cb20 . '</p>
												<p><b>cb21:</b> ' . $cb21 . '</p>
												<p><b>cb22:</b> ' . $cb22 . '</p>
												<p><b>cb23:</b> ' . $cb23 . '</p>
												<p><b>cb24:</b> ' . $cb24 . '</p>
												<p><b>cb25:</b> ' . $cb25 . '</p>
												<p><b>Visit Type:</b> ' . $vtype . '</p>
												<p><b>Dealer Email:</b> ' . $de . '</p>
											</div>
										</body>
										</html>';
		$mail->send();
		$mail->ClearAddresses();
		$error = true;
	}
	
	//Check Dealer Current Status...
	$csq = "SELECT * FROM `dealers` WHERE `ID` = '" . $id . "'";
	$csg = mysqli_query($conn, $csq) or die($conn->error);
	$csr = mysqli_fetch_array($csg);
	
	if($vtype == 'Check-In'){
		$stat = 'Visited';
		//Update main list
	$xq = "UPDATE `dealers` SET `email` = '" . $de . "', `status` = '" . $stat . "' WHERE `ID` = '" . $id . "'";
	$xg = mysqli_query($conn, $xq) or die($conn->error);
	}
	if($vtype == 'Training' && $csr['status'] != 'Visited'){
		$stat = 'Trained';
		//Update main list
	$xq = "UPDATE `dealers` SET `email` = '" . $de . "', `status` = '" . $stat . "' WHERE `ID` = '" . $id . "'";
	$xg = mysqli_query($conn, $xq) or die($conn->error);
	}
	if($unman == 'Unmanned Lot'){
		$stat = 'Unmanned Lot';
		//Update main list
	$xq = "UPDATE `dealers` SET `email` = '" . $de . "', `status` = '" . $stat . "' WHERE `ID` = '" . $id . "'";
	$xg = mysqli_query($conn, $xq) or die($conn->error);
	}
	

	//Update the Dealer Cell Number
	$dcuq = "UPDATE `new_dealer_form` SET `ownerphone` = '" . $dc . "' WHERE `ID` = '" . $id . "'";
	mysqli_query($conn, $dcuq) or die($conn->error);
	
	//Add the email to the databases
	$euq1 = "UPDATE `new_dealer_form` SET `email` = '" . $de . "' WHERE `ID` = '" . $id . "'";
	mysqli_query($conn, $euq1) or die($conn->error);
	$euq2 = "UPDATE `dealers` SET `email` = '" . $de . "' WHERE `ID` = '" . $id . "'";
	mysqli_query($conn, $euq2) or die($conn->error);
	
	//Add the fax# to the databases
	$fuq1 = "UPDATE `new_dealer_form` SET `fax` = '" . $df . "' WHERE `ID` = '" . $id . "'";
	mysqli_query($conn, $fuq1) or die($conn->error);
	$fuq2 = "UPDATE `dealers` SET `fax` = '" . $df . "' WHERE `ID` = '" . $id . "'";
	mysqli_query($conn, $fuq2) or die($conn->error);
	
	
//Send a Survey Request Email

//Setup Variables
$rnq = "SELECT * FROM `users` WHERE `ID` = '" . $user . "'";
$rng = mysqli_query($conn, $rnq) or die($conn->error);
$rnr = mysqli_fetch_array($rng);

$rep = $rnr['fname'] . ' ' . $rnr['lname'];
$dealer = $id;
$vd = date("m/d/Y");

//Get Dealer Information
$dnq = "SELECT * FROM `dealers` WHERE `ID` = '" . $dealer . "'";
$dng = mysqli_query($conn, $dnq);
$dnr = mysqli_fetch_array($dng);

$dealer_name = $dnr['name'];
$email = $dnr['email'];
//$email = 'michael@burtonsolution.tech';
	
//Check if dealer has email entered...
if($email == ''){
	//$email = 'info@burtonsolution.tech';
	$email = 'michael@allsteelcarports.com';

		//SMTP EMAIL SETUP
		/*include 'phpmailer/PHPMailerAutoload.php';
		$mail = new PHPMailer;
		include 'phpmailsettings.php';*/
//Email Parameters
$admin = "michael@burtonsolution.tech";
	

	
$mail->addAddress($email);
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
$mail->addCC('ashley@allsteelcarports.com');
$mail->addBCC($admin);
$mail->Subject = "We Value Your Input!";
$mail->Body = '<html>
								<head>
								<style>
								ul{
									list-style-type: none;
								}
								li{
									padding: 5px;
								}
								.img{
									text-align:center;
									margin: auto;
								}
								</style>
								<body>
								<div class="main">
								<div class="img" style="width:100%;">
									<img src="http://marketforceapp.com/marketforce/img/Market Force Logo 2.png" style="width:60%;" />
								</div>
									<h1 style="text-align:center;">Survey Not Sent!</h1>
									<p>
									<b>' . $rep . '</b> just made a visit with a dealer and we would like to send a survey, however, 
									no email was found for <b>' . $dnr['name'] . ' (' . $dnr['city'] . ', ' . $dnr['state'] . ')</b> in Market Force. 
									We are contacting you to let you know that they will not recieve 
									a survey via email due to this!
									</p>
									<p>
									You can do the following:<br>
									<ul>
										<li>1) If you have an email address for the dealer, you can log into
												Market Force and manually send the survey to that email address using
												the "Send A Survey" page!
										</li>';
								if($dnr['phone'] != ''){
										$mail->Body .= '<li>2) You can give the dealer a call at: <b>' . $dnr['phone'] . '</b> and perform 
															the survey over the phone by clicking <b><a href="http://marketforceapp.com/marketforce/forms/rep-survey.php?d=' . $dealer . '&r=' . $rep . '&vd=' . $vd . '" target="_blank">HERE</a></b>
													</li>';
								}
								$mail->Body .= '</div>
								</body>
								</html>';
	if($error != true){
		if($vtype != 'Internal Note'){
	$mail->send();
		}
	}
	
	//Add Support Ticket to change dealer status on Market Force Map
$mail->ClearAddresses();
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
$mail->addAddress('support@marketforceapp.com');
$mail->Subject = 'Dealer Status Update!';
$mail->Body = 'Please change the status of the following dealer on the map:<br>
							<br>
							' . $dnr['name'] . '<br>
							' . $dnr['address'] . '<br>
							' . $dnr['city'] . ', ' . $dnr['state'] . ' ' . $dnr['zip'] . '<br>
							Phone#: ' . $dnr['phone'] . '<br>
							Fax#: ' . $dnr['fax'] . '<br>
							Email: ' . $dnr['email'];

if($error != true){
	//$mail->send();	
	echo 'Your notes have been added to the system! Thank You!';
}else{
	echo 'An error has occured while entering your notes! The system administrator has been notified and your information has been saved!';
}
	break;
	
}
	
	
	
//SMTP EMAIL SETUP
/*include 'phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include 'phpmailsettings.php';*/

//Email Parameters
$admin = "archive@ignition-innovations.com";

//Parse Multiple Emails
$memails = explode(",", $dnr['email']);
$anum = count($memails) - 1;
$memi = 0;
	while($memi <= $anum){
		$xm = $memails[$memi];
		$mail->addAddress($xm);
		$memi++;
	}
	
//$mail->addAddress($email);
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
$mail->addBCC($admin);
$mail->Subject = "We Value Your Input!";
$mail->Body = '
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no" /> <!-- disable auto telephone linking in iOS -->
		<title>Respmail is a response HTML email designed to work on all major email platforms and smartphones</title>
		<style type="text/css">
			/* RESET STYLES */
			html { background-color:#E1E1E1; margin:0; padding:0; }
			body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, "Lucida Grande", sans-serif;}
			table{border-collapse:collapse;}
			table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#7A7A7A;font-weight:normal;}
			img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
			a {text-decoration:none !important;border-bottom: 1px solid;}
			h1, h2, h3, h4, h5, h6{color:#5F5F5F; font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}
			/* CLIENT-SPECIFIC STYLES */
			.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */
			table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */
			#outlook a{padding:0;} /* Force Outlook 2007 and up to provide a "view in browser" message. */
			img{-ms-interpolation-mode: bicubic;display:block;outline:none; text-decoration:none;} /* Force IE to smoothly render resized images. */
			body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; font-weight:normal!important;} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
			.ExternalClass td[class="ecxflexibleContainerBox"] h3 {padding-top: 10px !important;} /* Force hotmail to push 2-grid sub headers down */
			/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */
			/* ========== Page Styles ========== */
			h1{display:block;font-size:26px;font-style:normal;font-weight:normal;line-height:100%;}
			h2{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:120%;}
			h3{display:block;font-size:17px;font-style:normal;font-weight:normal;line-height:110%;}
			h4{display:block;font-size:18px;font-style:italic;font-weight:normal;line-height:100%;}
			.flexibleImage{height:auto;}
			.linkRemoveBorder{border-bottom:0 !important;}
			table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}
			body, #bodyTable{background-color:#E1E1E1;}
			#emailHeader{background-color:#E1E1E1;}
			#emailBody{background-color:#FFFFFF;}
			#emailFooter{background-color:#E1E1E1;}
			.nestedContainer{background-color:#F8F8F8; border:1px solid #CCCCCC;}
			.emailButton{background-color:#205478; border-collapse:separate;}
			.buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
			.buttonContent a{color:#FFFFFF; display:block; text-decoration:none!important; border:0!important;}
			.emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}
			.emailCalendarMonth{background-color:#205478; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}
			.emailCalendarDay{color:#205478; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}
			.imageContentText {margin-top: 10px;line-height:0;}
			.imageContentText a {line-height:0;}
			#invisibleIntroduction {display:none !important;} /* Removing the introduction text from the view */
			/*FRAMEWORK HACKS & OVERRIDES */
			span[class=ios-color-hack] a {color:#275100!important;text-decoration:none!important;} /* Remove all link colors in IOS (below are duplicates based on the color preference) */
			span[class=ios-color-hack2] a {color:#205478!important;text-decoration:none!important;}
			span[class=ios-color-hack3] a {color:#8B8B8B!important;text-decoration:none!important;}
			/* A nice and clean way to target phone numbers you want clickable and avoid a mobile phone from linking other numbers that look like, but are not phone numbers.  Use these two blocks of code to "unstyle" any numbers that may be linked.  The second block gives you a class to apply with a span tag to the numbers you would like linked and styled.
			Inspired by Campaign Monitor&apos;s article on using phone numbers in email: http://www.campaignmonitor.com/blog/post/3571/using-phone-numbers-in-html-email/.
			*/
			.a[href^="tel"], a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:none!important;cursor:default!important;}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:auto!important;cursor:default!important;}
			/* MOBILE STYLES */
			@media only screen and (max-width: 480px){
				/*////// CLIENT-SPECIFIC STYLES //////*/
				body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */
				/* FRAMEWORK STYLES */
				/*
				CSS selectors are written in attribute
				selector format to prevent Yahoo Mail
				from rendering media query styles on
				desktop.
				*/
				/*td[class="textContent"], td[class="flexibleContainerCell"] { width: 100%; padding-left: 10px !important; padding-right: 10px !important; }*/
				table[id="emailHeader"],
				table[id="emailBody"],
				table[id="emailFooter"],
				table[class="flexibleContainer"],
				td[class="flexibleContainerCell"] {width:100% !important;}
				td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {display: block;width: 100%;text-align: left;}
				/*
				The following style rule makes any
				image classed with "flexibleImage"
				fluid when the query activates.
				Make sure you add an inline max-width
				to those images to prevent them
				from blowing out.
				*/
				td[class="imageContent"] img {height:auto !important; width:100% !important; max-width:100% !important; }
				img[class="flexibleImage"]{height:auto !important; width:100% !important;max-width:100% !important;}
				img[class="flexibleImageSmall"]{height:auto !important; width:auto !important;}
				/*
				Create top space for every second element in a block
				*/
				table[class="flexibleContainerBoxNext"]{padding-top: 10px !important;}
				/*
				Make buttons in the email span the
				full width of their container, allowing
				for left- or right-handed ease of use.
				*/
				table[class="emailButton"]{width:100% !important;}
				td[class="buttonContent"]{padding:0 !important;}
				td[class="buttonContent"] a{padding:15px !important;}
			}
			/*  CONDITIONS FOR ANDROID DEVICES ONLY
			*   http://developer.android.com/guide/webapps/targeting.html
			*   http://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/ ;
			=====================================================*/
			@media only screen and (-webkit-device-pixel-ratio:.75){
				/* Put CSS for low density (ldpi) Android layouts in here */
			}
			@media only screen and (-webkit-device-pixel-ratio:1){
				/* Put CSS for medium density (mdpi) Android layouts in here */
			}
			@media only screen and (-webkit-device-pixel-ratio:1.5){
				/* Put CSS for high density (hdpi) Android layouts in here */
			}
			/* end Android targeting */
			/* CONDITIONS FOR IOS DEVICES ONLY
			=====================================================*/
			@media only screen and (min-device-width : 320px) and (max-device-width:568px) {
			}
			/* end IOS targeting */
		</style>
		<!--
			Outlook Conditional CSS
			These two style blocks target Outlook 2007 & 2010 specifically, forcing
			columns into a single vertical stack as on mobile clients. This is
			primarily done to avoid the "page break bug" and is optional.
			More information here:
			http://templates.mailchimp.com/development/css/outlook-conditional-css
		-->
		<!--[if mso 12]>
			<style type="text/css">
				.flexibleContainer{display:block !important; width:100% !important;}
			</style>
		<![endif]-->
		<!--[if mso 14]>
			<style type="text/css">
				.flexibleContainer{display:block !important; width:100% !important;}
			</style>
		<![endif]-->
	</head>
	<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

		<!-- CENTER THE EMAIL // -->
		<!--
		1.  The center tag should normally put all the
			content in the middle of the email page.
			I added "table-layout: fixed;" style to force
			yahoomail which by default put the content left.
		2.  For hotmail and yahoomail, the contents of
			the email starts from this center, so we try to
			apply necessary styling e.g. background-color.
		-->
		<center style="background-color:#E1E1E1;">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
				<tr>
					<td align="center" valign="top" id="bodyCell">

						<!-- EMAIL HEADER // -->
						<!--
							The table "emailBody" is the email&apos;s container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

							<!-- HEADER ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<!--
																		The "invisibleIntroduction" is the text used for short preview
																		of the email before the user opens it (50 characters max). Sometimes,
																		you do not want to show this message depending on your design but this
																		text is highly recommended.
																		You do not have to worry if it is hidden, the next <td> will automatically
																		center and apply to the width 100% and also shrink to 50% if the first <td>
																		is visible.
																	-->
																	<td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																						All Steel Carports would like your feedback on the last Rep who visited with you!
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td align="right" valign="middle" class="flexibleContainerBox">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					<!-- CONTENT // -->
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																						If you can&apos;t see this message, <a href="#" target="_blank" style="text-decoration:none;border-bottom:1px solid #828282;color:#828282;"><span style="color:#828282;">view&nbsp;it&nbsp;in&nbsp;your&nbsp;browser</span></a>.
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // END -->

						</table>
						<!-- // END -->

						<!-- EMAIL BODY // -->
						<!--
							The table "emailBody" is the email&apos;s container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">

							<!-- MODULE ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<!--
										The centering table keeps the content
										tables centered in the emailBody table,
										in case its width is set to 100%.
									-->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#3498db">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<!--
													The flexible container has a set width
													that gets overridden by the media query.
													Most content tables within can then be
													given 100% widths.
												-->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<!--
															The content table is the first element
																that&apos;s entirely separate from the structural
																framework of the email.
															-->
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top" class="textContent">
                                    <img src="http://burtonsolution.tech/allsteel/marketforce/img/logo-allsteel.png"><br>
																		<!--<h1 style="color:#FFFFFF;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:35px;font-weight:normal;margin-bottom:5px;text-align:center;">Introduction header</h1>-->
																		<h2 style="text-align:center;font-weight:normal;font-family:Helvetica,Arial,sans-serif;font-size:23px;margin-bottom:10px;color:#205478;line-height:135%;">Affordable Buildings, Exceptional Quality!</h2>
																		<div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#FFFFFF;line-height:135%;">All Steel Carports continues to lead the industry in creating affordable, high quality options for the consumer.</div>
																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


						<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					<h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Dear ' . $dealer_name . ',</h3>
																					<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">All Steel Carports takes pride in not only providing high quality buildings, but high quality representatives. Our representative, <strong>' . $rep . '</strong>, visited your business on <strong>' . date("m/d/Y",strtotime($vd)) . '</strong>. Please click the link below to fill out a quick survey to provide feedback that will be helpful to us.</div>
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr style="padding-top:0;">
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td style="padding-top:0;" align="center" valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table border="0" cellpadding="0" cellspacing="0" width="50%" class="emailButton" style="background-color: #3498DB;">
																<tr>
																	<td align="center" valign="middle" class="buttonContent" style="padding-top:15px;padding-bottom:15px;padding-right:15px;padding-left:15px;">
																		<a style="color:#FFFFFF;text-decoration:none;font-family:Helvetica,Arial,sans-serif;font-size:20px;line-height:135%;" href="http://marketforceapp.com/marketforce/forms/rep-survey.php?vorg=' . $_SESSION['org_id'] . '&d=' . $dealer . '&r=' . $rep . '&vd=' . $vd . '" target="_blank">Quick Survey</a>
																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


							


							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					<!--<h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Dear valued customer,</h3>-->
																					<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">Thank you so much for your input in our training department.</div>
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


							<!-- MODULE ROW // -->
							<tr bgcolor="#3498db">
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td valign="top" class="imageContent" style="height:75px;">
																		<!--<img src="http://www.charlesmudy.com/respmail/respmail-full.jpg" width="500" class="flexibleImage" style="max-width:500px;width:100%;display:block;" alt="Text" title="Text" />-->
																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


							<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">
							<!-- FOOTER ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td valign="top" bgcolor="#E1E1E1">

																		<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																			<div>Copyright &#169; 2017 <a href="http://www.burtonsolution.tech" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">Market Force</span></a>. All&nbsp;rights&nbsp;reserved.</div>
																			<!--<div>If you do not want to recieve emails from us, you can <a href="#" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">unsubscribe</span></a>.</div>-->
																		</div>

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>

						</table>
						<!-- // END -->

					</td>
				</tr>
			</table>
		</center>
	</body>
</html>';

if($error != true){
if($vtype != 'Internal Note'){
	$mail->send();
		}
	}
	
//Add Support Ticket to change dealer status on Market Force Map
$mail->ClearAddresses();
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
$mail->addAddress('help@burtonsolution.on.spiceworks.com');
$mail->Subject = 'Dealer Status Update!';
$mail->Body = 'Please change the status of the following dealer on the map:<br>
							<br>
							' . $dnr['name'] . '<br>
							' . $dnr['address'] . '<br>
							' . $dnr['city'] . ', ' . $dnr['state'] . ' ' . $dnr['zip'] . '<br>
							Phone#: ' . $dnr['phone'] . '<br>
							Fax#: ' . $dnr['fax'] . '<br>
							Email: ' . $dnr['email'];

if($error != true){
	//$mail->send();//Turn back on to send the support ticket emails
	echo 'Your notes have been added to the system! Thank You!';
}else{
	echo 'An error has occured while entering your notes! The system administrator has been notified and your information has been saved!';
}
	
}else{
	echo 'You have already added a note for this dealer today!';
}
}
?>