<?php
include 'connection.php';

//Load Variables
$id = $_GET['id'];
$filter = $_GET['filter'];
$userid = $_GET['uid'];

$dnq = "SELECT * FROM `dealers` WHERE `ID` = '" . $id . "'";
$dng = mysqli_query($conn, $dnq) or die($conn->error);
$dnr = mysqli_fetch_array($dng);

if($filter != 'single'){
echo '<input type="radio" name="filter" id="filter" value="multi" onchange="filters();" checked/> Show All
      &nbsp;&nbsp;
			<input type="radio" name="filter" id="filter" value="single" onchange="filters();" /> Show Only My Notes
			<br>';
}else{
echo '<input type="radio" name="filter" id="filter" value="multi" onchange="filters();" /> Show All
      &nbsp;&nbsp;
			<input type="radio" name="filter" id="filter" value="single" onchange="filters();" checked/> Show Only My Notes
			<br>';
}

echo '<h1>' . $dnr['name'] . '</h1>';

if($_GET['lookup'] == 'yes'){
  $imgq = "SELECT * FROM `dealer_imgs` WHERE `dealer_id` = '" . $id . "'";
  $imgg = mysqli_query($conn, $imgq) or die($conn->error);
  if(mysqli_num_rows($imgg) == 0){
    
  }else{
    echo '<div class="image_gallery">';
    while($imgr = mysqli_fetch_array($imgg)){
      echo '<a href="' . $imgr['url'] . '" target="_blank">
      <img src="' . $imgr['url'] . '" style="width:30%;" />
      </a>';
    }
    echo '</div>';
  }
}

//Get Notes
if($filter == 'single'){
$q = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $id . "' AND `user` = '" . $userid . "' ORDER BY `ID` DESC";
}
if($filter == 'multi'){
$q = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $id . "' ORDER BY `ID` DESC";
}
if(!$_GET['filter']){
$q = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $id . "' ORDER BY `ID` DESC";
}
$g = mysqli_query($conn, $q) or die($conn->error);
if(mysqli_num_rows($g) == 0){
    echo '<h1 style="text-align: center; color:red;">There are no notes to display...</h1>';
  }else{
while($r = mysqli_fetch_array($g)){
  
//Get Dealers Name
  $dealq = "SELECT * FROM `dealers` WHERE `ID` = '" . $id . "'";
  $dealg = mysqli_query($conn, $dealq) or die($conn->error);
  $dealr = mysqli_fetch_array($dealg);
  
  //Check if Cold Call
  if(strpos($id,'CCP') !== false){
    $pnq = "SELECT * FROM `prospects` WHERE `PID` = '" . $id . "'";
    $png = mysqli_query($conn, $pnq) or die($conn->error);
    $pnr = mysqli_fetch_array($png);
    $dname = str_replace('-xxx-','&',$pnr['bizname']);
    echo '<h1>' . $dname . '</h1>';
    $adder = '(Prospect)';
  }else{
  $dname = $dealr['name'];
  }
  
  //Get User's Name
$uq = "SELECT * FROM `users` WHERE `ID` = '" . $r['user'] . "'";
$ug = mysqli_query($conn, $uq) or die($conn->error);
$u = mysqli_fetch_array($ug);
  
  $date = strtotime($r['date']);
  echo ' <div class="panel panel-green">
         <div class="panel-heading">
        <h3 class="panel-title">' . date('m/d/Y (l)', $date) . ' ' . date('h:iA', strtotime($r['time'])) . ' by <strong>' . $u['fname'] . ' ' . $u['lname'] . '</strong> | ' . $dname . ' ' . $adder . '</h3>
        </div>
        <div class="panel-body">
        <h4>Contact: <span style="color: blue;">' . $r['name'] . '</span> &nbsp;&nbsp; | &nbsp;&nbsp; Visit Type: <span style="color: blue;">' . $r['vtype'] . '</span></h4>
       <u><h4>Notes From Visit:</h4></u>
        <div class="well">
        <p><strong>' . str_replace('-xxx-','&',$r['notes']) . '</strong></p>
        </div>
        <u><h4>Actions Taken While There:</h4></u>
        <ul class="main">
          <li>
            <ul>
              <div class="alert alert-info">
              <li><u><p>Waivers & Forms:</p></u></li>';
  
 for ($x = 1; $x <= 6; $x++){
   if($r['cb'.$x] != 'undefined'){
     echo '<li>- ' . $r['cb'.$x] . '</li>';
   }
 }
             
        echo'</div>
            </ul>
         </li>
         <li>
            <ul>
              <div class="alert alert-info">
              <li><u><p>Highlighted Area:</p></u></li>';
   for ($x = 7; $x <= 11; $x++){
   if($r['cb'.$x] != 'undefined'){
     echo '<li>- ' . $r['cb'.$x] . '</li>';
   }
 }
            
        echo' </div>
            </ul>
          </li>
          <li>
            <ul>
            <div class="alert alert-info">
              <li><u><p>Supplies:</p></u></li>';
   for ($x = 12; $x <= 15; $x++){
   if($r['cb'.$x] != 'undefined'){
     echo '<li>- ' . $r['cb'.$x] . '</li>';
   }
 }
             
       echo ' </div>
             </ul>
           </li>
           <li>
              <ul>
                <div class="alert alert-info">
                <li><u><p>Dealer Login:</p></u></li>';
   for ($x = 16; $x <= 18; $x++){
   if($r['cb'.$x] != 'undefined'){
     echo '<li>- ' . $r['cb'.$x] . '</li>';
   }
 }
                
               echo ' </div>
              </ul>
            </li>
            <li>
              <ul>
                <div class="alert alert-info">
                <li><u><p>Tasks:</p></u></li>';
   for ($x = 19; $x <= 25; $x++){
   if($r['cb'.$x] != 'undefined'){
     echo '<li>- ' . $r['cb'.$x] . '</li>';
   }
 }
              
           echo '</div>
               </ul>
             </li>
						 
						 <li>
              <ul>
                <div class="alert alert-info">
                <li><u><p>Price Training:</p></u></li>';
   for ($x = 1; $x <= 9; $x++){
   if($r['pb'.$x] != 'undefined'){
     echo '<li>- ' . $r['pb'.$x] . '</li>';
   }
 }
              
           echo '</div>
               </ul>
             </li>
						 
           </ul>
     </div>   
   </div>';
}
}
?>











