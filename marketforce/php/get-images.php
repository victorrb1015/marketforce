<?
session_start();

include 'connection.php';

$id = $_GET['id'];
?>
<html>
  <head>
    <script src="../js/load-image.all.min.js"></script>
    <title>Dealer Lot Images</title>
    <style>
      img {
        image-orientation: from-image;
        width:45%;
        float:left;
        padding: 5px;
      }
      div{
        float:left;
      }
      .main{
        text-align:center;
      }
      h3{
        color:red;
      }
    </style>
    <script>
      var rep_name = '<? echo $_SESSION['full_name']; ?>';
      var rep_id = '<? echo $_SESSION['rep_id']; ?>';
      
      function remove_img(id){
        if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","remove-dealer-img.php?id="+id+"&rep_id="+rep_id+"&rep_name="+rep_name,true);
  xmlhttp.send();
      }
    </script>
  </head>
  <body>
    <div id="main" class="main">
      <h1><u>Dealer Lot Images:</u></h1>
      <h3>(Click Image If Rotated Incorrectly)</h3>
       <?php
    $q = "SELECT * FROM `dealer_imgs` WHERE `dealer_id` = '" . $id . "' AND `inactive` != 'Yes'";
    $g = mysqli_query($conn, $q) or die($conn->error);
    while($r = mysqli_fetch_array($g)){
      
      //echo '<iframe src="' . $r['url'] . '"></iframe>';
      echo '<div>';
      echo '<a href="' . $r['url'] . '" target="_blank">
            <img src="' . $r['url'] . '" style="width:100%;" />
            </a>';
      echo '<a href="Javascript:remove_img(' . $r['ID'] . ');">Remove Image</a>';
      echo '</div>';
      
    }
      ?>
      
    </div>
  </body>
</html>