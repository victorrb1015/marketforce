<?php
include 'connection.php';
error_reporting(E_ALL);
//Load Variables
$email = mysqli_real_escape_string($mf_conn,$_POST['email']);
$org = mysqli_real_escape_string($mf_conn, $_POST['org']);

//Get Org Database Name...
$dbq = "SELECT * FROM `organizations` WHERE `org_id` = '" . $org . "'";
$dbg = mysqli_query($mf_conn, $dbq) or die($mf_conn->error);
$dbr = mysqli_fetch_array($dbg);
$_SESSION['org_db_name'] = $dbr['db_name'];
$conn = mysqli_connect('localhost','marketfo_mf','#NgTFJQ!z@t8',$_SESSION['org_db_name']) or die($conn->error);

//Check if exists in database...
$vq = "SELECT * FROM `users` WHERE `email` = '" . $email . "' AND `inactive` != 'Yes'";
$vg = mysqli_query($conn, $vq) or die($conn->error);
if(mysqli_num_rows($vg) != 0){//Account found with this email...
  //Load information from database
  $vr = mysqli_fetch_array($vg);
  
  //Setup Email settings
  include 'phpmailer/PHPMailerAutoload.php';
  $mail = new PHPMailer();
  include 'phpmailsettings.php';
  
  $mail->setFrom('no-reply@allsteelcarports.com','Market Force');
  $mail->addAddress($vr['email']);
  $mail->addBCC('archive@ignition-innovations.com');
  $mail->Subject = 'Forgot Password!';
  $mail->Body = '<html>
                  <head>
                  </head>
                  <body>
                    <div class="main">
                      <h1 style="text-align:center;">Your Market Force Credentials:</h1>
                      <h3>Username: ' . $vr['username'] . '</h3>
                      <h3>Password: ' . $vr['password'] . '</h3>
                    </div>
                  </body>
                  </html>';
  
  $mail->send();//Send the email
  
  $response = 'Your account credentials have been sent to this email address!';
  echo '<script>
        window.location = "../forgot_password.php?response=' . $response . '";
        </script>';
  
}else{//No account found with email...
  $response = 'There is no Market Force account associated with this email address!';
  echo '<script>
        window.location = "../forgot_password.php?response=' . $response . '";
        </script>';
}