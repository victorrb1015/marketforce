<?php
include 'connection.php';

//Load Variables 
$q = $_GET['q'];
$s = $_GET['s'];
$inum = $_GET['i'];
$cat = $_GET['cat'];
$roof_style = $_GET['roof_style'];
$width = $_GET['width'];
$roof_length = $_GET['roof_length'];
$frame_length = $_GET['frame_length'];
$leg_height = $_GET['leg_height'];
$gauge = $_GET['gauge'];
$roof_color = $_GET['roof_color'];
$price = $_GET['price'];
$ex1 = $_GET['ex1'];
$ex2 = $_GET['ex2'];
$ex3 = $_GET['ex3'];
$ex4 = $_GET['ex4'];
$ex5 = $_GET['ex5'];
$state = $_GET['state'];
$city = $_GET['city'];
$sold = $_GET['sold'];
$id = $_GET['id'];


//Check if invoice Number exists
$eq = "SELECT * FROM `listings` WHERE `invoice#` = '" . $inum . "'";
$eg = mysqli_query($conn, $eq) or die($conn->error);
$er = mysqli_fetch_array($eg);
if($q == 'edit'){
  if(mysqli_num_rows($eg) == 0){
    echo 'The invoice number you entered does not exist! <button type="button" onclick="res_edit();">Retry</button>';
  }else{
    echo '<div style="">
            <form id="edit_form">
            <table style="margin: auto;">
            <tr>
            <td>Invoice #: </td>
            <td><input type="text" id="invoice_num" value="' . $er['invoice#'] . '" /></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Category: </td>
            <td>';
            if($er['category'] == 'Carport'){
              echo '<input type="radio" name="cat" value="Carport" checked/> Carport <br>';
            }else{
              echo '<input type="radio" name="cat" value="Carport" /> Carport <br>';
            }
            if($er['category'] == 'Garage'){
              echo '<input type="radio" name="cat" value="Garage" checked/> Garage <br>';
            }else{
              echo '<input type="radio" name="cat" value="Garage" /> Garage <br>';
            }
            if($er['category'] == 'Utility Building'){
              echo '<input type="radio" name="cat" value="Utility Building" checked/> Utility Building <br>';
            }else{
              echo '<input type="radio" name="cat" value="Utility Building" /> Utility Building <br>';
            }
						if($er['category'] == 'Truss Building'){
              echo '<input type="radio" name="cat" value="Truss Building" checked/> Truss Building <br>';
            }else{
              echo '<input type="radio" name="cat" value="Truss Building" /> Truss Building <br>';
            }
						if($er['category'] == 'Agriculture Building'){
              echo '<input type="radio" name="cat" value="Agriculture Building" checked/> Agriculture Building <br>';
            }else{
              echo '<input type="radio" name="cat" value="Agriculture Building" /> Agriculture Building <br>';
            }
						if($er['category'] == 'Shed/Carport Combo Unit'){
              echo '<input type="radio" name="cat" value="Shed/Carport Combo Unit" checked/> Shed/Carport Combo Unit <br>';
            }else{
              echo '<input type="radio" name="cat" value="Shed/Carport Combo Unit" /> Shed/Carport Combo Unit <br>';
            }
            echo '</td> 
            <!--<td><select id="cat" value="' . $er['category'] . '">
                  <option value="default">Select A Category...</option>
                  <option value="Carport">Carport</option>
                  <option value="Garage">Garage</option>
                  <option value="Utility Building">Utility Building</option>
                 </select>
            </td>-->
            </tr>
            <tr>
            <td>Roof Style: </td>
            <td>';
            if($er['roof_style'] == 'A-Frame'){
              echo '<input type="radio" name="roof_style" value="A-Frame" checked/> A-Frame <br>';
            }else{
              echo '<input type="radio" name="roof_style" value="A-Frame" /> A-Frame <br>';
            }
            if($er['roof_style'] == 'Regular Roof'){
              echo '<input type="radio" name="roof_style" value="Regular Roof" checked/> Regular Roof <br>';
            }else{
              echo '<input type="radio" name="roof_style" value="Regular Roof" /> Regular Roof <br>';
            }
            if($er['roof_style'] == 'Vertical Roof'){
              echo '<input type="radio" name="roof_style" value="Vertical Roof" checked/> Vertical Roof <br>';
            }else{
              echo '<input type="radio" name="roof_style" value="Vertical Roof" /> Vertical Roof <br>';
            }
            echo '</td>
            <!--<td><select id="roof_style" value="' . $er['roof_style'] . '">
                  <option value="default">Select A Roof Style...</option>
                  <option value="A-Frame">A-Frame</option>
                  <option value="Regular Frame">Regular Frame</option>
                  <option value="Vertical Roof">Vertical Roof</option>
                </select>
            </td>-->
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Width: </td>
            <td><input type="text" id="width" value="' . $er['width'] . '"/></td>
            </tr>
            <tr>
            <td>Roof Length: </td>
            <td><input type="text" id="roof_length" value="' . $er['roof_length'] . '"/></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Frame Length: </td>
            <td><input type="text" id="frame_length" value="' . $er['frame_length'] . '"/></td>
            </tr>
            <tr>
            <td>Leg Height: </td>
            <td><input type="text" id="leg_height" value="' . $er['leg_height'] . '"/></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Gauge: </td>
            <td><input type="text" id="gauge" value="' . $er['gauge'] . '"/></td>
            </tr>
            <tr>
            <td>Roof Color: </td>
            <td><input type="text" id="roof_color" value="' . $er['roof_color'] . '"/></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Price: </td>
            <td>$<input type="text" id="price" value="' . $er['price'] . '"/></td>
            </tr>
            <tr>
            <td>Extra Option 1: </td>
            <td><input type="text" id="ex1" value="' . $er['ex1'] . '"/></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Extra Option 2: </td>
            <td><input type="text" id="ex2" value="' . $er['ex2'] . '"/></td>
            </tr>
            <tr>
            <td>Extra Option 3: </td>
            <td><input type="text" id="ex3" value="' . $er['ex3'] . '"/></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Extra Option 4: </td>
            <td><input type="text" id="ex4" value="' . $er['ex4'] . '"/></td>
            </tr>
            <tr>
            <td>Extra Option 5: </td>
            <td><input type="text" id="ex5" value="' . $er['ex5'] . '"/></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>State: </td>
            <td>';
            if($er['state'] == 'Indiana'){
              echo '<input type="radio" name="state" value="Indiana" checked/> Indiana <br>';
            }else{
              echo '<input type="radio" name="state" value="Indiana" /> Indiana <br>';
            }
            if($er['state'] == 'Ohio'){
              echo '<input type="radio" name="state" value="Ohio" checked/> Ohio <br>';
            }else{
              echo '<input type="radio" name="state" value="Ohio" /> Ohio <br>';
            }
            if($er['state'] == 'Oklahoma'){
              echo '<input type="radio" name="state" value="Oklahoma" checked/> Oklahoma <br>';
            }else{
              echo '<input type="radio" name="state" value="Oklahoma" /> Oklahoma <br>';
            }
            if($er['state'] == 'Michigan'){
              echo '<input type="radio" name="state" value="Michigan" checked/> Michigan <br>';
            }else{
              echo '<input type="radio" name="state" value="Michigan" /> Michigan <br>';
            }
            echo '</td>
            <!--<td><select id="state" value="' . $er['state'] . '">
                  <option value="default">Select A State...</option>
                  <option value="Indiana">Indiana</option>
                  <option value="Ohio">Ohio</option>
                  <option value="Oklahoma">Oklahoma</option>
                  <option value="Texas">Texas</option>
                </select>
            </td>-->
            </tr>
            <tr>
            <td>City: </td>
            <td><input type="text" id="city" value="' . $er['city'] . '"/></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Sold? </td>
            <td>';
            if($er['sold'] == 'Yes'){
              echo 'Yes <input type="radio" value="Yes" name="sold" checked/>';
              echo 'No <input type="radio" value="No" name="sold" />';
            }else{
              echo 'Yes <input type="radio" value="Yes" name="sold" />';
              echo 'No <input type="radio" value="No" name="sold" checked/>';
            }
            echo '</td>
            </tr>
            <tr>
            <td>
            <button type="button" onclick="check(&apos;exist&apos;);">Save</button>
            <button type="button" onclick="res_edit();">Go Back</button>
            <input type="hidden" id="id" value="' . $er['ID'] . '" />
            </td>
            <td></td>
            </tr>
            </table>
            </form>
            </div>';
            
            
  }
}

if($q == 'new'){
  if(mysqli_num_rows($eg) == 0){
    echo '<div style="">
            <form id="edit_form">
            <table style="margin: auto;">
            <tr>
            <td>Invoice #: </td>
            <td><input type="text" id="invoice_num" value="' . $inum . '" /></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Category: </td>
            <td>
            <input type="radio" name="cat" value="Carport" /> Carport <br>
            <input type="radio" name="cat" value="Garage" /> Garage <br>
            <input type="radio" name="cat" value="Utility Building" /> Utility Building <br>
						<input type="radio" name="cat" value="Truss Building" /> Truss Building <br>
						<input type="radio" name="cat" value="Agriculture Building" /> Agriculture Building <br>
						<input type="radio" name="cat" value="Shed/Carport Combo Unit" /> Shed/Carport Combo Unit <br>
            </td>
            </tr>
            <tr>
            <td>Roof Style: </td>
            <td>
            <input type="radio" name="roof_style" value="A-Frame" /> A-Frame <br>  
            <input type="radio" name="roof_style" value="Regular Roof" /> Regular Roof <br>
            <input type="radio" name="roof_style" value="Vertical Roof" /> Vertical Roof <br>
            </td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Width: </td>
            <td><input type="text" id="width" /></td>
            </tr>
            <tr>
            <td>Roof Length: </td>
            <td><input type="text" id="roof_length" /></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Frame Length: </td>
            <td><input type="text" id="frame_length" /></td>
            </tr>
            <tr>
            <td>Leg Height: </td>
            <td><input type="text" id="leg_height" /></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Gauge: </td>
            <td><input type="text" id="gauge" /></td>
            </tr>
            <tr>
            <td>Roof Color: </td>
            <td><input type="text" id="roof_color" /></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Price: </td>
            <td>$<input type="text" id="price" /></td>
            </tr>
            <tr>
            <td>Extra Option 1: </td>
            <td><input type="text" id="ex1" /></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Extra Option 2: </td>
            <td><input type="text" id="ex2" /></td>
            </tr>
            <tr>
            <td>Extra Option 3: </td>
            <td><input type="text" id="ex3" /></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Extra Option 4: </td>
            <td><input type="text" id="ex4" /></td>
            </tr>
            <tr>
            <td>Extra Option 5: </td>
            <td><input type="text" id="ex5" /></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>State: </td>
            <td>
            <input type="radio" name="state" value="Indiana" /> Indiana <br>
            <input type="radio" name="state" value="Ohio" /> Ohio <br>
            <input type="radio" name="state" value="Oklahoma" /> Oklahoma <br>
            <input type="radio" name="state" value="Michigan" /> Michigan <br>
            </td>
            </tr>
            <tr>
            <td>City: </td>
            <td><input type="text" id="city" /></td>
            </tr>
            <tr style="background: #E3E3E5;">
            <td>Sold? </td>
            <td>
            Yes <input type="radio" value="Yes" name="sold" /><br>
            No <input type="radio" value="No" name="sold" />
            </td>
            </tr>
            <tr>
            <td>
            <button type="button" id="adder_btn" onclick="check(&apos;new&apos;);">ADD</button>
            <button type="button" onclick="res_edit();">Go Back</button>
            <input type="hidden" id="id" value="1" />
            </td>
            <td></td>
            </tr>
            </table>
            </form>
            </div>';
  }else{
    echo 'That invoice number already exists! <button type="button" onclick="res_edit();">Retry</button>';
  }
}


if($s == 'new'){
	//Verify invoice does not exist
	$ivq = "SELECT * FROM `listings` WHERE `invoice#` = '" . $inum . "'";
	$ivg = mysqli_query($conn,$ivq) or die($conn->error);
	if(mysqli_num_rows($ivg) == 0){
  $nq = "INSERT 
          INTO 
          `listings`
          (`invoice#`,
          `category`,
          `roof_style`,
          `width`,
          `roof_length`,
          `frame_length`,
          `leg_height`,
          `gauge`,
          `roof_color`,
          `price`,
          `ex1`,
          `ex2`,
          `ex3`,
          `ex4`,
          `ex5`,
          `state`,
          `city`,
          `sold`)
          VALUES
          ('" . $inum . "',
          '" . $cat . "',
          '" . $roof_style . "',
          '" . $width . "',
          '" . $roof_length . "',
          '" . $frame_length . "',
          '" . $leg_height . "',
          '" . $gauge . "',
          '" . $roof_color . "',
          '" . $price . "',
          '" . $ex1 . "',
          '" . $ex2 . "',
          '" . $ex3 . "',
          '" . $ex4 . "',
          '" . $ex5 . "',
          '" . $state . "',
          '" . $city . "',
          '" . $sold . "')";
  
  $ng = mysqli_query($conn, $nq) or die($conn->error);
	}else{
		echo 'There is already an invoice listed in the system with invoice#: ' . $inum . ' <button type="button" onclick="res_edit();">Retry</button>';
		break;
	}
  
  //Email the listing...
  
  
  //Load Variables
$listing = $inum;
$send = 'Yes';

//Get info on listing
$l = "SELECT * FROM `listings` WHERE `invoice#` = '" . $listing . "'";
$lg = mysqli_query($conn, $l) or die($conn->error);
$lr = mysqli_fetch_array($lg);

include '../php/phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include '../php/phpmailsettings.php';

//Email Parameters
$admin = "archive@ignition-innovations.com";
$email = $admin;
$mail->setFrom('sales@allsteelcarports.com', 'All Steel Carports');
//$mail->addAddress($email);
$mail->addBCC($admin);
$mail->Subject = "New Pre-Owned Listing!";
$mail->Body = '
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no" /> <!-- disable auto telephone linking in iOS -->
		<title>New Preowned Listing! | All Steel Carports</title>
		<style type="text/css">
			/* RESET STYLES */
			html { background-color:#E1E1E1; margin:0; padding:0; }
			body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, "Lucida Grande", sans-serif;}
			table{border-collapse:collapse;}
			table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#7A7A7A;font-weight:normal;}
			img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
			a {text-decoration:none !important;border-bottom: 1px solid;}
			h1, h2, h3, h4, h5, h6{color:#5F5F5F; font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}
			/* CLIENT-SPECIFIC STYLES */
			.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */
			table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */
			#outlook a{padding:0;} /* Force Outlook 2007 and up to provide a "view in browser" message. */
			img{-ms-interpolation-mode: bicubic;display:block;outline:none; text-decoration:none;} /* Force IE to smoothly render resized images. */
			body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; font-weight:normal!important;} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
			.ExternalClass td[class="ecxflexibleContainerBox"] h3 {padding-top: 10px !important;} /* Force hotmail to push 2-grid sub headers down */
			/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */
			/* ========== Page Styles ========== */
			h1{display:block;font-size:26px;font-style:normal;font-weight:normal;line-height:100%;}
			h2{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:120%;}
			h3{display:block;font-size:17px;font-style:normal;font-weight:normal;line-height:110%;}
			h4{display:block;font-size:18px;font-style:italic;font-weight:normal;line-height:100%;}
			.flexibleImage{height:auto;}
			.linkRemoveBorder{border-bottom:0 !important;}
			table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}
			body, #bodyTable{background-color:#E1E1E1;}
			#emailHeader{background-color:#E1E1E1;}
			#emailBody{background-color:#FFFFFF;}
			#emailFooter{background-color:#E1E1E1;}
			.nestedContainer{background-color:#F8F8F8; border:1px solid #CCCCCC;}
			.emailButton{background-color:#205478; border-collapse:separate;}
			.buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
			.buttonContent a{color:#FFFFFF; display:block; text-decoration:none!important; border:0!important;}
			.emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}
			.emailCalendarMonth{background-color:#205478; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}
			.emailCalendarDay{color:#205478; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}
			.imageContentText {margin-top: 10px;line-height:0;}
			.imageContentText a {line-height:0;}
			#invisibleIntroduction {display:none !important;} /* Removing the introduction text from the view */
			/*FRAMEWORK HACKS & OVERRIDES */
			span[class=ios-color-hack] a {color:#275100!important;text-decoration:none!important;} /* Remove all link colors in IOS (below are duplicates based on the color preference) */
			span[class=ios-color-hack2] a {color:#205478!important;text-decoration:none!important;}
			span[class=ios-color-hack3] a {color:#8B8B8B!important;text-decoration:none!important;}
			/* A nice and clean way to target phone numbers you want clickable and avoid a mobile phone from linking other numbers that look like, but are not phone numbers.  Use these two blocks of code to "unstyle" any numbers that may be linked.  The second block gives you a class to apply with a span tag to the numbers you would like linked and styled.
			Inspired by Campaign Monitor&apos;s article on using phone numbers in email: http://www.campaignmonitor.com/blog/post/3571/using-phone-numbers-in-html-email/.
			*/
			.a[href^="tel"], a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:none!important;cursor:default!important;}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:auto!important;cursor:default!important;}
			/* MOBILE STYLES */
			@media only screen and (max-width: 480px){
				/*////// CLIENT-SPECIFIC STYLES //////*/
				body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */
				/* FRAMEWORK STYLES */
				/*
				CSS selectors are written in attribute
				selector format to prevent Yahoo Mail
				from rendering media query styles on
				desktop.
				*/
				/*td[class="textContent"], td[class="flexibleContainerCell"] { width: 100%; padding-left: 10px !important; padding-right: 10px !important; }*/
				table[id="emailHeader"],
				table[id="emailBody"],
				table[id="emailFooter"],
				table[class="flexibleContainer"],
				td[class="flexibleContainerCell"] {width:100% !important;}
				td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {display: block;width: 100%;text-align: left;}
				/*
				The following style rule makes any
				image classed with "flexibleImage"
				fluid when the query activates.
				Make sure you add an inline max-width
				to those images to prevent them
				from blowing out.
				*/
				td[class="imageContent"] img {height:auto !important; width:100% !important; max-width:100% !important; }
				img[class="flexibleImage"]{height:auto !important; width:100% !important;max-width:100% !important;}
				img[class="flexibleImageSmall"]{height:auto !important; width:auto !important;}
				/*
				Create top space for every second element in a block
				*/
				table[class="flexibleContainerBoxNext"]{padding-top: 10px !important;}
				/*
				Make buttons in the email span the
				full width of their container, allowing
				for left- or right-handed ease of use.
				*/
				table[class="emailButton"]{width:100% !important;}
				td[class="buttonContent"]{padding:0 !important;}
				td[class="buttonContent"] a{padding:15px !important;}
			}
			/*  CONDITIONS FOR ANDROID DEVICES ONLY
			*   http://developer.android.com/guide/webapps/targeting.html
			*   http://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/ ;
			=====================================================*/
			@media only screen and (-webkit-device-pixel-ratio:.75){
				/* Put CSS for low density (ldpi) Android layouts in here */
			}
			@media only screen and (-webkit-device-pixel-ratio:1){
				/* Put CSS for medium density (mdpi) Android layouts in here */
			}
			@media only screen and (-webkit-device-pixel-ratio:1.5){
				/* Put CSS for high density (hdpi) Android layouts in here */
			}
			/* end Android targeting */
			/* CONDITIONS FOR IOS DEVICES ONLY
			=====================================================*/
			@media only screen and (min-device-width : 320px) and (max-device-width:568px) {
			}
			/* end IOS targeting */
		</style>
		<!--
			Outlook Conditional CSS
			These two style blocks target Outlook 2007 & 2010 specifically, forcing
			columns into a single vertical stack as on mobile clients. This is
			primarily done to avoid the "page break bug" and is optional.
			More information here:
			http://templates.mailchimp.com/development/css/outlook-conditional-css
		-->
		<!--[if mso 12]>
			<style type="text/css">
				.flexibleContainer{display:block !important; width:100% !important;}
			</style>
		<![endif]-->
		<!--[if mso 14]>
			<style type="text/css">
				.flexibleContainer{display:block !important; width:100% !important;}
			</style>
		<![endif]-->
	</head>
	<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

		<!-- CENTER THE EMAIL // -->
		<!--
		1.  The center tag should normally put all the
			content in the middle of the email page.
			I added "table-layout: fixed;" style to force
			yahoomail which by default put the content left.
		2.  For hotmail and yahoomail, the contents of
			the email starts from this center, so we try to
			apply necessary styling e.g. background-color.
		-->
		<center style="background-color:#E1E1E1;">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
				<tr>
					<td align="center" valign="top" id="bodyCell">

						<!-- EMAIL HEADER // -->
						<!--
							The table "emailBody" is the email&apos;s container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

							<!-- HEADER ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<!--
																		The "invisibleIntroduction" is the text used for short preview
																		of the email before the user opens it (50 characters max). Sometimes,
																		you do not want to show this message depending on your design but this
																		text is highly recommended.
																		You do not have to worry if it is hidden, the next <td> will automatically
																		center and apply to the width 100% and also shrink to 50% if the first <td>
																		is visible.
																	-->
																	<td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																						A New Pre-Owned Listing From All Steel Carports
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td align="right" valign="middle" class="flexibleContainerBox">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					<!-- CONTENT // -->
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																						<!--If you can&apos;t see this message, <a href="#" target="_blank" style="text-decoration:none;border-bottom:1px solid #828282;color:#828282;"><span style="color:#828282;">view&nbsp;it&nbsp;in&nbsp;your&nbsp;browser</span></a>.-->
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // END -->

						</table>
						<!-- // END -->

						<!-- EMAIL BODY // -->
						<!--
							The table "emailBody" is the email&apos;s container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">

							<!-- MODULE ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<!--
										The centering table keeps the content
										tables centered in the emailBody table,
										in case its width is set to 100%.
									-->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#3498db">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<!--
													The flexible container has a set width
													that gets overridden by the media query.
													Most content tables within can then be
													given 100% widths.
												-->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<!--
															The content table is the first element
																that&apos;s entirely separate from the structural
																framework of the email.
															-->
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top" class="textContent">
                                    <img src="http://burtonsolution.tech/allsteel/marketforce/img/logo2.png"><br>
																		<!--<h1 style="color:#FFFFFF;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:35px;font-weight:normal;margin-bottom:5px;text-align:center;">Introduction header</h1>-->
																		<h2 style="text-align:center;font-weight:normal;font-family:Helvetica,Arial,sans-serif;font-size:23px;margin-bottom:10px;color:#205478;line-height:135%;">Affordable Buildings, Exceptional Quality!</h2>
																		<div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:25px;margin-bottom:0;color:#FFFFFF;line-height:135%;">New Pre-Owned Listing!</div>
																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


						<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					<!--<h3 mc:edit="header" style="color:black;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Email To: &nbsp;&nbsp;' . $cemail . '
																					<br>
																					Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2/1/2017
																					<br>
																					Topic: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New Pre-Owned Listing!</h3>
																					-->
                                          <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:17px;margin-bottom:0;color:black;line-height:135%;">
                                          <br>
                                          <!--MESSAGE HERE-->    
																					We wanted you to be the first to know that we have another new listing on our pre-owned site!
                                          <br><br>
                                          Our new listing is for a <strong>' . $lr['category'] . '</strong>:<br><br>
                                          <strong>Roof Style:</strong> ' . $lr['roof_style'] . '<br>
                                          <strong>Width:</strong> ' . $lr['width'] . '<br>
                                          <strong>Roof Length:</strong> ' . $lr['roof_length'] . '<br>
                                          <strong>Frame Length:</strong> ' . $lr['frame_length'] . '<br>
                                          <strong>Leg Height:</strong> ' . $lr['leg_height'] . '<br>
                                          <strong>Gauge:</strong> ' . $lr['gauge'] . '<br>
                                          <strong>Roof Color:</strong> ' . $lr['roof_color'] . '<br>
                                          <br>
                                          <strong><u>Extra Options:</u></strong><br>
                                          ' . $lr['ex1'] . '<br>
                                          ' . $lr['ex2'] . '<br>
                                          ' . $lr['ex3'] . '<br>
                                          ' . $lr['ex4'] . '<br>
                                          ' . $lr['ex5'] . '<br>
                                          <br>
                                          <strong>Item#: </strong>' . $lr['invoice#'] . '<br>
                                          <strong>Location:</strong> ' . $lr['city'] . ', ' . $lr['state'] . '<br><br>
                                          <strong><span style="font-size: 20px; color: green;">Price: $' . $lr['price'] . ' <small>(+Tax)</small></span></strong>
                                          <br><br>
                                          Thank you very much!
                                          <br>
                                          <strong>All Steel Carports</strong>
                                         </div>
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->



							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					<!--<h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Dear valued customer,</h3>-->
																					<!--<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">Thank you so much for your input in our training department. Together, what we can easily see is only a small percentage of what is possible!</div>-->
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->




              <!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#3498db">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																					<!--
																						The "mc:edit" is a feature for MailChimp which allows
																						you to edit certain row. It makes it easy for you to quickly edit row sections.
																						http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
																					-->
																					
																					<div mc:edit="body" style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:20px;margin-bottom:0;color:#000000;line-height:135%;">
                                          <strong>All Steel Sites
                                          <br><br>All Steel Caports: &nbsp; <br><a href="http://www.allsteelcarports.com" style="color: black; list-style-type: none;">www.allsteelcarports.com</a>
                                          <br><br>Pre-Owned: &nbsp; <a href="http://www.allsteelcarports.com/preowned" style="color: black; list-style-type: none;">www.allsteelcarports.com/preowned</a>
                                          <br>
                                          </strong>
                                         </div>
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->
							


							<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">
							<!-- FOOTER ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td valign="top" bgcolor="#E1E1E1">

																		<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																			<div>Copyright &#169; 2017 <a href="http://www.burtonsolution.tech" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">Market Force</span></a>. All&nbsp;rights&nbsp;reserved.</div>
																			<!--<div>If you do not want to recieve emails from us, you can <a href="#" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">unsubscribe</span></a>.</div>-->
																		</div>

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>

						</table>
						<!-- // END -->

					</td>
				</tr>
			</table>
		</center>
	</body>
</html>';

if($send == 'Yes'){
  $e = "SELECT * FROM `subscribers`";
  $eg = mysqli_query($conn, $e) or die($conn->error);
  while($er = mysqli_fetch_array($eg)){
    $mail->addAddress($er['email']);
    echo 'Listing has been sent to ' . $er['email'] . '<br>';
  }
  //$mail->send();
}
  
  echo '<p style="text-align: center;">Your new listing has been added! Thank You!</p>
        <button type="button" onclick="res_edit();">Go Back</button>
        <!--<button type="button" onclick="send_ad(&apos;No&apos;,&apos;Yes&apos;);">
        Send Listing To Subscribers
        </button>-->
        <input type="hidden" id="invnum" value="' . $inum . '" />';
}


if($s == 'exist'){
  $uq = "UPDATE 
        `listings`
        SET
        `invoice#` = '" . $inum . "',
        `category` = '" . $cat . "',
        `roof_style` = '" . $roof_style . "',
        `width` = '" . $width . "',
        `roof_length` = '" . $roof_length . "',
        `frame_length` = '" . $frame_length . "',
        `leg_height` = '" . $leg_height . "',
        `gauge` = '" . $gauge . "',
        `roof_color` = '" . $roof_color . "',
        `price` = '" . $price . "',
        `ex1` = '" . $ex1 . "',
        `ex2` = '" . $ex2 . "',
        `ex3` = '" . $ex3 . "',
        `ex4` = '" . $ex4 . "',
        `ex5` = '" . $ex5 . "',
        `state` = '" . $state . "',
        `city` = '" . $city . "',
        `sold` = '" . $sold . "',";
    if($sold == 'Yes'){
      $uq .= "`date_sold` = CURRENT_DATE ";
    }else{
      $uq .= "`date_sold` = '' ";
    }
        
       $uq .= "WHERE 
        `ID` = '" . $id . "'";
  
  $ug = mysqli_query($conn, $uq) or die($conn->error);
  echo '<p style="text-align: center;">Your listing has been updated! Thank You!</p>
        <button type="button" onclick="res_edit();">Go Back</button>';
}
?>

