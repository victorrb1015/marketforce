<?php
include 'connection.php';

//Load Varibles
$id = $_GET['id'];
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];

//Make the image inactive...
$q = "UPDATE `dealer_imgs` SET
      `inactive` = 'Yes'
      WHERE `ID` = '" . $id . "'";
mysqli_query($conn, $q) or die($conn->error);

$log = fopen("dealer-img-activity.txt","a");
fwrite($log, date("m/d/Y h:iA") . " " . $rep_name . "\n");
fwrite($log, "Image ID: " . $id . " has been set to INACTIVE\n");
fwrite($log, "----------------------------------------------------------\n");
fclose($log);

echo 'This image has been removed from the dealer!';

?>