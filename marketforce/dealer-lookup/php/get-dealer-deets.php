<?php
header('Content-Type: application/json');
error_reporting(0);
include '../../php/connection.php';

//Load Variables...
$id = $_GET['id'];



//Get Dealer Details...
$q = "SELECT * FROM `dealers` WHERE `ID` = '" . $id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);


$x->ID = $r['ID'];
$x->name = mysqli_real_escape_string($conn, $r['name']);
$x->address = mysqli_real_escape_string($conn, $r['address']);
$x->city = mysqli_real_escape_string($conn, $r['city']);
$x->state = $r['state'];
$x->zip = mysqli_real_escape_string($conn, $r['zip']);
$x->phone = mysqli_real_escape_string($conn, $r['phone']);
$x->fax = mysqli_real_escape_string($conn, $r['email']);
$x->email = mysqli_real_escape_string($conn, $r['email']);
$x->sun_open = mysqli_real_escape_string($conn, $r['sun_open']);
$x->sun_close = mysqli_real_escape_string($conn, $r['sun_close']);
$x->sun_status = $r['sun_status'];
$x->mon_open = mysqli_real_escape_string($conn, $r['mon_open']);
$x->mon_close = mysqli_real_escape_string($conn, $r['mon_close']);
$x->mon_status = $r['mon_status'];
$x->tue_open = mysqli_real_escape_string($conn, $r['tue_open']);
$x->tue_close = mysqli_real_escape_string($conn, $r['tue_close']);
$x->tue_status = $r['tue_status'];
$x->wed_open = mysqli_real_escape_string($conn, $r['wed_open']);
$x->wed_close = mysqli_real_escape_string($conn, $r['wed_close']);
$x->wed_status = $r['wed_status'];
$x->thu_open = mysqli_real_escape_string($conn, $r['thu_open']);
$x->thu_close = mysqli_real_escape_string($conn, $r['thu_close']);
$x->thu_status = $r['thu_status'];
$x->fri_open = mysqli_real_escape_string($conn, $r['fri_open']);
$x->fri_close = mysqli_real_escape_string($conn, $r['fri_close']);
$x->fri_status = $r['fri_status'];
$x->sat_open = mysqli_real_escape_string($conn, $r['sat_open']);
$x->sat_close = mysqli_real_escape_string($conn, $r['sat_close']);
$x->sat_status = $r['sat_status'];
$x->lat = $r['lat'];
$x->lng = $r['lng'];
$x->status = $r['status'];
$x->new_signs = $r['new_signs'];


//Get New_Dealer_form details...
$q2 = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $id . "'";
$g2 = mysqli_query($conn, $q2) or die($conn->error);
$rr = mysqli_fetch_array($g2);

$x->maddress = mysqli_real_escape_string($conn, $rr['maddress']);
$x->mcity = mysqli_real_escape_string($conn, $rr['mcity']);
$x->mstate = $rr['mstate'];
$x->mzip = mysqli_real_escape_string($conn, $rr['mzip']);
$x->saddress = mysqli_real_escape_string($conn, $rr['saddress']);
$x->scity = mysqli_real_escape_string($conn, $rr['scity']);
$x->sstate = $rr['sstate'];
$x->szip = mysqli_real_escape_string($conn, $rr['szip']);
$x->name1 = mysqli_real_escape_string($conn, $rr['name1']);
$x->email1 = mysqli_real_escape_string($conn, $rr['email1']);
$x->name2 = mysqli_real_escape_string($conn, $rr['name2']);
$x->email2 = mysqli_real_escape_string($conn, $rr['email2']);
$x->comm_phone = $rr['prefer_phone'];
$x->comm_fax = $rr['prefer_fax'];
$x->comm_email = $rr['prefer_email'];
$x->comm_usmail = $rr['prefer_usmail'];


$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;