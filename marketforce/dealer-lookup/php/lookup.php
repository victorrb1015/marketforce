<?php
include '../../php/connection.php';

//Load Variables
$str = mysqli_real_escape_string($conn, $_GET['str']);
$user = $_GET['user'];
$a = $_GET['a'];

echo '<div class="table-responsive card-view">
      <table class="table table-bordered table-hover table-striped packet-table">
      <tr>
      <th>Name</th>
      <th>State</th>
      <th>Dealer Since</th>
      <th>Dealer Notes</th>
      <th>Overview</th>
      <th>Add Internal Note</td>
      <th>Edit Dealer</th>
      <th>Update Signs</th>
      <th id="hider-th">Deactivate</th>
      <th>OSTR</th>
      </tr>';
$q = "SELECT * FROM `dealers` WHERE `name` LIKE '%" . $str . "%' ORDER BY `name` ASC";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  if($r['inactive'] != 'Yes'){
      //The Rep Assignment for Dealer Map...
    $rnq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $r['ID'] . "'";
    $rng = mysqli_query($conn, $rnq) or die($conn->error);
    $rnr = mysqli_fetch_array($rng);

    //Dealer Notes...
    $vnq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "'";
    $vng = mysqli_query($conn, $vnq) or die($conn->error);
    $vnr = mysqli_fetch_array($vng);
    
    
  echo '<tr>
  <td>' . $r['name'] . '</td>
  <td>' . $r['city'] . ', ' . $r['state'] . '</td>
  <td>' . date("m/d/Y",strtotime($rnr['date'])) . '</td>
  <td>' . $vnr['notes'] . '</td>
  <td style="text-align:center;"><h3><a href="manage-dealer.php?did=' . $r['ID'] . '" style="color:blue;"><i class="fa fa-newspaper-o"></i></a></h3></td>
  <td style="text-align:center;"><a onclick="load_modal(' . $r['ID'] . ',\'' . mysqli_real_escape_string($conn, $r['name']) . '\',\'int\');" data-toggle="modal" data-target="#intNote"><h3 style="color:blue;"><i class="far fa-comments"></i></h3></a></td>';
  echo '<td style="text-align:center;color:blue;"><h3><i class="fas fa-pencil-alt" data-toggle="modal" data-target="#dealerAppModal" onclick="load_da_modal(\'Edit\',' . $r['ID'] . ');"></i></h3></td>';
    if($r['new_signs'] == 'Done'){
      echo '<td style="text-align:center;color:black;"><button type="button" class="btn btn-success btn-sm" onclick="complete_signs(' . $r['ID'] . ',\'Not Done\');"><i class="fa fa-thumbs-up fa-2x"></i></button></td>';
    }else{
      echo '<td style="text-align:center;color:black;"><button type="button" class="btn btn-danger btn-sm" onclick="complete_signs(' . $r['ID'] . ',\'Done\');"><i class="fa fa-thumbs-down fa-2x"></i></button></td>';
    }
  echo '<td id="hider-td" style="text-align:center;"><h3><a href="reports/deactivate_dealer.php?id=' . $r['ID'] . '" style="color:blue;" target="_blank"><i class="fa fa-ban"></i></a></h3></td>';
    
    
    if($a == 'Yes'){
      echo '<td><span id="repLabel' . $r['ID'] . '">' . $rnr['user'] . '</span> <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#repAssign" onclick="assign_rep(' . $r['ID'] . ');">(<i class="fa fa-user"></i> Re-Assign)</button></td>';
    }else{
      echo '<td><span id="repLabel' . $r['ID'] . '">' . $rnr['user'] . '</span></td>';
    }
    
  echo '</tr>';
    
  }else{//if inactive...
    
  //The Rep Assignment for Dealer Map...
    $rnq = "SELECT * FROM `new_dealer_form` WHERE `ID` = '" . $r['ID'] . "'";
    $rng = mysqli_query($conn, $rnq) or die($conn->error);
    $rnr = mysqli_fetch_array($rng);
    
    //Dealer Notes...
    $vnq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $r['ID'] . "'";
    $vng = mysqli_query($conn, $vnq) or die($conn->error);
    $vnr = mysqli_fetch_array($vng);

  echo '<tr>
  <td style="color:red;">' . $r['name'] . '<br>{INACTIVE DEALER}</td>
  <td style="color:red;">' . $r['city'] . ', ' . $r['state'] . '</td>
  <td style="color:red;">' . date("m/d/Y",strtotime($rnr['date'])) . '</td>
  <td>' . $vnr['notes'] . '</td>
  <td style="text-align:center;"><h3><a href="manage-dealer.php?did=' . $r['ID'] . '" style="color:grey;"><i class="fa fa-newspaper-o"></i></a></h3></td>
  <td style="text-align:center;"><a onclick="load_modal(' . $r['ID'] . ',\'' . mysqli_real_escape_string($conn, $r['name']) . '\',\'int\');" style="color:grey;" data-toggle="modal" data-target="#intNote"><h3 style="color:blue;"><i class="far fa-comments"></i></h3></a></td>';
  echo '<td style="text-align:center;color:blue;"><h3><i class="fas fa-pencil-alt" data-toggle="modal" data-target="#dealerAppModal" onclick="load_da_modal(\'Edit\',' . $r['ID'] . ');"></i></h3></td>';
  echo '<td style="text-align:center;"><h3 style="color:gray;"><i class="fa fa-ban"></i></h3></td>';
  echo '<td style="text-align:center;"><h3 style="color:gray;"><i class="fa fa-ban"></i></h3></td>';
  /*echo '<td id="hider-td" style="text-align:center;"><h3><a href="reports/deactivate_dealer.php?id=' . $r['ID'] . '" target="_blank"><i class="fa fa-ban"></i></a></h3></td>';*/
    
    
  if($a == 'Yes'){
      echo '<td><span id="repLabel' . $r['ID'] . '">' . $rnr['user'] . '</span> <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#repAssign" onclick="assign_rep(' . $r['ID'] . ');">(<i class="fa fa-user"></i> Re-Assign)</button></td>';
    }else{
      echo '<td><span id="repLabel' . $r['ID'] . '">' . $rnr['user'] . '</span></td>';
    }
    
  echo '</tr>';

  }//End if inactive...
}
echo '</table>
</div>';

?>