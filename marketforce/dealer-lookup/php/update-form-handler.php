<?php
include '../../../php/connection.php';

//Load Variables From Form
//$id = $_REQUEST['id'];
$id = $_REQUEST['dealer_id'];
$user = mysqli_real_escape_string($conn, $_REQUEST['user']);
$bizname = mysqli_real_escape_string($conn, $_REQUEST['bizname']);
$address = mysqli_real_escape_string($conn, $_REQUEST['address']);
$city = mysqli_real_escape_string($conn, $_REQUEST['city']);
$state = mysqli_real_escape_string($conn, $_REQUEST['state']);
$zip = mysqli_real_escape_string($conn, $_REQUEST['zip']);
$dcphone = mysqli_real_escape_string($conn, $_REQUEST['dcphone']);
$dcphone = preg_replace("/[^0-9,.]/", "", $dcphone);
$dcphone = "(" . substr($dcphone,0,3) . ") " . substr($dcphone,3,3) . "-" . substr($dcphone,6);
$phone = mysqli_real_escape_string($conn, $_REQUEST['phone']);
$phone = preg_replace("/[^0-9,.]/", "", $phone);
$phone = "(" . substr($phone,0,3) . ") " . substr($phone,3,3) . "-" . substr($phone,6);
$fax = mysqli_real_escape_string($conn, $_REQUEST['fax']);
if($fax == ''){
	$fax = 'none';
}else{
$fax = preg_replace("/[^0-9,.]/", "", $fax);
$fax = "(" . substr($fax,0,3) . ") " . substr($fax,3,3) . "-" . substr($fax,6);
}


//Mailing Address Info
$maddress = mysqli_real_escape_string($conn, $_REQUEST['maddress']);
$mcity = mysqli_real_escape_string($conn, $_REQUEST['mcity']);
$mstate = mysqli_real_escape_string($conn, $_REQUEST['mstate']);
$mzip = mysqli_real_escape_string($conn, $_REQUEST['mzip']);

$auth_rep = mysqli_real_escape_string($conn, $_REQUEST['auth_rep']); 

$property = mysqli_real_escape_string($conn, $_REQUEST['property']);
$expiration = mysqli_real_escape_string($conn, $_REQUEST['expiration']);
if($expiration == 'mm/dd/yyyy'){
	$expiration = 'none';
}
$email = mysqli_real_escape_string($conn, $_REQUEST['email']);
if($email == ''){
	$email = 'none';
}
$website = mysqli_real_escape_string($conn, $_REQUEST['website']);
if($website == ''){
	$website = 'none';
}
$years = mysqli_real_escape_string($conn, $_REQUEST['years']);
$numbers = mysqli_real_escape_string($conn, $_REQUEST['numbers']);
$EIN = mysqli_real_escape_string($conn, $_REQUEST['EIN']);
if($EIN == ''){
	$EIN = 'none';
}
$name1 = mysqli_real_escape_string($conn, $_REQUEST['name1']);
$email1 = mysqli_real_escape_string($conn, $_REQUEST['email1']);
if($email1 == ''){
	$email1 = 'none';
}
$name2 = mysqli_real_escape_string($conn, $_REQUEST['name2']);
if($name2 == ''){
	$name2 = 'none';
}
$email2 = mysqli_real_escape_string($conn, $_REQUEST['email2']);
if($email2 == ''){
	$email2 = 'none';
}
//$products = mysqli_real_escape_string($conn, $_REQUEST['products']);
$fulltime = mysqli_real_escape_string($conn, $_REQUEST['fulltime']);
$parttime = mysqli_real_escape_string($conn, $_REQUEST['parttime']);
$ownername = mysqli_real_escape_string($conn, $_REQUEST['ownername']);
$owneraddress = mysqli_real_escape_string($conn, $_REQUEST['owneraddress']);
$ownercity = mysqli_real_escape_string($conn, $_REQUEST['ownercity']);
$ownerstate = mysqli_real_escape_string($conn, $_REQUEST['ownerstate']);
$ownerzip = mysqli_real_escape_string($conn, $_REQUEST['ownerzip']);
$ownerphone = mysqli_real_escape_string($conn, $_REQUEST['ownerphone']);
$ownerphone = preg_replace("/[^0-9,.]/", "", $ownerphone);
$ownerphone = "(" . substr($ownerphone,0,3) . ") " . substr($ownerphone,3,3) . "-" . substr($ownerphone,6);
$altphone = mysqli_real_escape_string($conn, $_REQUEST['altphone']);
if($altphone == ''){
	$altphone = 'none';
}else{
$altphone = preg_replace("/[^0-9,.]/", "", $altphone);
$altphone = "(" . substr($altphone,0,3) . ") " . substr($altphone,3,3) . "-" . substr($altphone,6);
}
$mainroad = mysqli_real_escape_string($conn, $_REQUEST['mainroad']);
$visibility = mysqli_real_escape_string($conn, $_REQUEST['visibility']);
$displayspace = mysqli_real_escape_string($conn, $_REQUEST['displayspace']);
$currentcompany = mysqli_real_escape_string($conn, $_REQUEST['currentcompany']);
$who = mysqli_real_escape_string($conn, $_REQUEST['who']);
if($who == ''){
	$who = 'none';
}
$salesarea = mysqli_real_escape_string($conn, $_REQUEST['salesarea']);
$where = mysqli_real_escape_string($conn, $_REQUEST['where']);
if($where == ''){
	$where = 'none';
}
$otherprod = mysqli_real_escape_string($conn, $_REQUEST['otherprod']);
$closedealers = mysqli_real_escape_string($conn, $_REQUEST['closedealers']);
$rto = mysqli_real_escape_string($conn, $_REQUEST['rto']);
$bbb = mysqli_real_escape_string($conn, $_REQUEST['bbb']);
if($bbb == ''){
	$bbb = 'none';
}
$mantra = mysqli_real_escape_string($conn, $_REQUEST['mantra']);
if($mantra == ''){
	$mantra = 'none';
}
$ap_name = mysqli_real_escape_string($conn, $_REQUEST['ap_name']);
$ap_email = mysqli_real_escape_string($conn, $_REQUEST['ap_email']);
$ap_phone = mysqli_real_escape_string($conn, $_REQUEST['ap_phone']);

//Insert into the main Dealer list for note additions
$xxq = "UPDATE `dealers` SET 
				`name` = '" . $bizname . "',
				`address` = '" . $address . "',
				`city` = '" . $city . "',
				`state` = '" . $state . "',
				`zip` = '" . $zip . "',
				`phone` = '" . $phone . "',
				`fax` = '" . $fax . "',
				`email` = '" . $email . "'
				WHERE `ID` = '" . $id . "'";
				
mysqli_query($conn, $xxq) or die($conn->error);


//INSERT DATA INTO DATABASE
$i = "UPDATE
      `new_dealer_form`
			SET
			`bizname` = '" . $bizname . "',
			`address` = '" . $address . "',
			`city` = '" . $city . "',
			`state` = '" . $state . "',
			`zip` = '" . $zip . "',
			`dcphone` = '" . $dcphone . "',
			`phone` = '" . $phone . "',
			`fax` = '" . $fax . "',
			`maddress` = '" . $maddress . "',
			`mcity` = '" . $mcity . "',
			`mstate` = '" . $mstate . "',
			`mzip` = '" . $mzip . "',
      `auth_rep` = '" . $auth_rep . "',
			`property` = '" . $property . "',
      `expiration` = '" . $expiration . "',
			`email` = '" . $email . "',
			`website` = '" . $website . "',
			`years` = '" . $years . "',
			`numbers` = '" . $numbers . "',
			`EIN` = '" . $EIN . "',
			`name1` = '" . $name1 . "',
			`email1` = '" . $email1 . "',
			`name2` = '" . $name2 . "',
			`email2` = '" . $email2 . "',
      `fulltime` = '" . $fulltime . "',
			`parttime` = '" . $parttime . "',
			`ownername` = '" . $ownername . "',
			`owneraddress` = '" . $owneraddress . "',
			`ownercity` = '" . $ownercity . "',
			`ownerstate` = '" . $ownerstate . "',
			`ownerzip` = '" . $ownerzip . "',
			`ownerphone` = '" . $ownerphone . "',
			`altphone` = '" . $altphone . "',
			`mainroad` = '" . $mainroad . "',
			`visibility` = '" . $visibility . "',
			`displayspace` = '" . $displayspace . "',
      `currentcompany` = '" . $currentcompany . "',
			`who` = '" . $who . "',
			`salesarea` = '" . $salesarea . "',
			`where` = '" . $where . "',
			`products` = '" . $otherprod . "',
			`closedealers` = '" . $closedealers . "',
			`rto` = '" . $rto . "',
			`bbb` = '" . $bbb . "',
			`mantra` = '" . $mantra . "',
      `ap_name` = '" . $ap_name . "',
      `ap_email` = '" . $ap_email . "',
      `ap_phone` = '" . $ap_phone . "',
			`edited_by` = '" . $user . "',
			`edit_timestamp` = (DATE_ADD(now() , INTERVAL 1 HOUR))
			WHERE
			`ID` = '" . $id . "'";
     

mysqli_query($conn, $i) or die($conn->error);

//Build Response
$x->response = 'GOOD';
$x->message = 'Successfully Updated';
$x->data = $_REQUEST;

//Send Response
$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;