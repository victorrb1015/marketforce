<div class="row">
  <div class="top" style="padding-left:20px;">
    <h2>
      Search for a
      <?php switch($_SESSION['org_id']) {
          case "832122":
              echo "Customer ";
              break;
          case "123456":
          case '532748':
              echo "Client ";
              break;
          default:
              echo 'Dealer ';
      }?> to lookup information:
    </h2>
    <input style="width:45%; height:35px;" type="text" name="search" id="search" placeholder="Search Here..." onkeyup="searcher(this.value);" autofocus/>
    <br><br>
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#dealerAppModal" onclick="load_da_modal('New');">New <?php switch($_SESSION['org_id']) {
            case "832122":
                echo "Customer ";
                break;
            case "123456":
            case '532748':
                echo "Client ";
                break;
            default:
                echo 'Dealer ';
        }?></button>
    <br><br>
    <div id="search_results"></div>
    <h1 class="page-header"></h1>
  </div>
  <br><br>
  <div id="accordion"></div>
</div>