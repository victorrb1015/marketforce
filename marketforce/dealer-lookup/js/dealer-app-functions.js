//Reset Form
function reset_da_form(){
  document.forms.dealer_app_form.reset();
}

//Submit Form
function submit_da_form(mode){
  //$('#dealer_app_form').reportValidity();
  document.forms.dealer_app_form.reportValidity();
  if(document.forms.dealer_app_form.checkValidity()){
    console.log('Validity Successful');
  }else{
    console.log('Validity Failed');
    return;
  }
  
  if(mode === 'New'){
    var url = 'dealer-lookup/php/form-handler.php';
  }else if(mode === 'Edit'){
    var url = 'dealer-lookup/php/update-form-handler.php';
    if(document.getElementById('dealer_id').value === ''){
      toast_alert('Something Went Wrong...','No Dealer ID Attached to Form','bottom-right','error');
      return;
    }
  }else{
    alert('An Error Occurred. Please contact your system administrator.');
    return;
  }
  //var url = 'dealer-lookup/php/form-handler.php';
  var params = $('#dealer_app_form').serialize();
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {//Call a function when the state changes.
      if(xhr.readyState == 4 && xhr.status == 200) {
          //Do Something...
        var r = JSON.parse(this.responseText);
        console.log(r);
        if(r.response === 'GOOD'){
          searcher(r.data.bizname);
          document.getElementById('search').value = r.data.bizname;
          $('#dealerAppModal').modal('toggle');
          toast_alert('Success',r.message,'bottom-right','success');
        }
      }
  }
  xhr.open('POST', url, true);
  //Send the proper header information along with the request
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhr.send(params); 
  
}


function load_da_modal(mode,did){
  if(mode === 'New'){
    reset_da_form();
    document.getElementById('save_da_form').setAttribute('onclick','submit_da_form(\'New\');');
  }else if(mode === 'Edit'){
    reset_da_form();
    document.getElementById('save_da_form').setAttribute('onclick','submit_da_form(\'Edit\');');
    document.getElementById('dealer_id').value = did;
    load_da_modal_info(did);
  }else{
    alert('An Error Occurred. Please contact your system administrator.');
  }
}


function load_da_modal_info(did){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200) {
         // Typical action to be performed when the document is ready:
         var r = JSON.parse(this.responseText);
        if(r.response === 'GOOD'){
          console.log(r.data);
          //Load Info
          Object.keys(r.data).forEach(function eachKey(key){
            if(document.getElementById(key)){
              document.getElementById(key).value = r.data[key];
            }
          });
          if(aam === false){
            document.getElementById('datepicker').value = r.data.expiration;
            switch(r.data.property){
              case 'OWNED':
                document.getElementById('owned').checked = true;
              case 'LEASED-RENTED':
                document.getElementById('leased').checked = true;
              default:
                break;
            }
            switch(r.data.mainroad){
              case 'Yes':
                document.getElementById('main-yes').checked = true;
              case 'No':
                document.getElementById('main-no').checked = true;
              default:
                break;
            }
            switch(r.data.visibility){
              case 'Poor':
                document.getElementById('poor').checked = true;
              case 'Good':
                document.getElementById('good').checked = true;
              case 'Great':
                document.getElementById('great').checked = true;
              case 'Excellent':
                document.getElementById('excellent').checked = true;
              default:
                break;
            }
            switch(r.data.displayspace){
              case 'Large':
                document.getElementById('large').checked = true;
              case 'Medium':
                document.getElementById('medium').checked = true;
              case 'Small':
                document.getElementById('small').checked = true;
              default:
                break;
            }
            switch(r.data.currentcompany){
              case 'No':
                document.getElementById('current-no').checked = true;
              case 'Yes':
                document.getElementById('current-yes').checked = true;
              default:
                break;
            }
            switch(r.data.salesarea){
              case 'No':
                document.getElementById('salesarea-no').checked = true;
              case 'Yes':
                document.getElementById('salesarea-yes').checked = true;
              default:
                break;
            }
          }
          
        }else{
          toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
        }
      }
  };
  xhttp.open("GET", "dealer-lookup/php/fetch-dealer-info.php?did="+did, true);
  //xhttp.open("GET", "dealer-lookup/php/get-dealer-deets.php?id="+did, true);
  xhttp.send();
}