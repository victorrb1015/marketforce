    function searcher(x) {
      //alert("test");
      var x = x.replace('&', '%26');
      if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
      } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("search_results").innerHTML = this.responseText;
          //alert(this.responseText);

        }
      }
      xmlhttp.open("GET", "dealer-lookup/php/lookup.php?a=" + a + "&str=" + x + "&user=" + rep_id, true);
      xmlhttp.send();
    }


    function log_inote() {
      var id = document.getElementById('int_id').value;
      var inote = document.getElementById('inote').value;
      if (inote === '') {
        document.getElementById('int_error_msg').innerHTML = 'Please Enter Your Note!';
        return;
      }
      inote = urlEncode(inote);

      if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
      } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          alert(this.responseText);
          //window.location.reload();
          document.getElementById('inote').value = '';
        }
      }
      xmlhttp.open("GET", "dealer-lookup/php/add-internal-note.php?id=" + id + "&rep=" + rep_id + "&note=" + inote, true);
      xmlhttp.send();
    }

    function load_modal(id, name, mode) {
      if (mode === 'int') {
        document.getElementById('int_id').value = id;
        document.getElementById('int_name').innerHTML = name;
      }
    }

    function urlEncode(url) {
      url = url.replace(/&/g, '%26');
      url = url.replace(/#/g, '%23');
      return url;
    }


    function complete_signs(id, mode) {
      if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
      } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          alert(this.responseText);
          //window.location.reload();
          var x = document.getElementById('search').value;
          searcher(x);
        }
      }
      xmlhttp.open("GET", "php/update-signs.php?id=" + id + "&mode=" + mode + "&rep_name=" + rep_name + "&rep_id=" + rep_id, true);
      xmlhttp.send();
    }


    /*====================================================
      This script is to allow the toggling of form fields
      when needed
    ====================================================*/
    $(document).ready(function() {
      $('#sameAddress').click(function() {

        //Current Variables
        var currentAddress = document.getElementById('address');
        var currentCity = document.getElementById('city');
        var currentState = document.getElementById('state');
        var currentZip = document.getElementById('zip');

        //New Variables
        var newAddress = document.getElementById('maddress');
        var newCity = document.getElementById('mcity');
        var newState = document.getElementById('mstate');
        var newZip = document.getElementById('mzip');

        var boxx = document.getElementById('sameAddress');
        if (boxx.checked) {
          newAddress.value = currentAddress.value;
          newCity.value = currentCity.value;
          newState.value = currentState.value;
          newZip.value = currentZip.value;
        } else {
          newAddress.value = '';
          newCity.value = '';
          newState.value = '';
          newZip.value = '';
        }

      })
    });



    function copyCell() {
      if (document.getElementById('same_cell').checked) {
        document.getElementById('ownerphone').value = document.getElementById('dcphone').value;
      } else {
        document.getElementById('ownerphone').value = '';
      }
    }