<div class="modal fade" id="dealerAppModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Client Application</h4>
      </div>
      <div class="modal-body" id="da_form">
        <form name="dealer_app_form" id="dealer_app_form">
          <input type="hidden" id="user" name="user" value="<?php echo $_SESSION['full_name']; ?>">
          <input type="hidden" id="dealer_id" name="dealer_id" value="">
          
          <h5>Business Details</h5>
          <hr>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Name of Business</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="bizname" name="bizname" required></div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4">
              <p>Physical Address</p>
            </div>
            <div class="col-lg-8"><input type="text" class="form-control" id="address" name="address" required></div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>City</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="city" name="city" required></div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>State</p></div>
            <div class="col-lg-8">
              <select id="state" name="state" class="form-control" required>
                <option value="">Select A State...</option>
                <?php
                  $ssq = "SELECT * FROM `states`";
                  $ssg = mysqli_query($conn, $ssq) or die($conn->error);
                  while($ssr = mysqli_fetch_array($ssg)){
                    echo '<option value="' . $ssr['state'] . '">' . $ssr['state'] . '</option>';
                  }
                ?>
              </select>
            </div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Zip Code</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="zip" name="zip" required></div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Phone Number</p></div>
            <div class="col-lg-8"><input type="text" class="form-control phone" id="phone" name="phone" required></div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Fax Number</p></div>
            <div class="col-lg-8"><input type="text" class="form-control phone" id="fax" name="fax"></div>
          </div>
        
          <h5>Mailing Address Info</h5>
          <hr>
        
          <div class="col-lg-12 mb-5">
            <input type="checkbox" id="sameAddress"> Same as Physical Address
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Mailing Address</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="maddress" name="maddress" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>City</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="mcity" name="mcity" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>State</p></div>
            <div class="col-lg-8">
              <select id="mstate" name="mstate" class="form-control" required>
                <option value="">Select A State...</option>
                <?php
                  $ssq = "SELECT * FROM `states`";
                  $ssg = mysqli_query($conn, $ssq) or die($conn->error);
                  while($ssr = mysqli_fetch_array($ssg)){
                    echo '<option value="' . $ssr['state'] . '">' . $ssr['state'] . '</option>';
                  }
                ?>
              </select>
            </div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Zip Code</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="mzip" name="mzip" required></div>
          </div>
        
          <h5>About The Business</h5>
          <hr>
          
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Authorized Representative Name</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="auth_rep" name="auth_rep" required></div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4">
              <p>
                Cell Phone Number<br>
                <small>(USED FOR SMS TEXT NOTIFICATIONS)</small>
              </p>
            </div>
            <div class="col-lg-8"><input type="text" class="form-control phone" id="dcphone" name="dcphone" required></div>
          </div>
        
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Email</p></div>
            <div class="col-lg-8"><input type="email" class="form-control" id="email" name="email" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Website</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="website" name="website" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Years in Business</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="years" name="years" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Number of Locations</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="numbers" name="numbers" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>EIN #</p></div>
            <div class="col-lg-8"><input type="text" class="form-control EIN" id="EIN" name="EIN" required></div>
          </div>
        
          <h5>Marketing Representative Info</h5>
          <hr>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Representative 1 Name</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="name1" name="name1" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Representative 1 Email</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="email1" name="email1" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Representative 2 Name</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="name2" name="name2" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Representative 2 Email</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="email2" name="email2" required></div>
          </div>
          
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Accounts Payable Rep Name</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="ap_name" name="ap_name" required></div>
          </div>
          
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Accounts Payable Email</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="ap_email" name="ap_email" required></div>
          </div>
          
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Accounts Payable Phone</p></div>
            <div class="col-lg-8"><input type="text" class="form-control phone" id="ap_phone" name="ap_phone" required></div>
          </div>
        
        </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="submit_da_form();" id="save_da_form">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>