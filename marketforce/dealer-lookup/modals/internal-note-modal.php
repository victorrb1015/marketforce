<div class="modal fade" id="intNote" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Log An Internal Note For <span style="color:red;" id="int_name"></span></h4>
      </div>
      <div class="modal-body">
        <p>Enter A Note Here (max 5,000 characters):</p>
        <textarea id="inote" class="form-control" style="height:250px;" maxlength="5000"></textarea>
        <input type="hidden" id="int_id" value="" />
        <br><br>
        <p id="int_error_msg" style="color:red;font-weight:bold;"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="log_inote();" data-dismiss="modal">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>