<div class="modal fade" id="dealerAppModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Dealer Application</h4>
      </div>
      <div class="modal-body" id="da_form">
        <form name="dealer_app_form" id="dealer_app_form">
          <input type="hidden" id="user" name="user" value="<?php echo $_SESSION['full_name']; ?>">
          <input type="hidden" id="dealer_id" name="dealer_id" value="">
          
          <h5>Dealer Details</h5>
          <hr>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Name of Business</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="bizname" name="bizname" required></div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4">
              <p>Address</p>
            </div>
            <div class="col-lg-8"><input type="text" class="form-control" id="address" name="address" required></div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>City</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="city" name="city" required></div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>State</p></div>
            <div class="col-lg-8">
              <select id="state" name="state" class="form-control" required>
                <option value="">Select A State...</option>
                <?php
                  $ssq = "SELECT * FROM `states`";
                  $ssg = mysqli_query($conn, $ssq) or die($conn->error);
                  while($ssr = mysqli_fetch_array($ssg)){
                    echo '<option value="' . $ssr['state'] . '">' . $ssr['state'] . '</option>';
                  }
                ?>
              </select>
            </div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Zip Code</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="zip" name="zip" required></div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4">
              <p>
                Cell Phone Number<br>
                <small>(USED FOR SMS TEXT NOTIFICATIONS)</small>
              </p>
            </div>
            <div class="col-lg-8"><input type="text" class="form-control phone" id="dcphone" name="dcphone" required></div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Phone Number</p></div>
            <div class="col-lg-8"><input type="text" class="form-control phone" id="phone" name="phone" required></div>
          </div>

          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Fax Number</p></div>
            <div class="col-lg-8"><input type="text" class="form-control phone" id="fax" name="fax"></div>
          </div>
        
          <h5>Mailing Address Info</h5>
          <hr>
        
          <div class="col-lg-12 mb-5">
            <input type="checkbox" id="sameAddress"> Same as Physical Address
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Mailing Address</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="maddress" name="maddress" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>City</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="mcity" name="mcity" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>State</p></div>
            <div class="col-lg-8">
              <select id="mstate" name="mstate" class="form-control" required>
                <option value="">Select A State...</option>
                <?php
                  $ssq = "SELECT * FROM `states`";
                  $ssg = mysqli_query($conn, $ssq) or die($conn->error);
                  while($ssr = mysqli_fetch_array($ssg)){
                    echo '<option value="' . $ssr['state'] . '">' . $ssr['state'] . '</option>';
                  }
                ?>
              </select>
            </div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Zip Code</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="mzip" name="mzip" required></div>
          </div>
        
          <h5>About The Business</h5>
          <hr>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>The Business Property is</p></div>
            <div class="col-lg-8">
              <input type="radio" id="owned" name="property" value="OWNED" required> OWNED
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="leased" name="property" value="LEASED-RENTED"> LEASED/RENTED
            </div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Lease Expiration Date</p></div>
            <div class="col-lg-8">
              <input type="text" class="form-control date" id="datepicker" name="expiration" placeholder="mm/dd/yyyy" required>
            </div>
            
            <div class="col-lg-12">
              <span style="color:red;">(Note: If you lease or rent your lot, <?php echo $_SESSION['org_name']; ?> will require a Landowner's Consent Form)</span>
            </div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Email</p></div>
            <div class="col-lg-8"><input type="email" class="form-control" id="email" name="email" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Website</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="website" name="website" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Years in Business</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="years" name="years" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Number of Locations</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="numbers" name="numbers" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>EIN #</p></div>
            <div class="col-lg-8"><input type="text" class="form-control EIN" id="EIN" name="EIN" required></div>
          </div>
        
          <h5>Salesman Info</h5>
          <hr>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Salesman 1 Name</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="name1" name="name1" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Salesman 1 Email</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="email1" name="email1" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Salesman 2 Name</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="name2" name="name2" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Salesman 2 Email</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="email2" name="email2" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Number of Full Time Employees</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="fulltime" name="fulltime" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Number of Part Time Employees</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="parttime" name="parttime" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Business Owner's Name</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="ownername" name="ownername" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Owner's Home Address</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="owneraddress" name="owneraddress" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>City</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="ownercity" name="ownercity" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>State</p></div>
            <div class="col-lg-8">
              <select id="ownerstate" name="ownerstate" class="form-control" required>
                <option value="">Select A State...</option>
                <?php
                  $ssq = "SELECT * FROM `states`";
                  $ssg = mysqli_query($conn, $ssq) or die($conn->error);
                  while($ssr = mysqli_fetch_array($ssg)){
                    echo '<option value="' . $ssr['state'] . '">' . $ssr['state'] . '</option>';
                  }
                ?>
              </select>
            </div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Zip Code</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="ownerzip" name="ownerzip" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Owner's Cell Number</p></div>
            <div class="col-lg-8"><input type="text" class="form-control phone" id="ownerphone" name="ownerphone" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Owner's Alt Phone Number</p></div>
            <div class="col-lg-8"><input type="text" class="form-control phone" id="altphone" name="altphone"></div>
          </div>
        
          <h5>Office Info</h5>
          <hr>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Dealer located on main road?</p></div>
            <div class="col-lg-8">
              <input type="radio" id="main-yes" name="mainroad" value="Yes" required> Yes
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="main-no" name="mainroad" value="No"> No
            </div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Visibility from the main road is:</p></div>
            <div class="col-lg-8">
              <input type="radio" id="poor" name="visibility" value="Poor" required> Poor
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="good" name="visibility" value="Good"> Good
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="great" name="visibility" value="Great"> Great
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="excellent" name="visibility" value="Excellent"> Excellent
            </div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Display space available?</p></div>
            <div class="col-lg-8">
              <input type="radio" id="large" name="displayspace" value="Large" required> Large
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="medium" name="displayspace" value="Medium"> Medium
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="small" name="displayspace" value="Small"> Small
            </div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Is this dealer currently selling for another company?</p></div>
            <div class="col-lg-8">
              <input type="radio" id="current-no" name="currentcompany" value="No" required> No
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="current-yes" name="currentcompany" value="Yes"> Yes
            </div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>If yes, who?</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="who" name="who" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Is there a sales area dedicated to the sales of buildings?</p></div>
            <div class="col-lg-8">
              <input type="radio" id="salesarea-yes" name="salesarea" value="Yes" required> Yes
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="radio" id="salesarea-no" name="salesarea" value="No"> No
            </div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>If yes, where?</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="where" name="where" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>What other products does this location sell?</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="otherprod" name="otherprod" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>What dealers are within 15 miles of this location?</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="closedealers" name="closedealers" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Recommend RTO Programs?</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="rto" name="rto" required></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>BBB Rating</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="bbb" name="bbb"></div>
          </div>
        
          <div class="col-lg-12 mb-5">
            <div class="col-lg-4"><p>Mantra</p></div>
            <div class="col-lg-8"><input type="text" class="form-control" id="mantra" name="mantra"></div>
          </div>
        
        </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="submit_da_form();" id="save_da_form">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>