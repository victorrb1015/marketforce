<?php
session_start();

if($_SESSION['repos'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
		th, td{
			padding: 10px;
		}
		#dl, dc{
			height: 430px;
		}
		.panel-body{
			height: 382px;
		}
		#scroll{
			overflow: scroll;
		}
		::-webkit-scrollbar {
   // width: 0px;  /* remove scrollbar space */
    //background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
   // background: #FF0000;
}
		
		
		.packet-table td, th{
			border: 1px solid black;
		}
		.packet-table{
			margin:auto;
		}
    
    #hoverGreen{
      color:blue;
    }
    #hoverGreen:hover{
      color:green;
    }
		#hoverRed{
			color:black;
		}
		#hoverRed:hover{
			color:blue;
		}
		
		mark{
			background:yellow;
		}
  </style>
  <script>

	var rep_name = '<? echo $_SESSION['full_name']; ?>';
	var rep_id = '<? echo $_SESSION['user_id']; ?>';
		
	function add_repair(mode,n){
		
		var cname = document.getElementById('cname').value;
		if(cname === ''){
			document.getElementById('cname_error').innerHTML = '*Please Enter The Customer Name!';
			return;
		}
		cname = urlEncode(cname);
		var inv = document.getElementById('inv').value;
		if(inv === '' && mode != 'Draft'){
			document.getElementById('inv_error').innerHTML = '*Please Enter The Invoice Number!';
			return;
		}
		inv = urlEncode(inv);
		var dname = document.getElementById('dname').value;
		if(dname === ''){
			document.getElementById('dname_error').innerHTML = '*Please Enter The Dealer\'s Name!';
			return;
		}
		dname = urlEncode(dname);
		var sale_date = document.getElementById('sale_date').value;
		if(sale_date === '' && mode != 'Draft'){
			document.getElementById('sale_date_error').innerHTML = '*Please Enter The Sale Date!';
			return;
		}
		sale_date = urlEncode(sale_date);
		var contractor_name = document.getElementById('contractor_name').value;
		if(contractor_name === '' && mode != 'Draft'){
			document.getElementById('contractor_name_error').innerHTML = '*Please Enter The Contractor\'s Name!';
			return;
		}
		contractor_name = urlEncode(contractor_name);
		var install_date = document.getElementById('install_date').value;
		if(install_date === '' && mode != 'Draft'){
			document.getElementById('install_date_error').innerHTML = '*Please Enter The Installed Date!';
			return;
		}
		install_date = urlEncode(install_date);
    var address = document.getElementById('raddress').value;
    if(address === '' && mode != 'Draft'){
      document.getElementById('address_error').innerHTML = 'Please Enter The Repair Address!';
      return;
    }
    address = urlEncode(address);
    var city = document.getElementById('rcity').value;
    if(city === '' && mode != 'Draft'){
      document.getElementById('city_error').innerHTML = 'Please Enter The City!';
      return;
    }
    city = urlEncode(city);
    var state = document.getElementById('state').value;
    if(state === ''){
      document.getElementById('state_error').innerHTML = 'Please Enter The State!';
      return;
    }
    state = urlEncode(state);
    var zip = document.getElementById('rzip').value;
    if(zip === '' && mode != 'Draft'){
      document.getElementById('zip_error').innerHTML = 'Please Enter The Zip Code!';
      return;
    }
    zip = urlEncode(zip);
		var details = document.getElementById('details').value;
		if(details === '' && mode != 'Draft'){
			document.getElementById('details_error').innerHTML = '*Please Enter The Details Of The Repair!';
			return;
		}
		details = urlEncode(details);
		
		document.getElementById('mode').value = mode;
		//document.getElementById('new').value = n;
		document.getElementById('sub_btn').disabled = true;//disable button to prevent duplicate entries...
		document.getElementById('draft_btn').disabled = true;//disable button to prevent duplicate entries...
		document.getElementById('new-repair-form').submit();
		$("#newRepair").modal("hide");
		//document.getElementById('new-repair-form').reset();
		//window.location.reload();
	}
		
		
		
	function urlEncode(url){
		url = url.replace(/&/g, '%26'); 
		url = url.replace(/#/g, '%23');
		return url;
	}
		
	function move(id,mode){
		if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","repos/repo-handler.php?id="+id+"&mode="+mode,true);
  xmlhttp.send();
	//xmlhttp.open("POST", pURL, true);
  //xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  //xmlhttp.send(params);
	}
		
	function load_modal(mode,id,name){
		//Load Note Modal
		if(mode === 'note'){
		document.getElementById('cust_id').value = id;
		document.getElementById('cust_name').innerHTML = name;
		document.getElementById('rcname').value = name;
		//document.getElementById('repairNote').style.display = 'block';
		//$("#repairNote").modal("show");
		}
		
		if(mode === 'New'){
			document.getElementById('new-repair-form').reset();
			document.getElementById('rep_name').value = '<? echo $_SESSION['full_name']; ?>';
			document.getElementById('rep_id').value = '<? echo $_SESSION['user_id']; ?>';
			document.getElementById('new').value = 'True';
		}
		
	}
		
		
		
	function add_note(){
		var id = document.getElementById('cust_id').value;
		var cname = document.getElementById('rcname').value;
		var rep_id = '<? echo $_SESSION['user_id']; ?>';
		var rep_name = '<? echo $_SESSION['full_name']; ?>';
		var note = document.getElementById('rnote').value;
		if(note === ''){
			document.getElementById('rnote_error').innerHTML = 'Please Enter A Note!';
			return;
		}
		note = urlEncode(note);
		if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","repos/add-repo-note.php?id="+id+"&rid="+rep_id+"&rname="+rep_name+"&note="+note+"&cname="+cname,true);
  xmlhttp.send();
	}
		
		
	
	function fetch_repair(id){
		
		if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //alert(this.responseText);
			var q = JSON.parse(this.responseText);//This puts the JSON data into useable data
      //window.location.reload();
      //Load Modal Fields Here...
			document.getElementById('cname').value = q.cname;
			document.getElementById('today_date').innerHTML = q.today_date;
			document.getElementById('inv').value = q.inv;
			document.getElementById('state').value = q.state;
			document.getElementById('dname').value = q.dname;
			document.getElementById('sale_date').value = q.sale_date;
			document.getElementById('contractor_name').value = q.contractor_name;
			document.getElementById('install_date').value = q.install_date;
			document.getElementById('raddress').value = q.address;
			document.getElementById('rcity').value = q.city;
			document.getElementById('state').value = q.state;
			document.getElementById('rzip').value = q.zip;
			document.getElementById('details').value = q.details;
			document.getElementById('cphone').value = q.cphone;
			document.getElementById('cemail').value = q.cemail;
			document.getElementById('iid').value = q.ID;
			document.getElementById('dname').value = q.did;
			if(q.img1 != ''){
				document.getElementById('img1').innerHTML = '<p>File 1</p><a href="' + q.img1 + '" target="_blank" style="color:blue;font-weight:bold;">View Image</a>';
			}
			if(q.img2 != ''){
				document.getElementById('img2').innerHTML = '<p>File 2</p><a href="' + q.img2 + '" target="_blank" style="color:blue;font-weight:bold;">View Image</a>';
			}
			if(q.img3 != ''){
				document.getElementById('img3').innerHTML = '<p>File 3</p><a href="' + q.img3 + '" target="_blank" style="color:blue;font-weight:bold;">View Image</a>';
			}
			if(q.img4 != ''){
				document.getElementById('img4').innerHTML = '<p>File 4</p><a href="' + q.img4 + '" target="_blank" style="color:blue;font-weight:bold;">View Image</a>';
			}
			if(q.img5 != ''){
				document.getElementById('img5').innerHTML = '<p>File 5</p><a href="' + q.img5 + '" target="_blank" style="color:blue;font-weight:bold;">View Image</a>';
			}
			//Setup Mode
			document.getElementById('new').value = 'False';
			
    }
  }
  xmlhttp.open("GET","repos/fetch-repo.php?id="+id,true);
  xmlhttp.send();
	}
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
       <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> Repos</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
														<li>
															<i class="fa fa-refresh"></i> Repos
													</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							
							
							
							
							<div class="row">
							  <div class="col-lg-12">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
												Pending Repos &nbsp;&nbsp;&nbsp;
                				<button type="button" class="btn btn-primary" onclick="load_modal('New')" data-toggle="modal" data-target="#newRepair">New Repo</button>
												<?php 
												if($_GET['q'] == 'ALL'){
												echo '<a href="repos.php" style="text-align:right;">
																<button type="button" class="btn btn-success">View My States</button>
															</a>';	
												}else{
												echo '<a href="repos.php?q=ALL" style="text-align:right;">
																<button type="button" class="btn btn-success">View All</button>
															</a>';
												}
												?>
												<a href="http://marketforceapp.com/marketforce/map/repo-map.php" target="_blank">
													<button type="button" class="btn btn-info" disabled><i class="fa fa-globe"></i> View Map</button>
												</a>
											</h3>
										</div>
										<div class="panel-body" style="height:100%;">
											<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
												<tr>
													<th>Name</th>
													<th>Date</th>
													<th>Location</th>
													<th>Contractor</th>
													<th>Status</th>
													<th>Notes</th>
													<th>Actions</th>
												</tr>
												<?php
												if($_GET['q'] == 'ALL'){
													$rq = "SELECT * FROM `repos` WHERE `status` != 'Completed' AND `status` != 'Cancelled' ORDER BY FIELD(`status`,'2nd Attempt','Pending','Scheduled','Draft')";
												}else{
											    if($_SESSION['user_id'] == '11'){
														$rq = "SELECT * FROM `repos` WHERE `status` != 'Completed' AND `status` != 'Cancelled' AND `status` = 'Draft'";
													}else{
														$rq = "SELECT * FROM `repos` WHERE `status` != 'Completed' AND `status` != 'Cancelled' AND `state` = '" . $_SESSION['myState'] . "' OR `state` = '" . $_SESSION['myState2'] . "' OR `state` = '" . $_SESSION['myState3'] . "' OR `state` = '" . $_SESSION['myState4'] . "' ORDER BY FIELD(`status`,'2nd Attempt','Pending','Scheduled','Draft')";
													}
												}
												$rg = mysqli_query($conn, $rq) or die($conn->error);
												while($rr = mysqli_fetch_array($rg)){
													//Main Color Coding...
													if($rr['urgent'] == 'Yes'){
														$style = ' style="color:white;background:green;font-weight:bold;"';
													}
													if($rr['urgent'] == 'No'){
														$style = ' style="color:white;background:red;font-weight:bold;"';
													}
													if($rr['status'] == '2nd Attempt'){
														$style = ' style="color:black;background:purple;font-weight:bold;"';
													}
													if($rr['status'] == 'Scheduled'){
														$style = ' style="color:black;background:white;font-weight:bold;"';
													}
													if($rr['status'] == 'Draft'){
														$style = ' style="color:black;background:pink;font-weight:bold;"';
													}
													
													//Date Color Coding...
													$td = date("m-d-y");
													$d1 = strtotime($rr['today_date']);
													$d2 = strtotime("-2 week");
													$d3 = strtotime("-3 week");
													$d4 = strtotime("-4 week");
													if($d4 >= $d1){
														$age = 'style="background:red;"';
													}else if($d3 >= $d1){
														$age = 'style="background:yellow;"';
													}else if($d2 >= $d1){
														$age = 'style="background:blue;"';
													}else{
														$age = 'style="background:white;color:black;"';
													}
													if($rr['status'] == 'Draft'){
														$age = 'style="background:pink;"';
													}
													echo '<tr' . $style . '>
																	<td>' . $rr['cname'] . '</td>
																	<td ' . $age . '>' . date("m/d/y", strtotime($rr['today_date'])) . '</td>
																	<td>' . $rr['city'] . ', ' . $rr['state'] . '</td>
																	<td>' . $rr['contractor_name'] . '</td>
																	<td>' . $rr['status'] . '</td>';
													
													$nq = "SELECT * FROM `repo_notes` WHERE `cid` = '" . $rr['ID'] . "' AND `inactive` != 'Yes'";
													$ng = mysqli_query($conn, $nq) or die($conn->error);
													
													echo '<td style="max-height:160px;">
																<div style="max-width:250px;max-height:150px;overflow:scroll;color:black;">';
													while($nr = mysqli_fetch_array($ng)){
														echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-comments-o"></i> ' . $nr['rep_name'] . ' ' . date("m/d/y h:m A",strtotime($nr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . mysqli_real_escape_string($conn,$nr['note']) . '
													</div>';
													}
													echo '</div>
																</td>';
													
													if($rr['status'] == 'Pending' || $rr['status'] == '2nd Attempt'){
																	echo '<td style="background:white;">
																		<button type="button" class="btn btn-warning btn-xs" onclick="load_modal(\'note\',' . $rr['ID'] . ',\'' . $rr['cname'] . '\');" data-toggle="modal" data-target="#repairNote">Add Note</button>
																		<button type="button" class="btn btn-primary btn-xs" onclick="move(' . $rr['ID'] . ',\'Scheduled\');">Schedule</button>
																		<br><br>
																		<button type="button" class="btn btn-danger btn-xs" style="background:black;" onclick="fetch_repair(' . $rr['ID'] . ');" data-toggle="modal" data-target="#newRepair"><i class="fa fa-pencil"></i> Edit</button>
																		<button type="button" class="btn btn-danger btn-xs" onclick="move(' . $rr['ID'] . ',\'Cancelled\');">Cancel</button>
																	</td>';
													}
													if($rr['status'] == 'Scheduled'){
														echo '<td style="background:white;">';
															echo '<button type="button" class="btn btn-danger btn-xs" onclick="move(' . $rr['ID'] . ',\'2nd Attempt\');">Not Completed</button>
																		<button type="button" class="btn btn-success btn-xs" onclick="move(' . $rr['ID'] . ',\'Completed\');">Completed</button>
																		<br><br>
																		<button type="button" class="btn btn-danger btn-xs" style="background:black;" onclick="fetch_repair(' . $rr['ID'] . ');" data-toggle="modal" data-target="#newRepair"><i class="fa fa-pencil"></i> Edit</button>
																		<button type="button" class="btn btn-warning btn-xs" onclick="load_modal(\'note\',' . $rr['ID'] . ',\'' . $rr['cname'] . '\');" data-toggle="modal" data-target="#repairNote">Add Note</button>
																	</td>';
													}
													if($rr['status'] == 'Draft'){
																	echo '<td style="background:white;">';
														
															echo '<button type="button" class="btn btn-warning btn-xs" onclick="load_modal(\'note\',' . $rr['ID'] . ',\'' . $rr['cname'] . '\');" data-toggle="modal" data-target="#repairNote">Add Note</button>
																		<button type="button" class="btn btn-danger btn-xs" style="background:black;" onclick="fetch_repair(' . $rr['ID'] . ');" data-toggle="modal" data-target="#newRepair"><i class="fa fa-pencil"></i> Edit</button>
																		<br><br>
																		<button type="button" class="btn btn-danger btn-xs" onclick="move(' . $rr['ID'] . ',\'Cancelled\');">Cancel</button>
																	</td>';
													}
														echo '</tr>';
												}
												
												?>
											</table>
										</div>
									</div>
									</div>
							</div><!--End Row-->
							
							
							
							
							</div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>


  
	

	
	
	<? include 'repos/modals/new-repo-modal.php'; ?>
	
	

	
 <!-- Modal -->
  <div class="modal fade" id="repairNote" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Note For <span id="cust_name" style="color:red;"></span>'s Repo</h4>
        </div>
        <div class="modal-body">
					<p>Please Enter Your Note:</p>
					<textarea class="form-control" name="rnote" id="rnote"></textarea>
					<p id="rnote_error" style="color:red;font-weight:bold;"></p>
					<input type="hidden" id="rcname" value="" />
					<input type="hidden" id="cust_id" value="" />
				</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success" id="sub_btn" onclick="add_note();">Submit</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->

	
	
	
	<script src="js/jquery.js"></script>
	
 <?php include 'footer.html'; ?>
	
	
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

	
	
 
</body>

</html>