<?php
include '../php/connection.php';

//Load Variables...
$id = $_GET['id'];//ReportID
$rep_name = $_GET['rep_name'];
$rep_id = $_GET['rep_id'];

//Checkbox Trigger...
$trigger = false;//If a checkbox is not checked (from the DB query) this will set this value to true.

$iq = "SELECT * FROM `expenses` WHERE `reportID` = '" . $id . "' AND `inactive` != 'Yes'";
$ig = mysqli_query($conn, $iq) or die($conn->error);
while($ir = mysqli_fetch_array($ig)){
  if($ir['checked'] != 'true'){
    $trigger = true;
  }
}

if($trigger == true){
  echo 'All of the expense items must have the checkbox checked in order to approve this expense report!';
}else{
  
  $q = "UPDATE `expense_reports` SET
        `approved_by_id` = '" . $rep_id . "',
        `approved_by_name` = '" . $rep_name . "',
        `approved_date` = CURRENT_DATE,
        `approved_time` = CURRENT_TIME,
        `status` = 'Approved'
        WHERE `reportID` = '" . $id . "'";
  mysqli_query($conn, $q) or die($conn->error);
  
  echo 'Expense Report ID: ' . $id . ' has been successfully approved!';
  
}

?>