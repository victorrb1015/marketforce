<?php
include '../php/connection.php';

$rID = uniqid();

//Load Variables
$rep_id = $_GET['rep_id'];
$rep_name = $_GET['rep_name'];


$iq = "INSERT INTO `expense_reports`
        (
        `reportID`,
        `sub_date`,
        `sub_time`,
        `rep_id`,
        `rep_name`,
        `status`
        )
        VALUES
        (
        '" . $rID . "',
        CURRENT_DATE,
        CURRENT_TIME,
        '" . $rep_id . "',
        '" . $rep_name . "',
        'Pending'
        )";
mysqli_query($conn, $iq) or die($conn->error);

echo 'Expense Report ID: ' . $rID . ' created. ';

$uq = "UPDATE `expenses` SET
        `sub_date` = CURRENT_DATE,
        `sub_time` = CURRENT_TIME,
        `reportID` = '" . $rID . "'
        WHERE 
        `rep_id` = '" . $rep_id . "'
        AND
        `reportID` = ''";
mysqli_query($conn, $uq) or die($conn->error);

echo 'All current items submitted in report ' . $rID . '!';

?>