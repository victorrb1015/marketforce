<?php
include '../php/connection.php';


//Load Variables
$notes = mysqli_real_escape_string($conn, $_POST['notes']);
$rID = $_POST['rID'];
$mode = $_POST['mode'];
$rep_name = $_POST['rep_name'];
$rep_id = $_POST['rep_id'];

$date = date("m/d/y h:iA");

$q = "SELECT * FROM `expense_reports` WHERE `reportID` = '" . $rID . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);


$sjson = json_decode($r['notes']);
//echo 'Line 1<br>';

$addition = array('Stamp' => $date . ' ' . $rep_name, 'Note' => $notes);
 
$i = 1;
foreach($sjson as $x){
  //echo $x->Stamp . ' -> ' . $x->Note . '<br><br>';
  $newJSON[$i] = array('Stamp' => $x->Stamp, 'Note' => $x->Note);
  $i++;
}
$newJSON[$i] = $addition;

//echo 'Line 2<br>';

$ejson = json_encode($newJSON);

//echo 'Line 3<br>';

$uq = "UPDATE `expense_reports` SET
      `notes` = '" . $ejson . "'
      WHERE `reportID` = '" . $rID . "'";
mysqli_query($conn, $uq) or die($conn->error);

echo 'Your notes saved successfully!';
echo '<script>
       window.location = "../expense-reports.php";
      </script>';
?>

