<?php
include '../php/connection.php';

//Load Variables
$rid = $_GET['rid'];

//Get Report Status...
$sq = "SELECT * FROM `expense_reports` WHERE `reportID` = '" . $rid . "'";
$sg = mysqli_query($conn, $sq) or die($conn->error);
$sr = mysqli_fetch_array($sg);

//Get Contents of Report
$q = "SELECT * FROM `expenses` WHERE `reportID` = '" . $rid . "' AND `inactive` != 'Yes' ORDER BY `item_date` ASC";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  if($r['checked'] == 'true'){
    $check = 'checked';
  }else{
    $check = '';
  }
  echo '<div class="row" style="width:95%;margin:auto;">
          <div class="col-lg-12 well">
          <div class="col-lg-10">
            <h5><u><b>' . date("m/d/y", strtotime($r['item_date'])) . '
            &nbsp;&nbsp;&nbsp;&nbsp;
            ' . $r['city'] . ', ' . $r['state'] . '
            &nbsp;&nbsp;&nbsp;&nbsp;
            ' . $r['category'] . '
            </u></b></h5>
            </div>
            <div class="col-lg-2" style="text-align:right;">
            <input type="checkbox" style="height:20px; width:20px;" class="disable" id="approve_' . $r['ID'] . '" name="approve_' . $r['ID'] . '" onchange="check_item(' . $r['ID'] . ');" ' . $check . ' />
            <br>
            <p style="color:blue;" id="check_res_' . $r['ID'] . '"></p>
            </div>
            <div class="col-lg-6">
            ' . $r['item'] . '
            &nbsp;&nbsp;&nbsp;&nbsp;';
  if($r['category'] == 'Mileage'){
    echo $r['miles'] . ' Miles';
  }else{
        echo '$' . $r['amount'];
  }
      echo '<br><br>
            <b><u>Notes:</u></b>
            <br>
            ' . $r['notes'] . '
            </div>
            <div class="col-lg-6">
              <a href="' . $r['receipt_img'] . '" target="_blank">
              <img src="' . $r['receipt_img'] . '" style="width:100%;">
              </a>
              <input type="hidden" id="rID" name="rID" value="' . $r['reportID'] . '" />
              <input type="hidden" id="hrep_id" name="hrep_id" value="' . $r['rep_id'] . '" />
            </div>
          </div>
        </div>';
}
//JSON Notes Code...
$rq = "SELECT * FROM `expense_reports` WHERE `reportID` = '" . $rid . "'";
$rg = mysqli_query($conn, $rq) or die($conn->error);
$rr = mysqli_fetch_array($rg);
$sjson = json_decode($rr['notes']);

echo '<div class="row" style="width:95%;margin:auto;">
      <h4><u>Notes:</u></h4>
      <div class="col-lg-12 alert alert-info" style="max-height:350px; overflow:scroll;">';
      foreach($sjson as $x){
        echo '<div class="col-lg-12 well" style="color:black;">
                  <h5><u>' . $x->Stamp . '</u></h5>
                  <p>' . $x->Note . '</p>
                  </div>';
      }
     echo '</div>
        </div>';



?>