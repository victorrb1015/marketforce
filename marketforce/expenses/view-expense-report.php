<?php
//session_start();
include '../php/connection.php';


//Load Variables
$rid = $_GET['rid'];



//Get Expense Items for Report...
$eq = "SELECT * FROM `expenses` WHERE inactive like 'No' AND `reportID` = '" . $rid . "'";
$eg = mysqli_query($conn, $eq) or die($conn->error);

//Get Report Info...
$rq = "SELECT * FROM `expense_reports` WHERE `reportID` = '" . $rid . "'";
$rg = mysqli_query($conn, $rq) or die($conn->error);
$rr = mysqli_fetch_array($rg);
?>
<html>
  <head>
    <title>Expense Report</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js" integrity="sha384-u/bQvRA/1bobcXlcEYpsEdFVK/vJs3+T+nXLsBYJthmdBuavHvAW6UsmqO2Gd/F9" crossorigin="anonymous"></script>
 <style>
   body {
    -webkit-print-color-adjust: exact !important;
   }
   div{
     //border: 2px solid black;
   }
   .borders div{
     border:2px solid black;
   }
   @media print {
  @page { margin: 0; }
  body { margin: 1.6cm; }
  footer {page-break-after: always;}
}
 </style>
  </head>
  <body>
    
  <div class="main" style="width:80%;margin:auto;">
    <br><br>
    
    <div class="row">
      <div class="col-4" style="text-align:center;">
        <img src="../img/all-steel-red.jpg" alt="Logo" style="width:100%;" />
      </div>
      <div class="col-8" style="text-align:right;">
        <h3>All Steel Carports</h3>
        <p>2200 N. Granville Ave. Muncie, Indiana 47303</p>
        <p>Phone: (765) 284-0694</p>
        <p>Email: info@allsteelcarports.com</p>
      </div>
    </div>
  
    <div class="row">
      <div class="col-12" style="text-align:center;">
        <h1>Expense Report</h1>
        <p style="font-weight:bold;"><? echo $rr['rep_name']; ?></p>
        <p>Submitted: <? echo date("l, F dS Y", strtotime($rr['sub_date'])); ?></p>
        <p>Report ID: <? echo $rid; ?></p>
      </div>
    </div>
    
    
    <div class="row" style="font-weight:bold;font-size:20px;color:black;background:#727272;">
      <div class="col">
        Date
      </div>
      <div class="col">
        Category
      </div>
      <div class="col">
        Item Description
      </div>
      <div class="col">
        Location
      </div>
      <div class="col">
        Mileage
      </div>
      <div class="col">
        Price/Gal.
      </div>
      <div class="col">
        # Gallons
      </div>
      <div class="col">
        Amount
      </div>
    </div>
    
    
    <?php
    $rnum = 1;
    $te = 0;
    $tm = 0;
    $food = 0;
    $gas = 0;
    $hotel = 0;
    $supplies = 0;
    $misc = 0;
    $ng = 0;
    $pg = 0;
    $valores = [];
    while($r = mysqli_fetch_array($eg)){
      if($rnum % 2 == 0){
        $style = 'style="background:#B2B2B2;"';
      }else{
        $style = '';
      }
      echo '<div class="row" ' . $style . '>
              <div class="col">
                ' . date("m/d/y",strtotime($r['date'])) . '
              </div>
              <div class="col">
                ' . $r['category'] . '
              </div>
              <div class="col">
                ' . $r['item'] . '
              </div>
              <div class="col">
                ' . $r['city'] . ', ' . $r['state'] . '
              </div>
              <div class="col">
                ' . $r['miles'] . '
              </div>
              <div class="col">';
           if($r['ppg'] != ''){
             echo '$' . $r['ppg'];
         }
        echo '</div>
              <div class="col">
                ' . $r['num_gallons'] . '
              </div>
              <div class="col">';
         if($r['amount'] != ''){
             echo '$' . $r['amount_decimal'];
         }
        echo '</div>
            </div>';
      $rnum++;
      $val2 = floatval($r['amount_decimal']);
      array_push($valores,$val2);
      $te = $te + $r['amount_decimal'];
      $tm = $tm + $r['miles'];
      if($r['category'] == 'Food'){
        $food = $food + $r['amount_decimal'];
      }
      if($r['category'] == 'Vehicle/Gas'){
        $gas = $gas + $r['amount_decimal'];
        $pg = $pg + $r['ppg'];
        
        if($r['ppg'] != 0){
          $cgas = $cgas + $r['amount_decimal'];
        }
        
        $ng = $ng + $r['num_gallons'];
      }
      if($r['category'] == 'Office Supplies'){
        $supplies = $supplies + $r['amount_decimal'];
      }
      if($r['category'] == 'Hotel'){
        $hotel = $hotel + $r['amount_decimal'];
      }
      if($r['category'] == 'Misc'){
        $misc = $misc + $r['amount_decimal'];
      }
      
    }
    
    ?>
    
    
    <br><br>
    
    <div class="row">
      <div class="col-9">
        <?
          $sjson = json_decode($rr['notes']);

          echo '<div class="row" style="width:95%;margin:auto;">
                <h5>Notes:</h5>';
                foreach($sjson as $x){
                  if($x->Note != ''){
                  echo '<div class="col-12 card bg-light" style="color:black;">
                            <h6>' . $x->Stamp . '</h6>
                            <p>' . $x->Note . '</p>
                            </div>';
                  }
                }
               echo '</div>';
        ?>
      </div>
      
      <div class="col-3" class="borders">
        
        <div class="row">
          <div class="col" style="background:#B2B2B2;border: 1px solid black;">
            <b>Total Expenses</b>
          </div>
          <div class="col" style="border: 1px solid black;">
            <b>$<? echo number_format($te,2); ?></b>
            <!--<b><? echo $valores[0]?></b>-->
          </div>
        </div>
        
        <div class="row">
          <div class="col" style="background:#B2B2B2;border: 1px solid black;">
            <b> Miles</b>
          </div>
          <div class="col" style="border: 1px solid black;">
            <? echo number_format($tm); ?>
          </div>
        </div>
        
        <div class="row">
          <div class="col" style="background:#B2B2B2;border: 1px solid black;">
            <b>Food</b>
          </div>
          <div class="col" style="border: 1px solid black;">
            $<? echo number_format($food,2); ?>
          </div>
        </div>
        
        <div class="row">
          <div class="col" style="background:#B2B2B2;border: 1px solid black;">
            <b>Gas</b>
          </div>
          <div class="col" style="border: 1px solid black;">
            $<? echo number_format($gas,2); ?>
          </div>
        </div>
        
        <div class="row">
          <div class="col" style="background:#DCDCDC;border: 1px solid black;">
            &nbsp;&nbsp;&nbsp;&nbsp;<b>Avg. Price/Gal.</b>
          </div>
          <div class="col" style="border: 1px solid black;">
            $<? 
            $nr = $rnum - 1;
            //$apg = $pg / $nr;
            $apg = $cgas / $ng;
            echo number_format($apg,2); 
            ?>
          </div>
        </div>
        
        <div class="row">
          <div class="col" style="background:#DCDCDC;border: 1px solid black;">
            &nbsp;&nbsp;&nbsp;&nbsp;<b># Gallons</b>
          </div>
          <div class="col" style="border: 1px solid black;">
            <? echo number_format($ng); ?>
          </div>
        </div>
        
        <div class="row">
          <div class="col" style="background:#B2B2B2;border: 1px solid black;">
            <b>Hotel</b>
          </div>
          <div class="col" style="border: 1px solid black;">
            $<? echo number_format($hotel,2); ?>
          </div>
        </div>
        
        <div class="row">
          <div class="col" style="background:#B2B2B2;border: 1px solid black;">
            <b>Office Supplies</b>
          </div>
          <div class="col" style="border: 1px solid black;">
            $<? echo number_format($supplies,2); ?>
          </div>
        </div>
        
        <div class="row">
          <div class="col" style="background:#B2B2B2;border: 1px solid black;">
            <b>Misc.</b>
          </div>
          <div class="col" style="border: 1px solid black;">
            $<? echo number_format($misc,2); ?>
          </div>
        </div>
        
      </div>
      
    </div>
    
      
    <br><br><br>
  </div>
    
    <footer></footer>
    
    <?php
    
      echo '<div class="row">';
    $iq = "SELECT * FROM `expenses` WHERE `reportID` = '" . $rid . "'";
    $ig = mysqli_query($conn, $iq) or die($conn->error);
    while($ir = mysqli_fetch_array($ig)){
      if($ir['receipt_img'] != ''){
        echo '<div class="col-6" style="margin-bottom:10px;">';
        echo '<img src="' . $ir['receipt_img'] . '" alt="Receipt Image" style="width:100%;" />';
        echo '</div>';
        //echo '<footer></footer>';
      }
    }
    echo '</div>'
    ?>
    
  </body>
</html>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    