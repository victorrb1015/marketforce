<?php
include 'security/session/session-settings.php';
include 'php/connection.php';

if($_SESSION['in'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}


$pageName = 'Sales';//Set this equal to the name of the page you would like to set...
$pageIcon = 'fa fa-file';//Set this equal to the class name of the icon you would like to set as the page icon...

//Cache Buster...
$cache_code = uniqid();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
  
      <!--Bootstrap 4 Code-->
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->


    <title><?php echo $pageName; ?> - Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/bs-custom.css" rel="stylesheet" type="text/css">


  
    <!--Form Steps Bootstrap CSS-->
    <link href="orders/css/form-steps.css" rel="stylesheet" type="text/css">
  
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
		th, td{
			padding: 10px;
		}
		#dl, dc{
			height: 430px;
		}
		.panel-body{
			height: 382px;
		}
		#scroll{
			overflow: scroll;
		}
		::-webkit-scrollbar {
    width: 0px;  /* remove scrollbar space */
    background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
		
		
		.packet-table td, th{
			border: 1px solid black;
		}
		.packet-table{
			margin:auto;
		}
    
    /*CSS for Tabs*************************************************************/
    .nav-tabs>li.active>a, 
    .nav-tabs>li.active>a:focus, 
    .nav-tabs>li.active>a:hover{
      background: #b3b0b0;
      color: black !Important;
      border: 1px solid black;
      border-bottom-color: transparent;
    }
    .nav-tabs>li>a{
      border: 1px solid #ddd;
      border-bottom-color: transparent;
    }   
  </style>
  <script>
		
  var rep_id = '<?php echo $_SESSION['user_id']; ?>';
  var rep_name = '<?php echo $_SESSION['full_name']; ?>';
		
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
       <?php 
        if($_SESSION['position'] == 'Shop Employee'){
          include 'shop-nav.php';
        }else{
          include 'nav.php';
        }
       ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> <?php echo $pageName; ?></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
														<li>
															<i class="<?php echo $pageIcon; ?>"></i> <?php echo $pageName; ?>
													</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
            
       <div class="container" style="width:100%; margin:auto;">
      <?php
         $orders_aria = 'aria-expanded="false"';
         $customers_aria = 'aria-expanded="false"';
         $products_aria = 'aria-expanded="false"';
         $financing_aria = 'aria-expanded="false"';
         $reports_aria = 'aria-expanded="false"';
         $portal_users_aria = 'aria-expanded="false"';
         switch ($_GET['tab']){
           case 'orders':
             $orders_tab = 'active';
             $orders_section = 'active in';
             $orders_aria = 'aria-expanded="true"';
             break;
           case 'customers':
             $customers_tab = 'active';
             $customers_section = 'active in';
             $customers_aria = 'aria-expanded="true"';
             break;
           case 'products':
             $products_tab = 'active';
             $products_section = 'active in';
             $products_aria = 'aria-expanded="true"';
             break;
           case 'financing':
             $financing_tab = 'active';
             $financing_section = 'active in';
             $financing_aria = 'aria-expanded="true"';
             break;
           case 'reports':
             $reports_tab = 'active';
             $reports_section = 'active in';
             $reports_aria = 'aria-expanded="true"';
             break;
           case 'portal_users':
             $portal_users_tab = 'active';
             $portal_users_section = 'active in';
             $portal_users_aria = 'aria-expanded="true"';
             break;
           default:
             $orders_tab = 'active';
             $orders_section = 'active in';
             $orders_aria = 'aria-expanded="true"';
         }
       ?>
         
      <ul class="nav nav-tabs">
          <li class="nav-item <?php echo $orders_tab; ?>"><a data-toggle="tab" href="#orders" style="color:#337ab7;" <?php echo $orders_aria; ?>><span class="glyphicon glyphicon-shopping-cart"></span>  Quotes</a></li>
          <li class="nav-item <?php echo $customers_tab; ?>"><a data-toggle="tab" href="#customers" style="color:#337ab7;" <?php echo $customers_aria; ?>><span class="glyphicon glyphicon-user"></span> Customers</a></li>
        <!--  <li class="nav-item <?php echo $products_tab; ?>"><a data-toggle="tab" href="#products" style="color:#337ab7;" <?php echo $products_aria; ?>><span class="glyphicon glyphicon-barcode"></span> Products</a></li>
          <li class="nav-item <?php echo $financing_tab; ?>"><a data-toggle="tab" href="#financial" style="color:#337ab7;" <?php echo $financing_aria; ?>><span class="glyphicon glyphicon-piggy-bank"></span> Financing <span style="color:red;">{BETA<i class="fa fa-code"></i>}</span></a></li>-->
          <li class="nav-item <?php echo $reports_tab; ?>"><a data-toggle="tab" href="#reports" style="color:#337ab7;" <?php echo $reports_aria; ?>><span class="glyphicon glyphicon-list-alt"></span> Reports</a></li>
          <!-- <li class="nav-item <?php echo $portal_users_tab; ?>"><a data-toggle="tab" href="#portal_users" style="color:#337ab7;" <?php echo $portal_users_aria; ?>><i class="fa fa-group"></i> Portal Users</a></li>-->
      </ul>
      
      <div class="tab-content" id="myTabContent">

        
        <div class="tab-pane fade <?php echo $orders_section; ?>" id="orders" role="tabpanel"><!-----------------------------START QUOTES TAB CONTENT----------------------------->
          <div class="tab-content" id="mySubTabContent">
          <ul class="nav nav-tabs">
            <li class="nav-item active"><a data-toggle="tab" href="#est" style="color:#337ab7;"><span class="glyphicon glyphicon-shopping-cart"></span> Pending Quotes</a></li>
            <li class="nav-item"><a data-toggle="tab" href="#invoiced" style="color:#337ab7;"><span class="glyphicon glyphicon-user"></span> Invoiced Quotes</a></li>
            <li class="nav-item"><a data-toggle="tab" href="#paid" style="color:#337ab7;"><span class="glyphicon glyphicon-barcode"></span> Paid Quotes</a></li>
          </ul>

          <div class="tab-pane active in" id="est" role="tabpanel">
          <?php
              //Add Order Tracking Sections Here...
              include 'sales/sections/orders-display-table.php';
           ?>
         </div>
         <div class="tab-pane fade" id="invoiced" role="tabpanel">
          <?php
            include 'sales/sections/invoiced-orders-display-table.php';
          ?>
          </div>
         <div class="tab-pane fade" id="paid" role="tabpanel">
          <?php
            include 'sales/sections/paid-orders-display-table.php';
          ?>
          </div>

        </div>
          
        </div><!-----------------------------END ORDERS TAB CONTENT----------------------------->
        
        <div class="tab-pane fade <?php echo $customers_section; ?>" id="customers" role="tabpanel"><!-----------------------------START CUSTOMERS TAB CONTENT----------------------------->
          <?php
             //Add customer display table
             include 'sales/sections/customer-display-table.php';
           ?>
          
        </div><!-----------------------------END CUSTOMERS TAB CONTENT----------------------------->
        
        <div class="tab-pane fade <?php echo $products_section; ?>" id="products" role="tabpanel"><!-----------------------------START PRODUCTS TAB CONTENT----------------------------->
          <?php
             //Add product display table
             include 'sales/sections/product-display-table.php';
           ?>
        </div><!-----------------------------END PRODUCTS TAB CONTENT----------------------------->
        
        <div class="tab-pane fade <?php echo $financing_section; ?>" id="financial" role="tabpanel"><!-----------------------------START FINANCING TAB CONTENT----------------------------->
          <?php
             //Add credit line display table
             include 'sales/sections/credit-line-display-table.php';
           ?>
        </div><!-----------------------------END FINANCING TAB CONTENT----------------------------->
        
        <div class="tab-pane fade <?php echo $reports_section; ?>" id="reports" role="tabpanel"><!-----------------------------START FINANCING TAB CONTENT----------------------------->
          <?php
             //Add credit line display table
             include 'sales/sections/reports-display-table.php';
           ?>
        </div><!-----------------------------END REPORTS TAB CONTENT----------------------------->
        
        <div class="tab-pane fade <?php echo $portal_users_section; ?>" id="portal_users" role="tabpanel"><!-----------------------------START FINANCING TAB CONTENT----------------------------->
          <?php
             //Add credit line display table
             include 'sales/sections/portal-users-display-table.php';
           ?>
        </div><!-----------------------------END REPORTS TAB CONTENT----------------------------->
        
    </div>
              
           
              
              
							
							
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
  
  <script>
    $('#newOrderForm').submit(function(){
      document.getElementById('newSubBTN').disabled = true;
    });
  </script>
  
      <script src="sales/js/order-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="sales/js/customer-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="sales/js/product-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="sales/js/credit-line-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="sales/js/special-pricing-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="sales/js/email-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="sales/js/color-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="sales/js/ship-address-functions.js?cb=<?php echo $cache_code; ?>"></script>
      <script src="sales/js/portal-user-functions.js?cb=<?php echo $cache_code; ?>"></script>
  
    <!-- Morris Charts JavaScript -->
    <!--<script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>-->

  <!----------------------------------------------------------Modals Go Here------------------------------------------------>
	<?php include 'sales/modals/add-note-modal.php'; ?>
  <?php include 'sales/modals/new-order-modal.php'; ?>
  <?php include 'sales/modals/info-box-modal.php'; ?>
  <?php include 'sales/modals/new-credit-line-modal.php'; ?>
  <?php include 'sales/modals/new-product-modal.php'; ?>
  <?php include 'sales/modals/new-special-pricing-modal.php'; ?>
  <?php include 'sales/modals/new-customer-modal.php'; ?>
  <?php include 'sales/modals/colors-modal.php'; ?>
  <?php include 'sales/modals/ship-address-modal.php'; ?>
  <?php include 'sales/modals/add-edit-portal-user-modal.php'; ?>
  <?php include 'sales/modals/new-order-date-modal.php'; ?>
  <?php include 'sales/modals/new-inv-number-modal.php'; ?>

 <?php include 'footer.html'; ?>
</body>

</html>