<?php
include 'security/session/session-settings.php';

if($_SESSION['collections'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--Form Steps Bootstrap CSS-->
    <link href="collections/legal/form-steps.css" rel="stylesheet" type="text/css">
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
		th, td{
			padding: 10px;
		}
		#dl, dc{
			height: 430px;
		}
		.panel-body{
			height: 382px;
		}
		#scroll{
			overflow: scroll;
		}
		::-webkit-scrollbar {
   // width: 0px;  /* remove scrollbar space */
    //background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
   // background: #FF0000;
}
		
		
		.packet-table td, th{
			border: 1px solid black;
		}
		.packet-table{
			margin:auto;
		}
    
    #hoverGreen{
      color:blue;
    }
    #hoverGreen:hover{
      color:green;
    }
		#hoverRed{
			color:black;
		}
		#hoverRed:hover{
			color:blue;
		}
		
		@media print{
			.note-box{
				width:200px;
			}
		}
  </style>
  <script>
    var rep_id = '<?php echo $_SESSION['user_id']; ?>';
    var rep_name = '<?php echo $_SESSION['full_name']; ?>';
		
		//Variable for JSON DATA on customer fetch query
		var q = '';
    
  function clr_res(id,s){
    //alert("test");
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			//alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://library.burtonsolution.tech/php/reserve.php?id="+id+"&s="+s,true);
  xmlhttp.send();
  }
		
		
  function log_call(){
		//alert("Log_Call was activated!");
		
		var id = document.getElementById('call_mid').value;
    var tphone = document.getElementById('tphone').value;
    if(tphone === ''){
      document.getElementById('error_msg').innerHTML = 'Please Enter The Phone Number Tried!';
      return;
    }
		tphone = urlEncode(tphone);
		if(document.querySelector('input[name="rnote"]:checked') != null){
  		var note = document.querySelector('input[name="rnote"]:checked').value;
  		}else{
				document.getElementById('error_msg').innerHTML = '*Please Specify Call Response!';
  		  return;
  	}
		var call_note = document.getElementById('call_note').value;
		call_note = urlEncode(call_note);
		
    $("#callModal").modal("hide");
    var URL = "collections/log-call.php?cid="+id+"&rep_id="+rep_id+"&rep_name="+rep_name+"&tphone="+tphone+"&note="+note+"&call_note="+call_note;
    var eURL = URL;
    
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET",eURL,true);
  xmlhttp.send();
  }
    
  
  function log_note(){
    
		var id = document.getElementById('note_mid').value;
		
		var note = document.getElementById('anote').value;
		if(note == ''){
			document.getElementById('note_error_msg').innerHTML = 'Please enter a note!';
			return;
		}else{
			$("#noteModal").modal("hide");
		}
		note = urlEncode(note);
		
    var URL = "collections/log-note.php?cid="+id+"&rep_id="+rep_id+"&rep_name="+rep_name+"&note="+note;
    var eURL = URL;
		
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET",eURL,true);
  xmlhttp.send();
  }
    
    
  function clear_customer(id){
		
    var URL = "collections/clear-customer.php?cid="+id;
    var eURL = URL;
		
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET",eURL,true);
  xmlhttp.send();
  }
    
		
	function remove_customer(id){
    
		
		
    var URL = "collections/deactivate.php?cid="+id;
    var eURL = URL;
		
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET",eURL,true);
  xmlhttp.send();
  }
		
		
    
  function reassign_user(){
    
		var id = document.getElementById('reassign_mid').value;
		var cname = document.getElementById('reassign_name').innerHTML;
		var nid = document.getElementById('reassign_nid').value;
		if(nid == ''){
			document.getElementById('reassign_error_msg').innerHTML = 'Please select a new Rep!';
			return;
		}else{
			$("#reassignModal").modal("hide");
		}
		
    var URL = "collections/reassign.php?cid="+id+"&rep_id="+rep_id+"&rep_name="+rep_name+"&nid="+nid+"&cust_name="+cname;
    var eURL = URL;
		
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET",eURL,true);
  xmlhttp.send();
  }
		
		
		function record_payment(){
    
		var id = document.getElementById('payment_mid').value;
		var cname = document.getElementById('payment_name').innerHTML;
		var amount = document.getElementById('payment_amount').value;
		var method = document.getElementById('payment_method').value;
		var cc_conf = '';
		var check_num = '';
		var edn = '';
		if(amount == ''){
			document.getElementById('payment_error_msg').innerHTML = 'Please Enter A Payment Amount!';
			return;
		}
			amount = amount.replace(/,/g,'');
			amount = urlEncode(amount);
			
		if(method === ''){
			document.getElementById('payment_error_msg').innerHTML = 'Please Specify The Payment Method!';
			return;
		}
		if(method === 'Check'){
			var check_num = document.getElementById('check_num').value;
			if(check_num === ''){
				document.getElementById('payment_error_msg').innerHTML = 'Please Enter The Check Number!';
				return;
			}
			check_num = urlEncode(check_num);
		}
		if(method === 'Credit Card'){
			var cc_conf = document.getElementById('cc_conf').value;
			if(cc_conf === ''){
				document.getElementById('payment_error_msg').innerHTML = 'Please Enter The CC Transaction Confirmation Number!';
				return;
			}
			cc_conf = urlEncode(cc_conf);
		}
		if(method === 'Electronic Payment'){
			var edn = document.getElementById('edn').value;
			if(edn === ''){
				document.getElementById('payment_error_msg').innerHTML = 'Please Enter A Note About The Electronic Payment!';
				return;
			}
			edn = urlEncode(edn);
		}
			$("#paymentModal").modal("hide");
		
		
    var URL = "collections/record-payment.php?cid="+id+"&rep_id="+rep_id+"&rep_name="+rep_name+"&amount="+amount+"&cust_name="+cname+"&method="+method+"&check_num="+check_num+"&cc_conf="+cc_conf+"&edn="+edn;
    var eURL = URL.replace("&","%26"); 
			var eURL = eURL.replace("#","%23");;
		
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET",URL,true);
  xmlhttp.send();
  }
		
		
    
	function add_collections(){
    
		var note = document.getElementById('add_note').value;
		note = urlEncode(note);
		var nfn = document.getElementById('nfull_name').value;
		if(nfn === ''){
			document.getElementById('add_error_msg').innerHTML = 'Please Enter The Customer&apos;s Full Name!';
			return;
		}
		nfn = urlEncode(nfn);
		var nphone = document.getElementById('nphone').value;
		var nemail = document.getElementById('nemail').value;
		if(nphone === '' && nemail === ''){
			document.getElementById('add_error_msg').innerHTML = 'Please Enter The Customer&apos;s Phone Number or Email Address!';
			return;
		}
		nphone = urlEncode(nphone);
		nemail = urlEncode(nemail);
		var nstate = document.getElementById('nstate').value;
		if(nstate === ''){
			document.getElementById('add_error_msg').innerHTML = 'Please Enter The Customer&apos;s State!';
			return;
		}
		var ninvoice = document.getElementById('ninvoice').value;
		if(ninvoice === ''){
			document.getElementById('add_error_msg').innerHTML = 'Please Enter The Customer&apos;s Invoice Number!';
			return;
		}
		ninvoice = urlEncode(ninvoice);
		var namount = document.getElementById('namount').value;
		if(namount === ''){
			document.getElementById('add_error_msg').innerHTML = 'Please Enter The Amount To Be Collected From The Customer!';
			return;
		}
		namount = namount.replace(/,/g,'');
		namount = urlEncode(namount);
		var ncat = document.getElementById('ncat').value;
		if(ncat === ''){
			document.getElementById('add_error_msg').innerHTML = 'Please Enter The Customer&apos;s Debt Category!';
			return;
		}
		var nrep = document.getElementById('nrep').value;
		if(nrep === ''){
			document.getElementById('add_error_msg').innerHTML = 'Please Assign A Rep To The Customer!';
			return;
		}
		
		document.getElementById('addCust').submit();
			$("#addCollection").modal("hide");
		setTimeout(function (){
			window.location.reload();
		},750);
		
    var URL = "collections/add-customer.php?nrep="+nrep+"&namount="+namount+"&nfn="+nfn+"&ncat="+ncat+"&ninvoice="+ninvoice+"&nstate="+nstate+"&nemail="+nemail+"&nphone="+nphone+"&note="+note;
		var eURL = URL;
		var pURL = "collections/add-customer.php";
		var params = "nrep="+nrep+"&namount="+namount+"&nfn="+nfn+"&ncat="+ncat+"&ninvoice="+ninvoice+"&nstate="+nstate+"&nemail="+nemail+"&nphone="+nphone+"&note="+note;
		
    /*if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  //xmlhttp.open("GET",eURL,true);
  //xmlhttp.send();
	xmlhttp.open("POST", pURL, true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send(params);*/
		
		
		function redo(){
		window.location.reload();
		}
		setTimeout(redo,3000);
  }
		
		
	function fetch_cust(id){
		
		if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //alert(this.responseText);
			q = JSON.parse(this.responseText);//This puts the JSON data into useable data
      //window.location.reload();
      //Load Modal Fields Here...
			document.getElementById('add_title').innerHTML = 'Edit Details For <span style="color:red;font-weight:bold;">' + q.name + '</span>';
			document.getElementById('nfull_name').value = q.name;
			document.getElementById('nphone').value = q.phone;
			document.getElementById('nemail').value = q.email;
			document.getElementById('nstate').value = q.state;
			document.getElementById('ninvoice').value = q.inv;
			document.getElementById('namount').value = q.initial_debt;
			document.getElementById('ncat').value = q.cat;
			document.getElementById('nrep').value = q.rep_id;
			document.getElementById('ndebt').value = q.cdebt;
			document.getElementById('ndebt').disabled = false;
			if(q.file_url != ''){
				document.getElementById('attach').innerHTML = '<a href="' + q.file_url + '" target="_blank" style="color:blue;font-weight:bold;">View File</a>';
			}
			document.getElementById('auto_frequency').value = q.auto_frequency;
			document.getElementById('mode').value = 'Edit';
			document.getElementById('edit_id').value = q.ID;
			document.getElementById('orig_cat').value = q.cat;
			document.getElementById('add_note').value = q.notes;
			$("#addCollection").modal("show");
    }
  }
  xmlhttp.open("GET","collections/php/fetch-customer.php?id="+id,true);
  xmlhttp.send();
	}
		
		
	function load_modal(id,x,name,pn){
		if(x === 1){
			document.getElementById('call_mid').value = id;
			document.getElementById('call_name').innerHTML = name;
			document.getElementById('phone_num').innerHTML = pn;
		}
		if(x === 2){
			document.getElementById('note_mid').value = id;
			document.getElementById('note_name').innerHTML = name;
		}
		if(x === 3){
			document.getElementById('reassign_mid').value = id;
			document.getElementById('reassign_name').innerHTML = name;
		}
		if(x === 4){
			document.getElementById('payment_mid').value = id;
			document.getElementById('payment_name').innerHTML = name;
		}
	}
		
		
	function load_num(){
		var cb = document.getElementById('same');
		if(cb.checked === true){
			document.getElementById('tphone').value = document.getElementById('phone_num').innerHTML;
		}else{
			document.getElementById('tphone').value = '';
		}
	}
		
		
	function urlEncode(url){
		url = url.replace(/&/g, '%26'); 
		url = url.replace(/#/g, '%23');
		return url;
	}
		
		
		
	function pause_vid(){
		var vid = document.getElementById("auto-plan"); 
    vid.pause(); 
	}
	function play_vid(){
		var vid = document.getElementById("auto-plan"); 
		vid.play();
	}
		
	
	function setup_new(){
		document.getElementById('ndebt').disabled = true;
	}
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
       <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> Display Collections</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
														<li>
															<i class="fa fa-home"></i> Display Collections
													</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							
							
							
							
							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-red">
										<div class="panel-heading">
											<h3 class="panel-title">
											 Collection Customers To Be Contacted:
													<br><br>
                          <!-- Single button -->
                          <!--<div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              View <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="legal.php?view=ALL">All Customers</a></li>
                              <li><a href="legal.php?view=mine">My Customers</a></li>
															<li><a href="legal.php?view=HL">Amount High->Low</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="legal.php?view=ck_ca_cc">ck_ca_cc</a></li>
                              <li><a href="legal.php?view=po">po</a></li>
                              <li><a href="legal.php?view=nsf_stop">nsf_stop</a></li>
                              <li><a href="legal.php?view=repair">repair</a></li>
                              <li><a href="legal.php?view=dealer_pmt">dealer_pmt</a></li>
                              <li><a href="legal.php?view=dealer_fin">dealer_fin</a></li>
                              <li><a href="legal.php?view=legal">legal</a></li>
                              <li><a href="legal.php?view=bli">bli</a></li>
                              <li><a href="legal.php?view=ezpay">ezpay</a></li>
                              <li><a href="legal.php?view=scoggin">scoggin</a></li>
                              <li><a href="legal.php?view=double_d">double_d</a></li>
                              <li><a href="legal.php?view=dallen_legacy">dallen_legacy</a></li>
                              <li><a href="legal.php?view=rto_national">rto_national</a></li>
                            </ul>
                          </div>-->
												  <!--
													<button type="button" class="btn btn-success" onclick="setup_new();" data-toggle="modal" data-target="#addCollection">
													Add Customer
													</button>
                          -->
											</h3>
										</div>
										<div class="panel-body" style="height:auto;">
											<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
                        <th>Name</th>
                        <th>Details</th>
                        <th>Category</th>
                        <th>Amount</th>
												<th>Rep</th>
												<th>Auto Freq</th>
                        <th>Notes</th>
												<th>File</th>
                        <th>Actions</th>
                      </thead>
							        <tbody>
                        
                <?php
                /*if($_GET['view'] == 'ALL'){  
                	$ciq = "SELECT * FROM `collections` WHERE `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
								}elseif($_GET['view'] == 'mine' || !isset($_GET['view'])){
                	$ciq = "SELECT * FROM `collections` WHERE `assigned_rep_id` = '" . $_SESSION['user_id'] . "' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
								}elseif($_GET['view'] == 'ck_ca_cc'){
                	$ciq = "SELECT * FROM `collections` WHERE `ck_ca_cc` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'po'){
                	$ciq = "SELECT * FROM `collections` WHERE `po` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'nsf_stop'){
                	$ciq = "SELECT * FROM `collections` WHERE `nsf_stop` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'repair'){
                	$ciq = "SELECT * FROM `collections` WHERE `repair` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'dealer_pmt'){
                	$ciq = "SELECT * FROM `collections` WHERE `dealer_pmt` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'dealer_fin'){
                	$ciq = "SELECT * FROM `collections` WHERE `dealer_fin` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'legal'){
                	$ciq = "SELECT * FROM `collections` WHERE `legal` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'bli'){
                	$ciq = "SELECT * FROM `collections` WHERE `bli` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'ezpay'){
                	$ciq = "SELECT * FROM `collections` WHERE `ezpay` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'scoggin'){
                	$ciq = "SELECT * FROM `collections` WHERE `scoggin` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'double_d'){
                	$ciq = "SELECT * FROM `collections` WHERE `double_d` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'dallen_legacy'){
                	$ciq = "SELECT * FROM `collections` WHERE `dallen_legacy` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'rto_national'){
                	$ciq = "SELECT * FROM `collections` WHERE `rto_national` != '' AND `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
                }elseif($_GET['view'] == 'HL'){
                	$ciq = "SELECT * FROM `collections` WHERE `inactive` != 'Yes' AND `collection` = 'Yes' AND `in_legal` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY FLOOR(`initial_debt`) DESC";
                }*/
                
                $ciq = "SELECT * FROM `collections` WHERE `inactive` != 'Yes' AND `in_display` = 'Yes' AND `collection` = 'Yes' AND `last_activity` != CURRENT_DATE ORDER BY `date` DESC, `ID` DESC";
								$cig = mysqli_query($conn, $ciq) or die($conn->error);
                        
                while($cir = mysqli_fetch_array($cig)){
									$cname = mysqli_real_escape_string($conn, $cir['name']);
									
                  $i = 0;
                  if($cir['ck_ca_cc'] != ''){
                    $cat = 'ck_ca_cc';
                    $i++;
                  }
                  if($cir['po'] != ''){
                    $cat = 'po';
                    $i++;
                  }
                  if($cir['nsf_stop'] != ''){
                    $cat = 'nsf_stop';
                    $i++;
                  }
                  if($cir['repair'] != ''){
                    $cat = 'repair';
                    $i++;
                  }
                  if($cir['dealer_pmt'] != ''){
                    $cat = 'dealer_pmt';
                    $i++;
                  }
                  if($cir['dealer_fin'] != ''){
                    $cat = 'dealer_fin';
                    $i++;
                  }
                  if($cir['legal'] != ''){
                    $cat = 'legal';
                    $i++;
                  }
                  if($cir['bli'] != ''){
                    $cat = 'bli';
                    $i++;
                  }
                  if($cir['ezpay'] != ''){
                    $cat = 'ezpay';
                    $i++;
                  }
                  if($cir['scoggin'] != ''){
                    $cat = 'scoggin';
                    $i++;
                  }
                  if($cir['double_d'] != ''){
                    $cat = 'double_d';
                    $i++;
                  }
                  if($cir['dallen_legacy'] != ''){
                    $cat = 'dallen_legacy';
                    $i++;
                  }
									if($cir['rto_national'] != ''){
										$cat = 'rto_national';
										$i++;
									}
                  
                  
                  echo '<tr>
                        <td>
                        <a href="collections/customer-report.php?cid=' . $cir['ID'] . '" style="color:black;">
                        ' . $cir['name'] . '
												</a>
												</td>
                        <td><b>Date:</b><br> ' . date("m/d/y", strtotime($cir['date'])) . '<br>
                            <b>Inv#:</b><br> ' . $cir['inv#'] . '<br>
                            <b>State:</b><br> ' . $cir['state'] . '<br>
                            <b>Phone:</b><br> <span style="white-space:nowrap;">' . $cir['phone'] . '</span>';
                  if($cir['phone'] != ''){
												//echo '&nbsp;&nbsp;<a href="tel:' . $cir['phone'] . '" title="' . $cir['phone'] . '" style="color:blue;"><i class="fa fa-phone"></i></a>';
									}else{
												//echo '&nbsp;&nbsp;<span style="color:gray;"><i class="fa fa-phone"></i></a>';
									}
                  
                      echo '<br>
                            <b>Email:</b><br> ';
                  if($cir['email'] != ''){
												echo '&nbsp;&nbsp;<a href="mailto:' . $cir['email'] . '" title="' . $cir['email'] . '" style="color:blue;"><i class="fa fa-envelope"></i></a>';
									}else{
												echo '&nbsp;&nbsp;<span style="color:gray;"><i class="fa fa-envelope"></i></span>';
									}
                       echo '</td>';
									
									
									
									echo '<td>' . $cat . '</td>
                        <td>' . number_format($cir[$cat],2) . '</td>
												<td>' . $cir['assigned_rep_name'] . '</td>
												<td>' . $cir['auto_frequency'] . '</td>
                        <td><div class="note-box" style="max-height:200px;width:400px;">';
												
									
                        echo '<div class="row bs-wizard" style="border-bottom:0;text-align:center;">
                                    
                                    
                                    
                                    <div class="col-xs-2 bs-wizard-step complete">
                                      <div class="text-center bs-wizard-stepnum">Legal</div>
                                      <div class="progress"><div class="progress-bar"></div></div>
                                      <a href="#" class="bs-wizard-dot"></a>
                                      <div class="bs-wizard-info text-center">Date</div>
                                    </div>
                                    
                                    <div class="col-xs-2 bs-wizard-step complete"><!-- complete -->
                                      <div class="text-center bs-wizard-stepnum">Letter 1</div>
                                      <div class="progress"><div class="progress-bar"></div></div>
                                      <a href="#" class="bs-wizard-dot"></a>
                                      <div class="bs-wizard-info text-center">Date</div>
                                    </div>
                                    
                                    <div class="col-xs-2 bs-wizard-step active"><!-- complete -->
                                      <div class="text-center bs-wizard-stepnum">Letter 2</div>
                                      <div class="progress"><div class="progress-bar"></div></div>
                                      <a href="#" class="bs-wizard-dot"></a>
                                      <div class="bs-wizard-info text-center">Date</div>
                                    </div>
                                    
                                    <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
                                      <div class="text-center bs-wizard-stepnum">Letter 3</div>
                                      <div class="progress"><div class="progress-bar"></div></div>
                                      <a href="#" class="bs-wizard-dot"></a>
                                      <div class="bs-wizard-info text-center">Date</div>
                                    </div>
                                    
                                    <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
                                      <div class="text-center bs-wizard-stepnum">Court</div>
                                      <div class="progress"><div class="progress-bar"></div></div>
                                      <a href="#" class="bs-wizard-dot"></a>
                                      <div class="bs-wizard-info text-center">Date</div>
                                    </div>
                                </div>';
                  
									
									/*$collnq = "SELECT * FROM `collections_calls`
														WHERE
														`customer_id` = '" . $cir['ID'] . "'
														ORDER BY `date` DESC";
									
									$collng = mysqli_query($conn, $collnq) or die($conn->error);
									
										$calls = 0;
                    $notes = 0;
                    $emails = 0;
                    $texts = 0;
										$payments = 0;
									while($collnr = mysqli_fetch_array($collng)){
										
										if($collnr['type'] == 'Note'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-comments-o"></i> ' . $collnr['rep_name'] . ' ' . date("m/d/y h:m A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . mysqli_real_escape_string($conn,$collnr['note']) . '
													</div>';
                      $notes++;
										}
										if($collnr['type'] == 'Phone Call'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-phone"></i> ' . $collnr['rep_name'] . ' ' . date("m/d/y h:m A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . $collnr['rep_name'] . ' attempted calling ' . $collnr['name'] . ' @ ' . $collnr['phone_tried'] . '. ' . $collnr['note'] . '
													</div>';
                      $calls++;
										}
                    if($collnr['type'] == 'Email'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-envelope"></i> ' . $collnr['rep_name'] . ' ' . date("m/d/y h:m A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . $collnr['rep_name'] . ' Automatically Emailed ' . $collnr['name'] . '.
													</div>';
                      $emails++;
										}
                    if($collnr['type'] == 'Text'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-mobile"></i> ' . $collnr['rep_name'] . ' ' . date("m/d/y h:m A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . $collnr['rep_name'] . ' Automatically SMS Texted ' . $collnr['name'] . '.
													</div>';
                      $texts++;
										}
										if($collnr['type'] == 'Payment'){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-usd"></i> ' . $collnr['rep_name'] . ' ' . date("m/d/y h:m A",strtotime($collnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . $collnr['rep_name'] . ' recorded a ' . $collnr['method'] . ' payment for ' . $collnr['name'] . ' in the amount of $' . $collnr['amount'] . '.';
											if($collnr['method'] == 'Check'){
												echo '<br>Check#: ' . $collnr['check_num'];
											}
											if($collnr['method'] == 'Credit Card'){
												echo '<br>Confirmation#: ' . $collnr['cc_conf'];
											}
											if($collnr['method'] == 'Electronic Payment'){
												echo '<br>Payment Note: ' . $collnr['edn'];
											}
													echo '</div>';
                      $payments++;
										}
									}
									if($cir['notes'] != ''){
										echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														Initial Note 
													</span></h4>
													<div class="well well-sm">
														' . $cir['notes'] . '
													</div><br>';
									}*/
                  
                  
                  
									
									
									echo '</div></td>';
												
									if($cir['file_url'] != ''){
										echo '<td style="text-align:center;"><a href="' . $cir['file_url'] . '" target="_blank"><h3 style="color:blue;"><i class="fa fa-file"></i></h3></a></td>';
									}else{
										echo '<td style="text-align:center;"><h3 style="color:grey;"><i class="fa fa-file"></i></h3></td>';
									}
									
                   echo '<td>
                          <h3 style="white-space:nowrap;">
                            <button type="button" class="btn btn-info btn-md" title="Log a Call" onclick="load_modal(' . $cir['ID'] . ',1,&apos;' . $cname . '&apos;,&apos;' . $cir['phone'] . '&apos;);" data-toggle="modal" data-target="#callModal" id="hoverGreen"><i class="fa fa-phone"></i></button>
                            <button type="button" class="btn btn-info btn-md" title="Add a Note" onclick="load_modal(' . $cir['ID'] . ',2,&apos;' . $cname . '&apos;);" data-toggle="modal" data-target="#noteModal" id="hoverGreen"><i class="fa fa-comments-o"></i></button>
                            <button type="button" class="btn btn-info btn-md" title="Record A Payment" onclick="load_modal(' . $cir['ID'] . ',4,&apos;' . $cname . '&apos;);" data-toggle="modal" data-target="#paymentModal" id="hoverGreen"><i class="fa fa-usd"></i></button>
														<br><br>
														<button type="button" class="btn btn-info btn-md" title="Re-Assign to another Rep" onclick="load_modal(' . $cir['ID'] . ',3,&apos;' . $cname . '&apos;);" data-toggle="modal" data-target="#reassignModal" id="hoverGreen"><i class="fa fa-user"></i></button>
                            <button type="button" class="btn btn-info btn-md" title="Done with Customer" onclick="clear_customer(' . $cir['ID'] . ');" id="hoverGreen"><i class="fa fa-check-square-o"></i></button>';
									if($_SESSION['user_id'] == 1 || $_SESSION['user_id'] == 3 || $_SESSION['user_id'] == 50){
                      echo '&nbsp;<button type="button" class="btn btn-info btn-md" title="Edit Customer" onclick="fetch_cust(' . $cir['ID'] . ');" id="hoverGreen"><i class="fa fa-pencil"></i></button>
														<br><br>
														<button type="button" class="btn btn-danger btn-md" title="Remove Customer" onclick="remove_customer(' . $cir['ID'] . ');" id="hoverRed"><i class="fa fa-times"></i></button>';
														}
										echo '</h3>
                        </td>
                        </tr>';
                  
                }
                        
                        
                        
                ?>
                        
							        </tbody>
										</table>
										</div>
										</div>
									</div>
								</div>
							</div><!--End Row-->
							
							
							
							
							</div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>


  
	
 <!-- Modal -->
  <div class="modal fade" id="callModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Log A Call For <span style="color:red;" id="call_name"></span></h4>
        </div>
        <div class="modal-body">
					<table>
						<tr>
							<td style="width:50%;text-align:center;">
								<p>Phone Number On File:</p>
								<h3><span class="label label-default" id="phone_num"></span></h3>
							</td>
							<td style="width:50%;text-align:center;">
          <p>Please Enter The Phone Number Tried:</p>
          <input type="phone" class="form-control col-lg-6" id="tphone" />
					<input type="checkbox" id="same" onchange="load_num();"/> Used Number On File
					<input type="hidden" id="call_mid" value="" />
							</td>
						</tr>
					</table>
					<br>
					<p>Please Specify Call Response:</p>
					<input type="radio" name="rnote" id="rnote" value="No Answer" /> No Answer
					&nbsp;&nbsp;
					<input type="radio" name="rnote" id="rnote" value="No Answer - Left Voicemail" /> No Answer - Left Voicemail
					&nbsp;&nbsp;
					<input type="radio" name="rnote" id="rnote" value="Talked With Customer" /> Talked With Customer
					<br><br>
					<p>Notes From Call:</p>
					<textarea id="call_note" class="form-control"></textarea>
					<br>
					<p id="error_msg" style="color:red;font-weight:bold;"></p>
        </div>
        <div class="modal-footer">
					<button type="button" class="btn btn-success" onclick="log_call();">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	
	
	 <!-- Modal -->
  <div class="modal fade" id="noteModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Log A Note For <span style="color:red;" id="note_name"></span></h4>
        </div>
        <div class="modal-body">
          <p>Enter A Note Here:</p>
					<textarea id="anote" style="width:100%;height:150px;"></textarea>
					<input type="hidden" id="note_mid" value="" />
					<br><br>
					<p id="note_error_msg" style="color:red;font-weight:bold;"></p>
        </div>
        <div class="modal-footer">
					<button type="button" class="btn btn-success" onclick="log_note();">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	
	
	
		 <!-- Modal -->
  <div class="modal fade" id="reassignModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Reassign <span style="color:red;" id="reassign_name"></span> to Another Rep</h4>
        </div>
        <div class="modal-body">
          <p>Please Select New Rep:</p>
					<select id="reassign_nid">
						<option value="">Select A Rep...</option>
						<?php
						$crq = "SELECT * FROM `users` WHERE `collections` = 'Yes' AND `inactive` != 'Yes'";
						$crg = mysqli_query($conn, $crq) or die($conn->error);
						while($crr = mysqli_fetch_array($crg)){
							echo '<option value="' . $crr['ID'] . '">' . $crr['fname'] . ' ' . $crr['lname'] . '</option>';
						}
						?>
					</select>
					<input type="hidden" id="reassign_mid" value="" />
					<br><br>
					<p id="reassign_error_msg" style="color:red;font-weight:bold;"></p>
        </div>
        <div class="modal-footer">
					<button type="button" class="btn btn-success" onclick="reassign_user();">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	
	
	<!-- Modal -->
  <div class="modal fade" id="addCollection" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="add_title">Add A Customer To Collections</h4>
        </div>
        <div class="modal-body">
					<form id="addCust" action="collections/add-customer.php" method="post" enctype="multipart/form-data" target="_blank">
					<table>
						<tr>
							<td>
          			<p>Full Name</p>
								<input type="text" class="form-control" id="nfull_name" name="nfull_name" />
							</td>
							<td>
								<p>Phone#</p>
								<input type="phone" class="form-control" id="nphone" name="nphone" />
							</td>
							<td>
								<p>Email</p>
								<input type="email" class="form-control" id="nemail" name="nemail" />
							</td>
						</tr>
						<tr>
							<td>
								<p>State</p>
								<select class="form-control" id="nstate" name="nstate">
									<option value="">Select State</option>
								<?php
									$ssq = "SELECT * FROM `states`";
									$ssg = mysqli_query($conn, $ssq) or die($conn->error);
									while($ssr = mysqli_fetch_array($ssg)){
										echo '<option value="' . $ssr['abrev'] . '">' . $ssr['state'] . '</option>';
									}
						?>
								</select>
							</td>
							<td>
								<p>Invoice#</p>
								<input type="text" class="form-control" id="ninvoice" name="ninvoice" />
							</td>
							<td>
								<p id="iamount">Amount To Collect</p>
								<div class="input-group">
  								<span class="input-group-addon" id="basic-addon1">$</span>
  								<input type="text" class="form-control" id="namount" name="namount" aria-describedby="basic-addon1">
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<p>Collection Category:</p>
								<select class="form-control" id="ncat" name="ncat">
									<option value="">Select Category</option>
									<option value="ck_ca_cc">ck_ca_cc</option>
									<option value="po">po</option>
									<option value="nsf_stop">nsf_stop</option>
									<option value="repair">repair</option>
									<option value="dealer_pmt">dealer_pmt</option>
									<option value="dealer_fin">dealer_fin</option>
									<option value="legal">legal</option>
									<option value="bli">bli</option>
									<option value="ezpay">ezpay</option>
									<option value="scoggin">scoggin</option>
									<option value="double_d">double_d</option>
									<option value="dallen_legacy">dallen_legacy</option>
									<option value="rto_national">rto_national</option>
								</select>
							</td>
							<td>
								<p>Assign A Rep:</p>
								<select class="form-control" id="nrep" name="nrep">
									<option value="">Select A Rep...</option>
									<?php
									$crq = "SELECT * FROM `users` WHERE `collections` = 'Yes' AND `inactive` != 'Yes'";
									$crg = mysqli_query($conn, $crq) or die($conn->error);
									while($crr = mysqli_fetch_array($crg)){
										echo '<option value="' . $crr['ID'] . '">' . $crr['fname'] . ' ' . $crr['lname'] . '</option>';
									}
									?>
								</select>
							</td>
							<td>
								<p>Current Debt Amount:</p>
								<div class="input-group">
  								<span class="input-group-addon" id="basic-addon1">$</span>
  								<input type="text" class="form-control" id="ndebt" name="ndebt" aria-describedby="basic-addon1" disabled>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<p>Upload An Attachment:<p>
								<span id="attach">
								<input type="file" class="form-control" name="fileToUpload" />
								</span>
							</td>
							<td>
								<p>Automation Plan</p>
								<select id="auto_frequency" name="auto_frequency" class="form-control">
									<option value="" selected>None</option>
									<option value="MONTHLY">Monthly</option>
									<option value="WEEKLY">Weekly</option>
									<option value="DAILY">Daily</option>
									<option value="HOURLY">Hourly</option>
								</select>
								<p><a href="#" style="color:blue;" onclick="play_vid();" data-toggle="modal" data-target="#auto_learn"><i class="fa fa-graduation-cap"></i> Learn More</a></p>
							</td>
						</tr>
					</table>
					<p> Initial Notes:</p>
					<textarea id="add_note" name="add_note" class="form-control" style="height:150px;"></textarea>
					<input type="hidden" id="mode" name="mode" />
					<input type="hidden" id="edit_id" name="edit_id" />
					<input type="hidden" id="orig_cat" name="ocat" />
					<p id="add_error_msg" style="color:red;font-weight:bold;"></p>
        </div>
        <div class="modal-footer">
					<!--<button type="button" class="btn btn-success" onclick="add_collections();">Submit</button>-->
					<button type="button" class="btn btn-success" onclick="add_collections();">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
				</form>
    </div>
  </div>
	<!-- End Modal -->
	
	
		 <!-- Modal -->
  <div class="modal fade" id="paymentModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Record A Payment For <span style="color:red;" id="payment_name"></span></h4>
        </div>
        <div class="modal-body">
					<table>
						<tr>
							<td>
          			<p>Please Select The Payment Method:</p>
								<select class="form-control" id="payment_method">
									<option value="">Select Payment Method</option>
									<option value="Cash">Cash</option>
									<option value="Check">Check</option>
									<option value="Credit Card">Credit Card</option>
									<option value="Electronic Payment">Electronic Payment</option>
								</select>
							</td>
							<td>
								<p>Enter Payment Amount</p>
								<div class="input-group">
  								<span class="input-group-addon" id="basic-addon1">$</span>
  								<input type="text" class="form-control" id="payment_amount" aria-describedby="basic-addon1">
								</div>
								<input type="hidden" id="payment_mid" />
							</td>
						</tr>
						<tr>
							<td>
								<p>Check#</p>
								<input type="text" id="check_num" class="form-control" />
							</td>
							<td>
								<p>CC Transaction Confirmation#</p>
								<input type="text" id="cc_conf" class="form-control" />
							</td>
						</tr>
					</table>
					<p>Electronic Payment Note:</p>
					<textarea id="edn" class="form-control"></textarea>
					<br>
					<p id="payment_error_msg" style="color:red;font-weight:bold;"></p>
        </div>
        <div class="modal-footer">
					<button type="button" class="btn btn-success" onclick="record_payment();">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	
	
		 <!-- Modal -->
  <div class="modal fade" id="auto_learn" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="pause_vid();" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Learn About The Automation Plan:</h4>
        </div>
        <div class="modal-body">
					<video id="auto-plan" height="100%" width="100%" controls>
						<source src="img/special/auto-communication.mp4" type="video/mp4">
						Your browser does not support the video tag.
					</video>
          <p>
						You can setup a customer on a Monthly, Weekly, Daily, or Hourly plan to have Market Force
						automatically send SMS text messages and emails directly to your clients!
					</p>
					
        </div>
        <div class="modal-footer">
					<!--<button type="button" class="btn btn-success" onclick="">Submit</button>-->
          <button type="button" class="btn btn-default" onclick="pause_vid();" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	
 <?php include 'footer.html'; ?>
	
	
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
	
</body>

</html>