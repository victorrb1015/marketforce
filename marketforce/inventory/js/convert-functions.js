function convert_coil(coil_id){
  var conf = confirm('Are you sure you want to convert this coil to a Raw Material?');
  if(conf === false){
    return;
  }
  var coil_length = prompt('Please enter the coil\'s length:');
  //Send info to PHP Script
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200) {
        var r = JSON.parse(this.responseText);
        if(r.response === 'GOOD'){
          toast_alert('Success!',r.message,'bottom-right','success');
          window.location.reload();
        }else{
          toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
        }
      }
  };
  xhttp.open("GET", "inventory/php/convert-coil.php?coil_id="+coil_id+"&coil_length="+coil_length, true);
  xhttp.send();
}


function convert_item(item_id){
  var conf = confirm('Are you sure you want to convert this item to a Raw Material?');
  if(conf === false){
    return;
  }
  //Send info to PHP Script
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200) {
        var r = JSON.parse(this.responseText);
        if(r.response === 'GOOD'){
          toast_alert('Success!',r.message,'bottom-right','success');
          window.location.reload();
        }else{
          toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
        }
      }
  };
  xhttp.open("GET", "inventory/php/convert-item.php?item_id="+item_id, true);
  xhttp.send();
}