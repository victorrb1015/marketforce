function new_item(type){
  document.getElementById('product_mode').value = "New";
  document.getElementById('product_bc').removeAttribute('readonly');
  document.getElementById('product_bc').value = '';
  document.getElementById('product_name').value = '';
  document.getElementById('product_info').value = '';
  if(type === 'Coil'){
    document.getElementById('product_units').value = 'LB';
    document.getElementById('product_units').setAttribute('readonly','readonly');
    document.getElementById('product_units').style.backgroundColor = '#212121';
  }else{
    document.getElementById('product_units').value = '';
    document.getElementById('product_units').removeAttribute('readonly');
    document.getElementById('product_units').style.backgroundColor = '#212121';
  }
  document.getElementById('product_cost').value = '';
  document.getElementById('product_price').value = '';
  document.getElementById('product_weight').value = '';
  document.getElementById('min_qty').value = 0;
  document.getElementById('length_yes').removeAttribute('checked');
  document.getElementById('length_no').removeAttribute('checked');
  document.getElementById('color_yes').removeAttribute('checked');
  document.getElementById('color_no').removeAttribute('checked');
  document.getElementById('color_list_choice').value = '';
  document.getElementById('color_list_box').style.display = 'none';
  document.getElementById('product_name').focus();
  document.getElementById('item_type').value = type;
}


function save_item(){
  var pbc = document.getElementById('product_bc').value;
  /*if(pbc === ''){
    alert('Please Enter The Products Barcode!');
    return;
  }*/
  pbc = urlEncode(pbc);
  var pname = document.getElementById('product_name').value;
  if(pname === ''){
    alert('Please Enter The Products Name!');
    return;
  }
  pname = urlEncode(pname);
  var pinfo = document.getElementById('product_info').value;
  if(pinfo === ''){
    alert('Please Enter The Products Info!');
    return;
  }
  pinfo = urlEncode(pinfo);
  var punits = document.getElementById('product_units').value;
  if(punits === ''){
    alert('Please Enter The Products Units!');
    return;
  }
  punits = urlEncode(punits);
  var pcost = document.getElementById('product_cost').value;
  if(pcost === '' && sessionPosition !== 'ICU'){
    alert('Please Enter The Products Cost!');
    return;
  }
  pcost = urlEncode(pcost);
  var pprice = document.getElementById('product_price').value;
  if(pprice === '' && sessionPosition !== 'ICU'){
    alert('Please Enter The Products Price!');
    return;
  }
  pprice = urlEncode(pprice);
  var pweight = document.getElementById('product_weight').value;
  
  pweight = urlEncode(pweight);
  var min_qty = document.getElementById('min_qty').value;
  if(min_qty === ''){
    alert('Please Enter The Minimum QTY!');
    return;
  }
  min_qty = urlEncode(min_qty);
  
  var ilength = document.querySelectorAll('input[name="length_choice"]:checked');
  if(ilength.length < 1){
    alert('Please Indicate Whether Length Is Required On The Invoice!');
    return;
  }
  var length = ilength[0].value;
  if(length === '' || length === 'undefined'){
    alert('Please Indicate Whether Length Is Required On The Invoice!');
    return;
  }
  var icolor = document.querySelectorAll('input[name="color_choice"]:checked');
  if(icolor.length < 1){
    alert('Please Indicate Whether Color Is Required On The Invoice!');
    return;
  }
  var color = icolor[0].value;
  if(color === '' || color === 'undefined'){
    alert('Please Indicate Whether Color Is Required On The Invoice!');
    return;
  }
  
  var color_list = document.getElementById('color_list_choice').value;
  if(color_list === '' && color === 'Yes'){
    alert('Please Select The Color List For This Product!');
    return;
  }
  
  var zone = document.getElementById('zone_list_choice').value;
  if(zone === ''){
    alert('Please Select the Zone for this Product!');
    return;
  }

  var m_lock_val = document.querySelectorAll('input[name="m_lock_choice"]:checked');
  if (m_lock_val[0] === undefined) {
    var m_lock = 0;
  }else{
    var m_lock = m_lock_val[0].value;
  }


  
  var mode = document.getElementById('product_mode').value;
  var pid = document.getElementById('product_id').value;
  var item_type = document.getElementById('item_type').value;
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      //window.location = 'orders.php?tab=products';
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","inventory/php/add-edit-item.php?pbc="+pbc+"&pname="+pname+"&pinfo="+pinfo+"&punits="+punits+"&pcost="+pcost+"&pprice="+pprice+"&pweight="+pweight+"&min_qty="+min_qty+"&length="+length+"&color="+color+"&color_list="+color_list+"&mode="+mode+"&id="+pid+"&item_type="+item_type+"&zone="+zone+"&m_lock="+m_lock,true);
  xmlhttp.send();
}


function edit_item(id){
  document.getElementById('product_mode').value = 'Edit';
  document.getElementById('product_id').value = id;
  document.getElementById('item_type').value = '';
  
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var q = JSON.parse(this.responseText);
      console.log(q);
    document.getElementById('product_bc').value = q.bc;
    document.getElementById('product_bc').setAttribute('readonly','readonly');
    document.getElementById('product_name').value = q.pname;
    document.getElementById('product_info').value = q.pinfo;
    document.getElementById('product_units').value = q.punits;
    document.getElementById('product_cost').value = q.pcost;
    document.getElementById('product_price').value = q.pprice;
    document.getElementById('product_weight').value = q.pweight;
    document.getElementById('min_qty').value = q.min_qty;
    document.getElementById('zone_list_choice').value = q.zone.toUpperCase();
      if(q.length_required === 'Yes'){
        document.getElementById('length_no').removeAttribute('checked');
        document.getElementById('length_yes').setAttribute('checked','checked');
      }
      if(q.length_required === 'No'){
        document.getElementById('length_no').setAttribute('checked','checked');
        document.getElementById('length_yes').removeAttribute('checked');
      }
      if(q.color_required === 'Yes'){
        document.getElementById('color_no').removeAttribute('checked');
        document.getElementById('color_yes').setAttribute('checked','checked');
        document.getElementById('color_list_box').style.display = 'inline';
        document.getElementById('color_list_choice').value = q.color_list;
      }
      if(q.m_lock === '0'){
        document.getElementById('m_lock_yes').removeAttribute('checked');
        document.getElementById('m_lock_no').setAttribute('checked','checked');
      }else{
        document.getElementById('m_lock_yes').setAttribute('checked','checked');
        document.getElementById('m_lock_no').removeAttribute('checked');
      }
      if(q.color_required === 'No'){
        document.getElementById('color_no').setAttribute('checked','checked');
        document.getElementById('color_yes').removeAttribute('checked');
        document.getElementById('color_list_box').style.display = 'none';
      }
    }
  }
  xmlhttp.open("GET","inventory/php/get-item-details.php?pid="+id,true);
  xmlhttp.send();
}


function remove_item(pid){
  var conf = confirm('Are you sure you want to remove this item?');
  if(conf === '' || conf === null || conf === false){
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			alert(this.responseText);
      //window.location = 'inventory.php?tab=products';
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","inventory/php/delete-item.php?pid="+pid,true);
  xmlhttp.send();
}


//inventory_items.qty
function edit_item_qty(pid,pqty){
  var reason = prompt('Why is this adjustment being made?');
  if(reason === false || reason === null || reason === ''){
    toast_alert('Invalid!','A reason must be entered in order to change inventory levels manually','bottom-right','error');
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById('qty_box_'+pid).innerHTML = r.new_total_qty;
			  toast_alert('Item Updated!',r.message,'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory/php/update-item-qoh.php?pid="+pid+"&pqty="+pqty+"&reason="+reason,true);
  xmlhttp.send();
}

//inventory_items.qty_in
function edit_item_qty_in(pid,pqty){
  var reason = prompt('Why is this adjustment being made?');
  if(reason === false || reason === null || reason === ''){
    toast_alert('Invalid!','A reason must be entered in order to change inventory levels manually','bottom-right','error');
    return;
  }
  console.log(pqty);
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      console.log(r);
      if(r.response === 'GOOD'){
        document.getElementById('qty_box_'+pid).innerHTML = r.new_total_qty;
        document.getElementById('zn_qty_box_'+pid).innerHTML = r.new_zn_qty;
			  toast_alert('Item Updated!',r.message,'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory/php/update-item-qoh_in.php?pid="+pid+"&pqty="+pqty+"&reason="+reason,true);
  xmlhttp.send();
}

//inventory_items.qty_nc
function edit_item_qty_nc(pid,pqty){
  var reason = prompt('Why is this adjustment being made?');
  if(reason === false || reason === null || reason === ''){
    toast_alert('Invalid!','A reason must be entered in order to change inventory levels manually','bottom-right','error');
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById('qty_box_'+pid).innerHTML = r.new_total_qty;
        document.getElementById('zn_qty_box_'+pid).innerHTML = r.new_zn_qty;
			  toast_alert('Item Updated!',r.message,'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory/php/update-item-qoh_nc.php?pid="+pid+"&pqty="+pqty+"&reason="+reason,true);
  xmlhttp.send();
}

//inventory_items.qty_ga
function edit_item_qty_ga(pid,pqty){
  var reason = prompt('Why is this adjustment being made?');
  if(reason === false || reason === null || reason === ''){
    toast_alert('Invalid!','A reason must be entered in order to change inventory levels manually','bottom-right','error');
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById('qty_box_'+pid).innerHTML = r.new_total_qty;
        document.getElementById('zn_qty_box_'+pid).innerHTML = r.new_zn_qty;
			  toast_alert('Item Updated!',r.message,'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory/php/update-item-qoh_ga.php?pid="+pid+"&pqty="+pqty+"&reason="+reason,true);
  xmlhttp.send();
}

//inventory_items.qty_ca
function edit_item_qty_ca(pid,pqty){
  var reason = prompt('Why is this adjustment being made?');
  if(reason === false || reason === null || reason === ''){
    toast_alert('Invalid!','A reason must be entered in order to change inventory levels manually','bottom-right','error');
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById('qty_box_'+pid).innerHTML = r.new_total_qty;
        document.getElementById('zn_qty_box_'+pid).innerHTML = r.new_zn_qty;
			  toast_alert('Item Updated!',r.message,'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory/php/update-item-qoh_ca.php?pid="+pid+"&pqty="+pqty+"&reason="+reason,true);
  xmlhttp.send();
}

//inventory_items.qty_tx
function edit_item_qty_tx(pid,pqty){
  var reason = prompt('Why is this adjustment being made?');
  if(reason === false || reason === null || reason === ''){
    toast_alert('Invalid!','A reason must be entered in order to change inventory levels manually','bottom-right','error');
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById('qty_box_'+pid).innerHTML = r.new_total_qty;
        document.getElementById('zn_qty_box_'+pid).innerHTML = r.new_zn_qty;
			  toast_alert('Item Updated!',r.message,'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory/php/update-item-qoh_tx.php?pid="+pid+"&pqty="+pqty+"&reason="+reason,true);
  xmlhttp.send();
}

//inventory_items.qty_mx
function edit_item_qty_mx(pid,pqty){
  var reason = prompt('Why is this adjustment being made?');
  if(reason === false || reason === null || reason === ''){
    toast_alert('Invalid!','A reason must be entered in order to change inventory levels manually','bottom-right','error');
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        document.getElementById('qty_box_'+pid).innerHTML = r.new_total_qty;
        document.getElementById('zn_qty_box_'+pid).innerHTML = r.new_zn_qty;
			  toast_alert('Item Updated!',r.message,'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory/php/update-item-qoh_mx.php?pid="+pid+"&pqty="+pqty+"&reason="+reason,true);
  xmlhttp.send();
}

$(".qty_box").keypress(function(e){ return e.which != 13; });




function color_list_option(){
  var icolor = document.querySelectorAll('input[name="color_choice"]:checked');
  var color_box = document.getElementById('color_list_box');
  if(icolor.length < 1){
    color_box.style.display = 'none';
  }
  var color = icolor[0].value;
  if(color === 'Yes'){
    color_box.style.display = 'inline';
  }else{
    color_box.style.display = 'none';
  }
}


function master_lock_list_option(){
  var m_lock = document.querySelectorAll('input[name="m_lock_choice"]:checked');
  var m_lock_box = document.getElementById('m_lock_choice');
  if(m_lock.length < 1){
    m_lock_box.style.display = 'none';
  }
  var color = m_lock[0].value;
  if(color === 'Yes'){
    m_lock_box.style.display = 'inline';
  }else{
    m_lock_box.style.display = 'none';
  }
}

function edit_item_qty_min_qty_v1(pid,pqty){
  var reason = prompt('Why is this adjustment being made?');
  if(reason === false || reason === null || reason === ''){
    toast_alert('Invalid!','A reason must be entered in order to change inventory levels manually','bottom-right','error');
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        //document.getElementById('qty_box_'+pid).innerHTML = r.new_total_qty;
        //document.getElementById('zn_qty_box_'+pid).innerHTML = r.new_zn_qty;
        toast_alert('Item Updated!',r.message,'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
    }
  }
  xmlhttp.open("GET","inventory/php/update-item-qoh-min.php?pid="+pid+"&pqty="+pqty+"&reason="+reason,true);
  xmlhttp.send();
}

function edit_item_qty_min_qty(pid){
  const num1 = prompt('Enter Minimum Threshold Value:');
  const min_group = parseInt(num1);
  const reason = prompt('Why is this adjustment being made?');
  if(reason === false || reason === null || reason === ''){
    toast_alert('Invalid!','A reason must be entered in order to change inventory levels manually','bottom-right','error');
    return;
  }
  console.log(min_group);
  $.ajax({
    type: "GET",
    url: "inventory/php/update-item-qoh-min.php",
    dataType: "json",
    data: {
      "pid": pid,
      "pqty": num1,
      "min_group": min_group
    },
    success: function(response){
      if(response['success'] === 'GOOD'){
        toast_alert('Item Updated!',response['message'],'bottom-right','success');
      }else{
        toast_alert('Something Went Wrong...','There was an error in your request','bottom-right','error');
      }
      console.log(response);
      window.reload();
    }
  });
}