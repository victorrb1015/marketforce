<div class="modal fade" role="dialog" tabindex="-1" id="printBarcodeModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h3 class="modal-title text-center">Print Barcode Labels</h3>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-6">
            <input type="text" class="form-control number" id="print_qty" name="print_qty" />
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="button" onclick="save_item();">Save</button>
      </div>
    </div>
  </div>
</div>