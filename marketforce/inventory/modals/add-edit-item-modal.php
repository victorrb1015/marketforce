<div class="modal fade" role="dialog" tabindex="-1" id="itemModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h3 class="modal-title text-center">Add/Edit Product</h3>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <p><strong>Product Barcode:</strong></p><input class="form-control" type="text" name="product_bc" id="product_bc" style="width: 100%;" maxlength="22">
            <br>
            <p><strong>Product Name:</strong></p><input class="form-control" type="text" name="product_name" id="product_name" style="width: 100%;">
            <br>
            <p><strong>Product Information:</strong></p><textarea class="form-control" name="product_info" placeholder="Enter Product Info Here..." id="product_info" style="width: 100%;height: 76px;"></textarea>
            <br>
            <p><strong>Is <mark>Length</mark> Required On Invoice?</strong></p>
            Yes <input type="radio" id="length_yes" name="length_choice" value="Yes"> No <input type="radio" id="length_no" name="length_choice" value="No">
            <br><br>
            <p><strong>Is <mark>Color</mark> Required On Invoice?</strong></p>
            Yes <input type="radio" id="color_yes" name="color_choice" onchange="color_list_option();" value="Yes"> No <input type="radio" id="color_no" name="color_choice" onchange="color_list_option();" value="No">
            <?if("832122" == $_SESSION['org_id']){
                echo '<br><br>
            <p><strong>Is <mark>Master Lock</mark> ON?</strong></p>
            Yes <input type="radio" id="m_lock_yes" name="m_lock_choice" onchange="master_lock_list_option();" value="1"> No <input type="radio" id="m_lock_no" name="m_lock_choice" onchange="master_lock_list_option();" value="0">';
            } ?>
          </div>
          <div class="col-md-6">
            <div class="input-group">
              <div class="input-group-addon" style="width: 122px;"><span>Units <small>(Sold Per)</small></span></div><input class="form-control" type="text" id="product_units" name="product_units">
              <div class="input-group-btn"><button class="btn btn-default" type="button" style="width:40px;" tabindex="-1"><i class="fa fa-cubes"></i></button></div>
            </div>
            <p>&nbsp;</p>
            <div class="input-group">
              <div class="input-group-addon" style="width: 122px;"><span>Cost <small>(Per Unit)</small></span></div><input class="form-control custom-usd" type="text" id="product_cost" name="product_cost">
              <div class="input-group-btn"><button class="btn btn-default" type="button" style="width: 40px;" tabindex="-1"><i class="fa fa-dollar"></i></button></div>
            </div>
            <p>&nbsp;</p>
            <div class="input-group">
              <div class="input-group-addon" style="width: 122px;"><span>Price <small>(Per Unit)</small></span></div><input class="form-control custom-usd" type="text" id="product_price" name="product_price">
              <div class="input-group-btn"><button class="btn btn-default" type="button" style="width: 40px;" tabindex="-1"><i class="fa fa-user"></i></button></div>
            </div>
            <p>&nbsp;</p>
            <div class="input-group">
              <div class="input-group-addon" style="width: 122px;"><span>Weight <small>(Per Unit)</small></span></div><input class="form-control" type="text" id="product_weight" name="product_weight">
              <div class="input-group-btn"><button class="btn btn-default" type="button" style="width: 40px;" tabindex="-1"><small style="padding-left: 0px !important; margin-left: -12px;">LBS</small></button></div>
            </div>
            <p>&nbsp;</p>
            <div class="input-group">
              <div class="input-group-addon" style="width: 122px;"><span>Min QTY</small></span></div><input class="form-control" type="text" id="min_qty" name="min_qty">
            </div> 
            <p>&nbsp;</p>
            <div id="zone_list_box">
              <select class="form-control" id="zone_list_choice">
                <option value="">Select Zone</option>';
              <?php
                $zlq = "SELECT * FROM `inventory_zones` WHERE `inactive` != 'Yes' ORDER BY `zone` ASC";
                $zlg = mysqli_query($conn, $zlq) or die($conn->error);
                while($zlr = mysqli_fetch_array($zlg)){
                  echo '<option value="' . $zlr['zone'] . '">' . strtoupper($zlr['name']) . ' </option>';
                }
               ?>
              </select>
            </div>
            <p>&nbsp;</p>
            <div id="color_list_box" style="display:none;">
              <select class="form-control" id="color_list_choice">
                                <option value="">Select Color List</option>
                                <?php
                                $clq = "SELECT * FROM `order_color_lists` WHERE `inactive` != 'Yes' ORDER BY `list` ASC";
                                $clg = mysqli_query($conn, $clq) or die($conn->error);
                                while($clr = mysqli_fetch_array($clg)){
                                  echo '<option value="' . $clr['list'] . '">' . strtoupper($clr['list_name']) . ' Color List</option>';
                                }
                                ?>
                </select>
            </div>
          </div>
          <input type="hidden" id="product_mode" name="product_mode" value="New" />
          <input type="hidden" id="product_id" name="product_id" value="" />
          <input type="hidden" id="item_type" name="item_type" value="" />
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="button" onclick="save_item();">Save</button>
      </div>
    </div>
  </div>
</div>