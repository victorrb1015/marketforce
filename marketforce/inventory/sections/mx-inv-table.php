    <!--Inventory Table-->
		        <div class="row">
		  <div class="col-md-12">
		    <div class="panel panel-primary card-view">
		      <div class="panel-heading">
		        <h1 class="panel-title">Inventory Display Table</h1>
		      </div>
		      <div class="panel-body table-responsive" style="height:auto;">
		        <table class="table table-bordered table-hover table-striped">
		          <thead>
		            <tr class="info">
		              <th style="font-size:18px;">Date</th>
		              <th style="font-size:18px;">Barcode</th>
                  <th style="font-size:18px;">Type</th>
		              <th style="font-size:18px;">Product Name</th>
		              <th style="font-size:18px;">Product Description</th>
		              <th style="font-size:18px;">Units</th>
		              <th style="font-size:18px;">Weight</th>
		              <th style="font-size:18px;">Cost</th>
		              <th style="font-size:18px;">Customer Price</th>
		              <th style="font-size:18px;">Location</th>
									<th style="font-size:18px;">Qty On-Hand</th>
		              <th style="font-size:18px;">Actions</th>
		            </tr>
		          </thead>
		          <tbody>
		         <?php
		            $mxq = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes'";
		            $mxg = mysqli_query($conn, $mxq) or die($conn->error);
		            while($mxr = mysqli_fetch_array($mxg)){
		            echo '<tr>
		              <td>' . date("m/d/y", strtotime($mxr['date'])) . '</td>
		              <td>' . $mxr['barcode'] . '</td>
                  <td>' . $mxr['item_type'] . '</td>
		              <td style="color: rgb(47,112,169);"><strong>' . $mxr['item_name'] . '</strong></td>
		              <td>' . $mxr['item_info'] . '</td>
		              <td>' . $mxr['item_units'] . '</td>
		              <td>' . $mxr['item_weight'] . ' LBS</td>
		              <td>' . $mxr['item_cost'] . '</td>
		              <td style="color: rgb(49,102,49);"><strong>' . $mxr['item_price'] . '</strong></td>
		              <td>' . $mxr['bin_location'] . '</td>
									<td class="qty_box" id="zn_qty_box_' . $mxr['ID'] . '" onblur="edit_item_qty(' . $mxr['ID'] . ',this.innerHTML);" ' . $contentEditable . '>' . $mxr['qty'] . '</td>
		              <td>
		                <div class="btn-group" role="group">
		                  <button class="btn btn-default" type="button" data-toggle="modal" data-target="#itemModal" onclick="edit_item(' . $mxr['ID'] . ');">Edit</button>
                      <button class="btn btn-warning btn-sm" style="color:#000;" type="button" onclick="print_barcodes(' . $pr['ID'] . ');">Print Barcode</button>
		                  <button class="btn btn-danger" type="button" onclick="remove_item(' . $mxr['ID'] . ');">Delete</button>
		                </div>
		              </td>
		            </tr>';
		            }
		         ?>
			          </tbody>
			        </table>
			      </div>
			    </div>
			  </div>
			</div>