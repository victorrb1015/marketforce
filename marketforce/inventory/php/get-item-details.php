<?php
include '../../php/connection.php';

//Load Variables...
$pid = $_GET['pid'];
$bc = $_REQUEST['bc'];

//Get Details...
if($_REQUEST['bc']){
  $q = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND `barcode` = '" . mysqli_real_escape_string($conn, $bc) . "'";
}else{
  $q = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $pid . "'";
}

$g = mysqli_query($conn, $q) or die($conn->error);

if(mysqli_num_rows($g) > 0){
  
  $r = mysqli_fetch_array($g);

  $x->response = 'GOOD';
  $x->ID = $r['ID'];
  $x->zone = $r['zone'];
  $x->itype = $r['item_type'];
  $x->bc = $r['barcode'];
  $x->pname = $r['item_name'];
  $x->pinfo = $r['item_info'];
  $x->punits = $r['item_units'];
  $x->pcost = $r['item_cost'];
  $x->pprice = $r['item_price'];
  $x->pweight = $r['item_weight'];
  $x->min_qty = $r['min_qty'];
  $x->length_required = $r['length_required'];
  $x->color_required = $r['color_required'];
  $x->color_list = $r['color_list'];
  $x->bin_location = $r['bin_location'];
  if("832122" == $_SESSION['org_id']){
      $x->m_lock = $r['m_lock'];
  }
  $x->inactive = $r['inactive'];
  
}else{
  $x->response = 'ERROR';
  $x->message = 'No item found with barcode: ' . $bc;
}
  

$response = json_encode($x);

echo $response;

?>