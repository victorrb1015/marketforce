<?php
include '../../php/connection.php';

//Load Variables...
$pid = mysqli_real_escape_string($conn, $_REQUEST['pid']);
$pqty = mysqli_real_escape_string($conn, $_REQUEST['pqty']);
$reason = mysqli_real_escape_string($conn, $_REQUEST['reason']);


$cqq = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $pid . "'";
$cqg = mysqli_query($conn, $cqq) or die($conn->error);
$cqr = mysqli_fetch_array($cqg);
$iname = $cqr['item_name'];
$cqty = $cqr['qty_tx'];
$nqty = $pqty;
$adjustment = $nqty - $cqty;
$ctqty = $cqr['qty'];
$x->new_total_qty = $ctqty + $adjustment;
$x->new_zn_qty = $nqty;

//Log Adjustment...
$aiq = "INSERT INTO `inventory_adjustments` 
        (
        `date`,
        `time`,
        `pid`,
        `zone`,
        `current_qty`,
        `new_qty`,
        `adjustment`,
        `type`,
        `reason`,
        `user_id`,
        `user_name`,
        `status`,
        `approval`,
        `inactive`
        )
        VALUES
        (
        CURRENT_DATE,
        CURRENT_TIME,
        '" . $pid . "',
        'TX',
        '" . $cqty . "',
        '" . $nqty . "',
        '" . $adjustment . "',
        'Manual',
        '" . $reason . "',
        '" . $_SESSION['user_id'] . "',
        '" . $_SESSION['full_name'] . "',
        'Pending',
        '',
        'No'
        )";
mysqli_query($conn, $aiq) or die($conn->error);


$uq = "UPDATE `inventory_items` SET 
		`qty_tx` = '" . $nqty . "',
    `qty` = (`qty` + " . $adjustment . "),
    `last_adjustment` = CURRENT_DATE,
    `last_adjustment_by` = '" . $_SESSION['user_id'] . "'
		WHERE `ID` = '" . $pid . "'";
mysqli_query($conn, $uq) or die($conn->error);

$x->response = 'GOOD';
$x->message = $iname . '\'s inventory has been adjusted ' . $adjustment . '.';

$response = json_encode($x);
echo $response;

?>