<?php
error_reporting(0);
include '../../php/connection.php';

//Load Variables...
$pid = mysqli_real_escape_string($conn, $_REQUEST['id']);
$pbc = mysqli_real_escape_string($conn, $_REQUEST['pbc']);//Barcode...
$pname = mysqli_real_escape_string($conn, $_REQUEST['pname']);
$pinfo = mysqli_real_escape_string($conn, $_REQUEST['pinfo']);
$units = mysqli_real_escape_string($conn, $_REQUEST['punits']);
$cost = mysqli_real_escape_string($conn, $_REQUEST['pcost']);
$price = mysqli_real_escape_string($conn, $_REQUEST['pprice']);
$pweight = mysqli_real_escape_string($conn, $_REQUEST['pweight']);
$min_qty = mysqli_real_escape_string($conn, $_REQUEST['min_qty']);
$length = mysqli_real_escape_string($conn, $_REQUEST['length']);
$color = mysqli_real_escape_string($conn, $_REQUEST['color']);
$color_list = mysqli_real_escape_string($conn, $_REQUEST['color_list']);
$item_type = $_REQUEST['item_type'];
$mode = mysqli_real_escape_string($conn, $_REQUEST['mode']);
$zone = mysqli_real_escape_string($conn, $_REQUEST['zone']);
$m_lock = mysqli_real_escape_string($conn, $_REQUEST['m_lock']);


//Add Product...
if($mode == 'New'){

    if("832122" == $_SESSION['org_id']){
        $aq = "INSERT INTO `inventory_items`
			(
			`date`,
			`time`,
			`zone`,
      `item_type`,
      `barcode`,
			`item_name`,
			`item_info`,
			`item_units`,
			`item_cost`,
			`item_price`,
      `item_weight`,
      `min_qty`,
      `length_required`,
      `color_required`,
      `color_list`,
			  `m_lock`,
			`inactive`
			)
			VALUES
			(
			CURRENT_DATE,
			CURRENT_TIME,
      '" . $zone . "',
      '" . $item_type . "',
      '" . $pbc . "',
			'" . $pname . "',
			'" . $pinfo . "',
			'" . $units . "',
			'" . $cost . "',
			'" . $price . "',
      '" . $pweight . "',
      '" . $min_qty . "',
      '" . $length . "',
      '" . $color . "',
      '" . $color_list . "',
      '" . $m_lock . "',
			'No'
			)";
    }else{
        $aq = "INSERT INTO `inventory_items`
			(
			`date`,
			`time`,
			`zone`,
      `item_type`,
      `barcode`,
			`item_name`,
			`item_info`,
			`item_units`,
			`item_cost`,
			`item_price`,
      `item_weight`,
      `min_qty`,
      `length_required`,
      `color_required`,
      `color_list`,
			`inactive`
			)
			VALUES
			(
			CURRENT_DATE,
			CURRENT_TIME,
      '" . $zone . "',
      '" . $item_type . "',
      '" . $pbc . "',
			'" . $pname . "',
			'" . $pinfo . "',
			'" . $units . "',
			'" . $cost . "',
			'" . $price . "',
      '" . $pweight . "',
      '" . $min_qty . "',
      '" . $length . "',
      '" . $color . "',
      '" . $color_list . "',
			'No'
			)";
    }


	mysqli_query($conn, $aq) or die($conn->error);
  $x->response = 'GOOD';
  $x->message = 'Item Added Successfully!';
}

//Edit Product...
if($mode == 'Edit'){
    if("832122" == $_SESSION['org_id']) {
        $uq = "UPDATE `inventory_items` SET 
      `barcode` = '" . $pbc . "',
			`item_name` = '" . $pname . "',
			`zone` = '" . $zone . "',
			`item_info` = '" . $pinfo . "',
			`item_units` = '" . $units . "',
			`item_cost` = '" . $cost . "',
			`item_price` = '" . $price . "',
      `item_weight` = '" . $pweight . "',
      `min_qty` = '" . $min_qty . "',
      `length_required` = '" . $length . "',
      `color_required` = '" . $color . "',
      `m_lock` = '" . $m_lock . "',
      `color_list` = '" . $color_list . "'
			WHERE `ID` = '" . $pid . "'";
    }else{
        $uq = "UPDATE `inventory_items` SET 
      `barcode` = '" . $pbc . "',
			`item_name` = '" . $pname . "',
			`zone` = '" . $zone . "',
			`item_info` = '" . $pinfo . "',
			`item_units` = '" . $units . "',
			`item_cost` = '" . $cost . "',
			`item_price` = '" . $price . "',
      `item_weight` = '" . $pweight . "',
      `min_qty` = '" . $min_qty . "',
      `length_required` = '" . $length . "',
      `color_required` = '" . $color . "',
      `color_list` = '" . $color_list . "'
			WHERE `ID` = '" . $pid . "'";
    }
			
	mysqli_query($conn, $uq) or die($conn->error);
  $x->response = 'GOOD';
  $x->message = 'Item Updated Successfully!';

}
$x->bc = $pbc;
$x->zone = $zone;
$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;

?>