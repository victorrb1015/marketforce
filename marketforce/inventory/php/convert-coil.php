<?php
header('Content-Type: application/json');
error_reporting(E_ALL);
include '../../php/connection.php';

//Load Variables...
$coil_id = $_REQUEST['coil_id'];
$coil_length = $_REQUEST['coil_length'];


#Main Functions...
$q = "SELECT * FROM `inventory_items` WHERE `ID` = '" . $coil_id . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
if(mysqli_num_rows($g) > 0){
  
  $r = mysqli_fetch_array($g);
  $zone = $r['zone'];
  $item_cost = number_format($r['item_cost'] / $coil_length,2);
  $item_price = number_format(($r['item_cost'] / $coil_length) / ($r['item_cost'] / $r['item_price']),2);
  $item_weight = number_format(($r['item_weight'] / $coil_length),2);
  
  //Update Coil in DB
  $uq = "UPDATE `inventory_items` SET 
          `item_type` = 'Raw',
          `item_units` = 'Foot',
          `item_cost` = '" . $item_cost . "',
          `item_price` = '" . $item_price . "',
          `item_weight` = '" . $item_weight . "',
          `qty` = '" . $coil_length . "',
          `qty_" . strtolower($zone) . "` = '" . $coil_length . "',
          `length_required` = 'Yes'
          WHERE
          `ID` = '" . $coil_id . "'";
  mysqli_query($conn, $uq) or die($conn->error);
  
  $x->response = 'GOOD';
  $x->message = 'Coil successfully converted to Raw Material.';
  
}else{
  $x->response = 'ERROR';
  $x->message = 'No Item found for Coil ID: ' . $coil_id;
}

//Setup Response Output...
$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;