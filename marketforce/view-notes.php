<?php
include 'security/session/session-settings.php';

if(!isset($_SESSION['in'])){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

		<!--JQuery CSS-->
    <link href="jquery/jquery-ui.css" rel="stylesheet">

	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
		.main li{
			float: left;
		}
		.main li li{
			float: none;
		}
		li{
			list-style-type: none;
		}
		img {
    image-orientation: from-image;
}
  </style>
  <script>
    
  function clr_res(id,s){
    //alert("test");
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			//alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://library.burtonsolution.tech/php/reserve.php?id="+id+"&s="+s,true);
  xmlhttp.send();
  }
    
 $(document).ready(function(){
  $("#dealer_select").change(function(){
    var id = document.getElementById('dealer_select').value;
		
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      document.getElementById("accordion").innerHTML=this.responseText;
			//alert(this.responseText);
			//alert(id);
      
    }
  }
  xmlhttp.open("GET","php/get-notes.php?id="+id,true);
  xmlhttp.send();
  });
 
 });
	
		
function filters(){
    var id = document.getElementById('dealer_select').value;
		var filter = document.querySelector('input[name="filter"]:checked').value;
		var uid = '<?php echo $_SESSION['user_id']; ?>';
		//alert('worked');
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      document.getElementById("accordion").innerHTML=this.responseText;
			//alert(this.responseText);
			//alert(id);
      
    }
  }
  xmlhttp.open("GET","php/get-notes.php?id="+id+"&filter="+filter+"&uid="+uid+"&lookup=yes",true);
  xmlhttp.send();
}
		
		
		<?php
		if(isset($_GET['id'])){
  echo 'var uid = "' . $_SESSION['user_id'] . '";';
        
	echo	'if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      document.getElementById("accordion").innerHTML=this.responseText;
			//alert(this.responseText);
			//alert(id);
      
    }
  }
  xmlhttp.open("GET","php/get-notes.php?id=' . $_GET['id'] . '&uid="+uid+"&lookup=yes",true);
  xmlhttp.send();';
		}
		
		?>
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> View Notes</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                          <li class="active">
                                <i class=" fa fa-fw fa-file"></i> View Notes
                          </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							<div class="row">
                <div class="top">
                  <h2>
                    Please Select A Dealer To View Notes On:
                  </h2>
                  <select id="dealer_select">
                    <option value="default">Select One...</option>
										<option value="cold">*COLD CALLS*</option>
                    <?php
                    $dq = "SELECT * FROM `dealers` WHERE `inactive` != 'Yes' ORDER BY `name` ASC";
                    $dg = mysqli_query($conn, $dq) or die($conn->error);
                    while($dr = mysqli_fetch_array($dg)){
                      echo '<option value="' . $dr['ID'] . '">' . $dr['name'] . ' - (' . $dr['city'] . ', ' . $dr['state'] . ')</option>';
                    
                    }
                    ?>
                  </select>
									
                  <h1 class="page-header"></h1>
                </div>
                <div id="accordion">
                  
                </div>
              </div>
							
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    
	
	  <!--JQuery Files-->
    <script src="jquery/external/jquery/jquery.js"></script>
    <script src="jquery/jquery-ui.js"></script>
    <script>

			
   $(document).ready(function(){
  $("#dealer_select").change(function(){
		
    $( "#accordion" ).accordion({
      collapsible: true
    });
  });
	 });
  
  </script>

 <?php include 'footer.html'; ?>
</body>

</html>