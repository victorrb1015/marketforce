<?php
include 'security/session/session-settings.php';
include 'php/connection.php';

if($_SESSION['in'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


  
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <script>
  function send_sug(){
		var mess = document.getElementById('mess').value;
		if(mess === ''){
			document.getElementById('response').innerHTML = 'Please Enter A Message...';
			return;
		}
		mess = urlEncode(mess);
    
    var rep_name = '<? echo $_SESSION['full_name']; ?>';
		var rep_id = '<? echo $_SESSION['user_id']; ?>';
    
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      //alert(this.responseText);
      document.getElementById('response').innerHTML = this.responseText;
			document.getElementById('mess').value='';
    }
  }
  xmlhttp.open("GET","php/suggest.php?mess="+mess+"&rep_name="+rep_name+"&rep_id="+rep_id,true);
  xmlhttp.send();
  }
		
		
	function update_char(){
		var curr = document.getElementById('mess').value.length;
		//alert(curr);
		var left = 5000 - curr;
		document.getElementById('char_limit').innerHTML = left;
	}
		
		function urlEncode(url){
		url = url.replace(/&/g, '%26'); 
		url = url.replace(/#/g, '%23');
		url = url.replace(/</g, '%3C');
		return url;
	}
  </script>
  
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> Suggestions</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  Dashboard
                            </li>
                            <li class="active">
                                <i class="fa fa-lightbulb-o"></i> Suggestions
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

    <!-- This is where the content for the page goes! -->
              <div class="row">
              <div class="col-lg-12" >
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-lightbulb-o fa-fw"></i> Send A Suggestion</h3>
                            </div>
                            <div class="panel-body" style="text-align: center;">
                              <h3>
                                Please Enter Your Suggestion Here:
                              </h3>
                              <textarea id="mess" maxlength="5000" onkeyup="update_char();" style="height: 150px; width: 800px;"></textarea>
															<br>Character Limit: <span id="char_limit">5000</span>
                              <br><br>
                              <button type="button" onclick="send_sug();">
                                Submit
                              </button>
															<br><br>
															<div id="response" style="text-align:center;color:red;font-weight:bold;"></div>
                              
                            </div>
                        </div>
                    </div>
                    </div>
                <!-- /.row -->  
              
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
 <?php include 'footer.html'; ?>
</body>

</html>
