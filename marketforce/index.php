<?php
include 'security/session/session-settings.php';



if(!isset($_SESSION['in'])){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="https://marketforce.allamericanmbs.com/?logout=true";
				}, 2000);
				</script>';
	return;
}

if($_GET['openmap'] == 'yes'){
	echo '<script>
	window.open("map2.php","_blank");
	alert("The Map was opened in the background to help aid in loading times");
	//window.blur();
	//window.focus();
	</script>';
}

if($_SESSION['position'] == 'Inventory Consignment User'){
  echo '<script>';
  echo 'window.location = "inventory.php";';
  echo '</script>';
}

include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>MarketForce | All Steel</title>
    <?php include 'global/sections/head.php'; ?>

    <?php include 'dashboard/js/script.php'; ?>

    <style>
		   .badge:hover{
      cursor: pointer;
    }
		th, td{
			padding: 10px;
		}
		#dl, dc{
			height: 430px;
		}
		.panel-body{
			height: 382px;
		}
		#scroll{
			overflow: scroll;
		}
		::-webkit-scrollbar {
    width: 0px;  /* remove scrollbar space */
    background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
		
		
		.packet-table td, th{
			border: 1px solid black;
		}
		.packet-table{
			margin:auto;
		}
	</style>
  
  <?php include 'dashboard/js/main-functions.php'; ?>
</head>

<body>
	<!-- Preloader -->
	<?php include 'global/sections/preloader.php'; ?>
	<!-- /Preloader -->
    <div class="wrapper theme-4-active pimary-color-red">

    	<!--Navigation-->
    	<?php 
		
        if($_SESSION['position'] == 'Shop Employee'){
          include 'global/sections/shop-nav.php';
        }else{
          include 'global/sections/nav.php';
        }
       ?>
		
		
        <!-- Main Content -->
		<div class="page-wrapper"><!--Includes Footer-->

            <div class="container-fluid pt-25"><!--Main Content Here-->
				
				<!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header" style="margin-top:10px;">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="<?php if('329698' == $_SESSION['org_id']){
                              echo  '//marketforceapp.com/assets/img/brands/Logo-blanco.png';
                          }else{
                              echo $_SESSION['org_logo_url'];
                          } ?>" style="width:10%;" ><small> Dashboard</small>
                        </h1>
                        <!--<ol class="breadcrumb">
                            <li class="active">
                                <i class="fas fa-tachometer-alt"></i> Dashboard
                            </li>
                        </ol>-->
                    </div>
                </div>

                <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable alert-style-1">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="zmdi zmdi-info-outline"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>



              <?php
							echo '<div class="row">
                      <div class="col-lg-12">';
											
						if($_GET['t'] != 'rt'){
      						 echo '<div class="panel panel-danger card-view">
													<div class="panel-heading">
														<h3 class="panel-title">
															My Tasks &nbsp;&nbsp;
															<a href="index.php?t=rt">
															<button type="button" class="btn btn-info">View Requested Tasks</button>
															</a>
															<button type="button" data-toggle="modal" data-target="#newTask" class="btn btn-success">New Task</button>
														</h3>
													</div>
												<div class="panel-body" id="scroll">
													<div class="table-responsive">
														<table class="table table-bordered table-hover table-striped packet-table">
															<thead>
																<tr>
																	<th>Date</th>
																	<th>Tasked By</th>
																	<th>Task</th>
																	<th>Deadline</th>
																	<th>Communication</th>
																	<th>Actions</th>
																</tr>
															</thead>
															<tbody>';
															
							$mtq = "SELECT * FROM `tasks` WHERE `inactive` != 'Yes' AND `status` != 'Completed' AND `to_rep_id` = '" . $_SESSION['user_id'] . "' ORDER BY `date` ASC";
							$mtg = mysqli_query($conn, $mtq) or die($conn->error);
							while($mtr = mysqli_fetch_array($mtg)){
								if($mtr['deadline'] != '' && $mtr['deadline'] != '0000-00-00 00:00:00'){
									$dl = date("m/d/y", strtotime($mtr['deadline']));
								}else{
									$dl = 'N/A';
								}
								echo '<tr>
												<td>' . date("m/d/y", strtotime($mtr['date'])) . '</td>
												<td>' . $mtr['from_rep_name'] . '</td>
												<td style="min-width:400px;">' . $mtr['task'] . '</td>
												<td>' . $dl . '</td>
												<td style="min-width:300px;">
												<div style="max-height:150px;overflow:scroll;">';
								$tnq = "SELECT * FROM `task_notes` WHERE `task_id` = '" . $mtr['ID'] . "' ORDER BY `date` DESC";
								$tng = mysqli_query($conn, $tnq) or die($conn->error);
								while($tnr = mysqli_fetch_array($tng)){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-comments-o"></i> ' . $tnr['rep_name'] . ' ' . date("m/d/y h:i A",strtotime($tnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . mysqli_real_escape_string($conn,$tnr['note']) . '
													</div>';
								}
									echo '</div>
												</td>
												<td>
													<button type="button" title="Send A Message" class="btn btn-warning" onclick="load_modal(' . $mtr['ID'] . ',\'\',\'Task Note\');" data-toggle="modal" data-target="#taskNote"><i class="fa fa-comments"></i></button>
													<button type="button" title="Mark As Completed" class="btn btn-success" onclick="taskDone(' . $mtr['ID'] . ');"><i class="fa fa-check-square-o"></i></button>
												</td>
											</tr>';
							}
							
												echo '</tbody>
														</table>
													</div>
												</div>
										</div>';
						}else{
							echo '<div class="panel panel-danger card-view">
													<div class="panel-heading">
														<h3 class="panel-title">
															Requested Tasks &nbsp;&nbsp;
															<a href="index.php">
															<button type="button" class="btn btn-info">View My Tasks</button>
															</a>
															<button type="button" data-toggle="modal" data-target="#newTask" class="btn btn-success">New Task</button>
														</h3>
													</div>
												<div class="panel-body" id="scroll">
													<div class="table-responsive">
														<table class="table table-bordered table-hover table-striped packet-table">
															<thead>
																<tr>
																	<th>Date</th>
																	<th>Tasked To</th>
																	<th>Task</th>
																	<th>Deadline</th>
																	<th>Communication</th>
																	<th>Actions</th>
																</tr>
															</thead>
															<tbody>';
															
							$mtq = "SELECT * FROM `tasks` WHERE `inactive` != 'Yes' AND `status` != 'Completed' AND `from_rep_id` = '" . $_SESSION['user_id'] . "' AND `to_rep_id` != '" . $_SESSION['user_id'] . "' ORDER BY `date` ASC";
							$mtg = mysqli_query($conn, $mtq) or die($conn->error);
							while($mtr = mysqli_fetch_array($mtg)){
								echo '<tr>
												<td>' . date("m/d/y", strtotime($mtr['date'])) . '</td>
												<td>' . $mtr['to_rep_name'] . '</td>
												<td style="min-width:400px;">' . $mtr['task'] . '</td>
												<td>' . date("m/d/y", strtotime($mtr['deadline'])) . '</td>
												<td style="min-width:300px;">
												<div style="max-height:150px;overflow:scroll;">';
								$tnq = "SELECT * FROM `task_notes` WHERE `task_id` = '" . $mtr['ID'] . "' ORDER BY `date` DESC";
								$tng = mysqli_query($conn, $tnq) or die($conn->error);
								while($tnr = mysqli_fetch_array($tng)){
											echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-comments-o"></i> ' . $tnr['rep_name'] . ' ' . date("m/d/y h:i A",strtotime($tnr['date'])) . ' 
													</span></h4>
														<div class="well well-sm">
														' . mysqli_real_escape_string($conn,$tnr['note']) . '
													</div>';
								}
									echo '</div>
												</td>
												<td>
													<button type="button" title="Send A Message" class="btn btn-warning" onclick="load_modal(' . $mtr['ID'] . ',\'\',\'Task Note\');" data-toggle="modal" data-target="#taskNote"><i class="fa fa-comments"></i></button>
													<!--
													<button type="button" title="Mark As Completed" class="btn btn-success" onclick="taskDone(' . $mtr['ID'] . ');"><i class="fa fa-check-square-o"></i></button>
													-->
												</td>
											</tr>';
							}
							
												echo '</tbody>
														</table>
													</div>
												</div>
											</div>';
						}
                      
            echo '</div>
								</div>';
              
              
                        //Projects section only visible for Grupo Comunicado
              if($_SESSION['org_id'] == '123456'){
  						echo'
                <div class="col-lg-12">
									<div class="panel panel-info">
										<div class="panel-heading">
											<h3 class="panel-title">
															Open Projects &nbsp;&nbsp;
															<!--<button id="projectsModalButton" type="button" data-toggle="modal" data-target="" class="btn btn-success">New Project Task</button>-->
                              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newProjectTaskModal">New Project Task</button>
											</h3>
										</div>
										<div class="panel-body" id="scroll">
											<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
												<tr>
													<th>Project Name</th>
													<th>Client Name</th>
													<th>Rep Name</th>
													<th>Date</th>
													<th>Task</th>
													<th>Start</th>
													<th>Pause</th>
                          <th>Interval A</th>
													<th>Resume</th>
 													<th>End</th>
                          <th>Interval B</th>
                          <th>Total Task Hours</th>
                         </tr>
												</thead>
												<tbody>';
                          
                $pdquery = "SELECT * FROM `projects` WHERE `inactive` != 'Yes' AND `from_rep_id` = '" . $_SESSION['user_id'] . "' AND `status` != 'Completed' AND `status` != 'Cancelled'";
								$pdgaux = mysqli_query($conn, $pdquery) or die($conn->error);
								while($pdresult = mysqli_fetch_array($pdgaux)){
									$ornquery = "SELECT * FROM `users` WHERE `ID` = '" . $pdresult['from_rep_id'] . "'";
							  	$orngaux = mysqli_query($conn, $ornquery) or die($conn->error);
								  $ornresult = mysqli_fetch_array($orngaux);
                  echo '
								<tr>
												<td>' . $pdresult['project'] . '</td>
												<td>' . $pdresult['client_name'] . '</td>
												<td>' . $ornresult['fname'] . ' ' . $ornresult['lname'] . '</td>
												<td>' . date("m/d/Y", strtotime($pdresult['date'])) . '</td>
												<td>' . $pdresult['task_name'] . '</td>
						  					<td>' . date("H:i:s", strtotime($pdresult['start'])) . '</td>
												<td>' . date("H:i:s", strtotime($pdresult['pause'])) . '</td>
                        <td>' . $pdresult['interval_a'] . '</td>
                        <td>' . date("H:i:s", strtotime($pdresult['resume'])) . '</td>
                        <td>' . date("H:i:s", strtotime($pdresult['end'])) . '</td>
                        <td>' . $pdresult['interval_b'] . '</td>
                        <td>' . $pdresult['total_hours_task'] . '</td>'; 
                                     
                 echo '
                     </tbody>
										</table>
										</div>
										</div>
									</div>
								</div>';
               }
              }
                else{
                  
                }


					if($_SESSION['doc_approval'] == 'Yes'){
							echo '<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-info card-view">
										<div class="panel-heading">
											<h3 class="panel-title">
												Pending Forms
											</h3>
										</div>
										<div class="panel-body" id="scroll">
											<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
												<tr>
													<th>';
                        switch ($_SESSION['org_id']) {
                            case "832122":
                                echo "Customer ";
                                break;
                            case "123456":
                                echo "Client ";
                                break;
                            default:
                                echo 'Dealer ';
                        }
                echo ' Name</th>
													<th>Document Type</th>
													<th>OSR</th>
													<th>Date</th>
													<th>Time</th>
													<th>Status</th>
													<th>Notes</th>
													<th>Actions</th>
												</tr>
												</thead>
												<tbody>';
								
								$tdq = "SELECT * FROM `new_packets` WHERE `inactive` != 'Yes' AND `status` = 'Pending' OR `status` = 'Need Info' OR `cl_approved` = 'Yes'";
								$tdg = mysqli_query($conn, $tdq) or die($conn->error);
								while($tdr = mysqli_fetch_array($tdg)){
									$ornq = "SELECT * FROM `users` WHERE `ID` = '" . $tdr['rep'] . "'";
									$orng = mysqli_query($conn, $ornq) or die($conn->error);
									$ornr = mysqli_fetch_array($orng);
									if($tdr['cl_submitted_by_id'] != '' && $tdr['cl_approved'] == ''){
										$rbg = 'background:green;';
										$fc = 'color:white;';
									}elseif($tdr['cl_approved'] == 'Yes'){
										$rbg = 'background:yellow;';
										$fc = 'color:black;';
									}else{
										$rbg = '';
										$fc = '';
									}
									echo '<tr style="' . $rbg . ' ' . $fc . '">
												<td>' . $tdr['name'] . '</td>
												<td>' . $tdr['form_type'] . '</td>
												<td>' . $ornr['fname'] . ' ' . $ornr['lname'] . '</td>
												<td>' . date("m/d/Y", strtotime($tdr['date'])) . '</td>
												<td>' . date("h:iA", strtotime($tdr['time'])) . '</td>
												<td>' . $tdr['status'] . '</td>
												<td>' . $tdr['note'] . '</td>';
									
										echo '<td>';
										
								if($tdr['cl_submitted_by_id'] == ''){
									echo '<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#packetChecklist" onclick="fetch_checklist(' . $tdr['ID'] . ');">Checklist</button>';
								}		
								if($_SESSION['ostr_supervisor'] == 'Yes' && $tdr['cl_submitted_by_id'] != '' && $tdr['cl_approved'] != 'Yes'){
										echo '<a onclick="new_packet(' . $tdr['ID'] . ',1);">
												<button type="button" class="btn btn-success">Accept</button>
											  </a>';

										echo '<a onclick="new_packet(' . $tdr['ID'] . ',2);">
												<button type="button" class="btn btn-warning">Need-Info</button>
											  </a>';
											}
									if($tdr['cl_approved'] == 'Yes'){
										echo '<a onclick="new_packet(' . $tdr['ID'] . ',3);">
												<button type="button" class="btn btn-success"><i class="fa fa-print"></i> Print</button>
											  </a>';
									}
									
									switch ($tdr['form_type']){
										case "New Dealer Packet":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/print-packet.php?id=' . $tdr['dealer_id'] . '&ps=1" target="_blank">';
											break;
										case "Display Addon Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-display-addon-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										case "Display Relocation Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-display-relocation-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										case "Display Removal Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-display-removal-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										case "Dealer Closing Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-dealer-closing-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										case "Ownership Change Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-ownership-change-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										default:
											echo '<a href="">';
									}
									
									echo '<button type="button" class="btn btn-primary"><i class="fa fa-eye"></i> View</button>
												</a>
												
												<button type="button" class="btn btn-danger" onclick="cancel_installation(' . $tdr['ID'] . ');">Cancel</button>
												</td>
												</tr>';
									
								}
												
							echo '</tbody>
										</table>
										</div>
										</div>
									</div>
								</div>
							</div><!--End Row-->';
								
							}else{
								
							echo '<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-info card-view">
										<div class="panel-heading">
											<h3 class="panel-title">
												Pending Forms
											</h3>
										</div>
										<div class="panel-body" id="scroll">
										<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
												<tr>
													<th>'; switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }
                           echo ' Name</th>
													<th>Document Type</th>
													<th>OSR</th>
													<th>Date</th>
													<th>Time</th>
													<th>Status</th>
													<th>Notes</th>
													<th>Actions</th>
												</tr>
												</thead>
												<tbody>';
								
								$tdq = "SELECT * FROM `new_packets` WHERE `inactive` != 'Yes' AND `rep` = '" . $_SESSION['user_id'] . "' AND `status` != 'Completed' AND `status` != 'Cancelled'";
								$tdg = mysqli_query($conn, $tdq) or die($conn->error);
								while($tdr = mysqli_fetch_array($tdg)){
									$ornq = "SELECT * FROM `users` WHERE `ID` = '" . $tdr['rep'] . "'";
									$orng = mysqli_query($conn, $ornq) or die($conn->error);
									$ornr = mysqli_fetch_array($orng);
									
									echo '<tr>
												<td>' . $tdr['name'] . '</td>
												<td>' . $tdr['form_type'] . '</td>
												<td>' . $ornr['fname'] . ' ' . $ornr['lname'] . '</td>
												<td>' . date("m/d/Y", strtotime($tdr['date'])) . '</td>
												<td>' . date("h:iA", strtotime($tdr['time'])) . '</td>
												<td>' . $tdr['status'] . '</td>
												<td>' . $tdr['note'] . '</td>
												<td>';
									
									switch ($tdr['form_type']){
										case "New Dealer Packet":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/print-packet.php?id=' . $tdr['dealer_id'] . '&ps=1" target="_blank">';
											break;
										case "Display Addon Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-display-addon-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										case "Display Relocation Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-display-relocation-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										case "Display Removal Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-display-removal-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										case "Dealer Closing Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-dealer-closing-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										case "Ownership Change Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-ownership-change-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										default:
											echo '<a href="">';
									}
									
									echo '<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>';
									
										echo '</tr>';
									
								}
												
							echo '</tbody>
										</table>
										</div>
										</div>
									</div>
								</div>
							</div><!--End Row-->';
							}
							
				if($_SESSION['doc_approval'] != 'Yes'){
							echo '<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-info card-view">
										<div class="panel-heading">
											<h3 class="panel-title">
												Pending Forms
											</h3>
										</div>
										<div class="panel-body" id="scroll">
										<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
												<tr>
													<th>'; switch($_SESSION['org_id']) {
                        case "832122":
                            echo "Customer ";
                            break;
                        case "123456":
                            echo "Client ";
                            break;
                        default:
                            echo 'Dealer ';
                    }
                    echo ' Name</th>
													<th>Document Type</th>
													<th>OSR</th>
													<th>Date</th>
													<th>Time</th>
													<th>Status</th>
													<th>Notes</th>
													<th>Actions</th>
												</tr>
												</thead>
												<tbody>';
								
								$tdq = "SELECT * FROM `new_packets` WHERE `rep` = '" . $_SESSION['user_id'] . "' AND `status` != 'Completed'";
								$tdg = mysqli_query($conn, $tdq) or die($conn->error);
								while($tdr = mysqli_fetch_array($tdg)){
									$ornq = "SELECT * FROM `users` WHERE `ID` = '" . $tdr['rep'] . "'";
									$orng = mysqli_query($conn, $ornq) or die($conn->error);
									$ornr = mysqli_fetch_array($orng);
									
									echo '<tr>
												<td>' . $tdr['name'] . '</td>
												<td>' . $tdr['form_type'] . '</td>
												<td>' . $ornr['fname'] . ' ' . $ornr['lname'] . '</td>
												<td>' . date("m/d/Y", strtotime($tdr['date'])) . '</td>
												<td>' . date("h:iA", strtotime($tdr['time'])) . '</td>
												<td>' . $tdr['status'] . '</td>
												<td>' . $tdr['note'] . '</td>
												<td>';
									
									switch ($tdr['form_type']){
										case "New Dealer Packet":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/print-packet.php?id=' . $tdr['dealer_id'] . '&ps=1" target="_blank">';
											break;
										case "Display Addon Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-display-addon-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										case "Display Relocation Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-display-relocation-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										case "Display Removal Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-display-removal-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										case "Dealer Closing Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-dealer-closing-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										case "Ownership Change Form":
											echo '<a href="https://marketforceapp.com/marketforce/forms/new-dealer-form/printable-ownership-change-form.php?id=' . $tdr['dealer_id'] . '" target="_blank">';
											break;
										default:
											echo '<a href="">';
									}
									
									echo '<span style="padding:5px;background:blue;font-weight:bold;border-radius:25px;white-space:nowrap;"><i class="fa fa-eye"></i> View</span>
												</a>
												</td>';
									
										echo '</tr>';
									
								}
												
							echo '</tbody>
										</table>
										</div>
										</div>
									</div>
								</div>
							</div><!--End Row-->';
							}
              
              
              
              
           if($_SESSION['doc_approval'] == 'Yes'){
							echo '<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-danger card-view">
										<div class="panel-heading">
											<h3 class="panel-title">
												Pending Display Installations
											</h3>
										</div>
										<div class="panel-body" id="scroll">
										<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped packet-table">
											<thead>
												<tr>
													<th>'; switch($_SESSION['org_id']) {
                   case "832122":
                       echo "Customer ";
                       break;
                   case "123456":
                       echo "Client ";
                       break;
                   default:
                       echo 'Dealer ';
               }
               echo ' Name</th>
													<th>Document Type</th>
													<th>OSR</th>
													<th>Date</th>
													<th>Time</th>
													<th>Status</th>
													<th>Notes</th>
													<th>Actions</th>
												</tr>
												</thead>
												<tbody>';
								
								$tdq = "SELECT * FROM `new_packets` WHERE `inactive` != 'Yes' AND `status` = 'Pending Installation'";
								$tdg = mysqli_query($conn, $tdq) or die($conn->error);
								while($tdr = mysqli_fetch_array($tdg)){
									$ornq = "SELECT * FROM `users` WHERE `ID` = '" . $tdr['rep'] . "'";
									$orng = mysqli_query($conn, $ornq) or die($conn->error);
									$ornr = mysqli_fetch_array($orng);
									
									echo '<tr>
												<td>' . $tdr['name'] . '</td>
												<td>' . $tdr['form_type'] . '</td>
												<td>' . $ornr['fname'] . ' ' . $ornr['lname'] . '</td>
												<td>' . date("m/d/Y", strtotime($tdr['date'])) . '</td>
												<td>' . date("h:iA", strtotime($tdr['time'])) . '</td>
												<td>' . $tdr['status'] . '</td>
												<td>' . $tdr['note'] . '</td>
												<td style="white-space:nowrap;">
                          <button type="button" class="btn btn-success" onclick="mark_installed(' . $tdr['ID'] . ');">Installed</button> <button type="button" class="btn btn-danger" onclick="cancel_installation(' . $tdr['ID'] . ');">Cancel</button>
                        </td>';
									
										echo '</tr>';
									
								}
												
							echo '</tbody>
										</table>
										</div>
										</div>
									</div>
								</div>
							</div><!--End Row-->';
							}
							?>
							
							
							
							<div class="row">
								<div class="col-lg-6" id="dl">
									<div class="panel panel-danger card-view">
										<div class="panel-heading">
											<h3 class="panel-title">
                                                <?php switch($_SESSION['org_id']) {
                                                    case "832122":
                                                        echo "Customer ";
                                                        break;
                                                    case "123456":
                                                        echo "Client ";
                                                        break;
                                                    default:
                                                        echo 'Dealer ';
                                                }?> With Overdue Visits (180+ Days)
											</h3>
										</div>
										<div class="panel-body" id="scroll">
											<table>
												<thead>
												<tr>
													<th><?php switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }?></th>
													<th>State</th>
													<th>Last Visit</th>
												</tr>
												</thead>
												<tbody>
													
							<?php
								$dq = "SELECT * FROM `dealers` WHERE `inactive` != 'Yes'";
								$dg = mysqli_query($conn, $dq) or die($conn->error);
								$num_not_visited = 0;//Number of dealers not visited.
								$tot_dealers = 0;//Number of active dealers in Market Force
								while($dr = mysqli_fetch_array($dg)){
									$lvq = "SELECT * FROM `visit_notes` WHERE `dealer_id` = '" . $dr['ID'] . "' AND `vtype` != 'Internal Note' ORDER BY `ID` DESC LIMIT 1";
									$lvg = mysqli_query($conn, $lvq) or die($conn->error);
									$lvr = mysqli_fetch_array($lvg);
									if(mysqli_num_rows($lvg) != 0){
									$date = $lvr['date'];
									$date = strtotime($date);
									$date1 = strtotime("+180 day", $date);
									$cdate = strtotime("now");
									$length = ($cdate - $date);
									$datediff = floor($length / (60 * 60 * 24));
									//if($cdate > $date1){
										/*echo '<tr>
														<td>' . $date1 . '</td>
														</tr>';*/
										if(intval($datediff) > 180){
										echo '<tr>
														<td>' . $dr['name'] . '</td>
														<td>' . $dr['state'] . '</td>
														<td>' . date("m/d/Y", $date) . '</td>
														<td><span class="label label-danger">' . $datediff . ' days ago</span></td>
													</tr>';
										}
										$tot_dealers++;
									}else{$num_not_visited++; $tot_dealers++;}
								}
													echo '<tr>
																	<td><u>';switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customer ";
                                    break;
                                case "123456":
                                    echo "Client ";
                                    break;
                                default:
                                    echo 'Dealer ';
                            }
                          echo '  Not Visited Yet:</u></td>
																	<td><span style="color: red;"><strong>' . $num_not_visited . '</strong></span></td>
																</tr>
																<tr>
																	<td><u>Total Active '; switch($_SESSION['org_id']) {
                                case "832122":
                                    echo "Customer ";
                                    break;
                                case "123456":
                                    echo "Client ";
                                    break;
                                default:
                                    echo 'Dealer ';
                            }
                          echo ' :</u></td>
																	<td><span style="color: blue;"><strong>' . $tot_dealers . '</strong></span></td>
																</tr>';
													?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							<!--</div>
							
							<div class="row">-->
								
								
								 <div class="col-lg-6">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-info fa-fw"></i> <?php switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }?> Accounts (This Month&apos;s Total)</h3>
                            </div>
                            <div class="panel-body">
                                <div id="donut_graph"></div>
                                <div class="text-right">
                                    <!--<a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>-->
                                </div>
                            </div>
                        </div>
                    </div>
								
								
				<?php
					if($_SESSION['manager'] == 'Yes'){
            
           
            
						/*$vntq = "SELECT * FROM `visit_notes` WHERE YEARWEEK(`date`,1) = YEARWEEK(CURRENT_DATE(),1)";
						$vntg = mysqli_query($conn, $vntq) or die($conn->error);
						$vnt2 = mysqli_num_rows($vntg) / 5;
						echo '<div class="col-lg-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Team Survey Ratings  |  <span style="color:black;">Total Average Visits Per Day: </span><span style="color:blue;">' . $vnt2 . '</span></h3>
                            </div>
                            <div class="panel-body">
                                <div id="all-chart"></div>
                                <!--<div class="text-right">
                                    <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                                </div>-->
                            </div>
                        </div>
                    </div>';*/
						
					$drlq = "SELECT * FROM `users` WHERE `position` != 'Office Employee' AND `position` != 'Shop Employee' AND `position` != 'Developer' AND `position` != 'Administrator' AND `inactive` != 'Yes' ORDER BY `fname` ASC";
					$drlg = mysqli_query($conn, $drlq) or die($conn->error);
					while($drlr = mysqli_fetch_array($drlg)){
						
								/*echo '<div class="col-lg-6" id="dc">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> ' . $drlr['fname'] . ' ' . $drlr['lname'] . ' Ratings</h3>
                            </div>
                            <div class="panel-body">
                                <div id="' . $drlr['ID'] . '"></div>
                                <div class="text-right">
                                    <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>';*/
						$vntq = "SELECT * FROM `visit_notes` WHERE `user` = '" . $drlr['ID'] . "' AND YEARWEEK(`date`,1) = YEARWEEK(CURRENT_DATE(),1)";
						$vntg = mysqli_query($conn, $vntq) or die($conn->error);
						$vnt = mysqli_num_rows($vntg) / 5;
						
							echo '<div class="col-lg-6">
                        <div class="panel panel-red card-view">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> ' . $drlr['fname'] . ' ' . $drlr['lname'] . ' Survey Ratings  |  <span style="color:black;">Visits Per Day: </span><span style="color:blue;">' . $vnt . '</span></h3>
                            </div>
                            <div class="panel-body">
                                <div id="' . $drlr['ID'] . '"></div>
                                <!--<div class="text-right">
                                    <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                                </div>-->
                            </div>
                        </div>
                    </div>';
					}
					}else{
						if($_SESSION['position'] == 'Outside Sales Rep'){
						$drlq = "SELECT * FROM `users` WHERE `ID` = '" . $_SESSION['user_id'] . "'";
						$drlg = mysqli_query($conn, $drlq) or die($conn->error);
						while($drlr = mysqli_fetch_array($drlg)){
							echo '<div class="col-lg-6">
                        <div class="panel panel-red card-view">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> ' . $drlr['fname'] . ' ' . $drlr['lname'] . ' Survey Ratings</h3>
                            </div>
                            <div class="panel-body">
                                <div id="' . $drlr['ID'] . '"></div>
                                <!--<div class="text-right">
                                    <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                                </div>-->
                            </div>
                        </div>
                    </div>';
						}
						}
					}
				?>
				
	
			
			</div>
			



			<!-- Footer -->
			<?php include 'global/sections/footer.php'; ?>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
      
			 <!-- Modal -->
  <div class="modal fade" id="alert1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="seen('alert1');" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Market Force Update</h4>
        </div>
        <div class="modal-body">
					<h2 style="color:blue;">Market Force now has a task manager on the dashboard!</h2>
          <img src="knowledgebase/tasks/myTasks.png" style="width:100%;" />
					<h4>You can assign tasks with deadlines to yourself or others within Market Force.</h4>
          <img src="knowledgebase/tasks/newTask.png" style="width:100%;" />
					<h4>If the task is for someone else, Market Force will notify them about the task via email and SMS text messages.</h4>
          <img src="knowledgebase/tasks/sms-notify.png" style="width:100%;" />
					<h4>You can send communications about the task!</h4>
          <img src="knowledgebase/tasks/taskMessage.png" style="width:100%;" />
					<hr />
					<h4>
						For more information about the task manager, please email Mike @ 
						<a href="mailto:michael@burtonsolution.tech" style="color:red;">michael@burtonsolution.tech</a>!
					</h4>
				</div>
        <div class="modal-footer">
					<!--<button type="button" class="btn btn-success" onclick="check_request('',2);">Submit</button>-->
          <button type="button" class="btn btn-default" onclick="seen('alert1');" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
  
  
  
			 <!-- Modal -->
  <div class="modal fade" id="alert2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="seen('alert2');" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">A Message From Market Force</h4>
        </div>
        <div class="modal-body text-center">
          <h1>New Help Desk Chat!</h1>
          <img src="knowledgebase/chat-feature/chat-icon.jpg" style="width:30%;margin:auto;"/>
          <h3>Notice this icon in the bottom right corner or your screen?</h3>
          <img src="knowledgebase/chat-feature/chat-open-screen.jpg" style="width:100%;" />
          <h3>
            Good! Click on it to be connected with a Help Desk Representative! 
            If the icon is pulsing, that means there is a representative currently online!
          </h3>
        </div>
        <div class="modal-footer">
					<!--<button type="button" class="btn btn-success" onclick="check_request('',2);">Submit</button>-->
          <button type="button" class="btn btn-default" onclick="seen('alert2','Yes');" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
    
    
			 <!-- Modal -->
  <div class="modal fade" id="alert3" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="seen('alert3');" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">A Message From Market Force</h4>
        </div>
        <div class="modal-body">
          <h4 style="text-align:center; color:blue;"><i class="fa fa-info-circle"></i> UPDATE: The new Collections Customer Filter has been installed! <br> (See Below Image)</h4>
          <img src="img/special/new-filter.jpg" style="width:100%;" />
        </div>
        <div class="modal-footer">
					<!--<button type="button" class="btn btn-success" onclick="check_request('',2);">Submit</button>-->
          <button type="button" class="btn btn-default" onclick="seen('alert3');" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->

  
	<!-- Modal -->
  <div class="modal fade" id="taskNote" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Task Communication</h4>
        </div>
        <div class="modal-body">
					<p>Please Enter Your Message:</p>
					<textarea id="task_note" class="form-control" style="height:150px;"></textarea>
					<input type="hidden" id="taskNote_id" />
					<span style="color:red;font-weight:bold;" id="task_note_error"></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success"  onclick="taskNote();">Submit</button>
        </div>
      </div>
      
    </div>
  </div>
	<!-- End Modal -->
	
	
	<?php include 'dashboard/modals/new-dealer-checklist-modal.php'; ?>
	
      
  <!-- jQuery -->
  <script src="js/jquery.js"></script>
      
      <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <!--<script src="js/plugins/morris/morris-data.js"></script>-->
	
									
									<?php
									include 'forms/php/rater.php';
										$x = new rater;
													
										$chartg = mysqli_query($conn, $drlq);
										while($chartr = mysqli_fetch_array($chartg)){
											
										$x->rep_id = $chartr['ID'];
										$x->IDToName();
										$x->rate();
                      
											 
                      
											
											echo '<script>
											// Bar Chart
    Morris.Bar({
        element: "' . $chartr['ID'] . '",
        data: [{
            device: "Helpfulness",
            geekbench: ' . intval($x->helpful) . ',
        }, {
            device: "Dependability",
            geekbench: ' . intval($x->dependable) . ',
        }, {
            device: "Knowledge",
            geekbench: ' . intval($x->knowledgeable) . ',
        }, {
            device: "Procedural",
            geekbench: ' . intval($x->follows_procedure) . ',
        }, {
            device: "Involved",
            geekbench: ' . intval($x->involved) . ',
        }],
        xkey: "device",
        ykeys: ["geekbench"],
        labels: ["Grade"],
        barRatio: 0.4,
				ymax: 100,
				ymin: 0,
        xLabelAngle: 35,
        hideHover: "auto",
        resize: true,
    });
		</script>';
							
				}
	
	
	$qdate = date("m");
  $qyear = date("Y");
	
	if($_SESSION['manager'] == 'Yes'){
		$daiq = "SELECT * FROM `new_dealer_form` WHERE MONTH(`date`) = '" . $qdate . "' AND YEAR(`date`) = '" . $qyear . "'";
	}else{
		$daiq = "SELECT * FROM `new_dealer_form` WHERE `user` = '" . $_SESSION['full_name'] . "' AND MONTH(`date`) = '" . $qdate . "' AND YEAR(`date`) = '" . $qyear . "'";
	}
	$daig = mysqli_query($conn, $daiq) or die($conn->error);
	$dain = mysqli_num_rows($daig);
	
	if($_SESSION['manager'] == 'Yes'){
		$dciq = "SELECT * FROM `new_packets` WHERE `form_type` = 'Dealer Closing Form' AND MONTH(`date`) = '" . $qdate . "' AND YEAR(`date`) = '" . $qyear . "'";
	}else{
		$dciq = "SELECT * FROM `new_packets` WHERE `rep` = '" . $_SESSION['user_id'] . "' AND `form_type` = 'Dealer Closing Form' AND MONTH(`date`) = '" . $qdate . "' AND YEAR(`date`) = '" . $qyear . "'";
	}
	$dcig = mysqli_query($conn, $dciq) or die($conn->error);
	$dcin = mysqli_num_rows($dcig);
	
	echo '<script>
				//alert("' . $dain . '");
				</script>';
	
									echo'<script>
												// Donut Chart
  										  Morris.Donut({
  										      element: "donut_graph",
  										      data: [{
  										          label: "New '; if($_SESSION['org_id'] == '123456'){echo 'Clients';}else{echo 'Dealers';}
                          echo '",
  										          value: ' . $dain . ',
																color: "green"
  										      }, {
  										          label: "'; if($_SESSION['org_id'] == '123456'){echo 'Clients';}else{echo 'Dealers';}
                          echo ' Closed",
  										          value: ' . $dcin . ',
																color: "red"
  										      }],
  										      resize: true
  										  });
												</script>';
											
											
		$y = new rater;	
					$y->rate_all();
	
				echo '<script>
											// Bar Chart
    /*Morris.Bar({
        element: "all-chart",
        data: [{
            device: "Helpfulness",
            geekbench: ' . intval($y->helpful) . ',
        }, {
            device: "Dependability",
            geekbench: ' . intval($y->dependable) . ',
        }, {
            device: "Knowledge",
            geekbench: ' . intval($y->knowledgeable) . ',
        }, {
            device: "Procedural",
            geekbench: ' . intval($y->follows_procedure) . ',
        }, {
            device: "Involved",
            geekbench: ' . intval($y->involved) . ',
        }],
        xkey: "device",
        ykeys: ["geekbench"],
        labels: ["Grade"],
        barRatio: 0.4,
				ymax: 100,
				ymin: 0,
        xLabelAngle: 35,
        hideHover: "auto",
        resize: true,
    });*/
		</script>';
									?>

  
 <?php //include 'footer.html'; ?>
 <?php include 'projects/new-project-task-modal.php'; ?>
 <?php //include 'projects/add-edit-task.php'; ?>
      
   <script src="dashboard/js/display-functions.js"></script>
	<script src="dashboard/js/checklist-functions.js"></script>
      
	<!--Footer-->
	<?php include 'global/sections/includes.php'; ?>
</body>
<script>
    $( document ).ready(function() {
        <?php
		$alertq = "SELECT * FROM `users` WHERE `ID` = '" . $_SESSION['user_id'] . "'";
		$alertg = mysqli_query($conn, $alertq) or die($conn->error);
		$alertr = mysqli_fetch_array($alertg);
		//if($_SESSION['user_id'] == '1'){
		if($alertr['alert1'] != 'Viewed'){
			echo '$("#alert1").modal("show");';
		}elseif($alertr['alert2'] != 'Viewed'){
			echo '$("#alert2").modal("show");';
			echo 'play_vid();';
		}elseif($_SESSION['collections'] == 'Yes' && $alertr['alert3'] != 'Viewed'){
			echo '$("#alert3").modal("show");';
		}
		?>
		document.getElementById('preloader-it').style.display = 'none';
		
		
    });		
	</script>

</html>
