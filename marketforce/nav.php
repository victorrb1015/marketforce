<?php
//error_reporting(0);
//include 'security/session/session-settings.php';
?>
  <!-- Jira Service Desk Widget -->
  <!--<script data-jsd-embedded data-key="b6c6e098-9239-4e3f-bb00-bbd0e9aaa0a5" data-base-url="https://jsd-widget.atlassian.com" src="https://jsd-widget.atlassian.com/assets/embed.js"></script>-->

  <!-- TeamWork Help Desk Widget -->
    <script>
        !(function(e) {
            var basehref = "https://ignitioninnovations.teamwork.com",
                token = "489ae318-07b3-468f-aed7-46c491f7b9a0";

            window.deskcontactwidget = {};
            var r=e.getElementsByTagName("script")[0],c=e.createElement("script");c.type="text/javascript",c.async=!0,c.src=basehref+"/support/v1/contact/main.js?token="+token,r.parentNode.insertBefore(c,r),window.addEventListener("message",function(e){var t=e.data[0],a=e.data[1];switch(t){case"setContactFormHeight":document.getElementById("deskcontactwidgetframe").height=Math.min(a, window.window.innerHeight - 75)}},!1);
        })(document);
    </script>
<script>
  var icon_link = document.createElement('link');
  icon_link.setAttribute('rel','icon');
  icon_link.setAttribute('href','img/favicon.ico');
  icon_link.setAttribute('type','image/x-icon');
  document.getElementsByTagName("head")[0].appendChild(icon_link);
</script>
<style>
  #form-box {
  border:2px solid rgb(51,122,183);
  background:rgba(51,122,183,0.1);
}
  mark{
    background: yellow;
  }

</style>
<link href="https://fonts.googleapis.com/css?family=Rock+Salt" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Alfa+Slab+One" rel="stylesheet">
<link href="css/bs-custom.css" rel="stylesheet">
<!--JQuery CSS-->
    <link href="jquery/jquery-ui.css" rel="stylesheet">
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php" style="padding-top:5px!Important;padding-left:5px!Important;">
                  <?php
                  if('' != $_SESSION['org_logo_url']){
                      $width = '80';
                      $logo = $_SESSION['org_logo_url'];
                      if($_SESSION['org_id'] == '987874'){
                            $width = '40';
                          $logo = $_SESSION['org_logo_url'];
                      }
                      if($_SESSION['org_id'] == '558558'){
                          $width = '10';
                          $logo = $_SESSION['org_logo_url'];
                      }
                      if($_SESSION['org_id'] == '329698'){
                          $width = '5';
                          $logo = '//marketforceapp.com/assets/img/brands/Logo-blanco.png';
                      }
                      if($_SESSION['org_id'] == '123456'){
                          $width = '8';
                          $logo = $_SESSION['org_logo_url'];
                      }
                      if($_SESSION['org_id'] == '532748'){
                          $width = '10';
                          $logo = $_SESSION['org_logo_url'];
                      }
                      if($_SESSION['org_id'] == '174449'){
                          $width = '5';
                          $logo = $_SESSION['org_logo_url'];
                      }
                      if($_SESSION['org_id'] == '191554'){
                        $width = '10';
                        $logo = $_SESSION['org_logo_url'];
                      }
                      if($_SESSION['org_id'] == '841963'){
                          $width = '3';
                          $logo = $_SESSION['org_logo_url'];
                      }
                      
                      echo '<img src="' . $logo . '" style="width:' . $width . '%;">';
                  }else{
                    echo '<h1 style="font-family:Alfa Slab One;color:white;font-size:20px;margin-top:5px;margin-left:-5px;">' . $_SESSION['org_name'] . '</h1>';
                  }
                  ?>
                </a>
            </div>
            <!-- Top Menu Items -->

            <?php
              //if($_SESSION['admin'] == 'Yes'){
                echo '<h4 id="tSales" style="color:green;font-weight:bold;text-align:center;float:left;margin-left:20%;"></h4>';
              //}
              ?>
  
            <ul class="nav navbar-right top-nav">
                				
								<li style="margin-top:8px;margin-right:15px;">
									<form action="search.php" method="post">
									<div class="input-group" style="width:350px;">
                                        <!--<div class="input-group-addon"></div>-->
                                        <input class="form-control" type="text" id="msearch" name="msearch" placeholder="Search Customer Name, Invoice#, Phone#">
                                        <div class="input-group-addon">
                                        	<span><i class="fa fa-search"></i></span>
                                        </div>
                                    </div>
                                	</form>
								</li>
								<li style="display:table;">
									<button type="button" data-toggle="modal" data-target="#newTask" onclick="clear_newTask_modal();" class="btn btn-success btn-sm" style="margin-top:10px;">New Task</button>
								</li>
                                <li>
                                    <div type="button" id="google_translate_element" class="google " style="margin-top: 17px !important; margin-left: 20px !important;margin-right: 20px !important;">
                                    </div>
                                    <script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                                    <script type="text/javascript">
                                        function googleTranslateElementInit() {
                                            new google.translate.TranslateElement({
                                                pageLanguage: 'en',
                                                includedLanguages: 'en,es',
                                                layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
                                                gaTrack: true
                                            }, 'google_translate_element');
                                        }
                                    </script>
                                </li>
								<li>
									<a href="#" data-toggle="modal" data-target="#phoneLookup" style="color:red;font-weight:bold;">
										<i class="fa fa-phone fa-fw"></i>
									</a>
								</li>
                				<li>
									<a href="#" data-toggle="modal" data-target="#dealerCalc" style="color:red;font-weight:bold;">
										<i class="fa fa-calculator fa-fw"></i>
									</a>
								</li>
                <li class="dropdown">
                    <a href="Javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['full_name']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
											<?php
											if($_SESSION['admin'] == 'Yes'){
												echo '<li>
													<a href="access.php"><i class="fa fa-users"></i> Manage Users</a>
											  </li>';
											}
											?>
											
												<li>
													<a href="settings.php"><i class="fa fa-cogs"></i> My Account</a>
												</li>
											
                      <?php
                      if($_SESSION['user_id'] == '1'){
                        echo '<li>
                                <a href="knowledge-base.php"><i class="fa fa-graduation-cap"></i> Training</a>
                              </li>';
                      }
                      ?>
											
											 <li>
												 <a href="../cron/labels.php" target="_blank"><i class="fa fa-tags"></i> Print Labels</a>
											 </li>

						        <?php
                      if($_SESSION['manager'] == 'Yes' || $_SESSION['admin'] == 'Yes'){
                        echo '<li>
                                <a href="orders/display/order-display.php?org_id=' . $_SESSION['org_id'] . '" target="_blank"><i class="fa fa-external-link"></i> Launch ODS</a>
                              </li>';
                      }
                      ?>

												
												<li class="divider"></li>
                        <li>
                            <a href="../index.php?logout=1"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav" style="flex-direction:inherit;">
                  
     
           <!-------------------------------------------------------------------DASHBOARD-------------------------------------------------------------------------------->
                    <li>
                        <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard
											<?php
													
													if($_SESSION['doc_approval'] == 'Yes'){
														$ndq = "SELECT * FROM `new_packets` WHERE `status` = 'Pending'";
													}else{
														$ndq = "SELECT * FROM `new_packets` WHERE `status` = 'Pending' AND `rep` = '" . $_SESSION['user_id'] . "'";
													}
													$ndg = mysqli_query($conn, $ndq);
													$ndn = mysqli_num_rows($ndg);
													if($ndn != 0){
													echo '&nbsp; <span style="background:red;color:white;padding:5px;border-radius:20px;">' . $ndn . '</span>';
													}
													?>
												</a>
                    </li>
           <!-------------------------------------------------------------------Accounting-------------------------------------------------------------------------------->
                  <?php
									//if($_SESSION['accountant'] == 'Yes'){	
									if($_SESSION['accountant'] == 'Yes' || $_SESSION['check_request'] == 'Yes'){	
										$crnum = 0;
													if($_SESSION['accountant'] == 'Yes'){
														$crq = "SELECT * FROM `check_requests` WHERE `status` != 'Completed' AND `status` != 'Cancelled'";
														$crg = mysqli_query($conn,$crq) or die($conn->error);
														$crnum = mysqli_num_rows($crg);
													}
									echo '<li>
											<a href="accounting.php"><i class="fa fa-usd fa-fw"></i> Accounting';
										if($crnum > 0){
											echo '&nbsp; <span style="background:red;color:white;padding:5px;border-radius:20px;">' . $crnum . '</span>';
										}
										echo '</a>
										</li>';
									}
                  ?>
                  
           <!-------------------------------------------------------------------Collections-------------------------------------------------------------------------------->
                  <?php
									$collq = "SELECT * FROM `collections` WHERE `inactive` != 'Yes' AND `in_legal` != 'Yes' AND `collection` = 'Yes' AND `assigned_rep_id` = '" . $_SESSION['user_id'] . "' AND `last_activity` != CURRENT_DATE";
									$collg = mysqli_query($conn, $collq) or die($conn->error);
									$colln = mysqli_num_rows($collg);
									$legq = "SELECT * FROM `collections` WHERE `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `assigned_rep_id` = '" . $_SESSION['user_id'] . "' AND `last_activity` != CURRENT_DATE";
									$legg = mysqli_query($conn, $legq) or die($conn->error);
									$legn = mysqli_num_rows($legg);
									$tn = $colln + $legn;
									if($_SESSION['collections'] == 'Yes' && $_SESSION['org_id'] != '987874'){
										echo '<li>
														<a href="javascript:;" data-toggle="collapse" data-target="#collections"><i class="fa fa-arrows-v fa-fw"></i> Collections <i class="fa fa-fw fa-caret-down"></i>';
										if($colln != 0 || $legn != 0){
										echo '&nbsp; <span style="background:red;color:white;padding:5px;border-radius:20px;">' . $tn . '</span>';
										}
											echo '</a>
                        		<ul id="collections" class="collapse">
															<li>
																<a href="collections.php"><i class="fa fa-usd fa-fw"></i> Collections List &nbsp; <span style="background:red;color:white;padding:5px;border-radius:20px;">' . $colln . '</span></a>
															</li>
															<li>
																<a href="collection-reports.php"><i class="fa fa-line-chart fa-fw"></i> Collection Reports</a>
															</li>';
										if($_SESSION['position'] == 'Developer'){
											echo '<li>
															<a href="display-collections.php"><i class="fa fa-home fa-fw"></i> Display Collections &nbsp; <span style="background:red;color:white;padding:5px;border-radius:20px;">' . $legn . '</span></a>
														</li>';
										}
										if($_SESSION['user_id'] == '1' || $_SESSION['user_id'] == '3' || $_SESSION['user_id'] == '50' || $_SESSION['user_id'] == '93'){
											echo '<li>
															<a href="legal.php"><i class="fa fa-gavel fa-fw"></i> Legal &nbsp; <span style="background:red;color:white;padding:5px;border-radius:20px;">' . $legn . '</span></a>
														</li>';
										}
											echo '</ul>
													</li>';
									}
                  ?>
                  
           <!-------------------------------------------------------------------Commission Form-------------------------------------------------------------------------------->
                  <?php
									if(($_SESSION['user_id'] == '3' || $_SESSION['user_id'] == '39' || $_SESSION['user_id'] =='1') && $_SESSION['org_id'] != '987874'){
                     echo '<!--<li>
                              <a href="commission/entry-form.php" target="_blank"><i class="fa fa-fw fa-file"></i> Commission Form</a>
                            </li>-->';
                    }
                  ?>
                  
           <!-------------------------------------------------------------------Communications-------------------------------------------------------------------------------->
									<?php
										if($_SESSION['admin'] == 'Yes' || $_SESSION['send_survey'] == 'Yes'){
						  echo	'<li>
						 					<a href="javascript:;" data-toggle="collapse" data-target="#communications"><i class="fa fa-fw fa-arrows-v"></i> Communications <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="communications" class="collapse">';
										}
							if($_SESSION['send_survey'] == 'Yes'){
										echo '<li>
															<a href="send-survey.php"><i class="fa fa-fw fa-paper-plane"></i> Send A Survey</a>
													</li>';
							}
							if($_SESSION['admin'] == 'Yes'){
              //if($_SESSION['user_id'] == 1){
										echo '<li>
                    			    <a href="dealer-update.php"><i class=" fa fa-fw fa-share-square-o"></i> Send '; 
                if($_SESSION['org_id'] == '832122' ||  $_SESSION['org_id'] == '738004' ||  $_SESSION['org_id'] == '654321' ){echo 'Acero News ';}else{echo 'Dealer Update';}
                      echo '</a>
                    			</li>
													<li>
                    			    <a href="osr-update.php"><i class="fa fa-fw fa-share-square-o"></i> Send OSTR Update</a>
                    			</li>
													<li>
															<a href="notification.php"><i class="fa fa-fw fa-share-square-o"></i> Create Notification</a>
													</li>';
							}
							if($_SESSION['admin'] == 'Yes' || $_SESSION['send_survey'] == 'Yes'){
									echo '</ul>
									 </li>';
										}
                  ?>

            <!-------------------------------------------------------------------Customer Search-------------------------------------------------------------------------------->
									<?php
										if($_SESSION['in'] == 'Yes' && $_SESSION['org_id'] != '987874'){
											echo '<li>
														<a href="search.php"><i class="fa fa-search fa-fw"></i> Customer Search</a>
														</li>';
										}
                  ?> 

           <!-------------------------------------------------------------------Dealer Displays-------------------------------------------------------------------------------->
                    <?php
                    if($_SESSION['doc_approval'] == 'Yes' || $_SESSION['position'] == 'Outside Sales Rep'){
                    	echo '<li>
												<a href="dealer-displays.php"><i class="fa fa-fw fa-home"></i> ';
                        switch($_SESSION['org_id']) {
                            case "832122":
                                echo "Customer ";
                                break;
                            case "123456":
                            case '532748':
                                echo "Client ";
                                break;
                            default:
                                echo 'Dealer ';
                        }
                      echo ' Displays</a>
										</li>';
                    }
                    ?>
                  
           <!-------------------------------------------------------------------Dealer Forms-------------------------------------------------------------------------------->
                    <?php
                  if($_SESSION['org_id'] != '987874'){
                    echo '<li>
												<a href="print-forms.php"><i class="fa fa-fw fa-files-o"></i> ';
                      switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                          case '532748':
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }
                      echo ' Forms</a>
										</li>';
                  }
                  ?>
                  
           <!-------------------------------------------------------------------Dealer Lookup-------------------------------------------------------------------------------->
										<?php
                    if($_SESSION['org_id'] != '987874'){
                    echo '<li>
                      <a href="dealer-lookup.php"><i class="fa fa-fw fa-search"></i> ';
                        switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                          case '532748':
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }
                      echo ' Lookup</a>
                    </li>';
                    }
                  ?>
                  
           <!-------------------------------------------------------------------Dealer Map-------------------------------------------------------------------------------->
									<?php
                  if($_SESSION['org_id'] != '987874'){
                    echo '<li>
												    <a href="map2.php"><i class="fa fa-globe fa-fw"></i> ';
                      switch($_SESSION['org_id']) {
                          case "832122":
                          case "123456":
                          case '532748':
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }
                      echo ' Map</a>
									        </li>';
                  }
                  ?>
                  
           <!-------------------------------------------------------------------Employee Center-------------------------------------------------------------------------------->
										<li>
											<a href="employee-center.php">
												<i class="fa fa-fw fa-users"></i> Employee Center 
												<?php
												if($_SESSION['EC'] == 'Yes'){
												$ecq = "SELECT * FROM `requests` WHERE `status` = 'Pending'";
												}else{
													$ecq = "SELECT * FROM `requests` WHERE `status` = 'Pending' AND `user_id` = '" . $_SESSION['user_id'] . "'";
												}
												$ecg = mysqli_query($conn, $ecq);
												$ecn = mysqli_num_rows($ecg);
												if($ecn == 0){
													$ecn = '';
												}else{
													echo '&nbsp;<span style="background:red;color:white;padding:5px;border-radius:20px;">' . $ecn . '</span>';
												}
												?>
												
											</a>
										</li>
                  
           <!-------------------------------------------------------------------Expense Reports-------------------------------------------------------------------------------->
                  <?php
										if($_SESSION['in'] == 'Yes'){
											echo '<li>
                   				   <a href="expense-reports.php"><i class="fa fa-fw fa-usd"></i> Expense Reports</a>
                   				 </li>';
										}
                    ?>
                  
           <!-------------------------------------------------------------------Inventory-------------------------------------------------------------------------------->
									<?php
                  if($_SESSION['inventory'] == 'Yes'){
										if($_SESSION['in'] == 'Yes'){
						  echo	'<li>
						 					<a href="javascript:;" data-toggle="collapse" data-target="#inventory"><i class="fa fa-fw fa-arrows-v"></i> Inventory <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="inventory" class="collapse">';
										}
							if($_SESSION['in'] == 'Yes'){
										echo '<li>
															<a href="inventory.php"><i class="fa fa-fw fa-boxes"></i> Items</a>
													</li>';
							}
							if($_SESSION['in'] == 'Yes'){
										echo '<li>
                    			    <a href="inventory-manufacturing.php"><i class=" fa fa-fw fa-city"></i> Manufacturing</a>
                    			</li>
													<li>
                    			    <a href="inventory-shipping.php"><i class="fa fa-fw fa-shipping-fast"></i>'; if($_SESSION['org_id'] == '257823'){echo 'Transfers ';}else{echo 'Shipping ';} 
                        echo '</a>
                    			</li>
													<li>
															<a href="inventory-receiving.php"><i class="fa fa-fw fa-dolly"></i> Receiving</a>
													</li>
													<li>
															<a href="inventory-reports.php"><i class="fa fa-fw fa-clipboard-list"></i> Reports</a>
													</li>';
							}
							if($_SESSION['in'] == 'Yes'){
									echo '</ul>
									 </li>';
										}
                  }//End Main Access...
                  ?>

                  
           <!-------------------------------------------------------------New Dealer Application-------------------------------------------------------------------------------->

										<?php
                  if($_SESSION['org_id'] != '987874'){
                    echo '<li>
												<a href="new-dealer.php"><i class="fa fa-fw fa-file-text"></i> New ';
                      switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                          case '532748':
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }
                      echo '</a>
										</li>';
                  }
                  ?>
                  
           <!-------------------------------------------------------------------Orders-------------------------------------------------------------------------------->
                  <?php
                   /*if($_SESSION['orders'] == 'Yes'){
                    echo '<li>
                              <a href="orders.php"><i class="fa fa-fw fa-shopping-cart"></i> Orders</a>
                          </li>';
                   } */
                  ?>
        <!-------------------------------------------------------------------Sales-------------------------------------------------------------------------------->
                  <?php
                   //if($_SESSION['orders'] == 'Yes'){
                    //echo '<li>
                    //          <a href="sales.php"><i class="fa fa-fw fa-shopping-cart"></i> Sales</a>
                     //     </li>';
                   //}
                  ?>


		    <!-------------------------------------------------------------------Permits-------------------------------------------------------------------------------->
                  <?php
                   if($_SESSION['permits'] == 'Yes' || $_SESSION['position'] == 'Developer'){
                    echo '<li>
                              <a href="permits.php"><i class="fa fa-fw fa-file-powerpoint-o"></i> Permits</a>
                          </li>';
                   }
                  ?> 
            <!-------------------------------------------------------------------Petty Cash-------------------------------------------------------------------------------->
                  <?php
                   if($_SESSION['pc'] == 'Yes' || $_SESSION['position'] == 'Developer'){
                    echo '<li>
                              <a href="petty-cash.php"><i class="fa fa-fw fa-money"></i> Petty Cash</a>
                          </li>';
                   }
                  ?>               
           <!-------------------------------------------------------------------Pre-Owned Listings-------------------------------------------------------------------------------->
                  <?php
					if($_SESSION['preowned'] == 'Yes' && $_SESSION['org_id'] == '257823'){
							echo '<li>
                        <a href="preowned-listings.php"><i class="fa fa-fw fa-th-list"></i> Pre-Owned Listings</a>
                    </li>';
										}
									?>
                  
           <!-------------------------------------------------------------------Repairs---------------------------------------------------------------------------------------->
                  <?php
										if(($_SESSION['repairs'] == 'Yes' || $_SESSION['repairs'] == '') && $_SESSION['org_id'] != '987874'){
											$rq = "SELECT * FROM `repairs` WHERE `inactive` != 'Yes' AND `status` != 'Completed' AND `status` != 'Cancelled' AND `status` != 'Draft' AND `state` = '" . $_SESSION['myState'] . "' OR `state` = '" . $_SESSION['myState2'] . "' OR `state` = '" . $_SESSION['myState3'] . "' OR `state` = '" . $_SESSION['myState4'] . "'";
											$rg = mysqli_query($conn, $rq) or die($conn->error);
											$rep = mysqli_num_rows($rg);
											echo '<li>
																<a href="repairs.php"><i class="fa fa-wrench fa-fw"></i> Repairs';
											if($rep == 0){
													$ecn = '';
												}else{
													echo '&nbsp;<span style="background:red;color:white;padding:5px;border-radius:20px;">' . $rep . '</span>';
												}
														echo '</a>
														</li>';
										}
                  ?>
                  
           <!-------------------------------------------------------------------Reports-------------------------------------------------------------------------------->
										<li>
											<a href="reports.php"><i class="fa fa-fw fa-area-chart"></i> Reports</a>
										</li>
                  
           <!-------------------------------------------------------------------REPOS---------------------------------------------------------------------------------------->
                  <?php
										if($_SESSION['repos'] == 'Yes' && $_SESSION['org_id'] != '987874'){
											$rq = "SELECT * FROM `repos` WHERE `status` != 'Completed' AND `status` != 'Cancelled' AND `state` = '" . $_SESSION['myState'] . "' OR `state` = '" . $_SESSION['myState2'] . "' OR `state` = '" . $_SESSION['myState3'] . "' OR `state` = '" . $_SESSION['myState4'] . "'";
											$rg = mysqli_query($conn, $rq) or die($conn->error);
											$rep = mysqli_num_rows($rg);
											echo '<li>
																<a href="repos.php"><i class="fa fa-refresh fa-fw"></i> Repos';
											if($rep == 0){
													$ecn = '';
												}else{
													echo '&nbsp;<span style="background:red;color:white;padding:5px;border-radius:20px;">' . $rep . '</span>';
												}
														echo '</a>
														</li>';
										}
                  ?>
                  
           <!-------------------------------------------------------------------Sandbox-------------------------------------------------------------------------------->
                  <?php
                  if($_SESSION['position'] == 'Developer'){
                  echo '<li>
                          <a href="sandbox.php"><i class="fa fa-fw fa-code"></i> Sandbox</a>
                        </li>';
                  }
                  ?>
                  
           <!-------------------------------------------------------------------Scheduler-------------------------------------------------------------------------------->
									<?php
										if($_SESSION['in'] == 'Yes' && $_SESSION['org_id'] != '987874'){
											echo '<li>
														<a href="scheduling.php"><i class="fa fa-calendar fa-fw"></i> Scheduler</a>
														</li>';
										}
                  ?>
                           
           <!------------------------------------------------------------------- Room ------------------------------------------------------------------------------->

                  <li>
                    <a href="meeting-rooms.php"><i class="fa fa-file-powerpoint-o"></i> &nbsp &nbsp Room</a>
                  </li>

            <!------------------------------------------------------------------- PO Order ------------------------------------------------------------------------------->

                  <?php
                  if($_SESSION['org_id'] == '841963' ){
                    if($_SESSION['full_name']=='training training'){
                      echo '<li><a href="orders_v2.php"><i class="fa fa-stack-exchange"></i> &nbsp &nbsp Orders</a></li>';
                    }else{
                      echo '<li><a href="orders_v2.php"><i class="fa fa-stack-exchange"></i> &nbsp &nbsp PO Order</a></li>';
                    }
                    
                  }
                  ?>
                  
           
            <!-------------------------------------------------------------------Visit Notes-------------------------------------------------------------------------------->
									<?php
                  if($_SESSION['org_id'] != '987874'){
                    echo '<li>
                    <a href="notes.php"><i class="fa fa-fw fa-comment-o"></i> Visit Notes</a>
                  </li>';
                  }
                  ?>
                  
           <!-------------------------------------------------------------------Waivers-------------------------------------------------------------------------------->
									<?php
										if($_SESSION['in'] == 'Yes'){
											echo '<!--<li style="border-bottom:1px dashed white;">
                   				   <a href="waivers.php"><i class="fa fa-fw fa-exclamation-triangle"></i> Waivers & Forms</a>
                   				 </li>-->';
										}
                  ?>
                  
           <!-------------------------------------------------------------------Make A Suggestion-------------------------------------------------------------------------------->
									<!--<li style="background:#3e3d3d;border:1px solid #222222;">
												<a href="suggestion.php"><i class="fa fa-lightbulb-o fa-fw"></i> Make A Suggestion</a>
									</li>-->
           <!-------------------------------------------------------------------View Open Issues-------------------------------------------------------------------------------->
                  <li style="background:#3e3d3d;border:1px solid #222222;">
												<a href="support.php" target="_blank"><i class="fa fa-eye fa-fw"></i> View Open Tickets</a>
									</li>
   
           <!-------------------------------------------------------------------Report An Issue-------------------------------------------------------------------------------->
                  <!--<li style="background:#3e3d3d;border:1px solid #222222;">
												<a href="https://ignitioninnovations.teamwork.com/support/" target="_blank"><i class="fa fa-ticket fa-fw"></i> New Support Ticket</a>
									</li>-->
									
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

<!--Market Force Error Notification-->
<!--<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-exclamation-triangle"></i>  
            <strong>
              NOTICE: Market Force is currently experiencing an issue with outgoing email communications. 
              Due to this issue, you may not receive some of the emails you would normally receive from Market Force. 
              We are working to correct the issue and you will be notified when this has been completely resolved!
            </strong>
        </div>
    </div>
</div>-->
<?php
if(($_SESSION['admin'] == 'Yes' || $_SESSION['manager'] == 'Yes') && $_SESSION['org_id'] == '257823'){
echo '<script>
	var getSalesStatus = true;
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      document.getElementById("tSales").innerHTML = "Current Day Sales: $"+r.salesToday;
      //set status based on response...
      
    }
  }
  xmlhttp.open("GET","apitest/daily-sales.php",true);
  xmlhttp.send();
  
  setInterval(function(){
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      document.getElementById("tSales").innerHTML = "Current Day Sales: $"+r.salesToday;
      
    }
  }
  xmlhttp.open("GET","apitest/daily-sales.php",true);
  xmlhttp.send();
  },10000);
</script>';
}
?>

<!--Chrome Browser Notification-->
<div class="row" id="chromeNotification" style="max-width:95%;"></div>
