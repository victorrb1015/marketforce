<div class="modal fade" role="dialog" tabindex="-1" id="timeoutModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <h1 class="text-center bg-primary modal-title"><i class="fa fa-clock-o"></i> Session Expiring!</h1>
                  <!--<h4 class="modal-title"><i class="fa fa-clock-o"></i> Your Session Is About To Expire!</h4>-->
                </div>
                <div class="modal-body">
                    <div class="row" style="background-color:#ffffff;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="/*background:rgba(51,122,183,0.2);*/">
                          <h3>Time Left On Current Session: <span id="timerText" style="color:red;"></span> </h3>
                          <br>
                          <h4>If you would like to continue, please click "Continue".</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" type="button" onclick="refresh_session();">Continue</button>
                </div>
            </div>
        </div>
    </div>