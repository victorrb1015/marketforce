<?php
include 'security/session/session-settings.php';

if(!isset($_SESSION['in'])){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';

$pageName = 'Permits';//Set this equal to the name of the page you would like to set...
$pageIcon = 'fa fa-file-powerpoint-o';//Set this equal to the class name of the icon you would like to set as the page icon...

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
		th, td{
			padding: 10px;
		}
		#dl, dc{
			height: 430px;
		}
		.panel-body{
			height: 382px;
		}
		#scroll{
			overflow: scroll;
		}
		::-webkit-scrollbar {
    width: 0px;  /* remove scrollbar space */
    background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
		
		
		.packet-table td, th{
			border: 1px solid black;
		}
		.packet-table{
			margin:auto;
		}
  </style>
  <script>
    //Global Variables...
    var rep_id = '<?php echo $_SESSION['user_id']; ?>';
    var rep_name = '<?php echo $_SESSION['full_name']; ?>';
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
       <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> <?php echo $pageName; ?></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
														<li>
															<i class="<?php echo $pageIcon; ?>"></i> <?php echo $pageName; ?>
													</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
		    <div>
        <div class="panel panel-success">
            <div class="panel-heading" style="background-color: rgb(171,204,163);">
                <h3 class="panel-title" style="background-color: rgba(171,204,163,0);">
                    Permits Display
                    &nbsp;&nbsp;
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#add-permit-form" onclick="load_permit_modal('New');"><i class="fa fa-plus"></i> Add Permit</button>
                </h3>
            </div>
            <div class="row">
           <div class="col-lg-12">
            <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped packet-table">
                                    <thead>
                                        <tr>
                                            <th>Unit</th>
                                            <th>Address</th>
                                            <th>Registration</th>
                                            <th>Registration Exp</th>
                                            <th>Bond #</th>
                                            <th>Bond Exp</th>
                                            <th>Fee</th>
                                            <th>Notes</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            <?php
                            $pq = "SELECT * FROM `building_permits` WHERE `inactive` != 'Yes'";
                            $pg = mysqli_query($conn, $pq) or die($conn->error);
                            while($pr = mysqli_fetch_array($pg)){
                                echo '<tr>
                                            <td>' . $pr['permit_unit'] . '</td>
                                            <td>' . $pr['permit_street1'] . ' ' . $pr['permit_street2'] . ' ' . $pr['permit_city'] . ', ' . $pr['permit_state'] . '</td>
                                            <td>' . $pr['permit_registration'] . '</td>
                                            <td>' . $pr['permit_registration_exp'] . '</td>
                                            <td>' . $pr['permit_bond'] . '</td>
                                            <td>' . $pr['permit_bond_exp'] . '</td>
                                            <td>' . $pr['permit_fee'] . '</td>
                                            <td>' . $pr['permit_notes'] . '</td>
                                            <td>
                                                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#add-permit-form" onclick="edit_permit(' . $pr['ID'] . ');">Edit</button>
                                            </td>
                                        </tr>';
                            }
                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
       </div>
          </div>      					
							
							
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!--<script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>-->

	<script src="permits/js/permit-functions.js"></script>
    <?php include 'permits/modals/add-permit-form.php'; ?>
    <?php include 'footer.html'; ?>
</body>

</html>