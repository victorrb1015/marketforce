<?php
include 'security/session/session-settings.php';

if($_SESSION['admin'] != 'Yes'){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
  </style>
  <script>
    
  function clr_res(id,s){
    //alert("test");
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			//alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","http://library.burtonsolution.tech/php/reserve.php?id="+id+"&s="+s,true);
  xmlhttp.send();
  }
		
		
		function s_s(){
			
			var r = document.getElementById('rep_name').value;
			var d = document.getElementById('dealer_name').value;
			var vd = document.getElementById('visit_date').value;
			var e = document.getElementById('email').value;
			
			if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //document.getElementById("livesearch").innerHTML=this.responseText;
			alert(this.responseText);
      window.location.reload();
      
    }
  }
  xmlhttp.open("GET","php/survey-sender.php?r="+r+"&d="+d+"&vd="+vd+"&e="+e,true);
  xmlhttp.send();
		}
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> Surveys</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                            <li>
                                <i class="fa fa-fw fa-file"></i> Send Survey Manually
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							<div class="row">
                <div class="col-lg-12">
                  <h2>
                    Rep Training Survey
                  </h2>
                  <p>
                    Name of All Steel Rep being surveyed:
                  </p>
									<select id="rep_name">
										<option value=''>Select A Rep...</option>
										<?php
										$rnq = "SELECT * FROM `users` WHERE `position` != 'Office Employee'";
										$rng = mysqli_query($conn, $rnq) or die($conn->error);
										while($rnr = mysqli_fetch_array($rng)){
											echo '<option value="' . $rnr['fname'] . ' ' . $rnr['lname'] . '">' . $rnr['fname'] . ' ' . $rnr['lname'] . '</option>';
										}
										?>
									</select>
                  <!--<input type="text" id="rep_name" placeholder="John Smith" />-->
                  <br><br>
                  <p>
                    Name of the <? switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }?> the survey is going to:
                  </p>
                  <select id="dealer_name">
                    <option value="default">Select One...</option>
                    <?php
                    $dnq = "SELECT * FROM `dealers` ORDER BY `name` ASC";
                    $dng = mysqli_query($conn, $dnq) or die($conn->error);
                    while($dnr = mysqli_fetch_array($dng)){
                      echo '<option value="' . $dnr['ID'] . '">' . $dnr['name'] . '</option>';
                    }
                    ?>
                  </select>
                  <br><br>
                  <p>
                    Date of Rep's visit:
                  </p>
                  <input type="date" placeholder="mm/dd/yyyy" id="visit_date" />
                  <br><br>
                  <p>
                    Email address to send survey to:
                  </p>
                  <input type="email" placeholder="john@example.com" id="email" />
                  <br><br>
                  <button type="button" onclick="s_s();">
                    Send Survey
                  </button>
                </div>
                
              </div>
							
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

 <?php include 'footer.html'; ?>
</body>

</html>