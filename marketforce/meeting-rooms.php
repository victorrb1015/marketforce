<?php
include 'security/session/session-settings.php';
include 'php/connection.php';
$pageName = 'Meeting Rooms';
$pageIcon = 'fas fa-handshake';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>MarketForce | All Steel</title>
    <?php include 'global/sections/head.php'; ?>
</head>
<body>
<!-- Preloader -->
<?php include 'global/sections/preloader.php'; ?>
<!-- /Preloader -->
<div class="wrapper theme-4-active pimary-color-red">
    <!--Navigation-->
    <?php include 'global/sections/nav.php'; ?>
    <!-- Main Content -->
    <div class="page-wrapper"><!--Includes Footer-->
        <div class="container-fluid pt-25">
            <?php include 'global/sections/page-title-bar.php'; ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary card-view" style="background: transparent;">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                MarketForce Room
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <!--Main Content Here-->
            <iframe
                    width="100%"
                    height="850"
                    src="<?php
                    if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
                        echo 'https';
                    }else{
                        echo 'http';
                    }
                    ?>://<?php echo $_SERVER['HTTP_HOST']; ?>/marketforce/meetings-rooms/main.php?org_id=<?echo $_SESSION['org_id']?>&user_name=<? echo $_SESSION['full_name']?>&user_id=<? echo $_SESSION['user_id']?>&org_database=<? echo $_SESSION['org_db_name']?>">
            </iframe>
        </div>
        <!-- Footer -->
        <?php include 'global/sections/footer.php'; ?>
        <!-- /Footer -->
    </div>
    <!-- /Main Content -->
</div>
<!-- /#wrapper -->
<!--Footer-->
<?php include 'global/sections/includes.php'; ?>

</body>
</html>
