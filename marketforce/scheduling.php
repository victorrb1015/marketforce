<?php
include 'security/session/session-settings.php';
include 'php/connection.php';
include 'security/sections/security-check-logged-in.php';
$pageName = 'Scheduler';
$pageIcon = 'fas fa-calendar';

echo '<script>
        var rep_id = "' . $_SESSION['user_id'] . '";
        var rep_name = "' . $_SESSION['full_name'] . '";
        var org_id = "' . $_SESSION['org_id'] . '";
      </script>';

$edit_route = $_GET['edit_route'];
$cache_buster = uniqid();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>MarketForce | All Steel</title>
  <?php include 'global/sections/head.php'; ?>
  <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <!-- Custom Map Scripts -->
  <link rel="stylesheet" href="scheduling/css/map-style.css"/>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>
  <script src="scheduling/js/map-functions.js?cb=<?php echo $cache_buster;?>"></script>
  <script src="scheduling/js/map-classes.js?cb=<?php echo $cache_buster; ?>"></script>
</head>

<body>
  <input type="hidden" id="exitCheck" value="No" />
  <!-- Preloader -->
  <?php //include 'global/sections/preloader.php'; ?>
  <!-- /Preloader -->
  <div class="wrapper theme-4-active pimary-color-red">

    <!--Navigation-->
    <?php include 'global/sections/nav.php'; ?>


    <!-- Main Content -->
    <div class="page-wrapper">
      <!--Includes Footer-->

      <div id="main-tour-window" class="container-fluid pt-25">
        <?php include 'global/sections/page-title-bar.php'; ?>
        
        <?php include 'scheduling/sections/legend.php'; ?>
        
        <div class="row mt-20">
          
          <!--Map Column-->
          <div class="col-lg-9">
            <?php include 'scheduling/sections/map-top-bar.php'; ?>
            <?php include 'scheduling/sections/scheduler-map.php'; ?>
          </div>
          <!--End Map Column-->
          
          <!--Route Column-->
          <div class="col-lg-3" style="max-height:650px; overflow:scroll;">
            <?php include 'scheduling/sections/route-display.php'; ?>
          </div>
          <!--End Route Column-->
          
        </div><!--//End Row-->
        
        <hr />
        
        <div class="row">
          <div class="col-12">
            <?php include 'scheduling/sections/routes-table.php'; ?>
          </div>
        </div><!--End Row-->
              
        <div class="row">
          <div class="col-lg-12">
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#viewActivity"><i class="fa fa-eye"></i> View Scheduler Activity</button>
            <button type="button" class="btn btn-warning" onclick="perform_manual_sync();"><i class="fas fa-sync-alt"></i> Manually Sync Orders</button>
          </div>
        </div>
        

        <!-- Footer -->
        <?php include 'global/sections/footer.php'; ?>
        <!-- /Footer -->

      </div>
      <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->

    <!--Footer-->
    <?php include 'global/sections/includes.php'; ?>
</body>
  
  <!--Modals-->
  <?php include 'scheduling/modals/route-details-modal.php'; ?>
  <?php include 'scheduling/modals/view-activity-modal.php'; ?>
  <?php include 'scheduling/modals/dispatch-layer-modal.php'; ?>
  <?php include 'scheduling/modals/complete-modal.php'; ?>
  
  <!-- JS Files -->
  <script src="scheduling/js/map-api.js?cb=<?php echo $cache_buster; ?>"></script>
  <script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgD4nC2sCBPqbuUw7fDlZBs6jV35lL5Mg&callback=initAutocomplete&libraries=places">
  </script>
  <script src="scheduling/js/print-topsheet.js"></script>
  <script src="scheduling/js/route-status-functions.js"></script>
  <script src="scheduling/js/route-details-functions.js"></script>
  
  <!--Custom Tour-->
  <!--<script src="scheduling/js/custom-tour.js"></script>-->

</html>