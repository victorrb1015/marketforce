let newWorker;
var cacheNameGlobal;


if ('serviceWorker' in navigator) {
  // Register the service worker
  navigator.serviceWorker.register('sw.js').then(reg => {
    getCacheName();
    reg.addEventListener('updatefound', () => {

      // An updated service worker has appeared in reg.installing!
      newWorker = reg.installing;

      newWorker.addEventListener('statechange', () => {

        // Has service worker state changed?
        switch (newWorker.state) {
          case 'installed':

            // There is a new service worker available, show the notification
            if (navigator.serviceWorker.controller) {
              skipper();
            }

            break;
        }
      });
    });
  });

}


let refreshing;
// The event listener that is fired when the service worker updates
// Here we reload the page
navigator.serviceWorker.addEventListener('controllerchange', function(e) {
  if (refreshing) return;
  window.location.reload();
  refreshing = true;
});


navigator.serviceWorker.addEventListener('message', event => {
  //console.warn(event.data.msg);
  if(event.data.type === 'Update'){
    toast_alert('Update', event.data.msg, 'top-center', 'info');
  }
  if(event.data.type === 'cacheName'){
    cacheNameGlobal = event.data.msg;
    console.warn('SW is using cacheName: '+cacheNameGlobal);
  }
  
});


function skipper() {
  newWorker.postMessage({
    action: 'skipWaiting'
  });
}

function getCacheName() {
  navigator.serviceWorker.controller.postMessage({
    action: 'getCacheName'
  });
}


//Set Cache Storage Expiration
const oneDay = 60 * 60 * 24 * 1000;
if(localStorage.getItem("PWA_Cache_Expire")){
  var pwa_expire = localStorage.getItem("PWA_Cache_Expire");
  var isOld = (Date.now() - pwa_expire) > oneDay;
  if(isOld === true){
    console.log("Cache is OLD");
    caches.delete(cacheNameGlobal).then(function(boolean) {
      // your cache is now deleted
      localStorage.setItem("PWA_Cache_Expire",Date.now());
      console.warn("Cache has been deleted and new Expire has been set");
    });
  }else{
    console.log("Cache is UP-TO-DATE");
  }
}else{
  //localStorage.setItem("PWA_Cache_Expire",(Date.now() - oneDay));//Used for testing
  localStorage.setItem("PWA_Cache_Expire",Date.now());
}

function reset_cache(){
  console.log("Resetting Cache...");
  caches.delete(cacheNameGlobal).then(function(boolean) {
    // your cache is now deleted
    localStorage.setItem("PWA_Cache_Expire",Date.now());
    console.warn("Cache has been deleted and new Expire has been set");
  });
}

function reset_sw_registration(confirm){
  navigator.serviceWorker.getRegistrations().then(function(registrations) {
   console.log('Current SW Registrations:');
    console.log(registrations);
   for(let registration of registrations) {
      registration.unregister()
    } 
  });
  if(confirm === true){
    alert('MarketForce has successfully reset its Service Worker Registrations.');
    window.location = 'index.php';
  }
}



/*if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("sw.js").then(registration => {
    console.log('SW Registered!');
    console.log(registration);
    if (registration.active) {
      console.log('Status: ' + registration.active.state);
    } else {
      console.log('Not Status: ');
    }

  }).catch(error => {
    console.log('SW Registration Failed...');
  })
}*/


/*async function subscribe_pwa_notifications(){
  navigator.serviceWorker.ready.then(reg => {
    reg.pushManager.getSubscription().then(sub => {
      if(sub == undefined){
        //ask user to subscribe
        navigator.serviceWorker.getRegistration().then(reg => {
          reg.pushManager.subscribe({
            userVisibileOnly: true,
            applicationServerKey: 'BDn9cGy9hKw7TONGWr0tNSd3KKkx1xBzBI6_IZ5fcPZYWwWj16GCseoj6CGjp6GiihPGqCmYMPrSNwxxWZFU2oA'
          }).then(sub => {
            //send json to server
            console.log(sub);
          });
        });
      }else{
        //subscription exists. update DB
      }
    });
  });
}*/



async function subscribe_pwa_notifications() {
  let sw = await navigator.serviceWorker.ready;
  let push = await sw.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: 'BDn9cGy9hKw7TONGWr0tNSd3KKkx1xBzBI6_IZ5fcPZYWwWj16GCseoj6CGjp6GiihPGqCmYMPrSNwxxWZFU2oA'
  });
  console.log(JSON.stringify(push));
}