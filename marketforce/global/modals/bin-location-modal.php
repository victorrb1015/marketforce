<div class="modal fade" role="dialog" tabindex="-1" id="binLocationModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h3 class="modal-title text-center">Update Bin Location</h3>
      </div>
      <div class="modal-body">
        <!--Content Here-->
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="button" onclick="">Save</button>
      </div>
    </div>
  </div>
</div>