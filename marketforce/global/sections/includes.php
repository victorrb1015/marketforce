<!-- Modals -->
<?php include 'global/modals/bin-location-modal.php'; ?>

<!-- JavaScript -->
	
    <!-- jQuery -->
    <script src="../skin2-assets/vendors/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../skin2-assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    
	<!-- Data table JavaScript -->
	<script src="../skin2-assets/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
	
	<!-- Slimscroll JavaScript -->
	<script src="../skin2-assets/dist/js/jquery.slimscroll.js"></script>
	
	<!-- simpleWeather JavaScript -->
	<script src="../skin2-assets/vendors/bower_components/moment/min/moment.min.js"></script>
	<script src="../skin2-assets/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
	<script src="../skin2-assets/dist/js/simpleweather-data.js"></script>
	
	<!-- Progressbar Animation JavaScript -->
	<script src="../skin2-assets/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="../skin2-assets/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>
	
	<!-- Fancy Dropdown JS -->
	<script src="../skin2-assets/dist/js/dropdown-bootstrap-extended.js"></script>
	
	<!-- Sparkline JavaScript -->
	<script src="../skin2-assets/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>
	
	<!-- Owl JavaScript -->
	<script src="../skin2-assets/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
	
	<!-- ChartJS JavaScript -->
	<script src="../skin2-assets/vendors/chart.js/Chart.min.js"></script>
	
	<!-- Morris Charts JavaScript -->
    <script src="../skin2-assets/vendors/bower_components/raphael/raphael.min.js"></script>
    <script src="../skin2-assets/vendors/bower_components/morris.js/morris.min.js"></script>
    <script src="../skin2-assets/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
	
	<!-- Switchery JavaScript -->
	<script src="../skin2-assets/vendors/bower_components/switchery/dist/switchery.min.js"></script>
	
	<!-- Init JavaScript -->
	<script src="../skin2-assets/dist/js/init.js"></script>
	<!--<script src="../skin2-assets/dist/js/dashboard-data.js"></script>-->

  <!-- jQuery DatePicker JS -->
<!--   <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script> -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.8/jquery.mask.min.js"></script> -->
  

  <!-- JSMask -->
  <!-- <script src="js/mask/jquery.mask.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.11.4/jquery.mask.min.js"></script>
  <script>
  	$(document).ready(function($){
      //$('.date').mask('00/00/0000');
      $('.number').mask('000,000', {reverse: true});
      $('.time').mask('00:00:00');
      $('.phone').mask('(000) 000-0000');
      $('.phone_us').mask('(000) 000-0000');
      $('.usd').mask('000,000,000,000,000.00', {reverse: true});
      $( ".date" ).datepicker({ showAnim: "fold" });
    });
  </script>
  <script>
  $(document).ready(function() {
      $(".custom-usd").keydown(function (e) {
          // Allow: backspace, delete, tab, escape, enter and .
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
               // Allow: Ctrl+A
              (e.keyCode == 65 && e.ctrlKey === true) || 
               // Allow: home, end, left, right
              (e.keyCode >= 35 && e.keyCode <= 39)) {
                   // let it happen, don't do anything
                   return;
          }
          // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            var v = $(this).val();
            if(e.keyCode != 190 || (e.keyCode == 190 && v.indexOf('.') > 0)){
              e.preventDefault();
            }  
          }
      });
      document.getElementById('preloader-it').style.display = 'none';
  });
  </script>

	<script src="global/js/global-functions.js"></script>
  <script src="global/js/inv-location-functions.js"></script>

<!--Bootstrap Tour Files-->
<link rel="stylesheet" type="text/css" href="global/tour/css/bootstrap-tour-standalone.min.css">
<script src="global/tour/js/bootstrap-tour-standalone.min.js"></script>

<!-- URL Update Function -->
<script>
function add_url_param(newParam,value){
  var params = new URLSearchParams(window.location.search);
  params.delete(newParam);
  params.append(newParam,value);
  var new_url = params.toString();
  console.log(new_url);
  console.log(params.entries());
  console.log(window.location.pathname+'?'+new_url);
  window.history.replaceState("", "Orders", window.location.pathname+'?'+new_url);
}
</script>


<!--<script src="global/tour/global-update-tour.js?cb=<?php echo $cache_buster; ?>"></script>-->
  