<!-- Top Menu Items -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="mobile-only-brand pull-left">
        <div class="nav-header pull-left">
            <div class="logo-wrap">
                <a href="index.php">
                    <?php
                    echo '<img class="brand-img" alt="brand" src="//' . $_SERVER['HTTP_HOST'] . '/assets/img/white-mf-logo.png" style="width:80%;">';
                    ?>
                </a>
            </div>
        </div>
        <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 mt-20 pull-left"
           href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
        <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view"
           href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
        <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
        <form id="search_form" action="search.php" method="post" role="search"
              class="top-nav-search collapse pull-left">
            <div class="input-group">
                <input type="text" id="msearch" name="msearch" class="form-control"
                       placeholder="Search Customer Name, Invoice#, Phone#">
                <span class="input-group-btn">
						<button type="button" class="btn  btn-default" data-target="#search_form" data-toggle="collapse"
                                aria-label="Close" aria-expanded="true"><i class="zmdi zmdi-search"></i></button>
						</span>
            </div>
        </form>
        <h4 id="tSales" class="inline-block pull-left ml-20 mt-20" style="color:green;font-weight:bold;"></h4>
    </div>
    <div id="mobile_only_nav" class="mobile-only-nav pull-right">
        <ul class="nav navbar-right top-nav pull-right">
            <?php
            switch ($_SESSION['org_id']) {
                case "162534": //North edge
                case "558558": // United
                case "615243": //Legacy
                case "329698": //Integrity
                case "987874": // AAB
                    break;
                default:
                    echo '
                    <li>
                <button type="button" class="btn btn-success btn-sm mt-20 ml-20" data-toggle="modal"
                        data-target="#newTask" onclick="clear_newTask_modal();">
                    <i class="zmdi zmdi-plus top-nav-icon"> New Task</i>
                </button>
            </li>
            <li>
                <button type="button" class="btn btn-success btn-sm mt-20 ml-20" onclick="update_bin_location();">
                    <i class="zmdi zmdi-my-location top-nav-icon"> Bin Location</i>
                </button>
            </li>
                    ';
            }
            ?>
            <li>
                <div type="button" id="google_translate_element" class="google " style="margin-top: 17px !important; margin-left: 20px !important;margin-right: 20px !important;">
                </div>
                <script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                <script type="text/javascript">
                    function googleTranslateElementInit() {
                        new google.translate.TranslateElement({
                            pageLanguage: 'en',
                            includedLanguages: 'en,es',
                            layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
                            gaTrack: true
                        }, 'google_translate_element');
                    }
                </script>
            </li>
            <!--<li>
                <a id="open_right_sidebar" class="mt-20" href="#"><i class="zmdi zmdi-settings top-nav-icon"></i></a>
            </li>-->
            <!--<li class="dropdown alert-drp">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-notifications top-nav-icon"></i><span class="top-nav-icon-badge">5</span></a>
                <ul  class="dropdown-menu alert-dropdown" data-dropdown-in="bounceIn" data-dropdown-out="bounceOut">
                    <li>
                        <div class="notification-box-head-wrap">
                            <span class="notification-box-head pull-left inline-block">notifications</span>
                            <a class="txt-danger pull-right clear-notifications inline-block" href="javascript:void(0)"> clear all </a>
                            <div class="clearfix"></div>
                            <hr class="light-grey-hr ma-0"/>
                        </div>
                    </li>
                    <li>
                        <div class="streamline message-nicescroll-bar">
                            <div class="sl-item">
                                <a href="javascript:void(0)">
                                    <div class="icon bg-green">
                                        <i class="zmdi zmdi-flag"></i>
                                    </div>
                                    <div class="sl-content">
                                        <span class="inline-block capitalize-font  pull-left truncate head-notifications">
                                        New subscription created</span>
                                        <span class="inline-block font-11  pull-right notifications-time">2pm</span>
                                        <div class="clearfix"></div>
                                        <p class="truncate">Your customer subscribed for the basic plan. The customer will pay $25 per month.</p>
                                    </div>
                                </a>
                            </div>
                            <hr class="light-grey-hr ma-0"/>
                            <div class="sl-item">
                                <a href="javascript:void(0)">
                                    <div class="icon bg-yellow">
                                        <i class="zmdi zmdi-trending-down"></i>
                                    </div>
                                    <div class="sl-content">
                                        <span class="inline-block capitalize-font  pull-left truncate head-notifications txt-warning">Server #2 not responding</span>
                                        <span class="inline-block font-11 pull-right notifications-time">1pm</span>
                                        <div class="clearfix"></div>
                                        <p class="truncate">Some technical error occurred needs to be resolved.</p>
                                    </div>
                                </a>
                            </div>
                            <hr class="light-grey-hr ma-0"/>
                            <div class="sl-item">
                                <a href="javascript:void(0)">
                                    <div class="icon bg-blue">
                                        <i class="zmdi zmdi-email"></i>
                                    </div>
                                    <div class="sl-content">
                                        <span class="inline-block capitalize-font  pull-left truncate head-notifications">2 new messages</span>
                                        <span class="inline-block font-11  pull-right notifications-time">4pm</span>
                                        <div class="clearfix"></div>
                                        <p class="truncate"> The last payment for your G Suite Basic subscription failed.</p>
                                    </div>
                                </a>
                            </div>
                            <hr class="light-grey-hr ma-0"/>
                            <div class="sl-item">
                                <a href="javascript:void(0)">
                                    <div class="sl-avatar">
                                        <img class="img-responsive" src="dist/img/avatar.jpg" alt="avatar"/>
                                    </div>
                                    <div class="sl-content">
                                        <span class="inline-block capitalize-font  pull-left truncate head-notifications">Sandy Doe</span>
                                        <span class="inline-block font-11  pull-right notifications-time">1pm</span>
                                        <div class="clearfix"></div>
                                        <p class="truncate">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                                    </div>
                                </a>
                            </div>
                            <hr class="light-grey-hr ma-0"/>
                            <div class="sl-item">
                                <a href="javascript:void(0)">
                                    <div class="icon bg-red">
                                        <i class="zmdi zmdi-storage"></i>
                                    </div>
                                    <div class="sl-content">
                                        <span class="inline-block capitalize-font  pull-left truncate head-notifications txt-danger">99% server space occupied.</span>
                                        <span class="inline-block font-11  pull-right notifications-time">1pm</span>
                                        <div class="clearfix"></div>
                                        <p class="truncate">consectetur, adipisci velit.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="notification-box-bottom-wrap">
                            <hr class="light-grey-hr ma-0"/>
                            <a class="block text-center read-all" href="javascript:void(0)"> read all </a>
                            <div class="clearfix"></div>
                        </div>
                    </li>
                </ul>
            </li>-->
            <li class="dropdown auth-drp">
                <a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown">
                    <i class="zmdi zmdi-account zmdi-hc-lg"></i><span> <?php echo $_SESSION['full_name']; ?></span>
                    <i class="zmdi zmdi-caret-down"></i>
                    <!--<img src="dist/img/user1.png" alt="user_auth" class="user-auth-img img-circle"/>-->
                    <!--<span class="user-online-status"></span>-->
                </a>
                <ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                    <li>
                        <a href="access.php"><i class="zmdi zmdi-accounts"></i><span>Manage Users</span></a>
                    </li>
                    <li>
                        <a href="settings.php"><i class="zmdi zmdi-settings"></i><span>My Account</span></a>
                    </li>
                    <?php
                    if ($_SESSION['user_id'] == '1') {
                        echo '<li>
                          <a href="knowledge-base.php"><i class="zmdi zmdi-graduation-cap"></i><span>Knowledge Base</span></a>
                        </li>';
                    }
                    ?>
                    <li>
                        <a href="../cron/labels.php" target="_blank"><i
                                    class="zmdi zmdi-labels"></i><span>Print Labels</span></a>
                    </li>
                    <!--<li class="divider"></li>
                    <li class="sub-menu show-on-hover">
                        <a href="#" class="dropdown-toggle pr-0 level-2-drp"><i class="zmdi zmdi-check text-success"></i> available</a>
                        <ul class="dropdown-menu open-left-side">
                            <li>
                                <a href="#"><i class="zmdi zmdi-check text-success"></i><span>available</span></a>
                            </li>
                            <li>
                                <a href="#"><i class="zmdi zmdi-circle-o text-warning"></i><span>busy</span></a>
                            </li>
                            <li>
                                <a href="#"><i class="zmdi zmdi-minus-circle-outline text-danger"></i><span>offline</span></a>
                            </li>
                        </ul>
                    </li>-->
                    <li class="divider"></li>
                    <li>
                        <a href="../index.php?logout=yes"><i class="zmdi zmdi-power"></i><span>Log Out</span></a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<!-- /Top Menu Items -->

<!-- Left Sidebar Menu -->
<?php include 'global/sections/left-sidebar.php'; ?>
<!-- /Left Sidebar Menu -->

<!-- Right Sidebar Menu -->
<?php //include 'global/sections/right-sidebar.php'; ?>
<!-- /Right Sidebar Menu -->


<?php
if (($_SESSION['admin'] == 'Yes' || $_SESSION['manager'] == 'Yes') && $_SESSION['org_id'] == '257823') {
    echo '<script>
	var getSalesStatus = true;
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      document.getElementById("tSales").innerHTML = "Current Day Sales: $"+r.salesToday;
      //set status based on response...
      
    }
  }
  xmlhttp.open("GET","apitest/daily-sales.php",true);
  xmlhttp.send();
  
  setInterval(function(){
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      console.log(this.responseText);
      var r = JSON.parse(this.responseText);
      document.getElementById("tSales").innerHTML = "Current Day Sales: $"+r.salesToday;
      
    }
  }
  xmlhttp.open("GET","apitest/daily-sales.php",true);
  xmlhttp.send();
  },10000);
</script>';
}
?>