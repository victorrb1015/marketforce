<div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
				<li class="navigation-header">
					<span>Navigation</span> 
					<i class="zmdi zmdi-more"></i>
				</li>
<!-------------------------------------------------------------------DASHBOARD-------------------------------------------------------------------------------->
				<?php
				if($_SESSION['in'] == 'Yes' && $_SESSION['position'] != 'Inventory Consignment User'){
			echo '<li>
					<a href="index.php">
						<div class="pull-left"><i class="fas fa-tachometer-alt mr-20"></i><span class="right-nav-text">Dashboard</span></div>';
						if($_SESSION['doc_approval'] == 'Yes'){
							$ndq = "SELECT * FROM `new_packets` WHERE `status` = 'Pending'";
						}else{
							$ndq = "SELECT * FROM `new_packets` WHERE `status` = 'Pending' AND `rep` = '" . $_SESSION['user_id'] . "'";
						}
						$ndg = mysqli_query($conn, $ndq);
						$ndn = mysqli_num_rows($ndg);
						if($ndn != 0){
						echo '<div class="pull-right"><span class="label label-danger">' . $ndn . '</span></div>';
						}
			echo '<div class="clearfix"></div>
					</a>
				  </li>';
				}
				?>

<!-------------------------------------------------------------------Accounting-------------------------------------------------------------------------------->
                  <?php
									
					if(($_SESSION['accountant'] == 'Yes' || $_SESSION['check_request'] == 'Yes') && $_SESSION['position'] != 'Inventory Consignment User'){	
						$crnum = 0;
									if($_SESSION['accountant'] == 'Yes'){
										$crq = "SELECT * FROM `check_requests` WHERE `status` != 'Completed' AND `status` != 'Cancelled'";
										//$crg = mysqli_query($conn,$crq) or die($conn->error);
										//$crnum = mysqli_num_rows($crg);
									}
					echo '<li>
							<a href="accounting.php">
							<div class="pull-left"><i class="fas fa-usd mr-20"></i><span class="right-nav-text">Accounting</span></div>';
						if($crnum > 0){
							echo '<div class="pull-right"><span class="label label-danger">' . $crnum . '</span></div>';
						}
						echo '<div class="clearfix"></div>
							</a>
						</li>';
					}
                  ?>
				
<!-------------------------------------------------------------------Collections-------------------------------------------------------------------------------->
                  <?php
									$collq = "SELECT * FROM `collections` WHERE `inactive` != 'Yes' AND `in_legal` != 'Yes' AND `collection` = 'Yes' AND `assigned_rep_id` = '" . $_SESSION['user_id'] . "' AND `last_activity` != CURRENT_DATE";
									$collg = mysqli_query($conn, $collq) or die($conn->error);
									$colln = mysqli_num_rows($collg);
									$legq = "SELECT * FROM `collections` WHERE `inactive` != 'Yes' AND `in_legal` = 'Yes' AND `collection` = 'Yes' AND `assigned_rep_id` = '" . $_SESSION['user_id'] . "' AND `last_activity` != CURRENT_DATE";
									$legg = mysqli_query($conn, $legq) or die($conn->error);
									$legn = mysqli_num_rows($legg);
									$tn = $colln + $legn;
									if($_SESSION['collections'] == 'Yes' && $_SESSION['org_id'] != '987874 ' && $_SESSION['position'] != 'Inventory Consignment User'){
										echo '<li>
														<a href="javascript:;" data-toggle="collapse" data-target="#collections"><div class="pull-left"><i class="fas fa-arrows-alt-v mr-20"></i><span class="right-nav-text">Collections <i class="fas fa-caret-down"></i></span></span></div>';
										if($colln != 0 || $legn != 0){
											echo '<div class="pull-right"><span class="label label-danger">' . $tn . '</span></div>';
										}
											echo '<div class="clearfix"></div>
														</a>
                        		<ul id="collections" class="collapse collapse-level-1 two-col-list">
															<li>
																<a href="collections.php"><i class="fa fa-usd fa-fw"></i> Collections List <div class="pull-right"><span class="label label-danger">' . $colln . '</span></div></a>
															</li>
															<li>
																<a href="collection-reports.php"><i class="fa fa-line-chart fa-fw"></i> Collection Reports</a>
															</li>';
										if($_SESSION['position'] == 'Developer'){
											echo '<li>
															<a href="display-collections.php"><i class="fa fa-home fa-fw"></i> Display Collections <div class="pull-right"><span class="label label-danger">' . $legn . '</span></div></a>
														</li>';
										}
										if($_SESSION['user_id'] == '1' || $_SESSION['user_id'] == '3' || $_SESSION['user_id'] == '50' || $_SESSION['user_id'] == '93'){
											echo '<li>
															<a href="legal.php"><i class="fa fa-gavel fa-fw"></i> Legal <div class="pull-right"><span class="label label-danger">' . $legn . '</span></div></a>
														</li>';
										}
											echo '</ul>
													</li>';
									}
                  ?>
				
<!-------------------------------------------------------------------Commission Form-------------------------------------------------------------------------------->
                  <?php
									if(($_SESSION['user_id'] == '3' || $_SESSION['user_id'] == '39' || $_SESSION['user_id'] =='1') && $_SESSION['org_id'] != '987874 ' && $_SESSION['position'] != 'Inventory Consignment User'){
                     echo '<li>
                              <a href="commission/entry-form.php" target="_blank"><i class="fas fa-file mr-20"></i><span class="right-nav-text">Commision</span></a>
                            </li>';
                    }
                  ?>
				
<!---------------------------------------------------------------------Communications-------------------------------------------------------------------------------->
									<?php
										if(($_SESSION['admin'] == 'Yes' || $_SESSION['send_survey'] == 'Yes') && $_SESSION['position'] != 'Inventory Consignment User'){
						  echo	'<li>
						 					<a href="javascript:;" data-toggle="collapse" data-target="#communications"><div class="pull-left"><i class="fas fa-arrows-alt-v mr-20"></i><span class="right-nav-text">Communications <i class="fas fa-caret-down"></i></span></div>
											<div class="clearfix"></div>
											</a>
                        <ul id="communications" class="collapse collapse-level-1 two-col-list">';
										}
							if($_SESSION['send_survey'] == 'Yes'&& $_SESSION['position'] != 'Inventory Consignment User'){
										echo '<li>
															<a href="send-survey.php"><i class="fas fa-paper-plane"></i> Send A Survey</a>
													</li>';
							}
							if($_SESSION['admin'] == 'Yes'&& $_SESSION['position'] != 'Inventory Consignment User'){
              //if($_SESSION['user_id'] == 1){
										echo '<li>
                    			    <a href="dealer-update.php"><i class=" fas fa-share-square"></i> Send Dealer Update</a>
                    			</li>
													<li>
                    			    <a href="osr-update.php"><i class="fas fa-share-square"></i> Send OSTR Update</a>
                    			</li>
													<li>
															<a href="notification.php"><i class="fas fa-share-square"></i> Create Notification</a>
													</li>';
							}
							if(($_SESSION['admin'] == 'Yes' || $_SESSION['send_survey'] == 'Yes') && $_SESSION['position'] != 'Inventory Consignment User'){
									echo '</ul>
									 </li>';
										}
                  ?>
				
<!-------------------------------------------------------------------Customer Search-------------------------------------------------------------------------------->
									<?php
										if($_SESSION['in'] == 'Yes' && $_SESSION['org_id'] != '987874 ' && $_SESSION['position'] != 'Inventory Consignment User'){
											echo '<li>
														<a href="search.php"><i class="fas fa-search mr-20"></i> Customer Search</a>
														</li>';
										}
                  ?> 
				
<!-------------------------------------------------------------------Dealer Displays-------------------------------------------------------------------------------->
                    <?php
                    if($_SESSION['doc_approval'] == 'Yes' || $_SESSION['position'] == 'Outside Sales Rep ' && $_SESSION['position'] != 'Inventory Consignment User'){
                    	echo '<li>
												<a href="dealer-displays.php"><i class="fas fa-home mr-20"></i> ';
                        switch($_SESSION['org_id']) {
                            case "832122":
                                echo "Customer ";
                                break;
                            case "123456":
                            case '532748':
                                echo "Client ";
                                break;
                            default:
                                echo 'Dealer ';
                        }
                        echo ' Displays</a>
										</li>';
                    }
                    ?>
				
<!-------------------------------------------------------------------Dealer Forms-------------------------------------------------------------------------------->
                    <?php
                  if($_SESSION['org_id'] != '987874 ' && $_SESSION['position'] != 'Inventory Consignment User'){
                    echo '<li>
												<a href="print-forms.php"><i class="fas fa-files-o mr-20"></i> ';
                      switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                          case '532748':
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }
                      echo ' Forms</a>
										</li>';
                  }
                  ?>
				
<!-------------------------------------------------------------------Dealer Lookup-------------------------------------------------------------------------------->
										<?php
                    if($_SESSION['org_id'] != '987874 ' && $_SESSION['position'] != 'Inventory Consignment User'){
                    echo '<li>
                      <a href="dealer-lookup.php"><i class="fas fa-search mr-20"></i> ';
                        switch($_SESSION['org_id']) {
                            case "832122":
                                echo "Customer ";
                                break;
                            case "123456":
                            case '532748':
                                echo "Client ";
                                break;
                            default:
                                echo 'Dealer ';
                        }
                        echo ' Lookup</a>
                    </li>';
                    }
                  ?>
				
<!-------------------------------------------------------------------Dealer Map-------------------------------------------------------------------------------->
									<?php
                  if($_SESSION['org_id'] != '987874 ' && $_SESSION['position'] != 'Inventory Consignment User'){
                    echo '<li>
												    <a href="map2.php"><i class="fas fa-globe mr-20"></i> ';
                      switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                          case '532748':
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }
                      echo ' Map</a>
									        </li>';
                  }
                  ?>
				
<!-------------------------------------------------------------------Employee Center-------------------------------------------------------------------------------->
										<?php
                    if($_SESSION['position'] != 'Inventory Consignment User'){
										echo '<li>
											<a href="employee-center.php">
												<i class="fas fa-users mr-20"></i> Employee Center ';
												
												if($_SESSION['EC'] == 'Yes' && $_SESSION['position'] != 'Inventory Consignment User'){
												$ecq = "SELECT * FROM `requests` WHERE `status` = 'Pending'";
												}else{
													$ecq = "SELECT * FROM `requests` WHERE `status` = 'Pending' AND `user_id` = '" . $_SESSION['user_id'] . "'";
												}
												$ecg = mysqli_query($conn, $ecq);
												$ecn = mysqli_num_rows($ecg);
												if($ecn == 0){
													$ecn = '';
												}else{
													echo '<div class="pull-right"><span class="label label-danger">' . $ecn . '</span></div>';
												}
				
											echo '</a>
													</li>';
                    }
												?>
				
<!-------------------------------------------------------------------Expense Reports-------------------------------------------------------------------------------->
                  <?php
										if($_SESSION['in'] == 'Yes ' && $_SESSION['position'] != 'Inventory Consignment User'){
											echo '<li>
                   				   <a href="expense-reports.php"><i class="fas fa-usd mr-20"></i> Expense Reports</a>
                   				 </li>';
										}
                    ?>

<!---------------------------------------------------------------------Inventory-------------------------------------------------------------------------------->
									<?php
										if($_SESSION['inventory'] == 'Yes' || $_SESSION['position'] == 'Inventory Consignment User'){
						  echo	'<li>
						 					<a href="javascript:;" data-toggle="collapse" data-target="#inventory"><div class="pull-left"><i class="fas fa-arrows-alt-v mr-20"></i><span class="right-nav-text">Inventory <i class="fas fa-caret-down"></i></span></div>
											<div class="clearfix"></div>
											</a>
                        <ul id="inventory" class="collapse collapse-level-1 two-col-list">';
										}
							if($_SESSION['inventory'] == 'Yes' || $_SESSION['position'] == 'Inventory Consignment User'){
										echo '<li>
															<a href="inventory.php"><i class="fas fa-boxes"></i> Items</a>
													</li>';
							}
							if($_SESSION['inventory'] == 'Yes' || $_SESSION['position'] == 'Inventory Consignment User'){
              //if($_SESSION['user_id'] == 1){
										echo '<li>
                    			    <a href="inventory-manufacturing.php"><i class=" fas fa-city"></i> Manufacturing</a>
                    			</li>
													<li>
                    			    <a href="inventory-shipping.php"><i class="fas fa-shipping-fast"></i>'; if($_SESSION['org_id'] == '257823'){echo 'Transfers ';}else{echo 'Shipping ';}
                                echo '</a>
                    			</li>
													<li>
															<a href="inventory-receiving.php"><i class="fas fa-dolly"></i> Receiving</a>
													</li>';
							}
							if($_SESSION['inventory'] == 'Yes' || $_SESSION['position'] == 'Inventory Consignment User'){
										echo '<li>
															<a href="inventory-reports.php"><i class="fas fa-clipboard-list"></i> Reports</a>
													</li>';
							}
							if($_SESSION['inventory'] == 'Yes' || $_SESSION['position'] == 'Inventory Consignment User'){
									echo '</ul>
									 </li>';
										}
                  ?>                    
				
<!-------------------------------------------------------------New Dealer Application-------------------------------------------------------------------------------->

										<?php
                  if($_SESSION['org_id'] != '987874 ' && $_SESSION['position'] != 'Inventory Consignment User'){
                    echo '<li>
												<a href="new-dealer.php"><i class="fas fa-file-text mr-20"></i> New ';
                      switch($_SESSION['org_id']) {
                          case "832122":
                              echo "Customer ";
                              break;
                          case "123456":
                          case '532748':
                              echo "Client ";
                              break;
                          default:
                              echo 'Dealer ';
                      }
                      echo '</a>
										</li>';
                  }
                  ?>
				
<!-------------------------------------------------------------------Orders-------------------------------------------------------------------------------->
                  <?php
                  /* if($_SESSION['orders'] == 'Yes' && $_SESSION['org_id'] != '987874 ' && $_SESSION['position'] != 'Inventory Consignment User'){
                    echo '<li>
                              <a href="orders.php"><i class="fas fa-shopping-cart mr-20"></i> Orders</a>
                          </li>';
                   } 
				   */
                  ?> 

				  <!------------------------------------------------------------------- PO Order ------------------------------------------------------------------------------->

                  <?php
                  if($_SESSION['org_id'] == '841963'){
                    
					if($_SESSION['full_name']=='training training'){
					echo '<li><a href="orders_v2.php"><i class="fa fa-file-powerpoint-o"></i> &nbsp &nbsp Orders</a></li>';
					}else{
					echo '<li><a href="orders_v2.php"><i class="fa fa-file-powerpoint-o"></i> &nbsp &nbsp PO Order</a></li>';
					}
                  }
                  ?>
				
<!-------------------------------------------------------------------Permits-------------------------------------------------------------------------------->
                  <?php
                   if($_SESSION['permits'] == 'Yes' || $_SESSION['position'] == 'Developer ' && $_SESSION['position'] != 'Inventory Consignment User'){
                    echo '<li>
                              <a href="permits.php"><i class="fas fa-file-powerpoint-o mr-20"></i> Permits</a>
                          </li>';
                   }
                  ?>
				
<!----------------------------------------------------------------------Petty Cash------------------------------------------------------------------------------------->
                  <?php
                   if($_SESSION['pc'] == 'Yes' || $_SESSION['position'] == 'Developer ' && $_SESSION['position'] != 'Inventory Consignment User'){
                    echo '<li>
                              <a href="petty-cash.php"><i class="fas fa-money mr-20"></i> Petty Cash</a>
                          </li>';
                   }
                  ?>  
				
<!-------------------------------------------------------------------Pre-Owned Listings-------------------------------------------------------------------------------->
                  <?php
					if($_SESSION['preowned'] == 'Yes' && $_SESSION['org_id'] == '257823 ' && $_SESSION['position'] != 'Inventory Consignment User'){
							echo '<li>
                        <a href="preowned-listings.php"><i class="fas fa-th-list mr-20"></i> Pre-Owned Listings</a>
                    </li>';
										}
									?>
				
<!---------------------------------------------------------------------Repairs---------------------------------------------------------------------------------------->
                  <?php
										if(($_SESSION['repairs'] == 'Yes' || $_SESSION['repairs'] == '') && $_SESSION['org_id'] != '987874 ' && $_SESSION['position'] != 'Inventory Consignment User'){
											$rq = "SELECT * FROM `repairs` WHERE `inactive` != 'Yes' AND `status` != 'Completed' AND `status` != 'Cancelled' AND `status` != 'Draft' AND `state` = '" . $_SESSION['myState'] . "' OR `state` = '" . $_SESSION['myState2'] . "' OR `state` = '" . $_SESSION['myState3'] . "' OR `state` = '" . $_SESSION['myState4'] . "'";
											$rg = mysqli_query($conn, $rq) or die($conn->error);
											$rep = mysqli_num_rows($rg);
											echo '<li>
																<a href="repairs.php"><i class="fas fa-wrench mr-20"></i> Repairs';
											if($rep == 0){
													$ecn = '';
												}else{
													echo '<div class="pull-right"><span class="label label-danger">' . $rep . '</span></div>';
												}
														echo '</a>
														</li>';
										}
                  ?>
				
<!------------------------------------------------------------------------Reports------------------------------------------------------------------------------------->
									<?php
                   if($_SESSION['position'] != 'Inventory Consignment User'){
                    echo '<li>
											<a href="reports.php"><i class="fas fa-area-chart mr-20"></i> Reports</a>
										</li>';
                   }
                  ?>
				
<!-------------------------------------------------------------------------REPOS---------------------------------------------------------------------------------------->
                  <?php
										if($_SESSION['repos'] == 'Yes' && $_SESSION['org_id'] != '987874 ' && $_SESSION['position'] != 'Inventory Consignment User'){
											$rq = "SELECT * FROM `repos` WHERE `status` != 'Completed' AND `status` != 'Cancelled' AND `state` = '" . $_SESSION['myState'] . "' OR `state` = '" . $_SESSION['myState2'] . "' OR `state` = '" . $_SESSION['myState3'] . "' OR `state` = '" . $_SESSION['myState4'] . "'";
											$rg = mysqli_query($conn, $rq) or die($conn->error);
											$rep = mysqli_num_rows($rg);
											echo '<li>
																<a href="repos.php"><i class="fas fa-refresh mr-20"></i> Repos';
											if($rep == 0){
													$ecn = '';
												}else{
													echo '<div class="pull-right"><span class="label label-danger">' . $rep . '</span></div>';
												}
														echo '</a>
														</li>';
										}
                  ?>
				
<!-------------------------------------------------------------------Sandbox-------------------------------------------------------------------------------->
                  <?php
                  if($_SESSION['position'] == 'Developer ' && $_SESSION['position'] != 'Inventory Consignment User'){
                  echo '<li>
                          <a href="sandbox.php"><i class="fas fa-code mr-20"></i> Sandbox</a>
                        </li>';
                  }
                  ?>
				
<!-------------------------------------------------------------------Scheduler-------------------------------------------------------------------------------->
									<?php
										if($_SESSION['in'] == 'Yes' && $_SESSION['org_id'] != '987874 ' && $_SESSION['position'] != 'Inventory Consignment User'){
											echo '<li>
														<a href="scheduling.php"><i class="fas fa-calendar mr-20"></i> Scheduler</a>
														</li>';
										}
                  ?>
				
<!-------------------------------------------------------------------Visit Notes-------------------------------------------------------------------------------->
									<?php
                  if($_SESSION['org_id'] != '987874 ' && $_SESSION['position'] != 'Inventory Consignment User'){
                    echo '<li>
                    <a href="notes.php"><i class="far fa-comments mr-20"></i> Visit Notes</a>
                  </li>';
                  }
                  ?>

                <li>
                    <a href="meeting-rooms.php"><i class="fas fa-handshake"></i> Room</a>
                </li>
				
<!-------------------------------------------------------------------Waivers-------------------------------------------------------------------------------->
									<?php
										if($_SESSION['in'] == 'Yes ' && $_SESSION['position'] != 'Inventory Consignment User'){
											echo '<li style="border-bottom:1px dashed white;">
                   				   <a href="waivers.php"><i class="fas fa-exclamation-triangle mr-20"></i> Waivers & Forms</a>
                   				 </li>';
										}
                  ?>
				
<!-------------------------------------------------------------------Make A Suggestion-------------------------------------------------------------------------------->
									<!--<li style="background:#3e3d3d;border:1px solid #222222;">
												<a href="suggestion.php"><i class="fas fa-lightbulb-o mr-20"></i> Make A Suggestion</a>
									</li>-->
				
 <!-------------------------------------------------------------------Report An Issue-------------------------------------------------------------------------------->
                  <li style="background:#3e3d3d;border:1px solid #222222;">
												<a href="support.php" target="_blank"><i class="fa fa-eye fa-fw"></i> View Open Tickets</a>
									</li>
        
<!-------------------------------------------------------------------Report An Issue-------------------------------------------------------------------------------->
                  <!--<li style="background:#3e3d3d;border:1px solid #222222;">
												<a href="https://ignitioninnovations.teamwork.com/support/" target="_blank"><i class="fas fa-ticket mr-20"></i> Help Desk Ticket</a>
									</li>-->
				
				
	</ul>
</div>