<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="description" content="Magilla is a Dashboard & Admin Site Responsive Template by hencework." />
<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Magilla Admin, Magillaadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
<meta name="author" content="hencework" />

<!-- Favicon -->
<link rel="shortcut icon" href="favicon.ico">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Morris Charts CSS -->
<link href="skin2-assets/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css" />

<!-- Data table CSS -->
<link href="skin2-assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

<link href="skin2-assets/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">

<!-- Custom CSS -->
<link href="skin2-assets/dist/css/style.css" rel="stylesheet" type="text/css">

<!--FontAwesome-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<!-- jQuery DatePicker CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" />

<!-- Jira Service Desk Widget -->
<!--<script data-jsd-embedded data-key="b6c6e098-9239-4e3f-bb00-bbd0e9aaa0a5" data-base-url="https://jsd-widget.atlassian.com" src="https://jsd-widget.atlassian.com/assets/embed.js"></script>-->

<!-- TeamWork Help Desk Widget -->
<script>
  !(function(e) {
    var basehref = "https://ignitioninnovations.teamwork.com",
      token = "489ae318-07b3-468f-aed7-46c491f7b9a0";

    window.deskcontactwidget = {};
    var r = e.getElementsByTagName("script")[0],
      c = e.createElement("script");
    c.type = "text/javascript", c.async = !0, c.src = basehref + "/support/v1/contact/main.js?token=" + token, r.parentNode.insertBefore(c, r), window.addEventListener("message", function(e) {
      var t = e.data[0],
        a = e.data[1];
      switch (t) {
        case "setContactFormHeight":
          document.getElementById("deskcontactwidgetframe").height = Math.min(a, window.window.innerHeight - 75)
      }
    }, !1);
  })(document);
</script>

<link href="global/css/date-picker-theme.css" rel="stylesheet" />

<!-- PWA Links -->
<meta name="theme-color" content="orange">
<link rel="manifest" href="manifest.json">
<link rel="apple-touch-icon" href="global/pwa/chrome-installprocess-128-128.png">
<script src="global/pwa/js/pwa.js?cb=<?php echo $cache_buster; ?>"></script>

