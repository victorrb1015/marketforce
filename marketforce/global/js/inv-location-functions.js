async function update_bin_location(){
  var bc = prompt('Please Scan Item\'s Barcode:');
  if(bc === false || bc === '' || bc === ' ' || bc === null){
    toast_alert('Invalid Barcode','Please Enter a valid barcode.','bottom-right','error');
    return;
  }
  var item = await lookup_item(bc);
  console.log('Item Details:');
  console.log(item);
  await update_item_location(item);
  console.log('Bin Location Update Complete');
}

function lookup_item(bc){
  return new Promise((resolve,reject) => {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          // Typical action to be performed when the document is ready:
          var r = JSON.parse(this.responseText);
          if(r.response === 'GOOD'){
            resolve(r);
          }else{
            //If error occurs...
            toast_alert('Something Went Wrong...',r.message,'bottom-right','error');
            reject(new Error(r.message));
          }
        }
    };
    xhttp.open("GET", "inventory/php/get-item-details.php?bc="+bc, true);
    xhttp.send();
  });
}


function update_item_location(item){
  return new Promise((resolve,reject) => {
    var loc;
    if(item.bin_location === ''){
      loc = 'Unknown';
    }else{
      loc = item.bin_location;
    }
    var bin = prompt('Item\'s current location is: '+loc+'\r\nPlease scan or enter the item\'s new location:');
    if(bin === false || bin === '' || bin === ' ' || bin === null){
      toast_alert('Something Went Wrong...','Please Enter a valid bin location.','bottom-right','error');
      //If error occurs...
      reject(new Error('Invalid bin location entered'));
      return;
    }
    var url = 'global/php/update-inv-bin-location.php';
    var params = 'pid='+item.ID+'&bin='+bin;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {//Call a function when the state changes.
        if(xhr.readyState == 4 && xhr.status == 200) {
          var r = JSON.parse(this.responseText);
          if(r.response === 'GOOD'){
            toast_alert('Success!','Item\'s Bin Location Updated Successfully...','bottom-right','success');
            resolve();
          }else{
            reject(new Error(r.message));
          }
          
        }
    }
    xhr.open('POST', url, true);
    //Send the proper header information along with the request
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send(params); 
    
  });
}