<?php
header('Content-Type: application/json');
error_reporting(0);
include '../../php/connection.php';

//Load Variables...
$pid = $_REQUEST['pid'];
$bin = $_REQUEST['bin'];

if($bin == ''){
  $x->response = 'ERROR';
  $x->message = 'No Bin Location Found';
  $response = json_encode($x,JSON_PRETTY_PRINT);
  echo $response;
  die();
}

#Main Functions...
$uq = "UPDATE `inventory_items` SET `bin_location` = '" . mysqli_real_escape_string($conn, $bin) . "' WHERE `ID` = '" . $pid . "'";

if(mysqli_query($conn, $uq)){
  $x->response = 'GOOD';
  $x->message = 'Item Bin Location Updated Sucessfully!';
}else{
  $x->response = 'Error';
  $x->message = $conn->error;
}


//Setup Response Output...
$response = json_encode($x,JSON_PRETTY_PRINT);
echo $response;