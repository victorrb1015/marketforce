//localStorage.clear();
//localStorage.removeItem("tour_current_step");
//localStorage.removeItem("tour_end");
// Instance the tour
var tour_template = `<div class="popover tour">
                    <div class="arrow" style="left: 50%;"></div>
                    <h3 class="popover-title" style="background:#b10058 !Important;text-align:center;"></h3>
                    <div class="popover-content"></div>
                    <div class="popover-navigation">
                      <div class="btn-group"> 
                        <button class="btn btn-sm btn-default disabled" data-role="prev" disabled="" tabindex="-1">« Prev</button> 
                        <button class="btn btn-sm btn-default" data-role="next">Next »</button> 
                      </div> 
                      <button class="btn btn-sm btn-default" data-role="end">Close</button>    
                    </div>
                  </div>`;

var tour = new Tour({
  steps: [
    {
      element: "",
      orphan: true,
      placement: "auto",
      title: "New MarketForce Scheduler!",
      content: `<p>MarketForce has been updated to version 3.13.0</p><br>
                <p>In this update, the Scheduler has been upgraded in the following ways:</p>
                <ol style="padding:5px;">
                  <li>Added Service Worker to improve Scheduler map load times</li>
                  <li>Removed options to Dispatch Routes, as this caused syncing issues with the Sales Portal</li>
                  <li>Upgraded to the new Application design</li>
                </ol>
               `,
      template: tour_template
    }
  ],
  backdrop: true
});

// Initialize the tour
tour.init();

// Start the tour
tour.start();