function view_det(){
  //alert('Good');
  var bc = document.getElementById('ebc').value;
  if (bc === ''){
    document.getElementById('response').innerHTML = 'Please Scan A Book...';
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      //alert(this.responseText);
      document.getElementById('response').innerHTML = this.responseText;
    }
  }
  xmlhttp.open("GET","php/book-details.php?bc="+bc,true);
  xmlhttp.send();
}


function update_det(){
  //alert("Test");
  var ID = document.getElementById('ID').value;
  var barcode = document.getElementById('barcode').value;
  var title = document.getElementById('title').value;
  var afn = document.getElementById('authorfname').value;
  var aln = document.getElementById('authorlname').value;
  var genre = document.getElementById('genre').value;
  var age = document.getElementById('age').value;
  var location = document.getElementById('location').value;
  var shelf = document.getElementById('shelf').value;
  var material = document.getElementById('material').value;
  var cost = document.getElementById('cost').value;
  var status = document.getElementById('status').value;
  var condition = document.getElementById('condition').value;
  
   if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      //alert(this.responseText);
      document.getElementById('response').innerHTML = this.responseText;
    }
  }
  xmlhttp.open("GET","php/book-details.php?s=1&ID="+ID+"&barcode="+barcode+"&title="+title+"&afn="+afn+"&aln="+aln+"&genre="+genre+"&age="+age+"&location="+location+"&shelf="+shelf+"&material="+material+"&cost="+cost+"&status="+status+"&condition="+condition,true);
  xmlhttp.send();
}