function in_out(x){
  //alert('Testing');//Turn on to test if function is found.
  
  
 //If this is an IN Query, Do This....
  if(x == 'in'){
    //alert('in');//Turn on to test this if statement
    
    //Load Values
    var bc = document.getElementById('bcin').value;
    var cond = document.getElementById('cond').value;
    //alert(cond);
     //Check for Return Condition
    if(cond == 'default'){
      document.getElementById('in_response').innerHTML='Please Select A Return Condition...';
      document.getElementById('in_star').innerHTML='*';
      return;
    }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      //alert(this.responseText);
      document.getElementById('in_response').innerHTML=this.responseText;
      document.getElementById('bcin').value='';
    }
  }
  xmlhttp.open("GET","php/checker.php?q=in&bc="+bc+"&cond="+cond,true);
  xmlhttp.send();
  }
  //End IN Query
  
  
  
 //If this is an Out Query, Do This...
  if(x == 'out'){
   // alert('out');//turn on to test this if statement
    
    //Load Values
    var bc = document.getElementById('bcout').value;
    var user = document.getElementById('sName').value;
    //Check for User input
    if(user == 'default'){
      document.getElementById('out_response').innerHTML='Please Select A User To Rent To...';
      document.getElementById('out_star').innerHTML='*';
      return;
    }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
      //alert(this.responseText);
      document.getElementById('out_response').innerHTML=this.responseText;
      document.getElementById('bcout').value='';
    }
  }
  xmlhttp.open("GET","php/checker.php?q=out&bc="+bc+"&user="+user,true);
  xmlhttp.send();
  }
  //End Out Query
  
  
}//END IN-OUT function


