<?php
include 'security/session/session-settings.php';

if(!isset($_SESSION['in'])){
	session_destroy();
	echo 'You do not have authentication for this site...<br>';
	echo '<script>
				setInterval(function(){
				window.location="http://marketforceapp.com";
				}, 2000);
				</script>';
	return;
}

include 'php/connection.php';

$pageName = 'Blank';//Set this equal to the name of the page you would like to set...
$pageIcon = 'fa fa-file';//Set this equal to the class name of the icon you would like to set as the page icon...

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Market Force | All Steel</title>
    <!--JQuery-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom JS -->
    <script src="js/new/viewed.js"></script>
  
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
    .badge:hover{
      cursor: pointer;
    }
		th, td{
			padding: 10px;
		}
		#dl, dc{
			height: 430px;
		}
		.panel-body{
			height: 382px;
		}
		#scroll{
			overflow: scroll;
		}
		::-webkit-scrollbar {
    width: 0px;  /* remove scrollbar space */
    background: transparent;  /* optional: just make scrollbar invisible */
}
/* optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: #FF0000;
}
		
		
		.packet-table td, th{
			border: 1px solid black;
		}
		.packet-table{
			margin:auto;
		}
  </style>
  
  <!-- CSKern CSS Code-->
  <style>
    /* DEFAULTS
----------------------------------------------------------*/

body
{
    background: #ffffff;
    font-size: .80em;
    font-family: "Helvetica Neue" , "Lucida Grande" , "Segoe UI" , Arial, Helvetica, Verdana, sans-serif;
    margin: 0px;
    padding: 0px;
    color: #696969;
}

a:link, a:visited
{
    color: #034af3;
}

a:hover
{
    color: #1d60ff;
    text-decoration: none;
}

a:active
{
    color: #034af3;
}

p
{
    margin-bottom: 10px;
    line-height: 1.6em;
}


/* HEADINGS   
----------------------------------------------------------*/

h1, h2, h3, h4, h5, h6
{
    font-size: 1.5em;
    color: #666666;
    font-variant: small-caps;
    text-transform: none;
    font-weight: bold;
}

h1
{
    font-size: 1.6em;
    padding-bottom: 0px;
    margin-bottom: 0px;
}

h2
{
    font-size: 1.5em;
    font-weight: 600;
}

h3
{
    font-size: 1.2em;
}

h4
{
    font-size: 1.1em;
}

h5, h6
{
    font-size: 1em;
}

/* this rule styles <h1> and <h2> tags that are the 
first child of the left and right table columns */
.rightColumn > h1, .rightColumn > h2, .leftColumn > h1, .leftColumn > h2
{
    margin-top: 0px;
}


/* PRIMARY LAYOUT ELEMENTS   
----------------------------------------------------------*/

.page
{
    width: 960px;
    background-color: #fff;
    margin: 20px auto 0px auto;
}

.header
{
    position: relative;
    margin: 0px;
    padding: 0px;
    background: #34557D;
    width: 100%;
    height: 265px;
}

.smheader
{
    position: relative;
    margin: 0px;
    padding: 0px;
    background: #380505;
    width: 100%;
    height: 50px;
}

.header h1
{
    font-weight: 700;
    margin: 0px;
    padding: 0px 0px 0px 20px;
    color: #f9f9f9;
    border: none;
    line-height: 2em;
    font-size: 2em;
}

.main
{
    padding: 0px 12px;
    margin: 12px 8px 0px 8px;
    min-height: 420px;
}

.leftCol
{
    padding: 6px 0px;
    margin: 12px 8px 8px 8px;
    width: 200px;
    min-height: 200px;
}

.footer
{
    color: #4e5766;
    background: #34557D;
    padding: 0px 0px 0px 0px;
    margin: 0px auto;
    height: 125px;
    text-align: center;
    line-height: normal;
}

.footerCopyright
{
    color: #ffffff;
}


/* TAB MENU   
----------------------------------------------------------*/

div.hideSkiplink
{
    background-color: #3a4f63;
    width: 100%;
}

div.menu
{
    padding: 4px 0px 4px 8px;
}

div.menu ul
{
    list-style: none;
    margin: 0px;
    padding: 0px;
    width: auto;
}

div.menu ul li a, div.menu ul li a:visited
{
    background-color: #465c71;
    border: 1px #4e667d solid;
    color: #dde4ec;
    display: block;
    line-height: 1.35em;
    padding: 4px 20px;
    text-decoration: none;
    white-space: nowrap;
}

div.menu ul li a:hover
{
    background-color: #bfcbd6;
    color: #465c71;
    text-decoration: none;
}

div.menu ul li a:active
{
    background-color: #465c71;
    color: #cfdbe6;
    text-decoration: none;
}

/* FORM ELEMENTS   
----------------------------------------------------------*/

fieldset
{
    margin: 1em 0px;
    padding: 1em;
    border: 1px solid #ccc;
}

fieldset p
{
    margin: 2px 12px 10px 10px;
}

fieldset.login label, fieldset.register label, fieldset.changePassword label
{
    display: block;
}

fieldset label.inline
{
    display: inline;
}

legend
{
    font-size: 1.1em;
    font-weight: 600;
    padding: 2px 4px 8px 4px;
}

input.textEntry
{
    width: 320px;
    border: 1px solid #ccc;
}

input.passwordEntry
{
    width: 320px;
    border: 1px solid #ccc;
}

div.accountInfo
{
    width: 42%;
}

/* MISC  
----------------------------------------------------------*/

.clear
{
    clear: both;
}

.title
{
    display: block;
    float: left;
    text-align: left;
    width: auto;
}

.loginDisplay
{
    font-size: 1.1em;
    display: block;
    text-align: right;
    padding: 10px;
    color: White;
}

.loginDisplay a:link
{
    color: white;
}

.loginDisplay a:visited
{
    color: white;
}

.loginDisplay a:hover
{
    color: white;
}

.failureNotification
{
    font-size: 1.2em;
    color: Red;
}

.bold
{
    font-weight: bold;
}

.submitButton
{
    text-align: right;
    padding-right: 10px;
}



/* Application Styles */

.divRow
{
    padding-bottom: 10px;
}

.titleArea
{
    padding-left: 25px;
}

.order-container
{
    width: 980px;
    background-color: #fff; /*  margin: 0px auto 0px auto;*/
}

.tblDescandPrice, .tbltd
{
    border: 1px solid black;
    border-collapse: collapse;
    padding: 6px;
}

.trDescAndPrice
{
    border: 1px;
}

.pnlAltSearch
{
    display: inline;
}

.rbl input[type="radio"]
{
    margin-left: 35px;
    margin-right: 5px;
}

.cbl input[type="checkbox"]
{
    margin-left: 10px;
    margin-right: 1px;
}

.itemCellPad
{
    padding-right: 15px !important;
}

.dField
{
    width: 410px;
}


.divPrintInfo
{
    visibility: collapse;
}

@media print
{
    .dField
    {
        border: 0px;
    }
}

@media print
{
    
    .chkPrnt
    {
        
       font-size: 14px; 
    }
    
    .dField2
    {
        border: 0px;
        font-size: 14px;
    }

    .dField3
    {
        border: 0px;
        text-decoration: underline;
        font-size: 14px;
    }

    .divPrintLegal2
    {
    }

    .divPrintLegal
    {
        font-size: 12px;
    }

    .cusInfoPrint
    {
        font-size: 15px;
    }
    
      .dField4
    {
        border: 0px;
        font-size: 14px;
        text-align:center;
    }
    
    .prntTitle
    {
      font-size:18px;   
        
    }


}


@media print
{
    .divNoPrint
    {
        visibility: hidden !important;
    }
}

@media print
{
    .header
    {
        display: none !important;
    }

    .panel
    {
        border: 0px;
        height: auto;
    }

    .panel-primary
    {
        border: 0px;
        height: auto;
    }

    .panel-body
    {
        border: 0px;
        height: auto;
    }
}


@page
{
    size: auto; /* auto is the initial value */
    margin: 0mm; /* this affects the margin in the printer settings */
}

html
{
    background-color: #FFFFFF;
    margin: 0px; /* this affects the margin on the html before sending to printer */
}

body
{
    margin: 0mm 0mm 0mm 0mm; /* margin you want for the content */
}
  </style>
  <script>
    

		
		
  </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
       <?php include 'nav.php'; ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <!-- Dashboard <small>Statistics Overview</small>-->
                          <img src="img/Market Force Logo 2.png" style="height: 70px; width: 175px;" ><small> <? echo $pageName; ?></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
														<li>
															<i class="<? echo $pageIcon; ?>"></i> <? echo $pageName; ?>
													</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              <!-- This is a notification -->
              <?php
              $notq = "SELECT * FROM `notifications` WHERE `user` = '" . $_SESSION['user_id'] . "' AND `viewed` = 'NO' ORDER BY `ID` ASC";
              $notget = mysqli_query($conn, $notq);
              while($notr = mysqli_fetch_array($notget)){
              echo '<div class="row">
							<!--Success->Green  Info->Blue  Warning->Yellow  Danger->Red-->
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" onclick="viewed(' . $notr['ID'] . ');" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>' . $notr['message'] . '</strong>
                        </div>
                    </div>
                </div>';
              }
              ?>
                <!-- /.row -->
              
							
		<!--This is where real content for the page will go-->
							
              <div class="panel panel-primary">
            <div class="panel-heading divNoPrint">
                Order Form</div>
            <div class="panel-body" id="divPrint">
                <div class="order-logo ">
                    <div style="width: 215px; float: left;">
                        <img src="../images/allsteelOrderLogo.png" alt="All Steel Carports" id="imgAllSteel" />
                    </div>
                    <div style="width: 310px; float: left;">
                        2200 North Granville Avenue Muncie, IN 47303 &nbsp; Phone: (765) 284-0694
                        <br />
                        Toll Free: (800) 730-7908 &nbsp; Fax: (765) 284-2689<br />
                        www.allsteelcarports.com
                    </div>
                    <div style="width: 410px; float: right;">
                        <strong>Date Created: </strong>
                        <span id="ContentPlaceHolder1_cntlManageOrder_lblDatCreated">5/11/2017 12:21:14 PM</span><br />
                        <strong>Sales Order #: </strong>
                        <span id="ContentPlaceHolder1_cntlManageOrder_lblOrderID">12307</span><br />
                        <strong>Date Installed: </strong>
                        <span id="ContentPlaceHolder1_cntlManageOrder_lblDateInstalled"></span>
                        <br />
                        <strong class="divNoPrint">Avalara Tax Doc Code:</strong><br />
                        <span id="ContentPlaceHolder1_cntlManageOrder_lblAvaDocCode" class="divNoPrint">0cd4d4d6-cbbd-4047-b4c2-e60cad8fb003</span>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
                <br />
                <div>
                    
                </div>
                <div class="divRow divNoPrint">
                    <strong style="float: left; padding-top: 3px;">Order Status:</strong>
                    <table id="ContentPlaceHolder1_cntlManageOrder_radOrderStatus" class="rbl" style="border-style:None;">
		<tr>
			<td><input id="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_0" type="radio" name="ctl00$ContentPlaceHolder1$cntlManageOrder$radOrderStatus" value="Quote" /><label for="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_0">Quote</label></td><td><input id="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_1" type="radio" name="ctl00$ContentPlaceHolder1$cntlManageOrder$radOrderStatus" value="In Shop" checked="checked" /><label for="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_1">In Shop</label></td><td><input id="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_2" type="radio" name="ctl00$ContentPlaceHolder1$cntlManageOrder$radOrderStatus" value="Installing" /><label for="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_2">Installing</label></td><td><input id="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_3" type="radio" name="ctl00$ContentPlaceHolder1$cntlManageOrder$radOrderStatus" value="Installed" /><label for="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_3">Installed</label></td><td><input id="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_4" type="radio" name="ctl00$ContentPlaceHolder1$cntlManageOrder$radOrderStatus" value="Canceled" /><label for="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_4">Canceled</label></td><td><input id="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_5" type="radio" name="ctl00$ContentPlaceHolder1$cntlManageOrder$radOrderStatus" value="On Hold" /><label for="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_5">On Hold</label></td><td><span class="aspNetDisabled"><input id="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_6" type="radio" name="ctl00$ContentPlaceHolder1$cntlManageOrder$radOrderStatus" value="Collections" disabled="disabled" /><label for="ContentPlaceHolder1_cntlManageOrder_radOrderStatus_6">Collections</label></span></td>
		</tr>
	</table>
                </div>
                <div style="width: 960px;">
                    <div style="width: 620px;">
                        <div class="divRow divNoPrint">
                            <strong>Dealer: </strong>
                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtDealer" type="text" value="All Steel Carports" id="ContentPlaceHolder1_cntlManageOrder_txtDealer" disabled="disabled" class="aspNetDisabled dField2" style="width:550px;" /></div>
                        <div class="divRow divNoPrint">
                            <strong>Dealer Phone:</strong>
                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtDealerPhone" type="text" value="765-284-0694" maxlength="50" id="ContentPlaceHolder1_cntlManageOrder_txtDealerPhone" disabled="disabled" tabindex="3" class="aspNetDisabled" style="width:500px;" />
                        </div>
                        <div class="divRow divNoPrint">
                            <strong>Customer Name: </strong>
                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtCusName" type="text" value="BENJAMIN WELDIN" id="ContentPlaceHolder1_cntlManageOrder_txtCusName" class="dField2" style="width:490px;" /></div>
                        <div class="divRow divNoPrint">
                            <strong>Customer Email: </strong>
                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtCustomerEmail" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtCustomerEmail" tabindex="3" style="width:490px;" />
                        </div>
                        <div class="divRow divNoPrint">
                            <strong>Address: </strong>
                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtAddress" type="text" value="5765 COUNTY RD P30" id="ContentPlaceHolder1_cntlManageOrder_txtAddress" class="dField2 divNoPrint" style="width:538px;" /></div>
                        <div class="divRow divNoPrint">
                            <strong>City:</strong>
                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtCity" type="text" value="FORT CALHOUN" id="ContentPlaceHolder1_cntlManageOrder_txtCity" class="dField2" style="width:160px;" />&nbsp;&nbsp;
                            <strong>State:</strong>
                            <select name="ctl00$ContentPlaceHolder1$cntlManageOrder$ddlState" id="ContentPlaceHolder1_cntlManageOrder_ddlState" class="dField2">
		<option value="Se">Select State</option>
		<option value="AL">Alabama</option>
		<option value="AK">Alaska</option>
		<option value="AZ">Arizona</option>
		<option value="AR">Arkansas</option>
		<option value="CA">California</option>
		<option value="CO">Colorado</option>
		<option value="CT">Connecticut</option>
		<option value="DE">Delaware</option>
		<option value="FL">Florida</option>
		<option value="GA">Georgia</option>
		<option value="HI">Hawaii</option>
		<option value="ID">Idaho</option>
		<option value="IL">Illinois</option>
		<option value="IN">Indiana</option>
		<option value="IA">Iowa</option>
		<option value="KS">Kansas</option>
		<option value="KY">Kentucky</option>
		<option value="LA">Lousiana</option>
		<option value="ME">Maine</option>
		<option value="MD">Maryland</option>
		<option value="MA">Massachusetts</option>
		<option value="MI">Michigan</option>
		<option value="MN">Minnesota</option>
		<option value="MS">Mississippi</option>
		<option value="MO">Missouri</option>
		<option value="MT">Montana</option>
		<option selected="selected" value="NE">Nebraska</option>
		<option value="NV">Nevada</option>
		<option value="NH">New Hampshire</option>
		<option value="NJ">New Jersy</option>
		<option value="NM">New Mexico</option>
		<option value="NY">New York</option>
		<option value="NC">North Carolina</option>
		<option value="ND">North Dakota</option>
		<option value="OH">Ohio</option>
		<option value="OK">Oklahoma</option>
		<option value="OR">Oregon</option>
		<option value="PA">Pennsylvania</option>
		<option value="RI">Rhode Island</option>
		<option value="SC">South Carolina</option>
		<option value="SD">South Dakota</option>
		<option value="TN">Tennessee</option>
		<option value="TX">Texas</option>
		<option value="UT">Utah</option>
		<option value="VT">Vermont</option>
		<option value="VA">Virginia</option>
		<option value="WA">Washington</option>
		<option value="WV">West Virginia</option>
		<option value="WI">Wisconsin</option>
		<option value="WY">Wyoming</option>
		<option value="DC">District of Columbia</option>

	</select>
                            &nbsp;&nbsp; <strong>Zip Code:</strong>
                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtZip" type="text" value="68023" maxlength="5" id="ContentPlaceHolder1_cntlManageOrder_txtZip" class="dField2" style="width:120px;" />
                        </div>
                        <div class="divNoPrint">
                            <strong>Cell Phone:</strong>
                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPhone1" type="text" value="402-536-0042" id="ContentPlaceHolder1_cntlManageOrder_txtPhone1" class="dField2" style="width:120px;" />
                            &nbsp; &nbsp; <strong>Phone 2:</strong>
                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPhone2" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtPhone2" class="dField2" style="width:120px;" />
                            &nbsp; &nbsp; <strong>Phone 3:</strong>
                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPhone3" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtPhone3" class="dField2" style="width:120px;" />
                        </div>
                        <br />
                    </div>
                    <div id="ContentPlaceHolder1_cntlManageOrder_updateOrder">
		
                            <div style="width: 624px; float: left; clear: both; padding-right: 15px;">
                                <table id="tbl" class="tblDescandPrice">
                                    <tr>
                                        <td class="tbltd">
                                            <strong>Description</strong>
                                        </td>
                                        <td class="tbltd">
                                            <table cellpadding="8" cellspacing="4" width="100%">
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Width</strong><br />
                                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtWidth" type="text" value="0.00" id="ContentPlaceHolder1_cntlManageOrder_txtWidth" tabindex="11" class="dField4" style="width:50px;" /><br />
                                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv1" style="color:Red;display:none;">must be numeric</span>
                                                    </td>
                                                    <td width="20%">
                                                        &nbsp; <strong>Roof</strong><br />
                                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtRoof" type="text" value="0.00" id="ContentPlaceHolder1_cntlManageOrder_txtRoof" tabindex="12" class="dField4" style="width:50px;" /><br />
                                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv2" style="color:Red;display:none;">must be numeric</span>
                                                    </td>
                                                    <td width="20%">
                                                        &nbsp; <strong>Frame</strong><br />
                                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtFrame" type="text" value="0.00" id="ContentPlaceHolder1_cntlManageOrder_txtFrame" tabindex="13" class="dField4" style="width:50px;" /><br />
                                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv3" style="color:Red;display:none;">must be numeric</span>
                                                    </td>
                                                    <td width="20%">
                                                        &nbsp; <strong>Leg </strong>
                                                        <br />
                                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtLeg" type="text" value="0.00" id="ContentPlaceHolder1_cntlManageOrder_txtLeg" tabindex="14" class="dField4" style="width:50px;" /><br />
                                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv4" style="color:Red;display:none;">must be numeric</span>
                                                    </td>
                                                    <td width="20%">
                                                        &nbsp;<strong>Gauge</strong><br />
                                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtGauge" type="text" value="0.00" id="ContentPlaceHolder1_cntlManageOrder_txtGauge" tabindex="15" class="dField4" style="width:50px;" /><br />
                                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv5" style="color:Red;display:none;">must be numeric</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="tbltd">
                                            <strong>Price</strong><br />
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice" type="text" value="0" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice" tabindex="16" class="dField2" style="width:60px;" />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv6" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            <strong>Options</strong>
                                        </td>
                                        <td class="tbltd">
                                            <table width="100%">
                                                <tr>
                                                    <td align="center">
                                                        <span><strong>Regular Frame</strong></span><br />
                                                        <span class="chkPrnt"><input id="ContentPlaceHolder1_cntlManageOrder_chkRegFrame" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkRegFrame" tabindex="17" /></span>
                                                    </td>
                                                    <td align="center">
                                                        <span><strong>A-Frame</strong></span><br />
                                                        <span class="chkPrnt"><input id="ContentPlaceHolder1_cntlManageOrder_chkAFrame" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkAFrame" tabindex="18" /></span>
                                                    </td>
                                                    <td align="center">
                                                        <span><strong>Vertical Roof</strong></span><br />
                                                        <span class="chkPrnt"><input id="ContentPlaceHolder1_cntlManageOrder_chkVerticleRoof" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkVerticleRoof" tabindex="19" /></span>
                                                    </td>
                                                    <td align="center">
                                                        <span><strong>All Vertical</strong></span><br />
                                                        <span class="chkPrnt"><input id="ContentPlaceHolder1_cntlManageOrder_chkAllVertical" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkAllVertical" tabindex="20" /></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice1" type="text" value="0" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice1\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice1" tabindex="20" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv7" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt2" type="text" value="***PICK UP IN OKLAHOMA***" id="ContentPlaceHolder1_cntlManageOrder_txtOpt2" tabindex="21" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice2" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice2\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice2" tabindex="22" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv8" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt3" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtOpt3" tabindex="23" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice3" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice3\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice3" tabindex="24" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv9" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt4" type="text" value="3 - 31&#39; SHEETS OF METAL" id="ContentPlaceHolder1_cntlManageOrder_txtOpt4" tabindex="25" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice4" type="text" value="360.00" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice4\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice4" tabindex="26" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv10" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt5" type="text" value="3- 30&#39; SHEETS OF METAL" id="ContentPlaceHolder1_cntlManageOrder_txtOpt5" tabindex="27" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice5" type="text" value="360.00" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice5\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice5" tabindex="28" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv11" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt6" type="text" value="3 - 20&#39; SHEETS OF METAL" id="ContentPlaceHolder1_cntlManageOrder_txtOpt6" tabindex="29" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice6" type="text" value="240.00" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice6\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice6" tabindex="30" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv12" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt7" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtOpt7" tabindex="31" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice7" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice7\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice7" tabindex="32" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv13" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt8" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtOpt8" tabindex="33" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice8" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice8\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice8" tabindex="34" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv14" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            &nbsp;
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOpt9" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtOpt9" tabindex="35" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice9" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPrice9\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPrice9" tabindex="36" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv15" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            <strong>Color / Top</strong>
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtColorTop" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtColorTop" tabindex="37" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceColorTop" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceColorTop\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPriceColorTop" tabindex="38" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv16" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            <strong>Sides</strong>
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtSides" type="text" value="WHITE" id="ContentPlaceHolder1_cntlManageOrder_txtSides" tabindex="39" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceSides" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceSides\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPriceSides" tabindex="40" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv17" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            <strong>Ends</strong>
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtEnds" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtEnds" tabindex="41" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceEnds" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceEnds\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPriceEnds" tabindex="42" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv18" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            <strong>Trim</strong>
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTrim" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtTrim" tabindex="43" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceTrim" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceTrim\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPriceTrim" tabindex="44" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv19" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbltd">
                                            <strong>NON TAXABLE FEES:</strong>
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtNonTaxFees" type="text" value="PICK UP DISCOUNT OF 10% - OK OFFICE " id="ContentPlaceHolder1_cntlManageOrder_txtNonTaxFees" tabindex="45" class="dField" />
                                        </td>
                                        <td class="tbltd">
                                            <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceNonTaxFees" type="text" value="-96.00" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtPriceNonTaxFees\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtPriceNonTaxFees" tabindex="46" class="dField2" style="width:60px;" /><br />
                                            <span id="ContentPlaceHolder1_cntlManageOrder_cv20" style="color:Red;display:none;">must be numeric</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--Tax Api Section-->
                            <div style="width: 320px; float: left;">
                                <div class="divRow" style="text-align: center; font-size: 15px;">
                                    <strong class="prntTitle">Installation Site</strong>
                                </div>
                                <div style="color: Red; text-align: center;" class="divNoPrint divRow">
                                    * Required for tax compulation
                                </div>
                                <div class="divNoPrint divRow">
                                    <strong>Same as Above</strong>&nbsp;<span style="font-weight:bold;"><input id="ContentPlaceHolder1_cntlManageOrder_chkSameAddress" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkSameAddress" onclick="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$chkSameAddress\&#39;,\&#39;\&#39;)&#39;, 0)" tabindex="47" /></span>
                                </div>
                                <div class="divRow divApiPrint">
                                    <strong>Address: </strong>
                                    <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtAddressApi" type="text" value="5765 COUNTY RD P30" id="ContentPlaceHolder1_cntlManageOrder_txtAddressApi" tabindex="48" class="dField3" style="width:250px;" />
                                    <span id="ContentPlaceHolder1_cntlManageOrder_reqAddressApi" style="color:Red;display:none;">Required</span>
                                </div>
                                <div class="divRow divApiPrint">
                                    <strong>City: </strong>
                                    <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtCityApi" type="text" value="FORT CALHOUN" id="ContentPlaceHolder1_cntlManageOrder_txtCityApi" tabindex="49" class="dField3" style="width:280px;" />
                                    <span id="ContentPlaceHolder1_cntlManageOrder_reqCityApi" style="color:Red;display:none;">Required</span></div>
                                <div class="divRow divApiPrint ">
                                    <strong>State: </strong>
                                    <select name="ctl00$ContentPlaceHolder1$cntlManageOrder$ddlStateApi" id="ContentPlaceHolder1_cntlManageOrder_ddlStateApi" tabindex="50" class="dField3" style="width:270px;">
			<option value="Se">Select State</option>
			<option value="AL">Alabama</option>
			<option value="AK">Alaska</option>
			<option value="AZ">Arizona</option>
			<option value="AR">Arkansas</option>
			<option value="CA">California</option>
			<option value="CO">Colorado</option>
			<option value="CT">Connecticut</option>
			<option value="DE">Delaware</option>
			<option value="FL">Florida</option>
			<option value="GA">Georgia</option>
			<option value="HI">Hawaii</option>
			<option value="ID">Idaho</option>
			<option value="IL">Illinois</option>
			<option value="IN">Indiana</option>
			<option value="IA">Iowa</option>
			<option value="KS">Kansas</option>
			<option value="KY">Kentucky</option>
			<option value="LA">Lousiana</option>
			<option value="ME">Maine</option>
			<option value="MD">Maryland</option>
			<option value="MA">Massachusetts</option>
			<option value="MI">Michigan</option>
			<option value="MN">Minnesota</option>
			<option value="MS">Mississippi</option>
			<option value="MO">Missouri</option>
			<option value="MT">Montana</option>
			<option selected="selected" value="NE">Nebraska</option>
			<option value="NV">Nevada</option>
			<option value="NH">New Hampshire</option>
			<option value="NJ">New Jersy</option>
			<option value="NM">New Mexico</option>
			<option value="NY">New York</option>
			<option value="NC">North Carolina</option>
			<option value="ND">North Dakota</option>
			<option value="OH">Ohio</option>
			<option value="OK">Oklahoma</option>
			<option value="OR">Oregon</option>
			<option value="PA">Pennsylvania</option>
			<option value="RI">Rhode Island</option>
			<option value="SC">South Carolina</option>
			<option value="SD">South Dakota</option>
			<option value="TN">Tennessee</option>
			<option value="TX">Texas</option>
			<option value="UT">Utah</option>
			<option value="VT">Vermont</option>
			<option value="VA">Virginia</option>
			<option value="WA">Washington</option>
			<option value="WV">West Virginia</option>
			<option value="WI">Wisconsin</option>
			<option value="WY">Wyoming</option>
			<option value="DC">District of Columbia</option>

		</select>
                                    <span id="ContentPlaceHolder1_cntlManageOrder_reqddlStateApi" style="color:Red;display:none;">Required</span>
                                </div>
                                <div class="divRow divApiPrint">
                                    <strong>Zip Code: </strong>
                                    <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtZipApi" type="text" value="68023" maxlength="5" id="ContentPlaceHolder1_cntlManageOrder_txtZipApi" tabindex="51" class="dField3" style="width:244px;" />
                                    <span id="ContentPlaceHolder1_cntlManageOrder_reqZipApi" style="color:Red;display:none;">Required</span>
                                </div>
                                <div class="divRow divApiPrint">
                                    <strong>Tax Exempt #</strong>
                                    <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTaxExemptAPI" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtTaxExemptAPI\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtTaxExemptAPI" tabindex="52" class="dField3" style="width:220px;" />
                                </div>
                                <div class="divRow divApiPrint">
                                    <strong>Total Tax: </strong>
                                    <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTaxRateApi" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtTaxRateApi" disabled="disabled" tabindex="53" class="aspNetDisabled dField3" style="width:244px;" /></div>
                                <input type="button" name="ctl00$ContentPlaceHolder1$cntlManageOrder$btnGetTaxRate" value="Get Tax" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$cntlManageOrder$btnGetTaxRate&quot;, &quot;&quot;, true, &quot;api&quot;, &quot;&quot;, false, true))" id="ContentPlaceHolder1_cntlManageOrder_btnGetTaxRate" tabindex="54" class="btn btn-primary divNoPrint" /><br />
                                
                                <div style="padding-top: 10px;">
                                    <div class="divRow divApiPrint">
                                        <strong>Total Sale: $ </strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTotalSale" type="text" value="960.00" id="ContentPlaceHolder1_cntlManageOrder_txtTotalSale" disabled="disabled" tabindex="55" class="aspNetDisabled dField3" style="width:230px;" /></div>
                                    <div class="divRow divApiPrint">
                                        <strong>Tax: $</strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTax" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtTax" disabled="disabled" tabindex="56" class="aspNetDisabled dField3" style="width:270px;" /></div>
                                    <div class="divRow divApiPrint">
                                        <strong>Tax Exempt #: </strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTaxExempt" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtTaxExempt\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtTaxExempt" tabindex="57" class="dField3" style="width:220px;" /></div>
                                    <div class="divRow divApiPrint">
                                        <strong>Total: $</strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtTotal" type="text" value="864.00" id="ContentPlaceHolder1_cntlManageOrder_txtTotal" disabled="disabled" tabindex="58" class="aspNetDisabled dField3" style="width:261px;" /></div>
                                    <div class="divRow divApiPrint">
                                        <strong>10% Deposit: $</strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtDeposit10pct" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtDeposit10pct\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtDeposit10pct" tabindex="59" class="dField3" style="width:211px;" /><br />
                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv21" style="color:Red;display:none;">must be numeric</span>
                                    </div>
                                    <div class="divRow divApiPrint">
                                        <strong>50% Deposit: $</strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtDeposit50pct" type="text" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$cntlManageOrder$txtDeposit50pct\&#39;,\&#39;\&#39;)&#39;, 0)" onkeypress="if (WebForm_TextBoxKeyHandler(event) == false) return false;" id="ContentPlaceHolder1_cntlManageOrder_txtDeposit50pct" tabindex="60" class="dField3" style="width:211px;" />
                                        <span id="ContentPlaceHolder1_cntlManageOrder_cv22" style="color:Red;display:none;">must be numeric</span>
                                    </div>
                                    <div class="divRow divApiPrint">
                                        <strong>Balance: $</strong>
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtBalance" type="text" value="864.00" id="ContentPlaceHolder1_cntlManageOrder_txtBalance" disabled="disabled" tabindex="61" class="aspNetDisabled dField3" style="width:240px;" /></div>
                                </div>
                            </div>
                            <div id="ContentPlaceHolder1_cntlManageOrder_updateProgressApiCallEdit" style="display:none;">
			
                                    <img src="../images/spinner.gif" alt="" />
                                
		</div>
                            <!--End Tax Api Section-->
                        
	</div>
                    <div style="clear: both;">
                    </div>
                    <div style="padding-top: 10px;" class="divPrintLegal2">
                        <div class="divRow" id="divSpecs" style="float: left; clear: both; width: 624px;
                            padding-right: 15px;">
                            <table border="1" style="width: 600px;">
                                <tr>
                                    <td style="padding: 3px;">
                                        <strong>Installation</strong>
                                    </td>
                                    <td style="padding: 3px;">
                                        <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkGround" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkGround" tabindex="62" /><label for="ContentPlaceHolder1_cntlManageOrder_chkGround">Ground</label></span>
                                        &nbsp;&nbsp;
                                        <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkCement" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkCement" tabindex="63" /><label for="ContentPlaceHolder1_cntlManageOrder_chkCement">Cement</label></span>
                                        &nbsp;&nbsp;
                                        <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkAsphalt" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkAsphalt" tabindex="64" /><label for="ContentPlaceHolder1_cntlManageOrder_chkAsphalt">Asphalt</label></span>
                                        &nbsp;&nbsp;
                                        <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkOther" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkOther" tabindex="65" /><label for="ContentPlaceHolder1_cntlManageOrder_chkOther">Other</label></span>
                                        &nbsp;&nbsp;
                                        <input name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtOther" type="text" id="ContentPlaceHolder1_cntlManageOrder_txtOther" class="dField2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 2px;">
                                        <strong>Land Level</strong>
                                    </td>
                                    <td style="padding: 2px;">
                                        <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkLandLevelY" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkLandLevelY" tabindex="66" /><label for="ContentPlaceHolder1_cntlManageOrder_chkLandLevelY">Yes</label></span>
                                        &nbsp;&nbsp;
                                        <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkLandLevelN" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkLandLevelN" tabindex="67" /><label for="ContentPlaceHolder1_cntlManageOrder_chkLandLevelN">No</label></span>
                                        &nbsp;&nbsp; (Building will be installed "as is")
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 2px;">
                                        <strong>Electricity</strong>
                                    </td>
                                    <td style="padding: 2px;">
                                        <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkElecYes" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkElecYes" tabindex="68" /><label for="ContentPlaceHolder1_cntlManageOrder_chkElecYes">Yes</label></span>
                                        &nbsp;&nbsp;
                                        <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkElecNo" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkElecNo" tabindex="69" /><label for="ContentPlaceHolder1_cntlManageOrder_chkElecNo">No</label></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 2px;">
                                        <strong>Payment Method</strong>
                                    </td>
                                    <td style="padding: 2px;">
                                        <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkCash" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkCash" tabindex="70" /><label for="ContentPlaceHolder1_cntlManageOrder_chkCash">Cash</label></span>
                                        &nbsp;&nbsp;
                                        <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkCheck" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkCheck" tabindex="71" /><label for="ContentPlaceHolder1_cntlManageOrder_chkCheck">Check</label></span>
                                        &nbsp;&nbsp;
                                        <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkCC" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkCC" tabindex="72" /><label for="ContentPlaceHolder1_cntlManageOrder_chkCC">C.C.</label></span>
                                        &nbsp;&nbsp;
                                        <span style="font-weight:normal;"><input id="ContentPlaceHolder1_cntlManageOrder_chkPO" type="checkbox" name="ctl00$ContentPlaceHolder1$cntlManageOrder$chkPO" tabindex="73" /><label for="ContentPlaceHolder1_cntlManageOrder_chkPO">P.O.</label></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="float: left; width: 320px;">
                            <strong>Special Instructions: </strong>
                            <br />
                            <textarea name="ctl00$ContentPlaceHolder1$cntlManageOrder$txtInstructions" rows="4" cols="42" id="ContentPlaceHolder1_cntlManageOrder_txtInstructions" tabindex="74" class="dField2">
TIFFANY SPOKE WITH JAMES AND CUSTOMER IS PAYING WITH A  P.O. 
</textarea>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="divPrintLegal">
                        <h4 style="font-weight: bold;">
                            Things You Should Know....</h4>
                        <p class="divPrintLegal">
                            <strong>All Steel Carports, Inc., (hereinafter "ASC"). is not responsible for permits,
                                covenanant searches or restrictions.</strong> Please contact your local Building
                            Inspector or Homeowners Association for Information. <strong>Lot must be level</strong>
                            or unit will be installed <strong>"AS IS"</strong> on lot. Prior to Installation,
                            please have any underground cables, gas lines or any other utility lines located
                            and marked. ASC will not be responsible for any damage to underground utility lines.
                            <strong>Installation is subject to change due to contractor availability and weather
                                conditions. A down payment of 10% (before tax) is required on all orders and 50%
                                on special orders. Any credit card refunds will be charged 5%. Balance must be paid
                                in full upon installation.</strong> No refunds on special orders. Deposits are
                            non-refundable. A $50 return trip fee (subcontractor) applies. <strong>Additional fees
                                may vary upon jobs (cuts, ground leveling, or anything additional the contractors
                                have to do extra on site).</strong> A fee of $35 will apply on returned checks.
                            <strong>All Steel Carports will not be held liable for any property damage don by self-employed
                                contractors.</strong>
                        </p>
                        <p class="divPrintLegal">
                            Buyer agrees that the balance shall be due and payable at the time of installation.
                            In the event that balances due and owing at the time of installation are not paid
                            in full, buyer shall be in default under this agreement. ASC may elect to repossess
                            the carport/garage and buyer hereby consent to allow ASC access to the carport/garage
                            to repossess the carport/garage, or at its sole discretion ASC may assess interest
                            at a rate of 18% per annum on any unpaid balance. Buyer agrees that in the event
                            of any default under this agreement, buyer shall be responsible for reasonable collection
                            agency costs, any attorney's fees and costs incurred as a result of the default.
                            <strong>JURISDICTION,</strong> it is expressly agreed that in any dispute, suit,
                            claim or legal proceeding of any nature arising from the obligations under this
                            agreement, shall be filed in a court of competent jurisdiction in Delaware County,
                            Indiana and be controlled by the law of the State of Indiana. ASC reserves the right
                            to terminate this agreement at any time.
                        </p>
                        <div style="text-align: center;" class="divPrintLegal">
                            <strong>I have read and completely understand the above information and give my approval
                                for construction of the above.</strong><br />
                        </div>
                        <p>
                            <strong>Buyer:</strong>____________________________________________ &nbsp; &nbsp;
                            Date:_________<br />
                            <strong>Contractor Name:</strong>__________________________________ &nbsp; &nbsp;
                            Date:__________
                        </p>
                    </div>
              </div>
                </div>
              </div>

              
              
              
							
							
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!--<script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>-->

	

 <?php include 'footer.html'; ?>
</body>

</html>