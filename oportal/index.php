<?php
//include 'security/session/session-settings.php';
include 'global/php/connection.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>MarketForce | All Steel</title>
    <?php include 'global/sections/head.php'; ?>
</head>

<body>
	<!-- Preloader -->
	<?php include 'global/sections/preloader.php'; ?>
	<!-- /Preloader -->
    <div class="wrapper theme-4-active pimary-color-red">

    	<!--Navigation-->
    	<?php include 'global/sections/nav.php'; ?>
		
		
        <!-- Main Content -->
		<div class="page-wrapper"><!--Includes Footer-->

            <div class="container-fluid pt-25"><!--Main Content Here-->
				


              <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-primary card-view">
                    <div class="panel-heading">
                      <h1 class="panel-title">Order Tracking Display Table &nbsp; 
                        <a href="orders/invoice/invoice.php?mode=New&org_id=<?php echo $_SESSION['org_id']; ?>">
                          <button type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> New Order</button>
                        </a>
                      </h1>
                    </div>
                    <div class="panel-body table-responsive" style="height:auto;">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr class="info">
                                    <th style="font-size:18px;">Date</th>
                                    <th style="font-size:18px;">User</th>
                                    <th style="font-size:18px;">Customer</th>
                                    <th style="font-size: 18px;width: 175px;">Order Info</th>
                                    <th style="font-size: 18px;">Status</th>
                                    <th style="font-size:18px;">Notes</th>
                                    <th style="font-size: 18px;width: 216px;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                        $oq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `status` != 'invoiced' AND `status` != 'paid'";
                        $og = mysqli_query($conn, $oq) or die($conn->error);
                        while($or = mysqli_fetch_array($og)){
                          echo '<tr>
                                    <td>' . date("m/d/y",strtotime($or['date'])) . '</td>
                                    <td>' . $or['rep_name'] . '</td>
                                    <td>' . $or['cname'] . '</td>
                                    <td>';
                          if($or['inv'] != ''){
                              echo 'Inv#: ' . $or['inv'] . '&nbsp;&nbsp;&nbsp;';
                          }
                          if($or['file_url'] != '' && $or['status'] != 'Estimate'){
                                echo '<br><br>
                                      <a href="' . $or['file_url'] . '" target="_blank">
                                        <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View Work Order</button>
                                      </a>';
                          }
                          if($or['inv_file_url'] != '' && $or['status'] == 'Estimate'){
                                echo '<br><br>
                                      <a href="' . $or['inv_file_url'] . '" target="_blank">
                                        <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View Estimate</button>
                                      </a>';
                          }else if($or['inv_file_url'] != '' && $or['status'] != 'Estimate'){
                            echo '<br><br>
                                      <a href="' . $or['inv_file_url'] . '" target="_blank">
                                        <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View Invoice</button>
                                      </a>';
                          }
                          if($or['bol_file_url'] != '' && $or['status'] != 'Estimate'){
                                echo '<br><br>
                                      <a href="' . $or['bol_file_url'] . '" target="_blank">
                                        <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View Bill of Lading</button>
                                      </a>';
                          }
                          
                              echo '</td>
                                    <td>
                                      <div class="note-box" style="max-height:200px;width:500px;margin:auto;">
                                        <div class="row bs-wizard" style="border-bottom:0;text-align:center;">';
                          
                  if($or['status'] == 'Estimate'){
                    echo '<div class="col-12 alert alert-warning" style="height:100px;background:pink;color:#000;font-weight:bold;font-size:4rem;">
                            ESTIMATE
                          </div>';
                  }else{
                          if($or['rec_order'] == 'Yes'){
                            $rec_order = 'complete';
                            $rec_order_date = date("m/d/y", strtotime($or['rec_order_date']));
                          }else{
                            $rec_order = 'disabled';
                            $rec_order_date = '';
                          }
                                    
                                    echo '<div class="col-xs-2 bs-wizard-step ' . $rec_order . '">
                                            <div class="text-center bs-wizard-stepnum">Rec. Order</div>
                                            <div class="progress"><div class="progress-bar"></div></div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            <div class="bs-wizard-info text-center">' . $rec_order_date . '</div>
                                          </div>';
                          
                          if($or['in_shop'] == 'Yes'){
                            $in_shop = 'complete';
                            $in_shop_date = date("m/d/y", strtotime($or['in_shop_date']));
                          }else{
                            $in_shop = 'disabled';
                            $in_shop_date = '';
                          }
                                    
                                    echo '<div class="col-xs-2 bs-wizard-step ' . $in_shop . '"><!-- complete -->
                                            <div class="text-center bs-wizard-stepnum">In-Shop</div>
                                            <div class="progress"><div class="progress-bar"></div></div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            <div class="bs-wizard-info text-center">' . $in_shop_date . '</div>
                                          </div>';
                          
                          if($or['shop_done'] == 'Yes'){
                            $shop_done = 'complete';
                            $shop_done_date = date("m/d/y", strtotime($or['shop_done_date']));
                          }else{
                            $shop_done = 'disabled';
                            $shop_done_date = '';
                          }
                                    
                                    echo '<div class="col-xs-2 bs-wizard-step ' . $shop_done . '"><!-- complete -->
                                            <div class="text-center bs-wizard-stepnum">Shop-Done</div>
                                            <div class="progress"><div class="progress-bar"></div></div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            <div class="bs-wizard-info text-center">' . $shop_done_date . '</div>
                                          </div>';
                          
                          if($or['shipped'] == 'Yes'){
                            $shipped = 'complete';
                            $shipped_date = date("m/d/y", strtotime($or['shipped_date']));
                          }else{
                            $shipped = 'disabled';
                            $shipped_date = '';
                          }
                                    
                                    echo '<div class="col-xs-2 bs-wizard-step ' . $shipped . '"><!-- active -->
                                            <div class="text-center bs-wizard-stepnum">Shipped</div>
                                            <div class="progress"><div class="progress-bar"></div></div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            <div class="bs-wizard-info text-center">' . $shipped_date . '</div>
                                          </div>';
                          
                          if($or['invoiced'] == 'Yes'){
                            $invoiced = 'complete';
                            $invoiced_date = date("m/d/y", strtotime($or['invoiced_date']));
                          }else{
                            $invoiced = 'disabled';
                            $invoiced_date = '';
                          }
                                    
                                    echo '<div class="col-xs-2 bs-wizard-step ' . $invoiced . '"><!-- active -->
                                            <div class="text-center bs-wizard-stepnum">Invoiced</div>
                                            <div class="progress"><div class="progress-bar"></div></div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            <div class="bs-wizard-info text-center">' . $invoiced_date . '</div>
                                          </div>';
                          
                          if($or['paid'] == 'Yes'){
                            $paid = 'complete';
                            $paid_date = date("m/d/y", strtotime($or['paid_date']));
                            $paid_time = date("h:i A", strtotime($or['paid_time']));
                          }else{
                            $paid = 'disabled';
                            $paid_date = '';
                            $paid_time = '';
                          }
                                          
                                    echo '<div class="col-xs-2 bs-wizard-step ' . $paid . '"><!-- active -->
                                            <div class="text-center bs-wizard-stepnum">Paid</div>
                                            <div class="progress"><div class="progress-bar"></div></div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            <div class="bs-wizard-info text-center"data-toggle="tooltip" data-placement="bottom" title="' . $paid_time . '">' . $paid_date . '</div>
                                          </div>';
                                          
                  }//END If Estimate...
                    
                                  echo '</div>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="note-box" style="max-height:200px;width:400px;margin:auto;overflow:scroll;">';
                                      if($or['notes'] != ''){
                                        echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
                                                <i class="fa fa-comments-o"></i> Initial Notes ' . date("m/d/y",strtotime($or['date'])) . ' ' . date("h:i A",strtotime($or['time'])) . '
                                              </span></h4>
                                                <div class="well well-sm">
                                                ' . $or['notes'] . '
                                              </div>';
                                      }
                   $onq = "SELECT * FROM `order_notes` WHERE `inactive` != 'Yes' AND `order_id` = '" . $or['ID'] . "' ORDER BY `ID` DESC";
                   $ong = mysqli_query($conn, $onq) or die($conn->error);
                   while($onr = mysqli_fetch_array($ong)){
														echo '<h4 style="margin-bottom:0px;"><span class="label label-primary">
														<i class="fa fa-comments-o"></i> ' . $onr['rep_name'] . ' ' . date("m/d/y",strtotime($onr['date'])) . ' ' . date("h:i A",strtotime($onr['time'])) . '
													</span></h4>
														<div class="well well-sm">
														' . $onr['note'] . '
													</div>';
													}
                              echo '</div>
                                    </td>
                                    <td style="color:rgb(213,19,7);">
                                      <button class="btn btn-primary btn-sm" type="button" style="margin:2px;" data-toggle="modal" data-target="#addNoteModal" onclick="add_note(' . $or['ID'] . ');"><i class="fa fa-comments-o"></i> Add Note</button>
                                      <!--<button class="btn btn-default btn-sm" type="button" style="margin:2px;" data-toggle="modal" data-target="#newOrderModal" onclick="get_order_details(' . $or['ID'] . ');"><i class="fa fa-pencil"></i> Edit</button>-->
                                      <a href="orders/invoice/invoice.php?org_id=' . $_SESSION['org_id'] . '&mode=Edit&inv=' . $or['inv'] . '"><button class="btn btn-default btn-sm" type="button" style="margin:2px;""><i class="fa fa-pencil"></i> Edit</button></a>';
                              if($or['status'] == 'Estimate'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'rec_order\');"><i class="fa fa-file"></i> Make Invoice</button>';
                              } 
                              if($or['status'] == 'rec_order'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'in_shop\');"><i class="fa fa-wrench"></i> Send To Shop</button>';
                              }
                              if($or['status'] == 'in_shop'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'shop_done\');"><i class="fa fa-wrench"></i> <i class="fa fa-check"></i> Shop Done</button>';
                              }
                              if($or['status'] == 'shop_done'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'shipped\');"><i class="fa fa-truck"></i> Ship Order</button>';
                              }
                              if($or['status'] == 'shipped'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'invoiced\');"><i class="fa fa-usd"></i> Invoice</button>';
                              }
                              if($or['status'] == 'invoiced'){
                                echo '<button class="btn btn-success btn-sm" type="button" style="margin:2px;" onclick="change_status(' . $or['ID'] . ',\'paid\');"><i class="fa fa-check-square-o"></i> Mark Paid</button>';
                              }
                              
                              if($or['status'] != 'Estimate'){
                                echo '<button class="btn btn-warning btn-sm" type="button" style="margin:2px;" data-toggle="modal" data-target="#infoBox" onclick="view_order_info(' . $or['ID'] . ');"><i class="fa fa-info-circle"></i> Info</button>';
                              }
                          
                              if($or['status'] == 'Estimate'){
                                echo '<button class="btn btn-warning btn-sm" type="button" style="margin:2px;color:#000;background:pink;" onclick="send_customer_estimate(' . $or['inv'] . ');"><i class="fa fa-envelope"></i> Send EST</button>';
                              }else{
                                echo '<button class="btn btn-warning btn-sm" type="button" style="margin:2px;color:white;background:black;" onclick="send_customer_invoice(' . $or['inv'] . ');"><i class="fa fa-envelope"></i> Send INV</button>';
                              }
                          
                                echo '<button class="btn btn-danger btn-sm" type="button" style="margin:2px;" onclick="delete_order(' . $or['ID'] . ');"><i class="fa fa-trash"></i> Delete</button>
                                    </td>
                                </tr>';
                              }
                         ?>
                             
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>

				
	
			
			</div>
			
			
			<!-- Footer -->
			<?php include 'global/sections/footer.php'; ?>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!--Footer-->
	<?php include 'global/sections/includes.php'; ?>
</body>

</html>