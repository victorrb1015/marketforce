<?php
//include 'global/security/session-settings.php';
include 'global/php/connection.php';
include 'global/security/validation-check.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Customer Order Portal | MarketForce</title>
    <?php include 'global/sections/head.php'; ?>
</head>

<body>
	<!-- Preloader -->
	<?php include 'global/sections/preloader.php'; ?>
	<!-- /Preloader -->
    <div class="wrapper theme-4-active pimary-color-red">

    	<!--Navigation-->
    	<?php include 'global/sections/nav.php'; ?>
		
		
        <!-- Main Content -->
		<div class="page-wrapper"><!--Includes Footer-->

            <div class="container-fluid pt-25"><!--Main Content Here-->


				<div class=row>
		     	<!--Documents Table Here-->
		     	<?php include 'documents/sections/documents-table.php'; ?>
		    </div>

				<div class=row>
		     	<!--Documents Table Here-->
		     	<?php include 'documents/sections/marketing-materials-table.php'; ?>
		    </div>
	
			
			</div>
			
			
			<!-- Footer -->
			<?php include 'global/sections/footer.php'; ?>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!--Footer-->
	<?php include 'global/sections/includes.php'; ?>
  <script src="documents/js/upload.js"></script>
</body>

</html>