function toast_alert(title,mess,pos,mode){
  $.toast({
		heading: title,
		text: mess,
		position: pos,
		loaderBg:'#f2b701',
		icon: mode,//success, error, info, warning...
		hideAfter: 3500, 
		stack: 6
	});
}