<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="MarketForce is a Dashboard & Admin Site for all Thriving Businesses." />
	<meta name="keywords" content="admin, admin dashboard, cms, crm, MarketForce Admin, MarketForceadmin, premium admin, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="Ignition Innovations"/>
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="../landing-assets/images/favicon.ico">
	<link rel="icon" href="../landing-assets/images/favicon.ico" type="image/x-icon">

	<!-- Morris Charts CSS -->
  <link href="../skin2-assets/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
	
	<!-- Data table CSS -->
	<link href="../skin2-assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
	<link href="../skin2-assets/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
		
	<!-- Custom CSS -->
	<link href="../skin2-assets/dist/css/style.css" rel="stylesheet" type="text/css">

	<!--FontAwesome-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!--Bootstrap Tour Files-->
  <link rel="stylesheet" type="text/css" href="global/tour/css/bootstrap-tour-standalone.min.css">
  <script src="global/tour/js/bootstrap-tour-standalone.min.js"></script>

<?php
$cache_buster = uniqid();
?>