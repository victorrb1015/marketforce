<div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
				<li class="navigation-header">
					<span>Main</span> 
					<i class="zmdi zmdi-more"></i>
				</li>
<!-------------------------------------------------------------------ORDERS-------------------------------------------------------------------------------->
				<?php
			echo '<li>
					  <a href="orders.php">
						<div class="pull-left"><i class="fa fa-barcode mr-20"></i><span class="right-nav-text">My Orders</span></div>';
			echo '<div class="clearfix"></div>
					  </a>
				    </li>';
				?>

<!-------------------------------------------------------------------Documents-------------------------------------------------------------------------------->
        
        <?php
        if($_SESSION['op_affiliate'] == 'Yes'){
					echo '<li>
							  <a href="documents.php">
							  <div class="pull-left"><i class="fa fa-file-text mr-20"></i><span class="right-nav-text">My Documents</span></div>';
					echo '<div class="clearfix"></div>
							  </a>
						    </li>';
        }
         ?>
				
							</ul>
						</li>
					</ul>
				</li>
			</ul>
		</div>