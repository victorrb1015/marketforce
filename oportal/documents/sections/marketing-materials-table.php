            <div class="col-md-12">
                  <div class="panel card-view">
                    <div class="panel-heading">
                    <h1 class="panel-title">Marketing Materials</h1>
                   </div>
								<div class="panel-body">
									<div class="table-wrap mt-40">
										<div class="table-responsive">
											<table class="table mb-0 table-bordered table-hover table-striped">
												<thead>
												  <tr>
													<th>Name</th>
													<th>Description</th>
													<th>Actions</th>
												  </tr>
												</thead>
												<tbody>
												  
                    <?php
                    $tq = "SELECT * FROM `portal_documents` WHERE `cid` = 'ALL'";
                    $tg = mysqli_query($pconn, $tq) or die($pconn->error);
                    while($tr = mysqli_fetch_array($tg)){
                          
                          echo '<tr>
													<td>' . $tr['doc_name'] . '</td>
													<td>' . $tr['doc_description'] . '</td>
													<td>
                            <a href="' . $tr['doc_url'] . '" target="_blank">
                              <button type="button" class="btn btn-primary btn-sm">
                                <i class="fa fa-eye"></i> View
                              </button>
                            </a>
                          </td>
												  </tr>';
                      
                    }
                    ?>
												  <!--<tr>
													<td>Credit Line Agreement</td>
													<td>Description Goes Here...</td>
													<td>
                            <span class="label label-warning">View</span> 
                            <span class="label label-success">Upload</span></td>
												  </tr>-->
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
            </div>