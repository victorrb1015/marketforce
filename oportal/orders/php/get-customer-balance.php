<?php
include '../../global/php/connection.php';

//Load Variables...
$cid = $_GET['cid'];

$cnq = "SELECT * FROM `order_customers` WHERE `ID` = '" . $cid . "'";
$cng = mysqli_query($conn, $cnq) or die($conn->error);
$cnr = mysqli_fetch_array($cng);
$cname = $cnr['company_name'];

//Get Balance Details...
$cq = "SELECT * FROM `order_credit_lines` WHERE `inactive` != 'Yes' AND `cid` = '" . $cid . "'";
$cg = mysqli_query($conn, $cq) or die($conn->error);
$cr = mysqli_fetch_array($cg);

if(mysqli_num_rows($cg) <= 0){
  $x->response = 'GOOD';
  $x->has_credit = 'No';
}else{
  $x->response = 'GOOD';
  $x->has_credit = 'Yes';
  $x->credit_amount = number_format(str_replace(',','',$cr['credit_amount']),2);
  
  $total_balance = 0;
  $inv_total_balance = 0;
  $est_total_balance = 0;
  $oq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `paid` != 'Yes' AND `status` != 'Quote' AND `cname` = '" . $cname . "'";
  $og = mysqli_query($conn, $oq) or die($conn->error);
  while($or = mysqli_fetch_array($og)){
    $iq = "SELECT * FROM `order_invoices` WHERE `inv_num` = '" . $or['inv'] . "'";
    $ig = mysqli_query($conn, $iq) or die($conn->error);
    $ir = mysqli_fetch_array($ig);
    //if($ir['status'] == 'Estimate'){
      //Do nothing because this does not add to balance owed...
    //}else{
      //echo 'Item: ' . round($ir['freight'],2) . '<br>';
      $total_balance = $total_balance + $ir['freight'];
      if($or['invoiced'] == 'Yes'){
        $inv_total_balance = $inv_total_balance + $ir['freight'];
      }else{
        $est_total_balance = $est_total_balance + $ir['freight'];
      }
      //echo 'Balance: ' . $total_balance . '<br>';
      //$taxRate = ($ir['tax_rate'] / 100) + 1;
      $taxRate = str_replace('.','',$ir['tax_rate']);
      $taxRate = '.0' . $taxRate;
      $taxRate = $taxRate + 1;
      //echo $taxRate . '<br>';
      $bq = "SELECT * FROM `order_invoice_items` WHERE `inactive` != 'Yes' AND `inv_num` = '" . $ir['inv_num'] . "'";
      $bg = mysqli_query($conn, $bq) or die($conn->error);
      while($br = mysqli_fetch_array($bg)){
        //echo 'Item: ' . ((int)$br['item_price'] * $taxRate) . '<br>';
        $total_balance = ($total_balance + $br['item_price'] * $taxRate);
        if($or['invoiced'] == 'Yes'){
          $inv_total_balance = ($inv_total_balance + $br['item_price'] * $taxRate);
        }else{
          $est_total_balance = ($est_total_balance + $br['item_price'] * $taxRate);
        }
        //echo 'Balance: ' . $total_balance . '<br>';
      }
    //}
  }
  
  $x->est_total_balance = number_format($est_total_balance,2);
  $x->inv_total_balance = number_format($inv_total_balance,2);
  $x->current_balance = number_format($total_balance,2);
  $x->credit_available = number_format((str_replace(',','',$cr['credit_amount']) - $total_balance),2);
}

$response = json_encode($x);
echo $response;

?>