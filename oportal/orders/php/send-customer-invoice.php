<?php
error_reporting(0);
include '../../global/php/connection.php';

//Load Variables...
$invNum = $_REQUEST['inv'];
$cemail = $_REQUEST['customer_email'];

//Get Customer...
$q = "SELECT * FROM `order_invoices` WHERE `inv_num` = '" . $invNum . "'";
$g = mysqli_query($conn, $q) or die($conn->error);
$r = mysqli_fetch_array($g);
$cid = $r['cid'];
$cname = $r['cname'];
$companyName = $cname;

$invLink = 'http://' . $_SERVER['HTTP_HOST'] . '/marketforce/orders/invoice/invoice.php?org_id=' . $_SESSION['op_org_id'] . '&mode=APrint&inv=' . $invNum;

//Email Settings...
include '../../../phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include '../../global/php/phpmailsettings.php';

$mail->setFrom('no-reply@marketforceapp.com', $companyName);
$mail->addAddress($cemail);
$mail->addBCC('archive@ignition-innovations.com');
$mail->Subject = 'Your Order Invoice!';

include '../emails/send-customer-invoice-affiliate.php';
$mail->Body = $sendInvoiceEmail;

if($mail->send()){
  $x->response = 'GOOD';
  $x->message = 'Invoice ' . $invNum . ' was sent to ' . $cemail;
  $x->error = 'Email Error: ' . $mail->ErrorInfo;
  //Add Note to DB...
  $nq = "INSERT INTO `order_notes`
        (
        `date`,
        `time`,
        `order_id`,
        `rep_id`,
        `rep_name`,
        `cname`,
        `note`,
        `inactive`
        )
        VALUES
        (
        CURRENT_DATE,
        CURRENT_TIME,
        '" . $ID . "',
        '" . $rep_id . "',
        '" . $rep_name . "',
        '" . $cname . "',
        'Invoice was emailed to the customer at " . $cemail . ".',
        'No'
        )";
  //mysqli_query($conn, $nq) or die($conn->error);
}else{
  $x->response = 'ERROR';
  $x->message = 'AN ERROR OCCURED!';
  $x->error = 'Email Error: ' . $mail->ErrorInfo;
}
$response = json_encode($x);
echo $response;