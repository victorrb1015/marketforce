 <div id="orders-table" class="col-md-12">
                  <div class="panel card-view">
                    <div class="panel-heading">
                    <h1 class="panel-title">My Orders &nbsp;
                      <a href="../marketforce/orders/invoice/invoice.php?mode=<?php echo $iMode; ?>New&org_id=<?php echo $_SESSION['op_org_id']; ?>&cid=<?php echo $_SESSION['op_cid']; ?>">
                        <button type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> New Order</button>
                      </a>
                    </h1>

                    </div>
                    <div class="panel-body table-responsive" style="height:auto;">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr class="info">
                                    <th>Date</th>
                                    <th>Submitted By</th>
                                    <th>Customer</th>
                                    <th>Status</th>
                                    <!--<th>Notes</th>-->
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                        $oq = "SELECT * FROM `orders` WHERE `inactive` != 'Yes' AND `status` = 'Quote' ORDER BY `ID` DESC";
                        $og = mysqli_query($conn, $oq) or die($conn->error);
                        while($or = mysqli_fetch_array($og)){
                          $coq = "SELECT * FROM `order_invoices` WHERE `inv_num` = '" . $or['inv'] . "'";
                          $cog = mysqli_query($conn, $coq) or die($conn->error);
                          $cor = mysqli_fetch_array($cog);
                          if($cor['cid'] != $_SESSION['op_cid']){
                            continue;
                          }else{
                          echo '<tr>
                                    <td>' . date("m/d/y",strtotime($or['date'])) . '</td>
                                    <td>' . $or['rep_name'] . '</td>
                                    <td>' . $or['cname'] . '</td>';
                          
                              echo '<td>
                                      <div class="note-box" style="max-height:200px;width:500px;margin:auto;">
                                        <div class="row bs-wizard" style="border-bottom:0;text-align:center;">';
                          
                                   echo '<div class="col-12 alert alert-warning" style="height:100px;background:#B22D58;color:#000;font-weight:bold;font-size:4rem;">
                                           Saved Quote
                                         </div>';
                                          
                                  echo '</div>
                                      </div>
                                    </td>';
                            
                              
                            
                                echo '<td>';
                          if($or['inv'] != ''){
                              echo 'Inv#: ' . $or['inv'] . '&nbsp;&nbsp;&nbsp;';
                              //echo '<br><br><button type="button" class="btn btn-success btn-xs"><i class="fa fa-paper-plane"></i> Submit As Order</button>';
                          }
                          
                          if($or['inv_file_url'] != ''){
                                echo '<br><br>
                                      <a href="../marketforce/orders/invoice/invoice.php?org_id=' . $_SESSION['op_org_id'] . '&mode=' . $iMode . 'Edit&cid=' . $_SESSION['op_cid'] . '&inv=' . $or['inv'] . '">
                                        <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View/Edit Quote</button>
                                      </a>';
                          }
                            echo '<br><br>';
                            echo '<button type="button" id="rm-button" class="btn btn-danger btn-xs" onclick="remove_order(' . $or['ID'] . ',this);"><i class="fas fa-trash"></i> Remove Quote</button>';
                          
                              echo '</td>';
                          echo '</tr>';
                          }
                              }
                         ?>
                             
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>