<?php
$clq = "SELECT * FROM `order_credit_lines` WHERE `cid` = '" . $_SESSION['op_cid'] . "'";
$clg = mysqli_query($conn, $clq) or die($conn->error);
if(mysqli_num_rows($clg) > 0){
  $clr = mysqli_fetch_array($clg);
  
  $credit_amount = str_replace(',','',$clr['credit_amount']);
  $credit_used = 5000;
  $credit_available = (intval($credit_amount) - intval($credit_used));
  $percentage = ($credit_used / $credit_amount) * 100;
  
  echo '<div id="financial-section" class="col-lg-12">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">Total Credit Line: $<span id="credit_amount">0</span></h6>
										<h6 class="panel-title txt-dark">Total Pending: $<span id="est_amount">0</span></h6>
										<h6 class="panel-title txt-dark">Total Invoiced: $<span id="inv_amount">0</span></h6>
										<h6 class="panel-title txt-dark">Total Credit Balance: $<span id="current_balance">0</span></h6>
										<h6 class="panel-title txt-dark">Total Credit Available: $<span id="credit_available">0</span></h6>
										<br>
										<h6 class="panel-title txt-dark">Total Balance Due: $<span id="balance_due">0</span></h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="progress progress-lg">
											<div class="progress-bar progress-bar-danger" id="current_balance_bar" style="width: 0%;" role="progressbar">$0</div>
										</div>
									</div>
								</div>
							</div>
						</div>';
}
?>