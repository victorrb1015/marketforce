function get_balance(cid){
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        if(r.has_credit === 'Yes'){
          
          document.getElementById('credit_amount').innerHTML = r.credit_amount;
					document.getElementById('est_amount').innerHTML = r.est_total_balance;
					document.getElementById('inv_amount').innerHTML = r.inv_total_balance;
          document.getElementById('current_balance').innerHTML = r.current_balance;
          document.getElementById('credit_available').innerHTML = r.credit_available;
					document.getElementById('balance_due').innerHTML = r.inv_total_balance;
          var p = (Number(r.current_balance.replace(',','')) / Number(r.credit_amount.replace(',',''))) * 100;
          console.warn('Percentage: '+p);
          document.getElementById('current_balance_bar').style.width = p+'%';
          document.getElementById('current_balance_bar').innerHTML = '$'+r.current_balance;
          
        }else{
          //Do Nothing...
          document.getElementById('financial-section').remove();
        }
        console.log('Credit Line Retrieved...Available Credit: $'+r.credit_available);
      }else{
        console.warn('ERROR: Customer balance was not retrieved...');
      }
      
    }
  }
  xmlhttp.open("GET","orders/php/get-customer-balance.php?cid="+cid,true);
  xmlhttp.send();
}