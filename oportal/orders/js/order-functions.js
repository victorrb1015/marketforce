function remove_order(oid,elem){
  var conf = confirm('Are you sure you want to remove this qoute?');
  if(conf === false){
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        
        $(elem).closest('tr').remove();
        console.log('Order '+oid+' Removed...');
        
      }else{
        
        console.warn('ERROR: Customer balance was not retrieved...');
        
      }
      
    }
  }
  xmlhttp.open("GET","orders/php/remove-order.php?oid="+oid,true);
  xmlhttp.send();
}