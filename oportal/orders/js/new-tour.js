var temp = "<div class='popover tour' style='background:#212121;padding:5px;'>"+
    "<div class='arrow'></div>"+
    "<h3 class='popover-title' style='background:#B10058;border-radius:.5rem;'></h3>"+
    "<div class='popover-content' style='color:#FFFFFF;'></div>"+
    "<div class='popover-navigation'>"+
        "<button class='btn btn-primary' data-role='prev'>« Prev</button>"+
        "&nbsp;<span data-role='separator'>|</span>&nbsp;"+
        "<button class='btn btn-primary' data-role='next'>Next »</button>"+
    "</div>"+
    "<button class='btn btn-info' data-role='end' style='float:right;'>End tour</button>"+
  "</div>";

//localStorage.clear();
// Instance the tour
var tour = new Tour({
  steps: [
    {
      element: ".logo-wrap",
      //placement: "bottom",
      title: "Welcome to the MarketForce Customer Portal!",
      content: "Follow this guide for a brief, step-by-step guide to this application!"
    },
    {
      element: ".fixed-sidebar-left",
      title: "Site Navigation Panel",
      content: "This is the Navigation Panel for the site. You will be able to access all of the pages using this navigation panel."
    },
    {
      element: "#financial-section",
      placement: "bottom",
      title: "Financial Overview",
      content: "This is where you can view your current credit line, credit used, and credit available."
    },
    {
      element: "#orders-table",
      placement: "top",
      title: "Orders Overview",
      content: "This is where you can create a new order, check the status of an order or view/print a previous invoice."
    },
    {
      element: ".auth-drp",
      placement: "left",
      title: "Account",
      content: "This is where you can report any system issues, or simply logout."
    }
  ],
  backdrop: true,
  template: temp
});

// Initialize the tour
tour.init();

// Start the tour
tour.start();

