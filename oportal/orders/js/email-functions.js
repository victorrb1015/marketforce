function email_invoice(inv){
  var conf = confirm("Are you sure you want to email this invoice?");
  if(conf === false){
    return;
  }
  var cemail = prompt('Please Enter the Email Address to send the Invoice to:');
  if(cemail === '' || cemail === null || cemail === undefined){
    alert('Please Enter A Valid Email Address!');
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      
			var r = JSON.parse(this.responseText);
      if(r.response === 'GOOD'){
        
        toast_alert('Success!',r.message,'bottom-right','success');
        
      }else{
        toast_alert('Something Went Wrong...',r.message+': '+r.error,'bottom-right','error');
      }
      
    }
  }
  xmlhttp.open("GET","orders/php/send-customer-invoice.php?inv="+inv+"&customer_email="+cemail,true);
  xmlhttp.send();
}