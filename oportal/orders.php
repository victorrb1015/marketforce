<?php
//include 'security/session/session-settings.php';
include 'global/php/connection.php';
include 'global/security/validation-check.php';
$hasCredit = false;

//Affiliate User?...
if($_SESSION['op_affiliate'] == 'Yes'){
  $iMode = 'A';
  $iPMode = 'A';
}else{
  $iMode = 'C';
  $iPMode = '';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Customer Order Portal | MarketForce</title>
    <?php include 'global/sections/head.php'; ?>
     <!--Form Steps Bootstrap CSS-->
    <link href="orders/css/form-steps.css" rel="stylesheet" type="text/css">
</head>

<body onload="get_balance(<?php echo $_SESSION['op_cid']; ?>)">
	<!-- Preloader -->
	<?php include 'global/sections/preloader.php'; ?>
	<!-- /Preloader -->
    <div class="wrapper theme-4-active pimary-color-red">

    	<!--Navigation-->
    	<?php include 'global/sections/nav.php'; ?>
		
		
        <!-- Main Content -->
		<div class="page-wrapper"><!--Includes Footer-->

            <div class="container-fluid pt-25"><!--Main Content Here-->
				
				<div class="row">
     		    <!--Financing Progress Bar Here-->
		     	<?php 
            $clq = "SELECT * FROM `order_credit_lines` WHERE `inactive` != 'Yes' AND `cid` = '" . $_SESSION['op_cid'] . "'";
            $clg = mysqli_query($conn, $clq) or die($conn->error);
            if(mysqli_num_rows($clg)){
              include 'orders/sections/progress-bar.php'; 
              $hasCredit = true;
            }
          ?>

		     		</div>

		<div class="row">
      
      
          <div class="tab-content" id="mySubTabContent">
          <ul class="nav nav-tabs">
            <li class="nav-item active"><a data-toggle="tab" href="#quote" style="color:#DC0031;"><span class="glyphicon glyphicon-floppy-disk"></span> Saved Quotes</a></li>
            <li class="nav-item"><a data-toggle="tab" href="#orders" style="color:#DC0031;"><span class="glyphicon glyphicon-shopping-cart"></span> Pending Orders</a></li>
            <li class="nav-item"><a data-toggle="tab" href="#invoiced" style="color:#DC0031;"><span class="glyphicon glyphicon-shopping-cart"></span> Invoiced Orders</a></li>
						<li class="nav-item"><a data-toggle="tab" href="#paid" style="color:#DC0031;"><span class="glyphicon glyphicon-usd"></span> Paid Orders</a></li>
          </ul>

          <div class="tab-pane active in" id="quote" role="tabpanel">
          <?php
              //Add Order Tracking Sections Here...
              include 'orders/sections/quote-display-table.php';
           ?>
         </div>
         <div class="tab-pane fade" id="orders" role="tabpanel">
          <?php
            include 'orders/sections/orders-table.php';
          ?>
          </div>
				<div class="tab-pane fade" id="invoiced" role="tabpanel">
          <?php
            include 'orders/sections/invoiced-orders-display-table.php';
          ?>
          </div>
         <div class="tab-pane fade" id="paid" role="tabpanel">
          <?php
            include 'orders/sections/paid-orders-display-table.php';
          ?>
          </div>

        </div>
            
            
		  </div>

				
	
			
			</div>
			
			
			<!-- Footer -->
			<?php include 'global/sections/footer.php'; ?>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!--Footer-->
	<?php include 'global/sections/includes.php'; ?>
  <script src="orders/js/new-tour.js?cb=<?php echo $cache_buster; ?>"></script>
  <?php if($hasCredit == true){echo '<script src="orders/js/get-balance.js?cb=' . $cache_buster . '"></script>';} ?>
  <script src="orders/js/order-functions.js?cb=<?php echo $cache_buster; ?>"></script>
  <script src="orders/js/email-functions.js?cb=<?php echo $cache_buster; ?>"></script>
</body>

</html>