<?php
//$switch = 'ON';
if($switch == 'ON'){
  include '../php/connection.php';

  //Get Databases...
  $dq = "SELECT * FROM `organizations` WHERE `db_modifiable` != 'No'";
  $dg = mysqli_query($mf_conn, $dq) or die('Main Connection Error: ' . $mf_conn->error);
  while($dr = mysqli_fetch_array($dg)){

    $conn = mysqli_connect($db_host,$db_user,$db_pass,$dr['db_name']) or die('Local Connection Error on ' . $dr['db_name'] . ': ' . $conn->error);

    $aq = "CREATE TABLE `vendors` 
           (
             `ID` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
             `date` DATE NOT NULL,
             `time` TIME NOT NULL,
             `vendor_name` VARCHAR(150) NOT NULL,
             `vendor_phone` VARCHAR(30) NOT NULL,
             `vendor_email` VARCHAR(150) NOT NULL,
             `category` VARCHAR(30) NOT NULL,
             `inactive` VARCHAR(30) NOT NULL
           )";
    
    //$aq = "DROP TABLE `vendors`";

    //if($dr['db_name'] != 'marketfo_mf_acerotx'){
      if(mysqli_query($conn, $aq)){
        echo '<p>Database: ' . $dr['db_name'] . ' has been ALTERED successfully!</p>';
      }else{
        echo '<p>Database: ' . $dr['db_name'] . ' has encountered an error: ' . $conn->error . '</p>';
      } 
    //}

  }
  echo '<h3 style="color:green;">Table Generating Complete...</h3>';
}else{
  echo '<h3>This script has been disabled for database protections. Please enable if you intend to make databse modifications</h3>';
}