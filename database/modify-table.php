<?php
//$switch = 'ON';
if($switch == 'ON'){
  include '../php/connection.php';

  //Get Databases...
  $dq = "SELECT * FROM `organizations` WHERE `db_modifiable` != 'No'";
  $dg = mysqli_query($mf_conn, $dq) or die('Main Connection Error: ' . $mf_conn->error);
  while($dr = mysqli_fetch_array($dg)){

    $conn = mysqli_connect($db_host,$db_user,$db_pass,$dr['db_name']) or die('Local Connection Error on ' . $dr['db_name'] . ': ' . $conn->error);

    //Add Column Statement...
    //$aq = "ALTER TABLE `inventory_adjustments` 
    //       ADD `process_run_id` VARCHAR(30) NOT NULL AFTER `waste_number`";
    
    //Drop Column Statement...
    //$aq = "ALTER TABLE `inventory_adjustments`
    //       DROP COLUMN `process_id`";
    
    //Change Column Type Statement...
    $aq = "ALTER TABLE `inventory_adjustments` 
           MODIFY `process_run_id` VARCHAR(30) NOT NULL";

    //if($dr['db_name'] != 'marketfo_mf_acerotx'){
      if(mysqli_query($conn, $aq)){
        echo '<p>Database: ' . $dr['db_name'] . ' has been ALTERED successfully!</p>';
      }else{
        echo '<p>Database: ' . $dr['db_name'] . ' has encountered an error: ' . $conn->error . '</p>';
      } 
    //}

  }
  echo '<h3 style="color:green;">ALTERING Complete...</h3>';
}else{
  echo '<h3>This script has been disabled for database protections. Please enable if you intend to make databse modifications</h3>';
}