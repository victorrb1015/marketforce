<?php
    function createImage($number, $color) {

        //$blank = "img/".$color."-0.png";

        $image = LoadPNG($blank);

        // pick color for the text
        $fontcolor = imagecolorallocate($image, 255, 255, 255);

        $font = 2;
        $fontsize = 8;

        $width = imagefontwidth($font) * strlen($number) ;
        $height = imagefontheight($font) ;

        $x = (imagesx($image) - $width) / 2;//Center...
        $y = (imagesy($image) - $height) / 2;//Center...

        //white background
        $backgroundColor = imagecolorallocate ($image, 255, 255, 255);

        //white text
        $textColor = imagecolorallocate($image, 255, 255, 255);

        //  preserves the transparency
        imagesavealpha($image, true);
        imagealphablending($image, false);

        //imagestring($image, $font, $x, $y, $number, $textColor);

        // tell the browser that the content is an image
        header('Content-type: image/png');

        // output image to the browser
        imagepng($image);

        // delete the image resource
        imagedestroy($image);
    }

    function LoadPNG($imgname) {
            /* Attempt to open */
            //$im = imagecreatefrompng($imgname);

            /* See if it failed */
            if(!$im) {
              $text = $_GET['text'];
              $x2 = (strlen($text) * 9.75);
              $y2 = 30;
              //Background Color...
              $bg = $_GET['bg'];
              list($bgr, $bgg, $bgb) = sscanf($bg, "%02x%02x%02x");
              
              
              //Text Color...
              $tcolor = $_GET['tcolor'];
              list($tr, $tg, $tb) = sscanf($tcolor, "%02x%02x%02x");
              
                    /* Create a blank image */
                    $im  = imagecreatetruecolor($x2, $y2);
                    $bgc = imagecolorallocate($im, $bgr, $bgg, $bgb);
                    $tc  = imagecolorallocate($im, $tr, $tg, $tb);
                    $border = imagecolorallocate($im, 0, 0, 0);
              
                    //if($_GET['border'] != 'Yes'){
                      imagefilledrectangle($im, 0, 0, $x2, $y2, $bgc);
                      imagerectangle($im, 0, 0, $x2-1, $y2-1, $border);
                    /*}else{
                      imagefilledrectangle($im, 0, 0, $x2, $y2, $bgc);
                    }*/
              
                    // Set the border and fill colors
                    //$fill = imagecolorallocate($im, 255, 0, 0);
              
                    // Fill the selection
                    //imagefilltoborder($im, 0, 0, $border, $bgc);
              
              

                    /* Output an error message */
                    imagestring($im, 12, 5, 5, $text, $tc);
            }
            return $im;
    }

    if(!isset($_GET['color'])) {
        $_GET['color'] = "blue";
    }
    if(!isset($_GET['number'])) {
        $_GET['number'] = "99";
    }

    createImage($_GET['number'], $_GET['color']);
?>