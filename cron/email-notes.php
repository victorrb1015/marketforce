<?php
include 'connection.php';
$_SESSION['org_db_name'] = 'marketfo_mf_allsteelcarports';
include 'connection.php';

//Email Parameters
include '../phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include '../phpmailsettings.php';
$mail->CharSet = "UTF-8";
$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
$mail->addAddress('michael@allsteelcarports.com');
$mail->addCC('teresa@allsteelcarports.com');
$mail->addCC('jeffrey@allsteelcarports.com');
//$mail->addAddress('michael@ignition-innovations.com');
$mail->addBCC('archive@ignition-innovations.com');
$mail->Subject = 'Visit Notes From ' . date('l, F d, Y');
//$mail->Subject = 'Visit Notes From ' . date('l, F d, Y',strtotime("-1 days"));
$notes = '';
$email = '';//Rep's Email

//Get note data...
//$nd = "SELECT * FROM `visit_notes` WHERE `date` = CURRENT_DATE - 1";
$nd = "SELECT * FROM `visit_notes` WHERE `date` = CURRENT_DATE";

$ng = mysqli_query($conn, $nd) or die($conn->error);
if(mysqli_num_rows($ng) != 0){
while($nr = mysqli_fetch_array($ng)){
  //Get Reps Name
  $rnq = "SELECT * FROM `users` WHERE `ID` = '" . $nr['user'] . "'";
  //$rnq = "SELECT * FROM `contacts`";
  $rng = mysqli_query($conn, $rnq) or die($conn->error);
  $rnr = mysqli_fetch_array($rng);
  
  if($email == ''){
  $mail->addAddress($rnr['email']);
  $email = $rnr['email'];
  }
  if($email != $rnr['email']){
  $mail->addAddress($rnr['email']);
  $email = $rnr['email'];
  }
  //Get Dealer Name
  $dnq = "SELECT * FROM `dealers` WHERE `ID` = '" . $nr['dealer_id'] . "'";
  $dng = mysqli_query($conn, $dnq) or die($conn->error);
  $dnr = mysqli_fetch_array($dng);
  if($nr['vtype'] == 'Cold Call'){
    $cdnq = "SELECT * FROM `prospects` WHERE `PID` = '" . $nr['dealer_id'] . "'";
    $cdng = mysqli_query($conn, $cdnq) or die($conn->error);
    $cdnr = mysqli_fetch_array($cdng);
    $dname = str_replace('-xxx-','&',$cdnr['bizname']) . '<span style="color:red;"> (Cold Call)</span>';
  }else{
    $dname = $dnr['name'] . ' -- ' . $dnr['city'] . ', ' . $dnr['state'] . '<span style="color:blue;"> (' . $nr['vtype'] . ' Visit)</span>';
  }
  $cont = str_replace('-xxx-', '&', $nr['notes']);
  
    $notes .= '<p><strong>' . $dname . ':</strong></p>
               <p>' . $cont . '<br><small>(Note entered by: ' . $rnr['fname'] . ' ' . $rnr['lname'] . ' @ ' . date("m/d/y",($nr['date']) . date("h:ia", strtotime($nr['time'])) . ')</small></p>
               <br><br>';
}

$mail->Body = '<html>
                  <head>
                  <style>
                  .pic{
                    margin: auto;
                    text-align: center;
                  }
                  small{
                    color: red;
                  }
                  </style>
                  </head>
                  <body>
                  <div class="main">
                  <div class="pic">
                    <img src="http://marketforceapp.com/assets/img/full-mf-logo.png" style="width: 25%;" />
                  </div>
                    <h1 style="text-align:center;"><u>Visit Notes From ' . date('l, F d, Y') . ':</u></h1>
                    <br><br>
                    ' . $notes . '
                  </div>
                  </body>
                  </html>';
  
  if($_GET['view'] == 'yes'){
  echo $mail->Body;
  }else{
  $mail->send();
  }
}else{
  echo 'No Email Sent!';
}
?>