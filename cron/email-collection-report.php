<?php
include 'connection.php';
$_SESSION['org_db_name'] = 'marketfo_mf_allsteelcarports';
include 'connection.php';
//Get Information from Calculation Script
include '../marketforce/collections/calc-total.php';



//Calculate ALL Collections Amount
//2017 Figures
$pp_overall_coll_amount = 0;
$lamount = 0;
$ocaq = "SELECT * FROM `collections` WHERE `precb` != 'Yes' AND `inactive` != 'Yes' AND `collection` = 'Yes' AND YEAR(`date`) = '2017'";
$ocag = mysqli_query($conn, $ocaq) or die($conn->error);
while($ocar = mysqli_fetch_array($ocag)){
  if($ocar['legal'] == ''){
  $pq = "SELECT * FROM `collections_calls` WHERE `inactive` != 'Yes' AND `customer_id` = '" . $ocar['ID'] . "' AND `type` = 'Payment'";
  $pg = mysqli_query($conn, $pq) or die($conn->error);
  $pmts = 0;
  while($pr = mysqli_fetch_array($pg)){
    $pmts = $pmts + $pr['amount'];
  }
  $amt = $ocar['initial_debt'] - $pmts;
  $pp_overall_coll_amount = $pp_overall_coll_amount + $amt;
  }else{
		$lamount = $lamount + $ocar['legal'];
	}
}

//2018 Figures
$p_overall_coll_amount = 0;

$ocaq = "SELECT * FROM `collections` WHERE `precb` != 'Yes' AND `inactive` != 'Yes' AND `collection` = 'Yes' AND YEAR(`date`) = '2018'";
$ocag = mysqli_query($conn, $ocaq) or die($conn->error);
while($ocar = mysqli_fetch_array($ocag)){
  if($ocar['legal'] == ''){
  $pq = "SELECT * FROM `collections_calls` WHERE `inactive` != 'Yes' AND `customer_id` = '" . $ocar['ID'] . "' AND `type` = 'Payment'";
  $pg = mysqli_query($conn, $pq) or die($conn->error);
  $pmts = 0;
  while($pr = mysqli_fetch_array($pg)){
    $pmts = $pmts + $pr['amount'];
  }
  $amt = $ocar['initial_debt'] - $pmts;
  $p_overall_coll_amount = $p_overall_coll_amount + $amt;
  }else{
		$lamount = $lamount + $ocar['legal'];
	}
}

//2019 Figures
$c_overall_coll_amount = 0;

$ocaq = "SELECT * FROM `collections` WHERE `precb` != 'Yes' AND `inactive` != 'Yes' AND `collection` = 'Yes' AND YEAR(`date`) = '2019'";
$ocag = mysqli_query($conn, $ocaq) or die($conn->error);
while($ocar = mysqli_fetch_array($ocag)){
  if($ocar['legal'] == ''){
  $pq = "SELECT * FROM `collections_calls` WHERE `inactive` != 'Yes' AND `customer_id` = '" . $ocar['ID'] . "' AND `type` = 'Payment'";
  $pg = mysqli_query($conn, $pq) or die($conn->error);
  $pmts = 0;
  while($pr = mysqli_fetch_array($pg)){
    $pmts = $pmts + $pr['amount'];
  }
  $amt = $ocar['initial_debt'] - $pmts;
  $c_overall_coll_amount = $c_overall_coll_amount + $amt;
  }else{
		$lamount = $lamount + $ocar['legal'];
	}
}


//2020 Figures
$cc_overall_coll_amount = 0;

$ocaq = "SELECT * FROM `collections` WHERE `precb` != 'Yes' AND `inactive` != 'Yes' AND `collection` = 'Yes' AND YEAR(`date`) = '2020'";
$ocag = mysqli_query($conn, $ocaq) or die($conn->error);
while($ocar = mysqli_fetch_array($ocag)){
  if($ocar['legal'] == ''){
  $pq = "SELECT * FROM `collections_calls` WHERE `inactive` != 'Yes' AND `customer_id` = '" . $ocar['ID'] . "' AND `type` = 'Payment'";
  $pg = mysqli_query($conn, $pq) or die($conn->error);
  $pmts = 0;
  while($pr = mysqli_fetch_array($pg)){
    $pmts = $pmts + $pr['amount'];
  }
  $amt = $ocar['initial_debt'] - $pmts;
  $cc_overall_coll_amount = $cc_overall_coll_amount + $amt;
  }else{
		$lamount = $lamount + $ocar['legal'];
	}
}



//Email Parameters
include '../phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include '../phpmailsettings.php';
$mail->setFrom('no-reply@allsteelcarports.com','All Steel Carports');
$mail->addReplyTo('collection@allsteelcarports.com');
$mail->addAddress('michael@allsteelcarports.com');
$mail->addAddress('dawn@allsteelcarports.com');
$mail->addAddress('Alex@legacybuildings.us');
$mail->addAddress('Areli@legacybuildings.us');
$mail->addAddress('Noe@legacybuildings.us');
//$mail->addAddress('chavez@allsteelcarports.com');
$mail->addBCC('archive@ignition-innovations.com');
$mail->Subject = 'Collection Report';

$mail->Body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Collection Report</title>


<style type="text/css">
img {
max-width: 100%;
}
body {
-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em;
}
body {
background-color: #f6f6f6;
}
@media only screen and (max-width: 640px) {
  body {
    padding: 0 !important;
  }
  h1 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h2 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h3 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h4 {
    font-weight: 800 !important; margin: 20px 0 5px !important;
  }
  h1 {
    font-size: 22px !important;
  }
  h2 {
    font-size: 18px !important;
  }
  h3 {
    font-size: 16px !important;
  }
  .container {
    padding: 0 !important; width: 100% !important;
  }
  .content {
    padding: 0 !important;
  }
  .content-wrap {
    padding: 10px !important;
  }
  .invoice {
    width: 100% !important;
  }
}

/* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
* {
  margin: 0;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  box-sizing: border-box;
  font-size: 14px;
}

img {
  max-width: 100%;
}

body {
  -webkit-font-smoothing: antialiased;
  -webkit-text-size-adjust: none;
  width: 100% !important;
  height: 100%;
  line-height: 1.6em;
  /* 1.6em * 14px = 22.4px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 22px;*/
}

/* Lets make sure all tables have defaults */
table td {
  vertical-align: top;
}

/* -------------------------------------
    BODY & CONTAINER
------------------------------------- */
body {
  background-color: #f6f6f6;
}

.body-wrap {
  background-color: #f6f6f6;
  width: 100%;
}

.container {
  display: block !important;
  max-width: 600px !important;
  margin: 0 auto !important;
  /* makes it centered */
  clear: both !important;
}

.content {
  max-width: 600px;
  margin: 0 auto;
  display: block;
  padding: 20px;
}

/* -------------------------------------
    HEADER, FOOTER, MAIN
------------------------------------- */
.main {
  background-color: #fff;
  border: 1px solid #e9e9e9;
  border-radius: 3px;
}

.content-wrap {
  padding: 20px;
}

.content-block {
  padding: 0 0 20px;
}

.header {
  width: 100%;
  margin-bottom: 20px;
}

.footer {
  width: 100%;
  clear: both;
  color: #999;
  padding: 20px;
}
.footer p, .footer a, .footer td {
  color: #999;
  font-size: 12px;
}

/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1, h2, h3 {
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  color: #000;
  margin: 40px 0 0;
  line-height: 1.2em;
  font-weight: 400;
}

h1 {
  font-size: 32px;
  font-weight: 500;
  /* 1.2em * 32px = 38.4px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 38px;*/
}

h2 {
  font-size: 24px;
  /* 1.2em * 24px = 28.8px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 29px;*/
}

h3 {
  font-size: 18px;
  /* 1.2em * 18px = 21.6px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 22px;*/
}

h4 {
  font-size: 14px;
  font-weight: 600;
}

p, ul, ol {
  margin-bottom: 10px;
  font-weight: normal;
}
p li, ul li, ol li {
  margin-left: 5px;
  list-style-position: inside;
}

/* -------------------------------------
    LINKS & BUTTONS
------------------------------------- */
a {
  color: #348eda;
  text-decoration: underline;
}

.btn-primary {
  text-decoration: none;
  color: #FFF;
  background-color: #348eda;
  border: solid #348eda;
  border-width: 10px 20px;
  line-height: 2em;
  /* 2em * 14px = 28px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 28px;*/
  font-weight: bold;
  text-align: center;
  cursor: pointer;
  display: inline-block;
  border-radius: 5px;
  text-transform: capitalize;
}

/* -------------------------------------
    OTHER STYLES THAT MIGHT BE USEFUL
------------------------------------- */
.last {
  margin-bottom: 0;
}

.first {
  margin-top: 0;
}

.aligncenter {
  text-align: center;
}

.alignright {
  text-align: right;
}

.alignleft {
  text-align: left;
}

.clear {
  clear: both;
}

/* -------------------------------------
    ALERTS
    Change the class depending on warning email, good email or bad email
------------------------------------- */
.alert {
  font-size: 16px;
  color: #fff;
  font-weight: 500;
  padding: 20px;
  text-align: center;
  border-radius: 3px 3px 0 0;
}
.alert a {
  color: #fff;
  text-decoration: none;
  font-weight: 500;
  font-size: 16px;
}
.alert.alert-warning {
  //background-color: #FF9F00;
  background-color:#F60032;
}
.alert.alert-bad {
  background-color: #D0021B;
}
.alert.alert-good {
  background-color: #68B90F;
}

/* -------------------------------------
    INVOICE
    Styles for the billing table
------------------------------------- */
.invoice {
  margin: 40px auto;
  text-align: left;
  width: 80%;
}
.invoice td {
  padding: 5px 0;
}
.invoice .invoice-items {
  width: 100%;
}
.invoice .invoice-items td {
  border-top: #eee 1px solid;
}
.invoice .invoice-items .total td {
  border-top: 2px solid #333;
  border-bottom: 2px solid #333;
  font-weight: 700;
}

/* -------------------------------------
    RESPONSIVE AND MOBILE FRIENDLY STYLES
------------------------------------- */
@media only screen and (max-width: 640px) {
  body {
    padding: 0 !important;
  }

  h1, h2, h3, h4 {
    font-weight: 800 !important;
    margin: 20px 0 5px !important;
  }

  h1 {
    font-size: 22px !important;
  }

  h2 {
    font-size: 18px !important;
  }

  h3 {
    font-size: 16px !important;
  }

  .container {
    padding: 0 !important;
    width: 100% !important;
  }

  .content {
    padding: 0 !important;
  }

  .content-wrap {
    padding: 10px !important;
  }

  .invoice {
    width: 100% !important;
  }
}

/*# sourceMappingURL=styles.css.map */
</style>
</head>

<body itemscope itemtype="http://schema.org/EmailMessage" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">

<div style="text-align:center;">
<img src="http://marketforceapp.com/marketforce/img/all-steel-red.jpg" width="500" />
</div>

<table class="body-wrap" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6"><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
		<td class="container" width="600" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
			<div class="content" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
				<table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff"><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert alert-warning" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #F60032; margin: 0; padding: 20px;" align="center" bgcolor="#F60032" valign="top">
							All Steel Carports Collection Report
						</td>
					</tr><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-wrap" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
							<table width="100%" cellpadding="0" cellspacing="0" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
										
                    <strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Total Collected Today:</strong> $' . number_format($paid_total,2) . '
										<br>
                    <strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">New Collections Added Today:</strong> $' . number_format($coll_total,2) . '<br>
                    <ul>
                    <li><b>ck_ca_cc:</b> $' . number_format($coll_ck_ca_cc,2) . '</li>
                    <li><b>po:</b> $' . number_format($coll_po,2) . '</li>
                    <li><b>nsf_stop:</b> $' . number_format($coll_nsf_stop,2) . '</li>
                    <li><b>repair:</b> $' . number_format($coll_repair,2) . '</li>
                    <li><b>dealer_pmt:</b> $' . number_format($coll_dealer,2) . '</li>
                    <li><b>dealer_fin:</b> $' . number_format($coll_dealer_fin,2) . '</li>
                    <li><b>legal:</b> $' . number_format($coll_legal,2) . '</li>
                    <li><b>bli:</b> $' . number_format($coll_bli,2) . '</li>
                    <li><b>ezpay:</b> $' . number_format($coll_ezpay,2) . '</li>
                    <li><b>scoggin:</b> $' . number_format($coll_scoggin,2) . '</li>
                    <li><b>double_d:</b> $' . number_format($coll_double_d,2) . '</li>
                    <li><b>dallen_lagacy:</b> $' . number_format($coll_dallen_legacy,2) . '</li>
                    <li><b>rto_national:</b> $' . number_format($coll_rto_national,2) . '</li>
                    </ul>
										<strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">2017 Total in Collections:</strong> $' . number_format($pp_overall_coll_amount,2) . '
										<br>
										<strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">2018 Total in Collections:</strong> $' . number_format($p_overall_coll_amount,2) . '
										<br>
										<strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">2019 Total in Collections:</strong> $' . number_format($c_overall_coll_amount,2) . '
										<br>
                    <strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">2020 Total in Collections:</strong> $' . number_format($cc_overall_coll_amount,2) . '
                    <br>
										<strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Combined Total in Collections:</strong> $' . number_format($overall_coll_amount,2) . '
										<br>
										<strong style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Amount in Legal:</strong> $' . number_format($lamount,2) . '
										<br>
									</td>
								</tr><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
										This report is based on collections information entered into Market Force on ' . date("F d, Y") . '.
									</td>
								</tr>
                <!--<tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; text-align: center;" valign="top">
										<a href="http://ignition.church/admin" class="btn-primary" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;">View Registrant Information</a>
									</td>
								</tr>-->
								<!--<tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
										Thanks for choosing Acme Inc.
									</td>
								</tr>-->
								</table></td>
					</tr></table><div class="footer" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">
					<table width="100%" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><tr style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="aligncenter content-block" style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">Questions? Email: info@burtonsolution.tech</td>
						</tr></table></div></div>
		</td>
		<td style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
	</tr></table></body>
</html>';

$mail->send();
//echo $mail->Body;
echo 'Email Collection Report Sent!<br>';
$mail->ClearAddresses();
$mail->addAddress('3362513969@txt.att.net');
$mail->addAddress('7657169694@vtext.com');//Michael Burton Cell
$mail->addAddress('7652289341@vtext.com');//Nacho Cell
$mail->addAddress('7659941199@vtext.com');//Steve Kramar Cell
$mail->addAddress('5555001804@txt.att.net');//Zaret Corona Cell
$mail->addBCC('archive@ignition-innovations.com');

$mail->isHTML(false);
$mail->Body = ' (2018): $' . number_format($p_overall_coll_amount,2) . ', (2019): $' . number_format($c_overall_coll_amount) . ', (2020): $' . number_format($cc_overall_coll_amount) . '      Collected: $' . number_format($paid_total,2) . ',    Added: $' . number_format($coll_total,2) . ', Legal: $' . number_format($lamount,2);

                    /*ck_ca_cc: $' . $coll_ck_ca_cc . '
                    po: $' . $coll_po . '
                    nsf_stop: $' . $coll_nsf_stop . '
                    repair: $' . $coll_repair . '
                    dealer_pmt: $' . $coll_dealer . '
                    dealer_fin: $' . $coll_dealer_fin . '
                    legal: $' . $coll_legal . '
                    bli: $' . $coll_bli . '
                    ezpay: $' . $coll_ezpay . '
                    scoggin: $' . $coll_scoggin . '
                    double_d: $' . $coll_double_d . '
                    dallen_lagacy: $' . $coll_dallen_legacy . '
                    rto_national: $' . $coll_rto_national;*/


$mail->send();
echo 'SMS Collection Report was sent!<br>';
//echo $mail->Body;
?>




