<?php
session_start();
$_SESSION['org_db_name'] = 'marketfo_mf_allsteelcarports';
include 'connection.php';

include '../phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include '../phpmailsettings.php';

$mail->setFrom('no-reply@allsteelcarports.com','Market Force');
$mail->addAddress('archive@ignition-innovations.com');
$mail->Subject = 'Pre-Owned Listings Updated!';
$mail->Body = '<html>
                <body>
                  <h1><u>List of updated listings:</u></h1>';



$q = "SELECT * FROM `listings` WHERE `date_sold` <= CURRENT_DATE - 7 AND `sold` = 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);

if(mysqli_num_rows($g) != 0){
  
$q2 = "UPDATE `listings` SET `sold` = 'CLEAR' WHERE `date_sold` <= CURRENT_DATE - 7 AND `sold` = 'Yes'";
mysqli_query($conn, $q2) or die($conn->error);
  
while($r = mysqli_fetch_array($g)){
 $mail->Body .= '<p>Invoice#: ' . $r['invoice#'] . ' has been updated to CLEAR</p>';
}
  
  $mail->send();
  
}else{
  echo 'No email was sent!';
}

?>