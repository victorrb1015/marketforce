<?php
//header('Content-Type: application/json');
error_reporting(0);
include 'connection.php';
$db_host = '185.28.23.230:6603';
$db_user = 'root';
$db_pass = 'secret';
$_SESSION['org_db_name'] = $_REQUEST['org_db_name'];
$db_db = $_REQUEST['org_db_name'];
$conn = mysqli_connect($db_host,$db_user,$db_pass,$db_db) or die('Main Connection Error: ' . $conn->error);

//Load Variables...
$items = [];

$q = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND `min_qty` != '0' AND `qty` <= `min_qty`";
//$q = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' LIMIT 10";
$g = mysqli_query($conn, $q) or die('inventory_items Connection Issue: ' . $conn->error);

$record_count = mysqli_num_rows($g);

while($r = mysqli_fetch_object($g)){
  array_push($items, $r);
}
include 'email-templates/min-thresholds-email.php';
//echo json_encode($items,JSON_PRETTY_PRINT);

#Main Functions...
//echo $email_tmp;

//Email Parameters
include '../phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
include '../phpmailsettings.php';
$mail->setFrom('no-reply@marketforceapp.com','MarketForce');
$mail->addAddress('michael@allsteelcarports.com');
$mail->addBCC('archive@ignition-innovations.com');
$mail->addCC('michael@allsteelcarports.com');
//Justin, Kelly, Roberto, Tony, Michael, Jesus, Ignacio
//$mail->addBCC('michael@burtonsolution.tech');
$mail->Subject = 'Low Inventory Report';

$mail->Body = $email_tmp;

if($record_count > 0){
  $mail->send();
  echo 'The Low Inventory Report has been sent!';
}else{
  echo 'No Email Sent!';
}