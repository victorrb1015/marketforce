<?php
include 'connection.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Print Labels</title>
    <!--<link href="labels.css" rel="stylesheet" type="text/css" >-->
    <style>
    body {
        width: 8.5in;
        //margin: 0in .1875in;
        margin: 0in .19in;
        margin-top: .45in;/*was -.5in*/
        //margin: .63in .31in;
        //margin-top: -15px;
        //margin-left: .55in;
        margin-left: 0in;
        //margin-right: 0px;
        //outline: 1px solid red;
        }
    .label{
        /* Avery 5160 labels -- CSS and HTML by MM at Boulder Information Services */
        //width: 2.025in; /* plus .6 inches from padding */
        //width: 2.3in;
        width: 2.55in;
        height: .825in; /* plus .125 inches from padding */
        //height: 1in;
        padding: .125in .1in;
        margin-right: .125in; /* the gutter */
        

        float: left;
        font-size:15px;
        text-align: center;
        overflow: hidden;

        outline: 1px dotted; /* outline doesn't occupy space like border does */
        }
    .page-break  {
        clear: left;
        display: block;
        page-break-after:always;
        }
      .marg{
        height: 1.2in;
      }
      .margins{
        //padding-right: 0px;
        margin-right: 0in;
      }
      @page {
    margin: 0;
    }
    </style>

</head>
<body>
<br><br><br><br>
  <?php 
  $q = "SELECT * FROM `new_dealer_form` WHERE `inactive` != 'Yes'";
  $g = mysqli_query($conn, $q) or die($conn->error);
  $i = 0;
  $ii = 0;
  while($r = mysqli_fetch_array($g)){
    $i++;
    $ii++;
    if($ii == 3){
      $margin = 'margins';
      $ii = 0;
    } 
    echo '<div class="label ' . $margin . '">
          <b>' . ucwords(strtolower($r['bizname'])) . '</b><br>
          ' . ucwords(strtolower($r['address'])) . '<br>
          ' . ucwords(strtolower($r['city'])) . ', ' . ucwords(strtolower($r['state'])) . ' ' . $r['zip'] . '
          </div>';
    $margin = '';
    if($i == 30){
      echo '<div class="page-break"></div>';
      echo '<div class="marg"></div>';
      $i = 0;
    }
  }
  ?>

<script>
  window.print();
  </script>
</body>
</html>