<?php
$db_host = '185.28.23.230:6603';
$db_user = 'root';
$db_pass = 'secret';
$db_mf_db = 'marketfo_marketforce';
$mysqli = new mysqli($db_host, $db_user, $db_pass, 'marketfo_mf_allamerican');
/* verificar la conexión */
if (mysqli_connect_errno()) {
    printf("Falló la conexión failed: %s\n", $mysqli->connect_error);
    exit();
}
$report = array();
$sel = "SELECT reportID, sub_date, rep_name, approved_by_name , approved_date  FROM `expense_reports` ORDER BY reportID DESC";
$g = $mysqli->query($sel);
while ($r = $g->fetch_array()) {
    $array = [
        'reportID' => $r['reportID'],
        'sub_date' => $r['sub_date'],
        'rep_name' => $r['rep_name'],
        'approved_by_name' => $r['approved_by_name'],
        'approved_date' => $r['approved_date']
    ];
    $report[] = $array;
}
$g->close();
foreach ($report as $clave=>$value){
    $sel2 = "SELECT amount FROM expenses WHERE reportID = '".$value['reportID']."'";
    $g2 = $mysqli->query($sel2);
    $total = 0;
    //echo 'El reporte es: '.$value['reportID'];
    //echo '<br/> Su consulta es : '.$sel2.'<br />';
    while ($r2 = $g2->fetch_array()) {
        $number = str_replace(',', '', $r2['amount']);
        $float = (float) $number;
        $total += $float;
        //echo 'Sus valores son: '.$r2['amount'].' y en numero son: '.$float.' el total actual es: '.$total.'<br/> ';
    }
    $g2->close();
    $total_usd = ($total/21.48);
    $report[$clave] += [
        'Total' => $total,
        'Total_USD' => $total_usd
    ];
}
$mysqli->close();
$excel = [
    ['Report ID', 'Submit Date','Representative Name','Approved by', 'Approved Date','Status','Total MXN','Total USD'],
];
$body = '
 <!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Special Pricing Notification</title>
     <style>
      th {
        background-color: black ;
        color: white;
        font-weight: normal;
        padding: 20px 30px;
        text-align: center;
      }
      td{
        padding: 10px 20px;
        background-color: #c1bbbb !important;
         color: black;
      }
     </style>
   </head>
   <body>
     <h1>Expenses report</h1>
     <table class="table table-striped table-bordered">
         <thead>
             <tr>
               <th scope="col">Report ID</th>
               <th scope="col">Submit Date</th>
               <th scope="col">Representative Name</th>
               <th scope="col">Approved by</th>
               <th scope="col">Approved Date</th>
               <th scope="col">Status</th>
               <th scope="col">Total MXN</th>
               <th scope="col">Total USD</th>
             </tr>
         </thead>
         <tbody>';
foreach ($report as $clave => $value) {
    $status = 'Not approved';
    if(strlen($value['approved_by_name']) > 0) {
        $status = 'Approved';
    }
    $body .= '<tr>
               <td>' . $value['reportID'] . '</td>
               <td>' . $value['sub_date'] . '</td>
               <td>' . $value['rep_name'] . '</td>
               <td>' . $value['approved_by_name'] . '</td>
               <td>' . $value['approved_date'] . '</td>
               <td>' . $status . '</td>
               <td> $ ' . number_format($value['Total'], 2, ',', ' ') . '</td>
               <td> $ ' . number_format($value['Total_USD'], 2, '.', ' ') . '</td>
       </tr>';
    $excel[] = [$value['reportID'],$value['sub_date'],$value['rep_name'],$value['approved_by_name'],$value['approved_date'],$status,number_format($value['Total'], 2, ',', ' '),number_format($value['Total_USD'], 2, '.', ' ')];
}
$body .= '
       </tbody>
     </table>
   </body>
 </html>
 <br />';
//echo $body;
// Open a file in write mode ('w')
$fp = fopen('report.csv', 'w');
// Loop through file pointer and a line
foreach ($excel as $fields) {
    fputcsv($fp, $fields);
}
fclose($fp);
$fichero = 'report.csv';

if (file_exists($fichero)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($fichero).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($fichero));
    readfile($fichero);
    exit;
}
