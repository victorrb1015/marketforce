<?php

$db_host = '185.28.23.230:6603';
$db_user = 'root';
$db_pass = 'secret';
$db_mf_db = 'marketfo_marketforce';
$mysqli = new mysqli($db_host, $db_user, $db_pass, 'marketfo_mf_acerotx');
/* verificar la conexión */
if (mysqli_connect_errno()) {
    printf("Falló la conexión failed: %s\n", $mysqli->connect_error);
    exit();
}
$report = array();
$sel = "SELECT date, item_name, item_info, item_type, item_units, item_weight, qty FROM `inventory_items` WHERE `inactive` != 'Yes'";
$g = $mysqli->query($sel);
while ($r = $g->fetch_array()) {
    $array = [
        'date' => date("m/d/y", strtotime($r['date'])),
        'item_name' => $r['item_name'],
        'item_info' => $r['item_info'],
        'item_type' => $r['item_type'],
        'item_units' => $r['item_units'],
        'item_weight' => $r['item_weight'],
        'qty' => $r['qty'],
    ];
    $report[] = $array;
}
$g->close();
$mysqli->close();
$body = '
 <!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Special Pricing Notification</title>
     <style>
      th {
        background-color: black ;
        color: white;
        font-weight: normal;
        padding: 20px 30px;
        text-align: center;
      }
      td{
        padding: 10px 20px;
        background-color: #c1bbbb !important;
         color: black;
      }
     </style>
   </head>
   <body>
     <h1>Special Pricing Notification!</h1>
     <table class="table table-striped table-bordered">
         <thead>
             <tr>
               <th scope="col">#</th>
               <th scope="col">Date</th>
               <th scope="col">Item Name</th>
               <th scope="col">Item Information</th>
               <th scope="col">Item Type</th>
               <th scope="col">Item Units</th>
               <th scope="col">Item Weight</th>
               <th scope="col">QTY</th>
             </tr>
         </thead>
         <tbody>';
$excel = [
    ['#', 'Date','Item Name', 'Item Information', 'Item Type','Item Units', 'Item Weight', 'QTY'],
];
foreach ($report as $clave => $value) {
    $body .= '<tr>
               <td>' . ($clave+1) . '</td>
               <td>' . $value['date'] . '</td>
               <td>' . $value['item_name'] . '</td>
               <td>' . $value['item_info'] . '</td>
               <td>' . $value['item_type'] . '</td>
               <td>' . $value['item_units'] . '</td>
               <td>' . $value['item_weight'] . '</td>
               <td>' . $value['qty'] . '</td>
       </tr>';
    $excel[] = [($clave+1),$value['date'],$value['item_name'],$value['item_info'], $value['item_type'],$value['item_units'], $value['item_weight'], $value['qty']];
}
$body .= '
       </tbody>
     </table>
   </body>
 </html>';
echo $body;
$date = date("m-d-y");
$file = 'excel/inventory_'.$date.'.csv';
// Open a file in write mode ('w')
$fp = fopen($file, 'w');
// Loop through file pointer and a line
foreach ($excel as $fields) {
    fputcsv($fp, $fields);
}
fclose($fp);

include '../../phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;

//Get SMTP Connection Details...
$mailconn = mysqli_connect('185.28.23.230:6603','root','secret','marketfo_mf_acerotx');
$mailq = "SELECT * FROM `mailgun_credentials`";
$mailg = mysqli_query($mailconn, $mailq) or die($mailconn->error);
$mailr = mysqli_fetch_array($mailg);

//Settings for the PHPMailer Class

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = $mailr['host'];  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = $mailr['username'];                 // SMTP username
$mail->Password = $mailr['password'];                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->isHTML(true);
$mail->setFrom('collections@allsteelcarports.com','Marketforce');/*
$mail->addAddress('michael.burton@acero.industries', 'Michael Burton');
$mail->addAddress('chuck@acero.industries', 'Chuck Wiedenhoefer');
$mail->addAddress('justin@acero.industries', 'Justin');
$mail->addAddress('roberto.cabrera@acero.industries', 'Roberto Cabrera');
$mail->addAddress('fabian@grupocomunicado.com', 'Fabian Rivera');
$mail->addAddress('igchavezcastillo@gmail.com', 'Ignacio Chavez');
$mail->addAddress('Jesus.Cabrera@acero.industries', 'Jesus Cabrera');*/
$mail->addAddress('Mark@acero.industries', 'Mark');
$mail->addAddress('victor@grupocomunicado.com', 'Victor Rojas');
$mail->Subject = 'Inventory excel report';
$mail->Body = $body;
$mail->AddAttachment($file);
$mail->AltBody = $body;
if (!$mail->send()) {
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'The email message was sent.';
}