<?php
$db_host = '185.28.23.230:6603';
$db_user = 'root';
$db_pass = 'secret';
$db_mf_db = 'marketfo_marketforce';
$conn = mysqli_connect($db_host, $db_user, $db_pass, 'marketfo_mf_acerotx') or die('Main Connection Error: ' . $conn->error);
$lowReport_v2 = array();
$q = "SELECT * FROM `inventory_items` WHERE `inactive` != 'Yes' AND `item_type` NOT LIKE 'Product'";
$g = mysqli_query($conn, $q) or die($conn->error);
while ($r = mysqli_fetch_array($g)) {
    $val_qty = (int)$r['qty'];
    $val_min_qty = (int)$r['min_qty'];
    if (($val_qty <= $val_min_qty) && $val_qty !== 0) {
        $array = [
            'barcode' => $r['barcode'],
            'item_name' => $r['item_name'],
            'qty' => number_format($r['qty']),
            'min_qty' => number_format($r['min_qty']),
            'ID' => $r['ID'],
        ];
        $lowReport_v2[] = $array;
    }
}
$query3 = 'SELECT item_id FROM inventory_requisition WHERE po_done is null AND inactive is False group by item_id';
$g3 = mysqli_query($conn, $query3) or die($conn->error);
$report = array();
while ($res = mysqli_fetch_array($g3)) {
    foreach ($lowReport_v2 as $clave => $value) {
        if ($value['ID'] === $res['item_id']) {
            $query2 = 'SELECT quote, po_due_date FROM inventory_requisition WHERE po_done is null AND inactive is False and item_id = ' . $value['ID'] . ' AND created_at = (SELECT max(created_at) from inventory_requisition where item_id = ' . $value['ID'] . ')';
            $g2 = mysqli_query($conn, $query2) or die($conn->error);
            $var = $g2->fetch_assoc();
            $lowReport_v2[$clave] += [
                'quote' => $var['quote'],
                'qty_requ' => $var['po_due_date']
            ];
        }
    }
}
$q2 = "SELECT item_name, count(*) as Quantity,  SUM(item_weight) as 'Total Weight', min_group FROM `inventory_items`  where item_type = 'Coil'  And inactive != 'Yes'  GROUP BY item_name, min_group";
$g2 = mysqli_query($conn, $q2) or die($conn->error);


$body = '
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <style>
    
    .container, .container-fluid, .container-lg, .container-md, .container-sm, .container-xl, .container-xxl {
        width: 100%;
        padding-right: var(--bs-gutter-x, .75rem);
        padding-left: var(--bs-gutter-x, .75rem);
        margin-right: auto;
        margin-left: auto
    }
    
    @media (min-width: 576px) {
        .container, .container-sm {
            max-width: 540px
        }
    }
    
    @media (min-width: 768px) {
        .container, .container-md, .container-sm {
            max-width: 720px
        }
    }
    
    @media (min-width: 992px) {
        .container, .container-lg, .container-md, .container-sm {
            max-width: 960px
        }
    }
    
    @media (min-width: 1200px) {
        .container, .container-lg, .container-md, .container-sm, .container-xl {
            max-width: 1140px
        }
    }
    
    @media (min-width: 1400px) {
        .container, .container-lg, .container-md, .container-sm, .container-xl, .container-xxl {
            max-width: 1320px
        }
    }
    .row {
    --bs-gutter-x: 1.5rem;
    --bs-gutter-y: 0;
    display: flex;
    flex-wrap: wrap;
    margin-top: calc(-1 * var(--bs-gutter-y));
    margin-right: calc(-.5 * var(--bs-gutter-x));
    margin-left: calc(-.5 * var(--bs-gutter-x))
}

.row > * {
    flex-shrink: 0;
    width: 100%;
    max-width: 100%;
    padding-right: calc(var(--bs-gutter-x) * .5);
    padding-left: calc(var(--bs-gutter-x) * .5);
    margin-top: var(--bs-gutter-y)
}
.h5, h5 {
    font-size: 1.25rem;
}

.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
    margin-top: 0;
    margin-bottom: .5rem;
    font-weight: 500;
    line-height: 1.2
}

.h1, h1 {
    font-size: calc(1.375rem + 1.5vw)
}

@media (min-width: 1200px) {
    .h1, h1 {
        font-size: 2.5rem
    }
}
.col-12 {
    flex: 0 0 auto;
    width: 100%
}
.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0, 0, 0, .125);
    border-radius: .25rem
}

.card > hr {
    margin-right: 0;
    margin-left: 0
}

.card > .list-group {
    border-top: inherit;
    border-bottom: inherit
}

.card > .list-group:first-child {
    border-top-width: 0;
    border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px)
}

.card > .list-group:last-child {
    border-bottom-width: 0;
    border-bottom-right-radius: calc(.25rem - 1px);
    border-bottom-left-radius: calc(.25rem - 1px)
}

.card > .card-header + .list-group, .card > .list-group + .card-footer {
    border-top: 0
}

.card-body {
    flex: 1 1 auto;
    padding: 1rem 1rem
}

.card-title {
    margin-bottom: .5rem
}

.card-subtitle {
    margin-top: -.25rem;
    margin-bottom: 0
}

.card-text:last-child {
    margin-bottom: 0
}

.card-link + .card-link {
    margin-left: 1rem
}

.card-header {
    padding: .5rem 1rem;
    margin-bottom: 0;
    background-color: rgba(0, 0, 0, .03);
    border-bottom: 1px solid rgba(0, 0, 0, .125)
}

.card-header:first-child {
    border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0
}

.card-footer {
    padding: .5rem 1rem;
    background-color: rgba(0, 0, 0, .03);
    border-top: 1px solid rgba(0, 0, 0, .125)
}

.card-footer:last-child {
    border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px)
}

.card-header-tabs {
    margin-right: -.5rem;
    margin-bottom: -.5rem;
    margin-left: -.5rem;
    border-bottom: 0
}

.card-header-pills {
    margin-right: -.5rem;
    margin-left: -.5rem
}

.card-img-overlay {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    padding: 1rem;
    border-radius: calc(.25rem - 1px)
}

.card-img, .card-img-bottom, .card-img-top {
    width: 100%
}

.card-img, .card-img-top {
    border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px)
}

.card-img, .card-img-bottom {
    border-bottom-right-radius: calc(.25rem - 1px);
    border-bottom-left-radius: calc(.25rem - 1px)
}

.card-group > .card {
    margin-bottom: .75rem
}

@media (min-width: 576px) {
    .card-group {
        display: flex;
        flex-flow: row wrap
    }

    .card-group > .card {
        flex: 1 0 0%;
        margin-bottom: 0
    }

    .card-group > .card + .card {
        margin-left: 0;
        border-left: 0
    }

    .card-group > .card:not(:last-child) {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0
    }

    .card-group > .card:not(:last-child) .card-header, .card-group > .card:not(:last-child) .card-img-top {
        border-top-right-radius: 0
    }

    .card-group > .card:not(:last-child) .card-footer, .card-group > .card:not(:last-child) .card-img-bottom {
        border-bottom-right-radius: 0
    }

    .card-group > .card:not(:first-child) {
        border-top-left-radius: 0;
        border-bottom-left-radius: 0
    }

    .card-group > .card:not(:first-child) .card-header, .card-group > .card:not(:first-child) .card-img-top {
        border-top-left-radius: 0
    }

    .card-group > .card:not(:first-child) .card-footer, .card-group > .card:not(:first-child) .card-img-bottom {
        border-bottom-left-radius: 0
    }
}
th {
    background-color: black ;
    color: white;
    font-weight: normal;
    padding: 20px 30px;
    text-align: center;
  }
  .red{
     background-color: #a31d2d !important;
     color: white;
     padding: 20px 30px;
  }
  .yellow {
     background-color: #fff200 !important;
     color: black;
  }
  .green {
     background-color: #00a651 !important;
     color: black;
  }
  td{
    padding: 10px 20px;
  }
    
    </style>
    <title>Minimum Thresholds Report</title>
  </head>
  <body>
    <div class="container">
        <div class="row">
            <h1>Inventory Report</h1>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Minimum Thresholds Report</h5>
                        <table class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th scope="col">Barcode</th>
                              <th scope="col">Item Name</th>
                              <th scope="col">Current QOH</th>
                              <th scope="col">Min Threshold</th>
                              <th scope="col">Quote</th>
                              <th scope="col">PO Due Date</th>
                            </tr>
                          </thead>
                          <tbody>
                          ';
$max = count($lowReport_v2);
$i = 0;
while($i < ($max)){
    if($lowReport_v2[$i]['quote'] === null){
        $class = 'red';
        $body .= '<tr class="'.$class.'">
                  <td data-label="Barcode">' . $lowReport_v2[$i]['barcode'] . '</td>
                  <td data-label="Item Name">' . $lowReport_v2[$i]['item_name'] . '</td>
                  <td data-label="Current QOH">' . $lowReport_v2[$i]['qty'] . '</td>
                  <td data-label="Min Threshold">' . $lowReport_v2[$i]['min_qty'] . '</td>
                  <td data-label="Quote">' . $lowReport_v2[$i]['quote'] . '</td>
                  <td data-label="PO Due Date">' . $lowReport_v2[$i]['qty_requ'] . '</td>
              </tr>';
        unset($lowReport_v2[$i]);
    }
    $i++;
}
$i = 0;
while($i < ($max)){
    if($lowReport_v2[$i]['quote'] !== null  && ($lowReport_v2[$i]['qty_requ'] === '0000-00-00')){
        $class = 'yellow';
        $body .= '<tr class="'.$class.'">
                  <td data-label="Barcode">' . $lowReport_v2[$i]['barcode'] . '</td>
                  <td data-label="Item Name">' . $lowReport_v2[$i]['item_name'] . '</td>
                  <td data-label="Current QOH">' . $lowReport_v2[$i]['qty'] . '</td>
                  <td data-label="Min Threshold">' . $lowReport_v2[$i]['min_qty'] . '</td>
                  <td data-label="Quote">' . $lowReport_v2[$i]['quote'] . '</td>
                  <td data-label="PO Due Date">' . $lowReport_v2[$i]['qty_requ'] . '</td>
              </tr>';
        unset($lowReport_v2[$i]);
    }
    $i++;
}
$i = 0;
while($i < ($max)){
    if(($lowReport_v2[$i]['quote'] !== NULL) && ($lowReport_v2[$i]['qty_requ'] !== '0000-00-00')){
        $class = 'green';
        $body .= '<tr class="'.$class.'">
                  <td data-label="Barcode">' . $lowReport_v2[$i]['barcode'] . '</td>
                  <td data-label="Item Name">' . $lowReport_v2[$i]['item_name'] . '</td>
                  <td data-label="Current QOH">' . $lowReport_v2[$i]['qty'] . '</td>
                  <td data-label="Min Threshold">' . $lowReport_v2[$i]['min_qty'] . '</td>
                  <td data-label="Quote">' . $lowReport_v2[$i]['quote'] . '</td>
                  <td data-label="PO Due Date">' . $lowReport_v2[$i]['qty_requ'] . '</td>
              </tr>';
        unset($lowReport_v2[$i]);
    }
    $i++;
}
$body .= '
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title">Coil Minimum Thresholds Report </h5>
                    <table class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th scope="col">Coil Color</th>
                              <th scope="col">QTY</th>
                              <th scope="col">Total Weight (lb)</th>
                              <th scope="col">Minimum Threshold</th>
                            </tr>
                          </thead>
                          <tbody>
                          ';
while ($r2 = mysqli_fetch_array($g2)) {
    if ($r2['Quantity'] <= $r2['min_group']) {
        $body .= '<tr>
              <td>' . $r2['item_name'] . '</td>
              <td>' . number_format($r2['Quantity']) . '</td>
              <td>' . number_format($r2['Total Weight']) . '</td>
              <td>' . $r2['min_group'] . '</td>
            </tr>';
    }
}
$body .= '
                          </tbody>
                        </table>
                  </div>
                </div>
            </div> 
        </div>
    </div>
  </body>
</html>';

echo $body;

include '../../phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;

//Get SMTP Connection Details...
$mailconn = mysqli_connect('185.28.23.230:6603','root','secret','marketfo_mf_acerotx') or die($conn->error);
$mailq = "SELECT * FROM `mailgun_credentials`";
$mailg = mysqli_query($mailconn, $mailq) or die($mailconn->error);
$mailr = mysqli_fetch_array($mailg);

//Settings for the PHPMailer Class

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = $mailr['host'];  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = $mailr['username'];                 // SMTP username
$mail->Password = $mailr['password'];                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->isHTML(true);
$mail->setFrom('collections@allsteelcarports.com','Marketforce');
$mail->addAddress('michael.burton@acero.industries', 'Michael Burton');
$mail->addAddress('chuck@acero.industries', 'Chuck Wiedenhoefer');
$mail->addAddress('justin@acero.industries', 'Justin');
$mail->addAddress('roberto.cabrera@acero.industries', 'Roberto Cabrera');
$mail->addAddress('Mark@acero.industries', 'Mark');
$mail->addAddress('igchavezcastillo@gmail.com', 'Ignacio Chavez');
$mail->addAddress('Jesus.Cabrera@acero.industries', 'Jesus Cabrera');/*
$mail->addAddress('fabian@grupocomunicado.com', 'Fabian Rivera');*/
$mail->addAddress('victor@grupocomunicado.com', 'Victor Rojas');
$mail->Subject = 'Inventory Report';
$mail->Body = $body;
$mail->AltBody    = $body;
if (!$mail->send()) {
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'The email message was sent.';
}
/*
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
//michael.burton@acero.industries,chuck@acero.industries,justin@acero.industries,roberto.cabrera@acero.industries
mail('victor@grupocomunicado.com',
    'Low Inventory Report',
    $body,
    $headers
);
*/