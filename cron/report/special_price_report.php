<?php
$db_host = '185.28.23.230:6603';
$db_user = 'root';
$db_pass = 'secret';
$db_mf_db = 'marketfo_marketforce';
$db_db1 = 'marketfo_mf_acerotx';
$db_db2 = 'marketfo_mf_aceroca';
$db_db3 = 'marketfo_mf_allsteelcarports';
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_db1) or die('Main Connection Error: ' . $conn->error);
$sp_report = array();
$select = 'SELECT b.company_name ,c.item_name, c.item_price, a.special_price FROM order_special_pricing AS a, order_customers as b, inventory_items as c WHERE a.cid = b.ID AND a.pid = c.ID AND a.Date = CURRENT_DATE';
$res1 = mysqli_query($conn, $select) or die($conn->error);
while ($r = mysqli_fetch_array($res1)) {
    $array = [
        'name' => $r['company_name'],
        'item_name' => $r['item_name'],
        'item_price' => number_format($r['item_price']),
        'special_price' => number_format($r['special_price'])
    ];
    $sp_report[] = $array;
}
mysqli_close($conn);
$conn2 =  mysqli_connect($db_host,$db_user,$db_pass,$db_db2) or die('Main Connection Error: ' . $conn2->error);
$select = 'SELECT b.company_name ,c.item_name, c.item_price, a.special_price FROM order_special_pricing AS a, order_customers as b, inventory_items as c WHERE a.cid = b.ID AND a.pid = c.ID AND a.Date = CURRENT_DATE';
$g = mysqli_query($conn2, $select) or die($conn2->error);
while ($r = mysqli_fetch_array($g)) {
    $array = [
        'name' => $r['company_name'],
        'item_name' => $r['item_name'],
        'item_price' => number_format($r['item_price']),
        'special_price' => number_format($r['special_price'])
    ];
    $sp_report[] = $array;
}
mysqli_close($conn2);
$conn3 =  mysqli_connect($db_host,$db_user,$db_pass,$db_db3) or die('Main Connection Error: ' . $conn3->error);
$select = 'SELECT b.company_name ,c.item_name, c.item_price, a.special_price FROM order_special_pricing AS a, order_customers as b, inventory_items as c WHERE a.cid = b.ID AND a.pid = c.ID AND a.Date = CURRENT_DATE';
$g = mysqli_query($conn3, $select) or die($conn3->error);
while ($r = mysqli_fetch_array($g)) {
    $array = [
        'name' => $r['company_name'],
        'item_name' => $r['item_name'],
        'item_price' => number_format($r['item_price']),
        'special_price' => number_format($r['special_price'])
    ];
    $sp_report[] = $array;
}
mysqli_close($conn3);
$body = '
 <!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Special Pricing Notification</title>
     <style>
      th {
        background-color: black ;
        color: white;
        font-weight: normal;
        padding: 20px 30px;
        text-align: center;
      }
      td{
        padding: 10px 20px;
        background-color: #c1bbbb !important;
         color: black;
      }
     </style>
   </head>
   <body>
     <h1>Special Pricing Notification!</h1>
     <table class="table table-striped table-bordered">
         <thead>
             <tr>
               <th scope="col">#</th>
               <th scope="col">Company</th>
               <th scope="col">Item Name</th>
               <th scope="col">Special price</th>
             </tr>
         </thead>
         <tbody>';
foreach ($sp_report as $clave => $value) {
    $body .= '<tr>
               <td>' . ($clave+1) . '</td>
               <td>' . $value['name'] . '</td>
               <td>' . $value['item_name'] . '</td>
               <td>' . $value['special_price'] . '</td>
       </tr>';
}
$body .= '
       </tbody>
     </table>
   </body>
 </html>';
echo $body;
include '../../phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer;

//Get SMTP Connection Details...
$mailconn = mysqli_connect('185.28.23.230:6603','root','185.28.23.230:6603','marketfo_mf_acerotx') or die($conn->error);
$mailq = "SELECT * FROM `mailgun_credentials`";
$mailg = mysqli_query($mailconn, $mailq) or die($mailconn->error);
$mailr = mysqli_fetch_array($mailg);

//Settings for the PHPMailer Class

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = $mailr['host'];  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = $mailr['username'];                 // SMTP username
$mail->Password = $mailr['password'];                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->isHTML(true);
$mail->setFrom('collections@allsteelcarports.com','Marketforce');/*
$mail->addAddress('michael.burton@acero.industries', 'Michael Burton');
$mail->addAddress('chuck@acero.industries', 'Chuck Wiedenhoefer');
$mail->addAddress('justin@acero.industries', 'Justin');
$mail->addAddress('roberto.cabrera@acero.industries', 'Roberto Cabrera');
$mail->addAddress('Mark@acero.industries', 'Mark');
$mail->addAddress('fabian@grupocomunicado.com', 'Fabian Rivera');
$mail->addAddress('igchavezcastillo@gmail.com', 'Ignacio Chavez');*/
$mail->addAddress('Jesus.Cabrera@acero.industries', 'Jesus Cabrera');
$mail->addAddress('victor@grupocomunicado.com', 'Victor Rojas');
$mail->Subject = 'Special Pricing Report';
$mail->Body = $body;
$mail->AltBody    = $body;
if (!$mail->send()) {
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'The email message was sent.';
}