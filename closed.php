<?php
include 'php/connection.php';

echo '<html>
      <head>
      <title>Inactive Dealers</title>
      <style>
      td{
        border: 1px solid black;
        padding: 10px;
      }
      </style>
      </head>
      <body>
      <h1>Inactive Dealers</h1>';

echo '<table>
      <tr>
      <th>Name</th><th>Location</th>
      </tr>';

$q = "SELECT * FROM `dealers` WHERE `inactive` = 'Yes'";
$g = mysqli_query($conn, $q) or die($conn->error);
while($r = mysqli_fetch_array($g)){
  echo '<tr><td>' . $r['name'] . '</td><td>' . $r['city'] . ', ' . $r['state'] . '</td></tr>';
}

echo '</table>
      </body>
      </html>';